package ar.gov.tresdefebrero.tish.interceptor;

import java.util.Map;
import java.util.regex.Pattern;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.StrutsStatics;
import org.apache.struts2.util.TokenHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.MethodFilterInterceptor;

import ar.gov.tresdefebrero.tish.util.ApplicationContextHelper;
import ar.gov.tresdefebrero.tish.util.SystemConfiguration;

public class CustomTokenInterceptor extends MethodFilterInterceptor {

	private static final long serialVersionUID = 1313293127657182786L;
	private static final String AJAX_TOKEN = "ajax.token";
	private static final String INVALID_TOKEN_CODE = "invalid.token";

	private static Logger LOG = LoggerFactory.getLogger(CustomTokenInterceptor.class);

	private SystemConfiguration conf;

	@SuppressWarnings("rawtypes")
	@Override
	public String doIntercept(ActionInvocation invocation) throws Exception {
		ActionContext actionContext = invocation.getInvocationContext();

		String sessionToken = (String) actionContext.getSession().get(AJAX_TOKEN);
		HttpServletResponse response = (HttpServletResponse) actionContext.get(StrutsStatics.HTTP_RESPONSE);
		HttpServletRequest request = (HttpServletRequest) actionContext.get(StrutsStatics.HTTP_REQUEST);

		// Se valida que el usuario haya ingresado a la aplicacion por medio de
		// la url definida
		//if (request.getHeader("Referer") != null && !request.getHeader("Referer").contains(getConf().getReferer())) {
		if (request.getHeader("Referer") != null && !Pattern.matches(getConf().getReferer(), request.getHeader("Referer"))) {			
			LOG.warn("Referer inválido {}, debe ser {}", request.getHeader("Referer"), getConf().getReferer());

			actionContext.getSession().clear();
			((org.apache.struts2.dispatcher.SessionMap) actionContext.getSession()).invalidate();
			return INVALID_TOKEN_CODE;
		}

		if (sessionToken != null) {
			String value = getTokenFromCookie(request);

			if (!validToken(value)) {
				LOG.warn("Token invalido - ReqToken {} - SessionToken {}", value, sessionToken);
				generateCookie(response);
				return INVALID_TOKEN_CODE;
			}
		}
		generateCookie(response);

		return invocation.invoke();
	}

	public void generateCookie(HttpServletResponse response) {
		// String token = TokenHelper.setToken(AJAX_TOKEN);
		String token = setToken();
		Cookie cookie = new Cookie(AJAX_TOKEN, token);
		// cookie.setPath(Configuration.getString(ConfigurationConstants.APPLICATION_CONTEXT));
		// cookie.setSecure(Configuration.getBoolean(ConfigurationConstants.COOKIE_SECURITY));
		cookie.setPath(getConf().getApplicationContext()); // conf.getApplicationContext());
		cookie.setSecure(getConf().getSecure()); // conf.getSecurity());

		response.addCookie(cookie);
	}

	private String getTokenFromCookie(HttpServletRequest request) {
		Cookie[] cookies = request.getCookies();
		String value = "";
		int j = 0;
		for (int i = 0; i < cookies.length; i++) {
			
			/*
			 * if (cookies[i].getName().equals(AJAX_TOKEN)) { break; }
			 */
			if (cookies[i].getName().equals(AJAX_TOKEN)) {
				value = cookies[i].getValue();
				j++;
				if (j > 1) {
					LOG.warn("Mas de una cookie de seguridad {}", j);
					cookies[i].setMaxAge(0);
				}
			}
		}

		return value;
	}

	private static boolean validToken(String token) {
		if (token == null) {
			if (LOG.isDebugEnabled())
				LOG.debug("no token found for token name " + AJAX_TOKEN + " -> Invalid token ");
			return false;
		}

		Map<String, Object> session = ActionContext.getContext().getSession();
		String sessionToken = (String) session.get(AJAX_TOKEN);

		if (!token.equals(sessionToken)) {
			/*
			 * LOG.warn(LocalizedTextUtil.findText(TokenHelper.class,
			 * "struts.internal.invalid.token", ActionContext.getContext()
			 * .getLocale(),
			 * "Form token {0} does not match the session token {1}.", new
			 * Object[] { token, sessionToken }));
			 */
			//LOG.warn("Token invalido {} - SessionToken {}", token, sessionToken);

			return false;
		}

		// remove the token so it won't be used again
		session.remove(AJAX_TOKEN);

		return true;
	}

	public static String setToken() {
		Map<String, Object> session = ActionContext.getContext().getSession();
		String token = TokenHelper.generateGUID();
		try {
			session.put(AJAX_TOKEN, token);
		} catch (IllegalStateException e) {
			// WW-1182 explain to user what the problem is
			String msg = "Error creating HttpSession due response is commited to client. You can use the CreateSessionInterceptor or create the HttpSession from your action before the result is rendered to the client: "
					+ e.getMessage();
			LOG.error(msg, e);
			throw new IllegalArgumentException(msg);
		}

		return token;
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	@Override
	public void init() {
		// TODO Auto-generated method stub
		ApplicationContext springContext = ApplicationContextHelper.getApplicationContext();
		conf = (SystemConfiguration) springContext.getBean("systemConf");
	}

	public SystemConfiguration getConf() {
		return conf;
	}

	public void setConf(SystemConfiguration conf) {
		this.conf = conf;
	}
}
