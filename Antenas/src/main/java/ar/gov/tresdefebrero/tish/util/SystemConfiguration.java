package ar.gov.tresdefebrero.tish.util;

public class SystemConfiguration {

	private String rubro;
	private Double montoConvencional;
	private Double montoWicaps;
	
	private String referer;
	private String applicationContext;
	private Boolean secure;

	public String getRubro() {
		return rubro;
	}

	public void setRubro(String rubro) {
		this.rubro = rubro;
	}

	public Double getMontoConvencional() {
		return montoConvencional;
	}

	public void setMontoConvencional(Double montoConvencional) {
		this.montoConvencional = montoConvencional;
	}

	public Double getMontoWicaps() {
		return montoWicaps;
	}

	public void setMontoWicaps(Double montoWicaps) {
		this.montoWicaps = montoWicaps;
	}

	public String getReferer() {
		return referer;
	}

	public void setReferer(String referer) {
		this.referer = referer;
	}

	public String getApplicationContext() {
		return applicationContext;
	}

	public void setApplicationContext(String applicationContext) {
		this.applicationContext = applicationContext;
	}

	public Boolean getSecure() {
		return secure;
	}

	public void setSecure(Boolean secure) {
		this.secure = secure;
	}

}
