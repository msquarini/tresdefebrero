package ar.gov.tresdefebrero.tish.actions;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;

import org.apache.struts2.dispatcher.Parameter;
import org.apache.struts2.interceptor.SessionAware;
import org.apache.struts2.util.ServletContextAware;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import ar.gov.tresdefebrero.tish.beans.PeriodoBean;
import ar.gov.tresdefebrero.tish.beans.SeguridadHigieneBean;
import ar.gov.tresdefebrero.tish.dao.SeguridadHigieneDao;
import ar.gov.tresdefebrero.tish.util.ApplicationContextHelper;
import ar.gov.tresdefebrero.tish.util.SystemConfiguration;
import ar.gov.tresdefebrero.tish.util.VerifyRecaptcha;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;

/**
 * @author msquarini
 *
 */
public class SeguridadHigieneAction extends ActionSupport
    implements SessionAware, ServletContextAware {

  private static final long serialVersionUID = 7353477345330099548L;

  private static final Logger logger = LoggerFactory.getLogger("TasaAntenasAction");

  /*
   * private static final Double MINIMO_CONVENCIONAL = Double.valueOf(18000); private static final
   * Double MINIMO_WICAPS = Double.valueOf(6000);
   */
  private ServletContext context;

  private Integer cuenta;
  private String dv;

  private Double montoPagar;
  // private Double montoDiferencias;

  private Integer idPeriodoSelected;

  /**
   * Tipo de antena 1 Convencional 2 Wicaps
   */
  private Integer tipoAntena;

  private Integer cantidad;

  private SeguridadHigieneDao dao;

  private Map<String, Object> session;

  private InputStream inputStreamA;
  private String fileName;

  private SystemConfiguration conf;

  /**
   * Rubros que aplica la tasa
   */
  private String rubros;

  private Double montoConvencional;
  private Double montoWicaps;

  /**
   * 
   */
  public SeguridadHigieneAction() {
    ApplicationContext springContext = ApplicationContextHelper.getApplicationContext();
    dao = (SeguridadHigieneDao) springContext.getBean("jdbcSeguridadHigieneDao");
    conf = (SystemConfiguration) springContext.getBean("systemConf");
    rubros = conf.getRubro();
    montoConvencional = conf.getMontoConvencional();
    montoWicaps = conf.getMontoWicaps();
    // dao = (SeguridadHigieneDao) springContext.getBean("dummy");
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.apache.struts2.util.ServletContextAware#setServletContext(javax.
   * servlet.ServletContext)
   */
  @Override
  public void setServletContext(ServletContext context) {
    this.context = context;
  }

  public Map<String, Object> getSession() {
    return session;
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.apache.struts2.interceptor.SessionAware#setSession(java.util.Map)
   */
  @Override
  public void setSession(Map<String, Object> session) {
    this.session = session;
  }

  public String input() {
    logger.info("antenas - input");
    // Elimino la cuenta de session
    getSession().remove("cuenta");

    return INPUT;
  }

  public String execute() throws Exception {
    logger.info("antenas - execute, cuenta {}, dv {}", cuenta, dv);


    if (!validateCaptcha()) {
      addActionMessage("Error validacion de seguridad");
      return INPUT;
    }

    if (cuenta == null || (cuenta > 199999 && cuenta < 90000)) {
      addFieldError("cuenta", getText("cuenta.invalida"));
      logger.info("antenas - cuenta invalidad {}", cuenta);
    }

    if (dv == null) {
      addFieldError("dv", getText("dv.invalido"));
      logger.info("antenas - dv is null");
    }

    if (hasFieldErrors()) {
      return INPUT;
    }

    // Buscar la datos de la cuenta
    SeguridadHigieneBean bean = dao.findAccount(cuenta, dv);

    if (bean == null) {
      logger.info("tish - cuenta no entontrada{}", cuenta);
      addActionError(getText("cuenta.notfound"));
      return INPUT;
    }
    if (!rubros.contains(bean.getRubro())) {
      logger.info("tish - rubro {} no valido para cuenta {}", bean.getRubro(), cuenta);
      addActionError(getText("rubro.notvalid"));
      return INPUT;
    }

    PeriodoBean periodo = dao.findPeriodo(1, new Date());
    if (periodo == null) {
      logger.error("tish - periodo no encontrado impuesto {}, fecha {}", 1, new Date());
      addActionError(getText("periodo.notexists"));
      return INPUT;
    }
    bean.setPeriodoBean(periodo);

    // Busqueda de periodos vencidos
    List<PeriodoBean> periodos = dao.findPeriodoPrevios(periodo);
    periodos.add(0, periodo);

    getSession().put("periodos", periodos);
    getSession().put("cuenta", bean);
    setTipoAntena(1);
    this.cantidad = 1;
    this.montoPagar = getMontoConvencional();

    return SUCCESS;
  }

  /**
   * Descargar el PDF
   * 
   * @return
   * @throws Exception
   */
  public String generate() throws Exception {
    logger.info("antenas - periodo {}", idPeriodoSelected);

    SeguridadHigieneBean bean = (SeguridadHigieneBean) getSession().get("cuenta");
    PeriodoBean periodo = obtenerPeriodo(idPeriodoSelected);

    if (bean == null || periodo == null) {
      logger.error("no se encontro en session SHB {} | PB {}", bean, periodo);
      return input();
    }

    logger.info("antenas - generate, datos {}, periodo {}", bean, periodo.getLeyenda());

    if (getMontoPagar() == null) {
      addFieldError("montoPagar", getText("monto.invalido"));
    }

    if (getCantidad() == null || getCantidad() < 1) {
      addFieldError("cantidad", getText("cantidad.invalido"));
    }

    /*
     * if (tipoAntena == 1 && getMontoPagar() < MINIMO_CONVENCIONAL) { addFieldError("montoPagar",
     * getText("antena.convencional.minimo")); } else if (tipoAntena == 2 && getMontoPagar() <
     * MINIMO_WICAPS) { addFieldError("montoPagar", getText("antena.wicaps.minimo")); }
     */

    if (tipoAntena == 1) {
      setMontoPagar(conf.getMontoConvencional());
    } else {
      setMontoPagar(getCantidad() * conf.getMontoWicaps());
    }
    // Actualizo el periodo a pagar con el elegido!!
    bean.setPeriodoBean(periodo);
    calcularMonto(bean, periodo);

    if (hasFieldErrors() || hasActionErrors()) {
      return SUCCESS;
    }

    return "predownload";
  }

  private void calcularMonto(SeguridadHigieneBean bean, PeriodoBean periodo) {
    // Double monto = getMontoDiferencias() + getMontoMinimoAjustado();
    Double monto = getMontoPagar();

    // if (monto > bean.getMontoMinimo().doubleValue()) {
    // //bean.setMontoDiferencias(BigDecimal.valueOf(getMontoDiferencias()));
    // bean.setMontoMinimoAjustado(BigDecimal.valueOf(getMontoMinimoAjustado()));
    // bean.setMonto(BigDecimal.valueOf(monto).setScale(2,
    // java.math.RoundingMode.HALF_UP));
    // } else {
    // logger.info("tish - monto menor que el minimo, datos {}", bean);
    // addActionError(getText("monto.menor.minimo"));
    // }
    bean.setMonto(BigDecimal.valueOf(monto).setScale(2, java.math.RoundingMode.HALF_UP));

    if (!periodo.esVigente()) {
      Calendar now = Calendar.getInstance();
      // Fecha vencimiento del periodo seleccionado
      Calendar desde = Calendar.getInstance();
      desde.setTime(periodo.getFechaVto());

      // Calculo de meses de diferencia para aplicar interes
      //int diffYear = now.get(Calendar.YEAR) - desde.get(Calendar.YEAR);
      //double diffMonth = diffYear * 12 + now.get(Calendar.MONTH) - desde.get(Calendar.MONTH);
      // Agregamos el interes del 1% mensual
      // 20170530 MS agregado de 5 dias de interes
      // diffMonth = diffMonth + 0.16;
      // monto = monto + (monto * diffMonth / 100);
      
      double days = daysBetween(desde.getTimeInMillis(), now.getTimeInMillis());
      monto = monto + (monto * (days/30) / 100);
  
      bean.setMonto(BigDecimal.valueOf(monto).setScale(2, java.math.RoundingMode.HALF_UP));
      // Seteo el periodo que se va a pagar
      bean.setPeriodo(periodo.getPeriodo().toString());
      bean.setIdPeriodo(periodo.getIdPeriodo());
      // bean.setFechaVto(periodo.getFechaVto());
      bean.setPeriodoAnio(periodo.getAnio());
    }
  }

  /**
   * Descarga de pdf
   * 
   * @return
   * @throws Exception
   */
  public String download() throws Exception {
    logger.info("descargando volante de pago");
    SeguridadHigieneBean bean = (SeguridadHigieneBean) getSession().get("cuenta");
    if (bean == null) {
      return input();
    }

    try {
      dao.registrarVEP(bean);
    } catch (Exception e) {
      logger.error("", e);
    }

    generarVolantePago(bean);

    // Elimino la cuenta de session
    getSession().remove("cuenta");
    getSession().remove("periodos");

    return "download";
  }

  /**
   * Crear PDF
   */
  private void generarVolantePago(SeguridadHigieneBean bean) {
    try {

      JasperReport jasperReport =
          JasperCompileManager.compileReport("/www/tomcat/conf/antenas/antenas.jrxml");

      // JasperReport jasperReport = JasperCompileManager
      // .compileReport(context.getResourceAsStream("/WEB-INF/classes/tish.jrxml"));

      Map<String, Object> params = new HashMap<String, Object>();
      params.put("bean", bean);

      // Ejecucion de fillReport
      JasperPrint jasperPrint =
          JasperFillManager.fillReport(jasperReport, params, new JREmptyDataSource());

      // Export
      File pdf = File.createTempFile("tasaantenas-" + bean.getCuenta(), ".pdf");

      setFileName(pdf.getName());

      JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
      inputStreamA = new FileInputStream(pdf);
    } catch (JRException e) {
      logger.error("", e);
    } catch (IOException e) {
      logger.error("", e);
    }
  }

  public Integer getCuenta() {
    return cuenta;
  }

  public void setCuenta(Integer cuenta) {
    this.cuenta = cuenta;
  }

  public String getDv() {
    return dv;
  }

  public void setDv(String dv) {
    this.dv = dv;
  }

  public InputStream getInputStreamA() {
    return inputStreamA;
  }

  public void setInputStreamA(InputStream inputStream) {
    this.inputStreamA = inputStream;
  }

  public String getFileName() {
    return fileName;
  }

  public void setFileName(String fileName) {
    this.fileName = fileName;
  }

  public Integer getIdPeriodoSelected() {
    return idPeriodoSelected;
  }

  public void setIdPeriodoSelected(Integer idPeriodoSelected) {
    this.idPeriodoSelected = idPeriodoSelected;
  }

  private PeriodoBean obtenerPeriodo(Integer idPeriodo) {
    List<PeriodoBean> periodos = (List<PeriodoBean>) getSession().get("periodos");
    PeriodoBean bean = null;
    for (PeriodoBean p : periodos) {
      if (p.getIdPeriodo() == idPeriodo) {
        bean = p;
        break;
      }
    }
    return bean;
  }

  public Integer getTipoAntena() {
    return tipoAntena;
  }

  public void setTipoAntena(Integer tipoAntena) {
    this.tipoAntena = tipoAntena;
  }

  public Double getMontoPagar() {
    return montoPagar;
  }

  public void setMontoPagar(Double montoPagar) {
    this.montoPagar = montoPagar;
  }

  public Integer getCantidad() {
    return cantidad;
  }

  public void setCantidad(Integer cantidad) {
    this.cantidad = cantidad;
  }

  public String getRubros() {
    return rubros;
  }

  public void setRubros(String rubros) {
    this.rubros = rubros;
  }

  public Double getMontoConvencional() {
    return montoConvencional;
  }

  public void setMontoConvencional(Double montoConvencional) {
    this.montoConvencional = montoConvencional;
  }

  public Double getMontoWicaps() {
    return montoWicaps;
  }

  public void setMontoWicaps(Double montoWicaps) {
    this.montoWicaps = montoWicaps;
  }

  /**
   * Verificar captcha
   * 
   * @return
   */
  private boolean validateCaptcha() {
    // Map<String, Object> params = ActionContext.getContext().getParameters();
    Map<String, Parameter> params = ActionContext.getContext().getParameters();
    // String[] response = (String[]) params.get("g-recaptcha-response");
    Parameter p = (Parameter) params.get("g-recaptcha-response");

    try {
      return VerifyRecaptcha.verify(p.getValue());
    } catch (IOException e) {
      logger.error("", e);
      return false;
    }
  }

  private int daysBetween(long t1, long t2) {
    return (int) ((t2 - t1) / (1000 * 60 * 60 * 24));
  }
}
