package ar.gov.tresdefebrero.tish.beans;

import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang3.time.DateUtils;

/**
 * Periodo de pago de un impuesto
 * 
 * @author UltraBook
 *
 */
public class PeriodoBean {

  private Integer idPeriodo;
  private Integer impuesto;
  private Integer periodo;
  private Integer anio;

  /**
   * Fecha de vencimiento del impuesto (pdf)
   */
  private Date fechaVto;

  /**
   * fecha de pago (pdf)
   */
  private Date fechaPago;

  /**
   * fecha de vigencia del periodo
   */
  private Date fechaVigencia;

  /**
   * fecha de proximo vencimiento del impuesto (pdf)
   */
  private Date fechaProximoPago;

  /**
   * fecha de codigo de barras
   */
  private Date fechaBarCode;

  /**
   * Indice para calcular interes
   */
  private Double indice;

  private Double minimo;

  /**
   * Importe minimo de deuda de la cuenta en el periodo y anio
   */
  private Double importe;

  /**
   * @return the impuesto
   */
  public Integer getImpuesto() {
    return impuesto;
  }

  /**
   * @param impuesto the impuesto to set
   */
  public void setImpuesto(Integer impuesto) {
    this.impuesto = impuesto;
  }

  /**
   * @return the periodo
   */
  public Integer getPeriodo() {
    return periodo;
  }

  /**
   * @param periodo the periodo to set
   */
  public void setPeriodo(Integer periodo) {
    this.periodo = periodo;
  }

  /**
   * @return the anio
   */
  public Integer getAnio() {
    return anio;
  }

  /**
   * @param anio the anio to set
   */
  public void setAnio(Integer anio) {
    this.anio = anio;
  }

  /**
   * @return the fechaVto
   */
  public Date getFechaVto() {
    return fechaVto;
  }

  /**
   * @param fechaVto the fechaVto to set
   */
  public void setFechaVto(Date fechaVto) {
    this.fechaVto = fechaVto;
  }

  /**
   * @return the fechaPago
   */
  public Date getFechaPago() {
    return fechaPago;
  }

  /**
   * @param fechaPago the fechaPago to set
   */
  public void setFechaPago(Date fechaPago) {
    this.fechaPago = fechaPago;
  }

  /**
   * @return the fechaVigencia
   */
  public Date getFechaVigencia() {
    return fechaVigencia;
  }

  /**
   * @param fechaVigencia the fechaVigencia to set
   */
  public void setFechaVigencia(Date fechaVigencia) {
    this.fechaVigencia = fechaVigencia;
  }

  /**
   * @return the fechaProximoPago
   */
  public Date getFechaProximoPago() {
    return fechaProximoPago;
  }

  /**
   * @param fechaProximoPago the fechaProximoPago to set
   */
  public void setFechaProximoPago(Date fechaProximoPago) {
    this.fechaProximoPago = fechaProximoPago;
  }

  /**
   * @return the fechaBarCode
   */
  public Date getFechaBarCode() {
    return fechaBarCode;
  }

  /**
   * @param fechaBarCode the fechaBarCode to set
   */
  public void setFechaBarCode(Date fechaBarCode) {
    this.fechaBarCode = fechaBarCode;
  }

  public String getLeyenda() {
    String leyenda = anio + "/" + periodo;
    if (esVigente()) {
      leyenda = leyenda + " - Vigente";
    }
    return leyenda;
  }

  public boolean esVigente() {
    // return getFechaVigencia().after(Calendar.getInstance().getTime());

    if (getFechaVigencia() == null) {
      return false;
    }
    Calendar c = Calendar.getInstance();
//    c.set(Calendar.HOUR, 0);
//    c.set(Calendar.MINUTE, 0);
//    c.set(Calendar.SECOND, 0);

//    return getFechaVigencia().after(c.getTime());
    
    // 20171001 MS - Fecha vigencia es mayor o IGUAL que fecha actual
    return (DateUtils.truncatedCompareTo(getFechaVigencia(), c.getTime(), Calendar.DATE) >= 0);
  }

  public Integer getIdPeriodo() {
    return idPeriodo;
  }

  public void setIdPeriodo(Integer idPeriodo) {
    this.idPeriodo = idPeriodo;
  }

	public Double getIndice() {
		return indice;
	}

	public void setIndice(Double indice) {
		this.indice = indice;
	}

	public Double getMinimo() {
		// Si el importe (minimo para la cuenta en el periodo adeudado)
		if (getImporte() != null && getImporte() > minimo) {
			return getImporte();
		}
		return minimo;
	}

	public void setMinimo(Double minimo) {
		this.minimo = minimo;
	}

	public Double getImporte() {
		return importe;
	}

	public void setImporte(Double importe) {
		this.importe = importe;
	}
}
