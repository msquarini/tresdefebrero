package ar.gov.tresdefebrero.tish.beans;

public class Nomenclatura {

    private Integer circunscripcion;
    private String seccion;

    private Integer codManzana;
    private Integer manzanaNro;
    private String manzanaLetra;

    private Integer parcelaNro;
    private String parcelaLetra;
    private String subParcela;

    /**
     * @return the circunscripcion
     */
    public Integer getCircunscripcion() {
        return circunscripcion;
    }

    /**
     * @param circunscripcion
     *            the circunscripcion to set
     */
    public void setCircunscripcion(Integer circunscripcion) {
        this.circunscripcion = circunscripcion;
    }

    /**
     * @return the seccion
     */
    public String getSeccion() {
        return seccion;
    }

    /**
     * @param seccion
     *            the seccion to set
     */
    public void setSeccion(String seccion) {
        this.seccion = seccion;
    }

    /**
     * @return the codManzana
     */
    public Integer getCodManzana() {
        return codManzana;
    }

    /**
     * @param codManzana
     *            the codManzana to set
     */
    public void setCodManzana(Integer codManzana) {
        this.codManzana = codManzana;
    }

    /**
     * @return the manzanaNro
     */
    public Integer getManzanaNro() {
        return manzanaNro;
    }

    /**
     * @param manzanaNro
     *            the manzanaNro to set
     */
    public void setManzanaNro(Integer manzanaNro) {
        this.manzanaNro = manzanaNro;
    }

    /**
     * @return the manzanaLetra
     */
    public String getManzanaLetra() {
        return manzanaLetra;
    }

    /**
     * @param manzanaLetra
     *            the manzanaLetra to set
     */
    public void setManzanaLetra(String manzanaLetra) {
        this.manzanaLetra = manzanaLetra;
    }

    /**
     * @return the parcelaNro
     */
    public Integer getParcelaNro() {
        return parcelaNro;
    }

    /**
     * @param parcelaNro
     *            the parcelaNro to set
     */
    public void setParcelaNro(Integer parcelaNro) {
        this.parcelaNro = parcelaNro;
    }

    /**
     * @return the parcelaLetra
     */
    public String getParcelaLetra() {
        return parcelaLetra;
    }

    /**
     * @param parcelaLetra
     *            the parcelaLetra to set
     */
    public void setParcelaLetra(String parcelaLetra) {
        this.parcelaLetra = parcelaLetra;
    }

    /**
     * @return the subParcela
     */
    public String getSubParcela() {
        return subParcela;
    }

    /**
     * @param subParcela
     *            the subParcela to set
     */
    public void setSubParcela(String subParcela) {
        this.subParcela = subParcela;
    }

}
