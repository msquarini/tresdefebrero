package ar.gov.tresdefebrero.tish.beans;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import ar.gov.tresdefebrero.tish.util.NumToText;

public class SeguridadHigieneBean {

  private Integer cuenta;
  private String dv;

  private String razonSocial;
  private String cuit;

  private int numeroCuota;

  /**
   * Se reemplaza por el bean de periodo !!!!!!!!!!!!!
   */
  private Date fechaVto;
  private Date fechaAbono;
  private Date fechaProx;
  private Date fechaBarCode;

  private Integer idPeriodo;

  private String periodo;
  private Integer periodoAnio;

  private PeriodoBean periodoBean;

  private Nomenclatura nomenclatura;
  private String rubro;
  private String rubroDescripcion;

  private String zona;

  // total
  private BigDecimal monto;

  // tasa_minima
  private BigDecimal montoMinimo;

  // Cargados por pantalla
  private BigDecimal montoMinimoAjustado;
  private BigDecimal montoDiferencias;

  private String domicilio;
  private String localidad;

  public SeguridadHigieneBean() {}

  /**
   * @return the cuenta
   */
  public Integer getCuenta() {
    return cuenta;
  }

  /**
   * @param cuenta the cuenta to set
   */
  public void setCuenta(Integer cuenta) {
    this.cuenta = cuenta;
  }

  /**
   * @return the dv
   */
  public String getDv() {
    return dv;
  }

  /**
   * @param dv the dv to set
   */
  public void setDv(String dv) {
    this.dv = dv;
  }

  /**
   * @return the razonSocial
   */
  public String getRazonSocial() {
    return razonSocial;
  }

  /**
   * @param razonSocial the razonSocial to set
   */
  public void setRazonSocial(String razonSocial) {
    this.razonSocial = razonSocial;
  }

  /**
   * @return the cuit
   */
  public String getCuit() {
    return cuit;
  }

  /**
   * @param cuit the cuit to set
   */
  public void setCuit(String cuit) {
    this.cuit = cuit;
  }

  /**
   * @return the numeroCuota
   */
  public int getNumeroCuota() {
    return numeroCuota;
  }

  /**
   * @param numeroCuota the numeroCuota to set
   */
  public void setNumeroCuota(int numeroCuota) {
    this.numeroCuota = numeroCuota;
  }

  /**
   * @return the monto
   */
  public BigDecimal getMonto() {
    return monto;
  }

  /**
   * @param monto the monto to set
   */
  public void setMonto(BigDecimal monto) {
    this.monto = monto;
  }

  /**
   * @return the fechaVto
   */
  public Date getFechaVto() {
    return fechaVto;
  }

  /**
   * @param fechaVto the fechaVto to set
   */
  public void setFechaVto(Date fechaVto) {
    this.fechaVto = fechaVto;
  }

  /**
   * @return the nomenclatura
   */
  public Nomenclatura getNomenclatura() {
    return nomenclatura;
  }

  /**
   * @param nomenclatura the nomenclatura to set
   */
  public void setNomenclatura(Nomenclatura nomenclatura) {
    this.nomenclatura = nomenclatura;
  }

  /**
   * @return the fechaAbono
   */
  public Date getFechaAbono() {
    return fechaAbono;
  }

  /**
   * @param fechaAbono the fechaAbono to set
   */
  public void setFechaAbono(Date fechaAbono) {
    this.fechaAbono = fechaAbono;
  }

  /**
   * @return the fechaProx
   */
  public Date getFechaProx() {
    return fechaProx;
  }

  /**
   * @param fechaProx the fechaProx to set
   */
  public void setFechaProx(Date fechaProx) {
    this.fechaProx = fechaProx;
  }

  public String getNomenclaturaLeyenda() {
    StringBuffer sb = new StringBuffer("Circ. ");
    sb.append(getNomenclatura().getCircunscripcion()).append(" Secc. ")
        .append(getNomenclatura().getSeccion()).append(" CMz. ");
    if (getNomenclatura().getCodManzana() != null) {
      sb.append(getNomenclatura().getCodManzana());
    }
    sb.append(" Mz. ");
    if (getNomenclatura().getManzanaNro() != null) {
      sb.append(getNomenclatura().getManzanaNro());
    }
    if (getNomenclatura().getManzanaLetra() != null) {
      sb.append(getNomenclatura().getManzanaLetra());
    }
    sb.append(" Pc. ");
    if (getNomenclatura().getParcelaNro() != null) {
      sb.append(getNomenclatura().getParcelaNro());
    }
    if (getNomenclatura().getParcelaLetra() != null) {
      sb.append(getNomenclatura().getParcelaLetra());
    }
    if (getNomenclatura().getSubParcela() != null) {
      sb.append(" Poli. ").append(getNomenclatura().getSubParcela());
    }
    return sb.toString();
  }

  /**
   * @return the rubro
   */
  public String getRubro() {
    return rubro;
  }

  /**
   * @param rubro the rubro to set
   */
  public void setRubro(String rubro) {
    this.rubro = rubro;
  }

  /**
   * @return the periodo
   */
  public String getPeriodo() {
    return periodo;
  }

  /**
   * @param periodo the periodo to set
   */
  public void setPeriodo(String periodo) {
    this.periodo = periodo;
  }

  /**
   * @return the zona
   */
  public String getZona() {
    return zona;
  }

  /**
   * @param zona the zona to set
   */
  public void setZona(String zona) {
    this.zona = zona;
  }

  /**
   * @return the montoMinimo
   */
  public BigDecimal getMontoMinimo() {
    return montoMinimo;
  }

  /**
   * @param montoMinimo the montoMinimo to set
   */
  public void setMontoMinimo(BigDecimal montoMinimo) {
    this.montoMinimo = montoMinimo;
  }

  /**
   * @return the montoMinimoAjustado
   */
  public BigDecimal getMontoMinimoAjustado() {
    return montoMinimoAjustado;
  }

  /**
   * @param montoMinimoAjustado the montoMinimoAjustado to set
   */
  public void setMontoMinimoAjustado(BigDecimal montoMinimoAjustado) {
    this.montoMinimoAjustado = montoMinimoAjustado;
  }

  /**
   * @return the montoDiferencias
   */
  public BigDecimal getMontoDiferencias() {
    return montoDiferencias;
  }

  /**
   * @param montoDiferencias the montoDiferencias to set
   */
  public void setMontoDiferencias(BigDecimal montoDiferencias) {
    this.montoDiferencias = montoDiferencias;
  }

  public String getMontoEnLetras() {
    NumToText ntt = new NumToText();
    return ntt.convertirLetras(getMonto());
  }

  /**
   * @return the rubroDescripcion
   */
  public String getRubroDescripcion() {
    return rubroDescripcion;
  }

  /**
   * @param rubroDescripcion the rubroDescripcion to set
   */
  public void setRubroDescripcion(String rubroDescripcion) {
    this.rubroDescripcion = rubroDescripcion;
  }

  /**
   * @return
   */
  public String getBarcode() {

    /**
     * Validar las cuentas en estos rangos 000000 / 199999 900000 / 999999
     * 
     */
    StringBuffer barcode = new StringBuffer();

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy");

    // anio YYYY
    // barcode.append(sdf.format(getFechaVto()));
    barcode.append(getPeriodoAnio());

    // cuota (3)
    // barcode.append(String.format("%03d", Integer.valueOf(getPeriodo())));
    barcode.append(String.format("%03d", Integer.valueOf(getPeriodo())));

    // cuenta (6)
    barcode.append(String.format("%06d", Integer.valueOf(getCuenta())));

    // dv (1), si es "-" entonces se traduce a 0
    if (getDv().equals("-")) {
      barcode.append(String.format("%01d", 0));
    } else {
      barcode.append(String.format("%01d", Integer.valueOf(getDv())));
    }

    // parte entera monto (10)
    barcode.append(String.format("%010d", getMonto().intValue()));
    BigDecimal centavos = getMonto().subtract(getMonto().setScale(0, RoundingMode.FLOOR))
        .movePointRight(getMonto().scale());
    // parte decimanl monto (2)
    barcode.append(String.format("%02d", centavos.intValue()));

    sdf = new SimpleDateFormat("yyyyMMdd");
    // Fecha vencimiento YYYYMMDD
    // barcode.append(sdf.format(periodoBean.getFechaBarCode()));
    // 20170810 MS cambio para que coincida con fecha de vencimiento en periodos no vigentes
    barcode.append(sdf.format(getFechaBarCode()));

    return barcode.toString();
  }

  @Override
  public String toString() {
    StringBuffer datos = new StringBuffer();
    datos.append("Cuenta ").append(getCuenta()).append(", dv ").append(getDv());
    return datos.toString();
  }

  /**
   * @return the domicilio
   */
  public String getDomicilio() {
    return domicilio;
  }

  /**
   * @param domicilio the domicilio to set
   */
  public void setDomicilio(String domicilio) {
    this.domicilio = domicilio;
  }

  /**
   * @return the localidad
   */
  public String getLocalidad() {
    return localidad;
  }

  /**
   * @param localidad the localidad to set
   */
  public void setLocalidad(String localidad) {
    this.localidad = localidad;
  }

  /**
   * @return the periodoBean
   */
  public PeriodoBean getPeriodoBean() {
    return periodoBean;
  }

  /**
   * @param periodoBean the periodoBean to set
   */
  public void setPeriodoBean(PeriodoBean periodoBean) {
    this.periodoBean = periodoBean;
    setIdPeriodo(periodoBean.getIdPeriodo());
    setPeriodo(periodoBean.getPeriodo().toString());

    /*
     * setFechaVto(periodoBean.getFechaVto()); setFechaAbono(periodoBean.getFechaPago());
     * setFechaProx(periodoBean.getFechaProximoPago()); setPeriodoAnio(periodoBean.getAnio());
     */

    if (!periodoBean.esVigente()) {
      Calendar c = Calendar.getInstance();
      c.add(Calendar.DATE, 5);
      setFechaVto(c.getTime());
      setFechaAbono(c.getTime());
      setFechaBarCode(c.getTime());
      setFechaProx(periodoBean.getFechaProximoPago());
    } else {
      setFechaVto(periodoBean.getFechaVto());
      setFechaAbono(periodoBean.getFechaPago());
      setFechaProx(periodoBean.getFechaProximoPago());
      setFechaBarCode(periodoBean.getFechaBarCode());
    }
    setPeriodoAnio(periodoBean.getAnio());
  }

  public Integer getIdPeriodo() {
    return idPeriodo;
  }

  public void setIdPeriodo(Integer idPeriodo) {
    this.idPeriodo = idPeriodo;
  }

  public Integer getPeriodoAnio() {
    return periodoAnio;
  }

  public void setPeriodoAnio(Integer periodoAnio) {
    this.periodoAnio = periodoAnio;
  }

  public Date getFechaBarCode() {
    return fechaBarCode;
  }

  public void setFechaBarCode(Date fechaBarCode) {
    this.fechaBarCode = fechaBarCode;
  }
}
