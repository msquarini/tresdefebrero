package ar.gov.tresdefebrero.tish.dao;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import ar.gov.tresdefebrero.tish.beans.Nomenclatura;
import ar.gov.tresdefebrero.tish.beans.PeriodoBean;
import ar.gov.tresdefebrero.tish.beans.SeguridadHigieneBean;

public class JdbcSeguridadHigieneDao implements SeguridadHigieneDao {

	private static final Logger logger = LoggerFactory.getLogger("JdbcSeguridadHigieneDao");

	private JdbcTemplate jdbcTemplate;

	private static final String SELECT_TISH = "select cuenta, digito_verificador, razon_social, calle, altura, "
			+ "codigo_postal, localidad,rubro,zona, tasa_minima, "
			+ " calle|| ' Nro. ' || altura as domicilio, codigo_postal|| ' ' || localidad as cplocalidad,"
			+ "circ,seccion,cod_manzana,mz_nro,mz_letra,mz_letra,pc_nro,pc_letra,subparcela,rubro_descripcion "
			+ "from  tish.tish where cuenta= ? and digito_verificador = ?";

	// private static final String INSERT_VEP = "INSERT INTO vep (cuenta,
	// digito_verificador, minimo_ajustado, diferencia_x_ventas)
	// VALUES(?,?,?,?)";
	private static final String INSERT_VEP = "INSERT INTO tish.vep (cuenta, digito_verificador, total, id_periodo) VALUES(?,?,?,?)";

	/*
	 * private static final String SELECT_PERIODO =
	 * "select periodo, anio, fecha_vto, fecha_pago, fecha_vigencia, fecha_proximo_pago, fecha_bar_code"
	 * +
	 * " from periodo where id_impuesto = ? and fecha_vigencia >= ? ORDER BY fecha_vigencia ASC LIMIT 1"
	 * ;
	 */
	private static final String SELECT_PERIODO = "select id_periodo, periodo, anio, fecha_vto, fecha_pago, fecha_vigencia, fecha_proximo_pago, fecha_bar_code from tish.periodo where id_impuesto = ? and fecha_vigencia >= ? ORDER BY fecha_vigencia ASC LIMIT 1";
	private static final String SELECT_PERIODOS_PREVIOS = "select id_periodo, periodo, anio, fecha_vto, fecha_pago, fecha_vigencia, fecha_proximo_pago, fecha_bar_code from tish.periodo where id_impuesto = ? and id_periodo <> ? and fecha_vigencia < ?  ORDER BY anio DESC, periodo DESC ";

	/*
	 * private static final String SELECT_PERIODOS_PREVIOS =
	 * "select periodo, anio, fecha_vto, fecha_pago, fecha_vigencia, fecha_proximo_pago, fecha_bar_code"
	 * +
	 * " from periodo where id_impuesto = ? and periodo < ?  and anio = ? ORDER BY periodo DESC "
	 * ;
	 */

	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	public void registrarVEP(SeguridadHigieneBean bean) {
		int result = 0;
		try {
			result = this.jdbcTemplate.update(INSERT_VEP,
					new Object[] { bean.getCuenta(), bean.getDv(), bean.getMonto(), bean.getIdPeriodo() },
					new int[] { Types.INTEGER, Types.VARCHAR, Types.NUMERIC, Types.INTEGER });
		} catch (DataAccessException e) {
			logger.error("", e);
		}
		if (result != 1) {
			logger.error("No se pudo persistir el registro de generacion de VEP {} {} {} {}", bean.getCuenta(),
					bean.getDv(), bean.getMontoMinimoAjustado(), bean.getMontoDiferencias());
		}

	}

	public PeriodoBean findPeriodo(final Integer idImpuesto, Date fecha) {
		PeriodoBean bean;
		try {
			bean = this.jdbcTemplate.queryForObject(SELECT_PERIODO, new Object[] { idImpuesto, fecha },
					new RowMapper<PeriodoBean>() {
						public PeriodoBean mapRow(ResultSet rs, int rowNum) throws SQLException {
							PeriodoBean bean = new PeriodoBean();
							bean.setIdPeriodo(Integer.valueOf(rs.getInt("id_periodo")));
							bean.setImpuesto(idImpuesto);
							bean.setPeriodo(rs.getInt("periodo"));
							bean.setAnio(rs.getInt("anio"));
							bean.setFechaVto(rs.getDate("fecha_vto"));
							bean.setFechaPago(rs.getDate("fecha_pago"));
							bean.setFechaVigencia(rs.getDate("fecha_vigencia"));
							bean.setFechaProximoPago(rs.getDate("fecha_proximo_pago"));
							bean.setFechaBarCode(rs.getDate("fecha_bar_code"));
							return bean;
						}
					});
		} catch (EmptyResultDataAccessException e) {
			logger.warn("Periodo no encontrado {} / {}", idImpuesto, fecha);
			return null;
		}
		return bean;
	}

	/**
	 * Buscqueda de periodos previos del mismo anio a un periodo determinado
	 */
	public List<PeriodoBean> findPeriodoPrevios(final PeriodoBean periodo) {
		List<PeriodoBean> beans;
		try {
			beans = this.jdbcTemplate.query(SELECT_PERIODOS_PREVIOS,
					new Object[] { periodo.getImpuesto(), periodo.getIdPeriodo(), periodo.getFechaVigencia() },
					new RowMapper<PeriodoBean>() {
						public PeriodoBean mapRow(ResultSet rs, int rowNum) throws SQLException {
							PeriodoBean bean = new PeriodoBean();
							bean.setIdPeriodo(Integer.valueOf(rs.getInt("id_periodo")));
							bean.setImpuesto(periodo.getImpuesto());
							bean.setPeriodo(rs.getInt("periodo"));
							bean.setAnio(rs.getInt("anio"));
							bean.setFechaVto(rs.getDate("fecha_vto"));
							bean.setFechaPago(rs.getDate("fecha_pago"));
							bean.setFechaVigencia(rs.getDate("fecha_vigencia"));
							bean.setFechaProximoPago(rs.getDate("fecha_proximo_pago"));
							bean.setFechaBarCode(rs.getDate("fecha_bar_code"));
							return bean;
						}
					});
		} catch (EmptyResultDataAccessException e) {
			logger.warn("Periodos previos no encontrado para impuesto {} / {} - {}", periodo.getImpuesto(),
					periodo.getPeriodo(), periodo.getAnio());
			return null;
		}
		return beans;
	}

	public SeguridadHigieneBean findAccount(Integer cuenta, String dv) {
		SeguridadHigieneBean bean;
		try {
			bean = this.jdbcTemplate.queryForObject(SELECT_TISH, new Object[] { cuenta, dv },
					new RowMapper<SeguridadHigieneBean>() {
						public SeguridadHigieneBean mapRow(ResultSet rs, int rowNum) throws SQLException {
							SeguridadHigieneBean bean = new SeguridadHigieneBean();
							bean.setCuenta(rs.getInt("cuenta"));
							bean.setDv(rs.getString("digito_verificador"));
							// bean.setCuit("");
							bean.setRazonSocial(rs.getString("razon_social"));

							bean.setDomicilio(rs.getString("domicilio"));
							bean.setLocalidad(rs.getString("cplocalidad"));

							bean.setMontoMinimo(BigDecimal.valueOf(rs.getInt("tasa_minima") / 100.0));
							logger.info("monto minimo {}", bean.getMontoMinimo());
							Nomenclatura nomenclatura = new Nomenclatura();
							nomenclatura.setCircunscripcion(rs.getInt("circ"));
							nomenclatura.setSeccion(rs.getString("seccion"));
							nomenclatura.setCodManzana(rs.getInt("cod_manzana"));
							nomenclatura.setManzanaNro(rs.getInt("mz_nro"));
							nomenclatura.setManzanaLetra(rs.getString("mz_letra"));
							nomenclatura.setParcelaNro(rs.getInt("pc_nro"));
							nomenclatura.setParcelaLetra(rs.getString("pc_letra"));
							nomenclatura.setSubParcela(rs.getString("subparcela"));

							bean.setNomenclatura(nomenclatura);

							bean.setRubro(rs.getString("rubro"));
							bean.setRubroDescripcion(rs.getString("rubro_descripcion"));
							bean.setZona(rs.getString("zona"));

							return bean;
						}
					});
		} catch (EmptyResultDataAccessException e) {
			logger.warn("cuenta no encontrada {} / {}", cuenta, dv);
			return null;
			// } catch (Exception e) {
			// logger.warn("error ", e);
			// return null;
		}
		return bean;
	}
}