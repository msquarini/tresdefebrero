<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>
<html lang="es">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="keywords" content="Tres de febrero, Seguridad, higiene" />
<meta name="description"
	content="Municipalidad de Tres de Febrero - Tasa Seguridad e Higiene" />
<title>Municipalidad de Tres de Febrero</title>

<!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
<!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
<sb:head includeScripts="false" includeScriptsValidation="false" />
<sj:head locale="es" compressed="true" loadAtOnce="true" jqueryui="true"
	loadFromGoogle="false" />
<script type="text/javascript" src="js/jquery.number.js"></script>

<script src="https://www.google.com/recaptcha/api.js" async defer></script>

<script type="text/javascript">
	$(function() {
		// Set up the number formatting.
		$('#cuenta').number(true, 0, '', '');

		$("#cuenta").attr("autocomplete", "off");
		$("#dv").attr("autocomplete", "off");

		$("#cuenta").focus();

	});

	var verifyCallback = function(response) {
		if (response) {
			$('#btn_enviar').prop('disabled', false)
			//$('#btn_enviar').toggleClass('disabled', false);
		}
	};
</script>

<style type="text/css">
#number_container {
	border: 1px dotted #d0d0d0;
	padding: 15px;
	margin: 10px;
	display: none;
	background: #fafafa;
}

body {
	padding-top: 60px;
	/* 60px to make the container go all the way to the bottom of the topbar */
}

.top-buffer {
	margin-top: 50px;
}

.navbar-brand, .navbar-nav li a {
	line-height: 80px;
	height: 80px;
	padding-top: 0;
}

.footer {
	position: absolute;
	bottom: 0;
	width: 100%;
	/* Set the fixed height of the footer here */
	height: 40px;
	background-color: #f5f5f5;
}

.footer>.container {
	padding-right: 15px;
	padding-left: 15px;
}
</style>
</head>

<body>
	<nav class="navbar navbar-default navbar-fixed-top">
		<div class="row">
			<a class="navbar-brand" href="http://www.tresdefebrero.gov.ar"> <span><img
					src="img/logo-new.png" alt="" height="75" width="180"></span>
			</a>
		</div>
	</nav>

	<div class="container" id="container">

		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<h1>Tasa de Antenas</h1>

				<s:actionerror theme="bootstrap" />
				<s:actionmessage theme="bootstrap" />


				<p>Ingrese la cuenta y d�gito verificador para generar el
					volante electr�nico de pago</p>
				<s:form action="antenas_execute" enctype="multipart/form-data"
					theme="bootstrap" cssClass="form-horizontal" label=""
					id="findAccount" focusElement="cuenta">
					<fieldset>

						<div class="row">
							<div class="col-xs-4 col-sm-4 col-md-3 col-lg-3">
								<label for="cuenta">Cuenta</label>
								<s:textfield id="cuenta" name="cuenta" class="number"
									tooltip="Ingrese el n�mero de cuenta" maxlength="6" />
							</div>
							<div class="col-xs-4 col-sm-3 col-md-2 col-lg-2">
								<label for="DV">D�gito Verificador</label>
								<s:textfield name="dv" id="dv" maxlength="1" />
							</div>
							<div class="col-xs-6 col-sm-5 col-md-6 col-lg-6">
								<div class="g-recaptcha" data-callback="verifyCallback"
									data-sitekey="6Lfe8hwUAAAAAEYizZYXWnxZozT9X-liQPYQyJpR"></div>
							</div>
						</div>
						<div class="row" style="margin-top: 20px;">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<s:url var="findAccount" action="seguridadhigiene_execute" />
								<s:submit id="btn_enviar" cssClass="btn btn-primary" disabled="true"
									value="Enviar"></s:submit>
							</div>
						</div>
					</fieldset>
				</s:form>
			</div>
		</div>
	</div>


	<footer class="footer">
		<div class="container">
			<p align="center">
				� Copyright 2015 - 2016 - <strong>MUNICIPALIDAD DE TRES DE
					FEBRERO</strong> - Conmutador: 4734-2400 - Todos los derechos reservados
			</p>
		</div>
	</footer>


</body>
</html>