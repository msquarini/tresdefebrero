<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<%
    response.setHeader("Cache-Control", "no-cache");
			response.setHeader("Pragma", "no-cache");
			response.setDateHeader("Expires", -1);
%>

<html lang="es">
<head>
<meta http-equiv="Cache-Control"
	content="no-cache, no-store, must-revalidate" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="0" />

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="keywords" content="Tres de febrero, Seguridad, higiene" />
<meta name="description"
	content="Municipalidad de Tres de Febrero - Tasa Seguridad e Higiene" />
<title>Municipalidad de Tres de Febrero</title>

<!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
<!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
<sb:head includeScripts="false" includeScriptsValidation="false" />
<sj:head locale="es" compressed="true" loadAtOnce="true" jqueryui="true"
	loadFromGoogle="false" />

</head>
<style type="text/css">
#number_container {
	border: 1px dotted #d0d0d0;
	padding: 15px;
	margin: 10px;
	display: none;
	background: #fafafa;
}

body {
	padding-top: 60px;
	/* 60px to make the container go all the way to the bottom of the topbar */
}

.top-buffer {
	margin-top: 50px;
}

.navbar-brand, .navbar-nav li a {
	line-height: 80px;
	height: 80px;
	padding-top: 0;
}

.footer {
	position: absolute;
	bottom: 0;
	width: 100%;
	/* Set the fixed height of the footer here */
	height: 40px;
	background-color: #f5f5f5;
}

.footer>.container {
	padding-right: 15px;
	padding-left: 15px;
}
</style>
<body>
	<s:form action="antenas_download" id="download"
		enctype="multipart/form-data" theme="bootstrap">
	</s:form>


	<nav class="navbar navbar-default navbar-fixed-top">
		<div class="row">
				<a class="navbar-brand" href="http://www.tresdefebrero.gov.ar">
					<span><img src="img/logo-new.png" alt="" height="75"
						width="180"></span>
				</a>
		</div>
	</nav>

	<div class="container" id="container">


		<div class="row">
			<div class="col-md-9">
				<h1>Token de seguridad inv�lido.</h1>
			</div>
			<div class="col-md-9">
				<s:url var="findAccount" action="antenas_input" />
				<s:a theme="bootstrap" cssClass="btn btn-primary"
					href="%{findAccount}">Inicio</s:a>
			</div>
		</div>
	</div>
	<footer class="footer">
		<div class="container">
			<p align="center">
				� Copyright 2015 - 2016 - <strong>MUNICIPALIDAD DE TRES DE
					FEBRERO</strong> - Conmutador: 4734-2400 - Todos los derechos reservados
			</p>
		</div>
	</footer>


</body>
