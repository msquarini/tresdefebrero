<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<script type="text/javascript">

</script>
<style type="text/css">
</style>

<div class="sg-container">
	<h1>Plan de regularización de deudas</h1>
	<h3>Impuesto Motocicleta</h3>

	<s:form theme="bootstrap" cssClass="form-vertical">
		<fieldset disabled>
			<div class="row">
				<div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
					<label for="dominio">Dominio</label>
					<s:textfield id="dominio" value="%{#session.cuenta.patente}" />
				</div>
				<div
					class="col-xs-6 col-sm-3 col-md-3 col-lg-3 col-md-offset-1 col-lg-offset-1">
					<label for="titular">Titular</label>
					<s:textfield id="titular" class="form-control"
						value="%{#session.cuenta.titular}" />
				</div>

			</div>
			<div class="row">
				<div class="form-group col-xs-6 col-sm-6 col-md-4 col-lg-4">
					<label for="calle">Calle</label>
					<s:textfield id="calle" class="form-control"
						value="%{#session.cuenta.calle}" />
				</div>
				<div class="form-group col-xs-2 col-sm-2 col-md-1 col-lg-1">
					<label for="numero">Nro</label>
					<s:textfield id="numero" class="form-control"
						value="%{#session.cuenta.numero}" />
				</div>
				<div class="form-group col-xs-2 col-sm-2 col-md-1 col-lg-1">
					<label for="CP">CP</label>
					<s:textfield id="CP" class="form-control"
						value="%{#session.cuenta.codigoPostal}" />
				</div>
				<div class="form-group col-xs-6 col-sm-6 col-md-5 col-lg-5">
					<label for="localidad">Localidad</label>
					<s:textfield id="loalidad" class="form-control"
						value="%{#session.cuenta.localidad}" />
				</div>
			</div>

		</fieldset>
	</s:form>
</div>
