<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<link rel="stylesheet" href="../css/bootstrapValidator.css">
<script src="../js/bootstrapValidator.js"></script>


<script type="text/javascript">
	$('input.number').number(true, 0, '', '');

	function changePlanPago() {
		var $el = $("#planpago-sel");
		var $plan = $("#plan_" + $el.val());

		var cuotas = $plan.val();
		var cuota_desde = Number(cuotas.substr(0, cuotas.indexOf("_")));
		var cuota_hasta = Number(cuotas.substr(cuotas.indexOf("_") + 1,
				cuotas.length));

		var $cuotasel = $("#cuotas-sel");
		$cuotasel.empty(); // remove old options
		if (cuota_desde != 0) {
			for (; cuota_desde <= cuota_hasta; cuota_desde++) {
				$cuotasel.append($("<option></option>").attr("value",
						cuota_desde).text(cuota_desde));

			}
			$cuotasel.val(cuota_hasta);
		}
		verMontos();
	}

	function verMontos() {
		var data = $('#planpago').serialize();
		$.ajax({
			type : "POST",
			url : '/moratoria/moratoria_simularMoratoria.action',
			data : data,
			success : function(data) {
				// data is ur summary
				$('#div-total').html(data);
			}
		});
	}

	changePlanPago();

	$(document).ready(function() {
		$('#planpago').bootstrapValidator({
			feedbackIcons : {
				valid : 'glyphicon glyphicon-ok',
				invalid : 'glyphicon glyphicon-remove',
				validating : 'glyphicon glyphicon-refresh'
			},
			fields : {
				'email' : {
					validators : {
						emailAddress : {
							message : 'Ingrese una direcci�n de email v�lida'
						}
					}
				}
			}
		})
	})

	$.subscribe("beforeFormSubmit", function(event, data) {
		$('#planpago').bootstrapValidator('validate');

		if ($('#planpago').data('bootstrapValidator').isValid()) {
			event.originalEvent.options.submit = true;
		} else {
			event.originalEvent.options.submit = false;
		}
	})
</script>

<style type="text/css">
</style>


<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

		<s:form id="planpago" action="moratoria_simularMoratoria"
			cssClass="form-vertical" namespace="/" theme="bootstrap">
			<div class="row">

				<div class="col-xs-9 col-sm-6 col-md-6 col-lg-6">
					<label for="planpago-sel">Opciones de pago</label>
					<s:select id="planpago-sel" theme="bootstrap" list="planes"
						listValue="descripcion" listKey="idPlanPago" name="idPlanPago"
						onchange="changePlanPago(); ">
					</s:select>
				</div>

				<div class="col-xs-9 col-sm-6 col-md-3 col-lg-3">
					<label for="cuotas-sel">Cant. de cuotas</label> <select
						id="cuotas-sel" class="form-control" name="cuotas"
						onselect="verMontos();" onchange="verMontos();"></select>
				</div>

				<s:iterator value="planes">
					<input type="hidden" id="plan_<s:property value='idPlanPago'/>"
						value="<s:property value='cuotasDesde'/>_<s:property value='cuotasHasta'/>">
				</s:iterator>
			</div>
<%-- 			<div class="row">
				<div class="col-xs-9 col-sm-6 col-md-7 col-lg-7">
					<label for="email">Email de contacto</label>
					<s:textfield id="email" name="email" maxlength="50"
						tooltip="Ingrese un mail de contacto" />
				</div>
				<div class="col-xs-9 col-sm-6 col-md-5 col-lg-5">
					<label for="telefono">Tel�fono de contacto</label>
					<s:textfield id="telefono" name="telefono" class="number"
						maxlength="11" tooltip="Ingrese un tel�fono de contacto" />
				</div>
			</div> --%>
		</s:form>
		<div class="row">
			<div class="col-xs-9 col-sm-6 col-md-3 col-lg-3">
				<s:url action="moratoria_adherir" namespace="/" var="generar"></s:url>
				<sj:submit id="enviar" cssClass="btn btn-primary" formIds="planpago"
					value="Adherir" href="%{generar}" targets="container"
					onBeforeTopics="beforeFormSubmit"></sj:submit>
			</div>
		</div>
	</div>

	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" id="div-total"></div>
</div>
