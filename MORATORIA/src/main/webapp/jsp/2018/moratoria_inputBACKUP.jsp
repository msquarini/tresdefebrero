<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<script type="text/javascript" src="../js/jquery.number.js"></script>
<script type="text/javascript">
	$(function() {
		// No aplica formateo, ya que puede ser alfanumerico!		
		// $('#cuenta').number(true, 0, '', '');

		$("#patente").attr("autocomplete", "off");
		$("#cuenta").attr("autocomplete", "off");
		$("#dv").attr("autocomplete", "off");

		$("#impuesto").focus();
		$("#impuesto").val('<s:property value="impuesto"/>');

		$('#div-patente').hide();
		$('#impuesto').change(function() {
			if (this.selectedIndex < 2) {
				$('#div-patente').show();
				$('#div-cuenta').hide();
			} else {
				$('#div-patente').hide();
				$('#div-cuenta').show();
			}
		});
		$('#impuesto').trigger("change");
	});
</script>

<style type="text/css">
#number_container {
	border: 1px dotted #d0d0d0;
	padding: 15px;
	margin: 10px;
	display: none;
	background: #fafafa;
}

body {
	padding-top: 60px;
	/* 60px to make the container go all the way to the bottom of the topbar */
}

.top-buffer {
	margin-top: 50px;
}

.bottom-buffer {
	margin-bottom: 20px;
}
</style>

<div class="row top-buffer">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<!-- <h1>Plan de regularizaci�n de deudas</h1> -->
		<!-- 		<h1>Si te adheriste al plan de regularizaci�n de deudas que
			finaliz� el 30/12/2016 descarg� tus boletas desde esta p�gina.</h1> -->
		<h2>Nuevo plan de pago vigente hasta 31/07/2017 para los per�odos 2012 al 2016</h2>
		<h3>Seleccion� el impuesto y luego ingres� el n�mero de cuenta o
			patente seg�n corresponda.</h3>
		<s:actionerror theme="bootstrap" />
		<s:actionmessage theme="bootstrap" />

		<s:form action="consulta_search" enctype="multipart/form-data"
			namespace="/" theme="bootstrap" cssClass="form-horizontal" label=""
			id="findAccount" focusElement="impuesto">
			<fieldset>
				<div class="row bottom-buffer">
					<div class="col-xs-9 col-sm-6 col-md-3 col-lg-3">
						<label for="impuesto">Impuesto:</label> <select
							class="form-control" id="impuesto" name="impuesto">
							<option value="1">Automotores</option>
							<option value="2">Motos</option>
							<option value="3">ABL</option>
							<option value="4">TISH - Seguridad e Higiene</option>
						</select>
					</div>
				</div>
				<div class="row toggle" id="div-cuenta">
					<div class="col-xs-9 col-sm-6 col-md-3 col-lg-3">
						<label for="cuenta">Cuenta</label>
						<s:textfield id="cuenta" name="cuenta" class="number"
							maxlength="6" tooltip="Ingrese el n�mero de cuenta" />
					</div>
					<div class="col-xs-4 col-sm-3 col-md-3 col-lg-2">
						<label for="DV">D�gito Verificador</label>
						<s:textfield name="dv" id="dv" />
					</div>
				</div>
				<div class="row toggle" id="div-patente">
					<div class="col-xs-9 col-sm-6 col-md-3 col-lg-3">
						<label for="patente">Patente</label>
						<s:textfield id="patente" name="patente" maxlength="7"
							tooltip="Ingrese numero de patente" />
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<sj:submit cssClass="btn btn-primary" value="Consultar"
							formIds="findAccount" targets="container"></sj:submit>
					</div>
				</div>
			</fieldset>
		</s:form>
	</div>
</div>