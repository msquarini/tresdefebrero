<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<script type="text/javascript">
	
</script>
<style type="text/css">
#number_container {
	border: 1px dotted #d0d0d0;
	padding: 15px;
	margin: 10px;
	display: none;
	background: #fafafa;
}

body {
	padding-top: 60px;
	/* 60px to make the container go all the way to the bottom of the topbar */
}

.top-buffer {
	margin-top: 50px;
}
</style>


<s:form theme="bootstrap" cssClass="form-vertical">
	<fieldset disabled>
		<div class="row">
			<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
				<label for="dominio">Dominio</label>
				<s:textfield id="dominio" value="%{#session.cuenta.dominioActual}" />
			</div>
			<div
				class="col-xs-6 col-sm-6 col-md-3 col-lg-4"> <!--  col-md-offset-1 col-lg-offset-1"> -->
				<label for="titular">Titular</label>
				<s:textfield id="titular" class="form-control"
					value="%{#session.cuenta.titular}" />
			</div>
			<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
				<label for="marca">Marca</label>
				<s:textfield id="marca" value="%{#session.cuenta.descripcionMarca}" />
			</div>
			<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
				<label for="modelo">Modelo</label>
				<s:textfield id="modelo" value="%{#session.cuenta.descripcionModelo}" />
			</div>
		</div>
		<div class="row">
			<div class="form-group col-xs-6 col-sm-6 col-md-4 col-lg-4">
				<label for="calle">Calle</label>
				<s:textfield id="calle" class="form-control"
					value="%{#session.cuenta.calle}" />
			</div>
			<div class="form-group col-xs-2 col-sm-2 col-md-1 col-lg-1">
				<label for="numero">Nro</label>
				<s:textfield id="numero" class="form-control"
					value="%{#session.cuenta.nro}" />
			</div>
			<div class="form-group col-xs-2 col-sm-2 col-md-1 col-lg-1">
				<label for="CP">CP</label>
				<s:textfield id="CP" class="form-control"
					value="%{#session.cuenta.codigoPostal}" />
			</div>
			<div class="form-group col-xs-6 col-sm-6 col-md-6 col-lg-6">
				<label for="localidad">Localidad</label>
				<s:textfield id="loalidad" class="form-control"
					value="%{#session.cuenta.localidad}" />
			</div>
		</div>

	</fieldset>
</s:form>
