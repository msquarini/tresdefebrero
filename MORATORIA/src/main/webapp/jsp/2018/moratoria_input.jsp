<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<script type="text/javascript" src="../js/jquery.number.js"></script>
<link rel="stylesheet" href="../css/bootstrapValidator.css">
<script src="../js/bootstrapValidator.js"></script>

<script type="text/javascript">
	$(function() {
		// No aplica formateo, ya que puede ser alfanumerico!		
		$('#cuit').number(true, 0, '', '');

		$("#patente").attr("autocomplete", "off");
		$("#cuenta").attr("autocomplete", "off");
		$("#dv").attr("autocomplete", "off");

		$("#cuit").focus();
		$("#impuesto").val('<s:property value="impuesto"/>');

		$('#div-patente').hide();
		$('#impuesto').change(function() {
			if (this.selectedIndex < 2) {
				$('#div-patente').show();
				$('#div-cuenta').hide();
			} else if (this.selectedIndex == 2 || this.selectedIndex == 4 || this.selectedIndex == 5) {
				$('#div-patente').hide();
				$('#div-cuenta').show();
			} else {
				$('#div-patente').hide();
				$('#div-cuenta').hide();
			}
		});
		$('#impuesto').trigger("change");
	});

	function validarForm() {
		$('#findAccount').bootstrapValidator('validate');

		if ($('#findAccount').data('bootstrapValidator').isValid()) {
			$('#findAccount').submit();
		}
	}

	$(document)
			.ready(
					function() {

						/* Configuracion del formulario, ya que se hace submit del mismo mediante javascript */
						$('#findAccount').ajaxForm({
							target : '#container',
							success : function(res, textStatus, jqXHR) {
							},
							error : function(res, textStatus, jqXHR) {
							}
						});

						$('#findAccount')
								.bootstrapValidator(
										{
											excluded : [ ':disabled', ':hidden' ],
											feedbackIcons : {
												valid : 'glyphicon glyphicon-ok',
												invalid : 'glyphicon glyphicon-remove',
												validating : 'glyphicon glyphicon-refresh'
											},
											fields : {
												'cuenta' : {
													validators : {
														notEmpty : {
															message : 'Obligatorio'
														}
													}
												},
												'patente' : {
													validators : {
														notEmpty : {
															message : 'Obligatorio'
														}
													}
												},
												'cuit' : {
													validators : {
														notEmpty : {
															message : 'Cuit/Cuil Obligatorio'
														},
														stringLength : {
															min : 11,
															max : 11,
															message : 'El dato ingresado debe ser de 11 posisiones num�ricas'
														},
														callback : {
															message : 'Cuit inv�lido',
															callback : function(
																	value,
																	validator,
																	$field) {
																return validaCuit(value);
															}
														}
													}
												}
											}
										});
					})
</script>

<style type="text/css">
#number_container {
	border: 1px dotted #d0d0d0;
	padding: 15px;
	margin: 10px;
	display: none;
	background: #fafafa;
}

body {
	padding-top: 60px;
	/* 60px to make the container go all the way to the bottom of the topbar */
}

.top-buffer {
	margin-top: 50px;
}

.bottom-buffer {
	margin-bottom: 20px;
}
</style>

<div class="row top-buffer">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<!-- <h2>Nuevo plan de pago vigente hasta 31/08/2018 para los per�odos 2013 al 2017</h2> -->
		<h2>Nuevo plan de pago vigente hasta 31/12/2019 para los per�odos 2014 al 2018</h2>
		<h3>Seleccion� el impuesto y luego ingres� el n�mero de cuenta o
			patente seg�n corresponda.</h3>

		<s:actionerror theme="bootstrap" />
		<s:actionmessage theme="bootstrap" />

		<s:form action="consulta_search" enctype="multipart/form-data"
			namespace="/" theme="bootstrap" cssClass="form-horizontal" label=""
			id="findAccount" focusElement="cuit">
			<fieldset>
				<div class="row toggle" id="div-cuit">
					<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
						<label for="cuenta">Cuit</label>
						<s:textfield id="cuit" name="cuit" class="number" maxlength="11"
							tooltip="Ingrese su cuit" />
					</div>
					<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
						<label for="impuesto">En caracter de:</label> <select
							class="form-control" id="caracter" name="caracter">
							<option value="1">Titular</option>
							<option value="2">Usufructuario</option>
							<option value="3">Poseedor a t�tulo de due�o</option>
							<option value="4">Tercero responsable</option>
						</select>
					</div>
				</div>
				<div class="row bottom-buffer">
					<div class="col-xs-9 col-sm-6 col-md-3 col-lg-3">
						<label for="impuesto">Impuesto:</label> <select
							class="form-control" id="impuesto" name="impuesto">
							<option value="1">Automotores</option>
							<option value="2">Motos</option>
							<option value="3">TSG</option>
							<option value="4">TISH - Seguridad e Higiene</option>
							<option value="5">TEEC - Tasa de Espacios Concesionados</option>
							<option value="6">FF - Ferias Francas</option>
			
						</select>
					</div>
				</div>
				<div class="row toggle" id="div-cuenta">
					<div class="col-xs-9 col-sm-6 col-md-3 col-lg-3">
						<label for="cuenta">Cuenta</label>
						<s:textfield id="cuenta" name="cuenta" class="number"
							maxlength="6" tooltip="Ingrese el n�mero de cuenta" />
					</div>
					<div class="col-xs-4 col-sm-3 col-md-3 col-lg-2">
						<label for="DV">D�gito Verificador</label>
						<s:textfield name="dv" id="dv" />
					</div>
				</div>
				<div class="row toggle" id="div-patente">
					<div class="col-xs-9 col-sm-6 col-md-3 col-lg-3">
						<label for="patente">Patente</label>
						<s:textfield id="patente" name="patente" maxlength="8"
							tooltip="Ingrese numero de patente" />
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<%-- <sj:submit cssClass="btn btn-primary" value="Consultar"
							formIds="findAccount" targets="container"
							onclick="validarForm();"></sj:submit> --%>

						<button type="button" class="btn btn-primary" data-dismiss="modal"
							onclick="validarForm();">Consultar</button>
					</div>
				</div>
			</fieldset>
		</s:form>
	</div>
</div>