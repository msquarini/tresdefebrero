<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<%
  response.setHeader("Cache-Control", "no-cache");
			response.setHeader("Pragma", "no-cache");
			response.setDateHeader("Expires", -1);
%>

<div class="row">
	<s:form action="moratoria_download" id="download" 
		namespace="/" enctype="multipart/form-data" theme="bootstrap">
	</s:form>

	<div class="col-md-12 alert alert-success">
		<p>Volante de pago generado con �xito</p>
	</div>
</div>

<SCRIPT>

	function downloadA() {
		$('#download').submit();
	}

	// volver a la pantalla de inicio
	function closeResponseDialog() {
		
 		$.ajax({
			type : "GET",
			url : '/moratoria/consulta_data.action',
			success : function(data) {
				$('#container').html(data);
			}
		}); 
	}
</SCRIPT>