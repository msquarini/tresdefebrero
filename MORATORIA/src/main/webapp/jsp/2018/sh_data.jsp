<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<script type="text/javascript">
	
</script>
<style type="text/css">
</style>

<s:form theme="bootstrap" cssClass="form-vertical">
	<fieldset disabled>
		<div class="row">
			<div class="col-xs-6 col-sm-3 col-md-2 col-lg-2">
				<label for="cuenta">Cuenta</label>
				<s:textfield id="cuenta" value="%{#session.cuenta.cuenta}" />
			</div>
			<div class="col-xs-6 col-sm-3 col-md-1 col-lg-1">
				<label for="DV">DV</label>
				<s:textfield id="DV" value="%{#session.cuenta.dv}" />
			</div>

			<div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
				<label for="titular">Raz�n Social</label>
				<s:textfield id="titular" class="form-control"
					value="%{#session.cuenta.razonSocial}" />
			</div>
			<div class="col-xs-6 col-sm-3 col-md-6 col-lg-6">
				<label for="rubro">Rubro</label>
				<s:textfield id="rubro" class="form-control"
					value="%{#session.cuenta.rubroDescripcion}" />
			</div>
		</div>
		<div class="row">
			<div class="form-group col-xs-6 col-sm-6 col-md-4 col-lg-5">
				<label for="calle">Calle</label>
				<s:textfield id="calle" class="form-control"
					value="%{#session.cuenta.calle}" />
			</div>
			<div class="form-group col-xs-2 col-sm-2 col-md-1 col-lg-1">
				<label for="numero">Nro</label>
				<s:textfield id="numero" class="form-control"
					value="%{#session.cuenta.numero}" />
			</div>
			<div class="form-group col-xs-2 col-sm-2 col-md-1 col-lg-1">
				<label for="CP">CP</label>
				<s:textfield id="CP" class="form-control"
					value="%{#session.cuenta.codigoPostal}" />
			</div>
			<div class="form-group col-xs-6 col-sm-6 col-md-5 col-lg-5">
				<label for="localidad">Localidad</label>
				<s:textfield id="loalidad" class="form-control"
					value="%{#session.cuenta.localidad}" />
			</div>
		</div>
		<%-- 			<div class="row">
				<div class="form-group col-xs-6 col-sm-6 col-md-6 col-lg-6">
					<label for="cuit">Nomenclatura</label>
					<s:textfield id="cuit" class="form-control"
						value="%{#session.cuenta.nomenclaturaLeyenda}" />
				</div>
			</div> --%>
	</fieldset>
</s:form>
