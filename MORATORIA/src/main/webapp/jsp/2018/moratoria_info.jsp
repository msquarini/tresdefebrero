<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<script type="text/javascript">
	function downloadA() {
		$('#downloadMoratoria').submit();
	}
</script>
<style type="text/css">
</style>

<!--    Informacion de ADHESION A MORATORIA  (mostrarse en dialogo!)-->

<s:include value="deuda.jsp"></s:include>
<s:set var="mora" value="#session.moratoria"></s:set>

<div class="row">
	<div class="col-xs-12 col-sm-4 col-md-2 col-lg-3">
		<div class="text-primary">F. Adhesi�n</div>
		<div>
			<span class="label label-info"><s:property
					value="getText('{0,date,dd/MM/yyyy HH:mm:ss}',{#mora.fechaAdhesion})" /></span>
		</div>
	</div>
	<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
		<div class="text-primary">Plan de pago</div>
		<div>
			<span class="label label-info"><s:property
					value="#mora.planPago.descripcion" /></span>
		</div>
	</div>
	<div class="col-xs-12 col-sm-4 col-md-2 col-lg-2">
		<div class="text-primary">$ Efectivo</div>
		<div id="montoefectivo">
			<span class="label label-info"><s:property
					value="getText('struts.money.format', {#mora.montoEfectivo})" /></span>
		</div>
	</div>
	<div class="col-xs-12 col-sm-4 col-md-1 col-lg-2">
		<div class="text-primary">$ cuota</div>
		<div id="montocuota">
			<span class="label label-info"><s:property
					value="getText('struts.money.format', {#mora.montoCuota})" /></span>
		</div>
	</div>
	<div class="col-xs-12 col-sm-4 col-md-1 col-lg-1">
		<div class="text-primary">Cuotas</div>
		<div>
			<span class="label label-info"><s:property
					value="#mora.cuotas" /></span>
		</div>
	</div>
</div>

<div class="row top-buffer">
	<div class="col-xs-6 col-sm-6 col-md-1 col-lg-1">
		<s:form action="moratoria_download" id="downloadMoratoria"
			namespace="/" enctype="multipart/form-data" theme="bootstrap">
			<s:hidden name="tipo" value=""></s:hidden>
		</s:form>

		<!--  <button type="button" class="btn btn-primary" data-dismiss="modal"
            onclick="downloadQ.submit();">Descargar</button>  -->
	</div>
</div>
