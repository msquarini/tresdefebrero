<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<link rel="stylesheet" href="../css/bootstrapValidator.css">
<script src="../js/bootstrapValidator.js"></script>


<script type="text/javascript">
	$(document).ready(function() {

		$('#seleccion_cuenta').ajaxForm({
			target : '#container',
			success : function(res, textStatus, jqXHR) {
			},
			error : function(res, textStatus, jqXHR) {
			}
		});
		$('#seleccion_cuenta').bootstrapValidator({
			excluded : [ ':disabled', ':hidden' ],
			feedbackIcons : {
				valid : 'glyphicon glyphicon-ok',
				invalid : 'glyphicon glyphicon-remove',
				validating : 'glyphicon glyphicon-refresh'
			},
			fields : {
				'cuentaSeleccionada' : {
					validators : {
						notEmpty : {
							message : 'Obligatorio'
						}
					}
				}
			}
		});
	})

	function continuar() {
		$('#seleccion_cuenta').submit();
	}
</script>

<style type="text/css">
</style>

<div class="row top-buffer">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<h2>Tasa de Seguridad e Higiene</h2>
		<h3>Seleccione la cuenta y presione continuar</h3>
	</div>
</div>
<div class="row top-buffer">
	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

		<s:form id="seleccion_cuenta" action="consulta_searchTish"
			cssClass="form-vertical" namespace="/" theme="bootstrap">
			<div class="row">
                <s:hidden name="cuit"></s:hidden>
                <s:hidden name="caracter"></s:hidden>
				<div class="col-xs-9 col-sm-6 col-md-6 col-lg-6">
					<label for="planpago-sel">Cuentas empadronadas</label>
					<%-- <s:select id="planpago-sel" theme="bootstrap" list="#session.planes_deudasPre" --%>
					<s:select id="cuenta-sel" theme="bootstrap" list="cuentas"
						listValue="cuenta" listKey="cuenta" name="cuenta">
					</s:select>
				</div>
			</div>
		</s:form>
		<div class="row">
			<div class="col-xs-9 col-sm-6 col-md-3 col-lg-3">
				<button type="button" class="btn btn-primary" data-dismiss="modal"
					onclick="continuar();">Continuar</button>
			</div>
		</div>
	</div>
</div>