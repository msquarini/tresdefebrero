<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%-- <%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%> --%>

<script type="text/javascript">
	function salir() {
		//loader(true, null);

		$.ajax({
			method : "POST",
			url : "/moratoria/consulta_input.action",
			traditional : true,
			success : function(res) {
				//loader(false, null);
				$('#container').html(res);
			},
			error : function(res, textStatus, jqXHR) {
				//loader(false, null);
				handleErrorMessages(res.responseJSON);
				showErrorModalDialog();
			}
		});
	}
</script>

<style type="text/css">
</style>

<s:url var="findAccount" action="consulta_input" namespace="/" />

<div class="row top-buffer">
	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
		<h3>Plan de regularización de deudas</h3>
	</div>
	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
		<h3 class="pull-right"><small>Cuenta</small> 
			<span class="label label-primary"><s:property value="#session.cuenta.cuenta"/></span>
			<span>    </span>
			<span class="label label-primary"><s:property value="impuestoLabel"/></span>
		</h3>
		
	</div>
</div>
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<ul class="nav nav-pills"
			style="background-color: #eeeeee; border-bottom-style: ridge;">
			<li class="nav-header disabled"><a><strong>Moratoria
						2018</strong></a></li>

			<li class="active"><a data-toggle="tab" href="#datos">Información
					de la cuenta</a></li>
			<li><a data-toggle="tab" href="#tabdeudas" onclick="loadPre();">Deuda
					Prejudicial</a></li>
			<li><a data-toggle="tab" href="#tabdeudas" onclick="loadJud();">Deuda
					Judicializada</a></li>
			<li><a data-toggle="tab" href="#tabdeudas" onclick="loadFis();">Deuda
					Fiscalizada</a></li>
			<li class="pull-right" style="padding-top: 3px; padding-right: 3px">
				<button id="btn_salir" class="btn btn-warning" type="button"
					onclick="salir()">
					<span class="fa fa-window-close"></span>&nbsp;&nbsp;
					<s:text name="salir" />
				</button>
			</li>
		</ul>

		<div class="tab-content">
			<div id="datos" class="tab-pane fade in active">
				<s:include value="cuenta-data.jsp"></s:include>
			</div>
			<div id="tabdeudas" class="tab-pane">
				<%-- <s:include value="prejudicial_info.jsp"></s:include> --%>
			</div>
		</div>
	</div>
</div>




<!-- Modal Medium (para respuestas)-->
<div class="modal fade" id="generic-dialog-response" tabindex="-1"
	role="dialog" aria-labelledby="generic-data-label">
	<div class="modal-dialog modal-md" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="generic-dialog-response-label"></h4>
			</div>
			<div class="modal-body" id="generic-modal-response-body">
				<p></p>
			</div>
			<div class="modal-footer">
				<button id="btn-print" type="button" class="btn btn-default"
					data-dismiss="modal" onclick="downloadA();">
					<i class="fa fa-download" aria-hidden="true"></i> Descargar PDF
				</button>

				<button type="button" class="btn btn-default" data-dismiss="modal"
					onclick="cerrar('#generic-dialog-response');">Cerrar</button>
			</div>
		</div>
	</div>
</div>

<!-- Modal Medium (para ERRORES)-->
<div id="error-dialog" class="modal fade" tabindex="-1" role="dialog"
	aria-labelledby="generic-data-label">
	<div class="modal-dialog modal-md" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="error-title">Error en la operacion
					realizada.</h4>
			</div>
			<div id="error-dialog-body" class="modal-body">
				<div id="errors-mensaje" class="alert alert-info"
					style="display: none;"></div>
				<div id="errors-data" class="alert alert-danger"
					style="display: none;"></div>
				<div id="warning-data" class="alert alert-warning"
					style="display: none;"></div>
				<div id="info-data" class="alert alert-info" style="display: none;"></div>
				<div id="success-data" class="alert alert-success"
					style="display: none;"></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"
					onclick="cerrar('#error-dialog');">Cerrar</button>
			</div>
		</div>
	</div>
</div>



<script type="text/javascript">
	function handleErrorMessages(jsonResp) {
		$("#errors-data").empty();
		$("#warning-data").empty();
		$("#errors-mensaje").empty();
		if (jsonResp.tituloError) {
			$("#error-title").text(jsonResp.tituloError);
		}
		;
		if (jsonResp.mensajeError) {
			$("#errors-mensaje").html("<p>" + jsonResp.mensajeError + "</p>");
			$("#errors-mensaje").show();
		}
		;
		$.each(jsonResp.actionErrors, function(i, error) {
			if (i == 0) {
				$("#errors-data").show();
			}
			$(".alert-danger").append("<p>" + error + "</p>");
		});
		$.each(jsonResp.actionMessage, function(i, error) {
			if (i == 0) {
				$("#warning-data").show();
			}
			$(".alert-warning").append("<p>" + error + "</p>");
		});
	}

	function showResponseModalDialog(res) {
		$("#generic-dialog-response-label").text("Resultado de la operacion");
		$('#generic-modal-response-body').html(res);
		$('#generic-dialog-response').modal('show');
	}

	function showErrorModalDialog() {
		$('#error-dialog').modal('show');
	}

	function cerrar(dialogo) {
		//$(dialogo).modal('hide');		
	}

	$('#generic-dialog-response').on('hidden.bs.modal', function() {
		closeResponseDialog();
	})
	$('#error-dialog').on('hidden.bs.modal', function() {
		closeErrorDialog();
	})

	function loadPre() {
		$.ajax({
			method : "POST",
			url : "/moratoria/moratoria_goToPrejudicial.action",
			data : {
					tipoMoratoria : 'P'
			},
			traditional : true,
			success : function(res) {
				$('#tabdeudas').html(res);
				
			},
			error : function(res, textStatus, jqXHR) {
				//loader(false, null);
				handleErrorMessages(res.responseJSON);
				showErrorModalDialog();
			}
		});
	}

	function loadJud() {
		$.ajax({
			method : "POST",
			url : "/moratoria/moratoria_goToJudicial.action",
			data : {
				tipoMoratoria : 'J'
			},
			traditional : true,
			success : function(res) {
				$('#tabdeudas').html(res);
			},
			error : function(res, textStatus, jqXHR) {
				//loader(false, null);
				handleErrorMessages(res.responseJSON);
				showErrorModalDialog();
			}
		});
	}

	function loadFis() {
		$.ajax({
			method : "POST",
			url : "/moratoria/moratoria_goToFiscalizada.action",
			data : {
				tipoMoratoria : 'F'
			},
			traditional : true,
			success : function(res) {
				$('#tabdeudas').html(res);
			},
			error : function(res, textStatus, jqXHR) {
				//loader(false, null);
				handleErrorMessages(res.responseJSON);
				showErrorModalDialog();
			}
		});
	}

	
</script>