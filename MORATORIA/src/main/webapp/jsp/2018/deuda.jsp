<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<%-- <script type="text/javascript" src="/moratoria/struts/js/base/jquery-2.2.4.min.js"></script>
<script type="text/javascript" src="/moratoria/struts/js/base/jquery-ui.min.js?s2j=4.0.1"></script>
<script type="text/javascript" src="/moratoria/struts/bootstrap/js/bootstrap.js?s2b=2.5.0"></script>
<link id="bootstrap_styles" rel="stylesheet" href="/moratoria/struts/bootstrap/css/bootstrap.css?s2b=2.5.0" type="text/css">
 --%>
<script type="text/javascript">
</script>

<style type="text/css">
.panel-footer.panel-custom {
	background: red;
	color: white;
}

.mano {
	cursor: pointer;
}
</style>

<div class="panel-group" id="deuda-panel">
	<div class="panel panel-default">
        <div class="panel-heading mano" data-toggle="collapse"
            data-parent="#deuda-panel" href="#detalleDeuda">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#deuda-panel"
                    href="#detalleDeuda">Per�odos <span class="badge"><s:property
                            value="deudas.size()" /></span></a>
            </h4>
        </div>
        <div id="detalleDeuda" class="panel-collapse collapse in">
			<div class="panel-body">
				<table class="table table-hover">
					<thead>
						<tr>
							<th>A�o</th>
							<th>Cuota</th>
							<!-- <th class="text-right">Cr�ditos</th> -->
							<th class="text-right">Monto Adeudado</th>
						</tr>
					</thead>
					<tbody>
						<s:iterator value="deudas">
							<tr>
								<td><s:property value="anio" /></td>
								<td><s:property value="cuota" /></td>
								<%-- <td align="right"><s:property value="getText('struts.money.format', {montoCredito})" /></td> --%>
								<td align="right"><s:property value="getText('struts.money.format', {montoDeuda})" /></td>
							</tr>
						</s:iterator>
					</tbody>
				</table>
			</div>
		</div>
		<div class="panel-footer" align="right">
			Total $
			<s:property value="getText('struts.money.format', {totalDeuda})" />
		</div>
	</div>
</div>