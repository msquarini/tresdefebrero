<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%-- <%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%> --%>

<script type="text/javascript">
function detalleMoratoria(id) {
	//loader(true, null);
//debugger;
	$.ajax({
		method : "POST",
		url : "/moratoria/moratoria_goToMoratoria.action",
		data : {
			moratoriaId : id
		},
		traditional : true,
		success : function(res) {
			//loader(false, null);
			$("#generic-dialog-response-label").text(
					"Detalle de moratoria");
			$("#generic-modal-response-body").html(res);
			$('#generic-dialog-response').modal('show');
		},
		error : function(res, textStatus, jqXHR) {
			//loader(false, null);			
			handleErrorMessages(res.responseJSON);
			showErrorModalDialog();
		}
	});

};
</script>
<style type="text/css">
</style>


<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

		<s:set var="imp" value="%{#session.cuenta.impuesto}"></s:set>

		<!--    Informacion de Impuesto (se toman datos de session) -->
		<s:if test="%{#imp == \"XP\"}">
			<s:include value="auto_data.jsp"></s:include>
		</s:if>
		<s:elseif test="%{#imp == \"PV\"}">
			<s:include value="moto_data.jsp"></s:include>
		</s:elseif>
		<s:elseif test="%{#imp == \"AL\"}">
			<s:include value="sg_data.jsp"></s:include>
		</s:elseif>
		<s:elseif test="%{#imp == \"IC\"}">
			<s:include value="sh_data.jsp"></s:include>
		</s:elseif>
		<s:elseif test="%{#imp == \"RC\"}">
			<s:include value="ec_data.jsp"></s:include>
		</s:elseif>
		<s:elseif test="%{#imp == \"FF\"}">
			<s:include value="ff_data.jsp"></s:include>
		</s:elseif>
	
	</div>
</div>

<div class="row">
	<s:if test="%{#session.presentaDeudaPre}">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="alert alert-danger" role="alert">
				<strong>Deuda prejudicial vigente al 2017 </strong>
				<s:if test="%{#imp != 'IC'}">
					<strong class="pull-right"> Total $ <s:property
							value="getText('struts.money.format', {totalDeudaPrejudicial})" />
					</strong>
				</s:if>
			</div>
		</div>
	</s:if>
</div>
<div class="row">
	<s:if test="%{#session.presentaDeudaJud}">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="alert alert-danger" role="alert">
				<strong>Deuda Judicializada vigente al 2017</strong>
				<s:if test="%{#imp != 'IC'}">
					<strong class="pull-right"> Total $ <s:property
							value="getText('struts.money.format', {totalDeudaJudicial})" />
					</strong>
				</s:if>
			</div>
		</div>
	</s:if>
</div>
<div class="row">
	<s:if test="%{#session.presentaDeudaFis}">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="alert alert-danger" role="alert">
				<strong>Deuda Fiscalizada vigente al 2017</strong>
				<s:if test="%{#imp != 'IC'}">
					<strong class="pull-right"> Total $ <s:property
							value="getText('struts.money.format', {totalDeudaFiscalizada})" />
					</strong>
				</s:if>
			</div>
		</div>
	</s:if>
</div>
<div class="row">
	<s:if
		test="%{!#session.presentaDeudaPre && !#session.presentaDeudaJud && !#session.presentaDeudaFis}">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="alert alert-info" role="alert">
				<p class="text-center">
					<strong>No presenta deuda</strong>
				</p>
			</div>
		</div>
	</s:if>
</div>


<s:iterator value="moratorias">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="alert alert-info" role="alert">
				<strong><s:property value="planPago.moratoriaNombre" /> -
					<s:property value="codMoratoria" /> - <s:property
						value="planPago.tipo" /></strong>
				<%-- $
				<s:property value="getText('struts.money.format', {deudaTotal})" /> --%>
				&nbsp;&nbsp;&nbsp; <small>Fecha Adhesi�n</small>&nbsp;<strong><s:property
						value="getText('{0,date,dd/MM/yyyy HH:mm:ss}',{fechaAdhesion})" /></strong>

				<button type="button" class="btn btn-primary pull-right"
					data-dismiss="modal" style="padding-top: 1px; padding-bottom: 1px"
					onclick='detalleMoratoria(<s:property value="idMoratoria" />)'>Detalle</button>
			</div>
		</div>
	</div>
</s:iterator>
