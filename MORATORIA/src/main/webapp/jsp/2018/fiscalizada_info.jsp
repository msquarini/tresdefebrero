<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<script type="text/javascript">
	
</script>
<style type="text/css">
</style>

<s:set var="imp" value="%{#session.cuenta.impuesto}"></s:set>

<!--    Informacion de DEUDA  (se toman datos de session) -->
<s:include value="deuda.jsp"></s:include>

<s:if test="%{#session.presentaDeudaFis && !existsAdhesionMoraVigente}">
	<s:include value="pago.jsp">
		<s:param name="tipo" value="'F'"></s:param>
	</s:include>
</s:if>
<s:elseif test="existsAdhesionMoraVigente">
	<s:include value="presenta_moratoria.jsp">
		<s:param name="tipo" value="'F'"></s:param>
	</s:include>
</s:elseif>

<s:else>
	<div class="row top-buffer">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 alert alert-info">
				<h3 align="center">
					<strong>No presenta deuda</strong>
				</h3>
			</div>
		</div>
	</div>
</s:else>
