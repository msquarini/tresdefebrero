<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<script type="text/javascript">
	
</script>
<style type="text/css">
</style>

<div class="container" id="moratoria-container">

	<s:set var="imp" value="%{#session.cuenta.impuesto}"></s:set>

	<!-- 	Informacion de Impuesto (se toman datos de session) -->
	<s:if test="%{#imp == \"XP\"}">
		<s:include value="auto_data.jsp"></s:include>
	</s:if>
	<s:elseif test="%{#imp == \"PV\"}">
		<s:include value="moto_data.jsp"></s:include>
	</s:elseif>
	<s:elseif test="%{#imp == \"AL\"}">
		<s:include value="sg_data.jsp"></s:include>
	</s:elseif>
	<s:elseif test="%{#imp == \"IC\"}">
		<s:include value="sh_data.jsp"></s:include>
	</s:elseif>

	<!-- 	Informacion de DEUDA  (se toman datos de session) -->
	<s:if test="%{#imp == \"IC\"}">
		<s:include value="deuda_tish.jsp"></s:include>
	</s:if>
	<s:else>
		<s:include value="deuda.jsp"></s:include>
	</s:else>

	<!-- Chequeo si se trata de moratoria existente -->
	<s:set var="me" value="moratoriaExistente"></s:set>

	<s:if test="#me == 0">
		<!-- 	Informacion de Pago  (se toman datos de session) -->
		<s:if test="%{#imp != \"IC\"}">
			<s:include value="pago.jsp"></s:include>
		</s:if>
	</s:if>
	<s:if test="#me == 1">
		<s:include value="presenta_moratoria.jsp"></s:include>
	</s:if>
	<s:else>
		<s:include value="volver.jsp"></s:include>
	</s:else>
</div>
