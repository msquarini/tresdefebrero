<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<style type="text/css">
.top-simulacion {
	margin-top: 15px;
}
</style>

<div class="row">
	<form class="form-vertical">

		<div class="col-xs-9 col-sm-6 col-md-6 col-lg-6">
			<label for="montoEfectivo">Monto en efectivo</label>
			<div class="input-group">
				<div class="input-group-addon">$</div>
				<input readonly="readonly" type="text" class="form-control"
					id="montoEfectivo" name="montoEfectivo"
					value="<s:property value='getText("struts.money.format", {montoMoratoria.efectivo})' />"
					placeholder="Seleccione plan de pagos y cuotas">
			</div>
		</div>
		<div class="col-xs-9 col-sm-6 col-md-6 col-lg-6">
			<label for="montoCuota"><s:property value='cuotas' /> cuotas
				de </label>
			<div class="input-group">
				<div class="input-group-addon">$</div>
				<input readonly="readonly" type="text" class="form-control"
					id="montoCuota" name="montoCuota"
					value="<s:property value='getText("struts.money.format", {montoMoratoria.montoCuota})' />"
					placeholder="Seleccione plan de pagos y cuotas">

			</div>
		</div>

	</form>
</div>
<%-- <div class="row top-simulacion">
	<div
		class="col-xs-9 col-sm-6 col-md-offset-10 col-md-2 col-lg-offset-10 col-lg-2">
		<s:url var="findAccount" action="moratoria_input" namespace="/" />
		<sj:a theme="bootstrap" cssClass="btn btn-primary" targets="container"
			href="%{findAccount}">Volver</sj:a>

	</div>
</div> --%>
