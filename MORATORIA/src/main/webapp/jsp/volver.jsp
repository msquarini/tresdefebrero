<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<script type="text/javascript">
	
</script>

<style type="text/css">
#number_container {
	border: 1px dotted #d0d0d0;
	padding: 15px;
	margin: 10px;
	display: none;
	background: #fafafa;
}

body {
	padding-top: 60px;
	/* 60px to make the container go all the way to the bottom of the topbar */
}

.top-buffer {
	margin-top: 30px;
}
</style>


<div class="row top-buffer">
	<div
		class="col-xs-12 col-sm-12 col-md-offset-11 col-md-1 col-lg-offset-11 col-lg-1">
		<s:url var="findAccount" action="moratoria_input" namespace="/" />
		<sj:a theme="bootstrap" cssClass="btn btn-primary" targets="container"
			href="%{findAccount}">Inicio</sj:a>
	</div>
</div>