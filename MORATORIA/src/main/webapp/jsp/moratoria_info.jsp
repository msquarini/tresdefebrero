<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<script type="text/javascript">
	
</script>
<style type="text/css">
</style>

<!-- Informacion de la cuenta -->
<div class="sg-container" id="moratoria-info-container">

	<s:set var="imp" value="impuesto"></s:set>
	<!-- 	Informacion de Impuesto (se toman datos de session) -->
	<s:if test="%{#imp == 1}">
		<s:include value="auto_data.jsp"></s:include>
	</s:if>
	<s:elseif test="%{#imp == 2}">
		<s:include value="moto_data.jsp"></s:include>
	</s:elseif>
	<s:elseif test="%{#imp == 3}">
		<s:include value="sg_data.jsp"></s:include>
	</s:elseif>
	<s:elseif test="%{#imp == 4}">
		<s:include value="sh_data.jsp"></s:include>
	</s:elseif>

	<%-- 	<!-- 	Informacion de DEUDA  (se toman datos de session) -->
	<s:include value="deuda.jsp"></s:include>
 --%>

</div>

<s:if test="existsDeudaMoratoria">
	<div class="row alert alert-danger">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<h3>Ya ten�s un plan de pago para los periodos adeudados desde
				2011 a 2015.</h3>
			<h4>Acercate a la Direcci�n de Recaudaci�n de la Municipalidad
				para rehabilitarlo con las condiciones acordadas.</h4>

		</div>
	</div>
</s:if>


<%-- <s:if test="%{getMoratorias().isEmpty()}"> --%>
<s:if test="%{!existsAdhesionMoraVigente && !getDeudas().isEmpty()}">

	<div class="row alert alert-danger">
		<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
			<h4>
				<strong>Deuda vigente al 2016 </strong>
			</h4>
		</div>
		<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
			<s:if test="%{#imp != 4}">

				<h4>
					Total $
					<s:property value="getText('struts.money.format', {totalDeuda})" />
				</h4>

			</s:if>
		</div>
		<%-- <s:if test="existsDeuda"> --%>
		<div
			class="col-xs-6 col-sm-6 col-md-offset-5 col-md-1 col-lg-offset-5 col-lg-1">
			<s:url var="download" action="moratoria_goToAdhesion" namespace="/" />
			<sj:a theme="bootstrap" cssClass="btn btn-primary"
				targets="container" href="%{download}">Adherir</sj:a>
		</div>
		<%-- </s:if> --%>
	</div>

</s:if>
<s:else>
	<div class="row alert alert-info">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<h3 align="center">
				<strong>No presenta deuda</strong>
			</h3>
		</div>
	</div>
</s:else>

<s:iterator value="moratorias">

	<div class="row alert alert-info">
		<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
			<h4>
				<small><strong><s:property
							value="planPago.moratoriaNombre" /> <s:property
                            value="codMoratoria" /></strong></small> $
				<s:property value="getText('struts.money.format', {deudaTotal})" />
			</h4>
		</div>
		<div
			class="col-xs-6 col-sm-6 col-md-offset-5 col-md-1 col-lg-offset-5 col-lg-1">
			<s:url var="data" action="moratoria_goToMoratoria" namespace="/">
				<s:param name="moratoriaId">
					<s:property value="idMoratoria" />
				</s:param>
			</s:url>
			<sj:a theme="bootstrap" cssClass="btn btn-primary"
				targets="container" href="%{data}">Detalle</sj:a>
		</div>
	</div>

</s:iterator>

<s:include value="volver.jsp"></s:include>


