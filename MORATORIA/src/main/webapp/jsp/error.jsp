<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<script type="text/javascript">
	
</script>
<style type="text/css">
</style>

<div class="sg-container">
    <p align="center" class="bg-danger">Se produjo un error en la
        aplicación.</p>


    <p align="center">
        <s:url var="inicio" action="moratoria_input" namespace="/" />
        <sj:a theme="bootstrap" cssClass="btn btn-primary" targets="container"
            href="%{inicio}">Inicio</sj:a>
    </p>
</div>

