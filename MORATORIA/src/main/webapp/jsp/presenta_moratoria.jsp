<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<script type="text/javascript">
	
</script>

<style type="text/css">
#number_container {
	border: 1px dotted #d0d0d0;
	padding: 15px;
	margin: 10px;
	display: none;
	background: #fafafa;
}

body {
	padding-top: 60px;
	/* 60px to make the container go all the way to the bottom of the topbar */
}

.top-buffer {
	margin-top: 30px;
}
</style>

<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<p class="bg-primary">La cuenta informada presenta adhesi�n a
			moratoria.</p>
	</div>
</div>
<div class="row">
	<div class="col-xs-12 col-sm-4 col-md-2 col-lg-2">
		<div class="text-primary">Fecha adhesi�n</div>
		<div>
			<span class="label label-info"><s:property
					value="moratoria.fechaAdhesion" /></span>
		</div>
	</div>
	<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
		<div class="text-primary">Plan de pago</div>
		<div>
			<span class="label label-info"><s:property
					value="moratoria.planPago.descripcion" /></span>
		</div>
	</div>
	<div class="col-xs-12 col-sm-4 col-md-3 col-lg-2">
		<div class="text-primary">Efectivo</div>
		<div id="montoefectivo">
			<span class="label label-info"><s:property
					value="getText('struts.money.format', {moratoria.montoEfectivo})" /></span>
		</div>
	</div>
	<div class="col-xs-12 col-sm-4 col-md-2 col-lg-2">
		<div class="text-primary">Monto cuota</div>
		<div id="montocuota">
			<span class="label label-info"><s:property
					value="getText('struts.money.format', {moratoria.montoCuota})" /></span>
		</div>
	</div>
	<div class="col-xs-12 col-sm-4 col-md-2 col-lg-2">
		<div class="text-primary">Cuotas</div>
		<div>
			<span class="label label-info"><s:property
					value="moratoria.cuotas" /></span>
		</div>
	</div>

</div>
<div class="row top-buffer">
	<div class="col-xs-6 col-sm-6 col-md-1 col-lg-1">
		<s:url var="download" action="moratoria_predownload" namespace="/" />
		<sj:a theme="bootstrap" cssClass="btn btn-primary" targets="container"
			href="%{download}">Imprimir</sj:a>
	</div>
	<div
		class="col-xs-6 col-sm-6 col-md-offset-10 col-md-1 col-lg-offset-10 col-lg-1">
		<s:url var="findAccount" action="moratoria_input" namespace="/" />
		<sj:a theme="bootstrap" cssClass="btn btn-primary" targets="container"
			href="%{findAccount}">Inicio</sj:a>
	</div>
</div>