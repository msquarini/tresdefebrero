<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<%
  response.setHeader("Cache-Control", "no-cache");
  response.setHeader("Pragma", "no-cache");
  response.setDateHeader("Expires", -1);
%>

<div class="row">
	<s:form action="moratoria_download" id="download" namespace="/"
		enctype="multipart/form-data" theme="bootstrap">
	</s:form>

	<div class="col-md-9">
		<h1>Plan de regularización de deudas</h1>
		<h3>No presente Volante de Pago (VEP) con deuda vigente.</h3>
	</div>
	<div class="col-md-9">
		<s:url var="findAccount" action="moratoria_input" namespace="/" />
		<sj:a theme="bootstrap" cssClass="btn btn-primary" targets="container"
			href="%{findAccount}">Inicio</sj:a>
	</div>
</div>
