<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>
<html lang="es">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="keywords" content="Tres de febrero, Seguridad, higiene" />
<meta name="description"
	content="Municipalidad de Tres de Febrero - Plan de regularización de deudas" />
<title>Municipalidad de Tres de Febrero</title>

<!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
<!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

<%-- <script src="//code.jquery.com/jquery-1.11.0.min.js"></script> --%>
<sj:head locale="es" compressed="false" loadAtOnce="true" jqueryui="true"
	loadFromGoogle="false" />
<sb:head includeScripts="true" includeScriptsValidation="true"
	compressed="true" includeStyles="true" />


    
<script src="../js/moratoria.js"></script>

<!-- Link a Font Awesome -->
<link rel="stylesheet" href="../css/fa/css/font-awesome.min.css">


<style type="text/css">
#loader {
    position: absolute;
    left: 50%;
    top: 50%;
    z-index: 1;
    width: 150px;
    height: 150px;
    margin: -75px 0 0 -75px;
    border: 16px solid #f3f3f3;
    border-radius: 50%;
    border-top: 16px solid #3498db;
    width: 120px;
    height: 120px;
    -webkit-animation: spin 2s linear infinite;
    animation: spin 2s linear infinite;
}

#number_container {
	border: 1px dotted #d0d0d0;
	padding: 15px;
	margin: 10px;
	display: none;
	background: #fafafa;
}

body {
	padding-top: 70px;
	/* 60px to make the container go all the way to the bottom of the topbar */
}

.navbar-brand,
.navbar-nav li a {
    line-height: 80px;
    height: 80px;
    padding-top: 0;
}

.top-buffer {
	margin-top: 10px;
}

.footer {
    position: relative;
    bottom: 0;
    width: 100%;
    /* Set the fixed height of the footer here */
    height: 40px;
    background-color: #f5f5f5;
}

.footer>.container {
    padding-right: 15px;
    padding-left: 15px;
}

</style>
</head>

<body>
	<nav class="navbar navbar-default navbar-fixed-top" >
	<div class="container-fluid">
		  <div class="navbar-header">
			<a class="navbar-brand" href="http://www.tresdefebrero.gov.ar"> <span><img
				src="../img/logo-new.png" alt="" height="75" width="180"></span>&nbsp;&nbsp;Moratoria WEB
			</a>
		</div>
		</div>
	</nav>

    <!-- Indicador de loading... -->
    <div id="loader" style="display: none;"></div>

	<div class="container" id="container">
		<%-- <s:include value="moratoria_input.jsp"></s:include> --%>
	</div>

<%-- 	<footer class="navbar-default top-buffer">

		<p align="center">
			© Copyright 2015 - 2016 - <strong>MUNICIPALIDAD DE TRES DE
				FEBRERO</strong> - Conmutador: 4734-2400 - Todos los derechos reservados
		</p>
	</footer> --%>
    <footer class="footer top-buffer">
        <div class="container">
            <p align="center">
                © Copyright 2017 - 2018 - <strong>MUNICIPALIDAD DE TRES DE
                    FEBRERO</strong> - Conmutador: 4734-2400 - Todos los derechos reservados
            </p>
        </div>
    </footer>

</body>
</html>
<script type="text/javascript">

var getUrlParameter = function getUrlParameter(sParam) {
    //var sPageURL = decodeURIComponent(window.location.search.substring(1)),
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

$.ajax({

    type: "GET",
    //url: '/moratoria/moratoria_input.action',
    url: '/moratoria/consulta_input.action',
    data: {'mail' : getUrlParameter('origen')},
    //url: '/moratoria/deuda_input.action',
    //url: '/moratoria/jsp/sg_data.jsp',
    success: function(data) {
          // data is ur summary
         $('#container').html(data);
    }
});    
</script>