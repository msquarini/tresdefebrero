<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<script type="text/javascript">
	
</script>
<style type="text/css">
</style>

<div class="container" id="moratoria-container">

	<s:set var="imp" value="impuesto"></s:set>
	<!-- 	Informacion de Impuesto (se toman datos de session) -->
	<s:if test="%{#imp == 1}">
		<s:include value="auto_data.jsp"></s:include>
	</s:if>
	<s:elseif test="%{#imp == 2}">
		<s:include value="moto_data.jsp"></s:include>
	</s:elseif>
	<s:elseif test="%{#imp == 3}">
		<s:include value="sg_data.jsp"></s:include>
	</s:elseif>
	<s:elseif test="%{#imp == 4}">
		<s:include value="tish_data.jsp"></s:include>
	</s:elseif>

	<%-- 	<!-- 	Informacion de DEUDA  (se toman datos de session) -->
	<s:include value="deuda.jsp"></s:include>
 --%>

	<!-- 	VALIDA si tiene moratoria vigente -->
	<!-- 20170101 FIN DE MORATORIA, no se presentan mas adhesiones!!!!!!!!!! -->
	<s:set var="me" value="moratoriaExistente"></s:set>

	<%-- <s:if test="#me == 0">
		<!-- 	Informacion de Pago  (se toman datos de session) -->
		<s:include value="pago.jsp"></s:include>
	</s:if> --%>
	<s:if test="#me == 1">
		<!-- 	Informacion de DEUDA  (se toman datos de session) -->
		<s:include value="deuda.jsp"></s:include>
		<s:include value="presenta_moratoria.jsp"></s:include>
	</s:if>
	<s:else>
		<s:include value="volver.jsp"></s:include>
	</s:else>
</div>
