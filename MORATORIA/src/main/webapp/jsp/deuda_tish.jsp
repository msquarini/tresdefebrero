<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<script type="text/javascript" src="../js/jquery.number.js"></script>
<link rel="stylesheet" href="../css/bootstrapValidator.css">
<script src="../js/bootstrapValidator.js"></script>

<script type="text/javascript">
	$('input.numberdecimal').number(true, 2, ',', '');

 	$(document).keypress(
	function(event) {
		if (event.which == '13') {
			event.preventDefault();
		}
	}); 

	/* Configuracion del formulario, ya que se hace submit del mismo mediante javascript */
	$('#tishData').ajaxForm({
		target : '#container',
		success : function(res, textStatus, jqXHR) {
			debugger;
		},
		error : function(res, textStatus, jqXHR) {
			debugger;
		}
	});

	$(document)
			.ready(
					function() {
						$('#tishData')
								.bootstrapValidator(
										{
											feedbackIcons : {
												valid : 'glyphicon glyphicon-ok',
												invalid : 'glyphicon glyphicon-remove',
												validating : 'glyphicon glyphicon-refresh'
											},
											fields : {
												'importes' : {
													validators : {
														callback : {
															message : 'Monto menor al m�nimo',
															callback : function(
																	value,
																	validator,
																	$field) {

																var id_name = $field
																		.attr('id');
																var pos = id_name
																		.substr(
																				id_name
																						.indexOf("_") + 1,
																				id_name.length);
																var minimo_field = $('#minimo_'
																		+ pos);
																var minimo = parseInt(minimo_field
																		.val());
																if (minimo > value) {
																	return false
																}
																return true;
															}
														}
													}
												}
											}
										})
					})

	function changePlanPago() {
		var $el = $("#planpago-sel");
		var $plan = $("#plan_" + $el.val());

		var cuotas = $plan.val();
		var cuota_desde = Number(cuotas.substr(0, cuotas.indexOf("_")));
		var cuota_hasta = Number(cuotas.substr(cuotas.indexOf("_") + 1,
				cuotas.length));

		var $cuotasel = $("#cuotas-sel");
		$cuotasel.empty(); // remove old options
		if (cuota_desde != 0) {
			for (; cuota_desde <= cuota_hasta; cuota_desde++) {
				$cuotasel.append($("<option></option>").attr("value",
						cuota_desde).text(cuota_desde));

			}
			$cuotasel.val(cuota_hasta);
		}
		verMontos();
	}

	function verMontos() {
		//var data = $("#tishData, #planpago").serialize();
		var data = $("#tishData").serialize();
		$.ajax({
			type : "POST",
			url : '/moratoria/moratoria_simularMoratoriaTish.action',
			data : data,
			success : function(data) {
				// data is ur summary
				$('#div-total').html(data);
			}
		});
	}

	changePlanPago();
</script>

<style type="text/css">
.panel-footer.panel-custom {
	background: red;
	color: white;
}

.mano {
	cursor: pointer;
}
/* 
.table-fixed thead {
	width: 97%;
}

.table-fixed tbody {
	height: 330px;
	overflow-y: auto;
	width: 100%;
}

.table-fixed thead, .table-fixed tbody, .table-fixed tr, .table-fixed td,
	.table-fixed th {
	display: block;
}
 */
.table-fixed tbody td, .table-fixed thead>tr>th {
	float: left;
	border-bottom-width: 0;
}

div.form-group {
	margin-bottom: 35px
}

div.has-error {
	height: 0px
}
</style>
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<s:actionerror theme="bootstrap" />
	</div>
</div>

<s:form id="tishData" action="moratoria_montosTish"
	enctype="multipart/form-data" method="post" namespace="/"
	theme="bootstrap" cssClass="form-horizontal">
	<fieldset>
		<div class="panel-group" id="deuda-panel">
			<div class="panel panel-default">
				<div class="panel-heading mano" data-toggle="collapse"
					data-parent="#deuda-panel" href="#detalleDeuda">
					<h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#deuda-panel"
							href="#detalleDeuda">Periodos <span class="badge"><s:property
									value="deudas.size()" /></span></a>
					</h4>
				</div>

				<!-- <div id="detalleDeuda" class="panel-collapse collapse in">
					<div class="panel-body"> -->

				<div style="overflow: auto; height: 300px;">
					<table class="table table-hover">
						<thead>
							<tr>
								<th class="col-sm-4 col-md-2 col-lg-2">A�o</th>
								<th class="col-sm-4 col-md-2 col-lg-2">Cuota</th>
								<th class="col-sm-4 col-md-2 col-lg-2">Presenta Pago</th>
								<th class="col-sm-4 col-md-2 col-lg-5">Monto a pagar</th>
							</tr>
						</thead>
						<tbody>
							<s:iterator value="deudas" status="row">
								<s:if test="presentaPago">
									<tr>
								</s:if>
								<s:else>
									<tr class="warning">
								</s:else>
								<td class="col-sm-4 col-md-2 col-lg-2"><s:property
										value="anio" /></td>
								<td class="col-sm-4 col-md-2 col-lg-2"><s:property
										value="cuota" /></td>
								<td class="col-sm-4 col-md-2 col-lg-2"><s:if
										test="presentaPago">Si
										<s:hidden id="minimo_%{#row.index}" name="minimo" value="-1"></s:hidden>
									</s:if> <s:else>No
										<s:hidden id="minimo_%{#row.index}" name="minimo"
											value="%{minimo}"></s:hidden>
									</s:else></td>
								<td class="col-sm-4 col-md-2 col-lg-5"><s:textfield
										id="imp_%{#row.index}" name="importes" class="numberdecimal"
										maxlength="10" value="%{importes[#row.index]}"
										onchange="verMontos();">
									</s:textfield> <s:hidden id="keyDeuda" name="keyDeuda" value="%{key}"></s:hidden></td>
								</tr>
							</s:iterator>

						</tbody>
					</table>

				</div>

			</div>
		</div>

		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

				<div class="row">
					<%-- <s:form id="planpago" action="moratoria_simularMoratoriaTish"
						cssClass="form-vertical" namespace="/" theme="bootstrap"> --%>
					<div class="col-xs-9 col-sm-6 col-md-6 col-lg-6">
						<label for="planpago-sel">Opciones de pago</label>
						<s:select id="planpago-sel" theme="bootstrap" list="planes"
							listValue="descripcion" listKey="idPlanPago" name="idPlanPago"
							onchange="changePlanPago(); ">
						</s:select>
					</div>

					<div class="col-xs-9 col-sm-6 col-md-3 col-lg-3">
						<label for="cuotas-sel">Cant. de cuotas</label> <select
							id="cuotas-sel" class="form-control" name="cuotas"
							onselect="verMontos();" onchange="verMontos();"></select>
					</div>

					<s:iterator value="planes">
						<input type="hidden" id="plan_<s:property value='idPlanPago'/>"
							value="<s:property value='cuotasDesde'/>_<s:property value='cuotasHasta'/>">
					</s:iterator>

					<%-- </s:form> --%>
				</div>

				<div class="row">
					<div class="col-xs-9 col-sm-6 col-md-3 col-lg-3">
						<%-- 	<s:url action="moratoria_montosTish" namespace="/" var="generar"></s:url>
						<sj:submit id="enviar" cssClass="btn btn-primary"
							formIds="tishData" value="Adherir" href="%{generar}"
							targets="container"></sj:submit>

						<sj:a id="saveBt" href="%{generar}" cssClass="btn btn-primary"
							formIds="tishData" targets="container"
							onclick="return validateImporte();">Save</sj:a> --%>
						<button id="enviar2" type="button" class="btn btn-primary"
							onclick="validateImporte();">AdherirT</button>
					</div>
				</div>
			</div>

			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" id="div-total"></div>
		</div>
	</fieldset>
</s:form>
<script type="text/javascript">
	function validateImporte() {
		$('#tishData').bootstrapValidator('validate');

		if ($('#tishData').data('bootstrapValidator').isValid()) {
			$('#tishData').submit();
			//return true;
		}
		//return false;
	}

	/* 	$('#tishData').submit(function(event) {
	 debugger;
	 if (!validateImporte()) {
	 event.preventDefault();
	 }
	 }); */

	
</script>