/*
 * Moratoria - Funciones genericas
 */

function validaCuit(cuit) {
	if (typeof (cuit) == 'undefined')
		return true;
	cuit = cuit.toString().replace(/[-_]/g, "");

	if (cuit == '')
		return true; // No estamos validando si el campo esta vacio, eso
	// queda para el "required"

	if (cuit.length != 11)
		return false;
	else {
		var mult = [ 5, 4, 3, 2, 7, 6, 5, 4, 3, 2 ];
		var total = 0;
		for (var i = 0; i < mult.length; i++) {
			total += parseInt(cuit[i]) * mult[i];
		}
		var mod = total % 11;
		var digito = mod == 0 ? 0 : mod == 1 ? 9 : 11 - mod;
	}
	return digito == parseInt(cuit[10]);
}

/**
 * Activar aviso de carga true cargando false finalizo si puede pasar un button
 * para actualizar el estado del mismo
 * 
 */
function loader(active, btn) {
	if (active) {
		document.getElementById("loader").style.display = "block";
		if (btn) {
			btn.button('loading');
		}
		$("html,body").css("cursor", "progress");
	} else {
		document.getElementById("loader").style.display = "none";
		if (btn) {
			btn.button('reset');
		}
		$("html,body").css("cursor", "default");
	}
}
