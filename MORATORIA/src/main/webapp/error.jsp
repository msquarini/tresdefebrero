
<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>
<html lang="es">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="keywords" content="Tres de febrero, Seguridad, higiene" />
<meta name="description"
	content="Municipalidad de Tres de Febrero - Plan de regularización de deudas" />
<title>Municipalidad de Tres de Febrero</title>

<!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
<!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<sb:head includeScripts="true" includeScriptsValidation="true"
	compressed="false" includeStyles="true" />
<sj:head locale="es" compressed="true" loadAtOnce="true" jqueryui="true"
	loadFromGoogle="false" />

<style type="text/css">
#number_container {
	border: 1px dotted #d0d0d0;
	padding: 15px;
	margin: 10px;
	display: none;
	background: #fafafa;
}

body {
	padding-top: 70px;
	/* 60px to make the container go all the way to the bottom of the topbar */
}

.navbar-brand,
.navbar-nav li a {
    line-height: 80px;
    height: 80px;
    padding-top: 0;
}

.top-buffer {
	margin-top: 50px;
}
</style>
</head>

<body>
	<nav class="navbar navbar-default navbar-fixed-top">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="http://www.tresdefebrero.gov.ar">
					<img src="../img/logo-new.png" alt="" height="75" width="180">
				</a>
			</div>
		</div>
	</nav>

	<div class="container top-buffer" id="container">
		<p align="center" class="bg-danger">Se produjo un error en la
			aplicación.</p>


		<p align="center">
			<s:url var="inicio" action="moratoria_input" namespace="/" />
			<sj:a theme="bootstrap" cssClass="btn btn-primary"
				targets="container" href="%{inicio}">Inicio</sj:a>
		</p>
	</div>


</body>
</html>