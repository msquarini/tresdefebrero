package ar.gov.tresdefebrero.moratoria.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import ar.gov.tresdefebrero.moratoria.beans.SiethCuenta;
import ar.gov.tresdefebrero.moratoria.dao.SiethDao;

public class JdbcSiethDao implements SiethDao {
  private static final Logger logger = LoggerFactory.getLogger("JdbcSiethDao");

  private JdbcTemplate jdbcTemplate;

  private static final String SELECT_CUENTAS_SITH =
      "SELECT c.CUIT as cuit, c.RAZON_SOCIAL as razonsocial, LPAD(m.NRO_CTA_CTE, 6, '0') as cuenta, m.DV_NRO_CTA_CTE as dv, "
          + " a.ID_ACTIVIDAD as act ,a.DESCRIPCION  as descripcion FROM contribuyente c "
          + " INNER JOIN contribuyente_sucursal m ON m.CUIT=c.CUIT INNER JOIN contribuyente_actividad ac "
          + " ON c.CUIT=ac.CUIT INNER JOIN actividad a ON a.ID_ACTIVIDAD=ac.ID_ACTIVIDAD  where c.CUIT = ? AND TIPO_ACTIVIDAD='P';";


  public void setDataSource(DataSource dataSource) {
    this.jdbcTemplate = new JdbcTemplate(dataSource);
  }


  public List<SiethCuenta> findCuentas(String cuit) {
    List<SiethCuenta> cuentas;
    try {
      cuentas = this.jdbcTemplate.query(SELECT_CUENTAS_SITH, new Object[] {cuit},
          new RowMapper<SiethCuenta>() {
            public SiethCuenta mapRow(ResultSet rs, int rowNum) throws SQLException {
              SiethCuenta bean = new SiethCuenta();
              bean.setCuenta(rs.getString("cuenta"));
              bean.setDv(rs.getString("dv"));
              bean.setActividad(rs.getString("act"));
              bean.setDescripcionActividad(rs.getString("descripcion"));
              bean.setCuit(rs.getString("cuit"));
              bean.setRazonSocial(rs.getString("razonsocial"));
              return bean;
            }
          });
    } catch (EmptyResultDataAccessException e) {
      logger.warn("No hay cuentas tish empadronadas");
      return new ArrayList<>();
    }
    return cuentas;
  }

}
