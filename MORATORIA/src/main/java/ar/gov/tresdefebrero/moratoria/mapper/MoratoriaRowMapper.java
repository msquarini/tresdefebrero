package ar.gov.tresdefebrero.moratoria.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import ar.gov.tresdefebrero.moratoria.beans.MoratoriaBean;
import ar.gov.tresdefebrero.moratoria.beans.PlanPago;

/**
 * Mapeo de tabla padron_tsg
 * 
 * @author msquarini
 *
 */
public class MoratoriaRowMapper implements RowMapper<MoratoriaBean> {

	@Override
	public MoratoriaBean mapRow(ResultSet rs, int rowNum) throws SQLException {
		MoratoriaBean bean = new MoratoriaBean();
		bean.setIdMoratoria(Integer.valueOf(rs.getInt("id_adhesion")));
		bean.setSistema(rs.getString("sistema"));
		bean.setCuenta(rs.getString("cuenta"));
		bean.setDv(rs.getString("dv"));
		bean.setDeudaTotal(rs.getBigDecimal("total"));
		bean.setFechaAdhesion(rs.getTimestamp("fecha"));
		bean.setMontoEfectivo(rs.getBigDecimal("monto_efectivo"));
		bean.setMontoCuota(rs.getBigDecimal("monto_cuota"));
		bean.setCuotas(Integer.valueOf(rs.getInt("cuotas")));

		// 20170513 Nueva columna de codigo de moratoria en tabla de adhesion
		//			Permite manejar multiples moratorias
		bean.setCodMoratoria(Integer.valueOf(rs.getInt("mora")));
		
		bean.setEstado(rs.getString("estado"));
		bean.setRegistraPago(Integer.valueOf(rs.getInt("registra_pago")));
		
		bean.setMail(rs.getString("mail"));
		bean.setCuit(rs.getString("cuit"));
		bean.setCodigoCaracter(Integer.valueOf(rs.getInt("codigo_caracter")));
		bean.setDvMoratoria(rs.getString("dv_moratoria"));
		bean.setFechaUltimaCuota(rs.getDate("fecha_ultima_cuota"));
		
		PlanPago planPago = new PlanPago();
		planPago.setIdPlanPago(Integer.valueOf(rs.getInt("id_plan_pago")));
		planPago.setDescripcion(rs.getString("descripcion"));
		planPago.setPorcentajeEfectivo(Double.valueOf(rs.getDouble("porc_efectivo")));
		planPago.setInteresFinanciero(Double.valueOf(rs.getDouble("interes_financiero")));

		// 20170513 nombre de moratoria
		planPago.setMoratoriaNombre(rs.getString("moratoria_nombre"));
		planPago.setMoratoriaTipo(rs.getString("aplica_tipo_deuda"));
		
		bean.setPlanPago(planPago);
		return bean;
	}
}
