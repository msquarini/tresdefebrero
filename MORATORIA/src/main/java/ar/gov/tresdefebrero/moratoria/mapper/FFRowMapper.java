package ar.gov.tresdefebrero.moratoria.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import ar.gov.tresdefebrero.moratoria.beans.FFBean;

/**
 * Mapeo de tabla padron_tsg
 * 
 * @author msquarini
 *
 */
public class FFRowMapper implements RowMapper<FFBean> {

	@Override
	public FFBean mapRow(ResultSet rs, int rowNum) throws SQLException {
		FFBean ff = new FFBean();
		
		ff.setCuenta(rs.getInt("cuenta"));
		ff.setDv(rs.getString("digito"));
		
		//auto.setNomenclatura();
		
		ff.setCalle(rs.getString("nombrecalle"));
		ff.setNumero(rs.getString("nropuerta"));
		ff.setCodigoPostal(rs.getString("localidad"));
		ff.setLocalidad(rs.getString("localidad_desc"));
		ff.setTitular(rs.getString("appynom"));
		
		//abl.setCategoria(rs.getInt("categoria"));
		//abl.setValuacionTotal(rs.getBigDecimal("vtotal"));
		
		return ff;
	}

}
