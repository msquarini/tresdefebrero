package ar.gov.tresdefebrero.moratoria.beans;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang3.time.DateUtils;

import ar.gov.tresdefebrero.moratoria.util.FechaVencimiento;
import ar.gov.tresdefebrero.moratoria.util.NumToText;

/**
 * Entidad que representa una deuda. Utilizada para las deudas existentes, como
 * para la generada a partir de la adhesion a moratoria
 * 
 * @author msquarini
 */
public class DeudaBean {
	protected String cuenta;
	protected String dv;
	protected String sistema;

	/**
	 * Codigo de moratoria
	 */
	protected Integer mora;
	protected Integer anio;
	protected Integer cuota;
	protected String ajuste;
	protected BigDecimal importe;
	protected String debcre;
	protected String marca;
	protected Integer codAdm;
	/**
	 * Fecha de vencimiento real
	 */
	protected Date fechaVto;

	private Integer cuotaBarra;
	private Integer anioBarra;

	/**
	 * Fecha proximo vencimiento, utilizado para generacion de VEPs No se persiste
	 */
	private Date fechaProximo;

	/**
	 * Fecha de pago si presenta uno, null en caso de no pago
	 */
	private Date fechaPago;

	/**
	 * Fecha de vto original
	 */
	protected Date fechaVto2;

	private BigDecimal intereses;

	private Integer idMoratoria;

	public DeudaBean(boolean deuda, boolean primeracuota, Integer cuota, Date desde, Integer mora, String marca) {
		if (deuda) {
			this.debcre = "D";
		} else {
			this.debcre = "C";
		}
		this.mora = mora;
		this.marca = marca;
		if (primeracuota) {
			// this.fechaVto = DateUtils.addDays(desde, 7);

			// Quinto dial habil partir de la fecha de adhesion
			this.fechaVto = DateUtils.addDays(desde, 5);
			this.fechaVto = FechaVencimiento.proximoHabil(fechaVto);
		} else {
			// this.fechaVto = DateUtils.addDays(desde, 30);
			this.fechaVto = DateUtils.addMonths(desde, 1);
			this.fechaVto = DateUtils.setDays(fechaVto, 10);
			this.fechaVto = FechaVencimiento.proximoHabil(fechaVto);
		}
		this.cuota = cuota;

		// this.anio =
		// Integer.valueOf(DateUtils.toCalendar(this.fechaVto).get(Calendar.YEAR));
		// 20180601 Fijo anio de cuotas de moratoria a 2018
		//this.anio = 2018;
		this.anio = 2019;
		this.ajuste = "0";
		this.codAdm = Integer.valueOf(0);

		setAnioCuotaBarra();

		this.fechaVto2 = this.fechaVto;
		this.intereses = BigDecimal.ZERO;
	}

	public DeudaBean() {
	}

	public void setAnioCuotaBarra() {
		// 20180531 Cambio para utilizar siempre la cuota 001 para la barra
		this.cuotaBarra = 1;

		switch (this.mora) {
		case 505:
			anioBarra=2002;
			break;
		case 506:
			if (this.cuota < 13) {
				this.anioBarra = 2003;
			} else {
				this.anioBarra = 2004;}
			break;
		case 507:
			if (this.cuota < 13) {
				this.anioBarra = 2005;
			} else {
				this.anioBarra = 2006;}
			break;
		case 508:
			this.anioBarra=2007;
			break;
		case 509:
			this.anioBarra=2008;
			break;
		case 510:
			this.anioBarra=2009;
			break;
			
		
		default:	
				this.anioBarra = this.anio;
				this.cuotaBarra = this.cuota;
			break;
			
	}
		
/*		if (this.mora.equals(505)) {
			this.anioBarra = 2002;
		} else if (this.mora.equals(506)) {
			if (this.cuota < 13) {
				this.anioBarra = 2003;
			} else {
				this.anioBarra = 2004;
			}
		} else if (this.mora.equals(507)) {
			if (this.cuota < 13) {
				this.anioBarra = 2005;
			} else {
				this.anioBarra = 2006;
			}
		} else {
			// Modulo 12 de las cuotas
			/*
			 * this.cuotaBarra = this.cuota % 12; if (this.cuotaBarra.equals(0)) {
			 * this.cuotaBarra = 12; }
			 */

			// Por compatibilidad
			// 20180803 MS Compatibilidad moratorias previas
	//		this.anioBarra = this.anio;
	//		this.cuotaBarra = this.cuota;
		//}
	}

	public String getCuenta() {
		return this.cuenta;
	}

	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}

	public String getDv() {
		return this.dv;
	}

	public void setDv(String dv) {
		this.dv = dv;
	}

	public String getSistema() {
		return this.sistema;
	}

	public void setSistema(String sistema) {
		this.sistema = sistema;
	}

	public Integer getMora() {
		return this.mora;
	}

	public void setMora(Integer mora) {
		this.mora = mora;
	}

	public Integer getAnio() {
		return this.anio;
	}

	public void setAnio(Integer anio) {
		this.anio = anio;
	}

	public Integer getCuota() {
		return this.cuota;
	}

	public void setCuota(Integer cuota) {
		this.cuota = cuota;
	}

	public String getAjuste() {
		return this.ajuste;
	}

	public void setAjuste(String ajuste) {
		this.ajuste = ajuste;
	}

	public BigDecimal getImporte() {
		// return this.importe;
		// devolver el importe original + los intereses calculados si es pago fuera de
		// termino
		if (intereses != null) {
			return this.importe.add(intereses);
		}
		return this.importe;
	}
	
	public BigDecimal getImporteOriginal() {
		return this.importe;
	}

	public void setImporte(BigDecimal importe) {
		this.importe = importe;
		this.importe.setScale(2, RoundingMode.HALF_UP);
	}

	public String getDebcre() {
		return this.debcre;
	}

	public void setDebcre(String debcre) {
		this.debcre = debcre;
	}

	public String getMarca() {
		return this.marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public Integer getCodAdm() {
		return this.codAdm;
	}

	public void setCodAdm(Integer codAdm) {
		this.codAdm = codAdm;
	}

	public Date getFechaVto() {
		return this.fechaVto;
	}

	public void setFechaVto(Date fechaVto) {
		this.fechaVto = fechaVto;
	}

	public Double getMontoCredito() {
		if (this.debcre.equals("C")) {
			return Double.valueOf(this.importe.doubleValue());
		}
		return null;
	}

	public Double getMontoDeuda() {
		if (this.debcre.equals("D")) {
			return Double.valueOf(this.importe.doubleValue());
		}
		return null;
	}

	public Integer getImporteEntero() {
		return Integer.valueOf(getImporte().intValue());
		// return Integer.valueOf(getImporteAPagar().intValue());
	}

	public Integer getImporteDecimal() {
		/*
		 * BigDecimal centavos = getImporte().subtract(getImporte().setScale(0,
		 * RoundingMode.FLOOR)).movePointRight(2);
		 */
		// return Integer.valueOf(centavos.intValue());

		BigDecimal centavos = getImporte().subtract(new BigDecimal(getImporte().intValue()));
		BigDecimal cent = centavos.setScale(2, RoundingMode.HALF_UP).multiply(BigDecimal.valueOf(100));
		return Integer.valueOf(cent.intValue());

	}

	/**
	 * Codigo de barra
	 * 
	 * LONGITUD 34 posiciones 1 a 4 Año 5 a 7 Cuota 8 a 13 Número de Cuenta 14
	 * Dígito Verificador 15 a 26 Importe de la Cuota (10 enteros 2 decimales,
	 * completado con ceros a izquierda) 27 a 34 Identificación de la Moratoria
	 * (00000502)
	 * 
	 * @return
	 */
	public String getBarcode() {
		StringBuffer barcode = new StringBuffer();

		/**
		 * 2018 Modificamos fecha y cuota para identificar moratoria
		 */
		// barcode.append(getAnio());
		// barcode.append(String.format("%03d", new Object[] {
		// Integer.valueOf(getCuota().intValue())
		// }));
		
		barcode.append(getAnioBarra());
		barcode.append(String.format("%03d", new Object[] { Integer.valueOf(getCuotaBarra().intValue()) }));

		barcode.append(String.format("%6s", new Object[] { getCuenta() }));

		// Solo para Servicios Generales (ABL)
		// if (getSistema().equals("AL") || getSistema().equals("IC") ) {
		if (getDv() != null && getDv().equals("-")) {
			barcode.append(String.format("%01d", new Object[] { Integer.valueOf(0) }));
		} else if (getDv() != null && getDv().length() > 0) {
			barcode.append(String.format("%01d", new Object[] { Integer.valueOf(getDv()) }));
		}
		// }
		// else { // Si no tiene DV caso Auto y motos
		// barcode.append(String.format("%01d", new Object[] {
		// Integer.valueOf(0) }));
		// }

		barcode.append(String.format("%010d", new Object[] { getImporteEntero() }));
		barcode.append(String.format("%02d", new Object[] { Integer.valueOf(getImporteDecimal().intValue()) }));

		// 2018 Modificamos el codigo de mora por FECHA de Vencimiento
		// Usamos el DV para indicar el codigo de moratoria!
		// 20180803 MS - Por compatibilidad se incorpora
		if (getMora() < 505) {
			barcode.append(String.format("%08d", new Object[] { getMora() }));
		} else {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			barcode.append(sdf.format(fechaVto));
		}

		return barcode.toString();
	}

	public Date getFechaProximo() {
		return fechaProximo;
	}

	public void setFechaProximo(Date fechaProximo) {
		this.fechaProximo = fechaProximo;
	}

	public String getKey() {
		return getSistema().concat(getCuenta()).concat(getAnio().toString()).concat(getCuota().toString());
	}

	public String getMontoEnLetras() {
		NumToText ntt = new NumToText();
		return ntt.convertirLetras(getImporte());
	}

	public Integer getCuotaBarra() {
		return cuotaBarra;
	}

	public void setCuotaBarra(Integer cuotaBarra) {
		this.cuotaBarra = cuotaBarra;
	}

	public Integer getAnioBarra() {
		return anioBarra;
	}

	public void setAnioBarra(Integer anioBarra) {
		this.anioBarra = anioBarra;
	}

	public Boolean isVencida() {
		Calendar now = Calendar.getInstance();
		return now.getTime().after(this.fechaVto);
	}

	public Date getFechaPago() {
		return fechaPago;
	}

	public void setFechaPago(Date fechaPago) {
		this.fechaPago = fechaPago;
	}

	public Date getFechaVto2() {
		return fechaVto2;
	}

	public void setFechaVto2(Date fechaVto2) {
		this.fechaVto2 = fechaVto2;
	}

	public BigDecimal getIntereses() {
		return intereses;
	}

	public void setIntereses(BigDecimal intereses) {
		this.intereses = intereses;
	}

	public Integer getIdMoratoria() {
		return idMoratoria;
	}

	public void setIdMoratoria(Integer idMoratoria) {
		this.idMoratoria = idMoratoria;
	}
}
