package ar.gov.tresdefebrero.moratoria.beans;

/**
 * Interfaz que implementan todo los tipos de impuestos
 * 
 * @author msquarini
 *
 */
public interface Cuenta {

	String getCuenta();

	String getDv();

	String getImpuesto();
	
	String getCuentaBarcode();
	
	String getDvBarcode();
	
	void setCuit(String cuit);
	void setEnCaracter(Integer enCaracter);
	
	String getCuit();
    Integer getEnCaracter();
}
