package ar.gov.tresdefebrero.moratoria.actions;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.SessionAware;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import com.opensymphony.xwork2.ActionSupport;

import ar.gov.tresdefebrero.moratoria.beans.Cuenta;
import ar.gov.tresdefebrero.moratoria.beans.DeudaBean;
import ar.gov.tresdefebrero.moratoria.beans.DeudaTishBean;
import ar.gov.tresdefebrero.moratoria.beans.MontoMoratoria;
import ar.gov.tresdefebrero.moratoria.beans.MoratoriaBean;
import ar.gov.tresdefebrero.moratoria.beans.PlanPago;
import ar.gov.tresdefebrero.moratoria.business.MoratoriaManager;
import ar.gov.tresdefebrero.moratoria.dao.ConsultaDao;
import ar.gov.tresdefebrero.moratoria.dao.MoratoriaDao;
import ar.gov.tresdefebrero.moratoria.util.ApplicationContextHelper;

/**
 * Accion para la gestion de moratoria Busqueda de cuentas Presentacion de deudas y planes de pago
 * Generacion de moratoria y veps
 * 
 * 2018 - Validacion de CUIT
 * 
 * @author msquarini
 *
 */
public class Moratoria2018Action extends ActionSupport implements SessionAware {

  private static final long serialVersionUID = 7353477345330099518L;

  private static final Logger logger = LoggerFactory.getLogger("Moratoria2018Action");

  private String email;
  private String telefono;

  /**
   * Tipo de deuda (Pre, jud, Fis)
   */
  /*private String tipo;*/
  
  /**
   * Planes de pagos disponibles
   */
  private List<PlanPago> planes;
  /**
   * Id de plan de pago seleccionado
   */
  private Integer idPlanPago;

  /**
   * Cantidad de cuotas seleccionada
   */
  private Integer cuotas;

  /**
   * Indica que tipo de moratoria se esta creando: PRE P - JUD J- FISCA F
   */
  private String tipoMoratoria;

  /**
   * Montos de la moratoria segun plan de pagos seleccionado
   */
  private MontoMoratoria montoMoratoria;

  /**
   * Deudas existentes de la cuenta
   */
  List<DeudaBean> deudas;
  /**
   * Total de deuda, calculado segun creditos y debitos
   */
  Double totalDeuda;

  // En ejecucion, TISH carga de montos
  double[] importes;
  String[] keyDeuda;

  /**
   * Flag si presenta moratoria
   */
  Integer moratoriaExistente;

  /**
   * Moratoria existente
   */
  MoratoriaBean moratoria;

  /**
   * 20170513 Inclusion de nueva moratoria Lista de moratorias adheridas
   */
  private List<MoratoriaBean> moratorias;
  private Integer moratoriaId;

  private ConsultaDao dao;
  private MoratoriaDao moratoriaDao;
  private MoratoriaManager manager;

  private Map<String, Object> session;
  private InputStream inputStreamA;
  private String fileName;

  /**
   * Flag para indicar si presenta deuda de moratoria 502
   */
  private Boolean existsDeudaMoratoria;
  /**
   * Flag para indicar que existe adhesion a moratoria vigente
   */
  private Boolean existsAdhesionMoraVigente;

  public Moratoria2018Action() {
    ApplicationContext springContext = ApplicationContextHelper.getApplicationContext();
    this.dao = ((ConsultaDao) springContext.getBean("jdbcConsultaDao"));
    this.moratoriaDao = ((MoratoriaDao) springContext.getBean("jdbcMoratoriaDao"));
    this.manager = ((MoratoriaManager) springContext.getBean("moratoriaManager"));
    
  }

  public Map<String, Object> getSession() {
    return this.session;
  }

  public void setSession(Map<String, Object> session) {
    this.session = session;
  }

  /*
   * public String goToAdhesion() { this.moratoriaExistente = 0; this.deudas = (List<DeudaBean>)
   * getSession().get("deudas"); // calcularTotalDeuda(); this.planes = this.dao.findPlanesPago();
   * getSession().put("planes", this.planes); return "datos"; }
   */

  /**
   * TAB Prejudicial
   * 
   * @return
   */
  public String goToPrejudicial() {
    Cuenta cuenta = (Cuenta) getSession().get("cuenta");

    logger.info("goToPrejudicial, cuenta {}, tipo: {}", cuenta, tipoMoratoria);

    checkAdhesionMoratoria(cuenta, "P");

    if (existsAdhesionMoraVigente) {
      this.deudas = moratoria.getDeudaOriginal();
      getSession().put("moratoria", moratoria);
    } else {
      this.deudas = this.dao.findDeuda(cuenta, "P");
      this.planes = this.dao.findPlanesPago("P");
      getSession().put("planes", planes);
    }

    this.totalDeuda = manager.calcularTotalDeuda(this.deudas);
    getSession().put("deudas", deudas);
    return "prejudicial";
  }

  /**
   * TAB Judicial
   * 
   * @return
   */
  public String goToJudicial() {
    Cuenta cuenta = (Cuenta) getSession().get("cuenta");

    logger.info("goToJudicial, cuenta {}, tipo: {}", cuenta, tipoMoratoria);

    checkAdhesionMoratoria(cuenta, "J");

    if (existsAdhesionMoraVigente) {
      this.deudas = moratoria.getDeudaOriginal();
      getSession().put("moratoria", moratoria);
    } else {
      this.deudas = this.dao.findDeuda(cuenta, "J");
      this.planes = this.dao.findPlanesPago("J");
      getSession().put("planes", planes);
    }

    this.totalDeuda = manager.calcularTotalDeuda(this.deudas);
    getSession().put("deudas", deudas);
    return "judicial";
  }
  
  /**
   * TAB Judicial
   * 
   * @return
   */
  public String goToFiscalizada() {
    Cuenta cuenta = (Cuenta) getSession().get("cuenta");

    logger.info("goToFiscalizada, cuenta {}, tipo: {}", cuenta, tipoMoratoria);

    checkAdhesionMoratoria(cuenta, "F");

    if (existsAdhesionMoraVigente) {
      this.deudas = moratoria.getDeudaOriginal();
      getSession().put("moratoria", moratoria);
    } else {
      this.deudas = this.dao.findDeuda(cuenta, "F");
      this.planes = this.dao.findPlanesPago("F");
      getSession().put("planes", planes);
    }

    this.totalDeuda = manager.calcularTotalDeuda(this.deudas);
    getSession().put("deudas", deudas);
    return "fiscalizada";
  }
  
  private void checkAdhesionMoratoria(Cuenta cuentaBean, String tipo) {
    existsAdhesionMoraVigente =
        this.dao.checkAdhesionMoratoria(cuentaBean, this.manager.getCodigoMoratoria(tipo));
    moratoria = this.moratoriaDao.findMoratoria(cuentaBean.getImpuesto(), cuentaBean.getCuenta(),
        cuentaBean.getDv(), this.manager.getCodigoMoratoria(tipo));

    logger.info("Consulta de adhesion moratoria vigente para cuenta {}, tipo: {}, existe: {}",
        cuentaBean, tipo, existsAdhesionMoraVigente);
  }

  public String goToMoratoria() {
    logger.info("gotomoratoria, id: {}", moratoriaId);

    moratoria = null;
    for (MoratoriaBean m : (List<MoratoriaBean>) getSession().get("moratorias")) {
      if (m.getIdMoratoria().equals(moratoriaId)) {
        moratoria = m;
        break;
      }
    }
    this.moratoriaExistente = 1;
    this.deudas = moratoria.getDeudaOriginal();
    this.totalDeuda = manager.calcularTotalDeuda(this.deudas);
    getSession().put("deudas", deudas);
    getSession().put("moratoria", moratoria);

    return "moratoria_info";
  }

  /**
   * Actualiza los valores de efectivo y monto cuota de acuerdo al plan de pago y cuotas
   * seleccionado
   * 
   * @return
   */
  public String simularMoratoria() {
    logger.info("simular moratoria 2018, PP {}, cuotas {}", this.idPlanPago, this.cuotas);
    try {
      this.deudas = ((List) getSession().get("deudas"));
      // this.deudas = ((List) getSession().get(getTipo()));

      PlanPago pp = buscarPlanPago(this.idPlanPago);
      if (this.cuotas == null) {
        this.cuotas = Integer.valueOf(0);
      }
      this.montoMoratoria = pp.calculateDeuda(this.deudas, this.cuotas);
    } catch (Exception e) {
      logger.error("", e);
    }
    logger.info("Simulacion deuda 2018 - Plan {} - Cuotas {} - Montos {}",
        new Object[] {this.idPlanPago, this.cuotas, this.montoMoratoria});

    return "simulate";
  }

  /**
   * Simulacion de moratoria TISH Se toman los importes ingresados y se evalua con el proceso
   * estandar
   * 
   * @return
   */
  public String simularMoratoriaTish() {
    logger.info("simular moratoria 2018 TISH, PP {}, cuotas {}", this.idPlanPago, this.cuotas);
    try {
      this.deudas = ((List) getSession().get("deudas"));
      for (int pos = 0; pos < importes.length; pos++) {
        Double importe = importes[pos];
        // if (importe != null && importe > 0) {
        // this.deudas.get(pos).setPagar(new BigDecimal(importe));
        setImporteDeuda(importe, keyDeuda[pos]);
        // }
      }
    } catch (Exception e) {
      logger.error("simuluar moratoria 2018 tish", e);
    }
    return simularMoratoria();
  }

  /**
   * Adhesion a moratoria segun plan de pagos seleccionado
   * 
   * @return
   * @throws Exception
   */
  public String adherir() {
	  
	if ((Boolean)getSession().get("finMoratoria")) {
		logger.error("Error FIN DE MORATORIA");
	      addActionError("La moratoria no se encuentra vigente.");
	      addActionMessage("No se pueden realizar nuevas adhesiones.");

		return "errorJSON";
	}
	
    Cuenta cuenta = (Cuenta) getSession().get("cuenta");
    this.deudas = ((List) getSession().get("deudas"));
    // this.deudas = ((List) getSession().get(getTipo()));

    PlanPago planPago = buscarPlanPago(this.idPlanPago);

    if ((cuenta == null) || (this.deudas == null) || (planPago == null)) {
      logger.warn("adherir - Datos no encontrados en session, cuenta {}, deudas {}, plan pago {}",
          new Object[] {cuenta, this.deudas, planPago});
      // invalidarSession();
      addActionError("Se produjo un error al intentar la operacion.");
      addActionMessage("Por favor intente más tarde.");
      // Retornar mensaje de error y init
      return "errorJSON";
    }

    if (this.cuotas == null) {
      this.cuotas = Integer.valueOf(0);
    }

    try {
      logger.info("moratoria 2018 - adherir, cuenta {}  impuesto {}, idPlanPago {}, cuotas {}",
          new Object[] {cuenta.getCuenta(), cuenta.getImpuesto(), this.idPlanPago, this.cuotas});

      Integer codigoMoratoria = this.manager.getCodigoMoratoria(getTipoMoratoria());

      MoratoriaBean moratoria = this.manager.adhesionMoratoria(cuenta, this.deudas, planPago,
          this.cuotas, codigoMoratoria, email);

      getSession().put("moratoria", moratoria);
    } catch (Exception e) {
      logger.error("Error en adhesion", e);
      return "errorJSON";
    }
    return "predownload";
  }

  public String predownload() {
    return "predownload";
  }

  /**
   * Descarga de la boleta de pago
   * 
   * @return
   * @throws Exception
   */
  public String download() throws Exception {

    try {
      Cuenta bean = (Cuenta) getSession().get("cuenta");
      MoratoriaBean moratoria;
      logger.info("descargando volante de pago, cuent: {}, tipo: {}", bean, tipoMoratoria);

      /*
       * if (tipo != null && tipo.equals("deudasPre")) { moratoria = (MoratoriaBean)
       * getSession().get("adhesionMoraVigentePrejudicial"); } else {
       */
      moratoria = (MoratoriaBean) getSession().get("moratoria");
      // }

      /*
       * File pdf = File.createTempFile( "moratoria-hardcode", ".pdf"); setFileName(pdf.getName());
       * 
       * // Stream para descarga this.inputStreamA = new FileInputStream(pdf);
       */

      if (bean == null || moratoria == null) {
        logger.error("Error en descarga de boletas, {}, {}", bean, moratoria);
        return "input";
      }

      File pdf = File.createTempFile(
          "moratoria-" + moratoria.getSistema() + "-" + moratoria.getCuenta(), ".pdf");
      manager.generarVolantePago(moratoria, bean, pdf);
      // Nombre del file
      setFileName(pdf.getName());

      // Stream para descarga
      this.inputStreamA = new FileInputStream(pdf);

      // invalidarSession();
    } catch (Exception e) {
      logger.error("", e);
    }
    return "download";
  }

  public InputStream getInputStreamA() {
    return this.inputStreamA;
  }

  public void setInputStreamA(InputStream inputStream) {
    this.inputStreamA = inputStream;
  }

  public String getFileName() {
    return this.fileName;
  }

  public void setFileName(String fileName) {
    this.fileName = fileName;
  }

  public List<DeudaBean> getDeudas() {
    return this.deudas;
  }

  public void setDeudas(List<DeudaBean> deudas) {
    this.deudas = deudas;
  }

  public Double getTotalDeuda() {
    return this.totalDeuda;
  }

  public void setTotalDeuda(Double totalDeuda) {
    this.totalDeuda = totalDeuda;
  }

  public List<PlanPago> getPlanes() {
    return this.planes;
  }

  public void setPlanes(List<PlanPago> planes) {
    this.planes = planes;
  }

  public Integer getIdPlanPago() {
    return this.idPlanPago;
  }

  public void setIdPlanPago(Integer idPlanPago) {
    this.idPlanPago = idPlanPago;
  }

  public Integer getCuotas() {
    return this.cuotas;
  }

  public void setCuotas(Integer cuotas) {
    this.cuotas = cuotas;
  }

  /**
   * Busca el plan de pago segun el id seleccionado por el usuario
   * 
   * @param idPlanPago
   * @return
   */
  private PlanPago buscarPlanPago(Integer idPlanPago) {
    this.planes = ((List) getSession().get("planes"));
    PlanPago result = null;
    for (PlanPago pp : this.planes) {
      if (pp.getIdPlanPago() == idPlanPago) {
        result = pp;
        break;
      }
    }
    return result;
  }

  public MontoMoratoria getMontoMoratoria() {
    return this.montoMoratoria;
  }

  public void setMontoMoratoria(MontoMoratoria montoMoratoria) {
    this.montoMoratoria = montoMoratoria;
  }

  public double[] getImportes() {
    return this.importes;
  }

  public void setImportes(double[] importes) {
    this.importes = importes;
  }

  /**
   * Remueve datos de session y la invalida
   */
  private void invalidarSession() {
    String mail = (String) getSession().get("mail");
    getSession().remove("cuenta");
    getSession().remove("deudas");
    getSession().remove("moratoria");
    ((SessionMap) getSession()).invalidate();

    getSession().put("mail", mail);
  }

  /**
   * Gestion de deuda TISH Actualiza montos y adhiere a la moratoria
   * 
   * @return
   */
  public String montosTish() {
    logger.info("montos tish PP {}, cuotas {}", this.idPlanPago, this.cuotas);
    try {
      this.deudas = ((List) getSession().get("deudas"));
      for (int pos = 0; pos < importes.length; pos++) {
        Double importe = importes[pos];
        // if (importe != null && importe > 0) {
        setImporteDeuda(importe, keyDeuda[pos]);
        // }
      }
    } catch (Exception e) {
      addActionError("Error en carga de importe de cuotas");
      logger.error("montos tish", e);
      invalidarSession();
      // Retornar mensaje de error y init
      return "errorJSON";
    }

    /*
     * if ((hasFieldErrors()) || (hasActionErrors())) { return goToAdhesion(); }
     */

    // Eliminar registros de ajustes sin importe!!
    // Marcar registros de ajustes con importe + 1 en el campo ajuste!!
    for (Iterator<DeudaBean> it = deudas.iterator(); it.hasNext();) {
      DeudaTishBean dtb = (DeudaTishBean) it.next();
      if (dtb.getPresentaPago() && dtb.getImporte().doubleValue() > 0) {
        dtb.setAjuste(String.valueOf(Integer.valueOf(dtb.getAjuste()) + 1));
      } else if (dtb.getPresentaPago() && dtb.getImporte().doubleValue() == 0) {
        it.remove();
      }
    }
    return adherir();
  }

  public List<MoratoriaBean> getMoratorias() {
    return moratorias;
  }

  public void setMoratorias(List<MoratoriaBean> moratorias) {
    this.moratorias = moratorias;
  }

  public Integer getMoratoriaId() {
    return moratoriaId;
  }

  public void setMoratoriaId(Integer moratoriaId) {
    this.moratoriaId = moratoriaId;
  }

  public MoratoriaBean getMoratoria() {
    return moratoria;
  }

  public void setMoratoria(MoratoriaBean moratoria) {
    this.moratoria = moratoria;
  }

  public Integer getMoratoriaExistente() {
    return moratoriaExistente;
  }

  public void setMoratoriaExistente(Integer moratoriaExistente) {
    this.moratoriaExistente = moratoriaExistente;
  }

  public Boolean getExistsDeuda() {
    return getTotalDeuda() > 0;
  }

  public Boolean getExistsDeudaMoratoria() {
    return existsDeudaMoratoria;
  }

  public void setExistsDeudaMoratoria(Boolean existsDeudaMoratoria) {
    this.existsDeudaMoratoria = existsDeudaMoratoria;
  }

  public String[] getKeyDeuda() {
    return keyDeuda;
  }

  public void setKeyDeuda(String[] keyDeuda) {
    this.keyDeuda = keyDeuda;
  }

  /**
   * Actualiza importe de deudas tish
   * 
   * @param importe
   * @param key
   */
  private void setImporteDeuda(Double importe, String key) {
    for (DeudaBean d : this.deudas) {

      if (d.getKey().equals(key)) {
        DeudaTishBean dtb = (DeudaTishBean) d;

        // if (!dtb.getPresentaPago() && dtb.getMinimo() != null
        // && dtb.getMinimo().compareTo(d.getImporte()) < 1) {

        if (importe == null) {
          d.setImporte(BigDecimal.ZERO);
        } else {
          d.setImporte(new BigDecimal(importe));
        }
        logger.info("Actualizando importe, Cuota {}, monto {}", key, importe);
        // } else {
        // d.setImporte(new BigDecimal(importe));
        // }
        break;
      }

    }
  }

  public Boolean getExistsAdhesionMoraVigente() {
    return existsAdhesionMoraVigente;
  }

  public void setExistsAdhesionMoraVigente(Boolean existsAdhesionMoraVigente) {
    this.existsAdhesionMoraVigente = existsAdhesionMoraVigente;
  }

  public String getTipoMoratoria() {
    return tipoMoratoria;
  }

  public void setTipoMoratoria(String tipoMoratoria) {
    this.tipoMoratoria = tipoMoratoria;
  }

/*  public String getTipo() {
    return tipo;
  }

  public void setTipo(String tipo) {
    this.tipo = tipo;
  }
*/
  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getTelefono() {
    return telefono;
  }

  public void setTelefono(String telefono) {
    this.telefono = telefono;
  }

  public MoratoriaDao getMoratoriaDao() {
    return moratoriaDao;
  }

  public void setMoratoriaDao(MoratoriaDao moratoriaDao) {
    this.moratoriaDao = moratoriaDao;
  }

}
