package ar.gov.tresdefebrero.moratoria.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import ar.gov.tresdefebrero.moratoria.beans.Nomenclatura;

public class NomenclaturaMapper {

	public static Nomenclatura map(ResultSet rs) throws SQLException {
		
		Nomenclatura nomenclatura = new Nomenclatura();
        nomenclatura.setCircunscripcion(rs.getInt("circ"));
        nomenclatura.setSeccion(rs.getString("sec"));
        nomenclatura.setCodManzana(rs.getInt("cod_mz"));
        nomenclatura.setManzanaNro(rs.getInt("mz"));
        nomenclatura.setManzanaLetra(rs.getString("mzl"));
        nomenclatura.setParcelaNro(rs.getInt("pc"));
        nomenclatura.setParcelaLetra(rs.getString("pcl"));
        nomenclatura.setSubParcela(rs.getString("uf"));
        
        return nomenclatura;
	}
}
