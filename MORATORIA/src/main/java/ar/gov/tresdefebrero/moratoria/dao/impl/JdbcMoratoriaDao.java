package ar.gov.tresdefebrero.moratoria.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.util.List;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import ar.gov.tresdefebrero.moratoria.beans.DeudaBean;
import ar.gov.tresdefebrero.moratoria.beans.MoratoriaBean;
import ar.gov.tresdefebrero.moratoria.dao.MoratoriaDao;
import ar.gov.tresdefebrero.moratoria.mapper.DeudaMoratoriaRowMapper;
import ar.gov.tresdefebrero.moratoria.mapper.MoratoriaRowMapper;

/**
 * Implementacion JDBC de acceso a datos para moratoria
 * 
 * @author admin
 *
 */
public class JdbcMoratoriaDao implements MoratoriaDao {

  private static final Logger logger = LoggerFactory.getLogger("JdbcMoratoriaDao");

  private JdbcTemplate jdbcTemplate;

  /**
   * Insert de registro de adesion a moratoria
   */
  private static final String INSERT_MORATORIA =
      "INSERT INTO adhesion_moratoria (cuenta, dv, sistema, id_plan_pago, total, cuotas, monto_efectivo, monto_cuota, "
          + "mora, mail, cuit, codigo_caracter, dv_moratoria, fecha_ultima_cuota, autog) VALUES( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
  private static final String UPDATE_MORATORIA =
      "UPDATE adhesion_moratoria SET estado = ? where id_adhesion = ?";

  private static final String INSERT_DEUDA_MORATORIA =
      "INSERT INTO moratoria_deuda (id_adhesion, anio, cuota, importe, fecha_vto, mora, marca, debcre, ajuste, cod_adm, fecha_vto2) VALUES(?,?,?,?,?,?,?, ?,?,?,?)";

  private static final String SELECT_MORATORIAS =
      "SELECT a.id_adhesion, a.sistema, a.cuenta, a.dv, a.total, a.id_plan_pago, "
          + " a.cuotas, a.fecha, a.monto_efectivo, a.monto_cuota, a.registra_pago, a.estado, a.mora, "
          + " a.mail, a.cuit, a.dv_moratoria, a.fecha_ultima_cuota, a.codigo_caracter, "
          + " p.moratoria_nombre, p.aplica_tipo_deuda, p.descripcion, p.porc_efectivo, p.interes_financiero "
          + " FROM adhesion_moratoria a inner join plan_pago p ON a.id_plan_pago = p.id_plan_pago "
          + " where a.sistema = ? and a.cuenta = ? and a.dv = ?";

  private static final String SELECT_MORATORIA =
      "SELECT a.id_adhesion, a.sistema, a.cuenta, a.dv, a.total, a.id_plan_pago, "
          + " a.cuotas, a.fecha, a.monto_efectivo, a.monto_cuota, a.registra_pago, a.estado, a.mora, "
          + " a.mail, a.cuit, a.dv_moratoria, a.fecha_ultima_cuota, a.codigo_caracter, "
          + " p.moratoria_nombre, p.aplica_tipo_deuda, p.descripcion, p.porc_efectivo, p.interes_financiero "
          + " FROM adhesion_moratoria a inner join plan_pago p ON a.id_plan_pago = p.id_plan_pago "
          + " where a.sistema = ? and a.cuenta = ? and a.dv = ? and a.mora = ?";

  /**
   * QUERY para la consulta de deudas de impuestos de una adhesion a moratoria
   */
  private static final String SELECT_MORATORIA_DEUDA =
      "select id_adhesion, anio, cuota, importe, fecha_vto, mora, marca, ajuste, debcre, cod_adm, fecha_pago, fecha_vto2, intereses "
          // + " from moratoria_deuda where id_adhesion = ? and marca = ? and
          // mora = ? order by fecha_vto asc";
          // 20170522 Se elimina el filtro x moratoria, se trabaja solo con la marca,
          // donde 1 esa deuda original, 2 deuda de moratoria
          //+ " from moratoria_deuda where id_adhesion = ? and marca = ? order by fecha_vto asc";
		  + " from moratoria_deuda where id_adhesion = ? and marca = ? order by anio, cuota asc";

  private static final String UPDATE_DEUDA_MORATORIA = 
      "UPDATE moratoria_deuda SET intereses = ?, fecha_vto = ? where id_adhesion = ? and anio = ? and cuota = ?";
      
  public void setDataSource(DataSource dataSource) {
    this.jdbcTemplate = new JdbcTemplate(dataSource);
  }

  /*
   * (non-Javadoc)
   * 
   * @see ar.gov.tresdefebrero.moratoria.dao.MoratoriaDao#adhesionMoratoria(ar.gov.
   * tresdefebrero.moratoria.beans.MoratoriaBean)
   */
  public Integer adhesionMoratoria(final MoratoriaBean bean) {
    int result = 0;
    KeyHolder keyHolder = new GeneratedKeyHolder();
    try {
      result = jdbcTemplate.update(new PreparedStatementCreator() {
        public PreparedStatement createPreparedStatement(Connection connection)
            throws SQLException {
          PreparedStatement ps =
              connection.prepareStatement(INSERT_MORATORIA, new String[] {"id_adhesion"});
          ps.setString(1, bean.getCuenta());
          ps.setString(2, bean.getDv());
          ps.setString(3, bean.getSistema());
          ps.setInt(4, bean.getPlanPago().getIdPlanPago());
          ps.setBigDecimal(5, bean.getDeudaTotal());
          ps.setInt(6, bean.getCuotas());
          ps.setBigDecimal(7, bean.getMontoEfectivo());
          ps.setBigDecimal(8, bean.getMontoCuota());
          ps.setInt(9, bean.getCodMoratoria());
          ps.setString(10, bean.getMail());
          ps.setString(11, bean.getCuit());
          ps.setInt(12, bean.getCodigoCaracter());
          ps.setString(13, bean.getDvMoratoria());
          ps.setDate(14, new java.sql.Date(bean.getFechaUltimaCuota().getTime()));
          ps.setInt(15, bean.getAutog());
          return ps;
        }
      }, keyHolder);
    } catch (DataAccessException e) {
      logger.error("No se pudo persistir la adhesion de moratoria {} {} {}", bean.getCuenta(),
          bean.getDv(), bean.getSistema(), e);
      throw e;
    }
    if (result == 0) {
      logger.error("No se pudo persistir la adhesion de moratoria {} {} {}", bean.getCuenta(),
          bean.getDv(), bean.getSistema());
      throw new IllegalStateException();
    }
    return keyHolder.getKey().intValue();
  }

  /*
   * (non-Javadoc)
   * 
   * @see ar.gov.tresdefebrero.moratoria.dao.MoratoriaDao#crearDeudaMoratoria(java. lang.Integer,
   * java.util.List, java.lang.String, java.lang.Integer)
   */
  @Override
  public void crearDeudaMoratoria(Integer idMoratoria, List<DeudaBean> deudas, String marca,
      Integer mora) {
    int result;

    try {
      for (DeudaBean deuda : deudas) {
        deuda.setMarca(marca);
        // La deuda debe venir con el codigo seteado
        // deuda.setMora(mora);
        result = this.jdbcTemplate.update(INSERT_DEUDA_MORATORIA,
            new Object[] {idMoratoria, deuda.getAnio(), deuda.getCuota(), deuda.getMontoDeuda(),
                deuda.getFechaVto(), deuda.getMora(), deuda.getMarca(), deuda.getDebcre(),
                deuda.getAjuste(), deuda.getCodAdm(), deuda.getFechaVto2()},
            new int[] {Types.INTEGER, Types.INTEGER, Types.INTEGER, Types.NUMERIC, Types.DATE,
                Types.INTEGER, Types.VARCHAR, Types.VARCHAR, Types.INTEGER, Types.INTEGER,
                Types.DATE});
        if (result != 1) {
          logger.error("error al crear deuda {} para moratoria id {}", deuda, idMoratoria);
          throw new IllegalStateException();
        }
      }
    } catch (DataAccessException e) {
      logger.error("", e);
      throw e;
    }
  }

  @Override
  public MoratoriaBean findMoratoria(String sistema, String cuenta, String dv,
      Integer codMoratoria) {
    MoratoriaBean bean;
    try {
      bean = this.jdbcTemplate.queryForObject(SELECT_MORATORIA,
          new Object[] {sistema, cuenta, dv, codMoratoria}, new MoratoriaRowMapper());

      /*
       * Cargo la deuda original y la generada para la moratoria
       */
      if (bean != null) {
        bean.setDeudaOriginal(findDeudaMoratoria(bean.getIdMoratoria(), "1", 0));
        // bean.setDeudaNueva(findDeudaMoratoria(bean.getIdMoratoria(),
        // "2", 502));
        bean.setDeudaNueva(findDeudaMoratoria(bean.getIdMoratoria(), "2", bean.getCodMoratoria()));
      }
    } catch (EmptyResultDataAccessException e) {
      logger.warn("moratoria no encontrada {}, {}/{} ", sistema, cuenta, dv);
      return null;
    }
    return bean;
  }

  @Override
  public List<MoratoriaBean> findMoratorias(String sistema, String cuenta, String dv) {
    List<MoratoriaBean> moratorias;
    try {
      moratorias = this.jdbcTemplate.query(SELECT_MORATORIAS, new Object[] {sistema, cuenta, dv},
          new MoratoriaRowMapper());

      /*
       * Cargo la deuda original y la generada para cada moratoria
       */
      for (MoratoriaBean moratoria : moratorias) {
        moratoria.setDeudaOriginal(findDeudaMoratoria(moratoria.getIdMoratoria(), "1", 0));
        // moratoria.setDeudaNueva(findDeudaMoratoria(moratoria.getIdMoratoria(),
        // "2", 502));
        moratoria.setDeudaNueva(
            findDeudaMoratoria(moratoria.getIdMoratoria(), "2", moratoria.getCodMoratoria()));
      }
    } catch (EmptyResultDataAccessException e) {
      logger.warn("moratoria no encontrada {}, {}/{} ", sistema, cuenta, dv);
      return null;
    }
    return moratorias;
  }

  public List<DeudaBean> findDeudaMoratoria(Integer idAdhesion, String marca, Integer mora) {
    try {
      // return this.jdbcTemplate.query(SELECT_MORATORIA_DEUDA, new Object[] { idAdhesion, marca,
      // mora },
      return this.jdbcTemplate.query(SELECT_MORATORIA_DEUDA, new Object[] {idAdhesion, marca},
          new DeudaMoratoriaRowMapper());
    } catch (EmptyResultDataAccessException e) {
      // logger.warn("deuda no encontrada adhesion {}, marca {}, mora {}", idAdhesion, marca, mora);
      logger.warn("deuda no encontrada adhesion {}, marca {}", idAdhesion, marca);
      return null;
    }
  }

  public void updateMoratoria(MoratoriaBean moratoria, String estado) {
    this.jdbcTemplate.update(UPDATE_MORATORIA, new Object[] {estado, moratoria.getIdMoratoria()},
        new int[] {Types.VARCHAR, Types.INTEGER});
  }

  public void updateDeudaMoratoria(DeudaBean deuda) {
    this.jdbcTemplate.update(
        UPDATE_DEUDA_MORATORIA, new Object[] {deuda.getIntereses(), deuda.getFechaVto(),
            deuda.getIdMoratoria(), deuda.getAnio(), deuda.getCuota()},
        new int[] {Types.NUMERIC, Types.DATE, Types.INTEGER, Types.INTEGER, Types.INTEGER});
  }

}
