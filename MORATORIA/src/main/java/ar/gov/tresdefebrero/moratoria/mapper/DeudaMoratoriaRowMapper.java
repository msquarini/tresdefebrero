package ar.gov.tresdefebrero.moratoria.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import ar.gov.tresdefebrero.moratoria.beans.DeudaBean;

/**
 * Mapeo de tabla moratoria_deuda
 * 
 * @author msquarini
 *
 */
public class DeudaMoratoriaRowMapper implements RowMapper<DeudaBean> {

	@Override
	public DeudaBean mapRow(ResultSet rs, int rowNum) throws SQLException {
		DeudaBean bean = new DeudaBean();		
		bean.setIdMoratoria(Integer.valueOf(rs.getInt("id_adhesion")));
		bean.setMora(rs.getInt("mora"));
		bean.setAnio(rs.getInt("anio"));
		bean.setCuota(rs.getInt("cuota"));
		bean.setImporte(rs.getBigDecimal("importe"));
		bean.setMarca(rs.getString("marca"));
		bean.setFechaVto(rs.getDate("fecha_vto"));
		bean.setAjuste(rs.getString("ajuste"));
		bean.setDebcre(rs.getString("debcre"));
		bean.setCodAdm(rs.getInt("cod_adm"));
		
		bean.setAnioCuotaBarra();
		
		// 20180609 MS - ajuste para generar deudas con intereses por pago fuera de termino
		bean.setFechaPago(rs.getDate("fecha_pago"));
		bean.setFechaVto2(rs.getDate("fecha_vto2"));
		bean.setIntereses(rs.getBigDecimal("intereses"));
		return bean;
	}
}
