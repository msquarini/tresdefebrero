package ar.gov.tresdefebrero.moratoria.beans;

public abstract class AbstractCuenta implements Cuenta {

  private String cuit;
  private Integer enCaracter;

  public String getCuit() {
    return cuit;
  }

  public void setCuit(String cuit) {
    this.cuit = cuit;
  }

  public Integer getEnCaracter() {
    return enCaracter;
  }

  public void setEnCaracter(Integer enCaracter) {
    this.enCaracter = enCaracter;
  }


}
