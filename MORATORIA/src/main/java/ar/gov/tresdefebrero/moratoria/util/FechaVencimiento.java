package ar.gov.tresdefebrero.moratoria.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.time.DateUtils;

public class FechaVencimiento {

	private static Map<String, Date> fechas = new HashMap<String, Date>();

	static {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try {
			fechas.put("20111", sdf.parse("2011-04-01"));
			fechas.put("20112", sdf.parse("2011-06-01"));
			fechas.put("20113", sdf.parse("2011-08-01"));
			fechas.put("20114", sdf.parse("2011-10-03"));
			fechas.put("20115", sdf.parse("2011-12-01"));
			fechas.put("20116", sdf.parse("2012-02-01"));
			fechas.put("20121", sdf.parse("2012-04-03"));
			fechas.put("20122", sdf.parse("2012-06-01"));
			fechas.put("20123", sdf.parse("2012-08-01"));
			fechas.put("20124", sdf.parse("2012-10-01"));
			fechas.put("20125", sdf.parse("2012-12-03"));
			fechas.put("20126", sdf.parse("2013-02-01"));
			fechas.put("20131", sdf.parse("2013-04-03"));
			fechas.put("20132", sdf.parse("2013-06-03"));
			fechas.put("20133", sdf.parse("2013-08-01"));
			fechas.put("20134", sdf.parse("2013-10-01"));
			fechas.put("20135", sdf.parse("2013-12-02"));
			fechas.put("20136", sdf.parse("2014-02-03"));
			fechas.put("20141", sdf.parse("2014-04-01"));
			fechas.put("20142", sdf.parse("2014-06-02"));
			fechas.put("20143", sdf.parse("2014-08-01"));
			fechas.put("20144", sdf.parse("2014-10-01"));
			fechas.put("20145", sdf.parse("2014-12-01"));
			fechas.put("20146", sdf.parse("2015-02-02"));
			fechas.put("20151", sdf.parse("2015-04-01"));
			fechas.put("20152", sdf.parse("2015-06-01"));
			fechas.put("20153", sdf.parse("2015-08-03"));
			fechas.put("20154", sdf.parse("2015-10-01"));
			fechas.put("20155", sdf.parse("2015-12-01"));
			fechas.put("20156", sdf.parse("2016-02-01"));
			fechas.put("20161", sdf.parse("2016-04-01"));
			fechas.put("20162", sdf.parse("2016-06-01"));
			fechas.put("20163", sdf.parse("2016-08-01"));
			fechas.put("20164", sdf.parse("2016-10-03"));
			fechas.put("20165", sdf.parse("2016-12-01"));
			fechas.put("20166", sdf.parse("2017-02-01"));
			fechas.put("20171", sdf.parse("2017-04-01"));
			fechas.put("20172", sdf.parse("2017-06-01"));
			fechas.put("20173", sdf.parse("2017-08-01"));
			fechas.put("20174", sdf.parse("2017-10-01"));
			fechas.put("20175", sdf.parse("2017-12-01"));
			fechas.put("20176", sdf.parse("2018-02-01"));
			fechas.put("20181", sdf.parse("2018-04-01"));
			fechas.put("20182", sdf.parse("2018-06-01"));
			fechas.put("20183", sdf.parse("2018-08-01"));
			fechas.put("20184", sdf.parse("2018-10-01"));
			fechas.put("20185", sdf.parse("2018-12-01"));
			fechas.put("20186", sdf.parse("2019-02-01"));

		
		} catch (Exception e) {
			System.err.println("Error inicializacion fechas");
		}
	}

	public static Date getFecha(Integer anio, Integer cuota) {
		return fechas.get(String.valueOf(anio) + String.valueOf(cuota));
	}

	/*
	 * public static Date proximoHabil(Date desde) { LocalDate proximo =
	 * desde.toInstant().atZone(ZoneId.systemDefault()).toLocalDate(); //proximo =
	 * proximo.plusDays(days); java.time.DayOfWeek dayOfWeek =
	 * proximo.getDayOfWeek(); switch (dayOfWeek) { case SUNDAY: proximo =
	 * proximo.plusDays(1); break; case SATURDAY: proximo = proximo.plus(2,
	 * ChronoUnit.DAYS); break; default: break; }; return
	 * Date.from(proximo.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
	 * }
	 * 
	 * public static Date fecha(Integer anio, Integer mes, Integer dia) { LocalDate
	 * fecha = LocalDate.of(anio, mes, dia); return
	 * Date.from(fecha.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()); }
	 */

	public static Date proximoHabil(Date desde) {
		Date proximo = desde;
		Calendar c = Calendar.getInstance();
		c.setTime(desde);

		if (c.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
			proximo = DateUtils.addDays(desde, 2);
		} else if (c.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
			proximo = DateUtils.addDays(desde, 1);
		}

		if (isFeriado(proximo)) {
			proximo = proximoHabil(DateUtils.addDays(proximo, 1));
		}
		return proximo;
	}

	public static boolean isFeriado(Date fecha) {
		Calendar c = Calendar.getInstance();
		c.setTime(fecha);
		if ((c.get(Calendar.DATE) == 1) && (c.get(Calendar.MONTH) == 0))
			return true;
		if ((c.get(Calendar.DATE) == 31) && (c.get(Calendar.MONTH) == 11))
			return true;
		return false;
	}

	public static Date fecha(Integer anio, Integer mes, Integer dia) {
		Calendar c = Calendar.getInstance();
		c.set(Calendar.YEAR, anio);
		c.set(Calendar.MONTH, mes - 1);
		c.set(Calendar.DATE, dia);

		return c.getTime();
	}
}
