package ar.gov.tresdefebrero.moratoria.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import ar.gov.tresdefebrero.moratoria.beans.ABLBean;
import ar.gov.tresdefebrero.moratoria.beans.AutomotorBean;

/**
 * Mapeo de tabla padron_tsg
 * 
 * @author msquarini
 *
 */
public class ABLRowMapper implements RowMapper<ABLBean> {

	@Override
	public ABLBean mapRow(ResultSet rs, int rowNum) throws SQLException {
		ABLBean abl = new ABLBean();
		
		abl.setCuenta(rs.getInt("cuenta"));
		abl.setDv(rs.getString("digito"));
		
		//auto.setNomenclatura();
		
		abl.setCalle(rs.getString("nombrecalle"));
		abl.setNumero(rs.getString("nropuerta"));
		abl.setPiso(rs.getString("piso"));
		abl.setDepartamento(rs.getString("dpto"));
		abl.setCodigoPostal(rs.getString("localidad"));
		abl.setLocalidad(rs.getString("localidad_desc"));
		abl.setTitular(rs.getString("appynom"));
		
		//abl.setCategoria(rs.getInt("categoria"));
		//abl.setValuacionTotal(rs.getBigDecimal("vtotal"));
		
		abl.setNomenclatura(NomenclaturaMapper.map(rs));
		return abl;
	}

}
