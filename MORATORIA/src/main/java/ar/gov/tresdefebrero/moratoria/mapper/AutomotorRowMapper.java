package ar.gov.tresdefebrero.moratoria.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import ar.gov.tresdefebrero.moratoria.beans.AutomotorBean;

/**
 * Mapeo de tabla padron_auto
 * 
 * @author msquarini
 *
 */
public class AutomotorRowMapper implements RowMapper<AutomotorBean> {

	@Override
	public AutomotorBean mapRow(ResultSet rs, int rowNum) throws SQLException {
		AutomotorBean auto = new AutomotorBean();
		
		auto.setPatente(rs.getString("patente"));
		
		auto.setAutog(rs.getInt("autog"));
		//auto.setNomenclatura();

		auto.setDominioOriginal(rs.getString("dominio_original"));
		auto.setDominioActual(rs.getString("dominio_actual"));

		auto.setTitular(rs.getString("apellido_nombre"));
		auto.setCalle(rs.getString("nombre_calle_pate"));
		auto.setNro(rs.getInt("nro_puerta_pate"));
		
		auto.setCodigoPostal(rs.getString("localidad_pate"));
		auto.setLocalidad(rs.getString("localidad"));
		
		auto.setDescripcionMarca(rs.getString("descrip_marca_pate"));
		auto.setDescripcionModelo(rs.getString("descrip_modelo_pate"));
		
		auto.setCategoria(rs.getString("categoria_pate"));
		auto.setCodigoTipo(rs.getString("tipo_patente"));
		auto.setModeloAnio(rs.getInt("modelo_anio"));
		
		auto.setNomenclatura(NomenclaturaMapper.map(rs));
		return auto;
	}

}
