package ar.gov.tresdefebrero.moratoria.beans;

import java.math.BigDecimal;

import ar.gov.tresdefebrero.moratoria.util.FechaVencimiento;

/**
 * Registro de deuda TISH
 * 
 * @author msquarini
 *
 */
public class DeudaTishBean extends DeudaBean {

  private BigDecimal minimo;

  private Boolean presentaPago;

  public DeudaTishBean() {
    this.presentaPago = false;
    this.setSistema("IC");
  }

  /**
   * Constructor a partir de registro de deuda
   * @param d
   */
/*  public DeudaTishBean(DeudaBean d) {
    this.presentaPago = false;
    this.importe = d.getImporte();
    this.minimo = d.getImporte();
    this.debcre = d.getDebcre();
    this.mora = d.getMora();
    this.marca = d.getMarca();
    this.ajuste = d.getAjuste();
    this.codAdm = d.getCodAdm();
    this.setSistema("IC");
    this.cuota = d.getCuota();
    this.anio = d.getAnio();
    this.cuenta = d.getCuenta();
    this.dv = d.getDv();
    this.fechaVto = d.getFechaVto();
  }
*/
  /**
   * 
   * @param anio
   * @param cuota
   * @param cuenta
   * @param dv
   */
  public DeudaTishBean(Integer anio, Integer cuota, String cuenta, String dv, String ajuste) {
    this.presentaPago = true;
    this.importe = BigDecimal.ZERO;
    this.minimo = BigDecimal.ZERO;
    this.debcre = "D";
    this.mora = 0;
    this.marca = "0";
    this.ajuste = ajuste;
    this.codAdm = Integer.valueOf(0);
    this.setSistema("IC");
    this.cuota = cuota;
    this.anio = anio;
    this.cuenta = cuenta;
    this.dv = dv;
    this.fechaVto = FechaVencimiento.getFecha(this.anio, this.cuota);
  }

  public Boolean getPresentaPago() {
    return presentaPago;
  }

  public void setPresentaPago(Boolean presentaPago) {
    this.presentaPago = presentaPago;
  }

  public BigDecimal getMinimo() {
    return minimo;
  }

  public void setMinimo(BigDecimal minimo) {
    this.minimo = minimo;
  }
}
