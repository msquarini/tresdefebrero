package ar.gov.tresdefebrero.moratoria.actions;

import java.util.Map;

import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.SessionAware;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;

import ar.gov.tresdefebrero.moratoria.beans.Titular;
import ar.gov.tresdefebrero.moratoria.dao.ConsultaDao;

/**
 * Busqueda de cuit
 * 
 * @author msquarini
 *
 */
public class CuitAction extends ActionSupport implements SessionAware {

  private static final long serialVersionUID = 7353477345330099518L;

  private static final Logger logger = LoggerFactory.getLogger("CuitAction");

  private Map<String, Object> session;

  @Autowired
  private ConsultaDao dao;
    
  private String cuit;

  public CuitAction() {
    // ApplicationContext springContext = ApplicationContextHelper.getApplicationContext();
    //this.dao = ((ConsultaDao) springContext.getBean("jdbcConsultaDao"));
    //this.manager = ((MoratoriaManager) springContext.getBean("moratoriaManager"));
  }

  public Map<String, Object> getSession() {
    return this.session;
  }

  public void setSession(Map<String, Object> session) {
    this.session = session;
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.opensymphony.xwork2.ActionSupport#input()
   */
  public String input() {
    logger.info("moratoria 2018 - input");

    invalidarSession();

    return "input";
  }

  /**
   * Busqueda de CUIT
   * 
   * @return
   */
  public String search() {
    logger.info("moratoria 2018 - busqueda de cuit {}", this.cuit);
    Titular titular = null;

    try {
      if ((this.cuit == null) || (this.cuit.equals(""))) {
        addFieldError("cuit", getText("cuit.invalida"));
        logger.info("moratoria 2018- cuit invalida {}", this.cuit);
        return "input";
      }

      //titular = this.dao.findCuit(this.cuit);
      if (titular == null) {
        addFieldError("cuit", getText("cuit.invalida"));
        logger.info("moratoria 2018- cuit invalida {}", this.cuit);
        return "input";
      }
    } catch (Exception e) {
      logger.error("error en busqueda de cuit", e);
      addActionError("Error en la busqueda de cuit, intente nuevamente.");
      return "input";
    }
    getSession().put("titular", titular);
    return "moratorio2018_input";
  }

  /**
   * Remueve datos de session y la invalida
   */
  private void invalidarSession() {
    getSession().remove("cuenta");
    getSession().remove("deudas");
    getSession().remove("moratoria");
    ((SessionMap) getSession()).invalidate();
  }

}