package ar.gov.tresdefebrero.moratoria.beans;

/**
 * Deuda de impuestos
 * 
 * @author admin
 *
 */
public class AutomotorBean extends AbstractCuenta implements Cuenta {

	/**
	 * Cuenta + DV --> identifica el impuesto
	 * Formato: AAA111
	 */
	private String patente;

	/**
	 * Valor utilizado en la generacion de codigo de barras en lugar de
	 * cuenta/patente
	 */
	private Integer autog;
	/**
	 * 
	 */
	private Nomenclatura nomenclatura;

	/**
	 * 
	 */
	private String dominioOriginal;

	/**
	 * 
	 */
	private String dominioActual;

	private String descripcionMarca;
	private String descripcionModelo;
	private Integer modeloAnio;

	private String categoria;
	private String codigoTipo;

	private String titular;

	/**
	 * Domicilio radicacion del vehiculo
	 */
	private String calle;
	private Integer nro;
	private String codigoPostal;
	private String localidad;

	public String getPatente() {
		return patente;
	}

	public void setPatente(String patente) {
		//this.patente = patente;
		// Eliminamos el 0 al comienzo de la patente
		this.patente = patente.substring(1);
	}

	public Nomenclatura getNomenclatura() {
		return nomenclatura;
	}

	public void setNomenclatura(Nomenclatura nomenclatura) {
		this.nomenclatura = nomenclatura;
	}

	public String getDominioOriginal() {
		return dominioOriginal;
	}

	public void setDominioOriginal(String dominioOriginal) {
		this.dominioOriginal = dominioOriginal;
	}

	public String getDominioActual() {
		return dominioActual;
	}

	public void setDominioActual(String dominioActual) {
		this.dominioActual = dominioActual;
	}

	public String getDescripcionMarca() {
		return descripcionMarca;
	}

	public void setDescripcionMarca(String descripcionMarca) {
		this.descripcionMarca = descripcionMarca;
	}

	@Override
	public String getCuenta() {
		//return patente.substring(0,5);
		return patente;
	}

	@Override
	public String getDv() {
		//return patente.substring(5);
		return "";
	}

	@Override
	public String getImpuesto() {
		return "XP";
	}

	public String getDescripcionModelo() {
		return descripcionModelo;
	}

	public void setDescripcionModelo(String descripcionModelo) {
		this.descripcionModelo = descripcionModelo;
	}

	public String getTitular() {
		return titular;
	}

	public void setTitular(String titular) {
		this.titular = titular;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public Integer getNro() {
		return nro;
	}

	public void setNro(Integer nro) {
		this.nro = nro;
	}

	public String getLocalidad() {
		return localidad;
	}

	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}

	public String getCodigoPostal() {
		return codigoPostal;
	}

	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	public Integer getModeloAnio() {
		return modeloAnio;
	}

	public void setModeloAnio(Integer modeloAnio) {
		this.modeloAnio = modeloAnio;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public Integer getAutog() {
		return autog;
	}

	public void setAutog(Integer autog) {
		this.autog = autog;
	}

	@Override
	public String getCuentaBarcode() {
		return getAutog().toString().substring(0,6);
	}

	public String getCodigoTipo() {
		return codigoTipo;
	}

	public void setCodigoTipo(String codigoTipo) {
		this.codigoTipo = codigoTipo;
	}

  @Override
  public String getDvBarcode() {
    return getAutog().toString().substring(6);
  }

}
