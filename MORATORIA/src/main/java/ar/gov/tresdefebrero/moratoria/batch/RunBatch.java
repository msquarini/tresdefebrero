package ar.gov.tresdefebrero.moratoria.batch;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;

/**
 * Ejecucion del job de generacion de file con novedades de adhesiones del dia
 * 
 * @author msquarini
 *
 */
public class RunBatch {

  private static final Logger logger = LoggerFactory.getLogger("RunBatch");

  private JobLauncher jobLauncher;
  private Job job;
  private Integer codMoratoria;

  /**
   * Path de salida de archivos generados
   */
  private String outputPath;

  public void run() {
    try {
      SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");

      /*
       * Nombre del file de salida con el sufijo YYYYMMDD
       */
      String fileLocation = outputPath + "moratoria" + codMoratoria + "-" + sdf.format(new Date());

      JobParameters param = new JobParametersBuilder().addDate("fecha", new Date())
          .addString("estado", "A").addLong("moratoria", codMoratoria.longValue())
          .addString("fileLocation", fileLocation).toJobParameters();

      JobExecution execution = jobLauncher.run(job, param);

      logger.info("Exit Status : " + execution.getStatus());

    } catch (Exception e) {
      logger.error("Error en generacion de file para mainframe", e);
    }
  }

  public JobLauncher getJobLauncher() {
    return jobLauncher;
  }

  public void setJobLauncher(JobLauncher jobLauncher) {
    this.jobLauncher = jobLauncher;
  }

  public Job getJob() {
    return job;
  }

  public void setJob(Job job) {
    this.job = job;
  }

  public String getOutputPath() {
    return outputPath;
  }

  public void setOutputPath(String outputPath) {
    this.outputPath = outputPath;
  }

  public Integer getCodMoratoria() {
    return codMoratoria;
  }

  public void setCodMoratoria(Integer codMoratoria) {
    this.codMoratoria = codMoratoria;
  }
}
