package ar.gov.tresdefebrero.moratoria.beans;

/**
 * Deuda de impuestos
 * 
 * @author admin
 *
 */
public class RCBean extends ABLBean {

  @Override
  public String getImpuesto() {
      return "RC";
  }
}
