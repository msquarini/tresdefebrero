package ar.gov.tresdefebrero.moratoria.dao;

import ar.gov.tresdefebrero.moratoria.beans.Cuenta;
import ar.gov.tresdefebrero.moratoria.beans.DeudaBean;
import ar.gov.tresdefebrero.moratoria.beans.PlanPago;
import java.util.List;

public abstract interface ConsultaDao {
  // public abstract List<DeudaBean> findDeuda(Integer paramInteger, String paramString1, String
  // paramString2,
  // String paramString3);

  public List<DeudaBean> findDeuda(Cuenta cuentaBean, String tipoDeuda);

  public abstract Cuenta findCuenta(Integer paramInteger, String paramString1, String paramString2,
      String paramString3);

  public abstract List<PlanPago> findPlanesPago(String tipoDeuda);

  public boolean checkAdhesionMoratoria(Cuenta cuentaBean, Integer moratoria);

  /**
   * Valida si existe deuda de moratoria 502
   * 
   * @param impuesto
   * @param cuenta
   * @param dv
   * @param patente
   * @return
   */
  // public boolean checkDeudaMoratoria(Integer impuesto, String cuenta, String dv, String patente,
  // Integer moratoria);

  /**
   * Verifica si existe adhesion a moratoria vigente
   * 
   * @param impuesto
   * @param cuenta
   * @param dv
   * @param patente
   * @param moratoria
   * @return
   */
  // public boolean checkAdhesionMoratoria(Integer impuesto, String cuenta, String dv, String
  // patente, Integer moratoria);


  /**
   * Verifica si no existe moratoria 504 y tenga paga la 503
   * 
   * @param impuesto
   * @param cuenta
   * @param dv
   * @param patente
   * @param moratoria
   * @return
   */
  // public boolean checkAdhesionMoratoria504(Integer impuesto, String cuenta, String dv, String
  // patente, Integer moratoria);
}
