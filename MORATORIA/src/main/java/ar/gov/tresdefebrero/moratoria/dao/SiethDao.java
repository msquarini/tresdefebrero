package ar.gov.tresdefebrero.moratoria.dao;

import java.util.List;

import ar.gov.tresdefebrero.moratoria.beans.Cuenta;
import ar.gov.tresdefebrero.moratoria.beans.SiethCuenta;

public abstract interface SiethDao {

  public List<SiethCuenta> findCuentas(String cuit);
  
}
