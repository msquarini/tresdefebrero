package ar.gov.tresdefebrero.moratoria.beans;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PlanPago {

  private static final Logger logger = LoggerFactory.getLogger("PlanPago");

  private Integer idPlanPago;
  private String descripcion;
  private Double porcentajeEfectivo;
  private Integer cuotasDesde;
  private Integer cuotasHasta;
  private Double interesFinanciero;
  private Double interesMora;

  private String moratoriaNombre;
  private String moratoriaTipo;

  public MontoMoratoria calculateDeuda(List<DeudaBean> deudas, Integer cuotas) {
    BigDecimal acumuladoConMora = BigDecimal.ZERO;
    BigDecimal acumuladoCuotaOriginal = BigDecimal.ZERO;

    BigDecimal acumuladoMontoEfectivo = BigDecimal.ZERO;

    for (DeudaBean d : deudas) {

      // Si es D sumo la deuda con el calculo de intereses x mora
      if (d.getDebcre().equalsIgnoreCase("D")) {
        acumuladoCuotaOriginal = acumuladoCuotaOriginal.add(d.getImporte());
        // acumuladoCuotaOriginal =
        // acumuladoCuotaOriginal.add(d.getImporteAPagar());

        // Monto a cuotificar (% de la deuda que no va en efectivo)
        BigDecimal montoACuotificar =
            d.getImporte().multiply(BigDecimal.valueOf(100 - getPorcentajeEfectivo()))
                .divide(BigDecimal.valueOf(100));

        BigDecimal montoEfectivo = d.getImporte().subtract(montoACuotificar);

        // Calculo el interes de mora desde la fecha de vencimiento a
        // hoy
        acumuladoConMora =
            acumuladoConMora.add(aplicarInteresMora(montoACuotificar, d.getFechaVto()));
        acumuladoMontoEfectivo =
            acumuladoMontoEfectivo.add(aplicarInteresMora(montoEfectivo, d.getFechaVto()));
        /*
         * } else { acumuladoCuotaOriginal = acumuladoCuotaOriginal.subtract(d.getImporte());
         * 
         * acumuladoConMora = acumuladoConMora.subtract(d.getImporte()
         * .multiply(BigDecimal.valueOf(100 -
         * getPorcentajeEfectivo())).divide(BigDecimal.valueOf(100)));
         */
      }

    }
    logger.info("Deuda ORIGINAL: {}", acumuladoCuotaOriginal);
    logger.info("Deuda en cuotas con % MORA incluido: {}", acumuladoConMora);
    logger.info("Deuda en efectivo con % MORA incluido: {}", acumuladoMontoEfectivo);

    // TOTAL a pagar en efectivo
    /*
     * BigDecimal efectivo = acumuladoCuotaOriginal.multiply(BigDecimal.valueOf(porcentajeEfectivo))
     * .divide(BigDecimal.valueOf(100));
     */


    // Ya tengo el total de deuda con % de mora, aplicar el interes
    // financiero y obtiene el valor de la cuota
    BigDecimal montoCuota = BigDecimal.ZERO;
    if (cuotas > 0) {
      // montoCuota = aplicarInteresFinanciero(acumuladoConMora, cuotas);
      montoCuota = aplicarInteresFinancieroSimple(acumuladoConMora, cuotas);

      logger.info("Monto cuota aplicado intereses financieros $ {}", montoCuota);

    }
    return new MontoMoratoria(acumuladoMontoEfectivo, montoCuota, acumuladoCuotaOriginal);
  }

  /**
   * Aplicamos el interes por MORA configurado en el plan de pagos. Se calculo desde la fecha
   * original de pago de la cuota adeudada a la fecha actual
   * 
   * Si se quiere bonificar se debe cargar en el plan de pagos el indice ya ajustado!
   * 
   * @param valor
   * @param desde
   * @return
   */
  private BigDecimal aplicarInteresMora(BigDecimal valor, Date desde) {
    Calendar now = Calendar.getInstance();

    // Fecha vencimiento del periodo seleccionado
    Calendar calDesde = Calendar.getInstance();
    calDesde.setTime(desde);

    // Calculo de meses de diferencia para aplicar interes
    int diffYear = now.get(Calendar.YEAR) - calDesde.get(Calendar.YEAR);
    int diffMonth = diffYear * 12 + now.get(Calendar.MONTH) - calDesde.get(Calendar.MONTH);

    // Agregamos el interes mensual
    return valor.add(((valor.multiply(BigDecimal.valueOf(getInteresMora() * diffMonth))
        .divide(BigDecimal.valueOf(100))))).setScale(2, java.math.RoundingMode.HALF_UP);

  }

  /**
   * Cuota= D/n + {(n*i / 2)* [2D – (D/n)*(n- 1)]}/n
   * 
   * D: Deuda a regularizar (original + intereses – bonif si correspondiera) n: cantidad de cuotas
   * (24 para el caso) i: tasa de interés (1% actualmente)
   * 
   * @param valor
   * @param cuotas
   * @return
   */
  private BigDecimal aplicarInteresFinancieroSimple(BigDecimal valor, Integer cuotas) {

    BigDecimal n = new BigDecimal(cuotas);
    BigDecimal partOne = valor.divide(n, 2, java.math.RoundingMode.HALF_UP);
    BigDecimal partTwo =
        n.multiply(BigDecimal.valueOf(getInteresFinanciero() / 100)).divide(BigDecimal.valueOf(2L));
    BigDecimal partThree = valor.multiply(BigDecimal.valueOf(2L))
        .subtract((partOne.multiply(n.subtract(BigDecimal.ONE))));

    BigDecimal result = partOne.add(partTwo.multiply(partThree).divide(n)).setScale(2,
        java.math.RoundingMode.HALF_UP);
    
    logger.info("calculando intereses financiero simple, monto $ {} en {} cuotas, valor cuota {}",
        valor, cuotas, result);
    return result;
  }

  /**
   * Calcula el valor de la cuota aplicado el interes financiero al monto a cuotificar
   * 
   * @param valor
   * @param cuotas
   * @return
   */
  private BigDecimal aplicarInteresFinanciero(BigDecimal valor, Integer cuotas) {
    logger.info("calculando intereses financiero a $ {} en {} cuotas", valor, cuotas);

    /*
     * Pasar a TEM la tasa de interes anual
     */
    Double t = Double.valueOf(1 + getInteresFinanciero() / 100);
    // 1/12 --> 0.083333
    double tasa = Math.pow(t, 0.083333333) - 1;

    // (1+i)^cuotas
    double tasamasuno = Math.pow((1 + tasa), cuotas);
    // C * ( tasa * tasamasuno) / (tasamasuno - 1)

    // double intermedio = tasa * tasamasuno / (tasamasuno - 1);
    // return valor.multiply(BigDecimal.valueOf(intermedio)).setScale(2,
    // java.math.RoundingMode.HALF_UP);

    double intermedio = (valor.doubleValue() * tasamasuno) / cuotas;
    return BigDecimal.valueOf(intermedio).setScale(2, java.math.RoundingMode.HALF_UP);

  }

  public Integer getIdPlanPago() {
    return idPlanPago;
  }

  public void setIdPlanPago(Integer idPlanPago) {
    this.idPlanPago = idPlanPago;
  }

  public Double getPorcentajeEfectivo() {
    return porcentajeEfectivo;
  }

  public void setPorcentajeEfectivo(Double porcentajeEfectivo) {
    this.porcentajeEfectivo = porcentajeEfectivo;
  }

  public Integer getCuotasDesde() {
    return cuotasDesde;
  }

  public void setCuotasDesde(Integer cuotasDesde) {
    this.cuotasDesde = cuotasDesde;
  }

  public Integer getCuotasHasta() {
    return cuotasHasta;
  }

  public void setCuotasHasta(Integer cuotasHasta) {
    this.cuotasHasta = cuotasHasta;
  }

  public Double getInteresFinanciero() {
    return interesFinanciero;
  }

  public void setInteresFinanciero(Double interesFinanciero) {
    this.interesFinanciero = interesFinanciero;
  }

  public Double getInteresMora() {
    return interesMora;
  }

  public void setInteresMora(Double interesMora) {
    this.interesMora = interesMora;
  }

  public String getDescripcion() {
    return descripcion;
  }

  public void setDescripcion(String descripcion) {
    this.descripcion = descripcion;
  }

  public String getMoratoriaNombre() {
    return moratoriaNombre;
  }

  public void setMoratoriaNombre(String moratoriaNombre) {
    this.moratoriaNombre = moratoriaNombre;
  }

  public String getMoratoriaTipo() {
    return moratoriaTipo;
  }

  public void setMoratoriaTipo(String moratoriaTipo) {
    this.moratoriaTipo = moratoriaTipo;
  }

  public String getTipo() {
    String result = "";
    switch (moratoriaTipo) {
      case "P":
        result = "Prejudicial";
        break;
      case "J":
        result = "Judicializada";
        break;
      case "F":
        result = "Fiscalizada";
        break;
      default:
        break;
    }
    return result;
  }
}
