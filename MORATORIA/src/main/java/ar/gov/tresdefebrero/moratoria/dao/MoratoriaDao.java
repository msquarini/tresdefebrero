package ar.gov.tresdefebrero.moratoria.dao;

import java.util.List;

import ar.gov.tresdefebrero.moratoria.beans.Cuenta;
import ar.gov.tresdefebrero.moratoria.beans.DeudaBean;
import ar.gov.tresdefebrero.moratoria.beans.MoratoriaBean;

/**
 * DAO para gestion de moratoria
 * 
 * @author mesquarini
 *
 */
public interface MoratoriaDao {

  public MoratoriaBean findMoratoria(String sistema, String cuenta, String dv,
      Integer codMoratoria);

  /**
   * 20170513 Consulta de moratorias, al existir nueva moratoria, una cuenta puede tener mas de una
   * adhesion
   * 
   * @param sistema
   * @param cuenta
   * @param dv
   * @return
   */
  public List<MoratoriaBean> findMoratorias(String sistema, String cuenta, String dv);

  public void crearDeudaMoratoria(Integer idMoratoria, List<DeudaBean> deuda, String marca,
      Integer mora);

  public Integer adhesionMoratoria(MoratoriaBean moratoria);

  public List<DeudaBean> findDeudaMoratoria(Integer idAdhesion, String marca, Integer mora);

  public void updateMoratoria(MoratoriaBean paramMoratoriaBean, String estado);
  
  /**
   * Actualizar intereses y fecha de vencimiento para deuda que no registra pago y vencio
   * @param deuda
   */
  public void updateDeudaMoratoria(DeudaBean deuda);
}
