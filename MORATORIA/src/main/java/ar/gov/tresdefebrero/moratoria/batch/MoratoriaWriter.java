package ar.gov.tresdefebrero.moratoria.batch;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;

import ar.gov.tresdefebrero.moratoria.beans.DeudaBean;
import ar.gov.tresdefebrero.moratoria.beans.MoratoriaBean;
import ar.gov.tresdefebrero.moratoria.dao.MoratoriaDao;

/**
 * Genera el file para mainframe y actualiza la base de datos de los registros
 * de moratoria procesados
 * 
 * @author msquarini
 *
 */
public class MoratoriaWriter implements ItemWriter<MoratoriaBean> {

	private static final Logger logger = LoggerFactory.getLogger("MoratoriaWriter");

	private ItemWriter<DeudaBean> delegate;
	private MoratoriaDao dao;

	@Override
	public void write(List<? extends MoratoriaBean> items) throws Exception {
		logger.info("Escribiendo {} cuentas...", items.size());
		try {
			for (MoratoriaBean item : items) {
				List<DeudaBean> deuda = item.getDeudaMoratoria();
				logger.info("Generando registros de cuenta {}, cantidad de registros de deuda {}", item.getCuenta(),
						deuda.size());
				delegate.write(deuda);

				logger.info("Actualizando estado de cuenta {}, sistema {} a Procesado", item.getCuenta(),
						item.getSistema());
				dao.updateMoratoria(item, "P");
			}
		} catch (Exception e) {
			logger.error("Error en writer, ", e);
			throw e;
		}
	}

	public ItemWriter<DeudaBean> getDelegate() {
		return delegate;
	}

	public void setDelegate(ItemWriter<DeudaBean> delegate) {
		this.delegate = delegate;
	}

	public MoratoriaDao getDao() {
		return dao;
	}

	public void setDao(MoratoriaDao dao) {
		this.dao = dao;
	}
}