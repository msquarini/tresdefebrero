package ar.gov.tresdefebrero.moratoria.beans;

public class SiethCuenta {

  private String cuenta;
  private String dv;
  private String cuit;
  private String descripcionActividad;
  private String razonSocial;
  private String actividad;

  public String getCuenta() {
    return cuenta;
  }

  public void setCuenta(String cuenta) {
    this.cuenta = cuenta;
  }

  public String getDv() {
    return dv;
  }

  public void setDv(String dv) {
    this.dv = dv;
  }

  public String getCuit() {
    return cuit;
  }

  public void setCuit(String cuit) {
    this.cuit = cuit;
  }

  public String getDescripcionActividad() {
    return descripcionActividad;
  }

  public void setDescripcionActividad(String descripcionActividad) {
    this.descripcionActividad = descripcionActividad;
  }

  public String getRazonSocial() {
    return razonSocial;
  }

  public void setRazonSocial(String razonSocial) {
    this.razonSocial = razonSocial;
  }

  public String getActividad() {
    return actividad;
  }

  public void setActividad(String actividad) {
    this.actividad = actividad;
  }

}
