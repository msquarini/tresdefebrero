package ar.gov.tresdefebrero.moratoria.actions;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.SessionAware;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import com.opensymphony.xwork2.ActionSupport;

import ar.gov.tresdefebrero.moratoria.beans.Cuenta;
import ar.gov.tresdefebrero.moratoria.beans.DeudaBean;
import ar.gov.tresdefebrero.moratoria.beans.DeudaTishBean;
import ar.gov.tresdefebrero.moratoria.beans.MoratoriaBean;
import ar.gov.tresdefebrero.moratoria.beans.PlanPago;
import ar.gov.tresdefebrero.moratoria.beans.SeguridadHigieneBean;
import ar.gov.tresdefebrero.moratoria.beans.SiethCuenta;
import ar.gov.tresdefebrero.moratoria.business.MoratoriaManager;
import ar.gov.tresdefebrero.moratoria.dao.ConsultaDao;
import ar.gov.tresdefebrero.moratoria.dao.MoratoriaDao;
import ar.gov.tresdefebrero.moratoria.util.ApplicationContextHelper;
import sun.rmi.runtime.Log;

/**
 * Consulta de datos de cuentas y cuits para iniciar la adhesion de moratoria
 * 
 * 2018 - Validacion de CUIT
 * 
 * @author msquarini
 *
 */
public class ConsultaAction extends ActionSupport implements SessionAware {

  private static final long serialVersionUID = 7353477345330099518L;

  private static final Logger logger = LoggerFactory.getLogger("ConsultaAction");

  /**
   * Datos de entrada de busqueda de cuenta segun impuesto Patente --> XP o PV Cuenta y DV --> IC o
   * AL
   */
  private String patente;
  private String cuenta;
  private String dv;
  private Integer impuesto;

  private String cuit;
  private Integer caracter;

  private List<SiethCuenta> cuentas;


  /**
   * Deudas existentes de la cuenta
   */
  List<DeudaBean> deudasPre;
  List<DeudaBean> deudasJud;
  List<DeudaBean> deudasFis;

  /**
   * Total de deuda por tipo
   */
  Double totalDeudaPrejudicial;
  Double totalDeudaJudicial;
  Double totalDeudaFiscalizada;

  Boolean presentaDeudaPre;
  Boolean presentaDeudaJud;
  Boolean presentaDeudaFis;

  /**
   * Flag si presenta moratoria
   */
  Integer moratoriaExistente;

  /**
   * 20170513 Inclusion de nueva moratoria Lista de moratorias adheridas
   */
  private List<MoratoriaBean> moratorias;
  private Integer moratoriaId;

  /**
   * Planes de pagos disponibles
   */
  private List<PlanPago> planes;

  private ConsultaDao dao;
  private MoratoriaDao moratoriaDao;
  private MoratoriaManager manager;
  private Map<String, Object> session;
  private InputStream inputStreamA;
  private String fileName;
  
  private Boolean finMoratoria = Boolean.FALSE;

  /**
   * Flag para indicar si presenta deuda de moratoria 502
   */
  private Boolean existsDeudaMoratoria;

  /**
   * Flag para indicar que existe adhesion a moratoria vigente Prejdicial
   */
  private Boolean existsAdhesionMoraVigentePrejudicial;
  private Boolean existsAdhesionMoraVigenteJudicial;
  private Boolean existsAdhesionMoraVigenteFiscalizada;

  public ConsultaAction() {
    ApplicationContext springContext = ApplicationContextHelper.getApplicationContext();
    this.dao = ((ConsultaDao) springContext.getBean("jdbcConsultaDao"));
    this.moratoriaDao = ((MoratoriaDao) springContext.getBean("jdbcMoratoriaDao"));
    this.manager = ((MoratoriaManager) springContext.getBean("moratoriaManager"));
    
    finMoratoria = (Boolean) springContext.getBean("finMoratoria");    
    logger.info("FIN MORATORIA: {}", finMoratoria);
  }

  public Map<String, Object> getSession() {
    return this.session;
  }

  public void setSession(Map<String, Object> session) {
    this.session = session;
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.opensymphony.xwork2.ActionSupport#input()
   */
  public String input() {
	
    logger.info("moratoria 2018 - consulta - input");
    /*
     * Impuesto default ABL
     */
    impuesto = 3;
    invalidarSession();
    
    getSession().put("finMoratoria", finMoratoria);
    return "input";
  }

  public void commonActions(Cuenta bean) {
    presentaDeudaFis = Boolean.FALSE;
    presentaDeudaJud = Boolean.FALSE;
    presentaDeudaPre = Boolean.FALSE;

    // Chequea adhesion a moratoria vigente
    checkAdhesionMoratoria(bean);

    // Busqueda de deuda vigente
    findExisteDeuda(bean);

    /*
     * Busqueda de adhesiones a moratoria existentes para la cuenta
     */
    findMoratorias(bean);

    getSession().put("cuenta", bean);
    getSession().put("moratorias", moratorias);
    getSession().put("presentaDeudaPre", presentaDeudaPre);
    getSession().put("presentaDeudaJud", presentaDeudaJud);
    getSession().put("presentaDeudaFis", presentaDeudaFis);
  }

  public String data() {
    Cuenta bean = (Cuenta) getSession().get("cuenta");
    commonActions(bean);
    return "info";
  }

  /**
   * Busqueda de impuesto por cuenta o patente
   * 
   * @return
   */
  public String search() {
    clearActionErrors();
    logger.info("moratoria 2018 - busqueda de cuenta {}|{} - Impuesto: {}", this.cuenta, this.dv,
        this.impuesto);
    try {
      if ((this.dv == null) || (this.dv.equals(""))) {
        this.dv = "-";
      }

      /*
       * Impuesto 1 y 2, autos y motos validamos solo patente
       */
      if (getImpuesto() < 3 && (this.patente == null || "".equals(this.patente))) {
        // addFieldError("patente", getText("patente.invalida"));
        addActionError(getText("patente.invalida"));
        logger.info("moratoria 2018 - patente invalida {}, impuesto {}", this.cuenta,
            this.impuesto);
        return "input";
      } else if (getImpuesto() == 3 && (this.cuenta == null || this.cuenta.equals(""))) {
        // addFieldError("cuenta", getText("cuenta.invalida"));
        addActionError(getText("cuenta.invalida"));
        logger.info("moratoria 2018 - cuenta invalida {}, impuesto {}", this.cuenta, this.impuesto);
        return "input";
      }

      /**
       * SI es TISH, flujo de accion particular
       */
      if (getImpuesto() == 4) {
        return cuentaTISH();
      }
      Cuenta bean = this.dao.findCuenta(this.impuesto, this.cuenta, this.dv, this.patente);

      if (bean == null) {
        if (this.impuesto < 3) {
          // addFieldError("patente", getText("patente.invalida"));
          addActionError(getText("patente.invalida"));
        } else {
          // addFieldError("cuenta", getText("cuenta.invalida"));
          addActionError(getText("cuenta.invalida"));
        }
        logger.info("moratoria 2018 - cuenta invalida {}|{}, impuesto {}", this.cuenta, this.dv,
            this.impuesto);
        return "input";
      }

      // 2018 Carga de datos del contribuyente
      bean.setCuit(cuit);
      bean.setEnCaracter(getCaracter());

      commonActions(bean);

      if (hasFieldErrors()) {
        return "input";
      }

      /*
       * getSession().put("cuenta", bean); getSession().put("moratorias", moratorias);
       * //getSession().put("deudasPre", deudasPre); getSession().put("presentaDeuda",
       * presentaDeuda);
       */ } catch (Exception e) {
      logger.error("error en busqueda de cuenta", e);
      addActionError("Error en la busqueda de cuenta, intente nuevamente.");
      return "input";
    }
    return "info";
  }


  /**
   * Ingreso por cuenta TISH
   * 
   * @return
   */
  public String searchTish() {
    clearActionErrors();
    logger.info("moratoria 2018 - TISH {}|{} - Impuesto: {}", this.cuenta, this.dv, this.impuesto);
    try {
      Cuenta bean = null;
      // Buscar la cuenta en la lista de cuentas SIETH en session!1!
      List<SiethCuenta> cuentasSieth = (List<SiethCuenta>) getSession().get("cuentas_sieth");
      for (SiethCuenta sc : cuentasSieth) {
        if (sc.getCuenta().equals(cuenta)) {
          bean = new SeguridadHigieneBean(sc);
        }
      }
      if (bean == null) {
        logger.info("moratoria 2018 - error TISH {}, {}", this.cuenta, this.dv);
        return "input";
      }

      // 2018 Carga de datos del contribuyente
      bean.setEnCaracter(getCaracter());

      commonActions(bean);

      /*
       * // Chequea adhesion a moratoria vigente checkAdhesionMoratoria(bean); // Busqueda de deuda
       * vigente findExisteDeuda(bean);
       */

      for (DeudaBean db : this.getDeudasPre()) {
        if (!((DeudaTishBean) db).getPresentaPago()) {
          presentaDeudaPre = true;
          break;
        }
      }
      for (DeudaBean db : this.getDeudasJud()) {
        if (!((DeudaTishBean) db).getPresentaPago()) {
          presentaDeudaJud = true;
          break;
        }
      }
      for (DeudaBean db : this.getDeudasFis()) {
        if (!((DeudaTishBean) db).getPresentaPago()) {
          presentaDeudaFis = true;
          break;
        }
      }

      // TODO: Determinar si existe deuda por cada tipo y si hay adhesion existente!!!
      getSession().put("presentaDeudaPre",
          (presentaDeudaPre && !existsAdhesionMoraVigentePrejudicial));
      getSession().put("presentaDeudaJud",
          (presentaDeudaJud && !existsAdhesionMoraVigenteJudicial));
      getSession().put("presentaDeudaFis",
          (presentaDeudaFis && !existsAdhesionMoraVigenteFiscalizada));
    } catch (Exception e) {
      logger.error("error en busqueda de cuenta", e);
      addActionError("Error en la busqueda de cuenta, intente nuevamente.");
      return "input";
    }
    return "info";
  }

  /*
   * private void findPlanesPago() {
   * 
   * getSession().put("planes_deudasPre", this.dao.findPlanesPago("P"));
   * //getSession().put("planesJud", this.dao.findPlanesPago("J")); //getSession().put("planesFis",
   * this.dao.findPlanesPago("F")); }
   */
  private void checkAdhesionMoratoria(Cuenta cuentaBean) {

    existsAdhesionMoraVigentePrejudicial =
        this.dao.checkAdhesionMoratoria(cuentaBean, this.manager.getCodigoMoratoria("P"));
    /*
     * getSession().put("adhesionMoraVigentePrejudicial",
     * this.moratoriaDao.findMoratoria(cuentaBean.getImpuesto(), cuentaBean.getCuenta(),
     * cuentaBean.getDv(), this.manager.getCodigoMoratoria("P")));
     */

    existsAdhesionMoraVigenteJudicial =
        this.dao.checkAdhesionMoratoria(cuentaBean, this.manager.getCodigoMoratoria("J"));

    existsAdhesionMoraVigenteFiscalizada =
        this.dao.checkAdhesionMoratoria(cuentaBean, this.manager.getCodigoMoratoria("F"));

    logger.info("Consulta de adhesion moratoria vigente para cuenta {}, Pre: {}, Jud: {}, Fis: {}",
        cuentaBean, existsAdhesionMoraVigentePrejudicial, existsAdhesionMoraVigenteJudicial,
        existsAdhesionMoraVigenteFiscalizada);
  }

  /**
   * Buscar los totales de deuda de cada tipo
   */
  private void findExisteDeuda(Cuenta cuentaBean) {
    /*
     * Busqueda de deudas sin moratoria generadas para la cuenta PRE
     */
    this.deudasPre = this.dao.findDeuda(cuentaBean, "P");
    totalDeudaPrejudicial = manager.calcularTotalDeuda(this.deudasPre);
    presentaDeudaPre = totalDeudaPrejudicial > 0;

    // JUD
    this.deudasJud = this.dao.findDeuda(cuentaBean, "J");
    totalDeudaJudicial = manager.calcularTotalDeuda(this.deudasJud);
    presentaDeudaJud = totalDeudaJudicial > 0;

    // FIS
    this.deudasFis = this.dao.findDeuda(cuentaBean, "F");
    totalDeudaFiscalizada = manager.calcularTotalDeuda(this.deudasFis);
    presentaDeudaFis = totalDeudaFiscalizada > 0;

  }

  private String cuentaTISH() {
    logger.info("tish - busqueda de cuit en sistema siateh {}", cuit);
    // Buscar cuenta TISH en sistema SIATEH

    // SI existe presentar pantalla con cuentas asociadas
    // cuentas = dummycuentas();
    cuentas = manager.buscarCuentasEmpadronadas(cuit);
    if (!cuentas.isEmpty()) {
      getSession().put("cuentas_sieth", cuentas);
      return "seleccionar_cuenta";
    }

    // SI NO existe pagina con mensaje y link a sistema SIATEH
    return "noempadronado";
  }

  private void findMoratorias(Cuenta cuentaBean) {
    this.moratorias = this.manager.findMoratorias(cuentaBean);
  }

  public String getCuenta() {
    return this.cuenta;
  }

  public void setCuenta(String cuenta) {
    this.cuenta = cuenta.toUpperCase();
  }

  public String getDv() {
    return this.dv;
  }

  public void setDv(String dv) {
    this.dv = dv;
  }

  public InputStream getInputStreamA() {
    return this.inputStreamA;
  }

  public void setInputStreamA(InputStream inputStream) {
    this.inputStreamA = inputStream;
  }

  public String getFileName() {
    return this.fileName;
  }

  public void setFileName(String fileName) {
    this.fileName = fileName;
  }

  public Integer getImpuesto() {
    return this.impuesto;
  }

  public void setImpuesto(Integer impuesto) {
    this.impuesto = impuesto;
  }

  public String getPatente() {
    return this.patente;
  }

  public void setPatente(String patente) {
    this.patente = patente.toUpperCase();
  }

  /**
   * Remueve datos de session y la invalida
   */
  private void invalidarSession() {
    getSession().remove("cuenta");
    getSession().remove("deudas");
    // getSession().remove("deudasPre");
    getSession().remove("moratoria");
    getSession().remove("adhesionMoraVigentePrejudicial");
    getSession().remove("presentaDeuda");
    ((SessionMap) getSession()).invalidate();
  }

  public List<MoratoriaBean> getMoratorias() {
    return moratorias;
  }

  public void setMoratorias(List<MoratoriaBean> moratorias) {
    this.moratorias = moratorias;
  }

  public Integer getMoratoriaId() {
    return moratoriaId;
  }

  public void setMoratoriaId(Integer moratoriaId) {
    this.moratoriaId = moratoriaId;
  }

  public Integer getMoratoriaExistente() {
    return moratoriaExistente;
  }

  public void setMoratoriaExistente(Integer moratoriaExistente) {
    this.moratoriaExistente = moratoriaExistente;
  }

  public Boolean getExistsDeudaMoratoria() {
    return existsDeudaMoratoria;
  }

  public void setExistsDeudaMoratoria(Boolean existsDeudaMoratoria) {
    this.existsDeudaMoratoria = existsDeudaMoratoria;
  }

  public Double getTotalDeudaPrejudicial() {
    return totalDeudaPrejudicial;
  }

  public void setTotalDeudaPrejudicial(Double totalDeudaPrejudicial) {
    this.totalDeudaPrejudicial = totalDeudaPrejudicial;
  }

  public Double getTotalDeudaJudicial() {
    return totalDeudaJudicial;
  }

  public void setTotalDeudaJudicial(Double totalDeudaJudicial) {
    this.totalDeudaJudicial = totalDeudaJudicial;
  }

  public Double getTotalDeudaFiscalizada() {
    return totalDeudaFiscalizada;
  }

  public void setTotalDeudaFiscalizada(Double totalDeudaFiscalizada) {
    this.totalDeudaFiscalizada = totalDeudaFiscalizada;
  }

  public List<DeudaBean> getDeudasPre() {
    return deudasPre;
  }

  public void setDeudasPre(List<DeudaBean> deudasPre) {
    this.deudasPre = deudasPre;
  }

  public List<DeudaBean> getDeudasJud() {
    return deudasJud;
  }

  public void setDeudasJud(List<DeudaBean> deudasJud) {
    this.deudasJud = deudasJud;
  }

  public List<DeudaBean> getDeudasFis() {
    return deudasFis;
  }

  public void setDeudasFis(List<DeudaBean> deudasFis) {
    this.deudasFis = deudasFis;
  }

  public List<PlanPago> getPlanes() {
    return planes;
  }

  public void setPlanes(List<PlanPago> planes) {
    this.planes = planes;
  }

  public String getCuit() {
    return cuit;
  }

  public void setCuit(String cuit) {
    this.cuit = cuit;
  }

  public Integer getCaracter() {
    return caracter;
  }

  public void setCaracter(Integer caracter) {
    this.caracter = caracter;
  }

  public Boolean getExistsAdhesionMoraVigentePrejudicial() {
    return existsAdhesionMoraVigentePrejudicial;
  }

  public void setExistsAdhesionMoraVigentePrejudicial(
      Boolean existsAdhesionMoraVigentePrejudicial) {
    this.existsAdhesionMoraVigentePrejudicial = existsAdhesionMoraVigentePrejudicial;
  }

  public List<SiethCuenta> getCuentas() {
    return cuentas;
  }

  public void setCuentas(List<SiethCuenta> cuentas) {
    this.cuentas = cuentas;
  }

  public Boolean getPresentaDeudaPre() {
    return presentaDeudaPre;
  }

  public void setPresentaDeudaPre(Boolean presentaDeudaPre) {
    this.presentaDeudaPre = presentaDeudaPre;
  }

  public Boolean getPresentaDeudaJud() {
    return presentaDeudaJud;
  }

  public void setPresentaDeudaJud(Boolean presentaDeudaJud) {
    this.presentaDeudaJud = presentaDeudaJud;
  }

  public Boolean getPresentaDeudaFis() {
    return presentaDeudaFis;
  }

  public void setPresentaDeudaFis(Boolean presentaDeudaFis) {
    this.presentaDeudaFis = presentaDeudaFis;
  }

  public Boolean getExistsAdhesionMoraVigenteJudicial() {
    return existsAdhesionMoraVigenteJudicial;
  }

  public void setExistsAdhesionMoraVigenteJudicial(Boolean existsAdhesionMoraVigenteJudicial) {
    this.existsAdhesionMoraVigenteJudicial = existsAdhesionMoraVigenteJudicial;
  }

  public Boolean getExistsAdhesionMoraVigenteFiscalizada() {
    return existsAdhesionMoraVigenteFiscalizada;
  }

  public void setExistsAdhesionMoraVigenteFiscalizada(
      Boolean existsAdhesionMoraVigenteFiscalizada) {
    this.existsAdhesionMoraVigenteFiscalizada = existsAdhesionMoraVigenteFiscalizada;
  }

  public String getImpuestoLabel() {
    Cuenta bean = (Cuenta) getSession().get("cuenta");
    String l = "Tipo tasa:";

    switch (bean.getImpuesto()) {
      case "XP":
        l = "Automotores";
        break;
      case "PV":
        l = "Motos";
        break;
      case "AL":
        l = "Tasa de Servicios Generales";
        break;
      case "IC":
        l = "TISH - Seguridad e Higiene";
       break;
      case "RC":
          l = "TEEC - Tasa de Espacios Concesionados";
      case "FF":
          l = "FF - Ferias Francas";

    }

    return l;
  }
}
