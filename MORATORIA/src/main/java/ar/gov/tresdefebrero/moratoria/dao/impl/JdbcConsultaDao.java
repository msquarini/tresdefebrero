package ar.gov.tresdefebrero.moratoria.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import ar.gov.tresdefebrero.moratoria.beans.ABLBean;
import ar.gov.tresdefebrero.moratoria.beans.AutomotorBean;
import ar.gov.tresdefebrero.moratoria.beans.Cuenta;
import ar.gov.tresdefebrero.moratoria.beans.DeudaBean;
import ar.gov.tresdefebrero.moratoria.beans.DeudaTishBean;
import ar.gov.tresdefebrero.moratoria.beans.FFBean;
import ar.gov.tresdefebrero.moratoria.beans.MotoBean;
import ar.gov.tresdefebrero.moratoria.beans.PlanPago;
import ar.gov.tresdefebrero.moratoria.beans.SeguridadHigieneBean;
import ar.gov.tresdefebrero.moratoria.beans.TECBean;
import ar.gov.tresdefebrero.moratoria.dao.ConsultaDao;
import ar.gov.tresdefebrero.moratoria.mapper.ABLRowMapper;
import ar.gov.tresdefebrero.moratoria.mapper.AutomotorRowMapper;
import ar.gov.tresdefebrero.moratoria.mapper.FFRowMapper;
import ar.gov.tresdefebrero.moratoria.mapper.MotoRowMapper;
import ar.gov.tresdefebrero.moratoria.mapper.TECRowMapper;
import ar.gov.tresdefebrero.moratoria.mapper.TISHRowMapper;

public class JdbcConsultaDao implements ConsultaDao {
  private static final Logger logger = LoggerFactory.getLogger("JdbcConsultaDao");
  private JdbcTemplate jdbcTemplate;
  private static final String SELECT_PLANES_PAGO =
      "select id_plan_pago, descripcion, porc_efectivo, cuotas_desde,  cuotas_hasta, interes_financiero, "
          + " interes_mora, fecha, activo, moratoria_nombre, aplica_tipo_deuda  from plan_pago "
          + " where activo = true and aplica_tipo_deuda = ? order by id_plan_pago asc";

  @Deprecated
  private static final String SELECT_DEUDA =
      "select sistema, cuenta, dv, mora, anio, cuota, ajuste, importe,  debcre, "
          + " marca, cod_adm, fecha_vto  "
          + " from DEUDA where cuenta = ? and dv = ? and sistema = ? and mora=0 and marca='0' and anio > 2009 and anio < 2016 and debcre='D' "
          + " order by anio asc, cuota asc";

  @Deprecated
  private static final String SELECT_MORATORIA_NOVEDADES =
      "select cuenta, digito_verificador, sistema, total, cuotas,  monto_cuota, monto_efectivo, fecha_moratoria  from adhesion_moratoria where estado = ?";

  /**
   * Consulta padron de automotores
   */
  private static final String SELECT_AUTO =
      "select patente, dominio_original, dominio_actual, apellido_nombre, nombre_calle_pate, "
          + " nro_puerta_pate,  localidad_pate, l.descripcion as localidad, descrip_marca_pate, descrip_modelo_pate,  "
          + " circ, sec, cod_mz, mz, mzl, pc, pcl, uf, modelo_anio, tipo_patente, categoria_pate, autog "
          + " from padron_automotor inner join localidad l ON localidad_pate = l.cp where patente = ?";

  private static final String SELECT_MOTO =
      "select patente, nombre_apellido, direccion, altura, localidad, cod_postal, "
          + "marca, anio, autog, ubic_catastral from padron_moto where patente = ?";

  /**
   * Consulta padron de servicios generales
   */
  private static final String SELECT_ABL =
      "select cuenta, digito, circ, sec, mz, mzl, pc,pcl,uf, nombrecalle, nropuerta, piso, dpto, "
          + " localidad, coalesce(l.descripcion, '') as localidad_desc, appynom, circ, sec, cod_mz, mz, mzl, pc, pcl, uf  "
          // + " vtotal, categoria "
          + " from padron_tsg left outer join localidad l ON localidad = l.cp::text  where cuenta = ? and digito = ?";

  private static final String SELECT_TEC =
	      "select cuenta, digito, circ, sec, mz, mzl, pc,pcl,uf, nombrecalle, nropuerta, piso, dpto, "
	          + " localidad, coalesce(l.descripcion, '') as localidad_desc, appynom, circ, sec, cod_mz, mz, mzl, pc, pcl, uf  "
	          // + " vtotal, categoria "
	          + " from padron_tec left outer join localidad l ON localidad = l.cp::text  where cuenta = ? and digito = ?";

  private static final String SELECT_FF =
	      "select cuenta, digito, nombrecalle, nropuerta,"
	          + " localidad, coalesce(l.descripcion, '') as localidad_desc, appynom "
	          // + " vtotal, categoria "
	          + " from padron_ferias left outer join localidad l ON localidad = l.cp::text  where cuenta = ? and digito = ?";

  
  private static final String SELECT_TISH =
      "select lpad(cuenta::text, 6, '0') as cuenta, digito_verificador, circ, seccion as sec, cod_manzana as cod_mz, mz_nro as mz, mz_letra as mzl, "
          + " pc_nro as pc,pc_letra as pcl, subparcela as uf, calle, altura, codigo_postal  codigo_postal, "
          + " l.descripcion as localidad_desc, razon_social, rubro_descripcion, cuit "
          + " from sh_pad inner join localidad l ON codigo_postal = l.cp::text  where cuenta = ? and digito_verificador = ?";

  /**
   * Deuda de cuenta sin incluir la ya adherida a moratoria previa
   */
  private static final String SELECT_DEUDA_SIN_ADHESION =
      "select sistema, cuenta, dv, mora, anio, cuota, ajuste, importe,  debcre, "
          + " marca, cod_adm, fecha_vto from DEUDA d "
          + " where d.cuenta = ? and d.dv = ? and d.sistema = ? and d.mora=0 and d.marca='0' and d.tipo = ?"
          // + " where d.cuenta = ? and d.dv = ? and d.sistema = ? "
          // + "and d.anio > 2009 and d.anio < 2017 and d.debcre='D' and not exists ( "

          + " and d.anio < 2019 and d.debcre='D' and not exists ( "
          + " select 1 from adhesion_moratoria am inner join moratoria_deuda md on am.id_adhesion = md.id_adhesion "
          // + " where am.cuenta = d.cuenta and am.dv = d.dv and md.anio =
          // d.anio and md.cuota = d.cuota and d.mora = md.mora and md.marca =
          // '1' "

          // 2018 cambio ya que dejaba registros de deuda afuera
          // + " where am.cuenta = d.cuenta and am.dv = d.dv and md.anio = d.anio and md.cuota =
          // d.cuota and md.marca <> d.marca "
          + " where am.cuenta = d.cuenta and am.dv = d.dv and md.anio = d.anio and md.cuota = d.cuota and md.importe = d.importe " // and
                                                                                                                                   // md.fecha_vto
                                                                                                                                   // =
                                                                                                                                   // d.fecha_vto
                                                                                                                                   // "
          + " ) order by anio asc, cuota asc";

  private static final String SELECT_DEUDA_TISH_SIN_ADHESION =
      "select d.sistema, d.cuenta, d.dv, d.mora, d.anio, d.cuota, "
          + " d.ajuste, d.importe,  d.debcre, d.marca, d.cod_adm, d.fecha_vto from DEUDA d "
          // + " where d.cuenta::int = ? and d.dv = ? and d.sistema = ? and d.mora=0 and d.marca='0'
          // "
          // 2018 agregado de tipo de deuda P/J/F
          + " where d.cuenta = ? and d.dv = ? and d.sistema = ? and d.mora=0 and d.marca='0' and d.tipo = ?"
          // + " and d.anio > 2009 and d.anio < 2017 and d.debcre='D' and not exists ( "

          + " and d.anio < 2019 and d.debcre='D' and not exists ( "
          + " select 1 from adhesion_moratoria am inner join moratoria_deuda md on am.id_adhesion = md.id_adhesion "
          // + " where am.cuenta = d.cuenta and am.dv = d.dv and md.anio = d.anio and md.cuota =
          // d.cuota and md.marca <> d.marca "
          + " where am.cuenta = d.cuenta and am.dv = d.dv and md.anio = d.anio and md.cuota = d.cuota and md.importe = d.importe " // and
                                                                                                                                   // md.fecha_vto
                                                                                                                                   // =
                                                                                                                                   // d.fecha_vto
                                                                                                                                   // "
          + " ) order by anio asc, cuota asc";

  /*
   * private static final String SELECT_EXISTS_DEUDA_MORATORIA =
   * "select 1 from DEUDA d where d.cuenta = ? and d.dv = ? and d.sistema = ? and d.mora = ?";
   */
  private static final String SELECT_EXISTS_ADHESION_MORATORIA =
      "select 1 from adhesion_moratoria d where d.cuenta = ? and d.dv = ? and d.sistema = ? and d.mora = ?";

  /*
   * private static final String SELECT_PERMITE_ADHESION_MORATORIA504 =
   * "select 1 from adhesion_moratoria d where d.cuenta = ? and d.dv = ? and d.sistema = ? and d.mora = ? "
   * + " and marca_pago = 1 and not exists (select 1 from adhesion_moratoria where cuenta = ? and "
   * + " dv = ? and sistema = ? and mora = ?)";
   */

  public void setDataSource(DataSource dataSource) {
    this.jdbcTemplate = new JdbcTemplate(dataSource);
  }

  /**
   * Verifica si la cuenta presenta adhesion a moratoria
   * 
   * @param cuentaBean
   * @param moratoria
   * @return
   */
  public boolean checkAdhesionMoratoria(Cuenta cuentaBean, Integer moratoria) {
    String tipoImpuesto = cuentaBean.getImpuesto();

    try {
      Integer exists = this.jdbcTemplate.queryForObject(SELECT_EXISTS_ADHESION_MORATORIA,
          new Object[] {cuentaBean.getCuenta(), cuentaBean.getDv(), tipoImpuesto, moratoria},
          Integer.class);

      return (exists > 0);

    } catch (EmptyResultDataAccessException e) {
      logger.warn("Sin adhesion de moratoria {}, {} / {}, {}", moratoria, cuentaBean.getCuenta(),
          cuentaBean.getDv(), tipoImpuesto);
      return false;
    }
  }

  // public boolean checkDeudaMoratoria(Integer impuesto, String cuenta, String dv, String patente,
  // Integer moratoria) {
  // String tipoImpuesto = "XP";
  // ;
  // try {
  // switch (impuesto.intValue()) {
  // case 1:
  // tipoImpuesto = "XP";
  // dv = "";
  // cuenta = patente;
  // break;
  // case 2:
  // tipoImpuesto = "PV";
  // dv = "";
  // cuenta = patente;
  // break;
  // case 3:
  // tipoImpuesto = "AL";
  // break;
  // default:
  // tipoImpuesto = "IC";
  // }
  //
  // Integer exists = this.jdbcTemplate.queryForObject(SELECT_EXISTS_DEUDA_MORATORIA,
  // new Object[] { cuenta, dv, tipoImpuesto, moratoria }, Integer.class);
  //
  // return (exists > 0);
  // } catch (EmptyResultDataAccessException e) {
  // logger.warn("Sin dueda de moratoria {}, {} / {}, {}", new Object[] { moratoria, cuenta, dv,
  // tipoImpuesto });
  // return false;
  // }
  // }

  // public List<DeudaBean> findDeuda(Integer impuesto, String cuenta, String dv,
  // String patente) {
  public List<DeudaBean> findDeuda(Cuenta cuentaBean, String tipoDeuda) {
    // String tipoImpuesto = "XP";
    String tipoImpuesto = cuentaBean.getImpuesto();
    logger.info("findDeuda {}, {} / {}, tipoDeuda: {}", cuentaBean.getImpuesto(),
        cuentaBean.getCuenta(), cuentaBean.getDv(), tipoDeuda);

    List<DeudaBean> bean = null;
    try {
      // switch (impuesto.intValue()) {
      // case 1:
      // tipoImpuesto = "XP";
      // dv = "";
      // cuenta = patente;
      // break;
      // case 2:
      // tipoImpuesto = "PV";
      // dv = "";
      // cuenta = patente;
      // break;
      // case 3:
      // tipoImpuesto = "AL";
      // break;
      // default:
      // tipoImpuesto = "IC";
      // }
      // bean = this.jdbcTemplate.query(SELECT_DEUDA, new Object[] {
      // cuenta, dv, tipoImpuesto },

      if (tipoImpuesto.equals("IC")) {
        // consulto las cuotas no pagas (deudas)
        List<DeudaBean> deudas = this.jdbcTemplate.query(SELECT_DEUDA_TISH_SIN_ADHESION,
            // new Object[] {Integer.valueOf(cuenta), dv, tipoImpuesto}, new
            // RowMapper<DeudaBean>() {
            /*
             * new Object[] {Integer.valueOf(cuentaBean.getCuenta()), cuentaBean.getDv(),
             * tipoImpuesto},
             */
            new Object[] {cuentaBean.getCuenta(), cuentaBean.getDv(), tipoImpuesto, tipoDeuda},
            new RowMapper<DeudaBean>() {
              public DeudaBean mapRow(ResultSet rs, int rowNum) throws SQLException {
                DeudaTishBean bean = new DeudaTishBean();
                bean.setSistema(rs.getString("sistema"));
                bean.setCuenta(rs.getString("cuenta"));
                bean.setDv(rs.getString("dv"));
                bean.setMora(Integer.valueOf(rs.getInt("mora")));
                bean.setAnio(Integer.valueOf(rs.getInt("anio")));
                bean.setCuota(Integer.valueOf(rs.getInt("cuota")));
                bean.setAjuste(rs.getString("ajuste"));
                bean.setImporte(rs.getBigDecimal("importe"));
                bean.setDebcre(rs.getString("debcre"));
                bean.setMarca(rs.getString("marca"));
                bean.setCodAdm(Integer.valueOf(rs.getInt("cod_adm")));
                bean.setFechaVto(rs.getDate("fecha_vto"));
                return bean;
              }
            });
        // Si existen deudas --> Completo los periodos para realizar
        // ajustes y marcamos las deudas
        /*
         * if (!deudas.isEmpty()) { bean = periodosCompletosTish(deudas, cuenta, dv); } else { bean
         * = deudas; }
         */

        // 2018 - Completamos los periodos solo para prejudicial. Jud y Fisca no pueden cargar
        // ajustes!
        if (tipoDeuda.equalsIgnoreCase("P")) {
          bean = periodosCompletosTish(deudas, cuentaBean.getCuenta(), cuentaBean.getDv());
        } else {
          // bean = transformarDeudaTish(deudas);
          bean = deudas;
        }
      } else {

        // bean = this.jdbcTemplate.query(SELECT_DEUDA_SIN_ADHESION, new Object[] {
        // cuenta, dv, tipoImpuesto },
        bean = this.jdbcTemplate.query(SELECT_DEUDA_SIN_ADHESION,
            new Object[] {cuentaBean.getCuenta(), cuentaBean.getDv(), tipoImpuesto, tipoDeuda},
            new RowMapper<DeudaBean>() {
              public DeudaBean mapRow(ResultSet rs, int rowNum) throws SQLException {
                DeudaBean bean = new DeudaBean();
                bean.setSistema(rs.getString("sistema"));
                bean.setCuenta(rs.getString("cuenta"));
                bean.setDv(rs.getString("dv"));
                bean.setMora(Integer.valueOf(rs.getInt("mora")));
                bean.setAnio(Integer.valueOf(rs.getInt("anio")));
                bean.setCuota(Integer.valueOf(rs.getInt("cuota")));
                bean.setAjuste(rs.getString("ajuste"));
                bean.setImporte(rs.getBigDecimal("importe"));
                bean.setDebcre(rs.getString("debcre"));
                bean.setMarca(rs.getString("marca"));
                bean.setCodAdm(Integer.valueOf(rs.getInt("cod_adm")));
                bean.setFechaVto(rs.getDate("fecha_vto"));
                return bean;
              }
            });
      }
    } catch (EmptyResultDataAccessException e) {
      logger.warn("Sin dueda {} / {}, {}", cuentaBean.getCuenta(), cuentaBean.getDv(),
          tipoImpuesto);
      return Collections.emptyList();
    }
    return bean;
  }

  public Cuenta findCuenta(Integer tipoImpuesto, String cuenta, String dv, String patente) {
    Cuenta bean = null;
    switch (tipoImpuesto.intValue()) {
      case 1:
        bean = findAccountAutomotor(patente);
        break;
      case 2:
        bean = findAccountMoto(patente);
        break;
      case 3:
        bean = findAccountABL(cuenta, dv);
        break;
      case 4:
        bean = findAccountTISH(cuenta, dv);
        break;
      case 5:
          bean = findAccountTEC(cuenta, dv); 
          break;
      case 6:
          bean = findAccountFF(cuenta, dv);
      
 
   
    }
    return bean;
  }

  private AutomotorBean findAccountAutomotor(String patente) {
    AutomotorBean bean;
    try {
      bean = (AutomotorBean) this.jdbcTemplate.queryForObject(SELECT_AUTO,
          new Object[] {"0" + patente}, new AutomotorRowMapper());
    } catch (EmptyResultDataAccessException e) {
      logger.warn("cuenta automotor no encontrada {} ", patente, e);
      return null;
    }
    return bean;
  }

  private MotoBean findAccountMoto(String patente) {
    MotoBean bean;
    try {
      bean = (MotoBean) this.jdbcTemplate.queryForObject(SELECT_MOTO, new Object[] {patente},
          new MotoRowMapper());
    } catch (EmptyResultDataAccessException e) {
      logger.warn("cuenta moto no encontrada {} ", patente);
      return null;
    }
    return bean;
  }

  private ABLBean findAccountABL(String cuenta, String dv) {
    ABLBean bean;
    try {
      bean = (ABLBean) this.jdbcTemplate.queryForObject(SELECT_ABL,
          new Object[] {Integer.valueOf(cuenta), dv}, new ABLRowMapper());
    } catch (EmptyResultDataAccessException e) {
      logger.warn("cuenta ABL no encontrada {} / {}", cuenta, dv, e);
      return null;
    }
    return bean;
  }

  private SeguridadHigieneBean findAccountTISH(String cuenta, String dv) {
    SeguridadHigieneBean bean;
    try {
      bean = (SeguridadHigieneBean) this.jdbcTemplate.queryForObject(SELECT_TISH,
          new Object[] {Integer.valueOf(cuenta), dv}, new TISHRowMapper());
    } catch (EmptyResultDataAccessException e) {
      logger.warn("cuenta SH no encontrada {} / {}", cuenta, dv, e);
      return null;
    }
    return bean;
  }
  
  private TECBean findAccountTEC (String cuenta, String dv)
  {
	    TECBean bean;
	    try {
	      bean = (TECBean) this.jdbcTemplate.queryForObject(SELECT_TEC,
	          new Object[] {Integer.valueOf(cuenta), dv}, new TECRowMapper());
	    } catch (EmptyResultDataAccessException e) {
	      logger.warn("cuenta TEC no encontrada {} / {}", cuenta, dv, e);
	      return null;
	    }
	    return bean;
	  
	  
  }
  
  private FFBean findAccountFF (String cuenta, String dv)
  {
	    FFBean bean;
	    
	    try {
	      bean = (FFBean) this.jdbcTemplate.queryForObject(SELECT_FF,
	          new Object[] {Integer.valueOf(cuenta), dv}, new FFRowMapper());
	    	
	    } catch (EmptyResultDataAccessException e) {
	    	logger.warn("cuenta FF no encontrada {} / {}", cuenta, dv, e);
	      return null;
	    }
	    return bean;
	  
	  
  }
  
  
  public List<PlanPago> findPlanesPago(String tipoDeuda) {
    List<PlanPago> planes;
    try {
      planes = this.jdbcTemplate.query(SELECT_PLANES_PAGO, new Object[] {tipoDeuda},
          new RowMapper<PlanPago>() {
            public PlanPago mapRow(ResultSet rs, int rowNum) throws SQLException {
              PlanPago bean = new PlanPago();
              bean.setIdPlanPago(Integer.valueOf(rs.getInt("id_plan_pago")));
              bean.setDescripcion(rs.getString("descripcion"));
              bean.setInteresFinanciero(Double.valueOf(rs.getDouble("interes_financiero")));
              bean.setInteresMora(Double.valueOf(rs.getDouble("interes_mora")));
              bean.setPorcentajeEfectivo(Double.valueOf(rs.getDouble("porc_efectivo")));
              bean.setCuotasDesde(Integer.valueOf(rs.getInt("cuotas_desde")));
              bean.setCuotasHasta(Integer.valueOf(rs.getInt("cuotas_hasta")));
              bean.setMoratoriaNombre(rs.getString("moratoria_nombre"));
              bean.setMoratoriaTipo(rs.getString("aplica_tipo_deuda"));
              return bean;
            }
          });
    } catch (EmptyResultDataAccessException e) {
      logger.warn("No hay planes de pago activos");
      return null;
    }
    return planes;
  }

  // @Deprecated
  // public List<MoratoriaBean> findMoratorias(String estado) {
  // List<MoratoriaBean> moratorias;
  // try {
  // moratorias = this.jdbcTemplate.query(SELECT_MORATORIA_NOVEDADES, new Object[] { estado },
  // new RowMapper<MoratoriaBean>() {
  // public MoratoriaBean mapRow(ResultSet rs, int rowNum) throws SQLException {
  // MoratoriaBean bean = new MoratoriaBean();
  // bean.setSistema(rs.getString("sistema"));
  // bean.setCuenta(rs.getString("cuenta"));
  // bean.setDv(rs.getString("dv"));
  // bean.setCuotas(Integer.valueOf(rs.getInt("cuotas")));
  //
  // return bean;
  // }
  // });
  // } catch (EmptyResultDataAccessException e) {
  // logger.warn("No hay adhesiones en estado: {} ", estado);
  // return null;
  // }
  // return moratorias;
  // }

  /**
   * Marcar como deuda y los minimos en caso de TISH
   * 
   * @param deudas
   * @return
   */
  /*
   * private List<DeudaBean> transformarDeudaTish(List<DeudaBean> deudas) { List<DeudaBean> result =
   * new ArrayList<DeudaBean>(deudas.size()); for (DeudaBean d : deudas) { DeudaTishBean dtb = new
   * DeudaTishBean(d); result.add(dtb); } return result; }
   */
  private List<DeudaBean> periodosCompletosTish(List<DeudaBean> deudas, String cuenta, String dv) {
    Map<String, DeudaBean> deudaCompleta = new HashMap<String, DeudaBean>();
    // Cargo los periodos!
    for (int anio = 2013; anio < 2019; anio++) {
      for (int periodo = 1; periodo < 7; periodo++) {
        DeudaTishBean dtb = new DeudaTishBean(anio, periodo, cuenta, dv, "0");
        deudaCompleta.put(dtb.getKey(), dtb);
      }
    }
    // Marco las deudas
    for (DeudaBean d : deudas) {
      DeudaTishBean dtb = (DeudaTishBean) deudaCompleta.get(d.getKey());
      if (dtb != null) {
        dtb.setPresentaPago(false);
        dtb.setMinimo(d.getImporte());
        dtb.setFechaVto(d.getFechaVto());
      } else {
        // Deuda fuera del periodo 2013-2017, se incorpora
        dtb = new DeudaTishBean(d.getAnio(), d.getCuota(), d.getCuenta(), d.getDv(), d.getAjuste());
        dtb.setPresentaPago(false);
        dtb.setMinimo(d.getImporte());
        dtb.setFechaVto(d.getFechaVto());
        deudaCompleta.put(dtb.getKey(), dtb);
      }
    }

    List<DeudaBean> result = new ArrayList<DeudaBean>(deudaCompleta.values());
    Collections.sort(result, new Comparator<DeudaBean>() {
      @Override
      public int compare(DeudaBean o1, DeudaBean o2) {
        int r = o1.getAnio().compareTo(o2.getAnio());
        if (r == 0) {
          return o1.getCuota().compareTo(o2.getCuota());
        }
        return r;
      }
    });

    return result;
  }

  /*
   * @Override public boolean checkAdhesionMoratoria504(Integer impuesto, String cuenta, String dv,
   * String patente, Integer moratoria) { try { Integer exists =
   * this.jdbcTemplate.queryForObject(SELECT_PERMITE_ADHESION_MORATORIA504, new Object[] { cuenta,
   * dv, "IC", 503, cuenta, dv, "IC", moratoria }, Integer.class);
   * 
   * return (exists > 0);
   * 
   * } catch (EmptyResultDataAccessException e) {
   * logger.warn("Sin adhesion de moratoria {}, {} / {}, {}", new Object[] { moratoria, cuenta, dv,
   * "IC" }); return false; } }
   */
}
