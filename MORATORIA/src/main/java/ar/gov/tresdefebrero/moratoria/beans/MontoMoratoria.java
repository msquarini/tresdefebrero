package ar.gov.tresdefebrero.moratoria.beans;

import java.math.BigDecimal;

public class MontoMoratoria {

	private BigDecimal efectivo = BigDecimal.ZERO;
	private BigDecimal montoCuota = BigDecimal.ZERO;

	private BigDecimal deudaOriginal = BigDecimal.ZERO;

	public MontoMoratoria(BigDecimal efectivo, BigDecimal montoCuota, BigDecimal deudaOriginal) {
		this.efectivo = efectivo;
		this.montoCuota = montoCuota;
		this.deudaOriginal = deudaOriginal;
	}

	public BigDecimal getEfectivo() {
		return efectivo;
	}

	public void setEfectivo(BigDecimal efectivo) {
		this.efectivo = efectivo;
	}

	public BigDecimal getMontoCuota() {
		return montoCuota;
	}

	public void setMontoCuota(BigDecimal montoCuota) {
		this.montoCuota = montoCuota;
	}

	public BigDecimal getDeudaOriginal() {
		return deudaOriginal;
	}

	public void setDeudaOriginal(BigDecimal deudaOriginal) {
		this.deudaOriginal = deudaOriginal;
	}
	
	public String toString() {
		return String.format("Efectivo %9.2f - Monto Cuota %9.2f", new Object[] {getEfectivo(), getMontoCuota()});
	}
}
