package ar.gov.tresdefebrero.moratoria.beans;

/**
 * Representacion simple de padron de motos
 * 
 * @author admin
 *
 */
public class MotoBean extends AbstractCuenta implements Cuenta {

  /**
   * Cuenta + DV --> identifica el impuesto
   */
  private String patente;

  /**
   * 
   */
  private Nomenclatura nomenclatura;

  private String titular;

  /**
   * Domilicio
   */
  private String calle;
  private String numero;
  private String localidad;
  private String codigoPostal;

  private String descripcionMarca;
  private Integer modeloAnio;

  private Integer autog;
  private String ubicacionCatastral;

  public String getPatente() {
    return patente;
  }

  public void setPatente(String patente) {
    this.patente = patente;
  }

  public Nomenclatura getNomenclatura() {
    return nomenclatura;
  }

  public void setNomenclatura(Nomenclatura nomenclatura) {
    this.nomenclatura = nomenclatura;
  }

  @Override
  public String getCuenta() {
    return patente;
  }

  @Override
  public String getDv() {
    return "";
  }

  @Override
  public String getImpuesto() {
    return "PV";
  }

  public String getTitular() {
    return titular;
  }

  public void setTitular(String titular) {
    this.titular = titular;
  }

  public String getCalle() {
    return calle;
  }

  public void setCalle(String calle) {
    this.calle = calle;
  }

  public String getNumero() {
    return numero;
  }

  public void setNumero(String numero) {
    this.numero = numero;
  }

  public String getLocalidad() {
    return localidad;
  }

  public void setLocalidad(String localidad) {
    this.localidad = localidad;
  }

  public String getCodigoPostal() {
    return codigoPostal;
  }

  public void setCodigoPostal(String codigoPostal) {
    this.codigoPostal = codigoPostal;
  }

  public String getDescripcionMarca() {
    return descripcionMarca;
  }

  public void setDescripcionMarca(String descripcionMarca) {
    this.descripcionMarca = descripcionMarca;
  }

  @Override
  public String getCuentaBarcode() {
    return getAutog().toString().substring(0, 6);
  }

  @Override
  public String getDvBarcode() {
    return getAutog().toString().substring(6);
  }

  public Integer getModeloAnio() {
    return modeloAnio;
  }

  public void setModeloAnio(Integer anio) {
    this.modeloAnio = anio;
  }

  public Integer getAutog() {
    return autog;
  }

  public void setAutog(Integer autog) {
    this.autog = autog;
  }

  public String getUbicacionCatastral() {
    return ubicacionCatastral;
  }

  public void setUbicacionCatastral(String ubicacionCatastral) {
    this.ubicacionCatastral = ubicacionCatastral;
  }

}
