package ar.gov.tresdefebrero.moratoria.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import ar.gov.tresdefebrero.moratoria.beans.SeguridadHigieneBean;

/**
 * Mapeo de tabla padron_sh (Tasa seguridad e higiene)
 * 
 * @author msquarini
 *
 */
public class TISHRowMapper implements RowMapper<SeguridadHigieneBean> {

	@Override
	public SeguridadHigieneBean mapRow(ResultSet rs, int rowNum) throws SQLException {
		SeguridadHigieneBean bean = new SeguridadHigieneBean();

		//bean.setCuit(rs.getString("cuit"));
		
		bean.setCuenta(rs.getString("cuenta"));
		bean.setDv(rs.getString("digito_verificador"));
		bean.setCalle(rs.getString("calle"));
		bean.setNumero(rs.getString("altura"));
		bean.setCodigoPostal(rs.getString("codigo_postal"));
		bean.setLocalidad(rs.getString("localidad_desc"));
		bean.setRazonSocial(rs.getString("razon_social"));
		bean.setRubroDescripcion(rs.getString("rubro_descripcion"));
		
		bean.setNomenclatura(NomenclaturaMapper.map(rs));
		return bean;
	}

}
