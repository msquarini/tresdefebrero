package ar.gov.tresdefebrero.moratoria.beans;
import java.math.BigDecimal;

public class TECBean extends AbstractCuenta implements Cuenta{
	 private Integer cuenta;
	 private String dv;
	 
	 private Nomenclatura nomenclatura;

	  /**
	   * Domicilio
	   */
	  private String calle;
	  private String numero;
	  private String piso;
	  private String departamento;
	  private String codigoPostal;
	  private String localidad;

	  private String titular;

	  public String getCuenta() {
		    return cuenta.toString();
		  }

		  public void setCuenta(Integer cuenta) {
		    this.cuenta = cuenta;
		  }

		  public String getDv() {
		    return dv;
		  }

		  public void setDv(String dv) {
		    this.dv = dv;
		  }

		  public Nomenclatura getNomenclatura() {
		    return nomenclatura;
		  }

		  public void setNomenclatura(Nomenclatura nomenclatura) {
		    this.nomenclatura = nomenclatura;
		  }

		  public String getCalle() {
		    return calle;
		  }

		  public void setCalle(String calle) {
		    this.calle = calle;
		  }

		  public String getNumero() {
		    return numero;
		  }

		  public void setNumero(String numero) {
		    this.numero = numero;
		  }

		  public String getPiso() {
		    return piso;
		  }

		  public void setPiso(String piso) {
		    this.piso = piso;
		  }

		  public String getDepartamento() {
		    return departamento;
		  }

		  public void setDepartamento(String departamento) {
		    this.departamento = departamento;
		  }

		  public String getCodigoPostal() {
		    return codigoPostal;
		  }

		  public void setCodigoPostal(String codigoPostal) {
		    this.codigoPostal = codigoPostal;
		  }

		  public String getLocalidad() {
		    return localidad;
		  }

		  public void setLocalidad(String localidad) {
		    this.localidad = localidad;
		  }

		  public String getTitular() {
		    return titular;
		  }

		  public void setTitular(String titular) {
		    this.titular = titular;
		  }

		  @Override
		  public String getImpuesto() {
		    return "RC";
		  }

		  @Override
		  public String getCuentaBarcode() {
		    return getCuenta();
		  }

		  @Override
		  public String getDvBarcode() {
		    return dv;
		  }

		  
		  
}

 
