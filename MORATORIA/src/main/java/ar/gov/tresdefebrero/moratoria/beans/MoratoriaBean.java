package ar.gov.tresdefebrero.moratoria.beans;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Deuda de impuestos
 * 
 * @author admin
 *
 */
public class MoratoriaBean {

  private Integer idMoratoria;
  private PlanPago planPago;
  private String cuenta;
  private String dv;
  private String sistema;
  private Integer cuotas;
  private BigDecimal deudaTotal;
  private BigDecimal montoEfectivo;
  private BigDecimal montoCuota;
  private Date fechaAdhesion;
  List<DeudaBean> deudaOriginal;
  List<DeudaBean> deudaNueva;
  private Integer registraPago;
  private String estado;

  // Identificador de moratoria (502 la primera)
  private Integer codMoratoria;
  // Mail de cuenta recibido x link
  private String mail;
  
  private Date fechaUltimaCuota;
  private String dvMoratoria;
  private String cuit;
  private Integer codigoCaracter;

  /**
   * Solo para autos y motos en otros casos 0
   */
  private Integer autog;
  
  public MoratoriaBean() {}

  public MoratoriaBean(Cuenta cuentaBean, List<DeudaBean> deuda, PlanPago planPago, Integer cuotas,
      MontoMoratoria montos, Integer codMoratoria, String mail, String cuit, Integer codigoCaracter, String dvMoratoria) {
    this.cuenta = cuentaBean.getCuenta();
    this.dv = cuentaBean.getDv();
    this.sistema = cuentaBean.getImpuesto();

    // 2018 GUardar el autog en casos de motos y autos
    this.autog = 0;
    if (this.sistema.equals("XP") || this.sistema.equals("PV")) {
      this.autog = Integer.valueOf(cuentaBean.getCuentaBarcode()+cuentaBean.getDvBarcode());
    }
    
    this.planPago = planPago;

    this.deudaOriginal = deuda;

    this.cuotas = cuotas;

    this.montoEfectivo = montos.getEfectivo();
    this.deudaTotal = montos.getDeudaOriginal();
    this.montoCuota = montos.getMontoCuota();

    this.fechaAdhesion = new Date();

    this.codMoratoria = codMoratoria;
    // Nuevos campos
    this.mail = mail;
    this.cuit = cuit;
    this.codigoCaracter = codigoCaracter;
    this.dvMoratoria = dvMoratoria;
    
    generarDeuda(montos.getMontoCuota());
  }

  public String getCuenta() {
    return cuenta;
  }

  public void setCuenta(String cuenta) {
    this.cuenta = cuenta;
  }

  public String getDv() {
    return dv;
  }

  public void setDv(String dv) {
    this.dv = dv;
  }

  public String getSistema() {
    return sistema;
  }

  public void setSistema(String sistema) {
    this.sistema = sistema;
  }

  public Integer getCuotas() {
    return cuotas;
  }

  public void setCuotas(Integer cuotas) {
    this.cuotas = cuotas;
  }

  /**
   * Registros de deuda original y nueva para la interaz de notificacion a mainframe (BATCH)
   * 
   * @return
   */

  public List<DeudaBean> getDeudaMoratoria() {
    List<DeudaBean> deudas = new ArrayList();
    for (DeudaBean d : getDeudaOriginal()) {
      d.setSistema(getSistema());

      /*
       * Ajuste por motos que tienen patente de 6 posiciones
       */
      if (getSistema().equals("PV") && getCuenta().length() == 6) {
        d.setCuenta("0" + getCuenta().substring(0, 5));
        d.setDv(getCuenta().substring(5));
      } else {
        d.setCuenta(getCuenta());
        d.setDv(getDv());
      }

      deudas.add(d);
    }
    for (DeudaBean d : getDeudaNueva()) {
      d.setSistema(getSistema());
      /*
       * Ajuste por motos que tienen patente de 6 posiciones
       */
      if (getSistema().equals("PV") && getCuenta().length() == 6) {
        d.setCuenta("0" + getCuenta().substring(0, 5));
        d.setDv(getCuenta().substring(5));
      } else {
        d.setCuenta(getCuenta());
        d.setDv(getDv());
      }

      deudas.add(d);
    }
    return deudas;
  }

  private void generarDeuda(BigDecimal valorCuota) {
    this.deudaNueva = new ArrayList();

    Date desde = getFechaAdhesion();


    if (this.montoEfectivo.doubleValue() > 0.0D) {

      DeudaBean cuotaEfectivo = new DeudaBean(true, true,
          Integer.valueOf(this.cuotas.intValue() + 1), desde, getCodMoratoria(), "2");

      cuotaEfectivo.setImporte(this.montoEfectivo);
      desde = cuotaEfectivo.getFechaVto();
      this.deudaNueva.add(cuotaEfectivo);
    }
    for (int i = 1; i <= this.cuotas.intValue(); i++) {
      DeudaBean d;

      if (this.deudaNueva.isEmpty()) {
        d = new DeudaBean(true, true, Integer.valueOf(i), desde, getCodMoratoria(), "2");
      } else {
        d = new DeudaBean(true, false, Integer.valueOf(i), desde, getCodMoratoria(), "2");
      }

      d.setImporte(valorCuota);
      this.deudaNueva.add(d);

      desde = d.getFechaVto();
    }
    // Seteo la fecha de la ultima cuota en la moratoria para saber si finalizo!
    setFechaUltimaCuota(desde);
  }

  public Integer getIdMoratoria() {
    return idMoratoria;
  }

  public void setIdMoratoria(Integer idMoratoria) {
    this.idMoratoria = idMoratoria;
  }

  public BigDecimal getMontoEfectivo() {
    return montoEfectivo;
  }

  public void setMontoEfectivo(BigDecimal montoEfectivo) {
    this.montoEfectivo = montoEfectivo;
  }

  public void setMontoCuota(BigDecimal montoCuota) {
    this.montoCuota = montoCuota;
  }

  public Date getFechaAdhesion() {
    return fechaAdhesion;
  }

  public void setFechaAdhesion(Date fechaAdhesion) {
    this.fechaAdhesion = fechaAdhesion;
  }

  public PlanPago getPlanPago() {
    return planPago;
  }

  public void setPlanPago(PlanPago planPago) {
    this.planPago = planPago;
  }

  public List<DeudaBean> getDeudaOriginal() {
    return deudaOriginal;
  }

  public void setDeudaOriginal(List<DeudaBean> deudaOriginal) {
    this.deudaOriginal = deudaOriginal;
  }

  public List<DeudaBean> getDeudaNueva() {
    return deudaNueva;
  }

  public void setDeudaNueva(List<DeudaBean> deudaNueva) {
    this.deudaNueva = deudaNueva;
  }

  public Integer getRegistraPago() {
    return registraPago;
  }

  public void setRegistraPago(Integer registraPago) {
    this.registraPago = registraPago;
  }

  public String getEstado() {
    return estado;
  }

  public void setEstado(String estado) {
    this.estado = estado;
  }

  public void setDeudaTotal(BigDecimal deudaTotal) {
    this.deudaTotal = deudaTotal;
  }

  public BigDecimal getDeudaTotal() {
    return deudaTotal;
  }

  public BigDecimal getMontoCuota() {
    return montoCuota;
  }

  private boolean cambioAnio(Date a) {
    Calendar calA = Calendar.getInstance();
    calA.setTime(a);
    return calA.get(Calendar.MONTH) == 11; // diciembre
  }

  public Integer getCodMoratoria() {
    return codMoratoria;
  }

  public void setCodMoratoria(Integer codMoratoria) {
    this.codMoratoria = codMoratoria;
  }

  public String getMail() {
    return mail;
  }

  public void setMail(String mail) {
    this.mail = mail;
  }

  public Date getFechaUltimaCuota() {
    return fechaUltimaCuota;
  }

  public void setFechaUltimaCuota(Date fechaUltimaCuota) {
    this.fechaUltimaCuota = fechaUltimaCuota;
  }

  public String getDvMoratoria() {
    return dvMoratoria;
  }

  public void setDvMoratoria(String dvMoratoria) {
    this.dvMoratoria = dvMoratoria;
  }

  public String getCuit() {
    return cuit;
  }

  public void setCuit(String cuit) {
    this.cuit = cuit;
  }

  public Integer getCodigoCaracter() {
    return codigoCaracter;
  }

  public void setCodigoCaracter(Integer codigoCaracter) {
    this.codigoCaracter = codigoCaracter;
  }

  public Integer getAutog() {
    return autog;
  }

  public void setAutog(Integer autog) {
    this.autog = autog;
  }
}
