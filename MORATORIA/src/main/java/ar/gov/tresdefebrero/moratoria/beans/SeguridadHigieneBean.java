package ar.gov.tresdefebrero.moratoria.beans;

import java.math.BigDecimal;

public class SeguridadHigieneBean extends AbstractCuenta implements Cuenta {
  private String cuenta;
  private String dv;
  private String razonSocial;
  private String cuit;
  private int numeroCuota;
  private Nomenclatura nomenclatura;
  private String rubro;
  private String rubroDescripcion;
  // private BigDecimal montoMinimo;
  private String calle;
  private String numero;
  private String codigoPostal;
  private String localidad;

  public SeguridadHigieneBean() {

  }

  public static String calculateDV(String cuenta) {
    Integer total = 0;
    for (int i = 1; i <= cuenta.length(); i++) {
      total = total + Character.getNumericValue(cuenta.charAt(i - 1)) * i;
    }
    total = 11 - (total % 11);

    if (total == 11) {
      return "-";
    } else if (total == 10) {
      return "0";
    } else {
      return String.valueOf(total);
    }
  }

  public SeguridadHigieneBean(SiethCuenta sieth) {
    this.cuenta = sieth.getCuenta();
    // this.dv = sieth.getDv();
    this.dv = calculateDV(this.cuenta);

    this.cuit = sieth.getCuit();
    this.razonSocial = sieth.getRazonSocial();
    this.rubroDescripcion = sieth.getDescripcionActividad();
    this.rubro = sieth.getActividad();
  }

  public String getCuenta() {
    return this.cuenta.toString();
  }

  public void setCuenta(String cuenta) {
    this.cuenta = cuenta;
  }

  public String getDv() {
    return this.dv;
  }

  public void setDv(String dv) {
    this.dv = dv;
  }

  public String getRazonSocial() {
    return this.razonSocial;
  }

  public void setRazonSocial(String razonSocial) {
    this.razonSocial = razonSocial;
  }


  public String getCuit() {
    return this.cuit;
  }

  public void setCuit(String cuit) {
    this.cuit = cuit;
  }


  public int getNumeroCuota() {
    return this.numeroCuota;
  }

  public void setNumeroCuota(int numeroCuota) {
    this.numeroCuota = numeroCuota;
  }

  public Nomenclatura getNomenclatura() {
    return this.nomenclatura;
  }

  public void setNomenclatura(Nomenclatura nomenclatura) {
    this.nomenclatura = nomenclatura;
  }

  public String getNomenclaturaLeyenda() {
    StringBuffer sb = new StringBuffer("Circ. ");
    sb.append(getNomenclatura().getCircunscripcion()).append(" Secc. ")
        .append(getNomenclatura().getSeccion()).append(" CMz. ");
    if (getNomenclatura().getCodManzana() != null) {
      sb.append(getNomenclatura().getCodManzana());
    }
    sb.append(" Mz. ");
    if (getNomenclatura().getManzanaNro() != null) {
      sb.append(getNomenclatura().getManzanaNro());
    }
    if (getNomenclatura().getManzanaLetra() != null) {
      sb.append(getNomenclatura().getManzanaLetra());
    }
    sb.append(" Pc. ");
    if (getNomenclatura().getParcelaNro() != null) {
      sb.append(getNomenclatura().getParcelaNro());
    }
    if (getNomenclatura().getParcelaLetra() != null) {
      sb.append(getNomenclatura().getParcelaLetra());
    }
    if (getNomenclatura().getSubParcela() != null) {
      sb.append(" Poli. ").append(getNomenclatura().getSubParcela());
    }
    return sb.toString();
  }

  public String getRubro() {
    return this.rubro;
  }

  public void setRubro(String rubro) {
    this.rubro = rubro;
  }

  /*
   * public BigDecimal getMontoMinimo() { return this.montoMinimo; }
   * 
   * public void setMontoMinimo(BigDecimal montoMinimo) { this.montoMinimo = montoMinimo; }
   */
  public String getRubroDescripcion() {
    return this.rubroDescripcion;
  }

  public void setRubroDescripcion(String rubroDescripcion) {
    this.rubroDescripcion = rubroDescripcion;
  }

  public String toString() {
    StringBuffer datos = new StringBuffer();
    datos.append("Cuenta ").append(getCuenta()).append(", dv ").append(getDv());
    return datos.toString();
  }

  public String getLocalidad() {
    return this.localidad;
  }

  public void setLocalidad(String localidad) {
    this.localidad = localidad;
  }

  public String getImpuesto() {
    return "IC";
  }

  public String getCalle() {
    return this.calle;
  }

  public void setCalle(String calle) {
    this.calle = calle;
  }

  public String getCodigoPostal() {
    return this.codigoPostal;
  }

  public void setCodigoPostal(String codigoPostal) {
    this.codigoPostal = codigoPostal;
  }

  public String getNumero() {
    return this.numero;
  }

  public void setNumero(String numero) {
    this.numero = numero;
  }

  @Override
  public String getCuentaBarcode() {
    return getCuenta();
  }

  @Override
  public String getDvBarcode() {
    return dv;
  }
}
