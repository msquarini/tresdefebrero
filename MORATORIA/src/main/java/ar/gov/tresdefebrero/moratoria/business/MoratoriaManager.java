package ar.gov.tresdefebrero.moratoria.business;

import java.io.File;
import java.util.List;

import ar.gov.tresdefebrero.moratoria.beans.Cuenta;
import ar.gov.tresdefebrero.moratoria.beans.DeudaBean;
import ar.gov.tresdefebrero.moratoria.beans.MoratoriaBean;
import ar.gov.tresdefebrero.moratoria.beans.PlanPago;
import ar.gov.tresdefebrero.moratoria.beans.SiethCuenta;

public interface MoratoriaManager {

  public MoratoriaBean adhesionMoratoria(Cuenta cuenta, List<DeudaBean> deuda, PlanPago planPago,
      Integer cuotas, Integer codigoMoratoria, String mail);

  // public MoratoriaBean findMoratoria(Integer impuesto, String cuenta, String
  // dv, String patente);

  // public List<MoratoriaBean> findMoratorias(Integer impuesto, String cuenta,
  // String dv, String patente);

  /**
   * Consulta de moratorias que la cuenta presenta adhesion
   * 
   * @param cuentaBean
   * @return
   */
  public List<MoratoriaBean> findMoratorias(Cuenta cuentaBean);

  public void generarVolantePago(MoratoriaBean moratoria, Cuenta bean, File pdf);

  public Double calcularTotalDeuda(List<DeudaBean> deudas);

  public Integer getCodigoMoratoria(String cod);
  
  public List<SiethCuenta> buscarCuentasEmpadronadas(String cuit);
}
