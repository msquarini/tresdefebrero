package ar.gov.tresdefebrero.moratoria.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import ar.gov.tresdefebrero.moratoria.beans.MotoBean;

/**
 * Mapeo de tabla padron_moto
 * 
 * @author msquarini
 *
 */
public class MotoRowMapper implements RowMapper<MotoBean> {

	@Override
	public MotoBean mapRow(ResultSet rs, int rowNum) throws SQLException {
		MotoBean moto = new MotoBean();

		moto.setPatente(rs.getString("patente"));
		moto.setTitular(rs.getString("nombre_apellido"));

		moto.setCalle(rs.getString("direccion"));
		moto.setNumero(rs.getString("altura"));
		moto.setLocalidad(rs.getString("localidad"));
		moto.setCodigoPostal(rs.getString("cod_postal"));

		// auto.setNomenclatura();

		moto.setDescripcionMarca(rs.getString("marca"));
		moto.setModeloAnio(rs.getInt("anio"));
		moto.setAutog(rs.getInt("autog"));
		moto.setUbicacionCatastral(rs.getString("ubic_catastral"));

		return moto;
	}

}