package ar.gov.tresdefebrero.moratoria.batch;

import org.springframework.batch.item.ItemProcessor;

import ar.gov.tresdefebrero.moratoria.beans.MoratoriaBean;
import ar.gov.tresdefebrero.moratoria.dao.MoratoriaDao;

/**
 * Obtiene los registros de deuda original y nueva para la moratoria
 * 
 * @author msquarini
 *
 */
public class MoratoriaProcessor implements ItemProcessor< MoratoriaBean, MoratoriaBean>{

	/**
	 * 
	 */
	private MoratoriaDao dao;
	
	@Override
	public MoratoriaBean process(MoratoriaBean item) throws Exception {
		// Cargar la deuda de la adhesion, original y nueva
		item.setDeudaOriginal(dao.findDeudaMoratoria(item.getIdMoratoria(), "1", 0));
		//item.setDeudaNueva(dao.findDeudaMoratoria(item.getIdMoratoria(), "2", 502));
		item.setDeudaNueva(dao.findDeudaMoratoria(item.getIdMoratoria(), "2", item.getCodMoratoria()));
		
		return item;
	}

	public MoratoriaDao getDao() {
		return dao;
	}

	public void setDao(MoratoriaDao dao) {
		this.dao = dao;
	}

}