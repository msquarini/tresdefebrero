package ar.gov.tresdefebrero.moratoria.batch;

import java.io.File;

import javax.mail.internet.MimeMessage;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;

/**
 * Tarea para el envio x mail del archivo generado
 * 
 * @author msquarini
 *
 */
public class SendFileNotificationTasklet implements Tasklet {

  private String host;
  private String port;
  private String username;
  private String password;
  private String from;
  private String to;

  private JavaMailSenderImpl mailSender = new JavaMailSenderImpl();

  @Override
  public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext)
      throws Exception {
    String fileName = chunkContext.getStepContext().getStepExecution().getJobParameters()
        .getString("fileLocation");
    Long moratoria =
        chunkContext.getStepContext().getStepExecution().getJobParameters().getLong("moratoria");
    try {
      mailSender.setHost(getHost());
      mailSender.setPort(Integer.valueOf(getPort()));

      // Properties props = new Properties();
      // props.setProperty("mail.smtp.auth", "true");
      // props.setProperty("mail.smtp.starttls.enable", "true");
      // mailSender.setJavaMailProperties(props);
      // mailSender.setUsername("");
      // mailSender.setPassword("");

      MimeMessage msg = mailSender.createMimeMessage();
      // use the true flag to indicate you need a multipart message
      MimeMessageHelper helper = new MimeMessageHelper(msg, true);

      helper.setTo(getTo());
      helper.setFrom(getFrom());

      helper.setSubject("Adhesiones moratoria del dia ");
      helper.setText("");

      // let's attach the infamous windows Sample file (this time copied to c:/)
      FileSystemResource file = new FileSystemResource(new File(fileName));
      helper.addAttachment("moratoria-"+moratoria+".txt", file);

      mailSender.send(msg);
    } catch (Exception e) {
      e.printStackTrace();
    }

    return RepeatStatus.FINISHED;
  }

  public String getHost() {
    return host;
  }

  public void setHost(String host) {
    this.host = host;
  }

  public String getPort() {
    return port;
  }

  public void setPort(String port) {
    this.port = port;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getFrom() {
    return from;
  }

  public void setFrom(String from) {
    this.from = from;
  }

  public String getTo() {
    return to;
  }

  public void setTo(String to) {
    this.to = to;
  }

  public JavaMailSenderImpl getMailSender() {
    return mailSender;
  }

  public void setMailSender(JavaMailSenderImpl mailSender) {
    this.mailSender = mailSender;
  }

}
