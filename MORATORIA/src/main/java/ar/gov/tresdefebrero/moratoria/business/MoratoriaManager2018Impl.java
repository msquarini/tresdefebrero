package ar.gov.tresdefebrero.moratoria.business;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import ar.gov.tresdefebrero.moratoria.beans.Cuenta;
import ar.gov.tresdefebrero.moratoria.beans.DeudaBean;
import ar.gov.tresdefebrero.moratoria.beans.MontoMoratoria;
import ar.gov.tresdefebrero.moratoria.beans.MoratoriaBean;
import ar.gov.tresdefebrero.moratoria.beans.PlanPago;
import ar.gov.tresdefebrero.moratoria.beans.SiethCuenta;
import ar.gov.tresdefebrero.moratoria.dao.MoratoriaDao;
import ar.gov.tresdefebrero.moratoria.dao.impl.JdbcSiethDao;
import ar.gov.tresdefebrero.moratoria.util.FechaVencimiento;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

public class MoratoriaManager2018Impl implements MoratoriaManager {

  private static final Logger logger = LoggerFactory.getLogger("MoratoriaManager2018Impl");

  private MoratoriaDao moratoriaDao;

  private JdbcSiethDao siethDao;

  private Integer codMoratoriaPrejudicial;
  private Integer codMoratoriaJudicial;
  private Integer codMoratoriaFiscalizada;

  /**
   * Genera el DVMoratoria a partir del DV original sumando 1,2,3 segun el codigo de moratoria
   * 
   * @param dv
   * @param codMoratoria
   * @return
   */
  /*
   * private String getDVMoratoria(String digitoVerificador, Integer codMoratoria) { Integer
   * dvMoratoria = 0; Integer dv;
   * 
   * if (digitoVerificador.equals("-") || digitoVerificador.equals("")) { dv = 0; } else { dv =
   * Integer.valueOf(digitoVerificador); }
   * 
   * if (codMoratoria.equals(codMoratoriaPrejudicial)) { dvMoratoria = dv + 1; } else if
   * (codMoratoria.equals(codMoratoriaJudicial)) { dvMoratoria = dv + 2; } else if
   * (codMoratoria.equals(codMoratoriaFiscalizada)) { dvMoratoria = dv + 3; } dvMoratoria =
   * dvMoratoria % 10; return dvMoratoria.toString(); }
   */

  public Integer getCodigoMoratoria(String codMoratoria) {
    Integer codigo = -1;
    switch (codMoratoria) {
      case "P":
        codigo = codMoratoriaPrejudicial;
        break;
      case "J":
        codigo = getCodMoratoriaJudicial();
        break;
      case "F":
        codigo = getCodMoratoriaFiscalizada();
        break;
      default:
        throw new IllegalStateException("codigo incorrecto de moratoria");
    }
    return codigo;
  }

  @Transactional
  public List<MoratoriaBean> findMoratorias(Cuenta cuentaBean) {
    logger.info("consulta de moratoria para {}, {}/{}",
        new Object[] {cuentaBean.getImpuesto(), cuentaBean.getCuenta(), cuentaBean.getDv()});
    return this.moratoriaDao.findMoratorias(cuentaBean.getImpuesto(), cuentaBean.getCuenta(),
        cuentaBean.getDv());
  }

  @Transactional
  public MoratoriaBean adhesionMoratoria(Cuenta cuenta, List<DeudaBean> deudas, PlanPago planPago,
      Integer cuotas, Integer codigoMoratoria, String mail) {
    logger.info("adhesion a moratoria {}, {}, {}", cuenta, planPago, cuotas);

    MontoMoratoria montoMoratoria = planPago.calculateDeuda(deudas, cuotas);

    MoratoriaBean moratoria = new MoratoriaBean(cuenta, deudas, planPago, cuotas, montoMoratoria,
        codigoMoratoria, mail, cuenta.getCuit(), cuenta.getEnCaracter(),
        // getDVMoratoria(cuenta.getDvBarcode(), codigoMoratoria));
        cuenta.getDv());

    Integer id = this.moratoriaDao.adhesionMoratoria(moratoria);

    /**
     * Copia los registros de deuda original que se incluyen en la moratoria
     */
    this.moratoriaDao.crearDeudaMoratoria(id, moratoria.getDeudaOriginal(), "1",
        Integer.valueOf(0));
    /**
     * Creacion de las cuotas de la moratoria
     */
    this.moratoriaDao.crearDeudaMoratoria(id, moratoria.getDeudaNueva(), "2",
        moratoria.getCodMoratoria());

    return moratoria;
  }

  /*
   * (non-Javadoc)
   * 
   * @see ar.gov.tresdefebrero.moratoria.business.MoratoriaManager#generarVolantePago(MoratoriaBean,
   * Cuenta, File)
   */
  @Transactional
  public void generarVolantePago(MoratoriaBean moratoria, Cuenta bean, File pdf) {
    try {
      logger.info("Generando volande de pago: {} {}", bean, moratoria);

      String sistema = moratoria.getSistema();
      JasperReport jasperReport = JasperCompileManager
          .compileReport("/www/tomcat/conf/moratoria/moratoria-" + sistema + ".jrxml");

      Map<String, Object> params = new HashMap<String, Object>();
      params.put("bean", bean);
      params.put("planpago", moratoria.getPlanPago());
      params.put("cuotas", moratoria.getCuotas());

      /*
       * Seteo de proximo vencimiento, recorre todas las deudas generadas, actualizando la fecha de
       * proximo vencimiento
       */
      DeudaBean prev = null;
      for (DeudaBean d : moratoria.getDeudaNueva()) {
        if (prev != null) {
          prev.setFechaProximo(d.getFechaVto());
        }
        prev = d;
        /**
         * Seteo los datos de cuenta y dv para la generacion de codigo de barras en la boleta
         */
        d.setCuenta(bean.getCuentaBarcode());
        // Obtengo el DVMoratoria para armar el codigo de barras!!
        // d.setDv(getDVMoratoria(bean.getDvBarcode(), moratoria.getCodMoratoria()));
        d.setDv(bean.getDvBarcode());
        // d.setDv(bean.getDv());
        d.setSistema(bean.getImpuesto());

        System.out.println(d.getBarcode());
      }

      // 20180606 MS - Ajuste para generar solo la deuda no vencida y las no pagas con los intereses
      // correspondientes
      List<DeudaBean> cuotasAGenerar = actualizarDeudasVencidas(moratoria);
      // JRBeanCollectionDataSource ds = new JRBeanCollectionDataSource(moratoria.getDeudaNueva());
      JRBeanCollectionDataSource ds = new JRBeanCollectionDataSource(cuotasAGenerar);

      JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, ds);

      JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));

    } catch (JRException e) {
      logger.error("Generando volande de pago", e);
    } catch (IOException e) {
      logger.error("Generando volande de pago", e);
    }
  }

  /**
   * Chequea cada deuda de la moratoria si esta vencida: no paga y fecha de pago vencido
   * 
   * @param moratoria
   */
  private List<DeudaBean> actualizarDeudasVencidas(MoratoriaBean moratoria) {
    List<DeudaBean> cuotasAGenerar = new ArrayList<>();
    for (DeudaBean deuda : moratoria.getDeudaNueva()) {
      // Si no presenta pago
      if (deuda.getFechaPago() == null) {
        if (deuda.isVencida()) {
          logger.info("Ajustando cuota vencida, {}", deuda);
          ajustarDeuda(deuda);
          moratoriaDao.updateDeudaMoratoria(deuda);
        }
        cuotasAGenerar.add(deuda);
      }
    }
    return cuotasAGenerar;
  }

  /**
   * Ajustar nueva fecha de vto y aplicar intereses al monto 
   * @param deuda
   */
  private void ajustarDeuda(DeudaBean deuda) {
    Calendar now = Calendar.getInstance();
    now.add(Calendar.DATE, 5);
    // Nuevo vencimiento
    Date nuevoVencimiento = FechaVencimiento.proximoHabil(now.getTime());

    // Cantidad de dias entre vencimientos para aplicar interes 1% mensual
    //long diffInMillies = Math.abs(nuevoVencimiento.getTime() - deuda.getFechaVto().getTime());
    // 20181214 FIX Interes - Calculo desde la fecha de vencimiento original
    long diffInMillies = Math.abs(nuevoVencimiento.getTime() - deuda.getFechaVto2().getTime());
    
    long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);

    // tasa 1% mensual
    double tasa = 0.01 / 30;
    BigDecimal tasaDiaria = BigDecimal.valueOf(tasa * diff);
    
    // aplicamos la tasa de interes por el monto para obtener el interes
    // deuda.setIntereses(tasaDiaria.multiply(deuda.getImporte()).setScale(2,RoundingMode.DOWN));
    deuda.setIntereses(tasaDiaria.multiply(deuda.getImporteOriginal()).setScale(2,RoundingMode.DOWN));
    deuda.setFechaVto(nuevoVencimiento);
  }

  /**
   * Calculo de total de deuda segun registros de base (NO tiene relevancia ya que solo se obtienen
   * los registros de D de la Base de datos)
   */
  public Double calcularTotalDeuda(List<DeudaBean> deudas) {
    double totalDebitos = 0.0D;
    double totalCreditos = 0.0D;
    if (deudas != null) {
      for (DeudaBean d : deudas) {
        if (d.getDebcre().equals("D")) {
          totalDebitos += d.getImporte().doubleValue();
        }
      }
    }
    return (Double.valueOf(totalDebitos - totalCreditos));
  }

  public List<SiethCuenta> buscarCuentasEmpadronadas(String cuit) {
    logger.info("buscarCuentasEmpadronadas cuit: {}", cuit);
    return siethDao.findCuentas(cuit);
  }

  public MoratoriaDao getMoratoriaDao() {
    return moratoriaDao;
  }

  public void setMoratoriaDao(MoratoriaDao moratoriaDao) {
    this.moratoriaDao = moratoriaDao;
  }

  public Integer getCodMoratoriaPrejudicial() {
    return codMoratoriaPrejudicial;
  }

  public void setCodMoratoriaPrejudicial(Integer codMoratoriaPrejudicial) {
    this.codMoratoriaPrejudicial = codMoratoriaPrejudicial;
  }

  public Integer getCodMoratoriaJudicial() {
    return codMoratoriaJudicial;
  }

  public void setCodMoratoriaJudicial(Integer codMoratoriaJudicial) {
    this.codMoratoriaJudicial = codMoratoriaJudicial;
  }

  public Integer getCodMoratoriaFiscalizada() {
    return codMoratoriaFiscalizada;
  }

  public void setCodMoratoriaFiscalizada(Integer codMoratoriaFiscalizada) {
    this.codMoratoriaFiscalizada = codMoratoriaFiscalizada;
  }

  public JdbcSiethDao getSiethDao() {
    return siethDao;
  }

  public void setSiethDao(JdbcSiethDao siethDao) {
    this.siethDao = siethDao;
  }
}
