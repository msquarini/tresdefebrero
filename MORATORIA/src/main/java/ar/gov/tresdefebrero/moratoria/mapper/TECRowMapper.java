package ar.gov.tresdefebrero.moratoria.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import ar.gov.tresdefebrero.moratoria.beans.ABLBean;
import ar.gov.tresdefebrero.moratoria.beans.AutomotorBean;
import ar.gov.tresdefebrero.moratoria.beans.TECBean;

/**
 * Mapeo de tabla padron_tsg
 * 
 * @author msquarini
 *
 */
public class TECRowMapper implements RowMapper<TECBean> {

	@Override
	public TECBean mapRow(ResultSet rs, int rowNum) throws SQLException {
		TECBean tec = new TECBean();
		
		tec.setCuenta(rs.getInt("cuenta"));
		tec.setDv(rs.getString("digito"));
		
		//auto.setNomenclatura();
		
		tec.setCalle(rs.getString("nombrecalle"));
		tec.setNumero(rs.getString("nropuerta"));
		tec.setPiso(rs.getString("piso"));
		tec.setDepartamento(rs.getString("dpto"));
		tec.setCodigoPostal(rs.getString("localidad"));
		tec.setLocalidad(rs.getString("localidad_desc"));
		tec.setTitular(rs.getString("appynom"));
		
		//abl.setCategoria(rs.getInt("categoria"));
		//abl.setValuacionTotal(rs.getBigDecimal("vtotal"));
		
		tec.setNomenclatura(NomenclaturaMapper.map(rs));
		return tec;
	}

}
