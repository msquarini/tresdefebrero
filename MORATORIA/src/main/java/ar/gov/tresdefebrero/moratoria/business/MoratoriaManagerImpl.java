package ar.gov.tresdefebrero.moratoria.business;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import ar.gov.tresdefebrero.moratoria.beans.Cuenta;
import ar.gov.tresdefebrero.moratoria.beans.DeudaBean;
import ar.gov.tresdefebrero.moratoria.beans.MontoMoratoria;
import ar.gov.tresdefebrero.moratoria.beans.MoratoriaBean;
import ar.gov.tresdefebrero.moratoria.beans.PlanPago;
import ar.gov.tresdefebrero.moratoria.dao.MoratoriaDao;

public class MoratoriaManagerImpl  {
  private static final Logger logger = LoggerFactory.getLogger("MoratoriaManagerImpl");
  private MoratoriaDao moratoriaDao;


  /**
   * Codigo de moratoria vigente
   */
  private Integer codMoratoria;

//  @Transactional
//  @Deprecated
//  public MoratoriaBean findMoratoria(Integer impuesto, String cuenta, String dv, String patente) {
//    String sistema = "XP";
//    switch (impuesto.intValue()) {
//      case 1:
//        sistema = "XP";
//        cuenta = "0" + patente.substring(0, 5);
//        dv = patente.substring(5);
//        break;
//      case 2:
//        sistema = "PV";
//        cuenta = patente;
//        dv = "";
//        break;
//      case 3:
//        sistema = "AL";
//        break;
//      case 4:
//        sistema = "IC";
//        break;
//    }
//    logger.info("consulta de moratoria para {}, {}/{}", new Object[] {sistema, cuenta, dv});
//
//    return this.moratoriaDao.findMoratoria(sistema, cuenta, dv);
//  }

  @Transactional
  public List<MoratoriaBean> findMoratorias(Cuenta cuentaBean) {    
    logger.info("consulta de moratoria para {}, {}/{}", new Object[] {cuentaBean.getImpuesto(), cuentaBean.getCuenta(), cuentaBean.getDv()});
    return this.moratoriaDao.findMoratorias(cuentaBean.getImpuesto(), cuentaBean.getCuenta(), cuentaBean.getDv());
  }
  
  @Transactional
  @Deprecated
  public List<MoratoriaBean> findMoratorias(Integer impuesto, String cuenta, String dv,
      String patente) {
    String sistema = "XP";
    switch (impuesto.intValue()) {
      case 1:
        sistema = "XP";
        cuenta = "0" + patente.substring(0, 5);
        dv = patente.substring(5);
        break;
      case 2:
        sistema = "PV";
        cuenta = patente;
        dv = "";
        break;
      case 3:
        sistema = "AL";
        break;
      case 4:
        sistema = "IC";
        break;
    }
    logger.info("consulta de moratoria para {}, {}/{}", new Object[] {sistema, cuenta, dv});

    return this.moratoriaDao.findMoratorias(sistema, cuenta, dv);
  }

  @Transactional
  public MoratoriaBean adhesionMoratoria(Cuenta cuenta, List<DeudaBean> deudas, PlanPago planPago,
      Integer cuotas, String mail, Integer codigoMoratoria) {
    logger.info("adhesion a moratoria {}, {}, {}", cuenta, planPago, cuotas);

    MontoMoratoria montoMoratoria = planPago.calculateDeuda(deudas, cuotas);

    MoratoriaBean moratoria = null;
        //new MoratoriaBean(cuenta, deudas, planPago, cuotas, montoMoratoria, codigoMoratoria, mail);

    Integer id = this.moratoriaDao.adhesionMoratoria(moratoria);

    /**
     * Copia los registros de deuda original que se incluyen en la moratoria
     */
    this.moratoriaDao.crearDeudaMoratoria(id, moratoria.getDeudaOriginal(), "1",
        Integer.valueOf(0));
    /**
     * Creacion de las cuotas de la moratoria
     */
    this.moratoriaDao.crearDeudaMoratoria(id, moratoria.getDeudaNueva(), "2",
        moratoria.getCodMoratoria());

    return moratoria;
  }

  public MoratoriaDao getMoratoriaDao() {
    return this.moratoriaDao;
  }

  public void setMoratoriaDao(MoratoriaDao moratoriaDao) {
    this.moratoriaDao = moratoriaDao;
  }

  public Integer getCodMoratoria() {
    return codMoratoria;
  }

  public void setCodMoratoria(Integer codMoratoria) {
    this.codMoratoria = codMoratoria;
  }
}
