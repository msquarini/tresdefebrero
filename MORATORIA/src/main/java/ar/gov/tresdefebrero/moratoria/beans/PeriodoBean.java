package ar.gov.tresdefebrero.moratoria.beans;

import java.util.Date;

/**
 * Periodo de pago de un impuesto
 * 
 * @author UltraBook
 *
 */
public class PeriodoBean {

    private Integer impuesto;
    private Integer periodo;
    private Integer anio;

    /**
     * Fecha de vencimiento del impuesto (pdf)
     */
    private Date fechaVto;

    /**
     * fecha de pago (pdf)
     */
    private Date fechaPago;

    /**
     * fecha de vigencia del periodo
     */
    private Date fechaVigencia;

    /**
     * fecha de proximo vencimiento del impuesto (pdf)
     */
    private Date fechaProximoPago;

    /**
     * fecha de codigo de barras
     */
    private Date fechaBarCode;

    /**
     * @return the impuesto
     */
    public Integer getImpuesto() {
        return impuesto;
    }

    /**
     * @param impuesto
     *            the impuesto to set
     */
    public void setImpuesto(Integer impuesto) {
        this.impuesto = impuesto;
    }

    /**
     * @return the periodo
     */
    public Integer getPeriodo() {
        return periodo;
    }

    /**
     * @param periodo
     *            the periodo to set
     */
    public void setPeriodo(Integer periodo) {
        this.periodo = periodo;
    }

    /**
     * @return the anio
     */
    public Integer getAnio() {
        return anio;
    }

    /**
     * @param anio
     *            the anio to set
     */
    public void setAnio(Integer anio) {
        this.anio = anio;
    }

    /**
     * @return the fechaVto
     */
    public Date getFechaVto() {
        return fechaVto;
    }

    /**
     * @param fechaVto
     *            the fechaVto to set
     */
    public void setFechaVto(Date fechaVto) {
        this.fechaVto = fechaVto;
    }

    /**
     * @return the fechaPago
     */
    public Date getFechaPago() {
        return fechaPago;
    }

    /**
     * @param fechaPago
     *            the fechaPago to set
     */
    public void setFechaPago(Date fechaPago) {
        this.fechaPago = fechaPago;
    }

    /**
     * @return the fechaVigencia
     */
    public Date getFechaVigencia() {
        return fechaVigencia;
    }

    /**
     * @param fechaVigencia
     *            the fechaVigencia to set
     */
    public void setFechaVigencia(Date fechaVigencia) {
        this.fechaVigencia = fechaVigencia;
    }

    /**
     * @return the fechaProximoPago
     */
    public Date getFechaProximoPago() {
        return fechaProximoPago;
    }

    /**
     * @param fechaProximoPago
     *            the fechaProximoPago to set
     */
    public void setFechaProximoPago(Date fechaProximoPago) {
        this.fechaProximoPago = fechaProximoPago;
    }

    /**
     * @return the fechaBarCode
     */
    public Date getFechaBarCode() {
        return fechaBarCode;
    }

    /**
     * @param fechaBarCode
     *            the fechaBarCode to set
     */
    public void setFechaBarCode(Date fechaBarCode) {
        this.fechaBarCode = fechaBarCode;
    }

}
