package ar.gov.tresdefebrero.moratoria.beans;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Deuda de impuestos
 * 
 * @author admin
 *
 */
public class MoratoriaBeanBACK {

	private Integer idMoratoria;
	private PlanPago planPago;
	private String cuenta;
	private String dv;
	private String sistema;
	private Integer cuotas;
	private BigDecimal deudaTotal;
	private BigDecimal montoEfectivo;
	private BigDecimal montoCuota;
	private Date fechaAdhesion;
	List<DeudaBean> deudaOriginal;
	List<DeudaBean> deudaNueva;
	private Integer registraPago;
	private String estado;

	// Identificador de moratoria (502 la primera)
	private Integer codMoratoria;
	// Mail de cuenta recibido x link
	private String mail;
	
	public MoratoriaBeanBACK() {
	}

	public MoratoriaBeanBACK(Cuenta cuentaBean, List<DeudaBean> deuda, PlanPago planPago, Integer cuotas,
			MontoMoratoria montos, Integer codMoratoria, String mail) {
		this.cuenta = cuentaBean.getCuenta();
		this.dv = cuentaBean.getDv();
		this.sistema = cuentaBean.getImpuesto();

		this.planPago = planPago;

		this.deudaOriginal = deuda;

		this.cuotas = cuotas;

		this.montoEfectivo = montos.getEfectivo();
		this.deudaTotal = montos.getDeudaOriginal();
		this.montoCuota = montos.getMontoCuota();

		this.fechaAdhesion = new Date();

		this.codMoratoria = codMoratoria;
		this.mail = mail;
		generarDeuda(montos.getMontoCuota());
	}

	public String getCuenta() {
		return cuenta;
	}

	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}

	public String getDv() {
		return dv;
	}

	public void setDv(String dv) {
		this.dv = dv;
	}

	public String getSistema() {
		return sistema;
	}

	public void setSistema(String sistema) {
		this.sistema = sistema;
	}

	public Integer getCuotas() {
		return cuotas;
	}

	public void setCuotas(Integer cuotas) {
		this.cuotas = cuotas;
	}

	/**
	 * Registros de deuda original y nueva para la interaz de notificacion a
	 * mainframe (BATCH)
	 * 
	 * @return
	 */

	public List<DeudaBean> getDeudaMoratoria() {
		List<DeudaBean> deudas = new ArrayList();
		for (DeudaBean d : getDeudaOriginal()) {
			d.setSistema(getSistema());

			/*
			 * Ajuste por motos que tienen patente de 6 posiciones
			 */
			if (getSistema().equals("PV") && getCuenta().length() == 6) {
				d.setCuenta("0" + getCuenta().substring(0, 5));
				d.setDv(getCuenta().substring(5));
			} else {
				d.setCuenta(getCuenta());
				d.setDv(getDv());
			}

			deudas.add(d);
		}
		for (DeudaBean d : getDeudaNueva()) {
			d.setSistema(getSistema());
			/*
			 * Ajuste por motos que tienen patente de 6 posiciones
			 */
			if (getSistema().equals("PV") && getCuenta().length() == 6) {
				d.setCuenta("0" + getCuenta().substring(0, 5));
				d.setDv(getCuenta().substring(5));
			} else {
				d.setCuenta(getCuenta());
				d.setDv(getDv());
			}

			deudas.add(d);
		}
		return deudas;
	}

	private void generarDeuda(BigDecimal valorCuota) {
		this.deudaNueva = new ArrayList();

		Date desde = getFechaAdhesion();

		
		// Tratamiento especial para XP y PV (patentes) para utilizar solo
		// numeracion de cuotas de 1 a 12 por año
		
		// 20170609 Se pidio mismo tratamiento para todos los impuestos de cuotas corridas!!!!
	/*	if ("PV".equalsIgnoreCase(getSistema()) || "XP".equalsIgnoreCase(getSistema())) {
			int nrocuota = 1;

			if (this.montoEfectivo.doubleValue() > 0.0D) {
				if (this.cuotas.intValue() > 12) {
					nrocuota = 13;
				} else {
					nrocuota = this.cuotas.intValue() + 1;
				}
				// DeudaBean cuotaEfectivo = new DeudaBean(true, true,
				// Integer.valueOf(nrocuota), desde, Integer.valueOf(502), "2");

				// 20170513 Codigo de moratoria dinamico, para utilizar en distintas moratorias!!
				DeudaBean cuotaEfectivo = new DeudaBean(true, true, Integer.valueOf(nrocuota), desde, getCodMoratoria(),
						"2");
				
				cuotaEfectivo.setImporte(this.montoEfectivo);

				desde = cuotaEfectivo.getFechaVto();
				this.deudaNueva.add(cuotaEfectivo);
			}
			nrocuota = 1;
			for (int i = 1; i <= this.cuotas.intValue(); i++) {
				DeudaBean d;
				// Si es la primer cuota (ya se sea efectivo o cuota) se aplican
				// 10 dias a partir de hoy de vencimiento

				// 20170513 Codigo de moratoria dinamico, para utilizar en distintas moratorias!!
				if (this.deudaNueva.isEmpty()) {
					//d = new DeudaBean(true, true, Integer.valueOf(nrocuota), desde, Integer.valueOf(502), "2");
					d = new DeudaBean(true, true, Integer.valueOf(nrocuota), desde, getCodMoratoria(), "2");
				} else {
					//d = new DeudaBean(true, false, Integer.valueOf(nrocuota), desde, Integer.valueOf(502), "2");
					d = new DeudaBean(true, false, Integer.valueOf(nrocuota), desde, getCodMoratoria(), "2");
				}

				d.setImporte(valorCuota);
				this.deudaNueva.add(d);

				if (cambioAnio(d.getFechaVto())) {
					nrocuota = 1;
				} else {
					nrocuota++;
				}
				desde = d.getFechaVto();
			}
		} else {*/
			// Tratamiento estandar de calculo de cuotas para impuestos que no
			// sean XP o PV
			if (this.montoEfectivo.doubleValue() > 0.0D) {
				//DeudaBean cuotaEfectivo = new DeudaBean(true, true, Integer.valueOf(this.cuotas.intValue() + 1), desde, Integer.valueOf(502), "2");
				DeudaBean cuotaEfectivo = new DeudaBean(true, true, Integer.valueOf(this.cuotas.intValue() + 1), desde, getCodMoratoria(), "2");
				
				cuotaEfectivo.setImporte(this.montoEfectivo);
				desde = cuotaEfectivo.getFechaVto();
				this.deudaNueva.add(cuotaEfectivo);
			}
			for (int i = 1; i <= this.cuotas.intValue(); i++) {
				DeudaBean d;
				// Si es la primer cuota (ya se sea efectivo o cuota) se aplican
				// 10 dias a partir de hoy de vencimiento
				if (this.deudaNueva.isEmpty()) {
					//d = new DeudaBean(true, true, Integer.valueOf(i), desde, Integer.valueOf(502), "2");
					d = new DeudaBean(true, true, Integer.valueOf(i), desde, getCodMoratoria(), "2");
				} else {
					//d = new DeudaBean(true, false, Integer.valueOf(i), desde, Integer.valueOf(502), "2");
					d = new DeudaBean(true, false, Integer.valueOf(i), desde, getCodMoratoria(), "2");
				}

				d.setImporte(valorCuota);
				this.deudaNueva.add(d);

				desde = d.getFechaVto();
			}
		//}
	}

	public Integer getIdMoratoria() {
		return idMoratoria;
	}

	public void setIdMoratoria(Integer idMoratoria) {
		this.idMoratoria = idMoratoria;
	}

	public BigDecimal getMontoEfectivo() {
		return montoEfectivo;
	}

	public void setMontoEfectivo(BigDecimal montoEfectivo) {
		this.montoEfectivo = montoEfectivo;
	}

	public void setMontoCuota(BigDecimal montoCuota) {
		this.montoCuota = montoCuota;
	}

	public Date getFechaAdhesion() {
		return fechaAdhesion;
	}

	public void setFechaAdhesion(Date fechaAdhesion) {
		this.fechaAdhesion = fechaAdhesion;
	}

	public PlanPago getPlanPago() {
		return planPago;
	}

	public void setPlanPago(PlanPago planPago) {
		this.planPago = planPago;
	}

	public List<DeudaBean> getDeudaOriginal() {
		return deudaOriginal;
	}

	public void setDeudaOriginal(List<DeudaBean> deudaOriginal) {
		this.deudaOriginal = deudaOriginal;
	}

	public List<DeudaBean> getDeudaNueva() {
		return deudaNueva;
	}

	public void setDeudaNueva(List<DeudaBean> deudaNueva) {
		this.deudaNueva = deudaNueva;
	}

	public Integer getRegistraPago() {
		return registraPago;
	}

	public void setRegistraPago(Integer registraPago) {
		this.registraPago = registraPago;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public void setDeudaTotal(BigDecimal deudaTotal) {
		this.deudaTotal = deudaTotal;
	}

	public BigDecimal getDeudaTotal() {
		return deudaTotal;
	}

	public BigDecimal getMontoCuota() {
		return montoCuota;
	}

	private boolean cambioAnio(Date a) {
		Calendar calA = Calendar.getInstance();
		calA.setTime(a);
		return calA.get(Calendar.MONTH) == 11; // diciembre
	}

	public Integer getCodMoratoria() {
		return codMoratoria;
	}

	public void setCodMoratoria(Integer codMoratoria) {
		this.codMoratoria = codMoratoria;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}
}