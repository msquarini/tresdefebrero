package net.sourceforge.barbecue.env;

import java.awt.*;

/**
 * * ILIN02 Comment * We need to overwrite this class to fix a bug on linux *
 * where no barcode text is printed in EAN13 barcode * * An environment used
 * when the machine running barbecue is in * headless mode, ie no monitor or
 * windowing system. * * @author Ian Bourke
 */
public final class HeadlessEnvironment implements Environment {
    /**
     * * The default output resolution (in DPI) for barcodes in headless mode.
     */
    public static final int DEFAULT_RESOLUTION = 72;
    public static final Font DEFAULT_FONT = new Font("Arial", Font.PLAIN, 20);

    /**
     * * Returns the environment determined resolution for * outputting
     * barcodes. * @return The resolution for the environment
     */
    public int getResolution() {
        return DEFAULT_RESOLUTION;
    }

    /** * Returns the default font for the environment. * @return null */
    public Font getDefaultFont() {
        return DEFAULT_FONT;
    }
}