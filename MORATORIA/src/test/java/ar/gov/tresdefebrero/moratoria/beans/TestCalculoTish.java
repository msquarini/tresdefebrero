package ar.gov.tresdefebrero.moratoria.beans;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class TestCalculoTish {

	public static void main(String[] args) {

		DeudaTishBean d = new DeudaTishBean();
		d.setMinimo(new BigDecimal(100));
		d.setImporte(new BigDecimal(50));
		
		System.out.println(((DeudaBean)d).getImporte());
	}
	
	public static List<DeudaBean> generarDeudas() {
		List<DeudaBean> deudas = new ArrayList<DeudaBean>();
		
		Calendar c = Calendar.getInstance();
		c.set(Calendar.YEAR, 2014);
		c.set(Calendar.MONTH, 5);
		
		DeudaBean d1 = new DeudaBean();
		d1.setDebcre("D");
		d1.setFechaVto(c.getTime());
		d1.setImporte(BigDecimal.valueOf(500));
		System.out.println("Deuda " + d1.getImporte() + " -- " + d1.getFechaVto());
		deudas.add(d1);
		
		c.set(Calendar.MONTH, 6);
		DeudaBean d2 = new DeudaBean();
		d2.setDebcre("D");
		d2.setFechaVto(c.getTime());
		d2.setImporte(BigDecimal.valueOf(500));
		System.out.println("Deuda " + d2.getImporte() + " -- " + d2.getFechaVto());
		deudas.add(d2);
		
		c.set(Calendar.MONTH, 7);
		DeudaBean d3 = new DeudaBean();
		d3.setDebcre("C");
		d3.setCuota(4);
		d3.setFechaVto(c.getTime());
		d3.setImporte(BigDecimal.valueOf(500));
		d3.setDv(null);
		d3.setCuenta("EHN926");
		d3.setMora(716);
		d3.setAnio(2016);
		System.out.println("Deuda " + d3.getImporte() + " -- " + d3.getFechaVto());
		deudas.add(d3);
		
//		d3.setCuenta("200867");
//		d3.setDv("4");
//		System.out.println(d3.getBarcode());
		return deudas;
	}

}
