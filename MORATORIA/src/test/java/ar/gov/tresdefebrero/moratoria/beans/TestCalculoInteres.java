package ar.gov.tresdefebrero.moratoria.beans;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

public class TestCalculoInteres {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Calendar now = Calendar.getInstance();
		now.set(Calendar.DATE, 20);
		Calendar old = Calendar.getInstance();
		old.set(Calendar.YEAR, 2018);
		old.set(Calendar.MONTH, 3);
		old.set(Calendar.DATE, 30);
		long diffInMillies = Math.abs(now.getTime().getTime() - old.getTime().getTime());
	    
	    long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);

	    // tasa 1% mensual
	    double tasa = 0.01 / 30;
	    BigDecimal tasaDiaria = BigDecimal.valueOf(tasa * diff);
	    
	    System.out.println("DIff " + diff);
	    System.out.println("tasadiaria " + tasaDiaria);
	    BigDecimal interes = tasaDiaria.multiply(BigDecimal.valueOf(145.84D)).setScale(2,RoundingMode.DOWN);
	    System.out.println("Interes " + interes);
	    System.out.println("Total " + (interes.add(BigDecimal.valueOf(145.84D))));

	}

}
