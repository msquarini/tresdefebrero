package ar.gov.tresdefebrero.moratoria.beans;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

public class TestReport {

	public static void main(String[] args) {

		PlanPago pp = new PlanPago();
		pp.setDescripcion("pp xxxx xx x xxxxx");

		/*
		 * AutomotorBean b = new AutomotorBean(); b.setDominioActual("AAA222");
		 * b.setDescripcionMarca("ford"); b.setDescripcionModelo("ecosport");
		 * b.setModeloAnio(2010);
		 */
		ABLBean b = new ABLBean();
		b.setCuenta(12345);
		b.setDv("5");
		b.setCalle("calle sdsdsdsdsds");
		b.setLocalidad("ttototot");
		b.setNumero("223");
		//b.setValuacionTotal(BigDecimal.valueOf(232323.43));
		b.setTitular("juananana");
		//b.setCategoria(3);
		
		Nomenclatura n = new Nomenclatura();
		n.setCircunscripcion(1);
		n.setSeccion("AA");
		n.setCodManzana(2);
		n.setManzanaNro(23);
		n.setManzanaLetra("B");
		n.setParcelaNro(12);
		n.setParcelaLetra("E");
		n.setSubParcela("3322");
		b.setNomenclatura(n);

		try {
			JasperReport jasperReport = JasperCompileManager
					.compileReport("/www/tomcat/conf/moratoria/moratoria-al.jrxml");

			Map<String, Object> params = new HashMap<String, Object>();
			params.put("bean", b);
			params.put("planpago", pp);

			List<DeudaBean> lista = new ArrayList<DeudaBean>();
			DeudaBean deuda = new DeudaBean();
			deuda.setAnio(2016);
			deuda.setCuota(1);
			deuda.setFechaVto(new Date());
			deuda.setImporte(BigDecimal.valueOf(233.4));
			lista.add(deuda);
			deuda = new DeudaBean();
			deuda.setAnio(2016);
			deuda.setCuota(2);
			Calendar c = Calendar.getInstance();
			c.add(Calendar.DATE, 30);
			deuda.setFechaVto(c.getTime());
			deuda.setImporte(BigDecimal.valueOf(233.4));
			lista.add(deuda);
			/*
			 * Seteo de proximo vencimiento, recorre todas las deudas generados,
			 * actualizando la fecha
			 */
			DeudaBean prev = null;
			for (DeudaBean d : lista) {
				if (prev != null) {
					prev.setFechaProximo(d.getFechaVto());
				}
				prev = d;
			}

			JRBeanCollectionDataSource ds = new JRBeanCollectionDataSource(lista);
			// Ejecucion de fillReport
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, ds);

			// Export
			File pdf = File.createTempFile("cuenta", ".pdf");
			JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
			System.out.println(pdf.getAbsolutePath());
		} catch (JRException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
