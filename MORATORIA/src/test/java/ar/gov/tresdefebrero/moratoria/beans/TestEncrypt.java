package ar.gov.tresdefebrero.moratoria.beans;

import java.net.URLDecoder;
import java.net.URLEncoder;

import ar.gov.tresdefebrero.moratoria.util.CipherHelper;

public class TestEncrypt {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String enc = CipherHelper.encryt("maildeprueba@aaa.com");
		System.out.println("Encriptado: " + enc);
		String urlenc = URLEncoder.encode(enc);
		System.out.println("Encriptado: " + urlenc);
		
		String plain = CipherHelper.decryp(URLDecoder.decode(urlenc));
		System.out.println("Plano " + plain);
	}

}
