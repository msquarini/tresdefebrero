package ar.gov.tresdefebrero.moratoria.beans;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import ar.gov.tresdefebrero.moratoria.batch.RunBatch;

public class TestBatch {

	public static void main(String[] args) {
		TestBatch tb = new TestBatch();
		 tb.formater();

		ApplicationContext context = new ClassPathXmlApplicationContext("application-context.xml");
		RunBatch runBatch = (RunBatch) context.getBean("runScheduler");
		runBatch.run();
	}

	public void formater() {
		// String template =
		// "%1$2s%2$06d%3$1s%4$03d%5$4d%6$03d%7$1d%8$09.2d%9$1s%10$1d%11$03d%12$tYtmtd";
		String template = "%2s%6s%1s%03d%4d%03d%1d%09.2f%1s%1s%03d%12$tY%12$tm%12$td";

		Object[] datos = { "PV", "123AAA", "", 716, 2016, 12, 5, BigDecimal.valueOf(145.98), "D", 2, 7, new Date() };
		// Object[] datos = {"XP", 123456 , "-", 716, 2016, 12, 5, 100.55, "D",
		// 2, 7, new Date()};
	}
}
