package ar.gov.tresdefebrero.moratoria.beans;

import java.util.Calendar;

import ar.gov.tresdefebrero.moratoria.util.FechaVencimiento;

public class TestFechaHabil {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Calendar now = Calendar.getInstance();
		System.out.println(FechaVencimiento.proximoHabil(now.getTime()));
		now.add(Calendar.DATE, 1);
		System.out.println(FechaVencimiento.proximoHabil(now.getTime()));
		now.add(Calendar.DATE, 2);
		System.out.println(FechaVencimiento.proximoHabil(now.getTime()));
	}

}
