﻿-- Table: public.padron_moto

-- DROP TABLE public.padron_moto;

CREATE TABLE public.padron_moto
(
  patente text,
  nombre_apellido text,
  tipo_doc integer,
  nro_doc integer,
  ubic_catastral text,
  cod_calle text,
  direccion text,
  altura integer,
  localidad text,
  cod_postal integer,
  piso text,
  utilidad text,
  marca text,
  anio integer,
  cilindrada integer,
  nro_motor text,
  uso text,
  tipo text,
  ddjj text,
  importada text,
  nombre_vendedor text,
  libre_prenda text,
  factura text,
  fecha_entrega_patente date,
  autoriza_menor text,
  verifica_motor text,
  tiene_apoderado text,
  esta_sucesion text,
  baja text,
  cp_nueva_radica integer,
  recibo_pate integer,
  comprobante integer,
  tiene_prenda integer,
  tiene_certificado integer,
  liberada_mpio_ant integer,
  fecha_alta date,
  fecha_baja date
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.padron_moto
  OWNER TO postgres;
