CREATE TABLE public.adhesion_moratoria
(
  id_adhesion serial CONSTRAINT pk_adhesion_moratoria PRIMARY KEY,
  sistema text not NULL,
  cuenta text not NULL,
  dv text,
  total numeric,
  id_plan_pago integer,
  cuotas integer,
  fecha date,
  monto_efectivo numeric,
  registra_pago integer  
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.adhesion_moratoria
  OWNER TO postgres;
ALTER TABLE public.adhesion_moratoria ADD estado text NULL DEFAULT 'A';
ALTER TABLE public.adhesion_moratoria ADD importe numeric NULL;

CREATE TABLE public.moratoria_deuda
(
  id_adhesion integer,
  anio integer,
  cuota integer,
  monto_cuota numeric,
  fecha_vto date  
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.moratoria_deuda
  OWNER TO postgres;
ALTER TABLE public.moratoria_deuda ADD importe numeric NULL;
ALTER TABLE public.moratoria_deuda ADD ajuste INTEGER NULL;
ALTER TABLE public.moratoria_deuda ADD debcre text NULL;
ALTER TABLE public.moratoria_deuda ADD cod_adm integer NULL;

alter table public.moratoria_deuda
    add constraint fk_adhesion_moratoria
    foreign key (id_adhesion) 
    REFERENCES adhesion_moratoria(id_adhesion);
    
    
CREATE TABLE public.plan_pago
(
  id_plan_pago serial CONSTRAINT pk_plan_pago PRIMARY KEY,
  descripcion text,
  porc_efectivo numeric,
  cuotas_desde integer,
  cuotas_hasta integer,
  interes_financiero NUMERIC,
  interes_mora NUMERIC,
  fecha date,
  activo boolean
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.plan_pago
  OWNER TO postgres;    