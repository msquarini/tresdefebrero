﻿-- Table: public.padron_automotor

-- DROP TABLE public.padron_automotor;

-- Table: public.deuda

-- DROP TABLE public.deuda;

CREATE TABLE public.deuda
(
  sistema text,
  cuenta text,
  dv text,
  mora integer,
  anio integer,
  cuota integer,
  ajuste text,
  importe numeric,
  debcre text,
  marca integer,
  cod_adm integer,
  fecha_vto date
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.deuda
  OWNER TO postgres;


CREATE TABLE public.padron_automotor
(
  patente text,
  circ integer,
  sec text,
  cod_mz integer,
  mz integer,
  mzl text,
  pc integer,
  pcl text,
  uf text,
  dominio_original text,
  dominio_actual text,
  fecha_ult_dj_pt date,
  fecha_alta_pate date,
  fecha_tributa_pate date,
  modelo_anio integer,
  tipo_patente text,
  uso_patente text,
  peso_patente integer,
  carga_patente integer,
  cod_marca_pate text,
  descrip_marca_pate text,
  descrip_modelo_pate text,
  cod_recu_pate text,
  nacionalidad_pate integer,
  categoria_pate text,
  inciso_pate text,
  fecha_baja_pate date,
  codigo_baja_pate text,
  marca_motor_pate text,
  serie_motor_pate text,
  mro_motor_pate text,
  mro_chasis_pate text,
  tipo_combustible text,
  tipo_nro_doc_pate integer,
  valaucion_pate numeric,
  nro_calle_pate text,
  nombre_calle_pate text,
  nro_puerta_pate numeric,
  apellido_nombre text,
  cod_post_fiscal numeric,
  piso text,
  dpto text,
  tel1 numeric,
  tel2 numeric,
  tel3 numeric,
  cuit numeric,
  nro_calle text,
  nombre_calle text,
  nro_puerta numeric,
  piso2_pate text,
  dpto2 text,
  localidad_pate numeric,
  desti_postal text,
  fecha_postal date,
  tel1_posta numeric,
  tel2_posta numeric,
  tel3_posta numeric,
  cuit_postal numeric,
  cod_mpal numeric,
  vtv_al_dia text,
  importe_cuota numeric,
  importe_anual numeric,
  fecha_vto_vtv date,
  marca_emitir text,
  marca_nuevo text,
  fecha_incorporacion date,
  autog numeric,
  cuenta_abl text
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.padron_automotor
  OWNER TO postgres;




-- Table: public.padron_sh

-- DROP TABLE public.padron_sh;

CREATE TABLE public.padron_sh
(
  cuenta integer,
  digito text,
  num_exp integer,
  let_exp text,
  anio_exp text,
  cant_personal integer,
  razon_social text,
  rubro integer,
  superficie integer,
  circ integer,
  sec text,
  cod_mz integer,
  mz text,
  mzl text,
  pc integer,
  pcl text,
  uf text,
  calle_codigo text,
  propietario_codigo text,
  localidad_codigo text,
  cod_registra_deuda text,
  cod_cierre_definitivo text,
  acta_infraccion text,
  cert_deuda text,
  liquidado text,
  no_abono text,
  zona text,
  tipo_doc text,
  nro_doc text,
  fechabaja date,
  terminal_baja text,
  usuario_baja text,
  cod_gran_med_peq_contri text,
  iibb_viejo text,
  cuit text,
  importe_habili numeric,
  recibo_habi text,
  marca_veraz text,
  cod_operativo text
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.padron_sh
  OWNER TO postgres;
-- Table: public.padron_tsg

-- DROP TABLE public.padron_tsg;

CREATE TABLE public.padron_tsg
(
  cuenta integer,
  digito text,
  circ integer,
  sec text,
  cod_mz integer,
  mz integer,
  mzl text,
  pc integer,
  pcl text,
  uf text,
  zona text,
  vtierra numeric,
  vedificio numeric,
  vtotal numeric,
  anioval integer,
  mtsfte numeric,
  supm numeric,
  coddist integer,
  codpropietario text,
  codcalle text,
  nombrecalle text,
  nropuerta text,
  appynom text,
  codcallebis text,
  nombrecallebis text,
  nropuertabis text,
  piso text,
  dpto text,
  localidad text,
  categoria integer,
  categoriabco text,
  ctaorigen text,
  digitoorigen text,
  fechasubdivision date,
  dominio text,
  serie text,
  anio text,
  partida text,
  dvpartida text,
  marca_especial text,
  marca_baja text,
  marca_certificada text,
  marca_citacion text,
  nueva_localidad text,
  piso_bis text,
  dpto_bis text,
  localidad_bis text,
  pago_anual character(1),
  importe_ult_cuota numeric
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.padron_tsg
  OWNER TO postgres;
