import java.util.Calendar;

public class Test {

  public static void main(String[] args) {
    // TODO Auto-generated method stub
    Calendar now = Calendar.getInstance();
    // Fecha vencimiento del periodo seleccionado
    Calendar desde = Calendar.getInstance();
    desde.add(Calendar.DATE, -10);
    
    Double monto = 100d;
    // Calculo de meses de diferencia para aplicar interes
    //int diffYear = now.get(Calendar.YEAR) - desde.get(Calendar.YEAR);
    //int diffMonth = diffYear * 12 + now.get(Calendar.MONTH) - desde.get(Calendar.MONTH);
    // Agregamos el interes del 1% mensual
    //monto = monto + (monto * diffMonth / 100);
    
    // Si es 1% mensual -> es 1/30 por dia
    double days = daysBetween(desde.getTimeInMillis(), now.getTimeInMillis());
    System.out.println(days);
    monto = monto + (monto * (days/30) / 100);
    System.out.println(monto);
  }

  private static int daysBetween(long t1, long t2) {
    return (int) ((t2 - t1) / (1000 * 60 * 60 * 24));
} 
}
