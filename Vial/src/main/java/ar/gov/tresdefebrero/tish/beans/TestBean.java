package ar.gov.tresdefebrero.tish.beans;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;

public class TestBean {

	public static void main(String[] args) {

		SeguridadHigieneBean b = new SeguridadHigieneBean();
		b.setCuenta(3434);
		b.setDv("6");
		b.setMonto(BigDecimal.valueOf(700.030));
		
		b.setRazonSocial("juan perez");
		PeriodoBean pb = new PeriodoBean();
		pb.setAnio(2015);
		pb.setFechaBarCode(new Date());
		pb.setPeriodo(1);
		pb.setFechaProximoPago(new Date());
		b.setPeriodoBean(pb);
		
		//b.setMontoMinimo(BigDecimal.valueOf(48750/100.0));
		//System.out.println(b.getMontoMinimo());
		b.setRubro("rubro A");

		
		Nomenclatura n = new Nomenclatura();
		n.setCircunscripcion(1);
		n.setSeccion("AA");
		n.setCodManzana(2);
		n.setManzanaNro(23);
		n.setManzanaLetra("B");
		n.setParcelaNro(12);
		n.setParcelaLetra("E");
		n.setSubParcela("3322");
		b.setNomenclatura(n);
		
		//System.out.println(b.getBarcode());
		
		try {
			JasperReport jasperReport = JasperCompileManager
					.compileReport("/www/tomcat/conf/antenas/antenas.jrxml");
			
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("bean", b);
			
			// Ejecucion de fillReport
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params,
					new JREmptyDataSource());

			// Export			
			File pdf = File.createTempFile("cuenta", ".pdf");
			JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
			System.out.println(pdf.getAbsolutePath());
		} catch (JRException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	

}
