package ar.gov.tresdefebrero.tish.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ar.gov.tresdefebrero.tish.beans.PeriodoBean;
import ar.gov.tresdefebrero.tish.beans.SeguridadHigieneBean;

public class DummySeguridadHigieneDao implements SeguridadHigieneDao {

    public void registrarVEP(SeguridadHigieneBean bean) {
        
    }
    
	public SeguridadHigieneBean findAccount(Integer cuenta, String dv) {
		SeguridadHigieneBean bean = new SeguridadHigieneBean();
		bean.setCuenta(cuenta);
		bean.setDv(dv);
//		bean.setCuenta("12345");
//		bean.setDv("7");
		bean.setRazonSocial("Raul Pedroni SRL");
		bean.setCuit("20-12333444-7");
		bean.setFechaVto(new Date());
		bean.setPeriodo(""+3);
		bean.setMonto(BigDecimal.valueOf(100.5));
	
		bean.setMontoMinimo(BigDecimal.valueOf(100.5));
		return bean;
	}

	@Override
	public PeriodoBean findPeriodo(Integer idImpuesto, Date fecha) {
		PeriodoBean p = new PeriodoBean();
		p.setAnio(2016);
		p.setPeriodo(4);
		p.setFechaBarCode(new Date());
		p.setFechaVto(new Date());
		p.setFechaVigencia(new Date());
		return p;
	}

	@Override
	public List<PeriodoBean> findPeriodoPrevios(PeriodoBean periodo) {
		List<PeriodoBean> previos = new ArrayList<PeriodoBean>();
		PeriodoBean p = new PeriodoBean();
		p.setAnio(2016);
		p.setPeriodo(3);
		previos.add(p);
		return previos;
	}
}
