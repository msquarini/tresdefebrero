<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<html lang="es">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="keywords" content="Tres de febrero, Seguridad, higiene" />
<meta name="description"
	content="Municipalidad de Tres de Febrero - Tasa Seguridad e Higiene" />
<title>Municipalidad de Tres de Febrero</title>

<!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
<!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
<sb:head includeScripts="false" includeScriptsValidation="false" />
<sj:head locale="es" compressed="true" loadAtOnce="true" jqueryui="true"
	loadFromGoogle="false" />
<script type="text/javascript" src="js/jquery.number.js"></script>

<script type="text/javascript">
	$(function() {

		$(function() {
			$("#montoPagar").attr("autocomplete", "off");
		});
		// Set up the number formatting.
		$('#montoPagar').number(true, 2, ',', '');

		/* $('#montoMinimoAjustado').currencyFormat(); */

		$("#montoPagar").focus();

		$('#tax').hide();
		$('#periodopagar').change(function() {
			if (this.selectedIndex > 0) {
				$('#tax').show();
			} else {
				$('#tax').hide();
			}
		});
	});
</script>
<style type="text/css">
#number_container {
	border: 1px dotted #d0d0d0;
	padding: 15px;
	margin: 10px;
	display: none;
	background: #fafafa;
}

html {
	position: relative;
	min-height: 100%;
}

body {
	padding-top: 60px;
	/* 60px to make the container go all the way to the bottom of the topbar */
	margin-bottom: 60px;
}

.top-buffer {
	margin-top: 50px;
}

.pull-middle {
	display: inline-block;
	vertical-align: middle;
	float: none;
}

.navbar-brand, .navbar-nav li a {
	line-height: 80px;
	height: 80px;
	padding-top: 0;
}

.footer {
	position: absolute;
	bottom: 0;
	width: 100%;
	/* Set the fixed height of the footer here */
	height: 40px;
	background-color: #f5f5f5;
}

.footer>.container {
	padding-right: 15px;
	padding-left: 15px;
}
</style>
</head>
<body>
	<nav class="navbar navbar-default navbar-fixed-top">
		<div class="row">
			<a class="navbar-brand" href="http://www.tresdefebrero.gov.ar"> <span><img
					src="img/logo-new.png" alt="" height="75" width="180"></span>
			</a>
		</div>
	</nav>
	<div class="container top-buffer" id="container">

		<!-- 	<div class="container" id="data-container">-->
		<div class="row">

			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<h1>Tasa Vial</h1>
				<p>Datos de la cuenta</p>

				<s:actionerror theme="bootstrap" />
				<s:actionmessage theme="bootstrap" />


				<s:form theme="bootstrap" cssClass="form-vertical">
					<fieldset disabled>
						<div class="row">
							<div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
								<label for="cuenta">Cuenta</label>
								<s:textfield id="cuenta" value="%{#session.cuenta.cuenta}" />
							</div>
							<div class="col-xs-6 col-sm-3 col-md-2 col-lg-2">
								<label for="DV">D�gito Verificador</label>
								<s:textfield id="DV" value="%{#session.cuenta.dv}" />
							</div>

							<div
								class="col-xs-6 col-sm-3 col-md-3 col-lg-3 col-md-offset-1 col-lg-offset-1">
								<label for="fechaVto">Vencimiento</label>
								<s:textfield id="fechaVto" class="form-control"
									value="%{#session.cuenta.fechaVto}" />
							</div>

						</div>
						<div class="row">
							<div class="form-group col-xs-6 col-sm-6 col-md-6 col-lg-6">
								<label for="cuit">Nomenclatura</label>
								<s:textfield id="cuit" class="form-control"
									value="%{#session.cuenta.nomenclaturaLeyenda}" />
							</div>
							<div class="form-group col-xs-6 col-sm-6 col-md-6 col-lg-6">
								<label for="razonsocial"><s:text name="razonsocial"></s:text>
								</label>
								<s:textfield id="razonsocial" class="form-control"
									value="%{#session.cuenta.razonSocial}" />
							</div>
						</div>
						<div class="row">
							<div class="form-group col-xs-6 col-sm-6 col-md-6 col-lg-6">
								<label for="domicilio">Domicilio</label>
								<s:textfield id="domicilio" class="form-control"
									value="%{#session.cuenta.domicilio}" />
							</div>
							<div class="form-group col-xs-6 col-sm-6 col-md-6 col-lg-6">
								<label for="localidad">Localidad</label>
								<s:textfield id="loalidad" class="form-control"
									value="%{#session.cuenta.localidad}" />
							</div>
						</div>
						<div class="row">
							<%-- 							<div class="form-group col-xs-6 col-sm-2 col-md-2 col-lg-2">
								<label for="periodo">Per�odo</label>
								<s:textfield id="periodo" class="form-control"
									value="%{#session.cuenta.periodo}" />
							</div>--%>
							<div class="form-group col-xs-6 col-sm-2 col-md-2 col-lg-2">
								<label for="rubro">Rubro</label>
								<s:textfield id="rubro" class="form-control"
									value="%{#session.cuenta.rubro}" />
							</div>
							<div class="form-group col-xs-6 col-sm-8 col-md-8 col-lg-8">
								<label for="rubroDesc"><s:text name="rubro.descripcion" /></label>
								<s:textfield id="rubroDesc" class="form-control"
									value="%{#session.cuenta.rubroDescripcion}" />
							</div>
						</div>
					</fieldset>
				</s:form>

				<s:form action="vial_generate" id="generateform"
					enctype="multipart/form-data" theme="bootstrap"
					cssClass="form-horizontal" label="Generar Volante de Pago"
					method="POST">

					<fieldset>

						<div class="row">
							<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
								<label for="montoPagar">Monto a pagar</label>
								<s:textfield name="montoPagar" id="montoPagar" maxlength="12"
									value="<s:property name='montoPagar'/>"
									tooltip="Ingrese monto a pagar" />
							</div>

						</div>

						<div class="row">
							<div class="col-xs-7 col-sm-6 col-md-4 col-lg-4 pull-middle">
								<label for="periodopagar">Seleccione el periodo a pagar</label>
								<s:select id="periodopagar" list="#session.periodos"
									theme="simple" elementCssClass="col-sm-4" class="form-control"
									listKey="idPeriodo" listValue="leyenda"
									name="idPeriodoSelected"></s:select>
							</div>
							<div class="col-xs-7 col-sm-6 col-md-6 col-lg-6 pull-middle">
								<p class="bg-warning toggle" id="tax">Se aplicar� inter�s
									seg�n normativa vigente</p>
							</div>
						</div>

						<div class="row top-buffer">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

								<s:submit cssClass="btn btn-primary" value="Generar"></s:submit>
							</div>
						</div>
					</fieldset>
				</s:form>
			</div>
		</div>
	</div>

	<footer class="footer">
		<div class="container">
			<p align="center">
				� Copyright 2015 - 2016 - <strong>MUNICIPALIDAD DE TRES DE
					FEBRERO</strong> - Conmutador: 4734-2400 - Todos los derechos reservados
			</p>
		</div>
	</footer>


</body>
</html>