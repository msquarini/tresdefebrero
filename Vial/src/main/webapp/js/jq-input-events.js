$(document).ready(function(){
	
	/* Para inputs con class="onlyDigits", sólo se permite el ingreso de Dígitos por teclado.*/
	$('.onlyDigits').keypress(function(event){
	  keycode = event.keyCode ? event.keyCode : event.which;
	  // alert('keyCode = ' + event.keyCode + ' - which = ' + event.which + ' - keycode = ' + keycode);
	  if (isDigit(keycode)) 
	     // La tecla presionada es un d�gito.
	     return;
	  if (isFirefoxControlKey(event)){
		// Tecla de Control en Firefox tab, flechas, inicio, fin, backspace, delete, etc.
		return;
	  }else
		  event.preventDefault();
	});

	/* Para inputs con class="number", sólo se permite el ingreso de Dígitos por teclado.*/
	$('.number').keypress(function(event){
		keycode = event.keyCode ? event.keyCode : event.which;
		if (isDigit(keycode) || keycode == 46) 
			// La tecla presionada es un dígito o un punto.
			return;
		if (isFirefoxControlKey(event)){
			// Tecla de Control en Firefox tab, flechas, inicio, fin, backspace, delete, etc.
			return;
		}
		else event.preventDefault();
	});

	/* Para inputs con class="numberDecimalComma", sólo se permite el ingreso de Dígitos y decimales con coma por teclado.*/
	$('.numberDecimalComma').keypress(function(event){
		keycode = event.keyCode ? event.keyCode : event.which;
		if (isDigit(keycode) || keycode == 44) 
			// La tecla presionada es un dígito o un punto.
			return;
		if (isFirefoxControlKey(event)){
			// Tecla de Control en Firefox tab, flechas, inicio, fin, backspace, delete, etc.
			return;
		}
		else event.defaultPrevented;
	});

	/* Para inputs con class="onlyDigitsOrComma", sólo se permite el ingreso de Dígitos y Punto y Coma por teclado para busqueda multiple.*/
	$('.onlyDigitsAndSemiColon').keypress(function(event){
		keycode = event.keyCode ? event.keyCode : event.which;
		  // alert('keyCode = ' + event.keyCode + ' - which = ' + event.which + ' - keycode = ' + keycode);
		  if (isDigitOrSemiColon(keycode)) 
		     // La tecla presionada es un d�gito.
		     return;
		  if (isFirefoxControlKey(event)){
			// Tecla de Control en Firefox tab, flechas, inicio, fin, backspace, delete, etc.
			return;
		  }
		  else event.preventDefault();
		});
	

	/* Funciones JavaScript auxiliares.*/
	// Retorna true si el código de tecla corresponde a un dígito
	function isDigit(keycode){
		return (keycode >= 48 && keycode <= 57);
	}
	
	/* Funciones JavaScript auxiliares.*/
	// Retorna true si el código de tecla corresponde a un dígito o un punto y coma
	function isDigitOrSemiColon(keycode){
		return (keycode == 59 || (keycode >= 48 && keycode <= 57));
	}
	
	// Retorna true si el evento corresponde a una tecla de control en Firefox. 
	// Teclas de flechas de movimiento, Inicio, Fin, Delete, Tab, RePag y AvPag.
	function isFirefoxControlKey(event){
		if (event.which == 8) 
			// tecla Backspace
			return true;
		if (event.which == 0) {
			// Es navegador Firefox.
			  keycode = event.keyCode;
			  // alert('tecla de control en firefox = ' + keycode);
			  if (keycode >= 33 && keycode <= 40 || keycode == 9 || keycode == 46) 
				  return true;
		  }
		  return false;
	  }


});

/*
 * Relaciona el evento 'keydown' sobre INPUTs y SELECTs de un Form para la tecla <enter>, con una función que realiza 
 * un 'click' sobre un button, submit o anchor.
 * Sirve para realizar un SUBMIT con el elemento de id "submitId", al presionar la tecla <enter> sobre 
 * cualquier elemento INPUT y SELECT (de html) del form.
 * submitId : ID del elemento Anchor, Button o Submit.
 */
function submitOnEnterKey(submitId) {
	$('input,select,textarea').bind('keydown', function(event) {
		var keycode = (event.keyCode ? event.keyCode : (event.which ? event.which : event.charCode));
		if (keycode == 13) {
			// Tecla <Enter> presionada
			$('#' + submitId).click();
			return;
		}
	});
}

function onkeyDownCtrlV(obj){
    if (event.ctrlKey == true && event.keyCode==86) {
    	 setTimeout(function () {
 			changeText(obj); }, 100);	        	
    }
}

function changeText(obj){
   var value = obj.value;
   value = value.replace(/(\r\n|\n|\r)/gm," ");
   value = value.replace(/\s+/g,";");
   obj.value = value;
}