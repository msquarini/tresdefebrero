<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<html lang="es">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<meta name="keywords" content="Tres de febrero, automotor, boletas" />
<meta name="description"
	content="Municipalidad de Tres de Febrero - Impuesto automotor" />
<title>Municipalidad de Tres de Febrero</title>

<link rel="stylesheet" href="../css/bootstrap.min.css">

<!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
<!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

<script src="../js/jquery-3.3.1.js"></script>
<script src="../js/jquery.form.js"></script>
<script type="text/javascript" src="../js/jquery.number.js"></script>
<script src="../js/popper.min.js"></script>
<script src="../js/bootstrap.min.js"></script>

<script src="../js/boletas.js"></script>


<script src="../js/bootstrapValidator.js"></script>

<link rel="stylesheet" href="../css/bootstrapValidator.css">
<!-- Link a Font Awesome -->
<link rel="stylesheet" href="../css/fa/css/font-awesome.min.css">

<style type="text/css">
.selector-for-some-widget {
	box-sizing: content-box;
}

#loader {
	position: absolute;
	left: 50%;
	top: 50%;
	z-index: 1;
	width: 150px;
	height: 150px;
	margin: -75px 0 0 -75px;
	border: 16px solid #f3f3f3;
	border-radius: 50%;
	border-top: 16px solid #3498db;
	width: 120px;
	height: 120px;
	-webkit-animation: spin 2s linear infinite;
	animation: spin 2s linear infinite;
}

#number_container {
	border: 1px dotted #d0d0d0;
	padding: 15px;
	margin: 10px;
	display: none;
	background: #fafafa;
}

body {
	padding-top: 70px;
	/* 60px to make the container go all the way to the bottom of the topbar */
}

.navbar-brand, .navbar-nav li a {
	line-height: 60px;
	height: 60px;
	padding-top: 0;
}

.top-buffer {
	margin-top: 20px;
}

.footer {
	position: absolute;
	bottom: 0;
	width: 100%;
	/* Set the fixed height of the footer here */
	height: 40px;
	background-color: #f5f5f5;
}

.footer>.container {
	padding-right: 15px;
	padding-left: 15px;
}
</style>
</head>

<body>
<%-- 	<nav class="navbar navbar-default navbar-fixed-top">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="http://www.tresdefebrero.gov.ar">
					<span><img src="../img/logo-new.png" alt="" height="75"
						width="180"></span>&nbsp;&nbsp;Boletas WEB
				</a>


			</div>
		</div>
	</nav> --%>

	<nav class="navbar fixed-top navbar-light bg-light">
		<a class="navbar-brand" href="#"><img src="../img/logo-new.png"
			alt="" height="75" width="180" class="d-inline-block align-middle">&nbsp;&nbsp;Boletas WEB
		</a>
	</nav>

	<!-- Indicador de loading... -->
	<div id="loader" style="display: none;"></div>

	<div class="container top-buffer" id="container"></div>

	<footer class="footer">
		<div class="container">
			<p align="center">
				� Copyright 2017 - 2018 - <strong>MUNICIPALIDAD DE TRES DE
					FEBRERO</strong> - Conmutador: 4734-2400 - Todos los derechos reservados
			</p>
		</div>
	</footer>

</body>
</html>

<script type="text/javascript">
	$.ajax({
		type : "GET",
		url : '/boletas/consulta_input.action',
		success : function(data) {
			$('#container').html(data);
		}
	});

	
</script>