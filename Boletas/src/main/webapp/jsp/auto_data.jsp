<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>

<script type="text/javascript">
	
</script>
<style type="text/css">
#number_container {
	border: 1px dotted #d0d0d0;
	padding: 15px;
	margin: 10px;
	display: none;
	background: #fafafa;
}

body {
	padding-top: 60px;
	/* 60px to make the container go all the way to the bottom of the topbar */
}

.top-buffer {
	margin-top: 10px;
}
</style>

<div class="row no-gutters bg-light">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<s:form theme="bootstrap" cssClass="form-vertical">
			<fieldset disabled>
				<div class="row">
					<div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
						<label for="dominio">Dominio</label>
						<s:textfield id="dominio" value="%{#session.cuenta.dominio}" />
					</div>
					<div
						class="col-xs-6 col-sm-3 col-md-3 col-lg-3 col-md-offset-1 col-lg-offset-1">
						<label for="titular">Titular</label>
						<s:textfield id="titular" class="form-control"
							value="%{#session.cuenta.nombre}" />
					</div>
					<div class="form-group col-xs-6 col-sm-6 col-md-6 col-lg-6">
						<label for="calle">Dominicilio Postal</label>
						<s:textfield id="calle" class="form-control"
							value="%{#session.cuenta.domicilioPostal}" />
					</div>
				</div>
				<div class="row">
					<div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
						<label for="dominio">Marca</label>
						<s:textfield id="dominio" value="%{#session.cuenta.marca}" />
					</div>
					<div
						class="col-xs-6 col-sm-3 col-md-3 col-lg-3 col-md-offset-1 col-lg-offset-1">
						<label for="titular">Modelo</label>
						<s:textfield id="titular" class="form-control"
							value="%{#session.cuenta.modelo}" />
					</div>
					<div
						class="col-xs-6 col-sm-3 col-md-3 col-lg-3 col-md-offset-1 col-lg-offset-1">
						<label for="titular">A�o</label>
						<s:textfield id="titular" class="form-control"
							value="%{#session.cuenta.anioAuto}" />
					</div>
					<div
						class="col-xs-6 col-sm-3 col-md-3 col-lg-3 col-md-offset-1 col-lg-offset-1">
						<label for="titular">C�digo Tipo</label>
						<s:textfield id="titular" class="form-control"
							value="%{#session.cuenta.codigoTipo}" />
					</div>
				</div>

			</fieldset>
		</s:form>
	</div>
</div>