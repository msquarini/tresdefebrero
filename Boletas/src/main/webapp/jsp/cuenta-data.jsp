<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>

<%
  response.setHeader("Cache-Control", "no-cache");
			response.setHeader("Pragma", "no-cache");
			response.setDateHeader("Expires", -1);
%>

<script type="text/javascript">
	function downloadA() {
		$('#download').submit();
	}
</script>

<style type="text/css">
</style>

<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<s:set var="imp" value="%{#session.cuenta.impuesto}"></s:set>
		<!--    Informacion de Impuesto (se toman datos de session) -->
		<%-- <s:if test="%{#imp == \"XP\"}"> --%>
		<s:include value="auto_data.jsp"></s:include>
		<%-- </s:if> --%>
	</div>
</div>

<div class="row my-2 py-2    no-gutters alert-info">
	<s:form action="consulta_download" id="download" namespace="/"
		enctype="multipart/form-data" theme="bootstrap">
	</s:form>

	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 ">
		<h4>
			Cuota periodo vigente <strong><s:property
					value="#session.cuenta.importe" /></strong>
		</h4>
	</div>
	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
		<button type="button" class="btn btn-info" data-dismiss="modal"
			onclick='downloadA()'>Descargar</button>
	</div>


</div>
