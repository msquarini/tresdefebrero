<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>

<script type="text/javascript">
	function salir() {
		//loader(true, null);
		$.ajax({
			method : "POST",
			url : "/boletas/consulta_input.action",
			traditional : true,
			success : function(res) {
				//loader(false, null);
				$('#container').html(res);
			},
			error : function(res, textStatus, jqXHR) {
				//loader(false, null);
				handleErrorMessages(res.responseJSON);
				showErrorModalDialog();
			}
		});
	}
</script>

<style type="text/css">
/* Fixes dropdown menus placed on the right side */
.ml-auto .dropdown-menu {
	left: auto !important;
	right: 0px;
}
</style>

<div class="row" style="margin-top: 30px;">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

		<ul class="nav nav-pills"
			style="background-color: #eeeeee; border-bottom-style: ridge;">
			<li class="nav-item"><a class="nav-link disabled " href="#">Boletas
					WEB</a></li>
			<li class="nav-item"><a id="info" class="nav-link active"
				href="#datos">Información de la cuenta</a></li>
			<!-- <li class="nav-item"><a class="nav-link" href="#">Periodos previos</a></li> -->
			<li class="ml-auto" style="padding-top: 3px; padding-right: 3px">
				<button id="btn_salir" class="btn btn-info" type="button"
					onclick="salir()">
					<span class="fa fa-window-close"></span>&nbsp;&nbsp;Salir
				</button>
			</li>
		</ul>
 	</div>
</div>
 
<div class="tab-content">
	<div id="datos" class="tab-pane active" role="tabpanel"
		aria-labelledby="info">
		<s:include value="cuenta-data.jsp"></s:include>
	</div>
	<!-- <div id="previos" class="tab-pane fade"></div> -->
</div>


