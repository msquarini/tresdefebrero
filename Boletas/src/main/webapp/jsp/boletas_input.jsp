<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>

<script type="text/javascript">
	$(function() {
		$("#patente").attr("autocomplete", "off");

		$("#impuesto").val('<s:property value="impuesto"/>');

	});

	function validarForm() {
		//Fetch form to apply custom Bootstrap validation
		var form = $("#findAccount")
		//alert(form.prop('id')) //test to ensure calling form correctly

		if (form[0].checkValidity() === false) {
			//  event.preventDefault()
			//  event.stopPropagation()
		} else {
			$('#findAccount').submit();
		}
		form.addClass('was-validated');
	}

	$(document).ready(function() {

		/* Configuracion del formulario, ya que se hace submit del mismo mediante javascript */
		$('#findAccount').ajaxForm({
			target : '#container',
			success : function(res, textStatus, jqXHR) {
			},
			error : function(res, textStatus, jqXHR) {
			}
		});

		$(function() {
			$('[data-toggle="tooltip"]').tooltip()
		})
	})
</script>

<style type="text/css">
#number_container {
	border: 1px dotted #d0d0d0;
	padding: 15px;
	margin: 10px;
	display: none;
	background: #fafafa;
}

body {
	padding-top: 60px;
	/* 60px to make the container go all the way to the bottom of the topbar */
}

.top-buffer {
	margin-top: 20px;
}

.bottom-buffer {
	margin-bottom: 20px;
}
</style>

<div class="row top-buffer">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<h2>Descarga de boletas de pago</h2>
		<h3>Seleccion� el impuesto y luego ingres� el n�mero de cuenta o
			patente seg�n corresponda.</h3>

		<s:actionerror theme="bootstrap" />
		<s:actionmessage theme="bootstrap" />

		<form action="../consulta_search.action" enctype="multipart/form-data"
			class="needs-validation" id="findAccount" novalidate>
			<fieldset>
				<%-- 				<div class="row toggle" id="div-cuit">
					<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
						<label for="cuenta">Cuit</label>
						<s:textfield id="cuit" name="cuit" class="number" maxlength="11"
							tooltip="Ingrese su cuit" />
					</div>
					<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
						<label for="impuesto">En caracter de:</label> <select
							class="form-control" id="caracter" name="caracter">
							<option value="1">Titular</option>
							<option value="2">Usufructuario</option>
							<option value="3">Poseedor a t�tulo de due�o</option>
							<option value="4">Tercero responsable</option>
						</select>
					</div>
				</div> --%>
				<div class="row bottom-buffer">
					<div class="col-xs-9 col-sm-6 col-md-3 col-lg-3">
						<div class="form-group was-validated">
							<label for="impuesto">Impuesto:</label> <select
								class="form-control" id="impuesto" name="impuesto">
								<option value="1">Automotores</option>
								<!-- 							<option value="2">Motos</option>
							<option value="3">ABL</option>
							<option value="4">TISH - Seguridad e Higiene</option> -->
							</select>
						</div>
					</div>
				</div>

				<div class="row toggle" id="div-patente">
					<div class="col-xs-9 col-sm-6 col-md-3 col-lg-3">
						<div class="form-group">
							<!-- <label for="patente">Patente</label> -->
							<input id="patente" name="patente" maxlength="8"
								class="input-group form-control" placeholder="Patente" required />
							<div class="invalid-feedback">Obligatorio</div>
						</div>
					</div>
				</div>
				<div class="row top-buffer">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<%-- <sj:submit cssClass="btn btn-primary" value="Consultar"
							formIds="findAccount" targets="container"
							onclick="validarForm();"></sj:submit> --%>

						<button type="button" class="btn btn-primary" data-dismiss="modal"
							onclick="validarForm();">Continuar</button>
					</div>
				</div>
			</fieldset>
		</form>
	</div>
</div>