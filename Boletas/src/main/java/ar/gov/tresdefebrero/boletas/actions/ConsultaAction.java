package ar.gov.tresdefebrero.boletas.actions;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.SessionAware;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import com.opensymphony.xwork2.ActionSupport;

import ar.gov.tresdefebrero.boletas.beans.AutomotorBean;
import ar.gov.tresdefebrero.boletas.dao.ConsultaDao;
import ar.gov.tresdefebrero.boletas.util.ApplicationContextHelper;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;

/**
 * Consulta de datos de cuentas y cuits
 * 
 * 
 * @author msquarini
 *
 */
public class ConsultaAction extends ActionSupport implements SessionAware {

  private static final long serialVersionUID = 7353477345330099518L;

  private static final Logger logger = LoggerFactory.getLogger("ConsultaAction");

  private String patente;
  private String cuenta;
  private String dv;
  private Integer impuesto;

  private ConsultaDao dao;

  private Map<String, Object> session;
  private InputStream inputStreamA;
  private String fileName;

  public ConsultaAction() {
    ApplicationContext springContext = ApplicationContextHelper.getApplicationContext();
    this.dao = ((ConsultaDao) springContext.getBean("jdbcConsultaDao"));
  }

  public Map<String, Object> getSession() {
    return this.session;
  }

  public void setSession(Map<String, Object> session) {
    this.session = session;
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.opensymphony.xwork2.ActionSupport#input()
   */
  public String input() {
    logger.info("boletas - consulta - input");

    /*
     * Impuesto default Automotores
     */
    impuesto = 1;
    invalidarSession();

    return "input";
  }

  /**
   * Busqueda de impuesto por cuenta o patente
   * 
   * @return
   */
  public String search() {
    clearActionErrors();

    logger.info("boletas - busqueda de cuenta {}|{} - Impuesto: {}", this.cuenta, this.dv,
        this.impuesto);
    try {
      if ((this.dv == null) || (this.dv.equals(""))) {
        this.dv = "-";
      }

      /*
       * Impuesto 1 y 2, autos y motos validamos solo patente
       */
      if (getImpuesto() < 3 && (this.patente == null || "".equals(this.patente))) {
        // addFieldError("patente", getText("patente.invalida"));
        addActionError(getText("patente.invalida"));
        logger.info("moratoria 2018 - patente invalida {}, impuesto {}", this.cuenta,
            this.impuesto);
        return "input";
      } else if (getImpuesto() == 3 && (this.cuenta == null || this.cuenta.equals(""))) {
        // addFieldError("cuenta", getText("cuenta.invalida"));
        addActionError(getText("cuenta.invalida"));
        logger.info("moratoria 2018 - cuenta invalida {}, impuesto {}", this.cuenta, this.impuesto);
        return "input";
      }

      AutomotorBean bean = this.dao.findAutomotor(this.patente);

      if (bean == null) {
        addActionError(getText("patente.invalida"));
        logger.info("boletas - patente invalida {}", this.patente);
        return "input";
      }

      if (hasFieldErrors()) {
        return "input";
      }

      getSession().put("cuenta", bean);

    } catch (Exception e) {
      logger.error("error en busqueda de cuenta", e);
      addActionError("Error en la busqueda de cuenta, intente nuevamente.");
      return "input";
    }
    return "info";
  }

  public String download() throws Exception {

    try {
      AutomotorBean bean = (AutomotorBean) getSession().get("cuenta");
      
      logger.info("descargando volante de pago, cuent: {}", bean);

      if (bean == null) {
        logger.error("Error en descarga de boletas, {}, {}", bean);
        return "input";
      }

      File pdf = File.createTempFile(
          "boleta-automotor-"+ bean.getDominio(), ".pdf");
      
      generarVolantePago(bean, pdf);

      // Nombre del file
      setFileName(pdf.getName());

      // Stream para descarga
      this.inputStreamA = new FileInputStream(pdf);
      // invalidarSession();
    } catch (Exception e) {
      logger.error("", e);
    }
    return "download";
  }
  
  public void generarVolantePago(AutomotorBean bean, File pdf) {
    try {
      logger.info("Generando volande de pago: {}", bean);

      JasperReport jasperReport = JasperCompileManager
          .compileReport("/www/tomcat/conf/boletas/boleta-xp.jrxml");

      Map<String, Object> params = new HashMap<String, Object>();
      params.put("bean", bean);

      JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, new JREmptyDataSource());

      JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));

    } catch (JRException e) {
      logger.error("Generando volande de pago", e);
    } catch (IOException e) {
      logger.error("Generando volande de pago", e);
    }
  }


  public String getCuenta() {
    return this.cuenta;
  }

  public void setCuenta(String cuenta) {
    this.cuenta = cuenta.toUpperCase();
  }

  public String getDv() {
    return this.dv;
  }

  public void setDv(String dv) {
    this.dv = dv;
  }

  public InputStream getInputStreamA() {
    return this.inputStreamA;
  }

  public void setInputStreamA(InputStream inputStream) {
    this.inputStreamA = inputStream;
  }

  public String getFileName() {
    return this.fileName;
  }

  public void setFileName(String fileName) {
    this.fileName = fileName;
  }

  public Integer getImpuesto() {
    return this.impuesto;
  }

  public void setImpuesto(Integer impuesto) {
    this.impuesto = impuesto;
  }

  public String getPatente() {
    return this.patente;
  }

  public void setPatente(String patente) {
    this.patente = patente.toUpperCase();
  }

  /**
   * Remueve datos de session y la invalida
   */
  private void invalidarSession() {
    getSession().remove("cuenta");

    ((SessionMap) getSession()).invalidate();
  }


}
