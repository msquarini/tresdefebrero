package ar.gov.tresdefebrero.boletas.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import ar.gov.tresdefebrero.boletas.beans.AutomotorBean;

/**
 * Mapeo de boleta automotor
 * 
 * @author msquarini
 *
 */
public class AutomotorRowMapper implements RowMapper<AutomotorBean> {

	@Override
	public AutomotorBean mapRow(ResultSet rs, int rowNum) throws SQLException {
		AutomotorBean auto = new AutomotorBean();
		
		auto.setAnio(rs.getString("anio"));
		auto.setAnioAuto(rs.getString("anio_auto"));
		auto.setCategoria(rs.getString("categora"));
		auto.setCodigoBarra(rs.getString("cod_barra_vto").trim());
		auto.setCodigoBarraDeuda(rs.getString("cod_barra_deuda").trim());
		auto.setCodigoBarraSaldo(rs.getString("cod_barra_saldo").trim());
		auto.setCodigoTipo(rs.getString("cod_tipo"));
		auto.setCuit(rs.getString("cuit"));
		auto.setCuota(rs.getString("cuota"));
		auto.setDeudaPeriodo1(rs.getString("periodo_1"));
		auto.setDeudaPeriodo2(rs.getString("periodo_2"));
		auto.setDeudaPeriodo3(rs.getString("periodo_3"));
        auto.setDeudaPeriodo4(rs.getString("periodo_4"));
        auto.setDeudaPeriodo5(rs.getString("periodo_5"));
        auto.setDomicilioPostal(rs.getString("domicilio_postal"));
        auto.setDominio(rs.getString("dominio"));
        auto.setImporte(rs.getString("importe").trim());
        auto.setImporteDeuda(rs.getString("importe_deuda").trim());
        auto.setImporteSaldo(rs.getString("importe_saldo").trim());
        auto.setMarca(rs.getString("marca"));
        auto.setModelo(rs.getString("modelo"));
        auto.setNombre(rs.getString("app_nombre"));
        auto.setNomenclatura(rs.getString("nomenclatura"));
        auto.setPostal(rs.getString("catastro_postal"));
        auto.setProximoVencimiento(rs.getString("vto_prox_cuota"));
        auto.setVencimiento(rs.getString("vencimieto"));
        auto.setVencimientoSaldoAnual(rs.getString("vto_saldo_anual"));
		
		return auto;
	}

}