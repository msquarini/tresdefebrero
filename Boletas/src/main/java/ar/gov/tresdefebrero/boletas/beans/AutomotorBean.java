package ar.gov.tresdefebrero.boletas.beans;

/**
 * 
 * @author admin
 *
 */
public class AutomotorBean {

  private String nombre;
  private String cuit;
  private String postal;
  private String domicilioPostal;
  private String vencimiento;
  private String proximoVencimiento;
  private String vencimientoSaldoAnual;
  private String dominio;
  private String codigoTipo;
  private String categoria;
  private String anio;
  private String cuota;
  private String nomenclatura;
  private String marca;
  private String modelo;
  private String anioAuto;
  private String deudaPeriodo1;
  private String deudaPeriodo2;
  private String deudaPeriodo3;
  private String deudaPeriodo4;
  private String deudaPeriodo5;
  private String importe;
  private String importeDeuda;
  private String importeSaldo;
  private String codigoBarra;
  private String codigoBarraDeuda;
  private String codigoBarraSaldo;

  public String getNombre() {
    return nombre;
  }

  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  public String getCuit() {
    return cuit;
  }

  public void setCuit(String cuit) {
    this.cuit = cuit;
  }

  public String getPostal() {
    return postal;
  }

  public void setPostal(String postal) {
    this.postal = postal;
  }

  public String getDomicilioPostal() {
    return domicilioPostal;
  }

  public void setDomicilioPostal(String domicilioPostal) {
    this.domicilioPostal = domicilioPostal;
  }

  public String getVencimiento() {
    return vencimiento;
  }

  public void setVencimiento(String vencimiento) {
    this.vencimiento = vencimiento;
  }

  public String getProximoVencimiento() {
    return proximoVencimiento;
  }

  public void setProximoVencimiento(String proximoVencimiento) {
    this.proximoVencimiento = proximoVencimiento;
  }

  public String getVencimientoSaldoAnual() {
    return vencimientoSaldoAnual;
  }

  public void setVencimientoSaldoAnual(String vencimientoSaldoAnual) {
    this.vencimientoSaldoAnual = vencimientoSaldoAnual;
  }

  public String getDominio() {
    return dominio;
  }

  public void setDominio(String dominio) {
    this.dominio = dominio;
  }

  public String getCodigoTipo() {
    return codigoTipo;
  }

  public void setCodigoTipo(String codigoTipo) {
    this.codigoTipo = codigoTipo;
  }

  public String getCategoria() {
    return categoria;
  }

  public void setCategoria(String categoria) {
    this.categoria = categoria;
  }

  public String getAnio() {
    return anio;
  }

  public void setAnio(String anio) {
    this.anio = anio;
  }

  public String getCuota() {
    return cuota;
  }

  public void setCuota(String cuota) {
    this.cuota = cuota;
  }

  public String getNomenclatura() {
    return nomenclatura;
  }

  public void setNomenclatura(String nomenclatura) {
    this.nomenclatura = nomenclatura;
  }

  public String getMarca() {
    return marca;
  }

  public void setMarca(String marca) {
    this.marca = marca;
  }

  public String getModelo() {
    return modelo;
  }

  public void setModelo(String modelo) {
    this.modelo = modelo;
  }

  public String getAnioAuto() {
    return anioAuto;
  }

  public void setAnioAuto(String anioAuto) {
    this.anioAuto = anioAuto;
  }

  public String getDeudaPeriodo1() {
    return deudaPeriodo1;
  }

  public void setDeudaPeriodo1(String deudaPeriodo1) {
    this.deudaPeriodo1 = deudaPeriodo1;
  }

  public String getDeudaPeriodo2() {
    return deudaPeriodo2;
  }

  public void setDeudaPeriodo2(String deudaPeriodo2) {
    this.deudaPeriodo2 = deudaPeriodo2;
  }

  public String getDeudaPeriodo3() {
    return deudaPeriodo3;
  }

  public void setDeudaPeriodo3(String deudaPeriodo3) {
    this.deudaPeriodo3 = deudaPeriodo3;
  }

  public String getDeudaPeriodo4() {
    return deudaPeriodo4;
  }

  public void setDeudaPeriodo4(String deudaPeriodo4) {
    this.deudaPeriodo4 = deudaPeriodo4;
  }

  public String getDeudaPeriodo5() {
    return deudaPeriodo5;
  }

  public void setDeudaPeriodo5(String deudaPeriodo5) {
    this.deudaPeriodo5 = deudaPeriodo5;
  }

  public String getImporte() {
    return importe;
  }

  public void setImporte(String importe) {
    this.importe = importe;
  }

  public String getImporteDeuda() {
    return importeDeuda;
  }

  public void setImporteDeuda(String importeDeuda) {
    this.importeDeuda = importeDeuda;
  }

  public String getImporteSaldo() {
    return importeSaldo;
  }

  public void setImporteSaldo(String importeSaldo) {
    this.importeSaldo = importeSaldo;
  }

  public String getCodigoBarra() {
    return codigoBarra;
  }

  public void setCodigoBarra(String codigoBarra) {
    this.codigoBarra = codigoBarra;
  }

  public String getCodigoBarraDeuda() {
    return codigoBarraDeuda;
  }

  public void setCodigoBarraDeuda(String codigoBarraDeuda) {
    this.codigoBarraDeuda = codigoBarraDeuda;
  }

  public String getCodigoBarraSaldo() {
    return codigoBarraSaldo;
  }

  public void setCodigoBarraSaldo(String codigoBarraSaldo) {
    this.codigoBarraSaldo = codigoBarraSaldo;
  }

}
