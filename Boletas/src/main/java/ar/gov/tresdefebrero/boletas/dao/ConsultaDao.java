package ar.gov.tresdefebrero.boletas.dao;

import ar.gov.tresdefebrero.boletas.beans.AutomotorBean;

public abstract interface ConsultaDao {

  public AutomotorBean findAutomotor(String patente);

}
