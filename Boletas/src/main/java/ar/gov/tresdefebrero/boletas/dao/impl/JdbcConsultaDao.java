package ar.gov.tresdefebrero.boletas.dao.impl;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

import ar.gov.tresdefebrero.boletas.beans.AutomotorBean;
import ar.gov.tresdefebrero.boletas.dao.ConsultaDao;
import ar.gov.tresdefebrero.boletas.mapper.AutomotorRowMapper;

public class JdbcConsultaDao implements ConsultaDao {
  private static final Logger logger = LoggerFactory.getLogger("JdbcConsultaDao");
  private JdbcTemplate jdbcTemplate;

  /**
   * Consulta padron de automotores
   */
  private static final String SELECT_AUTO =
      "select app_nombre, cuit, catastro_postal, domicilio_postal, vencimieto, vto_prox_cuota, vto_saldo_anual, "
          + " dominio, cod_tipo, categora, anio, cuota, nomenclatura, marca, modelo, anio_auto, periodo_1,  "
          + " periodo_2, periodo_3, periodo_4, periodo_5, importe, importe_deuda, importe_saldo, cod_barra_vto, cod_barra_deuda, cod_barra_saldo "
          + " from public.boleta_xp_01_2018 where upper(dominio) = ?";

  public void setDataSource(DataSource dataSource) {
    this.jdbcTemplate = new JdbcTemplate(dataSource);
  }

  public AutomotorBean findAutomotor(String patente) {
    AutomotorBean bean;
    try {
      bean = this.jdbcTemplate.queryForObject(SELECT_AUTO, new Object[] {patente.toUpperCase()},
          new AutomotorRowMapper());
    } catch (EmptyResultDataAccessException e) {
      logger.warn("cuenta automotor no encontrada {} ", patente, e);
      return null;
    }
    return bean;
  }


}
