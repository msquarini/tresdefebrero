package ar.com.jass.catastro.web.dto;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.Date;

import ar.com.jass.catastro.batch.deuda.file.DeudaDTO;

/**
 * Codigo de barras para deuda
 * 
 * 34 posiciones yyyyCCCccccccDmmmmmmmmmmmmyyyyMMdd
 * 
 * @author msquarini
 *
 */
public class CodigoBarra {

    public static String getCodigoBarra(DeudaDTO deuda) {
        return getCodigoBarra(deuda.getAnio(), deuda.getCuota(), deuda.getCuenta(), deuda.getDv(), deuda.getImporte(),
                deuda.getFechaVto());
    }

    // public static String getCodigoBarra(CuentaCorriente cc) {
    private static String getCodigoBarra(Integer anio, Integer cuota, String cuenta, String dv, BigDecimal importe,
            Date fechaVto) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        StringBuffer barcode = new StringBuffer();

        // anio YYYY
        barcode.append(anio);
        // cuota (3)
        barcode.append(String.format("%03d", cuota));
        // cuenta (6)
        barcode.append(String.format("%06d", Integer.valueOf(cuenta)));
        // dv (1), si es "-" entonces se traduce a 0
        if (dv.equals("-")) {
            barcode.append(String.format("%01d", 0));
        } else {
            barcode.append(String.format("%01d", Integer.valueOf(dv)));
        }

        // parte entera monto (10)
        barcode.append(String.format("%010d", importe.intValue()));
        BigDecimal centavos = importe.subtract(importe.setScale(0, RoundingMode.FLOOR)).movePointRight(importe.scale());
        // parte decimal monto (2)
        barcode.append(String.format("%02d", centavos.intValue()));

        // Fecha de vencimiento
        barcode.append(sdf.format(fechaVto));

        return barcode.toString();
    }
}
