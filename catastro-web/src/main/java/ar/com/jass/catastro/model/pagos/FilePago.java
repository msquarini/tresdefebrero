package ar.com.jass.catastro.model.pagos;

import java.io.File;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import ar.com.jass.catastro.business.impl.FileType;
import ar.com.jass.catastro.model.EntityBase;

/**
 * File de pagos
 * 
 * @author msquarini
 *
 */
@Entity
@Table(name = "File_Pago")
public class FilePago extends EntityBase implements Serializable {

    /**
     * Id unico de file de pagos
     */
    private Long id;

    /**
     * Nombre del archivo
     */
    private String nombre;

    /**
     * Origen del archivo (tipo)
     */
    private FileType origen;

    private EstadoFilePagos estado;

    /**
     * Cantidad de registros procesados
     */
    private Integer procesados;

    /**
     * Cantidad de registros no encontrados en cuenta corriente
     */
    private Integer sinDeuda;

    /**
     * Cantidad de registros con errores
     */
    private Integer errores;

    /**
     * Hash del file completo
     */
    private String hash;

    /**
     * Header del file
     */
    private String fileHeader;

    /**
     * Trailer del file
     */
    private String fileTrailer;

    /**
     * Cantidad de registros detalle de pagos. En trailer
     */
    private Integer cantidadRegistros;

    /**
     * Fecha del encabezado
     */
    private Date fechaFile;

    /**
     * Importe total de los pagos
     */
    private BigDecimal total;

    /**
     * Lista de pagos (detalles) del archivo procesado
     */
    private List<Pago> pagos;

    /**
     * 
     */
    public FilePago() {
        this.cantidadRegistros = 0;
        this.errores = 0;
        this.estado = EstadoFilePagos.PENDIENTE;
        this.procesados = 0;
        this.sinDeuda = 0;
        this.total = BigDecimal.ZERO;
        this.pagos = new ArrayList<Pago>();
    }

    public FilePago(String fileName) {
        this();
        this.nombre = fileName;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_filepago_gen")
    @SequenceGenerator(name = "seq_filepago_gen", sequenceName = "seq_filepago", allocationSize = 1)
    @Column(name = "id_filepago")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Column(name = "procesados")
    public Integer getProcesados() {
        return procesados;
    }

    public void setProcesados(Integer procesados) {
        this.procesados = procesados;
    }

    @Column(name = "errores")
    public Integer getErrores() {
        return errores;
    }

    public void setErrores(Integer errores) {
        this.errores = errores;
    }

    @Column(name = "hash")
    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    @OneToMany(mappedBy = "filePago", fetch = FetchType.LAZY)
    //@Cascade(value = CascadeType.)
    public List<Pago> getPagos() {
        return pagos;
    }

    public void setPagos(List<Pago> pagos) {
        this.pagos = pagos;
    }

    public void addPago(Pago p) {
        getPagos().add(p);
    }

    @Column(name = "header")
    public String getFileHeader() {
        return fileHeader;
    }

    public void setFileHeader(String fileHeader) {
        this.fileHeader = fileHeader;
    }

    @Column(name = "trailer")
    public String getFileTrailer() {
        return fileTrailer;
    }

    public void setFileTrailer(String fileTrailer) {
        this.fileTrailer = fileTrailer;
    }

    @Column(name = "registros")
    public Integer getCantidadRegistros() {
        return cantidadRegistros;
    }

    public void setCantidadRegistros(Integer cantidadRegistros) {
        this.cantidadRegistros = cantidadRegistros;
    }

    @Column(name = "total")
    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    @Column(name = "sin_deuda")
    public Integer getSinDeuda() {
        return sinDeuda;
    }

    public void setSinDeuda(Integer sinDeuda) {
        this.sinDeuda = sinDeuda;
    }

    @Column(name = "estado")
    public EstadoFilePagos getEstado() {
        return estado;
    }

    public void setEstado(EstadoFilePagos estado) {
        this.estado = estado;
    }

    @Column(name = "fecha_header")
    public Date getFechaFile() {
        return fechaFile;
    }

    public void setFechaFile(Date fechaFile) {
        this.fechaFile = fechaFile;
    }

    @Override
    public String toString() {
        return "FilePago [id=" + id + ", nombre=" + nombre + ", estado=" + estado + ", procesados=" + procesados
                + ", sinDeuda=" + sinDeuda + ", errores=" + errores + ", hash=" + hash + ", fileHeader=" + fileHeader
                + ", fileTrailer=" + fileTrailer + ", cantidadRegistros=" + cantidadRegistros + ", fechaFile="
                + fechaFile + ", total=" + total + "]";
    }

    @Column(name = "origen")
    @Enumerated(javax.persistence.EnumType.STRING)
    public FileType getOrigen() {
        return origen;
    }

    public void setOrigen(FileType origen) {
        this.origen = origen;
    }

    @Transient
    public String getUniqueName(){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd-hhmmss");
        return getNombre() + "-" + sdf.format(getCreatedDate());        
    }
}
