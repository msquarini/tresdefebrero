package ar.com.jass.catastro.batch.deuda.file.callback;

import java.io.IOException;
import java.io.Writer;
import java.util.Date;
import java.util.List;

import org.springframework.batch.item.file.FlatFileHeaderCallback;

import ar.com.jass.catastro.batch.deuda.file.DeudaDTO;

/**
 * Header para archivos a Pagomiscuentas
 * 
 * @author mesquarini
 *
 */
public class PMCHeaderCallback implements FlatFileHeaderCallback, DeudaFileCallback {

    public static final String HEADER = "04002337%1$tY%1$tm%1$td";
    public static final int FILLER = 264;
    
	@Override
	public void writeHeader(Writer writer) throws IOException {
	    String temp = String.format(HEADER, new Date()) + String.format("%0" + FILLER + "d", 0);		
		writer.write(temp);
	}

	public static void main(String[] args) {
	    PMCHeaderCallback c = new PMCHeaderCallback();
	    try {
            c.writeHeader(null);
        } catch (IOException e) {
            e.printStackTrace();
        }
	}

    @Override
    public void notify(List<DeudaDTO> deudas) {
        //noop
    }
}
