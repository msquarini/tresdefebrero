package ar.com.jass.catastro.report.impl;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperReport;

/**
 * Reportes compilados
 * 
 * @author msquarini
 *
 */
public class ReportsHelper {


    public enum ReportFormat {
        PDF {
            @Override
            public String getExtension() {                
                return ".pdf";
            }
        }, TXT {
            @Override
            public String getExtension() {
                return ".txt";
            }
        }, EXCEL {
            @Override
            public String getExtension() {
                return ".xls";
            }
        };        
        public abstract String getExtension();
    }

    private static final Logger LOG = LoggerFactory.getLogger("ReportsHelper");

    private static final String REPORT_EXTENSION = ".jrxml";
    /**
     * Path de reportes
     */
    private String reportPath;

    /**
     * Nombre de files de reportes
     */
    private String[] reportFiles;

    /**
     * Reportes compilados
     */
    private Map<String, JasperReport> reports;

    public void init() throws JRException {
        reports = new HashMap<String, JasperReport>();

        for (String reportFile : reportFiles) {
            String reportFilePath = reportPath.concat(reportFile).concat(REPORT_EXTENSION);
            LOG.info("Compilando reporte {}", reportFilePath);
            JasperReport jasperReport = JasperCompileManager.compileReport(reportFilePath);
            reports.put(reportFile, jasperReport);
        }
    }

    public JasperReport getReport(String reportName) {
        return reports.get(reportName);
    }

    public String getReportPath() {
        return reportPath;
    }

    public void setReportPath(String reportPath) {
        this.reportPath = reportPath;
    }

    public String[] getReportFiles() {
        return reportFiles;
    }

    public void setReportFiles(String[] reportFiles) {
        this.reportFiles = reportFiles;
    }

    public Map<String, JasperReport> getReports() {
        return reports;
    }

    public void setReports(Map<String, JasperReport> reports) {
        this.reports = reports;
    }
}
