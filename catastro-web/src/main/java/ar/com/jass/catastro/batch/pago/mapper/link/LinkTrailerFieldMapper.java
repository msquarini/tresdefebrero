package ar.com.jass.catastro.batch.pago.mapper.link;

import java.math.BigDecimal;

import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;

import ar.com.jass.catastro.model.pagos.link.LinkTrailer;

public class LinkTrailerFieldMapper implements FieldSetMapper<LinkTrailer> {

	public LinkTrailer mapFieldSet(FieldSet fs) {

		if (fs == null) {
			return null;
		}

		LinkTrailer trailer = new LinkTrailer();
		trailer.setLine(fs.readString("linea"));
		trailer.setCantidadRegistros(fs.readInt("registros"));
		
        
		String cadenaImporte = fs.readString("total");
        String entero = cadenaImporte.substring(0, 12);
        String decimal = cadenaImporte.substring(12);
        
        trailer.setTotal(new BigDecimal(entero+"."+decimal));
        
		return trailer;
	}

}
