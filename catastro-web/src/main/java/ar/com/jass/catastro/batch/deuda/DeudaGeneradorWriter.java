package ar.com.jass.catastro.batch.deuda;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.AfterStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

import ar.com.jass.catastro.dao.GeneralDAO;
import ar.com.jass.catastro.model.Cuenta;
import ar.com.jass.catastro.model.cuentacorriente.DeudaPeriodoStatus;
import ar.com.jass.catastro.model.cuentacorriente.GeneracionDeudaDTO;
import ar.com.jass.catastro.model.cuentacorriente.Periodo;

/**
 * Persiste registro de pagos y movimientos de Cuenta corriente
 * 
 * @author msquarini
 *
 */
public class DeudaGeneradorWriter implements ItemWriter<GeneracionDeudaDTO> {

    private static final Logger LOG = LoggerFactory.getLogger(DeudaGeneradorWriter.class);

    @Autowired
    private GeneralDAO generalDAO;

    @AfterStep
    public void retrieveInterstepData(StepExecution stepExecution) {
        JobExecution jobExecution = stepExecution.getJobExecution();
        ExecutionContext jobContext = jobExecution.getExecutionContext();

        // Id del periodo que ejecuta el JOB
        Long idPeriodo = jobExecution.getJobParameters().getLong("idPeriodo");
        
        Periodo periodo = (Periodo) jobContext.get("periodo_"+idPeriodo);
        if (stepExecution.getExitStatus().compareTo(ExitStatus.FAILED) > -1) {
            periodo.setEstado(DeudaPeriodoStatus.ERROR);
        } else {
            periodo.setEstado(DeudaPeriodoStatus.GENERADO);
        }
        LOG.info("BS-WRITER {} {}", idPeriodo, periodo);
        LOG.info("Cambiando estado del periodo {}", periodo);
    }

    @Override
    public void write(List<? extends GeneracionDeudaDTO> items) throws Exception {
        try {
            for (GeneracionDeudaDTO dto : items) {
                generalDAO.saveList(dto.getMovimientos());
            }
            
        } catch (Exception e) {
            LOG.error("error en persistencia de deuda", e);
        }
    }

    public GeneralDAO getGeneralDAO() {
        return generalDAO;
    }

    public void setGeneralDAO(GeneralDAO generalDAO) {
        this.generalDAO = generalDAO;
    }
}
