package ar.com.jass.catastro.model.cuentacorriente;

/**
 * Estados de generacion de deuda en un periodo
 * 
 * @author msquarini
 *
 */
public enum DeudaPeriodoStatus {

	PENDIENTE, A_PROCESAR, PROCESANDO, GENERADO, ERROR;

}
