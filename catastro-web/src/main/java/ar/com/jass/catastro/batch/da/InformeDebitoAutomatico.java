package ar.com.jass.catastro.batch.da;

import ar.com.jass.catastro.model.cuentacorriente.CuentaCorriente;
import ar.com.jass.catastro.model.da.DebitoAutomatico;

public class InformeDebitoAutomatico {

	private DebitoAutomatico da;
	private CuentaCorriente deuda;

	public DebitoAutomatico getDa() {
		return da;
	}

	public void setDa(DebitoAutomatico da) {
		this.da = da;
	}

	public CuentaCorriente getDeuda() {
		return deuda;
	}

	public void setDeuda(CuentaCorriente deuda) {
		this.deuda = deuda;
	}

}
