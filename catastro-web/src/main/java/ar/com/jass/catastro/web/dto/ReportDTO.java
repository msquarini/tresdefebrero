package ar.com.jass.catastro.web.dto;

import java.io.File;
import java.util.Date;

/**
 * DTO de informacion de reporte
 * 
 * @author msquarini
 *
 */
public class ReportDTO {

    String formato;
    String reportname;
    String fileName;
    File file;
    Date createdDate;
    String user;

    public ReportDTO(String formato, String reportname, String fileName, File file) {
        this.formato = formato;
        this.reportname = reportname;
        this.file = file;
        createdDate = new Date();
        user = "usuario en session";
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public String getFormato() {
        return formato;
    }

    public void setFormato(String formato) {
        this.formato = formato;
    }

    public String getReportname() {
        return reportname;
    }

    public void setReportname(String reportname) {
        this.reportname = reportname;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }
}
