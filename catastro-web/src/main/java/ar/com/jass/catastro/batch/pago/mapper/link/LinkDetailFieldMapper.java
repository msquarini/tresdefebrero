package ar.com.jass.catastro.batch.pago.mapper.link;

import java.math.BigDecimal;

import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;

import ar.com.jass.catastro.model.pagos.PagoDetailRecord;

public class LinkDetailFieldMapper implements FieldSetMapper<PagoDetailRecord> {

    public PagoDetailRecord mapFieldSet(FieldSet fs) {

        if (fs == null) {
            return null;
        }

        PagoDetailRecord detail = new PagoDetailRecord();
        detail.setAnio(fs.readInt("anio"));
        detail.setCuota(fs.readInt("cuota"));
        detail.setCuenta(fs.readString("cuenta"));
        detail.setDv(fs.readString("dv"));
        detail.setFechaVto(fs.readDate("fechaVto", "yyyyMMdd"));
        detail.setFechaPago(fs.readDate("fechaPago", "yyyyMMdd"));

        String cadenaImporte = fs.readString("importe");
        String entero = cadenaImporte.substring(0, 10);
        String decimal = cadenaImporte.substring(10);

        detail.setImporte(new BigDecimal(entero + "." + decimal));

        detail.setLine(fs.readString("linea"));

        return detail;
    }

}
