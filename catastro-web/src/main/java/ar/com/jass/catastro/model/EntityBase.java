package ar.com.jass.catastro.model;

import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenerationTime;
import org.hibernate.annotations.GeneratorType;
import org.hibernate.annotations.UpdateTimestamp;

import ar.com.jass.catastro.util.LoggedUserGenerator;

/**
 * Entidad base para el mapeo de datos de auditoria
 * 
 * @author msquarini
 *
 */
@MappedSuperclass
public abstract class EntityBase {

    private UUID uuid = UUID.randomUUID();
    
    private Date createdDate;
    private Date modifiedDate;
    private String createdBy;
    private String modifiedBy;

    @Temporal(TemporalType.TIMESTAMP)
    @CreationTimestamp
    @Column(name = "created_date", updatable = false)
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @UpdateTimestamp
    @Column(name = "modified_date")
    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    @Column(name = "created_by")
    @GeneratorType(type = LoggedUserGenerator.class, when = GenerationTime.INSERT)
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Column(name = "modified_by")
    @GeneratorType(type = LoggedUserGenerator.class, when = GenerationTime.ALWAYS)
    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @Transient
    public UUID getUuid() {
        return uuid;
    }

    @Transient
    public String getUuidString() {
        return uuid.toString();
    }
}