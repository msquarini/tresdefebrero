package ar.com.jass.catastro.model.pagos.link;

import java.math.BigDecimal;

import ar.com.jass.catastro.model.pagos.FilePago;
import ar.com.jass.catastro.model.pagos.FilePagosRecord;
import ar.com.jass.catastro.model.pagos.PagoTrailerRecord;

/**
 * Trailer de file de pagos de Link
 * @author msquarini
 *
 */
public class LinkTrailer extends PagoTrailerRecord implements FilePagosRecord {

	Integer cantidadRegistros;
	BigDecimal total;

	@Override
	public void complete(FilePago filePago) {
		filePago.setFileTrailer(getLine());
		filePago.setCantidadRegistros(this.cantidadRegistros);
		filePago.setTotal(this.total);
	}

	public Integer getCantidadRegistros() {
		return cantidadRegistros;
	}

	public void setCantidadRegistros(Integer cantidadRegistros) {
		this.cantidadRegistros = cantidadRegistros;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

}