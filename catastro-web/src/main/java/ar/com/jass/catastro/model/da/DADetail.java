package ar.com.jass.catastro.model.da;

import java.util.Date;

/**
 * Detail de file de pagos de Link
 * 
 * @author msquarini
 *
 */
public class DADetail {

	String cuenta;
	String dv;
	DANovedad novedad;
	Date fecha;
	String banco;
	String cuentaBanco;
	String cbu;
	String line;

	public DADetail(String banco) {
		this.banco = banco;
	}

	public String getCuenta() {
		return cuenta;
	}

	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}

	public String getDv() {
		return dv;
	}

	public void setDv(String dv) {
		this.dv = dv;
	}

	public DANovedad getNovedad() {
		return novedad;
	}

	public void setNovedad(DANovedad novedad) {
		this.novedad = novedad;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getBanco() {
		return banco;
	}

	public void setBanco(String banco) {
		this.banco = banco;
	}

	public String getLine() {
		return line;
	}

	public void setLine(String line) {
		this.line = line;
	}

	public String toString() {
		return "DADetail cuenta: [" + getCuenta() + "] Dv: [" + getDv() + "] Banco: [" + getBanco() + "] Novedad: ["
				+ getNovedad() + "]";
	}

	public String getCuentaBanco() {
		return cuentaBanco;
	}

	public void setCuentaBanco(String cuentaBanco) {
		this.cuentaBanco = cuentaBanco;
	}

	public String getCbu() {
		return cbu;
	}

	public void setCbu(String cbu) {
		this.cbu = cbu;
	}
}