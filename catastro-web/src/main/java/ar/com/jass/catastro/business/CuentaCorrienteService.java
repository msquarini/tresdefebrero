package ar.com.jass.catastro.business;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import ar.com.jass.catastro.business.impl.FileType;
import ar.com.jass.catastro.exceptions.BusinessException;
import ar.com.jass.catastro.model.Cuenta;
import ar.com.jass.catastro.model.Linea;
import ar.com.jass.catastro.model.Parcela;
import ar.com.jass.catastro.model.Tramite;
import ar.com.jass.catastro.model.cuentacorriente.Boleta;
import ar.com.jass.catastro.model.cuentacorriente.CuentaCorriente;
import ar.com.jass.catastro.model.cuentacorriente.DeudaPeriodoStatus;
import ar.com.jass.catastro.model.cuentacorriente.FileDeuda;
import ar.com.jass.catastro.model.cuentacorriente.Periodo;
import ar.com.jass.catastro.model.cuentacorriente.TipoMovimiento;
import ar.com.jass.catastro.model.pagos.Pago;
import ar.com.jass.catastro.web.dto.ResumenGeneracionDeuda;

/**
 * Calculo de cuotas de cuentas
 * 
 * @author msquarini
 *
 */
public interface CuentaCorrienteService {

    public void save(Object o);

    public void cambioEstadoProcesoGeneracionDeuda(Long idPeriodo, DeudaPeriodoStatus estado);

    public ResumenGeneracionDeuda resumenGeneracionCC(Periodo periodo);

    public Boleta findBoleta(Pago pago);

    public List<Boleta> findBoletas(Long idCuenta, Periodo periodo);

    public List<Boleta> findBoletas(Long idCuenta, Integer anio, Integer cuota, Boolean sinPago);

    /**
     * Periodos en el anio indicado
     * 
     * @param anio
     * @param initialize
     * @return
     */
    public List<Periodo> findPeriodos(Integer anio, String... initialize);

    /**
     * Devuelve el periodo que corresponde a la fecha indicada
     * 
     * @param fecha
     * @return
     */
    public Periodo findPeriodo(Date fecha);

    /**
     * Devuelve el periodo del anio y cuota
     * 
     * @param anio
     * @param cuota
     * @return
     */
    public Periodo findPeriodo(Integer anio, Integer cuota);
    
    /**
     * Periodos a partir de la fecha
     * 
     * @param desde
     * @param initialize
     * @return
     */
    public List<Periodo> findPeriodos(Calendar desde, String... initialize);
    
    /**
     * Periodos comprendidos entre los periodos desde (inclusive) y hasta (no incluido)
     * 
     * @param desde
     * @param hasta no incluido
     * @return
     */
    public List<Periodo> findPeriodos(Periodo desde, Periodo hasta);

    /**
     * Devuelve el movimiento correspondiente a los parametros
     * 
     * @param idCuenta
     * @param anio
     * @param cuota
     * @param ajuste
     * @return
     */
    public CuentaCorriente findCuotaByCuentaId(Long idCuenta, Integer anio, Integer cuota, Integer ajuste,
            TipoMovimiento tipo);

    /**
     * Busqueda de movimientos de cuenta corriente para la cuenta y los filtros de anio y tipo Si
     * anio es null o vacio, entonces no se utiliza como filtro Tipo C (credito) D (debito) NULL
     * (todos)
     * 
     * @param cuenta
     * @param anio
     * @param tipo
     * @return
     */
    public List<CuentaCorriente> findMovimientosCC(Cuenta cuenta, Integer anio, TipoMovimiento tipo);

    /**
     * Busqueda de movimientos de cuenta para el anio y cuota indicados
     * 
     * @param cuenta
     * @param anio
     * @param cuota
     * @return
     */
    public List<CuentaCorriente> findMovimientosCC(Cuenta cuenta, Integer anio, Integer cuota);

    /**
     * Buscar el detalle del movimiento de cuentacorriente por ID Si existe pago se incluye
     * 
     * @param id
     * @return
     */
    public CuentaCorriente findMovimientoCC(Long id);

    /**
     * Generar movimientos de deuda para la cuenta desde la fecha indicada Aplicada en procesamiento
     * de tramites que requiere recalcular deuda para cuentas involucradas
     * 
     * @param desde
     * @param cuenta
     * @return
     */
    public List<CuentaCorriente> generarDeuda(Date desde, Cuenta cuenta, boolean aplicaInteres)
            throws BusinessException;

    /**
     * Crear el registro en tabla para mapear el archivo de deuda generado y poder descargarlo
     * posteriormente
     * 
     * @param periodo
     * @param tipo
     */
    public void createFileDeuda(Periodo periodo, FileType tipo);

    /**
     * Busqueda por ID
     * 
     * @param idFileDeuda
     * @return
     */
    public FileDeuda findFileDeuda(Long idFileDeuda);

    /**
     * Generacion de movimientos de CC de ajustes de los periodos ya generado desde la fecha de
     * vigencia. Calculado con la nueva linea a partir de la diferencia con el anterior
     * 
     * @param nuevaLinea
     * @param cuenta
     * @return
     */
    // public List<CuentaCorriente> ajusteCambioLinea(Linea nuevaLinea, Cuenta cuenta);
    public List<CuentaCorriente> ajusteCuentaCorriente(Linea nuevaLinea, Cuenta cuenta, Integer codigoAdministrativo, Tramite tramite);

    public Integer getNroAjusteSiguiente(CuentaCorriente movimiento);
    public Integer getNroAjusteSiguiente(Cuenta cuenta, Periodo periodo);
    
    /**
     * Procesamiento de emision de deuda para todos los periodos en estado A PROCESAR
     */
    public void emisionDeudaAnual(List<Periodo> periodos);

    public List<CuentaCorriente> generarCalculoRetroactivo(Tramite tramite, List<Parcela> ufs);
    
    /**
     * Procesamiento de cuenta corrienta en cuentas generadas por un tramite
     * Se calculan las cuotas a partir de la fecha de inicio del tramite 
     * y se ajustas los movimientos desde la fecha de vigencia del tramite 
     * @param tramite
     * @param ufs
     * @return
     */
    public List<CuentaCorriente> procesamientoCuentaCorriente(Tramite tramite, List<Parcela> ufs) throws BusinessException;
}
