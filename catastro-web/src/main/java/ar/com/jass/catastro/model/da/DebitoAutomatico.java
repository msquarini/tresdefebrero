package ar.com.jass.catastro.model.da;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import ar.com.jass.catastro.model.Cuenta;

/**
 * Debito automatico informado por entidades
 * 
 * @author MESquarini
 * 
 */
@Entity
@Table(name = "DA")
public class DebitoAutomatico {

	/**
	 * Id unico
	 */
	private Long id;

	private String banco;
	private Cuenta cuenta;

	private Date fecha;

	private DANovedad novedad;

	private String cbu;

	private String cuentaBanco;

	/**
	 * Detalle informado por la entidad
	 */
	private String fileDetail;

	private Date fechaProceso;

	public DebitoAutomatico() {
		fechaProceso = new Date();
	}

	/**
	 * Creacion de entidad de DA a partir de registro de detalle de novedad
	 * 
	 * @param dad
	 */
	public DebitoAutomatico(DADetail dad) {
		this.banco = dad.getBanco();
		this.fecha = dad.getFecha();
		this.fileDetail = dad.getLine();
		this.novedad = dad.getNovedad();
		this.cuentaBanco = dad.getCuentaBanco();
		this.cbu = dad.getCbu();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_da_gen")
	@SequenceGenerator(name = "seq_da_gen", sequenceName = "seq_da", allocationSize = 1)
	@Column(name = "id_da")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "banco")
	public String getBanco() {
		return banco;
	}

	public void setBanco(String banco) {
		this.banco = banco;
	}

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_cuenta")
	public Cuenta getCuenta() {
		return cuenta;
	}

	public void setCuenta(Cuenta cuenta) {
		this.cuenta = cuenta;
	}

	@Column(name = "fecha")
	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	@Column(name = "novedad")
	@Enumerated(javax.persistence.EnumType.ORDINAL)
	public DANovedad getNovedad() {
		return novedad;
	}

	public void setNovedad(DANovedad novedad) {
		this.novedad = novedad;
	}

	@Column(name = "file_line")
	public String getFileDetail() {
		return fileDetail;
	}

	public void setFileDetail(String fileDetail) {
		this.fileDetail = fileDetail;
	}

	@Column(name = "cbu")
	public String getCbu() {
		return cbu;
	}

	public void setCbu(String cbu) {
		this.cbu = cbu;
	}

	@Column(name = "cuenta_bco")
	public String getCuentaBanco() {
		return cuentaBanco;
	}

	public void setCuentaBanco(String cuentaBanco) {
		this.cuentaBanco = cuentaBanco;
	}

	@Column(name = "fecha_proceso")
	public Date getFechaProceso() {
		return fechaProceso;
	}

	public void setFechaProceso(Date fechaProceso) {
		this.fechaProceso = fechaProceso;
	}

	public String toString() {
		return "DA id[" + getId() + "] Cuenta: [" + getCuenta().getCuenta() + "] Dv: [" + getCuenta().getDv()
				+ "] Novedad: [" + getNovedad() + "]";
	}
}