package ar.com.jass.catastro.model;

/**
 * Tipo de dominio
 * Folio o Matricula
 * @author MESquarini
 *
 */
public enum TipoDominio {

	F {
	    public String getTextKey() {
	        return "folio.label";
	    }
	}, M {
	    public String getTextKey() {
            return "matricula.label";
        }
	};
    
    public abstract String getTextKey();
}
