package ar.com.jass.catastro.batch.deuda.boleta;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;

import ar.com.jass.catastro.batch.HelperTasklet;
import ar.com.jass.catastro.dao.GeneralDAO;
import ar.com.jass.catastro.model.cuentacorriente.BoletaPeriodoStatus;
import ar.com.jass.catastro.model.cuentacorriente.Periodo;

/**
 * Inicializar la ejecucion del JOB con info del periodo
 * 
 * 
 * @author msquarini
 *
 */
public class InitPeriodoTasklet extends HelperTasklet implements Tasklet {

    private static final Logger LOG = LoggerFactory.getLogger(InitPeriodoTasklet.class);

    @Autowired
    private GeneralDAO generalDAO;

    @Override
    public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) throws Exception {

        try {
            Periodo periodo = generalDAO.find(Periodo.class, getJobLongParameter(chunkContext, "idPeriodo"));
            LOG.info("Procesando - {}", periodo);

            periodo.setBoletaStatus(BoletaPeriodoStatus.PROCESANDO);
            generalDAO.save(periodo);

            putExecutionParameter(chunkContext, "periodo_" + periodo.getId(), periodo);

        } catch (Exception e) {
            throw new IllegalStateException("No existe Periodo en estado para procesar");

        }
        return RepeatStatus.FINISHED;
    }
}