package ar.com.jass.catastro.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import ar.com.jass.catastro.dao.GeneralDAORatificacion;
import ar.com.jass.catastro.model.CuentaRatificacion;
import ar.com.jass.catastro.model.Nomenclatura;
import ar.com.jass.catastro.model.Parcela;
import ar.com.jass.catastro.model.ParcelaRatificacion;
import ar.com.jass.catastro.model.TitularRatificacion;
import ar.com.jass.catastro.model.Tramite;

@SuppressWarnings("unchecked")
@Repository("generalDAORatificacion")
public class GeneralDAORatificacionImpl implements GeneralDAORatificacion {

	private static final Logger LOG = LoggerFactory.getLogger("GeneralDAORatificacionImpl");

	@Autowired
	private SessionFactory sessionFactory;

	protected Session currentSession() {
		return sessionFactory.getCurrentSession();
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public List<Parcela> findParcelas(Nomenclatura nomenclatura) {
		Criteria c = createCriteriaNomenclatura(nomenclatura);
		return (List<Parcela>) c.list();
	}

	/*
	 * @Override public void initialize(Object o) { Hibernate.initialize(o); }
	 */

	/**
	 * Arma criterio de busqueda x nomenclatura para obtener todas las unidades funcionales de la tabla maestro
	 * 
	 * @param nomenclatura
	 * @return
	 */
	private Criteria createCriteriaNomenclatura(Nomenclatura nomenclatura) {
		Criteria c = getSessionFactory().getCurrentSession().createCriteria(Parcela.class);

		if (nomenclatura.getCircunscripcion() != null && nomenclatura.getCircunscripcion() > 0) {
			c.add(Restrictions.eq("nomenclatura.circunscripcion", nomenclatura.getCircunscripcion()));
		}
		if (nomenclatura.getSeccion() != null && nomenclatura.getSeccion().length() > 0) {
			c.add(Restrictions.eq("nomenclatura.seccion", nomenclatura.getSeccion()));
		}
		if (nomenclatura.getFraccion() != null && nomenclatura.getFraccion() > 0) {
			c.add(Restrictions.eq("nomenclatura.fraccion", nomenclatura.getFraccion()));
		}
		if (nomenclatura.getFraccionLetra() != null && nomenclatura.getFraccionLetra().length() > 0) {
			c.add(Restrictions.eq("nomenclatura.fraccionLetra", nomenclatura.getFraccionLetra()));
		}
		if (nomenclatura.getManzana() != null && nomenclatura.getManzana() > 0) {
			c.add(Restrictions.eq("nomenclatura.manzana", nomenclatura.getManzana()));
		}
		if (nomenclatura.getManzanaLetra() != null && nomenclatura.getManzanaLetra().length() > 0) {
			c.add(Restrictions.eq("nomenclatura.manzanaLetra", nomenclatura.getManzanaLetra()));
		}
		if (nomenclatura.getParcela() != null && nomenclatura.getParcela() > 0) {
			c.add(Restrictions.eq("nomenclatura.parcela", nomenclatura.getParcela()));
		}
		if (nomenclatura.getParcelaLetra() != null && nomenclatura.getParcelaLetra().length() > 0) {
			c.add(Restrictions.eq("nomenclatura.parcelaLetra", nomenclatura.getParcelaLetra()));
		}
		
		c.add(Restrictions.eq("activa", Boolean.TRUE));
		/*
		 * if (nomenclatura.getUnidadFuncional() != null &&
		 * nomenclatura.getUnidadFuncional().length() > 0) {
		 * c.add(Restrictions.eq("nomenclatura.unidadFuncional",
		 * nomenclatura.getUnidadFuncional())); }
		 */
		return c;
	}
	
	/**
	 * Arma criterio de busqueda x nomenclatura para obtener todas las unidades funcionales de la tabla parcela ratificacion
	 * 
	 * @param nomenclatura
	 * @return
	 */
	
	private Criteria createCriteriaNomenclaturaComplete(Nomenclatura nomenclatura) {
		Criteria c = getSessionFactory().getCurrentSession().createCriteria(ParcelaRatificacion.class);

		if (nomenclatura.getCircunscripcion() != null && nomenclatura.getCircunscripcion() > 0) {
			c.add(Restrictions.eq("nomenclatura.circunscripcion", nomenclatura.getCircunscripcion()));
		}
		if (nomenclatura.getSeccion() != null && nomenclatura.getSeccion().length() > 0) {
			c.add(Restrictions.eq("nomenclatura.seccion", nomenclatura.getSeccion()));
		}
		if (nomenclatura.getFraccion() != null && nomenclatura.getFraccion() > 0) {
			c.add(Restrictions.eq("nomenclatura.fraccion", nomenclatura.getFraccion()));
		}
		if (nomenclatura.getFraccionLetra() != null && nomenclatura.getFraccionLetra().length() > 0) {
			c.add(Restrictions.eq("nomenclatura.fraccionLetra", nomenclatura.getFraccionLetra()));
		}
		if (nomenclatura.getManzana() != null && nomenclatura.getManzana() > 0) {
			c.add(Restrictions.eq("nomenclatura.manzana", nomenclatura.getManzana()));
		}
		if (nomenclatura.getManzanaLetra() != null && nomenclatura.getManzanaLetra().length() > 0) {
			c.add(Restrictions.eq("nomenclatura.manzanaLetra", nomenclatura.getManzanaLetra()));
		}
		if (nomenclatura.getParcela() != null && nomenclatura.getParcela() > 0) {
			c.add(Restrictions.eq("nomenclatura.parcela", nomenclatura.getParcela()));
		}
		if (nomenclatura.getParcelaLetra() != null && nomenclatura.getParcelaLetra().length() > 0) {
			c.add(Restrictions.eq("nomenclatura.parcelaLetra", nomenclatura.getParcelaLetra()));
		}

		if (nomenclatura.getUnidadFuncional() != null &&
		   nomenclatura.getUnidadFuncional().length() > 0) {
		   c.add(Restrictions.eq("nomenclatura.unidadFuncional",
		   nomenclatura.getUnidadFuncional())); 
		}

		return c;
	}
	

	@Override
	public List<Parcela> findParcelasPH(Nomenclatura nomenclatura) {
		Criteria c = createCriteriaNomenclaturaPH(nomenclatura);
		return (List<Parcela>) c.list();
	}

	@Override
	public List<ParcelaRatificacion> findParcelasRatificacion(Nomenclatura nomenclatura) {
		Criteria c = createCriteriaNomenclaturaRatificacion(nomenclatura);
		return (List<ParcelaRatificacion>) c.list();
	}

	@Override
	public List<ParcelaRatificacion> findParcelasRatificacionRemove(Nomenclatura nomenclatura) {
		Criteria c = createCriteriaNomenclaturaRatificacionRemove(nomenclatura);
		return (List<ParcelaRatificacion>) c.list();
	}
	
	@Override
	public List<ParcelaRatificacion> findParcelasRatificacionNew(Nomenclatura nomenclatura) {
		Criteria c = createCriteriaNomenclaturaRatificacionNew(nomenclatura);
		return (List<ParcelaRatificacion>) c.list();
	}

	private Criteria createCriteriaNomenclaturaRatificacion(Nomenclatura nomenclatura) {
		Criteria c = getSessionFactory().getCurrentSession().createCriteria(ParcelaRatificacion.class);

		if (nomenclatura.getCircunscripcion() != null && nomenclatura.getCircunscripcion() > 0) {
			c.add(Restrictions.eq("nomenclatura.circunscripcion", nomenclatura.getCircunscripcion()));
		}
		if (nomenclatura.getSeccion() != null && nomenclatura.getSeccion().length() > 0) {
			c.add(Restrictions.eq("nomenclatura.seccion", nomenclatura.getSeccion()));
		}
		if (nomenclatura.getFraccion() != null && nomenclatura.getFraccion() > 0) {
			c.add(Restrictions.eq("nomenclatura.fraccion", nomenclatura.getFraccion()));
		}
		if (nomenclatura.getFraccionLetra() != null && nomenclatura.getFraccionLetra().length() > 0) {
			c.add(Restrictions.eq("nomenclatura.fraccionLetra", nomenclatura.getFraccionLetra()));
		}
		if (nomenclatura.getManzana() != null && nomenclatura.getManzana() > 0) {
			c.add(Restrictions.eq("nomenclatura.manzana", nomenclatura.getManzana()));
		}
		if (nomenclatura.getManzanaLetra() != null && nomenclatura.getManzanaLetra().length() > 0) {
			c.add(Restrictions.eq("nomenclatura.manzanaLetra", nomenclatura.getManzanaLetra()));
		}
		if (nomenclatura.getParcela() != null && nomenclatura.getParcela() > 0) {
			c.add(Restrictions.eq("nomenclatura.parcela", nomenclatura.getParcela()));
		}
		if (nomenclatura.getParcelaLetra() != null && nomenclatura.getParcelaLetra().length() > 0) {
			c.add(Restrictions.eq("nomenclatura.parcelaLetra", nomenclatura.getParcelaLetra()));
		}

		return c;
	}

	private Criteria createCriteriaNomenclaturaRatificacionRemove(Nomenclatura nomenclatura) {
		Criteria c = getSessionFactory().getCurrentSession().createCriteria(ParcelaRatificacion.class);

		if (nomenclatura.getCircunscripcion() != null && nomenclatura.getCircunscripcion() > 0) {
			c.add(Restrictions.eq("nomenclatura.circunscripcion", nomenclatura.getCircunscripcion()));
		}
		if (nomenclatura.getSeccion() != null && nomenclatura.getSeccion().length() > 0) {
			c.add(Restrictions.eq("nomenclatura.seccion", nomenclatura.getSeccion()));
		}
		if (nomenclatura.getFraccion() != null && nomenclatura.getFraccion() > 0) {
			c.add(Restrictions.eq("nomenclatura.fraccion", nomenclatura.getFraccion()));
		}
		if (nomenclatura.getFraccionLetra() != null && nomenclatura.getFraccionLetra().length() > 0) {
			c.add(Restrictions.eq("nomenclatura.fraccionLetra", nomenclatura.getFraccionLetra()));
		}
		if (nomenclatura.getManzana() != null && nomenclatura.getManzana() > 0) {
			c.add(Restrictions.eq("nomenclatura.manzana", nomenclatura.getManzana()));
		}
		if (nomenclatura.getManzanaLetra() != null && nomenclatura.getManzanaLetra().length() > 0) {
			c.add(Restrictions.eq("nomenclatura.manzanaLetra", nomenclatura.getManzanaLetra()));
		}
		if (nomenclatura.getParcela() != null && nomenclatura.getParcela() > 0) {
			c.add(Restrictions.eq("nomenclatura.parcela", nomenclatura.getParcela()));
		}
		if (nomenclatura.getParcelaLetra() != null && nomenclatura.getParcelaLetra().length() > 0) {
			c.add(Restrictions.eq("nomenclatura.parcelaLetra", nomenclatura.getParcelaLetra()));
		}

		c.add(Restrictions.eq("deleted", Boolean.TRUE));

		return c;
	}
	
	
	private Criteria createCriteriaNomenclaturaRatificacionNew(Nomenclatura nomenclatura) {
		Criteria c = getSessionFactory().getCurrentSession().createCriteria(ParcelaRatificacion.class);

		if (nomenclatura.getCircunscripcion() != null && nomenclatura.getCircunscripcion() > 0) {
			c.add(Restrictions.eq("nomenclatura.circunscripcion", nomenclatura.getCircunscripcion()));
		}
		if (nomenclatura.getSeccion() != null && nomenclatura.getSeccion().length() > 0) {
			c.add(Restrictions.eq("nomenclatura.seccion", nomenclatura.getSeccion()));
		}
		if (nomenclatura.getFraccion() != null && nomenclatura.getFraccion() > 0) {
			c.add(Restrictions.eq("nomenclatura.fraccion", nomenclatura.getFraccion()));
		}
		if (nomenclatura.getFraccionLetra() != null && nomenclatura.getFraccionLetra().length() > 0) {
			c.add(Restrictions.eq("nomenclatura.fraccionLetra", nomenclatura.getFraccionLetra()));
		}
		if (nomenclatura.getManzana() != null && nomenclatura.getManzana() > 0) {
			c.add(Restrictions.eq("nomenclatura.manzana", nomenclatura.getManzana()));
		}
		if (nomenclatura.getManzanaLetra() != null && nomenclatura.getManzanaLetra().length() > 0) {
			c.add(Restrictions.eq("nomenclatura.manzanaLetra", nomenclatura.getManzanaLetra()));
		}
		if (nomenclatura.getParcela() != null && nomenclatura.getParcela() > 0) {
			c.add(Restrictions.eq("nomenclatura.parcela", nomenclatura.getParcela()));
		}
		if (nomenclatura.getParcelaLetra() != null && nomenclatura.getParcelaLetra().length() > 0) {
			c.add(Restrictions.eq("nomenclatura.parcelaLetra", nomenclatura.getParcelaLetra()));
		}

		c.add(Restrictions.eq("newuf", Boolean.TRUE));

		return c;
	}

	@Override
	public CuentaRatificacion findCuentaRatificacionByCuenta(String cuenta) {
		String hql = "Select p from CuentaRatificacion p where p.cuenta = :cuenta";
		Query q = getSessionFactory().getCurrentSession().createQuery(hql);
		q.setParameter("cuenta", cuenta);

		return (CuentaRatificacion) q.list().get(0);
	}

	@Override
	public List<CuentaRatificacion> findCuentasRatificacionByTramite(Tramite tramite) {
	    Criteria c = getSessionFactory().getCurrentSession().createCriteria(CuentaRatificacion.class);
        c.add(Restrictions.eq("tramite.id", tramite.getId()));

		return (List<CuentaRatificacion>) c.list();
	}

	/**
	 * Arma criterio de busqueda x nomenclatura
	 * 
	 * @param nomenclatura
	 * @return
	 */
	private Criteria createCriteriaNomenclaturaPH(Nomenclatura nomenclatura) {
		Criteria c = getSessionFactory().getCurrentSession().createCriteria(Parcela.class);

		if (nomenclatura.getCircunscripcion() != null && nomenclatura.getCircunscripcion() > 0) {
			c.add(Restrictions.eq("nomenclatura.circunscripcion", nomenclatura.getCircunscripcion()));
		}
		if (nomenclatura.getSeccion() != null && nomenclatura.getSeccion().length() > 0) {
			c.add(Restrictions.eq("nomenclatura.seccion", nomenclatura.getSeccion()));
		}
		if (nomenclatura.getFraccion() != null && nomenclatura.getFraccion() > 0) {
			c.add(Restrictions.eq("nomenclatura.fraccion", nomenclatura.getFraccion()));
		}
		if (nomenclatura.getFraccionLetra() != null && nomenclatura.getFraccionLetra().length() > 0) {
			c.add(Restrictions.eq("nomenclatura.fraccionLetra", nomenclatura.getFraccionLetra()));
		}
		if (nomenclatura.getManzana() != null && nomenclatura.getManzana() > 0) {
			c.add(Restrictions.eq("nomenclatura.manzana", nomenclatura.getManzana()));
		}
		if (nomenclatura.getManzanaLetra() != null && nomenclatura.getManzanaLetra().length() > 0) {
			c.add(Restrictions.eq("nomenclatura.manzanaLetra", nomenclatura.getManzanaLetra()));
		}
		if (nomenclatura.getParcela() != null && nomenclatura.getParcela() > 0) {
			c.add(Restrictions.eq("nomenclatura.parcela", nomenclatura.getParcela()));
		}
		if (nomenclatura.getParcelaLetra() != null && nomenclatura.getParcelaLetra().length() > 0) {
			c.add(Restrictions.eq("nomenclatura.parcelaLetra", nomenclatura.getParcelaLetra()));
		}
		if (nomenclatura.getUnidadFuncional() != null && nomenclatura.getUnidadFuncional().length() > 0) {
			c.add(Restrictions.eq("nomenclatura.unidadFuncional", nomenclatura.getUnidadFuncional()));
		}
		return c;
	}

	@Override
	public List<TitularRatificacion> findTitulares(TitularRatificacion titular) {
		Criteria c = getSessionFactory().getCurrentSession().createCriteria(TitularRatificacion.class);
		if (titular.getApellido() != null && titular.getApellido().length() > 0) {
			c.add(Restrictions.like("apellido", titular.getApellido()));
		}
		if (titular.getNombre() != null && titular.getNombre().length() > 0) {
			c.add(Restrictions.like("nombre", titular.getNombre()));
		}
		if (titular.getCuitcuil() != null && titular.getCuitcuil().length() > 0) {
			c.add(Restrictions.like("cuitcuil", titular.getCuitcuil()));
		}
		return (List<TitularRatificacion>) c.list();
	}
	
    @Override
    public boolean exists(Nomenclatura nomenclatura) {
        Criteria c = createCriteriaNomenclaturaComplete(nomenclatura);
        return (c.uniqueResult() == null) ? false : true;
    }
}
