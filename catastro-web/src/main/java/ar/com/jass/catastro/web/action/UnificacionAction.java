package ar.com.jass.catastro.web.action;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import ar.com.jass.catastro.business.BusinessHelper;
import ar.com.jass.catastro.business.CatastroService;
import ar.com.jass.catastro.exceptions.BusinessException;
import ar.com.jass.catastro.model.Cuenta;
import ar.com.jass.catastro.model.Domicilio;
import ar.com.jass.catastro.model.Linea;
import ar.com.jass.catastro.model.Nomenclatura;
import ar.com.jass.catastro.model.Parcela;
import ar.com.jass.catastro.model.ResponsableCuenta;
import ar.com.jass.catastro.model.TipoTramite;
import ar.com.jass.catastro.model.TitularParcela;
import ar.com.jass.catastro.model.Tramite;
import ar.com.jass.catastro.model.error.ErrorLog;
import ar.com.jass.catastro.web.commons.WebConstants;

/**
 * Action de gestion de Unificacion de parcelas
 * 
 * @author msquarini
 *
 */
public class UnificacionAction extends GridBaseAction<Parcela> {

    private static final long serialVersionUID = 7353477345330099548L;

    private static final Logger LOG = LoggerFactory.getLogger("UnificacionAction");

    private static final String LIST_PARCELAS = "lista_parcelas";

    @Autowired
    private CatastroService catastroService;

    @Autowired
    private BusinessHelper businessHelper;

    /**
     * Datos de nueva parcela (formulario)
     */
    private Parcela nuevaParcela;

    /**
     * Indentificadores de parcela para unificar
     */
    private List<Long> idsParcela;
    private Long idParcelaTORemove;

    /**
     * Lista de parcelas a unificar
     */
    private List<Parcela> parcelasAUnificar;

    /**
     * Tipo de tramite de unificacion
     */
    private Integer tipoUnificacion;

    /**
     * Tramite de unificacion
     */
    private Tramite tramite;

    /**
     * Si permite el borrado en la grilla de cuentas origen
     */
    public Boolean canDelete = Boolean.TRUE;

    /**
     * Agrego una nueva parcela origen en la lista para la unificacion
     * 
     * Verifico reglas de negocio (no presenta deuda) Deben pertenecer al mismo macizo y no ser UF
     * 
     * @return
     */
    public String addParcelaTOUnificacion() {
        LOG.info("Agregar parcela a proceso de unificacion, {}", idsParcela);

        parcelasAUnificar = (List<Parcela>) getFromSession(LIST_PARCELAS);

        // Recorro la lista de ids a unificar
        for (Long id : idsParcela) {
            Parcela p = getConsultaService().findParcelaById(id, "titulares", "domicilioInmueble",
                    "cuenta.responsables");

            if (!p.getCuenta().getActiva()) {
                LOG.warn("Cuenta no activa: {}", p.getCuenta());
                addActionError(getText("error.cuenta.inactiva"));
                return "errorJSON";
            }
            if (p.getCuenta().isBloqueada()) {
                LOG.warn("Cuenta bloqueada: {}", p.getCuenta());
                addActionError(getText("error.cuenta.bloqueada"));
                return "errorJSON";
            }
            /*
             * if (p.getNomenclatura().isUF()) { LOG.warn("Parcela es UF: {}", p.getCuenta());
             * addActionError(getText("error.unificacion.uf")); return "errorJSON"; }
             */

            /**
             * Es la primera en la lista --> Mantengo la nomenclatura en session para validar MACIZO
             * con las siguientes Se transaladan los responsables de la primera cuenta a la nueva
             */
            if (parcelasAUnificar.isEmpty()) {
                setInSession(WebConstants.NOMENCLATURA, p.getNomenclatura());
                setInSession(WebConstants.RESPONSABLES_ADD,
                        new HashSet<ResponsableCuenta>(p.getCuenta().getResponsables()));
            } else {
                if (!p.getNomenclatura().mismoMacizo((Nomenclatura) getFromSession(WebConstants.NOMENCLATURA))) {
                    LOG.warn("Parcela no pertenece al mismo macizo: {}", p.getCuenta());
                    addActionError(getText("error.unificacion.macizo"));
                    return "errorJSON";
                }
            }

            // Si no existe en la lista la agrego
            if (parcelasAUnificar.indexOf(p) < 0) {

                // Verificar que tengan los mismos titulares ambas parcelas!!
                try {
                    verificarTitulares(p);

                    // TODO eliminar comentario, solo para pruebas
                    /*
                     * if (catastroService.presentaDeuda(p.getCuenta())) {
                     * LOG.warn("Cuenta presenta deuda: {}", p.getCuenta());
                     * addActionError("Cuenta presenta deuda"); return "errorJSON"; }
                     */

                } catch (Exception e) {
                    LOG.error("Error al intentar agregar parcela en lista de unificacion", e);
                    addActionError(e.getMessage());
                    return "errorJSON";
                }

                parcelasAUnificar.add(p);
            } else {
                LOG.warn("Parcela ya en la lista de parcelas a unificar");
                addActionError(getText("error.unificacion.existe"));
                return "errorJSON";
            }
        }
        setInSession(LIST_PARCELAS, parcelasAUnificar);

        return "addresponse";
    }

    /**
     * Elimino una nueva parcela origen de la lista para la unificacion
     * 
     * @return
     */
    public String removeParcelaTOUnificacion() {

        LOG.info("Eliminar parcela del proceso de unificacion, {}", idParcelaTORemove);

        parcelasAUnificar = (List<Parcela>) getFromSession(LIST_PARCELAS);
        Parcela toRemove = null;
        for (Parcela p : parcelasAUnificar) {
            if (p.getId().equals(idParcelaTORemove)) {
                toRemove = p;
                break;
            }
        }
        parcelasAUnificar.remove(toRemove);

        // Si no parcelas seleccionadas --> blanqueo la lista de titulares
        if (parcelasAUnificar.isEmpty()) {
            setInSession(WebConstants.TITULARES_ADD, null);
        }
        setInSession(LIST_PARCELAS, parcelasAUnificar);

        return "addresponse";
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.opensymphony.xwork2.ActionSupport#input()
     */
    public String input() {
        // Parcelas a unificar
        setInSession(LIST_PARCELAS, new ArrayList<Parcela>());

        initCustomData();

        /**
         * Seteo en NULL los titulares para permitir agregar los correspondientes a la primer
         * parcela seleccionada
         */
        setInSession(WebConstants.TITULARES_ADD, null);
        return "input";
    }

    /**
     * Llamada AJAX para obtener la informacion para la grilla en formato JSON
     * 
     * @return
     */
    public String getData() {
        LOG.info("getData Parcelas Unificacion");

        try {
            List<Parcela> data = (List<Parcela>) getFromSession(LIST_PARCELAS);
            setData(data);
        } catch (Exception e) {
            LOG.error("error en getData parcelas", e);
        }
        return "dataJSON";
    }

    /**
     * Validacion de parcelas a unificar e inicio de carga de datos de nueva parcela
     * 
     * @return
     */
    @SuppressWarnings("unchecked")
    public String init() {
        LOG.info("init proceso de unificacion.");
        canDelete = null;

        Parcela parcelaAUnificar = ((List<Parcela>) getFromSession(LIST_PARCELAS)).get(0);
        nuevaParcela = new Parcela();
        nuevaParcela.setNomenclatura(new Nomenclatura(parcelaAUnificar.getNomenclatura()));
        nuevaParcela.setDomicilioInmueble(new Domicilio(parcelaAUnificar.getDomicilioInmueble()));

        List<Parcela> parcelasOrigen = (List<Parcela>) getFromSession(LIST_PARCELAS);
        List<Cuenta> cuentasOrigen = new ArrayList<>(parcelasOrigen.size());

        Linea vigente = nuevaParcela.getLineaVigente();
        for (Parcela po : parcelasOrigen) {
            cuentasOrigen.add(po.getCuenta());

            vigente.setValorComun(vigente.getValorComun().add(po.getLineaVigente().getValorComun()));
            vigente.setValorEdificio(vigente.getValorEdificio().add(po.getLineaVigente().getValorEdificio()));
            vigente.setValorTierra(vigente.getValorTierra().add(po.getLineaVigente().getValorTierra()));

            vigente.setSuperficieCubierta(
                    vigente.getSuperficieCubierta() + po.getLineaVigente().getSuperficieCubierta());
            vigente.setSuperficieSemi(vigente.getSuperficieSemi() + po.getLineaVigente().getSuperficieSemi());
            vigente.setSuperficieTerreno(vigente.getSuperficieTerreno() + po.getLineaVigente().getSuperficieTerreno());
        }

        /*
         * 20180225 Prueba CREAMOS el tramite y lo mantemos en sesion
         */
        tramite = new Tramite(TipoTramite.UNI, new ArrayList<Cuenta>(), cuentasOrigen);
        try {
            tramite = catastroService.grabarTramite(tramite, new ArrayList<Parcela>(), null, null, Boolean.FALSE);
        } catch (Exception e) {
            LOG.error("", e);
            return "process_error";
        }
        setInSession(WebConstants.TRAMITE, tramite);
        return "inprocess";
    }

    /**
     * Continuar la gestion de unificacion desde un tramite previo
     * 
     * @return
     */
    public String initFromTramite() {
        canDelete = null;
        tramite = (Tramite) getFromSession(WebConstants.TRAMITE);
        LOG.info("init proceso de unificacion desde tramite grabado {}.", tramite);

        // Parcelas Origen a UNIFICAF
        setInSession(LIST_PARCELAS, tramite.getParcelasOrigen());

        // Titulares en session para completar CUITCUIL
        Cuenta origen = tramite.getCuentasOrigen().get(0);
        setInSession(WebConstants.TITULARES_ADD, new HashSet<TitularParcela>(origen.getParcela().getTitulares()));
        setInSession(WebConstants.RESPONSABLES_ADD, new HashSet<ResponsableCuenta>(origen.getResponsables()));

        // Nueva Parcela
        nuevaParcela = (tramite.getParcelas().size() == 0) ? null : tramite.getParcelas().get(0);
        if (nuevaParcela == null) {
            nuevaParcela = new Parcela();
            nuevaParcela.setNomenclatura(new Nomenclatura(origen.getParcela().getNomenclatura()));
            nuevaParcela.setDomicilioInmueble(new Domicilio(origen.getParcela().getDomicilioInmueble()));
        }
        return "inprocess";
    }

    /**
     * Grabar el tramite para un posterior tratamiento (Sin procesar!!!)
     * 
     * @return
     */
    public String saveTramite() {
        LOG.info("Grabando tramite de unificacion");

        try {
            /**
             * Si no es el tramite por (6) Plano sin apertura en ARBA Validamos que no exista deuda
             * en la cuenta origen
             */
            // VALIDAR chequeo de DEUDA
            /*
             * if (!tramite.getCodigo().equals(6)) { if
             * (catastroService.presentaDeuda(cuentaOrigen)) { LOG.warn("Cuenta presenta deuda: {}",
             * cuentaOrigen); addActionError("Cuenta presenta deuda"); return "process_error"; } }
             */

            tramite = actualizarTramite(tramite);
            if (tramite == null) {
                throw new IllegalStateException();
            }
            
            if (!tramite.getOficio()) {
                Cuenta cuentaOrigen = tramite.getCuentasOrigen().get(0);
                if (catastroService.presentaDeuda(cuentaOrigen)) {
                    LOG.warn("Cuenta presenta deuda: {}", cuentaOrigen);
                    addActionError("Cuenta presenta deuda");
                    return "process_error";
                }
            }

            List<TitularParcela> titulares = new ArrayList<>(
                    (Set<TitularParcela>) getFromSession(WebConstants.TITULARES_ADD));
            List<ResponsableCuenta> responsables = new ArrayList<>(
                    (Set<ResponsableCuenta>) getFromSession(WebConstants.RESPONSABLES_ADD));
            // PROBLEMA: NUEVA PARCELA SE CREA SIEMPRE POR LO CUAL SE PRODUCE INSERT EN BASE Y ERORR
            // DE CLAVE DUPLICADA

            Tramite t = actualizarTramite(tramite);
            tramite = catastroService.grabarTramite(t, Arrays.asList(getNuevaParcela()), titulares, responsables,
                    Boolean.FALSE);
            setInSession(WebConstants.TRAMITE, tramite);

            clearMessages();
            addActionMessage(getText("tramite.save.success"));
            return "success";
        } catch (Exception e) {
            ErrorLog log = crearErrorLog("saveTramite", e.getMessage());
            LOG.error("{}", log, e);
            addActionError(getText(e.getMessage()));
            return "process_error";
        }
    }

    /**
     * Procesamiento de la unificacion!!!!
     * 
     * @return
     */
    public String process() {
        LOG.info("Procesando Unificacion");

        try {
            tramite = actualizarTramite(tramite);
            if (tramite == null) {
                throw new IllegalStateException();
            }

            if (!tramite.getOficio()) {
                Cuenta cuentaOrigen = tramite.getCuentasOrigen().get(0);
                if (catastroService.presentaDeuda(cuentaOrigen)) {
                    LOG.warn("Cuenta presenta deuda: {}", cuentaOrigen);
                    addActionError("Cuenta presenta deuda");
                    return "process_error";
                }
            }

            List<TitularParcela> titulares = new ArrayList<TitularParcela>(
                    (Set<TitularParcela>) getFromSession(WebConstants.TITULARES_ADD));

            // Chequear que los Titulares presenten CUIT/CUIL cargado/actualizado
            businessHelper.checkTitularesConCuit(titulares);

            // Verificar que la nomenclatura no existe en la BD
            if (getConsultaService().exists(getNuevaParcela())) {
                LOG.warn("Nomenclatura existente en BD, {}", getNuevaParcela());
                addActionError(getText("nomenclatura.existente"));
                return "process_error";
            }

            List<ResponsableCuenta> responsables = new ArrayList<>(
                    (Set<ResponsableCuenta>) getFromSession(WebConstants.RESPONSABLES_ADD));

            tramite = catastroService.grabarTramite(tramite, Arrays.asList(getNuevaParcela()), titulares, responsables,
                    Boolean.TRUE);

            // Para pruebas!!!!
            // tramite = getConsultaService().findTramite(43L, "cuentas.parcelas");

            removeTramiteDataFromSession();

            clearMessages();
            addActionMessage(getText("tramite.process.success"));
        } catch (BusinessException be) {
            ErrorLog log = crearErrorLog("saveTramite", be.getMessage());
            LOG.error("error de negocio en procesamiento de subdivision, {}", log, be);
            addActionError(getText(be.getMessage()));
            return "process_error";
        } catch (Exception e) {
            ErrorLog log = crearErrorLog("saveTramite", e.getMessage());
            LOG.error("error en procesamiento de unificacion, {}", log, e);
            addActionError(getText("error.unknown"));
            return "process_error";
        } finally {

        }
        // Para chequear error en pantalla rapidamente!!
        /*
         * if (getOrigen().getNumero() > 5) { addActionError(getText("unificacion_process_error"));
         * return "process_error"; }
         */
        return "success";
    }

    /**
     * Dada una parcela se verifican que compartan los mismo titulares que las otras parcelas de la
     * unificacion
     * 
     * @param p
     */
    private void verificarTitulares(Parcela p) {
        Set<TitularParcela> titulares = (Set<TitularParcela>) getFromSession(WebConstants.TITULARES_ADD);

        if (titulares == null) {
            LOG.info("Se agregan los titulares de la parcela a unificar");
            titulares = new HashSet<TitularParcela>();
            titulares.addAll(p.getTitulares());

            // Titulares en session para completar CUITCUIL
            setInSession(WebConstants.TITULARES_ADD, titulares);
        } else {
            // Comentamos la validacion de titulares
            /*
             * if (titulares.size() != p.getTitulares().size()) { throw new
             * IllegalStateException("parcelas con diferentes titulares"); } else { // Valido uno a
             * uno los titulares verificando que sean los // mismos for (Titular t : titulares) { if
             * (!p.getTitulares().contains(t)) { throw new
             * IllegalStateException("parcelas con diferentes titulares"); } } }
             */

        }
    }

    public CatastroService getCatastroService() {
        return catastroService;
    }

    public void setCatastroService(CatastroService catastroService) {
        this.catastroService = catastroService;
    }

    public Parcela getNuevaParcela() {
        tramite = (Tramite) getFromSession(WebConstants.TRAMITE);
        if (tramite.getParcelas().size() == 1 && tramite.getParcelas().get(0).getId() != null) {
            return tramite.getParcelas().get(0);
        } else {
            return nuevaParcela;
        }
    }

    public void setNuevaParcela(Parcela nuevaParcela) {
        this.nuevaParcela = nuevaParcela;
    }

    public List<Long> getIdsParcela() {
        return idsParcela;
    }

    public void setIdsParcela(List<Long> idsParcela) {
        this.idsParcela = idsParcela;
    }

    public Long getIdParcelaTORemove() {
        return idParcelaTORemove;
    }

    public void setIdParcelaTORemove(Long idParcelaTORemove) {
        this.idParcelaTORemove = idParcelaTORemove;
    }

    public List<Parcela> getParcelasAUnificar() {
        return parcelasAUnificar;
    }

    public void setParcelasAUnificar(List<Parcela> parcelasAUnificar) {
        this.parcelasAUnificar = parcelasAUnificar;
    }

    public Integer getTipoUnificacion() {
        return tipoUnificacion;
    }

    public void setTipoUnificacion(Integer tipo) {
        this.tipoUnificacion = tipo;
    }

    public Boolean getCanDelete() {
        return canDelete;
    }

    public void setCanDelete(Boolean canDelete) {
        this.canDelete = canDelete;
    }

    public Tramite getTramite() {
        return tramite;
    }

    public void setTramite(Tramite tramite) {
        this.tramite = tramite;
    }

}
