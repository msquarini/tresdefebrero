package ar.com.jass.catastro.batch.pagos;

import java.util.ArrayList;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.LineMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.LineTokenizer;
import org.springframework.core.io.Resource;

import ar.com.jass.catastro.business.impl.FileType;
import ar.com.jass.catastro.model.pagos.FilePago;
import ar.com.jass.catastro.model.pagos.FilePagosRecord;
import ar.com.jass.catastro.model.pagos.Pago;

/**
 * 
 * @author msquarini
 *
 */
public class PagosCommonReader implements ItemReader<FilePago> {

	private static final Logger LOG = LoggerFactory.getLogger("PagosCommonReader");

	private FilePago fileToProcess;

	private FlatFileItemReader<FilePagosRecord> delegate;
	
	/**
	 * Mapa de tokenizadores para cada tipo de archivo de pagos
	 */
	private Map<String, LineTokenizer> tokenizers;
	
	/**
	 * Mapeador comun de campos
	 */
	private FieldSetMapper<FilePagosRecord> commonFieldSetMapper;

	private Boolean finish = Boolean.FALSE;
	
	@BeforeStep
	public void retrieveInterstepData(StepExecution stepExecution) {
		JobExecution jobExecution = stepExecution.getJobExecution();
		ExecutionContext jobContext = jobExecution.getExecutionContext();

		fileToProcess = (FilePago) jobContext.get("filepago");		
		Resource resource = new org.springframework.core.io.FileSystemResource((String) jobContext.get("pathtofile"));
		delegate.setResource(resource);
		
		// Seteamos el line mapper correspondiente al tipo de origen del archivo de pagos
		delegate.setLineMapper(getLineMapper(fileToProcess.getOrigen()));

		fileToProcess.setPagos(new ArrayList<Pago>());
		LOG.info("(INIT) Leyendo {}", fileToProcess);
	}

	private LineMapper<FilePagosRecord> getLineMapper(FileType type) {
		DefaultLineMapper<FilePagosRecord> lm = new DefaultLineMapper<>();
		lm.setFieldSetMapper(getCommonFieldSetMapper());
		
		LineTokenizer tokenizer = tokenizers.get(type.name());
		if (tokenizer == null) {
			throw new IllegalStateException("No existe tokenizador para el tipo de archivo, " + type.name());
		}
		lm.setLineTokenizer(tokenizer);
		
		return lm;
	}
	
	public FilePago read() throws Exception {
		for (FilePagosRecord record = null; (record = this.delegate.read()) != null;) {
			LOG.info("(REGPAGO), {}", record);
			record.complete(fileToProcess);			
		}
		
		// Flag para hacer una sola pasada
		if (finish) {
			return null;
		}
		// Cambio el estado para finalizar la lectura		
		finish = Boolean.TRUE;
		return fileToProcess;		
	}

	public FlatFileItemReader<FilePagosRecord> getDelegate() {
		return delegate;
	}

	public void setDelegate(FlatFileItemReader<FilePagosRecord> delegate) {
		this.delegate = delegate;
	}

	public FilePago getFileToProcess() {
		return fileToProcess;
	}

	public void setFileToProcess(FilePago fileToProcess) {
		this.fileToProcess = fileToProcess;
	}

	public Map<String, LineTokenizer> getTokenizers() {
		return tokenizers;
	}

	public void setTokenizers(Map<String, LineTokenizer> tokenizers) {
		this.tokenizers = tokenizers;
	}

	public FieldSetMapper<FilePagosRecord> getCommonFieldSetMapper() {
		return commonFieldSetMapper;
	}

	public void setCommonFieldSetMapper(FieldSetMapper<FilePagosRecord> commonFieldSetMapper) {
		this.commonFieldSetMapper = commonFieldSetMapper;
	}

}