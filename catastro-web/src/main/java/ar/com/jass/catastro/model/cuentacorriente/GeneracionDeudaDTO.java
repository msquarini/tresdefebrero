package ar.com.jass.catastro.model.cuentacorriente;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import ar.com.jass.catastro.model.Cuenta;

/**
 * DTO de cuentas usado en el proceso de generacion de deuda
 * 
 * @author mesquarini
 *
 */
public class GeneracionDeudaDTO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private Long idCuenta;
    private String cuenta;
    private String dv;
    private Boolean exenta;
    private Integer categoria;
    private BigDecimal valorTierra;
    private BigDecimal valorComun;
    private BigDecimal valorEdificio;
    private boolean sinValuacion;
    private boolean conTope;

    private List<CuentaCorriente> movimientos;
        
    public GeneracionDeudaDTO() {
        movimientos = new ArrayList<CuentaCorriente>();
    }

    public Long getIdCuenta() {
        return idCuenta;
    }

    public void setIdCuenta(Long idCuenta) {
        this.idCuenta = idCuenta;
    }

    public String getCuenta() {
        return cuenta;
    }

    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }

    public String getDv() {
        return dv;
    }

    public void setDv(String dv) {
        this.dv = dv;
    }

    public Boolean getExenta() {
        return exenta;
    }

    public void setExenta(Boolean exenta) {
        this.exenta = exenta;
    }

    public BigDecimal getValorTierra() {
        return valorTierra;
    }

    public void setValorTierra(BigDecimal valorTierra) {
        this.valorTierra = valorTierra;
    }

    public BigDecimal getValorComun() {
        return valorComun;
    }

    public void setValorComun(BigDecimal valorComun) {
        this.valorComun = valorComun;
    }

    public BigDecimal getValorEdificio() {
        return valorEdificio;
    }

    public void setValorEdificio(BigDecimal valorEdificio) {
        this.valorEdificio = valorEdificio;
    }

    public Integer getCategoria() {
        return categoria;
    }

    public void setCategoria(Integer categoria) {
        this.categoria = categoria;
    }

    /**
     * Valuacion para el calculo de cuota
     * 
     * @return
     */
    public BigDecimal getValuacion() {
        return getValorComun().add(getValorEdificio()).add(getValorTierra());
    }

    @Override
    public String toString() {
        return "GeneracionDeudaDTO [idCuenta=" + idCuenta + ", cuenta=" + cuenta + ", dv=" + dv + ", exenta=" + exenta
                + ", categoria=" + categoria + ", valorTierra=" + valorTierra + ", valorComun=" + valorComun
                + ", valorEdificio=" + valorEdificio + " ]";
    }

    public List<CuentaCorriente> getMovimientos() {
        return movimientos;
    }

    public void setMovimientos(List<CuentaCorriente> movimientos) {
        this.movimientos = movimientos;
    }

    public void addMovimiento(CuentaCorriente movimiento) {
        getMovimientos().add(movimiento);
    }

    public boolean isSinValuacion() {
        return (valorTierra == null || valorComun == null || valorEdificio == null);
    }
    
    public static class Builder {
        private Cuenta cuenta;
        
        public Builder() {            
        }
        
        public Builder cuenta(Cuenta cuenta) {
            this.cuenta = cuenta;
            return this;
        }
        
        public GeneracionDeudaDTO build() {
            GeneracionDeudaDTO dto = new GeneracionDeudaDTO();
            dto.setCategoria(cuenta.getParcela().getLineaVigente().getCategoria());
            dto.setCuenta(cuenta.getCuenta());
            dto.setDv(cuenta.getDv());
            dto.setExenta(cuenta.isExenta());
            dto.setIdCuenta(cuenta.getId());
            dto.setValorComun(cuenta.getParcela().getLineaVigente().getValorComun());
            dto.setValorEdificio(cuenta.getParcela().getLineaVigente().getValorEdificio());
            dto.setValorTierra(cuenta.getParcela().getLineaVigente().getValorTierra());
            dto.setConTope(cuenta.getConTope());
            return dto;
        }
    }

    public boolean isConTope() {
        return conTope;
    }

    public void setConTope(boolean conTope) {
        this.conTope = conTope;
    }
}
