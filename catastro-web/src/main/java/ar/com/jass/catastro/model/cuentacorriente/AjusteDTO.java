package ar.com.jass.catastro.model.cuentacorriente;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.List;

import org.jfree.util.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ar.com.jass.catastro.business.impl.DeudaServiceImpl;
import ar.com.jass.catastro.model.Cuenta;
import ar.com.jass.catastro.model.Linea;

/**
 * DTO de cuentas usado en el proceso de ajuste de deuda
 * 
 * @author mesquarini
 *
 */
public class AjusteDTO implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(AjusteDTO.class);

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private Integer anio;
    private Cuenta cuenta;
    private Integer categoria;
    private BigDecimal nuevaValuacion;
    private Integer codigoAdministrativo;
    private Date fechaVtoAjustes;

    private Boolean oficio;

    // por el metodo calcularDiferencial
    private BigDecimal montoCuotaNueva;
    private BigDecimal montoCuotaDoce;

    /**
     * % de ajuste diferencial entre valor cuota nuevo y original
     */
    private BigDecimal diferencial;

    /**
     * Periodo a ajustar
     */
    private Periodo periodo;
    /**
     * Mov a ajustar
     */
    private List<CuentaCorriente> movimientos;

    public AjusteDTO() {
        diferencial = BigDecimal.ZERO;
    }

    /**
     * Aplica el calculo de ajuste segun criterio: si cc es anio previo -> se aplica el diferencial
     * de cuota 12 si cc es anio corriente -> se aplica diferencia entre cuotas
     * 
     * @param cc
     * @return
     */
    /*
     * public BigDecimal montoAjuste(CuentaCorriente cc) { if (getAnio().intValue() ==
     * cc.getAnio().intValue()) { return getMontoCuotaNueva().subtract(cc.getMontoTotal()); } else {
     * return cc.getMontoTotal().multiply(getDiferencial().setScale(2, RoundingMode.DOWN)); } }
     */
    // 20181101 MS - A pedido de Juliana se hace el calculo siempre con el % de variacion para todas
    // los periodos
    // public BigDecimal montoAjuste(CuentaCorriente cc) {
    public BigDecimal montoAjuste() {
        return sumarizarMovimientosAjuste(movimientos).multiply(getDiferencial()).setScale(2, RoundingMode.DOWN);
        // return cc.getMontoTotal().multiply(getDiferencial()).setScale(2, RoundingMode.DOWN);
    }

    public Integer getCategoria() {
        return categoria;
    }

    public BigDecimal getMontoCuotaNueva() {
        return montoCuotaNueva;
    }

    public BigDecimal getMontoCuotaDoce() {
        return montoCuotaDoce;
    }

    /*
     * public void calcularDiferencial(BigDecimal nuevoMonto, CuentaCorriente cc) { montoCuotaNueva
     * = nuevoMonto; if (cc != null) { montoCuotaDoce = cc.getMontoTotal(); diferencial =
     * (montoCuotaNueva.subtract(montoCuotaDoce)).divide(montoCuotaDoce, 5, RoundingMode.DOWN); } }
     */
    public void calcularDiferencial(BigDecimal nuevoMonto, List<CuentaCorriente> ccs) {
        montoCuotaNueva = nuevoMonto;
        if (ccs != null && ccs.size() > 0) {
            montoCuotaDoce = sumarizarMovimientosAjuste(ccs);
            diferencial = (montoCuotaNueva.subtract(montoCuotaDoce)).divide(montoCuotaDoce, 5, RoundingMode.DOWN);
        }
        LOG.debug("nuevoMonto: {}, lista de movs {}, sumC12: {}, diferencial: {}", nuevoMonto, ccs.size(),
                montoCuotaDoce, diferencial);
    }

    public void calcularDiferencial(BigDecimal nuevoMonto, BigDecimal subtotalPrevio) {
        montoCuotaNueva = nuevoMonto;
        if (subtotalPrevio != null && subtotalPrevio.compareTo(BigDecimal.ZERO) != 0) {
            montoCuotaDoce = subtotalPrevio;
            diferencial = (montoCuotaNueva.subtract(montoCuotaDoce)).divide(montoCuotaDoce, 5, RoundingMode.DOWN);
        }
    }

    public BigDecimal getDiferencial() {
        return diferencial;
    }

    public Cuenta getCuenta() {
        return cuenta;
    }

    public Integer getCodigoAdministrativo() {
        return codigoAdministrativo;
    }

    /*
     * public Date getFechaVtoAjustes() { return fechaVtoAjustes; }
     */

    /**
     * Devuelve la fecha de vto para el ajuste. Si es oficio se mantiene la fecha de vto original
     * 
     * @param original
     * @return
     */
    public Date getFechaVtoAjuste(CuentaCorriente original) {
        if (oficio) {
            return original.getFechaVto();
        } else {
            return fechaVtoAjustes;
        }
    }

    /**
     * @return
     */
    public Date getFechaVtoAjuste() {
        if (oficio) {
            return getPeriodo().getFechaVto();
        } else {
            return fechaVtoAjustes;
        }
    }

    public BigDecimal getNuevaValuacion() {
        return nuevaValuacion;
    }

    public static class Builder {
        private Integer anio;
        private Cuenta cuenta;
        private Linea linea;
        private Integer codigoAdministrativo;
        private Date fechaVtoAjustes;
        private Boolean oficio = Boolean.FALSE;

        public Builder() {
        }

        public Builder setAnio(Integer anio) {
            this.anio = anio;
            return this;
        }

        public Builder setCuenta(Cuenta cuenta) {
            this.cuenta = cuenta;
            return this;
        }

        public Builder setLinea(Linea linea) {
            this.linea = linea;
            return this;
        }

        public Builder setFechaVtoAjustes(Date fecha) {
            this.fechaVtoAjustes = fecha;
            return this;
        }

        public Builder setCodAdministrativo(Integer cod) {
            this.codigoAdministrativo = cod;
            return this;
        }

        public Builder setOficio(Boolean oficio) {
            this.oficio = oficio;
            return this;
        }

        public AjusteDTO build() {
            AjusteDTO dto = new AjusteDTO();
            dto.anio = anio;
            dto.cuenta = cuenta;
            // dto.categoria = cuenta.getParcela().getLineaVigente().getCategoria();
            dto.nuevaValuacion = linea.getValuacion();
            dto.categoria = linea.getCategoria();
            dto.codigoAdministrativo = codigoAdministrativo;
            dto.fechaVtoAjustes = fechaVtoAjustes;
            dto.oficio = oficio;
            return dto;
        }
    }

    @Override
    public String toString() {
        return "AjusteDTO [cuenta=" + cuenta + ", categoria=" + categoria + ", nuevaValuacion=" + nuevaValuacion
                + ", codigoAdministrativo=" + codigoAdministrativo + ", fechaVtoAjustes=" + fechaVtoAjustes
                + ", montoCuotaNueva=" + montoCuotaNueva + ", montoCuotaDoce=" + montoCuotaDoce + ", diferencial="
                + diferencial + ", oficio=" + oficio + "]";
    }

    public Integer getAnio() {
        return anio;
    }

    public Boolean getOficio() {
        return oficio;
    }

    public void setOficio(Boolean oficio) {
        this.oficio = oficio;
    }

    public Periodo getPeriodo() {
        return periodo;
    }

    public void setPeriodo(Periodo periodo) {
        this.periodo = periodo;
    }

    public List<CuentaCorriente> getMovimientos() {
        return movimientos;
    }

    public void setMovimientos(List<CuentaCorriente> movimientos) {
        this.movimientos = movimientos;
    }

    /**
     * Calcular el subtotal de los movimientos sumando y restando debitos y creditos
     * 
     * @param ccs
     * @return
     */
    public BigDecimal sumarizarMovimientosAjuste(List<CuentaCorriente> ccs) {
        BigDecimal subtotal = BigDecimal.ZERO;

        // Contabilizamos solo los movimientos de cuota pura y ajustes (C o D)
        for (CuentaCorriente cc : ccs) {
            if ((cc.esCuotaBase() || cc.getFlagAjuste()) && !cc.esMoratoria()) {
                if (cc.getTipo().isDebito()) {
                    subtotal = subtotal.add(cc.getMontoTotal());
                } else {
                    subtotal = subtotal.subtract(cc.getMontoTotal());
                }
            }
        }
        return subtotal;
    }
}
