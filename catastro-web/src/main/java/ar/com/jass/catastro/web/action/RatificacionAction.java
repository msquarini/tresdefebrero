package ar.com.jass.catastro.web.action;

import java.util.ArrayList;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.Iterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;


import ar.com.jass.catastro.business.BusinessHelperRatificacion;
import ar.com.jass.catastro.business.CatastroService;
import ar.com.jass.catastro.business.RatificacionService;
import ar.com.jass.catastro.exceptions.BusinessException;
import ar.com.jass.catastro.model.Cuenta;
import ar.com.jass.catastro.model.CuentaRatificacion;
import ar.com.jass.catastro.model.DomicilioRatificacion;
import ar.com.jass.catastro.model.Parcela;
import ar.com.jass.catastro.model.ParcelaRatificacion;
import ar.com.jass.catastro.model.ResponsableCuenta;
import ar.com.jass.catastro.model.ResponsableCuentaRatificacion;
import ar.com.jass.catastro.model.TitularParcela;
import ar.com.jass.catastro.model.TitularParcelaRatificacion;
import ar.com.jass.catastro.model.Tramite;
import ar.com.jass.catastro.web.commons.WebConstants;
import ar.com.jass.catastro.web.commons.WebConstantsRatificacion;


/**
 * Action de gestion de Ratificacion de parcela
 * 
 * @author msquarini
 *
 */
@SuppressWarnings("rawtypes")
public class RatificacionAction extends GridBaseAction {

    private static final long serialVersionUID = 7353477345330099548L;

    private static final Logger LOG = LoggerFactory.getLogger(RatificacionAction.class);

    @Autowired
    private CatastroService catastroService;
    
    @Autowired
    private RatificacionService ratificacionService;

	@Autowired
    private BusinessHelperRatificacion businessHelperRatificacion;

    /**
     * Indentificador de parcela para edicion
     */
    private Integer partida;
    
    /**
     * Indentificadores de parcela para ratificar
     */
    private List<Long> idsParcela;
    private Long idParcelaRatificacionTORemove;
    private Long idTitularToRatificacionUpdate;
    private Long idResponsableToRatificacionUpdate;

    
    /**
     * Lista de parcelas a remover
     */
    private List<ParcelaRatificacion> parcelasaRemover;
    
    /**
     * Lista de parcelas a crear
     */
    
    private List<ParcelaRatificacion> parcelasNew;
    
    
    private ParcelaRatificacion parcelaNueva;
    
    
    private ParcelaRatificacion parcelaEdit; 
    
    /**
     * Identificar de cuenta origen a ratificar
     */
    private Long idCuentaOrigen;

    /**
     * Cuenta origen
     */
    private Cuenta cuentaOrigen;
    
    private CuentaRatificacion cuentaRatificacionOrigen;

    private Integer tipoRatificacion;

    private Tramite tramite;
    
    private List<String> unidades_funcionales_origen;
    
    
    /**
     * Si permite el borrado en la grilla de cuentas origen
     */
    public Boolean canDelete = Boolean.TRUE;

    
    
    private TitularParcelaRatificacion titularParcela;
    private ResponsableCuentaRatificacion responsableCuenta;
    
    private String uuidTP;


    public String process() {
        try {
            tramite = (Tramite) getFromSession(WebConstants.TRAMITE);
            if (tramite == null) {
                throw new IllegalStateException();
            }

            
            ratificacionService.procesarTramiteRatificacion(tramite);

            LOG.info("Procesando Ratificacion Tramite: {}", tramite);

        }  catch (Exception e) {
            LOG.error("error en procesamiento de ratificacion", e);
            addActionError(getText("error.unknown"));
            return "process_error";
        }
        return "success";
    }

    


    

    public String input() {
        LOG.info("Ingreso a ratificacion de parcela");
        
        // Parcelas a ratificar
        setInSession(WebConstantsRatificacion.RATIFICACION_LIST_PARCELAS, new ArrayList<ParcelaRatificacion>());
        
     // Parcelas a ratificar
        setInSession(WebConstantsRatificacion.RATIFICACION_LIST_PARCELAS_NEW, new ArrayList<ParcelaRatificacion>());


        // Parcelas a ratificar
        setInSession(WebConstantsRatificacion.RATIFICACION_LIST_PARCELAS_REMOVE, new ArrayList<ParcelaRatificacion>());
        
        initCustomData();
        return "input";
    }
    
    /**
     * Llamada AJAX para obtener la informacion para la grilla en formato JSON
     * 
     * @return
     */
    @SuppressWarnings("unchecked")
	public String getData() {
        LOG.info("getData Parcelas Ratificacion");

        try {
            List<ParcelaRatificacion> data = (List<ParcelaRatificacion>) getFromSession(WebConstantsRatificacion.RATIFICACION_LIST_PARCELAS);
            
            setData(data);
        } catch (Exception e) {
            LOG.error("error en getData parcelas", e);
        }
        return "dataJSON";
    }
    
    /**
     * Llamada AJAX para obtener la informacion para la grilla en formato JSON
     * 
     * @return
     */
    @SuppressWarnings("unchecked")
	public String getDataNew() {
        LOG.info("getData Parcelas Ratificacion");

        try {
            List<ParcelaRatificacion> data = (List<ParcelaRatificacion>) getFromSession(WebConstantsRatificacion.RATIFICACION_LIST_PARCELAS_NEW);
            
            setData(data);
        } catch (Exception e) {
            LOG.error("error en getData parcelas", e);
        }
        return "dataJSON";
    }    
    
    /**
     * Llamada AJAX para obtener la informacion para la grilla en formato JSON
     * 
     * @return
     */
    @SuppressWarnings("unchecked")
	public String getDataRemove() {
        LOG.info("getData Parcelas Ratificacion");

        try {
            List<ParcelaRatificacion> data = (List<ParcelaRatificacion>) getFromSession(WebConstantsRatificacion.RATIFICACION_LIST_PARCELAS_REMOVE);
            setData(data);
        } catch (Exception e) {
            LOG.error("error en getData parcelas remove", e);
        }
        return "dataJSON";
    }    
    

    /*pasa del resultado de la busqueda a la primer grilla*/
    public String init() {
        LOG.info("Inicio ratificacion Cuenta ID: {}", idCuentaOrigen);
        

        // Busqueda de cuenta origen a partir de ID
        cuentaOrigen = getConsultaService().findCuentaById(idCuentaOrigen, "responsables", "parcela.titulares",
                "parcela.domicilioInmueble");
        

        if (cuentaOrigen == null) {
            LOG.warn("No se encontro cuenta ID: {}", idCuentaOrigen);
            addActionError(getText("error.cuenta.notfound"));
            return "input";
        }

        if (!cuentaOrigen.getActiva()) {
            LOG.warn("Cuenta no activa: {}", cuentaOrigen);
            addActionError(getText("error.cuenta.inactiva"));
            return "errorJSON";
        }
        if (cuentaOrigen.isBloqueada()) {
            LOG.warn("Cuenta bloqueada: {}", cuentaOrigen);
            addActionError(getText("error.cuenta.bloqueada"));
            return "errorJSON";
        }
        
        if (!cuentaOrigen.getParcela().getNomenclatura().isUF()) {
            LOG.warn("Parcela no es UF: {}", cuentaOrigen);
            addActionError(getText("error.ratificacion.uf"));
            return "errorJSON";
        }

       
        // Cuenta para ratificar
        setInSession(WebConstants.CUENTA_ORIGEN, cuentaOrigen);
        
        
        // Titulares en session para completar CUITCUIL
        setInSession(WebConstantsRatificacion.TITULARES_RATIFICACION_ADD, new HashSet<TitularParcelaRatificacion>());
        setInSession(WebConstantsRatificacion.RESPONSABLES_RATIFICACION_ADD, new HashSet<ResponsableCuentaRatificacion>());

        
        setInSession(WebConstants.TITULARES_ADD, new HashSet<TitularParcela>());
        setInSession(WebConstants.RESPONSABLES_ADD, new HashSet<ResponsableCuenta>());
        /*
         * 20180220 Prueba CREAMOS el tramite y lo mantemos en sesion
         */

        try {
        	  tramite = ratificacionService.crearTramiteRatificacion(cuentaOrigen);
        	  
        	  
            
        } catch (Exception e) {
            LOG.error("", e);
            return "process_error";
        }
        setInSession(WebConstants.TRAMITE, tramite);
        
        List<ParcelaRatificacion> parcelas = new ArrayList<ParcelaRatificacion>();
        try {
            // Valida datos

            parcelas = ratificacionService.findParcelasRatificacion(cuentaOrigen.getParcela().getNomenclatura(),"nomenclatura","cuenta");
  
            setInSession(WebConstantsRatificacion.RATIFICACION_GRID_DATA, parcelas);
            
            setInSession(WebConstantsRatificacion.RATIFICACION_LIST_PARCELAS, parcelas);
            
            return "first-grid";
        } catch (JpaObjectRetrievalFailureException e) {
            setInSession(WebConstantsRatificacion.RATIFICACION_GRID_DATA, parcelas);
            // LOG.warn("Datos no encontrados en busqueda de cuentas/parcelas",
            // e.getCause());
            LOG.warn("Datos no encontrados en busqueda de cuentas/parcelas", e.getMessage());
            return "first-grid";
        } catch (Exception e) {
            LOG.error("Error en busqueda de cuentas/parcelas", e);
            addActionError("Error en proceso de búsqueda, intente nuevamente.");
            return "errorJSON";
        }
        
    }
    
	public String initFromTramite() {
        tramite = (Tramite) getFromSession(WebConstants.TRAMITE);
        cuentaOrigen = tramite.getCuentasOrigen().get(0);
        
        // Titulares en session para completar CUITCUIL
        setInSession(WebConstantsRatificacion.TITULARES_RATIFICACION_ADD, new HashSet<TitularParcelaRatificacion>());
        setInSession(WebConstantsRatificacion.RESPONSABLES_RATIFICACION_ADD, new HashSet<ResponsableCuentaRatificacion>());

        
        setInSession(WebConstants.TITULARES_ADD, new HashSet<TitularParcela>());
        setInSession(WebConstants.RESPONSABLES_ADD, new HashSet<ResponsableCuenta>());

        setInSession(WebConstants.CUENTA_ORIGEN, cuentaOrigen);


        List<ParcelaRatificacion> parcelas = new ArrayList<ParcelaRatificacion>();
        try {
            // Valida datos

            parcelas = ratificacionService.findParcelasRatificacion(cuentaOrigen.getParcela().getNomenclatura());
  
            setInSession(WebConstantsRatificacion.RATIFICACION_GRID_DATA, parcelas);
            
            setInSession(WebConstantsRatificacion.RATIFICACION_LIST_PARCELAS, parcelas);
            
            parcelasaRemover = ratificacionService.findParcelasRatificacionRemove(cuentaOrigen.getParcela().getNomenclatura());
            
            setInSession(WebConstantsRatificacion.RATIFICACION_LIST_PARCELAS_REMOVE, parcelasaRemover);
            
            parcelasNew = ratificacionService.findParcelasRatificacionNew(cuentaOrigen.getParcela().getNomenclatura());
            
            setInSession(WebConstantsRatificacion.RATIFICACION_LIST_PARCELAS_NEW, parcelasNew);
            
            return "first-grid";
        } catch (JpaObjectRetrievalFailureException e) {
            setInSession(WebConstantsRatificacion.RATIFICACION_GRID_DATA, parcelas);
            // LOG.warn("Datos no encontrados en busqueda de cuentas/parcelas",
            // e.getCause());
            LOG.warn("Datos no encontrados en busqueda de cuentas/parcelas", e.getMessage());
            return "first-grid";
        } catch (Exception e) {
            LOG.error("Error en busqueda de cuentas/parcelas", e);
            addActionError("Error en proceso de búsqueda, intente nuevamente.");
            return "errorJSON";
        }
        
    }
    
    
    /*Vuelve a la grilla de unidades funcionales a eliminar, desde la grilla de unidades funcionales a actualizar*/

	@SuppressWarnings("unchecked")
	public String backToRemove()
    {
        
        List<ParcelaRatificacion> parcelasRemove = (List<ParcelaRatificacion>) getFromSession(WebConstantsRatificacion.RATIFICACION_LIST_PARCELAS_REMOVE);
        
        List<ParcelaRatificacion> parcelasRatificacion = (List<ParcelaRatificacion>) getFromSession(WebConstantsRatificacion.RATIFICACION_GRID_DATA);
        
        parcelasRatificacion.addAll(parcelasRemove);
        
        setInSession(WebConstantsRatificacion.RATIFICACION_GRID_DATA, parcelasRatificacion);
    	
    	return "first-grid";
    }
    
	/*Vuelve a la grilla de unidades funcionales a actualizar, desde el formulario final*/
	
    public String backToNew()
    {
    	return init_update();
    }
    
    public String backToUpdate()
    {
    	return init_remove();
    }
    
    /*Pasa de la primer grilla a la segunda*/
    @SuppressWarnings("unchecked")
	public String init_remove() {
    	LOG.info("segundo paso en proceso de ratificacion.");
        canDelete = null;
        
        cuentaOrigen = (Cuenta)getFromSession(WebConstants.CUENTA_ORIGEN);
        
        List<ParcelaRatificacion> parcelasRemove = (List<ParcelaRatificacion>) getFromSession(WebConstantsRatificacion.RATIFICACION_LIST_PARCELAS_REMOVE);
        
        //Recargo la lista de parcelas ratificacion para este ph
        
        List<ParcelaRatificacion> parcelasRatificacion = ratificacionService.findParcelasRatificacion(cuentaOrigen.getParcela().getNomenclatura());
        
        parcelasRatificacion.removeAll(parcelasRemove);
        
        
        setInSession(WebConstantsRatificacion.RATIFICACION_LIST_PARCELAS, parcelasRatificacion);
        
        setInSession(WebConstantsRatificacion.RATIFICACION_GRID_DATA, parcelasRatificacion);
        
        return "second-grid";
    }
    
    @SuppressWarnings("unchecked")
	public String saveuf()
    {
    	LOG.info("Salvando cambios sobre UF");
    	
    	cuentaRatificacionOrigen = (CuentaRatificacion)getFromSession(WebConstantsRatificacion.CUENTA_RATIFICACION_ORIGEN);
        
        ParcelaRatificacion p = (ParcelaRatificacion)getFromSession(WebConstantsRatificacion.RATIFICACION_PARCELA_UPDATE);
        
        List<TitularParcelaRatificacion> titularesRatificacionAdd =  new ArrayList<TitularParcelaRatificacion>((Set<TitularParcelaRatificacion>)getFromSession(WebConstantsRatificacion.TITULARES_RATIFICACION_ADD));
        List<ResponsableCuentaRatificacion> responsablesRatificacionAdd =  new ArrayList<ResponsableCuentaRatificacion>((Set<ResponsableCuentaRatificacion>)getFromSession(WebConstantsRatificacion.RESPONSABLES_RATIFICACION_ADD));
        
        try {
			businessHelperRatificacion.checkTitularesConCuit(titularesRatificacionAdd);
		} catch (BusinessException be) {
            LOG.error("error en el chequeo de titulares", be);
            addActionError(getText(be.getMessage()));
            return "errorJSON";
            
        } catch (Exception e) {
            LOG.error("error en la actualizacion de la uf", e);
            addActionError(getText("error.unknown"));
            return "process_error";
        }
        
        List<Parcela> parelasOrigen = new ArrayList<Parcela>();
        
        for (String id :  getUnidades_funcionales_origen()) {

        	Parcela pa = getConsultaService().findParcelaById(Long.valueOf(id));

        	parelasOrigen.add(pa);
        }
    	
    	ratificacionService.updateParcelaRatificacion(p.getId(),cuentaRatificacionOrigen.getId(),getParcelaEdit(),titularesRatificacionAdd,responsablesRatificacionAdd,parelasOrigen);
    	
    	return init_remove();
    }
    
    
    @SuppressWarnings("unchecked")
	public String savenewuf()
    {
    	LOG.info("Salvando nueva UF");
    	
    	// Verificar que la nomenclatura no existe en la BD
        if (getConsultaService().exists(getParcelaNueva())) {
            LOG.warn("Nomenclatura existente en BD, {}", getParcelaNueva());
            addActionError(getText("nomenclatura.existente"));
            return "errorJSON";
        }
        

        
    	// Verificar que la unidad funcional no exista ya para este ph
        if (ratificacionService.existUF(getParcelaNueva().getNomenclatura())) {
            LOG.warn("Unidad funcional existente en el PH, {}", getParcelaNueva().getNomenclatura().getUnidadFuncional());
            addActionError(getText("ratificacion.ufexistente"));
            return "errorJSON";
        }
        
        
    	
        tramite = (Tramite) getFromSession(WebConstants.TRAMITE);
        
        List<Parcela> parelasOrigen = new ArrayList<Parcela>();
        
        for (String id :  getUnidades_funcionales_origen()) {

        	Parcela p = getConsultaService().findParcelaById(Long.valueOf(id));

        	parelasOrigen.add(p);
        }
        
   			
    	//Persistir
        
        List<TitularParcelaRatificacion> titularesRatificacionAdd =  new ArrayList<TitularParcelaRatificacion>((Set<TitularParcelaRatificacion>)getFromSession(WebConstantsRatificacion.TITULARES_RATIFICACION_ADD));
        List<ResponsableCuentaRatificacion> responsablesRatificacionAdd =  new ArrayList<ResponsableCuentaRatificacion>((Set<ResponsableCuentaRatificacion>)getFromSession(WebConstantsRatificacion.RESPONSABLES_RATIFICACION_ADD));
    	
        
        try {
			businessHelperRatificacion.checkTitularesConCuit(titularesRatificacionAdd);
			
		} catch (BusinessException e) {
			LOG.error("error en el chequeo de titulares", e);
            addActionError(getText(e.getMessage()));
		}
        
        ratificacionService.saveParcelaRatificacion(getParcelaNueva(), titularesRatificacionAdd, responsablesRatificacionAdd, parelasOrigen, tramite);
        
        parcelasNew = ratificacionService.findParcelasRatificacionNew(getParcelaNueva().getNomenclatura());
        
        setInSession(WebConstantsRatificacion.RATIFICACION_LIST_PARCELAS_NEW, parcelasNew);
        
    	return "final-interface";
    }
    

	@SuppressWarnings("unchecked")
	public String init_update() {
    	LOG.info("tercer paso en proceso de ratificacion.");
    	
    	tramite = (Tramite) getFromSession(WebConstants.TRAMITE);
    	
    	parcelasNew = (List<ParcelaRatificacion>) getFromSession(WebConstantsRatificacion.RATIFICACION_LIST_PARCELAS_NEW);
        
        setInSession(WebConstantsRatificacion.RATIFICACION_GRID_DATA, parcelasNew);
        
        return "final-interface";
    }
    
    
    @SuppressWarnings("unchecked")
	public String addParcelaTORatificacionRemove() {
        LOG.info("Agregar parcela a proceso de ratificacion, {}", idsParcela);

        parcelasaRemover = (List<ParcelaRatificacion>) getFromSession(WebConstantsRatificacion.RATIFICACION_LIST_PARCELAS_REMOVE);

        // Recorro la lista de ids a unificar
        for (Long id : idsParcela) {
            ParcelaRatificacion p = ratificacionService.findParcelaRatificacionById(id, "titulares", "domicilioInmueble",
                    "cuenta.responsables");

            if (!p.getCuenta().getActiva()) {
                LOG.warn("Cuenta no activa: {}", p.getCuenta());
                addActionError(getText("error.cuenta.inactiva"));
                return "errorJSON";
            }
            
            // Si no existe en la lista la agrego
            
            //APORTE MIO
            //Controlar que no se ingrese la misma parcela-unidad funcional en la lista de parcelas a ratificar
            
            
            if (!checkUFexistente(parcelasaRemover, p.getNomenclatura().getUnidadFuncional())) {

            	parcelasaRemover.add(p);
            	
            	p.setDeleted(Boolean.TRUE);
            	
            	
            	ratificacionService.updateParcelaRatificacion(p);
            	
            } else {
                LOG.warn("Parcela ya en la lista de parcelas a eliminar");
                addActionError(getText("error.ratificacion.existe"));
                return "errorJSON";
            }
        }
        setInSession(WebConstantsRatificacion.RATIFICACION_LIST_PARCELAS_REMOVE, parcelasaRemover);

        return "addresponse";
    }
    
    
    
    
    @SuppressWarnings("unchecked")
	public String removeParcelaTORatificacionRemove() {

        LOG.info("Eliminar unidad funncional de la lista de unidades funcionales a eliminar, {}", idParcelaRatificacionTORemove);

        parcelasaRemover = (List<ParcelaRatificacion>) getFromSession(WebConstantsRatificacion.RATIFICACION_LIST_PARCELAS_REMOVE);
        ParcelaRatificacion toRemove = null;
        for (ParcelaRatificacion p : parcelasaRemover) {
            if (p.getId().equals(idParcelaRatificacionTORemove)) {
                toRemove = p;
                break;
            }
        }
        
    	List<ParcelaRatificacion> parcelas = new ArrayList<ParcelaRatificacion>();
        
        parcelas = ratificacionService.findParcelasRatificacion(toRemove.getNomenclatura(),"parcelasOrigen");
        
        ParcelaRatificacion toCheck = ratificacionService.findParcelaRatificacionById(toRemove.getId(), "origen");
        
        
        if(toCheck.hasOrigin())
        {
            try {
            	
    			businessHelperRatificacion.checkParcelaEnOrigenes(toCheck, parcelas);
    			
    		} catch (BusinessException e) {
    			LOG.error("error en el chequeo de la parcela, la misma se encuentra como origen de otra parcela", e);
                addActionError(getText(e.getMessage()));
                return "errorJSON";
                
    		}
        	
        }

        
        
        parcelasaRemover.remove(toRemove);
        
        toRemove.setDeleted(Boolean.FALSE);
        
        ratificacionService.updateParcelaRatificacion(toRemove);

        // Si no parcelas seleccionadas --> blanqueo la lista de titulares
        if (parcelasaRemover.isEmpty()) {
            setInSession(WebConstants.TITULARES_ADD, null);
        }
        setInSession(WebConstantsRatificacion.RATIFICACION_LIST_PARCELAS_REMOVE, parcelasaRemover);

        return "addresponse";
    }   


    @SuppressWarnings("unchecked")
	public String removeTitularToRatificacionUpdate() {
    	
        Set<TitularParcelaRatificacion> titularesAdd = (Set<TitularParcelaRatificacion>) getFromSession(WebConstantsRatificacion.TITULARES_RATIFICACION_ADD);
        UUID uuid = UUID.fromString(getUuidTP());
        for (TitularParcelaRatificacion tp : titularesAdd) {
            if (uuid.equals(tp.getUuid())) {
                setTitularParcela(tp);
                break;
            }
        }
        titularesAdd.remove(titularParcela);
        //setInSession(WebConstantsRatificacion.TITULARES_RATIFICACION_ADD, titularesAdd);
        return "addresponse";
    }
    
    
    
    @SuppressWarnings("unchecked")
	public String removeResponsableToRatificacionUpdate() {
        Set<ResponsableCuentaRatificacion> responsablesAdd = (Set<ResponsableCuentaRatificacion>) getFromSession(WebConstantsRatificacion.RESPONSABLES_RATIFICACION_ADD);
        UUID uuid = UUID.fromString(getUuidTP());
        for (ResponsableCuentaRatificacion rc : responsablesAdd) {
            if (uuid.equals(rc.getUuid())) {
                setResponsableCuenta(rc);
                break;
            }
        }
        responsablesAdd.remove(responsableCuenta);
        //setInSession(WebConstantsRatificacion.RESPONSABLES_RATIFICACION_ADD, responsablesAdd);
        return "addresponse";
    }
    
    
    


	@SuppressWarnings("unchecked")
	public String addParcelaTORatificacionUpdate() {
	
        // Busqueda de cuenta origen a partir de ID
        cuentaRatificacionOrigen = ratificacionService.findCuentaRatificacionById(idCuentaOrigen, "responsables", "parcela.titulares",
                "parcela.domicilioInmueble", "parcela.lineaVigente", "parcela.dominio", "parcela.observacion");
        
        parcelasaRemover = (List<ParcelaRatificacion>)getFromSession(WebConstantsRatificacion.RATIFICACION_LIST_PARCELAS_REMOVE);
        
        
        
        
        parcelaEdit = ratificacionService.findParcelaRatificacionById(cuentaRatificacionOrigen.getParcela().getId(), "nomenclatura", "titulares",
                "domicilioInmueble", "lineaVigente", "dominio", "observacion","parcelasOrigen");
         

       
        
		LOG.info("Cargar parcela para actualizar en proceso de ratificacion, {}", idsParcela);
        
		//Filtrar los titulares y dejar solo los de la parcela seleccionada
    
		setInSession(WebConstantsRatificacion.CUENTA_RATIFICACION_ORIGEN, cuentaRatificacionOrigen);
        setInSession(WebConstantsRatificacion.RATIFICACION_PARCELA_UPDATE, parcelaEdit);
        
        Set<TitularParcelaRatificacion> titulares = new HashSet<TitularParcelaRatificacion>();
        titulares.addAll(parcelaEdit.getTitulares());
        
        Set<ResponsableCuentaRatificacion> reponsables = new HashSet<ResponsableCuentaRatificacion>();
        reponsables.addAll(cuentaRatificacionOrigen.getResponsables());
        
        setInSession(WebConstantsRatificacion.TITULARES_RATIFICACION_ADD, titulares);
        
        setInSession(WebConstantsRatificacion.RESPONSABLES_RATIFICACION_ADD, reponsables);
        
        return "updateUF";
    }
	
	
	
	@SuppressWarnings("unchecked")
	public String addNewUF()
	{
		parcelaNueva = new ParcelaRatificacion();	
		cuentaOrigen = (Cuenta) getFromSession(WebConstants.CUENTA_ORIGEN);		
		parcelaNueva.setNomenclatura(cuentaOrigen.getParcela().getNomenclatura());	
		parcelaNueva.getNomenclatura().setUnidadFuncional("");	
		parcelaNueva.setDomicilioInmueble(new DomicilioRatificacion(cuentaOrigen.getParcela().getDomicilioInmueble()));        
        setInSession(WebConstantsRatificacion.PARCELA_RATIFICACION_NEW, parcelaNueva);        
        parcelasaRemover = (List<ParcelaRatificacion>)getFromSession(WebConstantsRatificacion.RATIFICACION_LIST_PARCELAS_REMOVE);
        
        /*Resetear las colecciones de titulares y responsables*/        
        setInSession(WebConstantsRatificacion.TITULARES_RATIFICACION_ADD, new HashSet<TitularParcelaRatificacion>());        
        setInSession(WebConstantsRatificacion.RESPONSABLES_RATIFICACION_ADD, new HashSet<ResponsableCuentaRatificacion>());

        return "newUF";
	}
    
    /*Chequea si una unidad funcional se encuentra en una lista de parcelas*/
    private boolean checkUFexistente(List<ParcelaRatificacion> list, String uf)
    {
    	Iterator<ParcelaRatificacion> it = list.iterator();
    	while(it.hasNext())
    	{
    		ParcelaRatificacion p = it.next();    		
    		if(p.getNomenclatura().getUnidadFuncional().equals(uf))
    			return true;
    	}
    	return false;
    }

    public Long getIdResponsableToRatificacionUpdate() {
		return idResponsableToRatificacionUpdate;
	}

	public void setIdResponsableToRatificacionUpdate(Long idResponsableToRatificacionUpdate) {
		this.idResponsableToRatificacionUpdate = idResponsableToRatificacionUpdate;
	}


    public Long getIdTitularToRatificacionUpdate() {
		return idTitularToRatificacionUpdate;
	}

	public void setIdTitularToRatificacionUpdate(Long idTitularToRatificacionUpdate) {
		this.idTitularToRatificacionUpdate = idTitularToRatificacionUpdate;
	}


    public CatastroService getCatastroService() {
        return catastroService;
    }

    public void setCatastroService(CatastroService catastroService) {
        this.catastroService = catastroService;
    }

	public Integer getPartida() {
        return partida;
    }

    public void setPartida(Integer partida) {
        this.partida = partida;
    }

    public Long getIdParcelaRatificacionTORemove() {
		return idParcelaRatificacionTORemove;
	}


	public void setIdParcelaRatificacionTORemove(Long idParcelaRatificacionTORemove) {
		this.idParcelaRatificacionTORemove = idParcelaRatificacionTORemove;
	}


	public static Logger getLog() {
        return LOG;
    }

    public Long getIdCuentaOrigen() {
        return idCuentaOrigen;
    }

    public void setIdCuentaOrigen(Long idCuentaOrigen) {
        this.idCuentaOrigen = idCuentaOrigen;
    }

    public Cuenta getCuentaOrigen() {
        return cuentaOrigen;
    }

    public void setCuentaOrigen(Cuenta cuentaOrigen) {
        this.cuentaOrigen = cuentaOrigen;
    }
    
    public CuentaRatificacion getCuentaRatificacionOrigen() {
        return cuentaRatificacionOrigen;
    }

    public void setCuentaRatificacionOrigen(CuentaRatificacion cuentaRatificacionOrigen) {
        this.cuentaRatificacionOrigen = cuentaRatificacionOrigen;
    }
    public List<Long> getIdsParcela() {
		return idsParcela;
	}


	public List<ParcelaRatificacion> getParcelasaRemover() {
		return parcelasaRemover;
	}


	public void setParcelasaRemover(List<ParcelaRatificacion> parcelasARemover) {
		this.parcelasaRemover = parcelasARemover;
	}


	public Boolean getCanDelete() {
		return canDelete;
	}


	public void setCanDelete(Boolean canDelete) {
		this.canDelete = canDelete;
	}


	public void setIdsParcela(List<Long> idsParcela) {
		this.idsParcela = idsParcela;
	}


	public Integer getTipoRatificacion() {
        return tipoRatificacion;
    }

    public void setTipoRatificacion(Integer tipoRatificacion) {
        this.tipoRatificacion = tipoRatificacion;
    }

    public Tramite getTramite() {
        return tramite;
    }

    public void setTramite(Tramite tramite) {
        this.tramite = tramite;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }


	public ParcelaRatificacion getParcelaNueva() {
		return parcelaNueva;
	}


	public void setParcelaNueva(ParcelaRatificacion parcelaNueva) {
		this.parcelaNueva = parcelaNueva;
	}

    public RatificacionService getRatificacionService() {
		return ratificacionService;
	}

	public void setRatificacionService(RatificacionService ratificacionService) {
		this.ratificacionService = ratificacionService;
	}

	public TitularParcelaRatificacion getTitularParcela() {
		return titularParcela;
	}

	public void setTitularParcela(TitularParcelaRatificacion titularParcela) {
		this.titularParcela = titularParcela;
	}

	public String getUuidTP() {
		return uuidTP;
	}

	public void setUuidTP(String uuidTP) {
		this.uuidTP = uuidTP;
	}

	public ResponsableCuentaRatificacion getResponsableCuenta() {
		return responsableCuenta;
	}

	public void setResponsableCuenta(ResponsableCuentaRatificacion responsableCuenta) {
		this.responsableCuenta = responsableCuenta;
	}

	public ParcelaRatificacion getParcelaEdit() {
		return parcelaEdit;
	}

	public void setParcelaEdit(ParcelaRatificacion parcelaEdit) {
		this.parcelaEdit = parcelaEdit;
	}

	public List<String> getUnidades_funcionales_origen() {
		return (unidades_funcionales_origen!=null)?unidades_funcionales_origen:new ArrayList<String>();
	}

	public void setUnidades_funcionales_origen(List<String> unidades_funcionales_origen) {
		this.unidades_funcionales_origen = unidades_funcionales_origen;
	}

	public List<ParcelaRatificacion> getParcelasNew() {
		return parcelasNew;
	}

	public void setParcelasNew(List<ParcelaRatificacion> parcelasNew) {
		this.parcelasNew = parcelasNew;
	}

}