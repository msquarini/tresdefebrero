package ar.com.jass.catastro.model;

import java.util.List;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;

/**
 * Titular o Responsable de una parcela
 * 
 * Junto con TitularParcela se representa la titularidad en ARBA de la parcela
 * 
 * La relacion con cuenta de Responsable es a fines de pago de deuda, solo con fecha desde y hasta
 * 
 * @author MESquarini
 * 
 */

@Entity
@Table(name = "Titular")
public class Titular {

    /**
     * Identificador unico de titular
     */
    private Long id;

    private String apellido;
    private String nombre;
    private Integer tipoDocumento;
    private Boolean personaJuridica;
    private Integer documento;
    private String cuitcuil;
    private String razonSocial;

    private String mail;
    /**
     * 
     */
    private Domicilio domicilioPostal;

    /**
     * Parcelas de las cuales es titular
     */
    private List<TitularParcela> parcelas;

    private String observacion;

    public Titular() {
        personaJuridica = Boolean.FALSE;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_titular_gen")
    @SequenceGenerator(name = "seq_titular_gen", sequenceName = "seq_titular", allocationSize = 1)
    @Column(name = "id_titular")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "apellido")
    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Column(name = "tipo_doc")
    public Integer getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(Integer tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    @Column(name = "persona_juridica")
    public Boolean getPersonaJuridica() {
        return personaJuridica;
    }

    public void setPersonaJuridica(Boolean personaJuridica) {
        this.personaJuridica = personaJuridica;
    }

    @Column(name = "numero_doc")
    public Integer getDocumento() {
        return documento;
    }

    public void setDocumento(Integer documento) {
        this.documento = documento;
    }

    @Column(name = "cuit")
    public String getCuitcuil() {
        return cuitcuil;
    }

    public void setCuitcuil(String cuitcuil) {
        this.cuitcuil = cuitcuil;
    }

    @Column(name = "razon_social")
    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    /*
     * @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
     * 
     * @JoinTable( name = "titular_parcela", joinColumns = { @JoinColumn(name = "id_titular",
     * nullable = false, updatable = false) }, inverseJoinColumns = { @JoinColumn(name =
     * "id_parcela", nullable = false, updatable = false) })
     */
    @OneToMany(mappedBy = "titular")
    public List<TitularParcela> getParcelas() {
        return parcelas;
    }

    public void setParcelas(List<TitularParcela> parcelas) {
        this.parcelas = parcelas;
    }

    @OneToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "id_domicilio")
    @Cascade({org.hibernate.annotations.CascadeType.ALL})
    public Domicilio getDomicilioPostal() {
        return domicilioPostal;
    }

    public void setDomicilioPostal(Domicilio domicilioPostal) {
        this.domicilioPostal = domicilioPostal;
    }

    @Column(name = "observacion")
    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id, this.cuitcuil);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Titular other = (Titular) obj;
        return Objects.equals(this.id, other.id) && Objects.equals(this.cuitcuil, other.cuitcuil);
    }

    @Override
    public String toString() {
        return "Titular [id=" + id + ", apellido=" + apellido + ", nombre=" + nombre + ", tipoDocumento="
                + tipoDocumento + ", personaJuridica=" + personaJuridica + ", documento=" + documento + ", cuitcuil="
                + cuitcuil + ", razonSocial=" + razonSocial + ", domicilioPostal=" + domicilioPostal + ", observacion="
                + observacion + "]";
    }

    @Column(name = "mail")
    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

}