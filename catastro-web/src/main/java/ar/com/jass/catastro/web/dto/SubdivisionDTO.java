package ar.com.jass.catastro.web.dto;

import java.util.HashSet;
import java.util.Set;

import ar.com.jass.catastro.model.Cuenta;
import ar.com.jass.catastro.model.Parcela;

/**
 * DTO para la operacion de subdivision Cuenta origen a subdividir y las N
 * nuevas parcelas
 * 
 * @author MESquarini
 * 
 */
@Deprecated
public class SubdivisionDTO {

	private Cuenta cuentaOrigen;

	private Set<Parcela> parcelas;

	public SubdivisionDTO(Cuenta origen) {
		this.cuentaOrigen = origen;
		this.parcelas = new HashSet<Parcela>();
	}

	/**
	 * @param p
	 */
	public void addParcela(Parcela p) {
		// validacion de negocio

		this.parcelas.add(p);
	}

	public Cuenta getCuentaOrigen() {
		return cuentaOrigen;
	}

	public void setCuentaOrigen(Cuenta cuentaOrigen) {
		this.cuentaOrigen = cuentaOrigen;
	}

	public Set<Parcela> getParcelas() {
		return parcelas;
	}

	public void setParcelas(Set<Parcela> parcelas) {
		this.parcelas = parcelas;
	}
}
