package ar.com.jass.catastro.model;

import java.util.Date;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 * Titularidad de parcela. Fecha desde y hasta y porcentaje de titularidad
 * 
 * @author MESquarini
 * 
 */

@Entity
@Table(name = "Titular_Parcela_Ratificacion")
public class TitularParcelaRatificacion extends EntityBase implements Editable {

	private UUID tpUUID = UUID.randomUUID();

	private Long id;

	private ParcelaRatificacion parcela;
	private TitularRatificacion titular;

	private Double porcentaje;
	private Date desde;
	private Date hasta;

	public TitularParcelaRatificacion() {
		porcentaje = 0d;
	}

	public TitularParcelaRatificacion(ParcelaRatificacion parcela, TitularRatificacion titular, Double porcentaje,
			Date desde, Date hasta) {
		this.parcela = parcela;
		this.titular = titular;
		this.porcentaje = porcentaje;
		this.desde = desde;
		this.hasta = hasta;
	}

	public TitularParcelaRatificacion(TitularParcela titularParcela) {
		this.parcela = new ParcelaRatificacion(titularParcela.getParcela());
		this.titular = new TitularRatificacion(titularParcela.getTitular());
		this.porcentaje = titularParcela.getPorcentaje();
		this.desde = titularParcela.getDesde();
		this.hasta = titularParcela.getHasta();
	}

	@Transient
	public TitularParcela getTitularParcela() {
		TitularParcela tp = new TitularParcela();
		tp.setPorcentaje(this.getPorcentaje());
		tp.setCreatedBy(this.getCreatedBy());
		tp.setCreatedDate(this.getCreatedDate());
		tp.setDesde(this.getDesde());
		tp.setHasta(this.getHasta());
		tp.setModifiedBy(this.getModifiedBy());
		tp.setModifiedDate(this.getModifiedDate());
		tp.setTitular(this.getTitular().getTitular());
		return tp;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_titularparcela_ratificacion_gen")
	@SequenceGenerator(name = "seq_titularparcela_ratificacion_gen", sequenceName = "seq_titularparcela_ratificacion", allocationSize = 1)
	@Column(name = "id_titularparcela")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne
	@JoinColumn(name = "id_parcela")
	public ParcelaRatificacion getParcela() {
		return parcela;
	}

	public void setParcela(ParcelaRatificacion parcela) {
		this.parcela = parcela;
	}

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id_titular")
	public TitularRatificacion getTitular() {
		return titular;
	}

	public void setTitular(TitularRatificacion titular) {
		this.titular = titular;
	}

	@Column(name = "porcentaje")
	public Double getPorcentaje() {
		return porcentaje;
	}

	public void setPorcentaje(Double porcentaje) {
		this.porcentaje = porcentaje;
	}

	@Column(name = "desde")
	@Temporal(TemporalType.DATE)
	public Date getDesde() {
		return desde;
	}

	public void setDesde(Date desde) {
		this.desde = desde;
	}

	@Column(name = "hasta")
	@Temporal(TemporalType.DATE)
	public Date getHasta() {
		return hasta;
	}

	public void setHasta(Date hasta) {
		this.hasta = hasta;
	}

	/*
	 * @Override public int hashCode() { final int prime = 31; int result = 1;
	 * result = prime * result + ((desde == null) ? 0 : desde.hashCode()); result =
	 * prime * result + ((hasta == null) ? 0 : hasta.hashCode()); result = prime *
	 * result + ((id == null) ? 0 : id.hashCode()); result = prime * result +
	 * ((parcela == null) ? 0 : parcela.hashCode()); result = prime * result +
	 * ((porcentaje == null) ? 0 : porcentaje.hashCode()); result = prime * result +
	 * ((titular == null) ? 0 : titular.hashCode()); return result; }
	 */

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TitularParcelaRatificacion other = (TitularParcelaRatificacion) obj;
		if (desde == null) {
			if (other.desde != null)
				return false;
		} else if (!desde.equals(other.desde))
			return false;
		if (hasta == null) {
			if (other.hasta != null)
				return false;
		} else if (!hasta.equals(other.hasta))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (parcela == null) {
			if (other.parcela != null)
				return false;
		} else if (!parcela.equals(other.parcela))
			return false;
		if (porcentaje == null) {
			if (other.porcentaje != null)
				return false;
		} else if (!porcentaje.equals(other.porcentaje))
			return false;
		if (titular == null) {
			if (other.titular != null)
				return false;
		} else if (!titular.equals(other.titular))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TitularParcelaRatificacion [id=" + id + ", parcela=" + parcela + ", titular=" + titular
				+ ", porcentaje=" + porcentaje + ", desde=" + desde + ", hasta=" + hasta + "]";
	}

	@Override
	@Transient
	public UUID getUuid() {
		return tpUUID;
	}

	@Transient
	public String getUuidString() {
		return tpUUID.toString();
	}

}