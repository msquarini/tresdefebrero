package ar.com.jass.catastro.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

/**
 * @author MESquarini
 * 
 */
@Entity
@Table(name = "Cuenta")
public class Cuenta extends EntityBase implements Serializable {

    private Long id;

    private String cuenta;
    private String dv;

    /**
     * Parcela que representa la cuenta Uso de SET para optimizar la relacion OneToOne
     */
    private Set<Parcela> parcelas;

    /**
     * Estado de la cuenta
     */
    private Boolean activa;

    /**
     * Bloqueada por un tramite. Hasta la finalizacion del mismo permanece en este estado
     */
    private Boolean bloqueada;

    /**
     * Marca de exencion de pago de impuesto
     */
    private Boolean exenta;

    /**
     * Marca que indica que la cuenta presenta tope en el calculo de deuda
     */
    private Boolean conTope;

    /**
     * Tramite que da origen a la cuenta
     */
    private Tramite tramite;

    private List<ResponsableCuenta> responsables;

    public Cuenta() {

    }

    public Cuenta(BigInteger numeroCuenta) {
        /**
         * Numero de cuenta y calculo de DV
         */
        this.cuenta = numeroCuenta.toString();
        this.dv = calculateDV(this.cuenta);

        this.activa = Boolean.FALSE;
        this.exenta = Boolean.FALSE;
        this.bloqueada = Boolean.TRUE;
        this.conTope = Boolean.FALSE;

        this.responsables = new ArrayList<>();
        this.parcelas = new HashSet<Parcela>();
    }

    /**
     * Creo cuenta a partir de una origen.
     * 
     * @param parcela
     */
    public Cuenta(BigInteger numeroCuenta, Parcela parcela) {
        this(numeroCuenta);
        parcela.setCuenta(this);
        addParcela(parcela);
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_cuenta_gen")
    @SequenceGenerator(name = "seq_cuenta_gen", sequenceName = "seq_cuenta", schema = "catastro", allocationSize = 1)
    @Column(name = "id_cuenta")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "cuenta", nullable = false)
    public String getCuenta() {
        return cuenta;
    }

    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }

    @Column(name = "dv")
    public String getDv() {
        return dv;
    }

    public void setDv(String dv) {
        this.dv = dv;
    }

    // @OneToOne(optional = true, mappedBy = "cuenta", fetch = FetchType.EAGER)
    // @OneToOne(mappedBy = "cuenta")
    // @Fetch(FetchMode.JOIN)
    // @org.hibernate.annotations.LazyToOne(org.hibernate.annotations.LazyToOneOption.NO_PROXY)
    // @Cascade(CascadeType.SAVE_UPDATE)
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "cuenta")
    @Cascade(CascadeType.ALL)
    public Set<Parcela> getParcelas() {
        return parcelas;
    }

    public void setParcelas(Set<Parcela> parcelas) {
        this.parcelas = parcelas;
    }

    @javax.persistence.Transient
    public Parcela getParcela() {
        return parcelas.iterator().next();
    }

    /**
     * Agrega la parcela a la cuenta
     * 
     * @param parcela
     */
    private void addParcela(Parcela parcela) {
        parcelas.add(parcela);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id, this.cuenta, this.dv);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Cuenta other = (Cuenta) obj;
        return Objects.equals(this.cuenta, other.cuenta) && Objects.equals(this.dv, other.dv)
                && Objects.equals(this.id, other.id);
    }

    @Column(name = "activa")
    public Boolean getActiva() {
        return activa;
    }

    public void setActiva(Boolean activa) {
        this.activa = activa;
    }

    /**
     * Calculo de DV
     * 
     * @param cuenta
     * @return
     */
    public static String calculateDV(String cuenta) {
        Integer total = 0;
        for (int i = 1; i <= cuenta.length(); i++) {
            total = total + Character.getNumericValue(cuenta.charAt(i - 1)) * i;
        }
        total = 11 - (total % 11);

        if (total == 11) {
            return "-";
        } else if (total == 10) {
            return "0";
        } else {
            return String.valueOf(total);
        }
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_tramite")
    public Tramite getTramite() {
        return tramite;
    }

    public void setTramite(Tramite tramite) {
        this.tramite = tramite;
    }

    @Transient
    public void desactivar() {
        setActiva(Boolean.FALSE);
        // getParcela().setActiva(Boolean.FALSE);
    }

    @Transient
    public BigDecimal getValuacion() {
        return getParcela().getLineaVigente().getValuacion();
    }

    @Column(name = "exenta")
    public Boolean isExenta() {
        return exenta;
    }

    public void setExenta(Boolean exenta) {
        this.exenta = exenta;
    }

    @OneToMany(mappedBy = "cuenta")
    @Cascade(CascadeType.ALL)
    public List<ResponsableCuenta> getResponsables() {
        return responsables;
    }

    public void setResponsables(List<ResponsableCuenta> responsables) {
        this.responsables = responsables;
    }

    @Column(name = "bloqueada")
    public Boolean isBloqueada() {
        return bloqueada;
    }

    public void setBloqueada(Boolean bloqueada) {
        this.bloqueada = bloqueada;
    }

    @Override
    public String toString() {
        return "Cuenta [id=" + id + ", cuenta=" + cuenta + ", dv=" + dv + ", activa=" + activa + ", bloqueada="
                + bloqueada + ", exenta=" + exenta + "]";
    }

    @Column(name = "con_tope")
    public Boolean getConTope() {
        return conTope;
    }

    public void setConTope(Boolean conTope) {
        this.conTope = conTope;
    }

}