package ar.com.jass.catastro.business;

import java.util.List;

import ar.com.jass.catastro.model.Cuenta;
import ar.com.jass.catastro.model.cuentacorriente.Boleta;
import ar.com.jass.catastro.model.cuentacorriente.BoletaPeriodoStatus;
import ar.com.jass.catastro.model.cuentacorriente.Periodo;

/**
 * Generar boletas de pago
 * 
 * @author msquarini
 *
 */
public interface BoletaService {

    public Periodo cambioEstadoPeriodoGeneracionBoleta(Long idPeriodo, BoletaPeriodoStatus estado);

    /**
     * Lanzar proceso batch de generacion de boletas para una lista de periodos
     * 
     * @param periodos
     */
    public void generacionBoletasBatch(List<Periodo> periodos);

    /**
     * @param cuenta
     * @param periodo
     * @param aplicaInteres
     * @param cuotaAnual
     * @param vencidas
     * @return
     */
    public List<Boleta> generarBoleta(Cuenta cuenta, Periodo periodo, Boolean aplicaInteres, Boolean cuotaAnual,
            Boolean vencidas);
}
