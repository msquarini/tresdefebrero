package ar.com.jass.catastro.business.impl;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.com.jass.catastro.business.TasaService;
import ar.com.jass.catastro.dao.CuentaCorrienteDAO;
import ar.com.jass.catastro.model.cuentacorriente.TasaCategoria;

@Service
public class TasaServiceImpl implements TasaService {

    private static final Logger LOG = LoggerFactory.getLogger("TasaServiceImpl");
    
    /**
     * Tasa de interes mensual para deuda vencida
     */
    public static final double TASA_INTERES_MENSUAL = 0.01;
    
    @Autowired
    private CuentaCorrienteDAO ccDAO;
    
    private Map<Integer, TasaCategoria> tasaPorCategoria;
    
    @PostConstruct
    public void initialize() {
        tasaPorCategoria = new HashMap<Integer, TasaCategoria>();
        reload();
    }
        
    public void reload() {
        LOG.info("Reloading tasas por categoria...");
        
        tasaPorCategoria.clear();
        
        Calendar now = Calendar.getInstance();        
        List<TasaCategoria> tasas = ccDAO.findTasas(now.get(Calendar.YEAR));
        for (TasaCategoria tasa : tasas) {
            tasaPorCategoria.put(tasa.getCategoria(), tasa);
        }
    }
    
    public Double getTasa(Integer categoria) {
        TasaCategoria tasa = tasaPorCategoria.get(categoria);
        if (tasa == null) {
            throw new IllegalStateException("tasa no existe para categoria: " + categoria);
        }
        return tasa.getTasa();
    }
    
    public double getTasaInteresMora() {
        return TASA_INTERES_MENSUAL;
    }
}
