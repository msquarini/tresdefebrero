package ar.com.jass.catastro.web.action.deuda;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionContext;

import ar.com.jass.catastro.business.ConsultaService;
import ar.com.jass.catastro.business.CuentaCorrienteService;
import ar.com.jass.catastro.model.Cuenta;
import ar.com.jass.catastro.model.TitularParcela;
import ar.com.jass.catastro.model.cuentacorriente.CuentaCorriente;
import ar.com.jass.catastro.model.cuentacorriente.TipoMovimiento;
import ar.com.jass.catastro.model.error.ErrorLog;
import ar.com.jass.catastro.web.action.GridBaseAction;
import ar.com.jass.catastro.web.commons.WebConstants;
import ar.com.jass.catastro.web.dto.CCTotalizadorDTO;
import ar.com.jass.catastro.web.dto.CuentaCorrienteDTO;

/**
 * Action de gestion de cuenta corriente de parcela
 * 
 * @author msquarini
 *
 */
public class CuentaCorrienteAction extends GridBaseAction<CuentaCorrienteDTO> {

    private static final long serialVersionUID = 7353477345330099548L;

    private static final Logger LOG = LoggerFactory.getLogger(CuentaCorrienteAction.class);

    @Autowired
    private CuentaCorrienteService ccService;

    @Autowired
    private ConsultaService consultaService;

    /**
     * Entidad Cuenta para gestion de cuenta corriente
     */
    private Cuenta cuenta;

    /**
     * Filtro de Cuenta Corriente
     */
    private Integer anio;

    private TipoMovimiento tipo;

    private Boolean sinPago;

    private Long idCC;

    private CuentaCorriente cc;

    private CCTotalizadorDTO totalizador;

    private Boolean generic = Boolean.FALSE;

    /**
     * Inicializar los datos al ingresar a cuenta corriente
     */
    public String input() {
        removeFromSession(WebConstants.CC_DATA);
        removeFromSession(WebConstants.CUENTA);
        anio = Calendar.getInstance().get(Calendar.YEAR);
        // Ingreso a CC para uso generico (habilitar busqueda por #cuenta!!
        generic = Boolean.TRUE;

        return "input";
    }

    /**
     * Llamada AJAX para obtener la informacion para la grilla en formato JSON
     * 
     * Se toma la cuenta en session para la busqueda de movimientos
     * 
     * @return
     */
    @SuppressWarnings("unchecked")
    public String getData() {
        try {
            Map<String, CuentaCorrienteDTO> agrupador = (Map<String, CuentaCorrienteDTO>) getFromSession(
                    WebConstants.CC_DATA);
            if (agrupador != null && !agrupador.isEmpty()) {
                setData(new ArrayList<CuentaCorrienteDTO>(agrupador.values()));
            }
            // List<CuentaCorrienteDTO> data = (List<CuentaCorrienteDTO>)
            // getFromSession(WebConstants.CC_DATA);
            // setData(data);
        } catch (Exception e) {
            LOG.error("error en getData cuenta corriente", e);
        }
        return "dataJSON";
    }

    /**
     * Busqueda de la cuenta origen a partir de ID de la cuenta seleccionada. Se redirije a la
     * pantalla principal de cuenta corriente.
     * 
     * @return
     */
    public String search() {
        LOG.info("Search CC filtros: {} {} {}", anio, tipo, sinPago);
          
        /* Si se recibe # de cuenta, entonces se realiza la busqueda */
        if (cuenta != null && cuenta.getCuenta().length() > 0) {
            try {
                cuenta = consultaService.findCuenta(cuenta.getCuenta(), null, "parcelas.titulares.titular",
                        "parcelas.domicilioInmueble", "parcelas.lineas");
            } catch (Exception e) {
                ErrorLog log = crearErrorLog("search", e.getMessage());
                LOG.error("Cuenta no encontrada {}, {}", log, cuenta.getCuenta(), e);
                setTituloError("Busqueda de cuenta");
                setMensajeError("No se encontro cuenta con Nro.: " + cuenta.getCuenta());
                return "errorJSON";
            }
            setInSession(WebConstants.CUENTA, cuenta);
            setInSession(WebConstants.CUENTA_CON_DEUDA, getCatastroService().presentaDeuda(cuenta));
            setInSession(WebConstants.TITULARES_ADD, new HashSet<TitularParcela>(cuenta.getParcela().getTitulares()));
            setInSession(WebConstants.LINEAS_DATA, cuenta.getParcela().getLineas());
        } else {
            cuenta = (Cuenta) getFromSession(WebConstants.CUENTA);
        }
        /**
         * Si estamos con una cuenta realizamos el calculo y agrupacion de movimientos de CC!
         */
        if (cuenta != null) {
            List<CuentaCorriente> movimientosCC = ccService.findMovimientosCC(cuenta, anio, tipo);
            Map<String, CuentaCorrienteDTO> agrupador = agruparMovimientos(movimientosCC);

            setInSession(WebConstants.CC_DATA, agrupador);
            return "cuenta-summary";
        }

        /* Cuenta no encontrada */
        return "json";
    }

    public String detail() {
        LOG.info("Detalle CC Id: {} ", idCC);
        cc = ccService.findMovimientoCC(idCC);
        return "info-detalle";
    }

    /**
     * Contabiliza totales
     * 
     * @param movimientos
     */
    /*
     * private void totalizar(List<CuentaCorriente> movimientos) { totalizador = new
     * CCTotalizadorDTO(); for (CuentaCorriente cc : movimientos) { totalizador.addMovimiento(cc); }
     * }
     */
    private Map<String, CuentaCorrienteDTO> agruparMovimientos(List<CuentaCorriente> movimientos) {
        Map<String, CuentaCorrienteDTO> agrupador = new HashMap<String, CuentaCorrienteDTO>();

        for (CuentaCorriente cc : movimientos) {
            String key = cc.getAnio() + "-" + cc.getCuota();
            CuentaCorrienteDTO ccDTO = agrupador.get(key);
            if (ccDTO == null) {
                ccDTO = new CuentaCorrienteDTO(cc.getId(), cc.getAnio(), cc.getCuota());
                ccDTO.addMovimiento(cc);
                agrupador.put(key, ccDTO);
            } else {
                ccDTO.addMovimiento(cc);
            }
        }
        // return new ArrayList<CuentaCorrienteDTO>(agrupador.values());
        return agrupador;
    }

    public static Logger getLog() {
        return LOG;
    }

    public ConsultaService getConsultaService() {
        return consultaService;
    }

    public void setConsultaService(ConsultaService consultaService) {
        this.consultaService = consultaService;
    }

    public Cuenta getCuenta() {
        return cuenta;
    }

    public void setCuenta(Cuenta cuenta) {
        this.cuenta = cuenta;
    }

    public CuentaCorrienteService getCcService() {
        return ccService;
    }

    public void setCcService(CuentaCorrienteService ccService) {
        this.ccService = ccService;
    }

    public Integer getAnio() {
        return anio;
    }

    public void setAnio(Integer anio) {
        this.anio = anio;
    }

    public TipoMovimiento getTipo() {
        return tipo;
    }

    public void setTipo(TipoMovimiento tipo) {
        this.tipo = tipo;
    }

    public Boolean getSinPago() {
        return sinPago;
    }

    public void setSinPago(Boolean sinPago) {
        this.sinPago = sinPago;
    }

    public Long getIdCC() {
        return idCC;
    }

    public void setIdCC(Long idCC) {
        this.idCC = idCC;
    }

    public CuentaCorriente getCc() {
        return cc;
    }

    public void setCc(CuentaCorriente cc) {
        this.cc = cc;
    }

    public CCTotalizadorDTO getTotalizador() {
        return totalizador;
    }

    public void setTotalizador(CCTotalizadorDTO totalizador) {
        this.totalizador = totalizador;
    }

    public Boolean getGeneric() {
        return generic;
    }

    public void setGeneric(Boolean generic) {
        this.generic = generic;
    }
}
