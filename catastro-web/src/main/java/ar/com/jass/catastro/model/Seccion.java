package ar.com.jass.catastro.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.OrderBy;

@Entity
@Table(name = "Seccion")
public class Seccion {

	public String seccion;

	public Seccion() {
		
	}
	
	@Id
	@Column(name = "seccion", nullable = false)	
	public String getSeccion() {
		return seccion;
	}

	public void setSeccion(String seccion) {
		this.seccion = seccion;
	}
	
	
}
