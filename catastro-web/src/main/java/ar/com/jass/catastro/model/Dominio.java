package ar.com.jass.catastro.model;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

/**
 * Dominio de inscripcion de la parcela
 * 
 * @author MESquarini
 *
 */
// @Entity
// @Table(name = "Dominio")
@Embeddable
public class Dominio implements Serializable {

    // private Long id;
    // private Parcela parcela;
    private TipoDominio tipo;
    private Integer numero;
    private Integer anio;

    public Dominio() {
        this.tipo = TipoDominio.F;
    }

    public Dominio(TipoDominio tipo, Integer numero, Integer anio) {
        if (tipo.equals(TipoDominio.M) && anio == null) {
            throw new IllegalArgumentException("dominio M sin anio");
        }

        // this.parcela = parcela;
        // parcela.setDominio(this);

        this.tipo = tipo;
        this.numero = numero;
        this.anio = anio;
    }

    /*
     * @Id
     * 
     * @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_dominio_gen")
     * 
     * @SequenceGenerator(name = "seq_dominio_gen", sequenceName = "seq_dominio", allocationSize =
     * 1)
     * 
     * @Column(name = "id_dominio") public Long getId() { return id; }
     * 
     * public void setId(Long id) { this.id = id; }
     */

    // @OneToOne(fetch = FetchType.EAGER)
    // @JoinColumn(name = "id_parcela")
    // public Parcela getParcela() {
    // return parcela;
    // }
    //
    // public void setParcela(Parcela parcela) {
    // this.parcela = parcela;
    // }

    @Enumerated(EnumType.STRING)
    @Column(name = "dominio_tipo")
    public TipoDominio getTipo() {
        return tipo;
    }

    public void setTipo(TipoDominio tipo) {
        this.tipo = tipo;
    }

    @Column(name = "dominio_numero")
    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    @Column(name = "dominio_anio")
    public Integer getAnio() {
        return anio;
    }

    public void setAnio(Integer anio) {
        this.anio = anio;
    }

    public String toString() {
        return "Dominio Tipo: [" + getTipo() + "] Nro: [" + getNumero() + "] Anio: [" + getAnio() + "]";
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.tipo, this.numero, this.anio);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Dominio other = (Dominio) obj;
        return Objects.equals(this.anio, other.anio) && Objects.equals(this.numero, other.numero)
                && Objects.equals(this.tipo, other.tipo);
    }

}
