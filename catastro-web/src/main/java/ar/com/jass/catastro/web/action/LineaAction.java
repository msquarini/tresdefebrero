package ar.com.jass.catastro.web.action;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ar.com.jass.catastro.model.Cuenta;
import ar.com.jass.catastro.model.EstadoTramite;
import ar.com.jass.catastro.model.Linea;
import ar.com.jass.catastro.model.TipoTramite;
import ar.com.jass.catastro.model.Tramite;
import ar.com.jass.catastro.model.temporal.LineaTemporal;
import ar.com.jass.catastro.web.commons.WebConstants;

/**
 * Gestion de Linea Valuatoria
 * 
 * @author msquarini
 *
 */
public class LineaAction extends GridBaseAction<Linea> {

    private static final long serialVersionUID = 7353477345330099548L;

    private static final Logger LOG = LoggerFactory.getLogger("LineaAction");

    private Linea nuevaLinea;

    private Integer ajusteCategoria;

    private Tramite tramite;

    /*
     * (non-Javadoc)
     * 
     * @see com.opensymphony.xwork2.ActionSupport#input()
     */
    public String input() {
        return "input";
    }

    /**
     * Inicializar la gestion de ajuste de lineas
     * 
     * @return
     */
    public String init() {
        // Busqueda de cuenta origen a partir de ID
        cuentaOrigen = getConsultaService().findCuentaById(idCuentaOrigen, "parcela.lineas");

        if (cuentaOrigen == null) {
            LOG.warn("No se encontro cuenta ID: {}", idCuentaOrigen);
            addActionError(getText("error.cuenta.notfound"));
            return "input";
        }

        if (!cuentaOrigen.getActiva()) {
            LOG.warn("Cuenta no activa: {}", cuentaOrigen);
            addActionError(getText("error.cuenta.inactiva"));
            return "errorJSON";
        }
        if (cuentaOrigen.isBloqueada()) {
            LOG.warn("Cuenta bloqueada: {}", cuentaOrigen);
            addActionError(getText("error.cuenta.bloqueada"));
            return "errorJSON";
        }

        // Validacion si presenta cuenta y pasar la decision de continuar al operador
        String hashinsession = (String) getFromSession("hash");
        if (hashinsession != null && hashinsession.equals(hash)) {
            LOG.info("Cuenta presenta deuda, el operador decide continuar con el trámite, {}", cuentaOrigen);
            setInSession(WebConstants.CUENTA_CON_DEUDA, Boolean.TRUE);
            removeFromSession("hash");
        } else {
            setInSession(WebConstants.CUENTA_CON_DEUDA, Boolean.FALSE);
            if (getCatastroService().presentaDeuda(cuentaOrigen)) {
                hash = "AJU-" + UUID.randomUUID().toString();
                setInSession("hash", hash);
                LOG.warn("Cuenta presenta deuda: {}", cuentaOrigen);
                addActionError("Cuenta presenta deuda");
                return "errorDeuda";
            }
        }
        // tramite = new Tramite(TipoTramite.LIN);
        tramite = new Tramite(TipoTramite.LIN, new ArrayList<Cuenta>(), Arrays.asList(cuentaOrigen));
        setInSession(WebConstants.TRAMITE, tramite);

        //setInSession(WebConstants.CUENTA_ORIGEN, cuentaOrigen);
        setInSession(WebConstants.CUENTA, cuentaOrigen);

        // List<Linea> lineas = cuentaOrigen.getParcela().getLineas();
        // lineas.add(0, cuentaOrigen.getParcela().getLineaVigente());

        // Seteo nuevaLinea con los valores de la linea vigente
        nuevaLinea = new Linea(cuentaOrigen.getParcela().getLineaVigente());

        setInSession(WebConstants.LINEAS_DATA, cuentaOrigen.getParcela().getLineas());
        return "inprocess";
    }

    public String initFromTramite() {
        tramite = (Tramite) getFromSession(WebConstants.TRAMITE);
        cuentaOrigen = tramite.getCuentasOrigen().get(0);

        // Seteo nuevaLinea con los valores de la linea vigente en caso de tramite finalizado
        // Sino tomo la linea temporal creada en la grabacion del tramite
        if (tramite.getEstado().isFinalizado()) {
            nuevaLinea = cuentaOrigen.getParcela().getLineaVigente();
        } else {
            LineaTemporal lineaTemporal = getConsultaService().findLineaTemporal(tramite);
            nuevaLinea = lineaTemporal.createLinea();
        }

        //setInSession(WebConstants.CUENTA_ORIGEN, cuentaOrigen);
        setInSession(WebConstants.CUENTA, cuentaOrigen);
        setInSession(WebConstants.LINEAS_DATA, cuentaOrigen.getParcela().getLineas());
        return "inprocess";
    }

    /**
     * Llamada AJAX para obtener la informacion para la grilla en formato JSON
     * 
     * @return
     */
    @SuppressWarnings("unchecked")
    public String getData() {
        LOG.info("getData");
        try {
            List<Linea> data = (List<Linea>) getFromSession(WebConstants.LINEAS_DATA);
            setData(data);
        } catch (Exception e) {
            LOG.error("error en getData lineas", e);
        }
        return "dataJSON";
    }

    /**
     * Grabar tramite
     * 
     * @return
     */
    public String saveTramite() {
        LOG.info("Grabando tramite de ajuste de linea");

        try {
            // cuentaOrigen = (Cuenta) getFromSession(WebConstants.CUENTA_ORIGEN);

            Tramite t = (Tramite) getFromSession(WebConstants.TRAMITE);
            t.setObservacion(tramite.getObservacion());
            t.setOficio(tramite.getOficio());
            
            tramite = getCatastroService().grabarTramite(t, nuevaLinea, ajusteCategoria);
            setInSession(WebConstants.TRAMITE, tramite);

            addActionMessage(getText("tramite.save.success"));
            return "success";
        } catch (Exception e) {
            LOG.error("error en grabacion de tramite de ajuste de linea", e);
            addActionError(getText("error.unknown"));
            return "errorJSON";
        }
    }

    /**
     * Procesar el tramite
     * 
     * @return
     */
    public String process() {
        try {
            //cuentaOrigen = (Cuenta) getFromSession(WebConstants.CUENTA_ORIGEN);
            cuentaOrigen = (Cuenta) getFromSession(WebConstants.CUENTA);
            LOG.info("Procesando ajuste de linea {}", cuentaOrigen);

            tramite = (Tramite) getFromSession(WebConstants.TRAMITE);
            if (tramite == null) {
                throw new IllegalStateException();
            }

            // LLamar a Servicio para actualizar la lista de titulares
            tramite = getCatastroService().updateLinea(tramite, cuentaOrigen, nuevaLinea, ajusteCategoria);

            addActionMessage(getText("tramite.process.success"));
            removeFromSession(WebConstants.TRAMITE);

            /*
             * } catch (BusinessException e) {
             * LOG.error("error en procesamiento de tramite de ajuste de linea", e);
             * addActionError(getText(e.getMessage())); return "errorJSON";
             */
        } catch (Exception e) {
            LOG.error("error en procesamiento de tramite de ajuste de linea", e);
            addActionError(getText("error.unknown"));
            return "errorJSON";
        }
        // Ticket del tramite!
        return "success";
    }

    public Linea getNuevaLinea() {
        return nuevaLinea;
    }

    public void setNuevaLinea(Linea nuevaLinea) {
        this.nuevaLinea = nuevaLinea;
    }

    public Tramite getTramite() {
        return tramite;
    }

    public void setTramite(Tramite tramite) {
        this.tramite = tramite;
    }

    public Integer getAjusteCategoria() {
        return ajusteCategoria;
    }

    public void setAjusteCategoria(Integer ajusteCategoria) {
        this.ajusteCategoria = ajusteCategoria;
    }

}