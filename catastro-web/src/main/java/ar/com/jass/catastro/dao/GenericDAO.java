package ar.com.jass.catastro.dao;

import java.util.List;

import ar.com.jass.catastro.model.Cuenta;

public interface GenericDAO<E, K> {
	
	public void save(E entity);

	public void saveOrUpdate(E entity);

	public void update(E entity);

	public void remove(E entity);

	public E find(Class<E> type, K key);

	public List<E> getAll(Class<E> type);
	
	/**
	 * Obtiene Cuenta x cuenta&dv
	 * @param cuenta
	 * @param dv
	 * @return
	 */
	Cuenta findCuentaByCuentaDV(String cuenta, String dv);
}