package ar.com.jass.catastro.model.cuentacorriente;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import ar.com.jass.catastro.model.EntityBase;

/**
 * Entidad de movimientos de cuenta corriente. Deudas y creditos
 * 
 * @author msquarini
 *
 */
@Entity
@Table(name = "Periodo")
public class Periodo extends EntityBase implements Serializable {

    private Long id;

    private Integer anio;
    private Integer cuota;
    private Date fechaVto;
    private Date fechaSegundoVto;

    /**
     * Indica si ya se genero deuda para el periodo
     */
    private DeudaPeriodoStatus estado;

    /**
     * Indica si debe generar cuota anual en el periodo
     */
    private Boolean generarAnual;

    /**
     * cantidad de periodos que integran la cuota
     */
    private Integer periodosAnual;

    /**
     * Numero de cuota a utilizar para indicar que es cuota anual
     */
    private Integer numeroCuotaAnual;

    /**
     * Porcentaje a aplicar al valor de cuota (si != null)
     */
    private Double porcentajeAjuste;
    
    /**
     * Porcentaje de bonificacion que aplica la cuota anual
     */
    private Double porcentajeBonificacion;
    
    private BoletaPeriodoStatus boletaStatus;
    /**
     * Constructor default de movimiento de debito
     */

    /**
     * File de deuda generados
     */
    private List<FileDeuda> fileDeuda;

    public Periodo() {
    }

    public Periodo(Integer anio, Integer cuota) {
        this.anio = anio;
        this.cuota = cuota;
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_periodo_gen")
    @SequenceGenerator(name = "seq_periodo_gen", sequenceName = "seq_periodo", allocationSize = 1)
    @Column(name = "id_periodo")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "anio")
    public Integer getAnio() {
        return anio;
    }

    public void setAnio(Integer anio) {
        this.anio = anio;
    }

    @Column(name = "cuota")
    public Integer getCuota() {
        return cuota;
    }

    public void setCuota(Integer cuota) {
        this.cuota = cuota;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id, this.anio, this.cuota);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Periodo other = (Periodo) obj;
        return Objects.equals(this.id, other.id) && Objects.equals(this.anio, other.anio)
                && Objects.equals(this.cuota, other.cuota);
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "fecha_vto")
    public Date getFechaVto() {
        return fechaVto;
    }

    public void setFechaVto(Date fechaVto) {
        this.fechaVto = fechaVto;
    }

    @Column(name = "periodos_cuota_anual")
    public Integer getPeriodosAnual() {
        return periodosAnual;
    }

    public void setPeriodosAnual(Integer periodosAnual) {
        this.periodosAnual = periodosAnual;
    }

    @Column(name = "numero_cuota_anual")
    public Integer getNumeroCuotaAnual() {
        return numeroCuotaAnual;
    }

    public void setNumeroCuotaAnual(Integer numeroCuotaAnual) {
        this.numeroCuotaAnual = numeroCuotaAnual;
    }

    @Column(name = "genera_cuota_anual")
    public Boolean isGenerarAnual() {
        return generarAnual;
    }

    public void setGenerarAnual(Boolean generarAnual) {
        this.generarAnual = generarAnual;
    }

    @Column(name = "estado")
    public DeudaPeriodoStatus getEstado() {
        return estado;
    }

    public void setEstado(DeudaPeriodoStatus estado) {
        this.estado = estado;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "fecha_vto2")
    public Date getFechaSegundoVto() {
        return fechaSegundoVto;
    }

    public void setFechaSegundoVto(Date fechaSegundoVto) {
        this.fechaSegundoVto = fechaSegundoVto;
    }

    @Column(name = "porc_ajuste")
    public Double getPorcentajeAjuste() {
        return porcentajeAjuste;
    }

    public void setPorcentajeAjuste(Double porcentajeAjuste) {
        this.porcentajeAjuste = porcentajeAjuste;
    }

    @Override
    public String toString() {
        return "Periodo [id=" + id + ", anio=" + anio + ", cuota=" + cuota + ", fechaVto=" + fechaVto
                + ", fechaSegundoVto=" + fechaSegundoVto + ", estado=" + estado + ", generarAnual=" + generarAnual
                + ", periodosAnual=" + periodosAnual + ", numeroCuotaAnual=" + numeroCuotaAnual + ", porcentajeAjuste="
                + porcentajeAjuste + "]";
    }

    @OneToMany(mappedBy = "periodo", fetch = FetchType.LAZY)
    public List<FileDeuda> getFileDeuda() {
        return fileDeuda;
    }

    public void setFileDeuda(List<FileDeuda> fileDeuda) {
        this.fileDeuda = fileDeuda;
    }
    
    /**
     * Calcula el monto de bonificacion de cuota anual sobre el total de acuerdo a la configuracion del periodo
     * @param total
     * @return
     */
    public BigDecimal aplicarBonificacion(BigDecimal total) {
        if (getPorcentajeBonificacion() != null && getPorcentajeBonificacion() > 0) {
            BigDecimal tasaBonificacion = BigDecimal.valueOf(porcentajeBonificacion);
            BigDecimal bonificacion = total.multiply(tasaBonificacion).setScale(2, java.math.RoundingMode.HALF_DOWN);
            return bonificacion;
        }
        return null;
    }

    @Column(name = "porc_bonificacion")
    public Double getPorcentajeBonificacion() {
        return porcentajeBonificacion;
    }

    public void setPorcentajeBonificacion(Double porcentajeBonificacion) {
        this.porcentajeBonificacion = porcentajeBonificacion;
    }

    @Column(name = "boleta_status")
    public BoletaPeriodoStatus getBoletaStatus() {
        return boletaStatus;
    }

    public void setBoletaStatus(BoletaPeriodoStatus boletaStatus) {
        this.boletaStatus = boletaStatus;
    }

    /**
     * Si el movimiento corresponde a un periodo previo al periodo indicado
     * 
     * @param periodo
     * @return
     */
    public Boolean esPrevio(Periodo periodo) {
        if (getAnio() < periodo.getAnio()) {
            return true;
        } else if ((getCuota() < periodo.getCuota()) && (getAnio().intValue() == periodo.getAnio().intValue())) {
            return true;
        }
        return false;
    }
}