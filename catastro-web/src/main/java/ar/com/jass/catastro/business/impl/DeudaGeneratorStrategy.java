package ar.com.jass.catastro.business.impl;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.com.jass.catastro.business.DeudaService;
import ar.com.jass.catastro.business.deuda.TemplateCuotaService;
import ar.com.jass.catastro.dao.CuentaCorrienteDAO;
import ar.com.jass.catastro.model.Cuenta;
import ar.com.jass.catastro.model.Parcela;
import ar.com.jass.catastro.model.cuentacorriente.AjusteDTO;
import ar.com.jass.catastro.model.cuentacorriente.CuentaCorriente;
import ar.com.jass.catastro.model.cuentacorriente.GeneracionDeudaDTO;
import ar.com.jass.catastro.model.cuentacorriente.Periodo;
import ar.com.jass.catastro.model.cuentacorriente.TipoMovimiento;

/**
 * Proceso de generacion de deuda para el periodo Crea los registros de CuentaCorriente y el detalle
 * de ellos
 * 
 * @author msquarini
 *
 */
@Service
public class DeudaGeneratorStrategy {

    private static final Logger LOG = LoggerFactory.getLogger(DeudaGeneratorStrategy.class);

    @Autowired
    private DeudaService deudaService;
    
    @Autowired
    private TemplateCuotaService templateCuotaService;

    @Autowired
    private CuentaCorrienteDAO ccDAO;

    /**
     * Generacion de deuda para un periodo y los datos de generacion. Proceso BATCH de generacion de
     * deuda
     * 
     * @param periodo
     * @param dto
     * @return
     */
    public GeneracionDeudaDTO generarDeuda(Periodo periodo, GeneracionDeudaDTO dto) {
        Cuenta cuenta = new Cuenta();
        cuenta.setId(dto.getIdCuenta());

        // Nuevo Movimiento de Deuda
        CuentaCorriente movimientoCuota = deudaService.crearCuotaCuenta(dto, periodo);
        movimientoCuota.setCuenta(cuenta);
        dto.addMovimiento(movimientoCuota);

        // Crear los movimiento de exenciones
        if (dto.getExenta()) {
            // aplicarExencion(periodo, dto);
        }

        /**
         * Presenta tope en el calculo de deuda, por lo cual se debe crear un movimiento de credito
         * por la diferencia, solo para periodos mayores a 1
         */
        if (dto.isConTope() && periodo.getCuota() > 1) {
            CuentaCorriente credito = deudaService.aplicarTope(movimientoCuota, periodo);
            if (credito != null) {
                dto.addMovimiento(credito);
            }
        }

        return dto;
    }

    /**
     * Calculo de ajustes de cuenta corriente
     * 
     * @param periodoDesde
     * @param ajusteDTO
     * @param periodoVigente
     */
    @Transactional
    // public List<CuentaCorriente> ajusteCuenta(Periodo periodoDesde, AjusteDTO ajusteDTO, Periodo
    // periodoVigente) {
    public List<CuentaCorriente> ajusteCuenta(Integer anioDesde, Integer mesDesde, AjusteDTO ajusteDTO,
            Periodo periodoVigente) {
        // Calculamos el nuevo valor total de cuota para obtener el diferencial con cada valor de
        // cuota previo
/*        BigDecimal valorCuotaNuevo = deudaService.calculoValorCuotaTotal(ajusteDTO.getCategoria(),
                ajusteDTO.getNuevaValuacion());
*/
        BigDecimal valorCuotaNuevo = templateCuotaService.generarCuota(TipoMovimiento.D, periodoVigente, ajusteDTO.getCategoria(),
                ajusteDTO.getNuevaValuacion(), null).getMontoTotal();
        
        /**
         * Ajustes para los movimientos desde el periodo de la fecha de vigencia del ajuste
         */
        // Integer desdeCuota = periodoDesde.getCuota();
        Integer desdeCuota = mesDesde;

        List<CuentaCorriente> ajustes = new ArrayList<CuentaCorriente>();
        // for (int anio = periodoDesde.getAnio(); anio <= periodoVigente.getAnio(); anio++) {
        for (int anio = anioDesde; anio <= periodoVigente.getAnio(); anio++) {
            // Obtenemos la ultima cuota de cada anio, para calcular el diferencial a aplicar en los
            // ajustes
            // CuentaCorriente cuotaDoce = ccDAO.findCuotaByCuentaId(ajusteDTO.getCuenta().getId(),
            // anio, 12, 0,
            // TipoMovimiento.D);

            /*
             * 20181124 MS - Cambio para sumarizar los movimientos para calcular el ajuste Tomamos
             * todos los movimientos del periodo/cuota 12 para calcular el ajuste
             */
            List<CuentaCorriente> movimientosPeriodoDoce = ccDAO.findMovimientos(ajusteDTO.getCuenta().getId(), anio,
                    12);

            // Seteamos la cuota doce, para realizar el calculo diferencial entre cuotas y aplicar
            // el ajuste a cada periodo
            // Double diferencial = (VCN - VCV) / VCV ;
            // ajusteDTO.calcularDiferencial(valorCuotaNuevo, cuotaDoce);
            ajusteDTO.calcularDiferencial(valorCuotaNuevo, movimientosPeriodoDoce);

            LOG.debug("Aplicando ajuste, anio: {}, {}, vigente: {}", anio, ajusteDTO, periodoVigente);

            // Realizar el ajuste de los movimientos del anio (ajuste = 0) aplicando el
            // diferencial!!
            // List<CuentaCorriente> cuotas =
            // ccDAO.findMovimientosEnAnio(ajusteDTO.getCuenta().getId(), anio,
            // desdeCuota, 0, TipoMovimiento.D);

            /*
             * 20181126 MS - Se toman todos los moviemientos para calcular el ajuste en base a la
             * diferencia entre debitos y creditos de cada periodo
             */
            List<CuentaCorriente> cuotas = ccDAO.findMovimientosEnAnio(ajusteDTO.getCuenta().getId(), anio, desdeCuota,
                    null, null);
            Map<Periodo, List<CuentaCorriente>> cuotasAgrupados = agruparMovimientosAjuste(cuotas);
            for (Map.Entry<Periodo, List<CuentaCorriente>> e : cuotasAgrupados.entrySet()) {
                ajusteDTO.setPeriodo(e.getKey());
                ajusteDTO.setMovimientos(e.getValue());
                ajustes.addAll(deudaService.crearMovimientoAjuste(ajusteDTO, periodoVigente));
            }

            // Para cada movimiento calculamos el movimiento de ajuste
            /*
             * for (CuentaCorriente cuotaOriginal : cuotas) {
             * ajustes.addAll(deudaService.crearMovimientoAjuste(cuotaOriginal, ajusteDTO,
             * periodoVigente)); }
             */

            // Reseteo cuota desde la 1 (la primera del año) para los años posteriores
            desdeCuota = 1;
        }

        return ajustes;
    }

    @Transactional
    public List<CuentaCorriente> generarDeudaRetroactivaSubdivision(List<CuentaCorriente> movimientosCuentaOrigen,
            List<Parcela> ufs, Date fechaVencimiento, Integer codigoAdministrativo) {

        LOG.debug("movimientos: {}, ufs: {}, vto: {}, codAdm: {}", movimientosCuentaOrigen.size(), ufs.size(),
                fechaVencimiento, codigoAdministrativo);

        List<CuentaCorriente> ajustes = new ArrayList<CuentaCorriente>();

        /**
         * Calculo de sumatoria de cuotas de UFs
         */
        BigDecimal sumatoriaCuotasUFs = BigDecimal.ZERO;
        for (Parcela uf : ufs) {
        /*    sumatoriaCuotasUFs = sumatoriaCuotasUFs.add(deudaService
                    .calculoValorCuota(uf.getLineaVigente().getCategoria(), uf.getLineaVigente().getValuacion()));*/
            sumatoriaCuotasUFs = sumatoriaCuotasUFs.add(templateCuotaService
                    .calculoValorCuotaPuro(uf.getLineaVigente().getCategoria(), uf.getLineaVigente().getValuacion(), null).getMonto());
        }

        for (Parcela uf : ufs) {
            // Calculo de proporcion de cuota de nueva parcela sobre todas las que componen la
            // subdivision
            BigDecimal proporcion = templateCuotaService
                    .calculoValorCuotaPuro(uf.getLineaVigente().getCategoria(), uf.getLineaVigente().getValuacion(), null).getMonto()
                    .divide(sumatoriaCuotasUFs, 4, RoundingMode.HALF_DOWN);

            // Copiar el movimiento, cambiando la cuenta y aplicando el % proporcion de la nueva
            // cuenta sobre el valor original
            for (CuentaCorriente cc : movimientosCuentaOrigen) {
                CuentaCorriente ajuste = aplicarRetroactivo(cc, proporcion, uf.getCuenta(), fechaVencimiento,
                        codigoAdministrativo);

                LOG.info("ajuste, sumUfs: {}, proporcion: {}, ajuste: {}", sumatoriaCuotasUFs, proporcion, ajuste);
                ajustes.add(ajuste);
            }
        }
        return ajustes;
    }

    @Transactional
    public List<CuentaCorriente> generarDeudaRetroactivaUnificacion(Integer desdeAnio, Integer desdeCuota,
            Cuenta nuevaCuenta, List<Cuenta> cuentasOrigen, Date fechaVencimiento, Integer codigoAdministrativo) {
        LOG.debug("Anio/Cuota {}/{}, {} {} {}", desdeAnio, desdeCuota, nuevaCuenta, fechaVencimiento,
                codigoAdministrativo);

        List<CuentaCorriente> ajustes = new ArrayList<CuentaCorriente>();
        List<Periodo> periodos = ccDAO.findPeriodosDesde(desdeAnio, desdeCuota);
        /**
         * Para cada periodo desde la fecha de vigencia se buscan los movimientos de cada cuenta
         * origen y se sumarizan para generar el movimiento en la cuesta destino
         */
        for (Periodo p : periodos) {
            CuentaCorriente totalDebito = new CuentaCorriente(TipoMovimiento.D, p);
            CuentaCorriente totalCredito = new CuentaCorriente(TipoMovimiento.C, p);
            totalDebito.setCuenta(nuevaCuenta);
            totalDebito.setFechaVto(fechaVencimiento);
            totalDebito.setCodAdministrativo(codigoAdministrativo);
            totalCredito.setCuenta(nuevaCuenta);
            totalCredito.setFechaVto(fechaVencimiento);
            totalCredito.setCodAdministrativo(codigoAdministrativo);

            /**
             * Por cada cuenta origen sumarizamos el total de credito y debito del periodo
             */
            for (Cuenta origen : cuentasOrigen) {
                List<CuentaCorriente> movimientos = ccDAO.findMovimientos(origen.getId(), p.getAnio(), p.getCuota());

                for (CuentaCorriente cc : movimientos) {
                    if (TipoMovimiento.D.equals(cc.getTipo())) {
                        totalDebito.setImporte(totalDebito.getImporte().add(cc.getMontoTotal()));
                    } else {
                        totalCredito.setImporte(totalCredito.getImporte().add(cc.getMontoTotal()));
                    }
                }
            }

            if (totalDebito.getMontoTotal().compareTo(BigDecimal.ZERO) > 0) {
                ajustes.add(totalCredito);
            }

            if (totalCredito.getMontoTotal().compareTo(BigDecimal.ZERO) > 0) {
                ajustes.add(totalCredito);
            }
        }

        return ajustes;
    }

    /**
     * Generamos una copia del movimiento de CC para la nueva UF con el importe proporcional a la UF
     * 
     * @param movimiento
     * @param proporcion
     * @param cuenta
     * @param fechaVencimiento
     * @return
     */
    private CuentaCorriente aplicarRetroactivo(CuentaCorriente movimiento, BigDecimal proporcion, Cuenta cuenta,
            Date fechaVencimiento, Integer codigoAdministrativo) {
        CuentaCorriente nueva = new CuentaCorriente(movimiento);
        nueva.setCuenta(cuenta);

        // Si se especifica fecha de vencimiento se actualiza, sino se mantiene la fecha del
        // movimiento original
        if (fechaVencimiento != null) {
            nueva.setFechaVto(fechaVencimiento);
        }
        nueva.setImporte(movimiento.getImporte().multiply(proporcion, new MathContext(2, RoundingMode.HALF_DOWN)));
        nueva.setCodAdministrativo(codigoAdministrativo);

        // TODO: se agrega detalle al movimiento de ajuste de subdivision???
        /*
         * for (CCDetalle detalle : movimiento.getDetalle()) { }
         */
        return nueva;
    }

    /**
     * Agrupar los moviemientos por periodo para el calculo de ajuste
     * 
     * @param ccs
     * @return
     */
    private Map<Periodo, List<CuentaCorriente>> agruparMovimientosAjuste(List<CuentaCorriente> ccs) {
        Map<Periodo, List<CuentaCorriente>> grupoAjustes = new HashMap<Periodo, List<CuentaCorriente>>();

        // Contabilizamos solo los movimientos de cuota pura y ajustes (C o D)
        for (CuentaCorriente cc : ccs) {
            List<CuentaCorriente> movimientos = grupoAjustes.get(cc.getPeriodo());
            if (movimientos == null) {
                List<CuentaCorriente> mov = new ArrayList<CuentaCorriente>();
                mov.add(cc);
                grupoAjustes.put(cc.getPeriodo(), mov);
            } else {
                movimientos.add(cc);
            }

        }
        return grupoAjustes;
    }
}