package ar.com.jass.catastro.web.action;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import ar.com.jass.catastro.business.CatastroService;
import ar.com.jass.catastro.business.ConsultaService;
import ar.com.jass.catastro.business.RatificacionService;
import ar.com.jass.catastro.model.TitularRatificacion;
import ar.com.jass.catastro.web.commons.WebConstants;

/**
 * Gestion de titulares
 * 
 * @author msquarini
 *
 */
public class TitularRatificacionAction extends GridBaseAction<TitularRatificacion> {

	private static final long serialVersionUID = 7353477345330099548L;

	private static final Logger LOG = LoggerFactory.getLogger("TitularAction");

	/**
	 * Indica si la busqueda es por Apellido o CUIT/CUIL o DNI
	 */
	private Integer searchBy;

	/**
	 * Busquedas
	 */
	private TitularRatificacion searchTitular;
	private String apellido;
	private String cuitcuil;
	private Integer dni;

	/**
	 * Id titular??
	 */
	private Long idTitular;

	/**
	 * Nuevo titular
	 */
	private TitularRatificacion titular;

	@Autowired
	private CatastroService catastroService;

	@Autowired
	private ConsultaService consultaService;
	
    @Autowired
    private RatificacionService ratificacionService;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.opensymphony.xwork2.ActionSupport#input()
	 */
	public String input() {
		apellido = "";
		cuitcuil = "";

		return "input";
	}

	/**
	 * Consulta de titulares x apellido o cuitcuil
	 * 
	 * @return
	 */
	public String search() {
		LOG.info("apellido {} / cuitcuil {}", searchTitular.getApellido(), searchTitular.getCuitcuil());

		try {
			// Valida datos

			// Consulta de titulares segun el criterio de busqueda
			
			/*
			 * if (searchBy.equals(1)) { // Cuenta c =
			 * catastroService.findCuenta(cuenta, dv); //
			 * titulares.add(c.getParcela()); } else { // titulares =
			 * consultaService.findParcelas(getNomenclatura()); }
			 */
			List<TitularRatificacion> titulares = ratificacionService.findTitulares(searchTitular);
			setInSession(WebConstants.TITULARES_DATA, titulares);

			return "grid";

		} catch (Exception e) {
			LOG.error("Error en busqueda de titulares", e);
			addActionError("Error en proceso de búsqueda, intente nuevamente.");
			return "errorJSON";
		}
	}

	/**
	 * Llamada AJAX para obtener la informacion para la grilla en formato JSON
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public String getData() {
		LOG.info("getData");

		try {
			List<TitularRatificacion> data = (List<TitularRatificacion>) getFromSession(WebConstants.TITULARES_DATA);

			setData(data);

		} catch (Exception e) {
			LOG.error("error en getData titulares", e);
		}
		return "dataJSON";
	}

	public ConsultaService getConsultaService() {
		return consultaService;
	}

	public void setConsultaService(ConsultaService consultaService) {
		this.consultaService = consultaService;
	}

	public Integer getSearchBy() {
		return searchBy;
	}

	public void setSearchBy(Integer searchBy) {
		this.searchBy = searchBy;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getCuitcuil() {
		return cuitcuil;
	}

	public void setCuitcuil(String cuitcuil) {
		this.cuitcuil = cuitcuil;
	}

	public Long getIdTitular() {
		return idTitular;
	}

	public void setIdTitular(Long idTitular) {
		this.idTitular = idTitular;
	}

	public Integer getDni() {
		return dni;
	}

	public void setDni(Integer dni) {
		this.dni = dni;
	}

	public TitularRatificacion getSearchTitular() {
		return searchTitular;
	}

	public void setSearchTitular(TitularRatificacion searchTitular) {
		this.searchTitular = searchTitular;
	}

	public TitularRatificacion getTitular() {
		return titular;
	}

	public void setTitular(TitularRatificacion titular) {
		this.titular = titular;
	}

}
