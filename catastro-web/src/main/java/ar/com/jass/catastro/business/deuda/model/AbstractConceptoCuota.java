package ar.com.jass.catastro.business.deuda.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import ar.com.jass.catastro.business.deuda.ConceptoCuotaDTO;

/**
 * Detalle de concepto de cuota
 * 
 * @author msquarini
 *
 */
@Entity()
@Table(name = "template_cuota_detalle")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "codigo", discriminatorType = DiscriminatorType.STRING)
public abstract class AbstractConceptoCuota {

    public static final BigDecimal CIEN = BigDecimal.valueOf(100);
    public static final BigDecimal DOCE = BigDecimal.valueOf(12);

    
    private Long id;
    private String codigo;
    private String description;
    private BigDecimal minValue;
    private Double percentaje;
    private TemplateCuota template;

    public abstract void crearConceptoCuota(ConceptoCuotaDTO conceptoCuota);

    @Id
    @Column(name = "id_template_detalle")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "codigo", insertable = false, updatable = false)
    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    @Column(name = "descripcion")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "min_value")
    public BigDecimal getMinValue() {
        return minValue;
    }

    public void setMinValue(BigDecimal minValue) {
        this.minValue = minValue;
    }

    @Column(name = "porcentaje")
    public Double getPercentaje() {
        return percentaje;
    }

    public void setPercentaje(Double percentaje) {
        this.percentaje = percentaje;
    }

    @Override
    public String toString() {
        return "AbstractConceptoDeuda [id=" + id + ", codigo=" + codigo + ", description=" + description + ", minValue="
                + minValue + ", percentaje=" + percentaje + "]";
    }

    @ManyToOne()
    @JoinColumn(name = "id_template")
    public TemplateCuota getTemplate() {
        return template;
    }

    public void setTemplate(TemplateCuota template) {
        this.template = template;
    }
}
