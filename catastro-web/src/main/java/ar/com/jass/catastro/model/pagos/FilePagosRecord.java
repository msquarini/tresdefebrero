package ar.com.jass.catastro.model.pagos;

public interface FilePagosRecord {

	boolean isHeader();
	boolean isDetail();
	boolean isTrailer();
	
	void complete(FilePago filePago);
}
