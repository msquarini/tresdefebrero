package ar.com.jass.catastro.dao.impl;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import ar.com.jass.catastro.dao.GenericDAO;
import ar.com.jass.catastro.model.Cuenta;
import ar.com.jass.catastro.model.Dominio;
import ar.com.jass.catastro.model.Nomenclatura;
import ar.com.jass.catastro.model.Parcela;
import ar.com.jass.catastro.model.TipoDominio;
import ar.com.jass.catastro.model.Titular;

@SuppressWarnings("unchecked")
@Repository("genericDAODummy")
public abstract class GenericDAODummy<E, K extends Serializable> implements
		GenericDAO<E, K> {

	private static final Logger LOG = LoggerFactory.getLogger("GenericDAOImpl");

	protected Class<? extends E> daoType;

	/**
	 * By defining this class as abstract, we prevent Spring from creating
	 * instance of this class If not defined as abstract,
	 * getClass().getGenericSuperClass() would return Object. There would be
	 * exception because Object class does not hava constructor with parameters.
	 */
	public GenericDAODummy() {
		Type t = getClass().getGenericSuperclass();
		ParameterizedType pt = (ParameterizedType) t;
		daoType = (Class) pt.getActualTypeArguments()[0];
	}	


	/*
	 * (non-Javadoc)
	 * 
	 * @see ar.com.jass.catastro.dao.GenericDAO#findByCuentaDV(java.lang.String,
	 * java.lang.String)
	 */
	public Cuenta findCuentaByCuentaDV(String cuenta, String dv) {
		LOG.info("findByCuenta");

		Parcela p = createParcela(Integer.valueOf(cuenta));
		Cuenta c = new Cuenta();
		c.setCuenta("123456");
		c.setDv("1");
		c.setId(Long.valueOf(1));
	

		return c;
	}

	protected Parcela createParcela(Integer i) {
		Parcela p = new Parcela();

		p.setNomenclatura(createNomenclatura(i));
		p.setDominio(createDominio(p));
		p.setId(Long.valueOf(i));
		p.setPartida(123*i);

		//p.setTitulares(createTitulares(2));

		return p;
	}

	protected Nomenclatura createNomenclatura(Integer i) {
		Nomenclatura n = new Nomenclatura();
		n.setCircunscripcion(i);
		n.setSeccion("S" + i);
		n.setFraccion(i);
		n.setFraccionLetra("fr" + i);
		n.setManzana(i);
		n.setManzanaLetra("Mz" + i);
		n.setParcela(i);
		n.setParcelaLetra("A");
		n.setUnidadFuncional("UF" + i);
		return n;
	}

	protected Dominio createDominio(Parcela p) {
		Dominio d = new Dominio();
		d.setAnio(2010);
		d.setNumero(123456);
		//d.setParcela(p);
		d.setTipo(TipoDominio.M);
		return d;
	}

	protected List<Titular> createTitulares(Integer i) {
		List<Titular> titulares = new ArrayList<Titular>();
		for (int x = 0; x < i; x++) {
			Titular t = new Titular();
			t.setApellido("Tit Apellido"+x);
			t.setNombre("Tit nombre"+x);
			t.setCuitcuil("30-222222222-3");
			t.setId(Long.valueOf(x));
			t.setRazonSocial("Razon Social " + x);
			titulares.add(t);
		}
		return titulares;
	}
}
