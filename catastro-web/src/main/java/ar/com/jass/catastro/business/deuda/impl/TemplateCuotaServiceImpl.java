package ar.com.jass.catastro.business.deuda.impl;

import java.math.BigDecimal;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import ar.com.jass.catastro.business.TasaService;
import ar.com.jass.catastro.business.deuda.ConceptoCuotaDTO;
import ar.com.jass.catastro.business.deuda.TemplateCuotaService;
import ar.com.jass.catastro.business.deuda.model.ConceptoCuotaPura;
import ar.com.jass.catastro.business.deuda.model.TemplateCuota;
import ar.com.jass.catastro.dao.CuentaCorrienteDAO;
import ar.com.jass.catastro.model.cuentacorriente.CCDetalle;
import ar.com.jass.catastro.model.cuentacorriente.CuentaCorriente;
import ar.com.jass.catastro.model.cuentacorriente.Periodo;
import ar.com.jass.catastro.model.cuentacorriente.TipoMovimiento;

/**
 * Servicio de calculo de cuota de acuerdo al template de cuota vigente
 * @author msquarini
 *
 */
public class TemplateCuotaServiceImpl implements TemplateCuotaService {

    @Autowired
    private TasaService tasaService;
    
    @Autowired
    @Qualifier("ccDAO")
    private CuentaCorrienteDAO ccDAO;
    
    private List<TemplateCuota> templates;
    
    @PostConstruct
    public void initialize() {
       // templates = new HashMap<>();
        templates = ccDAO.findTemplateCuota();
    }
    
    public CCDetalle calculoValorCuotaPuro(Integer categoria, BigDecimal valuacion, BigDecimal base) {
        ConceptoCuotaDTO conceptoCuotaDTO = new ConceptoCuotaDTO(null, categoria, valuacion, tasaService.getTasa(categoria), base);
        ConceptoCuotaPura ccp = new ConceptoCuotaPura();
        ccp.crearConceptoCuota(conceptoCuotaDTO);
        return conceptoCuotaDTO.getDetalles().get(0);
    }
    
    public CuentaCorriente generarCuota(TipoMovimiento tipo, Periodo periodo, Integer categoria, BigDecimal valuacion, BigDecimal base) {
        if (periodo == null) {
            //periodo = current!!!
        }
        TemplateCuota template = getTemplate(periodo);
        
        // Creamos el movieminto de CC
        CuentaCorriente movimiento = new CuentaCorriente(tipo, periodo);
                        
        // Agregamos los detalle que componene el movimiento
        ConceptoCuotaDTO conceptoCuotaDTO = new ConceptoCuotaDTO(periodo, categoria, valuacion, tasaService.getTasa(categoria), base); 
        template.generarCuota(conceptoCuotaDTO);
        
        movimiento.setDetalle(conceptoCuotaDTO.getDetalles());
        return movimiento;
    }

    private TemplateCuota getTemplate(Periodo periodo) {
        return templates.get(0);
    }
    
    public TasaService getTasaService() {
        return tasaService;
    }

    public void setTasaService(TasaService tasaService) {
        this.tasaService = tasaService;
    }
}
