package ar.com.jass.catastro.batch.pago.mapper.link;

import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;

import ar.com.jass.catastro.model.pagos.link.LinkHeader;

public class LinkHeaderFieldMapper implements FieldSetMapper<LinkHeader> {

	public LinkHeader mapFieldSet(FieldSet fs) {

		if (fs == null) {
			return null;
		}

		LinkHeader header = new LinkHeader();
		header.setFecha(fs.readDate("fecha", "yyyyMMdd"));
		header.setLine(fs.readString("linea"));
		return header;
	}

}
