package ar.com.jass.catastro.batch.pagos;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import ar.com.jass.catastro.business.CuentaCorrienteService;
import ar.com.jass.catastro.dao.GeneralDAO;
import ar.com.jass.catastro.model.cuentacorriente.Boleta;
import ar.com.jass.catastro.model.cuentacorriente.CuentaCorriente;
import ar.com.jass.catastro.model.pagos.FilePago;
import ar.com.jass.catastro.model.pagos.Pago;

/**
 * Procesa los pagos, generando el moviemiento de CC de credito para cada caso
 * 
 * @author mesquarini
 *
 */
public class PagosProcessor implements ItemProcessor<FilePago, FilePago> {

	private static final Logger LOG = LoggerFactory.getLogger("PagosProcessor");

	@Autowired
	@Qualifier("generalDAO")
	private GeneralDAO dao;

	@Autowired
	CuentaCorrienteService ccService;

	/* (non-Javadoc)
	 * @see org.springframework.batch.item.ItemProcessor#process(java.lang.Object)
	 */
	@Override
	public FilePago process(FilePago item) throws Exception {
		int errores = 0;
		int sinDeuda = 0;
		int procesados = 0;

		for (Pago pago : item.getPagos()) {

			/**
			 * Se identifica el PAGO buscando si el mismo corresponde a un registro de DEUDA
			 * CC o a uno de CUOTA ANUAL 
			 */
			try {
			    LOG.info("Procesando registro de pago {}", pago);
			    				
				Boleta boleta = ccService.findBoleta(pago);

				// SI no existe la boleta --> es una pago que va a otro lado para su tratamiento
				// posterior
				if (boleta == null) {
					pago.setSinDeudaRegistrada(Boolean.TRUE);
					sinDeuda++;
					LOG.warn("(SINDEUDA) Deuda no encontrado para el pago {}", pago);										
				} else {
					LOG.info("Pago para {}", boleta );
					
					List<CuentaCorriente> creditos = boleta.procesarPago(pago);
					dao.saveList(creditos);
					dao.save(boleta);
				}
                pago.setProcesado(Boolean.TRUE);
				procesados++;				
			} catch (Exception e) {
				LOG.error("Error en procesamiento de pago {}", pago, e);
				errores++;
				pago.setProcesado(Boolean.FALSE);
			} finally {
			    // Persistimos el registro de PAGO
			    dao.save(pago);
            }
		}
		item.setErrores(errores);
		item.setSinDeuda(sinDeuda);
		item.setProcesados(procesados);
		return item;
	}
}