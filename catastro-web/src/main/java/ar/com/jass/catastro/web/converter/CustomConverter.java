package ar.com.jass.catastro.web.converter;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.xwork2.conversion.TypeConversionException;
import com.opensymphony.xwork2.conversion.impl.DefaultTypeConverter;

import ar.com.jass.catastro.model.EstadoTramite;
import ar.com.jass.catastro.model.cuentacorriente.TipoMovimiento;
import ar.com.jass.catastro.model.pagos.EstadoFilePagos;

public class CustomConverter extends DefaultTypeConverter {

    private static final Logger LOG = LoggerFactory.getLogger(CustomConverter.class);

    
    public Object convertValue(Map context, Object object, Class type) {

        /*
         * Locale locale = getLocale(context); System.out.println(locale);
         */
        String[] obj = (String[]) object;
        if (obj.length == 0) {
            return null;
        }
        if (type == Date.class) {
            String datePattern = "dd/MM/yyyyy";
            DateFormat format = new SimpleDateFormat(datePattern);
            format.setLenient(false);
            try {
                String[] dateString = (String[]) object;
                return format.parse(dateString[0]);
            } catch (Exception e) {
            	LOG.error("", e);
                throw new TypeConversionException("Given Date is Invalid");
            }
        }
        if (type == Double.class) {
            try {
                String[] dateString = (String[]) object;
                return Double.valueOf(dateString[0]);
            } catch (Exception e) {
            	LOG.error("", e);
                throw new TypeConversionException("Given Double is Invalid");
            }
        }
        if (type == BigDecimal.class) {
            try {
                String[] dateString = (String[]) object;
                return new BigDecimal(dateString[0]);
            } catch (Exception e) {
            	LOG.error("", e);
                throw new TypeConversionException("Given BigDecimal is Invalid");
            }
        }
        if (type == EstadoFilePagos.class) {
            try {
                String[] s = (String[]) object;
                //return EstadoFilePagos.values()[Integer.valueOf((String) object)];
                return EstadoFilePagos.values()[Integer.valueOf(s[0])];
            } catch (Exception e) {
            	LOG.error("", e);
                throw new TypeConversionException("Given estado is Invalid");
            }
        }
        if (type == TipoMovimiento.class) {
            try {
                String[] s = (String[]) object;
                //return TipoMovimiento.values()[Integer.valueOf((String) object)];
                if (s[0].equals("")) return null;
                return TipoMovimiento.valueOf(s[0]);
            } catch (Exception e) {
            	LOG.error("", e);
                throw new TypeConversionException("Given tipo is Invalid");
            }
        }
        if (type == EstadoTramite.class) {
            try {
                String[] s = (String[]) object;
                return EstadoTramite.values()[Integer.valueOf(s[0])];
            } catch (Exception e) {
            	//LOG.error("", e);
                //throw new TypeConversionException("Given tipo is Invalid");
            }
        }
        return null;
    }

}