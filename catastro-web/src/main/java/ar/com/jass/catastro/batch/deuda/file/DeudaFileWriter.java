package ar.com.jass.catastro.batch.deuda.file;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileFooterCallback;
import org.springframework.batch.item.file.FlatFileHeaderCallback;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.transform.LineAggregator;
import org.springframework.beans.BeansException;
import org.springframework.core.io.Resource;

import ar.com.jass.catastro.batch.deuda.file.callback.DeudaFileCallback;
import ar.com.jass.catastro.business.impl.FileType;

/**
 * Writer de files de deudas para los bancos
 * 
 * @author mesquarini
 *
 */
public class DeudaFileWriter implements ItemWriter<CuentaDTO> {

    private static final Logger LOG = LoggerFactory.getLogger("DeudaFileWriter");

    /**
     * FlatWriter
     */
    private FlatFileItemWriter<DeudaDTO> delegate;

    /**
     * Destino del archivo
     */
    private FileType destination;

    /**
     * Mapa de tokenizadores para cada tipo de archivo de pagos
     */
    private Map<String, LineAggregator<DeudaDTO>> aggregators;

    private DeudaFileCallback headerCallback = null;
    private DeudaFileCallback footerCallback = null;

    @BeforeStep
    public void retrieveInterstepData(StepExecution stepExecution) {
        JobExecution jobExecution = stepExecution.getJobExecution();
        ExecutionContext jobContext = jobExecution.getExecutionContext();

        Resource resource = new org.springframework.core.io.FileSystemResource((String) jobContext.get("path"));
        delegate.setResource(resource);

        destination = (FileType) jobContext.get("destino");

        // Seteamos el line mapper correspondiente al tipo de origen del archivo de
        // pagos
        delegate.setLineAggregator(getLineAggregator());

        try {
            headerCallback = destination.getDeudaFileHeaderCallback();
            footerCallback = destination.getDeudaFileFooterCallback();            
        } catch (BeansException be) {
            LOG.error("Header & Trailer no encontrado para tipo: ", destination, be);
        }

        if (headerCallback != null) {
            delegate.setHeaderCallback((FlatFileHeaderCallback) headerCallback);
        }
        if (footerCallback != null) {
            delegate.setFooterCallback((FlatFileFooterCallback) footerCallback);
        }
        LOG.info("Init DeudaFileWriter Destino: {} Header: {} Footer: {}", destination, headerCallback != null,
                footerCallback != null);
    }

    private LineAggregator<DeudaDTO> getLineAggregator() {
        LineAggregator<DeudaDTO> aggregator = aggregators.get(destination.name());
        if (aggregator == null) {
            throw new IllegalStateException("No existe aggregator para el tipo de archivo, " + destination.name());
        }
        return aggregator;
    }

    @Override
    public void write(List<? extends CuentaDTO> cuentas) throws Exception {

        for (CuentaDTO cuenta : cuentas) {
            LOG.debug("Escribiendo registro deuda para {}", cuenta);
            List<DeudaDTO> deudas = cuenta.getDeudasDTO();
            delegate.write(deudas);

            if (footerCallback != null) {
                footerCallback.notify(deudas);
            }
        }
    }

    public FlatFileItemWriter<DeudaDTO> getDelegate() {
        return delegate;
    }

    public void setDelegate(FlatFileItemWriter<DeudaDTO> delegate) {
        this.delegate = delegate;
    }

    public Map<String, LineAggregator<DeudaDTO>> getAggregators() {
        return aggregators;
    }

    public void setAggregators(Map<String, LineAggregator<DeudaDTO>> aggregators) {
        this.aggregators = aggregators;
    }

}