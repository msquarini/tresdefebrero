package ar.com.jass.catastro.batch.deuda.boleta;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import ar.com.jass.catastro.model.Cuenta;
import ar.com.jass.catastro.model.cuentacorriente.Boleta;

/**
 * DTO de cuentas usado en el proceso de generacion de boletas
 * 
 * @author mesquarini
 *
 */
public class GeneracionBoletaDTO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private Cuenta cuenta;

    private List<Boleta> boletas;

    public GeneracionBoletaDTO() {
        boletas = new ArrayList<Boleta>();
    }

    @Override
    public String toString() {
        return "GeneracionBoletaDTO [" + cuenta + "]";
    }

    public List<Boleta> getBoletas() {
        return boletas;
    }

    public void setBoletas(List<Boleta> boletas) {
        this.boletas = boletas;
    }

    public Cuenta getCuenta() {
        return cuenta;
    }

    public void setCuenta(Cuenta cuenta) {
        this.cuenta = cuenta;
    }
}
