package ar.com.jass.catastro.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import ar.com.jass.catastro.model.cuentacorriente.CCDetalle;
import ar.com.jass.catastro.model.cuentacorriente.CCDetalle.CCDetalleBuilder;

/**
 * Titularidad de parcela. Fecha desde y hasta y porcentaje de titularidad
 * 
 * @author MESquarini
 * 
 */

@Entity
@Table(name = "Titular_Parcela")
public class TitularParcela extends EntityBase {//implements Editable {

	/*private UUID tpUUID = UUID.randomUUID();*/

	private Long id;

	private Parcela parcela;
	private Titular titular;

	private Double porcentaje;
	private Date desde;
	private Date hasta;

	public TitularParcela() {
		porcentaje = 0d;
	}

	public TitularParcela(Parcela parcela, Titular titular, Double porcentaje, Date desde, Date hasta) {
		this.parcela = parcela;
		this.titular = titular;
		this.porcentaje = porcentaje;
		this.desde = desde;
		this.hasta = hasta;		
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_titularparcela_gen")
	@SequenceGenerator(name = "seq_titularparcela_gen", sequenceName = "seq_titularparcela", allocationSize = 1)
	@Column(name = "id_titularparcela")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id_parcela")
	public Parcela getParcela() {
		return parcela;
	}

	public void setParcela(Parcela parcela) {
		this.parcela = parcela;
	}

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id_titular")
	public Titular getTitular() {
		return titular;
	}

	public void setTitular(Titular titular) {
		this.titular = titular;
	}

	@Column(name = "porcentaje")
	public Double getPorcentaje() {
		return porcentaje;
	}

	public void setPorcentaje(Double porcentaje) {
		this.porcentaje = porcentaje;
	}

	@Column(name = "desde")
	@Temporal(TemporalType.DATE)
	public Date getDesde() {
		return desde;
	}

	public void setDesde(Date desde) {
		this.desde = desde;
	}

	@Column(name = "hasta")
	@Temporal(TemporalType.DATE)
	public Date getHasta() {
		return hasta;
	}

	public void setHasta(Date hasta) {
		this.hasta = hasta;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((desde == null) ? 0 : desde.hashCode());
		result = prime * result + ((hasta == null) ? 0 : hasta.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((parcela == null) ? 0 : parcela.hashCode());
		result = prime * result + ((porcentaje == null) ? 0 : porcentaje.hashCode());
		result = prime * result + ((titular == null) ? 0 : titular.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TitularParcela other = (TitularParcela) obj;
		if (desde == null) {
			if (other.desde != null)
				return false;
		} else if (!desde.equals(other.desde))
			return false;
		if (hasta == null) {
			if (other.hasta != null)
				return false;
		} else if (!hasta.equals(other.hasta))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (parcela == null) {
			if (other.parcela != null)
				return false;
		} else if (!parcela.equals(other.parcela))
			return false;
		if (porcentaje == null) {
			if (other.porcentaje != null)
				return false;
		} else if (!porcentaje.equals(other.porcentaje))
			return false;
		if (titular == null) {
			if (other.titular != null)
				return false;
		} else if (!titular.equals(other.titular))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TitularParcela [id=" + id + ", parcela=" + parcela + ", titular=" + titular + ", porcentaje="
				+ porcentaje + ", desde=" + desde + ", hasta=" + hasta + "]";
	}

/*	@Override
	@Transient
	public UUID getUuid() {
		return tpUUID;
	}

	@Transient
	public String getUuidString() {
		return tpUUID.toString();
	}*/

}