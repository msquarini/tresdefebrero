package ar.com.jass.catastro.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.FilterDef;

/**
 * Linea de valuacion de parcela Solo 1 esta vigente, se mantiene el historial de lineas
 * 
 * @author MESquarini
 *
 */
@Entity
@Table(name = "Linea")
@FilterDef(name = "vigente", defaultCondition = "vigente = 'true'")
public class Linea extends EntityBase implements Serializable {

    private Long id;

    // private Parcela parcela;
    private Double superficieTerreno;
    private Double superficieCubierta;
    private Double superficieSemi;

    private BigDecimal valorTierra;
    private BigDecimal valorComun;
    private BigDecimal valorEdificio;

    private Date fecha;
    private boolean vigente;
    
    private Integer categoria;

    public Linea() {
        this.fecha = new Date();
        this.vigente = Boolean.TRUE;

        this.valorEdificio = BigDecimal.ZERO;
        this.valorComun = BigDecimal.ZERO;
        this.valorTierra = BigDecimal.ZERO;

        this.superficieCubierta = 0D;
        this.superficieTerreno = 0D;
        this.superficieSemi = 0D;
    }

    public Linea(Linea from) {
        this();
        this.vigente = Boolean.TRUE;
        this.valorComun = from.valorComun;
        this.valorEdificio = from.valorEdificio;
        this.valorTierra = from.valorTierra;
        this.superficieCubierta = from.superficieCubierta;
        this.superficieSemi = from.superficieSemi;
        this.superficieTerreno = from.superficieTerreno;
        this.categoria = from.categoria;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_linea_gen")
    @SequenceGenerator(name = "seq_linea_gen", sequenceName = "seq_linea", allocationSize = 1)
    @Column(name = "id_linea")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    /*
     * @OneToOne(fetch = FetchType.LAZY, mappedBy="lineaVigente") public Parcela getParcela() {
     * return parcela; }
     * 
     * public void setParcela(Parcela parcela) { this.parcela = parcela; }
     */

    @Column(name = "sup_terreno")
    public Double getSuperficieTerreno() {
        return superficieTerreno;
    }

    public void setSuperficieTerreno(Double superficieTerreno) {
        this.superficieTerreno = superficieTerreno;
    }

    @Column(name = "sup_cubierta")
    public Double getSuperficieCubierta() {
        if (superficieCubierta == null) {
            return 0D;
        }
        return superficieCubierta;
    }

    public void setSuperficieCubierta(Double superficieCubierta) {
        this.superficieCubierta = superficieCubierta;
    }

    @Column(name = "sup_semi")
    public Double getSuperficieSemi() {
        if (superficieSemi == null) {
            return 0D;
        }
        return superficieSemi;
    }

    public void setSuperficieSemi(Double superficieSemi) {
        this.superficieSemi = superficieSemi;
    }

    @Column(name = "valor_tierra")
    public BigDecimal getValorTierra() {
        return valorTierra;
    }

    public void setValorTierra(BigDecimal valorTierra) {
        this.valorTierra = valorTierra;
    }

    @Column(name = "valor_comun")
    public BigDecimal getValorComun() {
        return valorComun;
    }

    public void setValorComun(BigDecimal valorComun) {
        this.valorComun = valorComun;
    }

    @Column(name = "valor_edificio")
    public BigDecimal getValorEdificio() {
        if (valorEdificio == null) {
            return BigDecimal.ZERO;
        }
        return valorEdificio;
    }

    public void setValorEdificio(BigDecimal valorEdificio) {
        this.valorEdificio = valorEdificio;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fecha")
    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @Column(name = "vigente")
    public boolean isVigente() {
        return vigente;
    }

    public void setVigente(boolean vigente) {
        this.vigente = vigente;
    }
    
	  @Column(name = "categoria")
	  public Integer getCategoria() {
		  return categoria;
	  }
	
	  public void setCategoria(Integer categoria) {
		  this.categoria = categoria;
	  }

    @Override
    public int hashCode() {
        return Objects.hash(this.vigente, this.id, this.valorTierra, this.fecha);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Linea other = (Linea) obj;
        return Objects.equals(this.id, other.id) && Objects.equals(this.fecha, other.fecha)
                && Objects.equals(this.vigente, other.vigente) && Objects.equals(this.valorTierra, other.valorTierra);
    }

    /**
     * Valuacion de la parcela segun la linea
     * 
     * @return
     */
    @Transient
    public BigDecimal getValuacion() {
        return valorTierra.add(valorComun).add(valorEdificio);
    }
}
