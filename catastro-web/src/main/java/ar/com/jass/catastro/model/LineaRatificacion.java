package ar.com.jass.catastro.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.FilterDef;
import org.springframework.transaction.annotation.Transactional;

/**
 * Linea de valuacion de parcela
 * Solo 1 esta vigente, se mantiene el historial de lineas
 * 
 * @author MESquarini
 *
 */
@Entity
@Table(name = "Linea_Ratificacion")
@FilterDef(name="vigente", defaultCondition="vigente = 'true'")
public class LineaRatificacion extends EntityBase {

	private Long id;
	
//    private Parcela parcela;
	private Double superficieTerreno;
	private Double superficieCubierta;
	private Double superficieSemi;

	private BigDecimal valorTierra;
	private BigDecimal valorComun;
	private BigDecimal valorEdificio;

	private Date fecha;
	private boolean vigente;
	private Integer categoria;
	
	public LineaRatificacion() {
		this.fecha = new Date();
		this.vigente = Boolean.TRUE;
		
		this.valorEdificio = BigDecimal.ZERO;
		this.valorComun = BigDecimal.ZERO;
		this.valorTierra = BigDecimal.ZERO;
		
		this.superficieCubierta = 0D;
		this.superficieTerreno = 0D;
		this.superficieSemi = 0D;
	}
	
	public LineaRatificacion(Linea linea) {
		this.fecha = linea.getFecha();
		this.vigente = linea.isVigente();
		
		this.valorEdificio = linea.getValorEdificio();
		this.valorComun = linea.getValorComun();
		this.valorTierra = linea.getValorTierra();
		
		this.superficieCubierta = linea.getSuperficieCubierta();
		this.superficieTerreno = linea.getSuperficieTerreno();
		this.superficieSemi = linea.getSuperficieTerreno();
		
		this.categoria = linea.getCategoria();

	}

	/**
	 * Crea la entidad principal
	 * @return
	 */
	@Transient
	public Linea getLinea() {
		Linea l = new Linea();
		l.setCreatedBy(this.getCreatedBy());
		l.setCreatedDate(this.getCreatedDate());
		l.setFecha(this.getFecha());
		l.setModifiedBy(this.getModifiedBy());
		l.setModifiedDate(this.getModifiedDate());
		l.setSuperficieCubierta(this.getSuperficieCubierta());
		l.setSuperficieSemi(this.getSuperficieSemi());
		l.setSuperficieTerreno(this.getSuperficieTerreno());
		l.setValorComun(this.getValorComun());
		l.setValorEdificio(this.getValorEdificio());
		l.setValorTierra(this.valorTierra);
		l.setVigente(Boolean.TRUE);
		return l;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_linea_ratificacion_gen")
	@SequenceGenerator(name = "seq_linea_ratificacion_gen", sequenceName = "seq_linea_ratificacion", allocationSize = 1)
	@Column(name = "id_linea")
	public Long getId() {
		return id;
	}
	

	public void setId(Long id) {
		this.id = id;
	}	
	
/*    @OneToOne(fetch = FetchType.LAZY, mappedBy="lineaVigente")
    public Parcela getParcela() {
        return parcela;
    }

    public void setParcela(Parcela parcela) {
        this.parcela = parcela;
    }*/

	@Column(name = "sup_terreno")
	public Double getSuperficieTerreno() {
		return superficieTerreno;
	}

	public void setSuperficieTerreno(Double superficieTerreno) {
		this.superficieTerreno = superficieTerreno;
	}

	@Column(name = "sup_cubierta")
	public Double getSuperficieCubierta() {
		return superficieCubierta;
	}

	public void setSuperficieCubierta(Double superficieCubierta) {
		this.superficieCubierta = superficieCubierta;
	}

	@Column(name = "sup_semi")
	public Double getSuperficieSemi() {
		return superficieSemi;
	}

	public void setSuperficieSemi(Double superficieSemi) {
		this.superficieSemi = superficieSemi;
	}

	@Column(name = "valor_tierra")
	public BigDecimal getValorTierra() {
		return valorTierra;
	}

	public void setValorTierra(BigDecimal valorTierra) {
		this.valorTierra = valorTierra;
	}

	@Column(name = "valor_comun")
	public BigDecimal getValorComun() {
		return valorComun;
	}

	public void setValorComun(BigDecimal valorComun) {
		this.valorComun = valorComun;
	}

	@Column(name = "valor_edificio")
	public BigDecimal getValorEdificio() {
		return valorEdificio;
	}

	public void setValorEdificio(BigDecimal valorEdificio) {
		this.valorEdificio = valorEdificio;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "fecha")
	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	@Column(name = "vigente")
	public boolean isVigente() {
		return vigente;
	}

	public void setVigente(boolean vigente) {
		this.vigente = vigente;
	}
	
    @Column(name = "categoria")
    public Integer getCategoria() {
        return categoria;
    }

    public void setCategoria(Integer categoria) {
        this.categoria = categoria;
    }

	@Override
	public int hashCode() {
		return Objects.hash(
		         this.vigente, this.id, this.valorTierra, this.fecha);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
	      {
	         return false;
	      }
	      if (getClass() != obj.getClass())
	      {
	         return false;
	      }
	      final LineaRatificacion other = (LineaRatificacion) obj;
	      return    Objects.equals(this.id, other.id)
	             && Objects.equals(this.fecha, other.fecha)
	             && Objects.equals(this.vigente, other.vigente)
	             && Objects.equals(this.valorTierra, other.valorTierra);
	}

	/**
	 * Valuacion de la parcela segun la linea
	 * 
	 * @return
	 */
	@Transient
	public BigDecimal getValuacion() {
	    return valorTierra.add(valorComun).add(valorEdificio);
	}
}
