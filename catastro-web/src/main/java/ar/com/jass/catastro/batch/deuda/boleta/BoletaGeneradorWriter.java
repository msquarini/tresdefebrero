package ar.com.jass.catastro.batch.deuda.boleta;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.AfterStep;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

import ar.com.jass.catastro.business.BoletaService;
import ar.com.jass.catastro.model.Cuenta;
import ar.com.jass.catastro.model.cuentacorriente.BoletaPeriodoStatus;
import ar.com.jass.catastro.model.cuentacorriente.Periodo;

/**
 * Persiste registro de pagos y movimientos de Cuenta corriente
 * 
 * @author msquarini
 *
 */
public class BoletaGeneradorWriter implements ItemWriter<Cuenta> {

    private static final Logger LOG = LoggerFactory.getLogger(BoletaGeneradorWriter.class);

    @Autowired
    private BoletaService boletaService;

    private Periodo periodo;

    @BeforeStep
    public void retrieveInterstepData(StepExecution stepExecution) {
        JobExecution jobExecution = stepExecution.getJobExecution();
        ExecutionContext jobContext = jobExecution.getExecutionContext();

        // Id del periodo que ejecuta el JOB
        Long idPeriodo = jobExecution.getJobParameters().getLong("idPeriodo");
        setPeriodo((Periodo) jobContext.get("periodo_" + idPeriodo));
    }

    @AfterStep
    public void updatePeriodoData(StepExecution stepExecution) {
        if (stepExecution.getExitStatus().compareTo(ExitStatus.FAILED) > -1) {
            periodo.setBoletaStatus(BoletaPeriodoStatus.ERROR);
        } else {
            periodo.setBoletaStatus(BoletaPeriodoStatus.GENERADO);
        }
        LOG.info("Cambiando estado del periodo {}", periodo);
    }

    @Override
    public void write(List<? extends Cuenta> items) throws Exception {
        try {
            for (Cuenta c : items) {
                LOG.debug("Procesando Generacion Boletas, periodo {}, {}", periodo, c);
                boletaService.generarBoleta(c, periodo, false, true, true);
            }
        } catch (Exception e) {
            LOG.error("error en generacion de boletas", e);
        }
    }

    public BoletaService getBoletaService() {
        return boletaService;
    }

    public void setBoletaService(BoletaService boletaService) {
        this.boletaService = boletaService;
    }

    public Periodo getPeriodo() {
        return periodo;
    }

    public void setPeriodo(Periodo periodo) {
        this.periodo = periodo;
    }
}
