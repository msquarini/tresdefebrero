package ar.com.jass.catastro.web.action;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import ar.com.jass.catastro.business.BusinessHelper;
import ar.com.jass.catastro.business.CatastroService;
import ar.com.jass.catastro.exceptions.BusinessException;
import ar.com.jass.catastro.model.Cuenta;
import ar.com.jass.catastro.model.Parcela;
import ar.com.jass.catastro.model.ResponsableCuenta;
import ar.com.jass.catastro.model.TipoTramite;
import ar.com.jass.catastro.model.TitularParcela;
import ar.com.jass.catastro.model.Tramite;
import ar.com.jass.catastro.web.commons.WebConstants;

/**
 * (NO SE UTILIZA POR AHORA) Action de gestion de Desdoblamiento de parcela
 * 
 * @author msquarini
 *
 */
public class DesdoblamientoAction extends BaseAction {

    private static final long serialVersionUID = 7353477345330099548L;

    private static final Logger LOG = LoggerFactory.getLogger("DesdoblamientoAction");

    @Autowired
    private CatastroService catastroService;

    @Autowired
    private BusinessHelper businessHelper;

    /**
     * Datos de nueva parcela (formulario)
     */
    private Parcela nuevaParcela;

    /**
     * Identificar de cuenta origen a desdoblar
     */
    private Long idCuentaOrigen;

    /**
     * Cuenta origen
     */
    private Cuenta cuentaOrigen;

    /**
     * Tipo de tramite
     */
    private Tramite tramite;

    /**
     * Procesamiento de Desdoblamiento. Tomando datos de la cuenta origen y nueva parcela, se
     * procede a realizar logica de negocio
     * 
     * @return
     */
    public String process() {
        try {
            tramite = new Tramite();
            tramite.setTipo(TipoTramite.DES);
            cuentaOrigen = (Cuenta) getFromSession(WebConstants.CUENTA_ORIGEN);

            LOG.info("Procesando Desdoblamiento Cuenta: {}, Tramite: {}", cuentaOrigen, tramite);

            List<TitularParcela> titulares = new ArrayList<TitularParcela>(
                    (Set<TitularParcela>) getFromSession(WebConstants.TITULARES_ADD));
            businessHelper.checkTitularesConCuit(titulares);

            /*
             * Llamado a logica de negocio de desdoblamiento
             * 
             */
            //catastroService.desdoblar(tramite, cuentaOrigen, nuevaParcela, titulares);

        } catch (BusinessException be) {
            LOG.error("error de negocio en procesamiento de desdoblamiento", be);
            addActionError(getText(be.getMessage()));
            return "process_error";
        } catch (Exception e) {
            LOG.error("error en procesamiento de desdoblamiento", e);
            addActionError(getText("unknown_error"));
            return "process_error";
        }
        return "success";
    }

    /**
     * Ingreso a la pantalla de desdoblamiento. Se presenta formulario de busqueda de cuenta
     * 
     */
    public String input() {
        LOG.info("Ingreso a desdoblamiento de parcela");
        initCustomData();

        return "input";
    }

    /**
     * Busqueda de la cuenta origen a partir de ID de la cuenta seleccionada. Se redirije a la
     * pantalla principal de desdoblamiento para la carga de datos generales.
     * 
     * @return
     */
    public String init() {
        LOG.info("Inicio desdoblamiento Cuenta ID: {}", idCuentaOrigen);

        // Busqueda de cuenta origen a partir de ID
        cuentaOrigen = getConsultaService().findCuentaById(idCuentaOrigen, "parcela.titulares",
                "responsables", "parcela.domicilioInmueble");

        if (cuentaOrigen == null) {
            LOG.warn("No se encontro cuenta ID: {}", idCuentaOrigen);
            addActionError("Cuenta no encontrada, intente nuevamente");
            return "input";
        }

        // Validacion de cuenta No presente deuda!!!

        // Cuenta para subdividir
        setInSession(WebConstants.CUENTA_ORIGEN, cuentaOrigen);
        // Titulares en session para completar CUITCUIL
        setInSession(WebConstants.TITULARES_ADD, new HashSet<TitularParcela>(cuentaOrigen.getParcela().getTitulares()));
        setInSession(WebConstants.RESPONSABLES_ADD, new ArrayList<ResponsableCuenta>(cuentaOrigen.getResponsables()));
        
        // Copia los datos de la parcela origen en la nueva a crear
        nuevaParcela = createParcelaFromCuenta(cuentaOrigen);

        return "inprocess";
    }

    public Parcela getNuevaParcela() {
        return nuevaParcela;
    }

    public void setNuevaParcela(Parcela nuevaParcela) {
        this.nuevaParcela = nuevaParcela;
    }

    public CatastroService getCatastroService() {
        return catastroService;
    }

    public void setCatastroService(CatastroService catastroService) {
        this.catastroService = catastroService;
    }

    public static Logger getLog() {
        return LOG;
    }

    public Long getIdCuentaOrigen() {
        return idCuentaOrigen;
    }

    public void setIdCuentaOrigen(Long idCuentaOrigen) {
        this.idCuentaOrigen = idCuentaOrigen;
    }

    public Cuenta getCuentaOrigen() {
        return cuentaOrigen;
    }

    public void setCuentaOrigen(Cuenta cuentaOrigen) {
        this.cuentaOrigen = cuentaOrigen;
    }

    public Tramite getTramite() {
        return tramite;
    }

    public void setTramite(Tramite tramite) {
        this.tramite = tramite;
    }

}