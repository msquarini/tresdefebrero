package ar.com.jass.catastro.web.action;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import ar.com.jass.catastro.business.BusinessHelper;
import ar.com.jass.catastro.exceptions.BusinessException;
import ar.com.jass.catastro.model.Cuenta;
import ar.com.jass.catastro.model.ResponsableCuenta;
import ar.com.jass.catastro.model.TipoTramite;
import ar.com.jass.catastro.model.Titular;
import ar.com.jass.catastro.model.TitularParcela;
import ar.com.jass.catastro.model.Tramite;
import ar.com.jass.catastro.model.error.ErrorLog;
import ar.com.jass.catastro.web.commons.WebConstants;

/**
 * Gestion ABM de titulares2
 * 
 * Alta y edicion de TitularParcela
 * 
 * @author msquarini
 *
 */
public class TitularABMAction2 extends GridBaseAction<TitularParcela> {

	private static final long serialVersionUID = 7353477345330099548L;

	private static final Logger LOG = LoggerFactory.getLogger("TitularABMAction2");

	/**
	 * Se utiliza para tener en session el ID o Cuit del titular en edicion
	 */
	private static final String EDIT_TITULAR = "titular_edition";

	/**
	 * Se utiliza para indicar si es modo edicion o alta de titular
	 */
	private static final String EDIT_MODE = "edit_mode";

	/**
	 * ID de titular para agregar a la lista de TitularParcela
	 */
	private Long idTitular;

	/**
	 * Cuit/Cuil de titular a editar
	 */
	private String cuitcuil;

	private String uuidTP;

	/**
	 * Nuevo TitularParcela
	 */
	private TitularParcela titularParcela;

	@Autowired
	private BusinessHelper businessHelper;

	private Tramite tramite;
	private List<TitularParcela> titulares;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.opensymphony.xwork2.ActionSupport#input()
	 */
	public String input() {
		return "input";
	}

	/**
	 * Inicializar la gestion de cambio de titulares
	 * 
	 * @return
	 */
	public String init() {
		// Busqueda de cuenta origen a partir de ID
		cuentaOrigen = getConsultaService().findCuentaById(idCuentaOrigen, "responsables", "parcela.titulares",
				"parcela.domicilioInmueble");

		// Validacion si presenta cuenta y pasar la decision de continuar al operador
		String hashinsession = (String) getFromSession("hash");
		if (hashinsession != null && hashinsession.equals(hash)) {
			LOG.info("Cuenta presenta deuda, el operador decide continuar con el trámite, {}", cuentaOrigen);
			removeFromSession("hash");
		} else {
			if (getCatastroService().presentaDeuda(cuentaOrigen)) {
				hash = "TIT-" + UUID.randomUUID().toString();
				setInSession("hash", hash);
				LOG.warn("Cuenta presenta deuda: {}", cuentaOrigen);
				addActionError("Cuenta presenta deuda");
				return "errorDeuda";
			}
		}
		tramite = new Tramite(TipoTramite.TIT);
		setInSession(WebConstants.TRAMITE, tramite);
		// Cuenta para subdividir
		setInSession(WebConstants.CUENTA_ORIGEN, cuentaOrigen);
		// Titulares en session para editar
		setInSession(WebConstants.TITULARES_ADD, new HashSet<TitularParcela>(cuentaOrigen.getParcela().getTitulares()));
		setInSession(WebConstants.RESPONSABLES_ADD, new HashSet<ResponsableCuenta>(cuentaOrigen.getResponsables()));
		setInSession(WebConstants.TITULARES_DATA, null);
		return "inprocess";
	}

	public String initFromTramite() {
		tramite = (Tramite) getFromSession(WebConstants.TRAMITE);
		cuentaOrigen = tramite.getCuentasOrigen().get(0);

		setInSession(WebConstants.CUENTA_ORIGEN, cuentaOrigen);
		// Titulares en session
		setInSession(WebConstants.TITULARES_ADD, new HashSet<TitularParcela>(cuentaOrigen.getParcela().getTitulares()));
		setInSession(WebConstants.RESPONSABLES_ADD, new HashSet<ResponsableCuenta>(cuentaOrigen.getResponsables()));
		setInSession(WebConstants.TITULARES_DATA, null);
		return "inprocess";
	}

	/**
	 * Actualizar CUITCUIL de un titular en la grilla de datos de session
	 * 
	 * 
	 * @return
	 */
	public String updateCuitCuil() {
		LOG.info("updateCuitCuil titular, UUID [{}], CUIT [{}]", getUuidTP(), getCuitcuil());

		try {
			UUID uuid = UUID.fromString(getUuidTP());

			Set<TitularParcela> data = (Set<TitularParcela>) getFromSession(WebConstants.TITULARES_ADD);
			for (TitularParcela tp : data) {
				if (tp.getUuid().equals(uuid)) {
					LOG.info("updateCuitCuil OK");
					tp.getTitular().setCuitcuil(getCuitcuil());
					break;
				}
			}
			// Persisto el titular
			// dao.save(tp.getTitular());
		} catch (Exception e) {
			ErrorLog log = crearErrorLog("updateCuitCuil", e.getMessage());
			LOG.error("{}", log, e);
			addActionError("error_update_cuit_titular");
			return "errorJSON";
		}
		return "update_ok";
	}

	/**
	 * Llamada AJAX para obtener la informacion para la grilla en formato JSON
	 * Grilla de edicion de titulares
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public String getData() {
		try {
			List<TitularParcela> data = new ArrayList<TitularParcela>(
					(Set<TitularParcela>) getFromSession(WebConstants.TITULARES_ADD));
			setData(data);
		} catch (Exception e) {
			LOG.error("error en getData titulares", e);
		}
		return "dataJSON";
	}

	/**
	 * Redirije a pagina de alta
	 * 
	 * @return
	 */
	public String goToAddTitular() {
		setInSession(EDIT_MODE, false);
		return "add_titular_input";
	}

	/**
	 * Buscar el titular por el cuitcuil y setearlo para editar
	 * 
	 * @return
	 */
	public String goToEditTitular() {
		Set<TitularParcela> titularesParcela = (Set<TitularParcela>) getFromSession(WebConstants.TITULARES_ADD);
		UUID uuid = UUID.fromString(getUuidTP());
		for (TitularParcela tp : titularesParcela) {
			if (uuid.equals(tp.getUuid())) {
				titularParcela = tp;
				setInSession(EDIT_TITULAR, uuid);
				break;
			}
		}

		setInSession(EDIT_MODE, true);
		return "add_titular_input";

	}

	/**
	 * Agrega titular ya existente a la grilla ABM de titularesparcela. Valido que
	 * el titular no este presente en la lista
	 * 
	 * @return
	 */
	public String addSelectedTitular() {
		LOG.info("Agregando Titular ID [{}]", getIdTitular());

		Set<TitularParcela> titularesParcela = (Set<TitularParcela>) getFromSession(WebConstants.TITULARES_ADD);
		if (!titularesParcela.isEmpty()) {
			// UUID uuid = UUID.fromString(getUuidTP());

			// Verificar que no exista en la grilla por el ID del titular
			for (TitularParcela tp : titularesParcela) {

				// Si existe el titular dentro de los titularesParcela, vuelvo sin agregarlo
				if (getIdTitular().equals(tp.getTitular().getId())) {
					return "update_ok";
				}
			}
		}

		Titular titular = getConsultaService().findTitularById(getIdTitular(), "domicilioPostal");
		TitularParcela newTitularParcela = new TitularParcela();
		newTitularParcela.setTitular(titular);
		titularesParcela.add(newTitularParcela);
		return "update_ok";
	}

	/**
	 * Actualiza la grilla de titulares en la gestion de tramite
	 * 
	 * @return
	 */
	public String addTitular() {
		boolean isEditMode = (Boolean) getFromSession(EDIT_MODE);

		LOG.info("AddTitular ModoEdicion [{}], TitularParcela [{}]", isEditMode, titularParcela);

		Set<TitularParcela> titularesParcela = (Set<TitularParcela>) getFromSession(WebConstants.TITULARES_ADD);

		try {
			// Si no es modo edit -> es ALTA
			if (!isEditMode) {
				// TitularParcela tp = new TitularParcela();
				titularesParcela.add(titularParcela);
			} else {
				for (TitularParcela tp : titularesParcela) {
					if (isTitularParcelaInEdition(tp)) {
						LOG.info("Se modifico el TitularParcela: {}", tp);

						// Cambiar para copiar los valores y no reemplazar la entidad, se pierden
						// las
						// relaciones!!!
						updateValues(tp);
						break;
					}
				}
				setInSession(EDIT_TITULAR, null);
			}
		} catch (Exception e) {
			ErrorLog log = crearErrorLog("addTitular", e.getMessage());
			addActionError("error en ALTA de titular en grilla de edicion!!!");
			LOG.error("error agregando titular, {}", log, e);
			return "errorJSON";
		}

		return "update_ok";
	}

	/**
	 * Actualiza los datos del titular
	 * 
	 * @param t
	 */
	private void updateValues(TitularParcela tp) {
		updateTitularValues(titularParcela.getTitular(), tp.getTitular());
		tp.setPorcentaje(titularParcela.getPorcentaje());
		tp.setDesde(titularParcela.getDesde());
		tp.setHasta(titularParcela.getHasta());
	}

	/**
	 * Valida que el titular es el que esta en modo edicion.
	 * 
	 * @param t
	 * @return
	 */
	private boolean isTitularParcelaInEdition(TitularParcela tp) {
		return ((UUID) getFromSession(EDIT_TITULAR)).equals(tp.getUuid());
	}

	/**
	 * Procesar el tramite de gestion de titulares
	 * 
	 * @return
	 */
	public String process() {
		try {
			cuentaOrigen = (Cuenta) getFromSession(WebConstants.CUENTA_ORIGEN);

			tramite = (Tramite) getFromSession(WebConstants.TRAMITE);
			if (tramite == null) {
				throw new IllegalStateException();
			}

			titulares = new ArrayList<>((Set<TitularParcela>) getFromSession(WebConstants.TITULARES_ADD));

			// Chequear que los Titulares presenten CUIT/CUIL cargado/actualizado
			businessHelper.checkTitulares(titulares);

			// LLamar a Servicio para actualizar la lista de titulares
			tramite = getCatastroService().updateTitulares(tramite, cuentaOrigen, titulares);
			// tramite = getConsultaService().findTramite(100L);
			addActionMessage(getText("tramite.process.success"));
			removeFromSession(WebConstants.TRAMITE);
		} catch (BusinessException e) {
			ErrorLog log = crearErrorLog("process", e.getMessage());
			LOG.error("error en procesamiento de tramite de actualizacion de titulares, {}", log, e);
			addActionError(getText(e.getMessage()));
			return "errorJSON";
		} catch (Exception e) {
			ErrorLog log = crearErrorLog("process", e.getMessage());
			LOG.error("error en procesamiento de tramite de actualizacion de titulares, {}", log, e);
			addActionError(getText("error.unknown"));
			return "errorJSON";
		}
		// Ticket del tramite!
		return "success";
	}

	public String getCuitcuil() {
		return cuitcuil;
	}

	public void setCuitcuil(String cuitcuil) {
		this.cuitcuil = cuitcuil;
	}

	public TitularParcela getTitularParcela() {
		return titularParcela;
	}

	public void setTitularParcela(TitularParcela titularParcela) {
		this.titularParcela = titularParcela;
	}

	public Long getIdTitular() {
		return idTitular;
	}

	public void setIdTitular(Long idTitular) {
		this.idTitular = idTitular;
	}

	public String getUuidTP() {
		return uuidTP;
	}

	public void setUuidTP(String uuidTP) {
		this.uuidTP = uuidTP;
	}

	public Tramite getTramite() {
		return tramite;
	}

	public void setTramite(Tramite tramite) {
		this.tramite = tramite;
	}

	public List<TitularParcela> getTitulares() {
		return titulares;
	}

	public void setTitulares(List<TitularParcela> titulares) {
		this.titulares = titulares;
	}

}