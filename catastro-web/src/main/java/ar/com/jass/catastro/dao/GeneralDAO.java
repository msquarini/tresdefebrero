package ar.com.jass.catastro.dao;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import ar.com.jass.catastro.model.Cuenta;
import ar.com.jass.catastro.model.EstadoTramite;
import ar.com.jass.catastro.model.Nomenclatura;
import ar.com.jass.catastro.model.Parcela;
import ar.com.jass.catastro.model.Titular;
import ar.com.jass.catastro.model.Tramite;
import ar.com.jass.catastro.model.da.DebitoAutomatico;
import ar.com.jass.catastro.model.temporal.LineaTemporal;

public interface GeneralDAO {

    public Serializable save(Object entity);

    public <T> void saveList(List<T> entities);

    public <T> void updateList(List<T> entities);

    public void saveOrUpdate(Object entity);

    public void update(Object entity);

    public Object merge(Object entity);

    public void remove(Object entity);

    public <T> T find(Class<T> type, Serializable key);

    public <T> List<T> getAll(Class<T> type);

    public BigInteger obtenerNumeroCuenta();

    public Cuenta findCuenta(String cuenta, String dv);

    public Parcela findParcelaByPartida(Integer partida);

    public List<Titular> findTitulares(Titular titular);

    public Parcela findParcela(Nomenclatura nomenclatura);

    public List<Parcela> findParcelas(Nomenclatura nomenclatura);

    public boolean exists(Nomenclatura nomenclatura);

    /*
     * public CuentaCorriente findCuentaCorriente(Integer anio, Integer cuota, String cuenta, String
     * dv, BigDecimal importe, Date fechaVto, TipoMovimiento tipo);
     */

    public DebitoAutomatico findDA(String cuenta, String dv);

    public List<Cuenta> findCuentasLike(String cuenta);

    /**
     * Busqueda de tramites donde la cuenta es origen del mismo
     * 
     * @param cuentaId
     * @return
     */
    public List<Tramite> findTramites(Long cuentaId);

    public List<Tramite> getTramites(Date from, Date to, EstadoTramite estado, String usuario);

    public BigDecimal getDeuda(Long cuentaId);

    public LineaTemporal findLineaTemporalByTramite(Long idTramite);

}