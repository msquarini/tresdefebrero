package ar.com.jass.catastro.business.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.com.jass.catastro.business.CodAdministrativoService;
import ar.com.jass.catastro.business.CuentaCorrienteService;
import ar.com.jass.catastro.business.DeudaService;
import ar.com.jass.catastro.business.TasaService;
import ar.com.jass.catastro.business.deuda.TemplateCuotaService;
import ar.com.jass.catastro.business.deuda.model.ConceptoCustom;
import ar.com.jass.catastro.model.cuentacorriente.AjusteDTO;
import ar.com.jass.catastro.model.cuentacorriente.CCDetalle;
import ar.com.jass.catastro.model.cuentacorriente.CCDetalle.CCDetalleBuilder;
import ar.com.jass.catastro.model.cuentacorriente.ConceptoCuentaCorriente;
import ar.com.jass.catastro.model.cuentacorriente.CuentaCorriente;
import ar.com.jass.catastro.model.cuentacorriente.GeneracionDeudaDTO;
import ar.com.jass.catastro.model.cuentacorriente.Periodo;
import ar.com.jass.catastro.model.cuentacorriente.TipoMovimiento;

/**
 * Logica de calculo de cuota
 * 
 * @author msquarini
 *
 */
@Service
public class DeudaServiceImpl implements DeudaService {

    private static final Logger LOG = LoggerFactory.getLogger(DeudaServiceImpl.class);

    private static final BigDecimal MIN_TASA_SEG = BigDecimal.valueOf(70);
    private static final BigDecimal TASA_MANT = BigDecimal.valueOf(40);
    private static final BigDecimal PORC_TASA_SEG = BigDecimal.valueOf(0.1);

    private static final BigDecimal AJUSTE_TOPE = BigDecimal.valueOf(1.19);

    private Set<Integer> tasaSeguridadFija;
    
    @Autowired
    private TemplateCuotaService templateCuotaService;

    @Autowired
    private CodAdministrativoService codAdministrativoService;

    @Autowired
    private CuentaCorrienteService ccService;

    @PostConstruct
    public void initialize() {
        //tasaSeguridadFija = new HashSet<Integer>(Arrays.asList(1, 4, 6, 7));
    }


    /**
     * Crea el debito de cuota para cuentas. Si no tiene valuacion -> se calcula a partir del
     * concepto cuota pura de la cuota 1 del mismo anio
     * 
     * @param cuenta
     * @param periodo
     * @return
     */
    @Transactional
    public CuentaCorriente crearCuotaCuenta(GeneracionDeudaDTO dto, Periodo periodo) {
        return crearCuotaCuenta(periodo, dto.getCategoria(), dto.getIdCuenta(),
                (dto.isSinValuacion() ? null : dto.getValuacion()));
    }

    /*
     * (non-Javadoc)
     * 
     * @see ar.com.jass.catastro.business.DeudaService#crearCuotaCuenta(ar.com.jass.catastro.model.
     * cuentacorriente.Periodo, java.lang.Integer, java.lang.Long, java.math.BigDecimal)
     */
    @Transactional
    public CuentaCorriente crearCuotaCuenta(Periodo periodo, Integer categoria, Long idCuenta, BigDecimal valuacion) {
        LOG.debug("Creando id:{} Val:{} Cat:{} ", idCuenta, valuacion, categoria);

        BigDecimal base = null;
        if (valuacion == null || (valuacion.compareTo(BigDecimal.ZERO) == 0)) {
            LOG.warn("CUENTA SIN VALUACION {}", idCuenta);
            // Busco la cuota 1 del anio
            CuentaCorriente cuotaUNO = ccService.findCuotaByCuentaId(idCuenta, periodo.getAnio(), 1, 0,
                    TipoMovimiento.D);
            if (cuotaUNO == null) {
                throw new IllegalStateException("Cuenta sin VALUACION y sin CUOTA UNO");
            }
            CCDetalle cuotaPuraUNO = cuotaUNO.getCuotaPura();
            base = cuotaPuraUNO.getMonto().multiply(BigDecimal.valueOf(periodo.getPorcentajeAjuste())).setScale(2,
                    RoundingMode.DOWN);
        }

        // Calculo con el template de cuota!!
        CuentaCorriente debito  = templateCuotaService.generarCuota(TipoMovimiento.D, periodo, categoria, valuacion, base);

        // Codigo administrativo de cuota estandar
        //debito.setCodAdministrativo(codAdministrativoService.getCodigoCuota());

        LOG.debug("Cuenta: {} Cuota: {}", idCuenta, debito);
        return debito;
    }

    public List<CuentaCorriente> crearMovimientoAjuste(AjusteDTO ajusteDTO, Periodo vigente) {
        LOG.debug("crear mov ajuste: {}, {}, vigente: {}", ajusteDTO, ajusteDTO.getPeriodo(), vigente);
        List<CuentaCorriente> movimientosAjuste = new ArrayList<CuentaCorriente>();

        /**
         * SI movimiento original es previo al periodo vigente se realiza un ajuste
         */
        CuentaCorriente ajuste = new CuentaCorriente();
        if (ajusteDTO.getPeriodo().esPrevio(vigente)) {
            // Como obtener el # ajuste que se debe utilizar!!!
            Integer nroAjuste = ccService.getNroAjusteSiguiente(ajusteDTO.getCuenta(), ajusteDTO.getPeriodo());

            ajuste.setAjuste(nroAjuste);
            ajuste.setAnio(ajusteDTO.getPeriodo().getAnio());
            ajuste.setCodAdministrativo(ajusteDTO.getCodigoAdministrativo());
            ajuste.setCuenta(ajusteDTO.getCuenta());
            ajuste.setCuota(ajusteDTO.getPeriodo().getCuota());
            ajuste.setFechaVto(ajusteDTO.getFechaVtoAjuste());
            ajuste.setPeriodo(ajusteDTO.getPeriodo());
            ajuste.setFlagAjuste(Boolean.TRUE);
            /*
             * BigDecimal montoAjuste = original.getMontoTotal()
             * .multiply(ajusteDTO.getDiferencial().setScale(2, RoundingMode.DOWN));
             */
            BigDecimal montoAjuste = ajusteDTO.montoAjuste();
            if (montoAjuste.compareTo(BigDecimal.ZERO) < 0) {
                ajuste.setTipo(TipoMovimiento.C);
                montoAjuste = montoAjuste.negate();
            }
            ajuste.addDetalle(
                    new CCDetalleBuilder().monto(montoAjuste).concepto(ConceptoCustom.AJUSTE_LINEA).build());

            // SI el ajuste no genera diferencia entonces no se lo crea
            if (!(ajuste.getMontoTotal().compareTo(BigDecimal.ZERO) == 0)) {
                movimientosAjuste.add(ajuste);
                LOG.debug("creando mov ajuste {} ", ajuste);
            } else {
                LOG.warn("No se crea ajuste, ya que el mismo es de monto 0");
            }
        } else {
            // SI el movimiento es posterior al periodo vigente , se recalcula el movimiento con la
            // nueva valuacion y se desactiva el original
            List<CuentaCorriente> movs = ajusteDTO.getMovimientos();
            for (CuentaCorriente cc : movs) {
                if (cc.esCuotaBase()) {
                    cc.setActivo(Boolean.FALSE);
                    movimientosAjuste.add(cc);
                }
            }

            ajuste = crearCuotaCuenta(ajusteDTO.getPeriodo(), ajusteDTO.getCategoria(), ajusteDTO.getCuenta().getId(),
                    ajusteDTO.getNuevaValuacion());
            // ajuste.setFechaVto(ajusteDTO.getFechaVtoAjustes());
            // Se mantiene la fecha de vto original del movimiento
            ajuste.setFechaVto(ajusteDTO.getPeriodo().getFechaVto());
            ajuste.setCuenta(ajusteDTO.getCuenta());
            ajuste.setCodAdministrativo(ajusteDTO.getCodigoAdministrativo());

            LOG.debug("creando mov ajuste {} ", ajuste);
            movimientosAjuste.add(ajuste);
        }
        return movimientosAjuste;
    }

    @Override
    @Transactional
    public CuentaCorriente aplicarTope(CuentaCorriente deuda, Periodo periodo) {
        // Busco la cuota 1 del anio
        CuentaCorriente cuotaUNO = ccService.findCuotaByCuentaId(deuda.getCuenta().getId(), periodo.getAnio(), 1, 0,
                TipoMovimiento.D);

        CCDetalle cuotaPuraUNO = cuotaUNO.getCuotaPura();
        BigDecimal montoCredito = deuda.getCuotaPura().getMonto()
                .subtract(cuotaPuraUNO.getMonto().multiply(AJUSTE_TOPE).setScale(2, RoundingMode.DOWN));

        // Si el monto es mayor que 0, entonces se crea el movimiento de credito
        if (montoCredito.compareTo(BigDecimal.ZERO) > 0) {
            CuentaCorriente credito = new CuentaCorriente(TipoMovimiento.C, periodo);
            credito.addDetalle(
                    new CCDetalleBuilder().monto(montoCredito).concepto(ConceptoCustom.TOPE).build());

            // credito.setCodAdministrativo(codAdministrativoService.getCodigo(ConceptoCuentaCorriente.TOPE));
            credito.setCuenta(deuda.getCuenta());

            return credito;
        } else {
            return null;
        }
    }

    public CuentaCorrienteService getCcService() {
        return ccService;
    }

    public void setCcService(CuentaCorrienteService ccService) {
        this.ccService = ccService;
    }
}
