package ar.com.jass.catastro.dao.impl;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import ar.com.jass.catastro.dao.GenericDAO;
import ar.com.jass.catastro.model.Cuenta;

@SuppressWarnings("unchecked")
@Repository
public abstract class GenericDAOImpl<E, K extends Serializable> implements GenericDAO<E, K> {

    private static final Logger LOG = LoggerFactory.getLogger("GenericDAOImpl");

    @Autowired
    private SessionFactory sessionFactory;

    protected Class<? extends E> daoType;

    /**
     * By defining this class as abstract, we prevent Spring from creating instance of this class If
     * not defined as abstract, getClass().getGenericSuperClass() would return Object. There would
     * be exception because Object class does not hava constructor with parameters.
     */
    @SuppressWarnings("rawtypes")
    public GenericDAOImpl() {
        Type t = getClass().getGenericSuperclass();
        ParameterizedType pt = (ParameterizedType) t;
        daoType = (Class) pt.getActualTypeArguments()[0];
    }

    protected Session currentSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public void save(E entity) {
        currentSession().save(entity);
    }

    @Override
    public void saveOrUpdate(E entity) {
        currentSession().saveOrUpdate(entity);
    }

    @Override
    public void update(E entity) {
        currentSession().saveOrUpdate(entity);
    }

    @Override
    public void remove(E entity) {
        currentSession().delete(entity);
    }

    @Override
    public E find(Class<E> type, K key) {
        return (E) currentSession().get(type, key);
    }

    @SuppressWarnings("rawtypes")
    @Override
    public List<E> getAll(Class type) {
        return currentSession().createCriteria(type).list();
    }

    /*
     * (non-Javadoc)
     * 
     * @see ar.com.jass.catastro.dao.GenericDAO#findByCuentaDV(java.lang.String, java.lang.String)
     */
    public Cuenta findCuentaByCuentaDV(String cuenta, String dv) {
        LOG.info("findByCuenta");

        Query hql = currentSession().createQuery("from Cuenta where cuenta = :cuenta and dv = :dv");
        hql.setString("cuenta", cuenta);
        hql.setString("dv", dv);

        return (Cuenta) hql.list().get(0);
    }

}
