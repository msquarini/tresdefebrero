package ar.com.jass.catastro.business.deuda.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import ar.com.jass.catastro.business.deuda.ConceptoCuotaDTO;
import ar.com.jass.catastro.model.cuentacorriente.CCDetalle.CCDetalleBuilder;

@Entity
@DiscriminatorValue("mantenimi")
public class ConceptoTasaMantenimiento extends AbstractConceptoCuota {

    @Override
    public void crearConceptoCuota(ConceptoCuotaDTO conceptoCuota) {
        conceptoCuota.addDetalle(new CCDetalleBuilder().monto(getMinValue()).concepto(this).build());
    }

}
