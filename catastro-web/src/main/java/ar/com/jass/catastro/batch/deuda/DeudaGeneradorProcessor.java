package ar.com.jass.catastro.batch.deuda;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;

import ar.com.jass.catastro.business.impl.DeudaGeneratorStrategy;
import ar.com.jass.catastro.model.cuentacorriente.GeneracionDeudaDTO;
import ar.com.jass.catastro.model.cuentacorriente.Periodo;

/**
 * Procesamiento de deuda para la cuenta en el periodo Se calculan los movimientos de DEBITO para
 * cada cuenta
 * 
 * @author mesquarini
 *
 */
public class DeudaGeneradorProcessor implements ItemProcessor<GeneracionDeudaDTO, GeneracionDeudaDTO> {

    private static final Logger LOG = LoggerFactory.getLogger(DeudaGeneradorProcessor.class);

    @Autowired
    private DeudaGeneratorStrategy deudaGenerator;

    private Periodo periodo;

    @BeforeStep
    public void retrieveInterstepData(StepExecution stepExecution) {
        JobExecution jobExecution = stepExecution.getJobExecution();
        ExecutionContext jobContext = jobExecution.getExecutionContext();

        // Id del periodo que ejecuta el JOB
        Long idPeriodo = jobExecution.getJobParameters().getLong("idPeriodo");
        setPeriodo((Periodo) jobContext.get("periodo_" + idPeriodo));
        LOG.info("BS-Processor {} {}", idPeriodo, periodo);
    }

    @Override
    public GeneracionDeudaDTO process(GeneracionDeudaDTO item) throws Exception {

        // LOG.debug("Procesando Generacion Deuda, periodo {}, {}", periodo, item);
        item = deudaGenerator.generarDeuda(periodo, item);

        // FIXME esto tiene que resolverse en otro lado, cuando se genera file para boletas!!!!
        // Actualizo los registros de deuda vencida con el nuevo interes
        // cuentaCorrienteService.actualizarInteresDeudaVencida(periodo, item.getIdCuenta());
        // LOG.debug("Resultado Generacion Deuda, {}", item);
        return item;
    }

    public DeudaGeneratorStrategy getDeudaGenerator() {
        return deudaGenerator;
    }

    public void setDeudaGenerator(DeudaGeneratorStrategy deudaGenerator) {
        this.deudaGenerator = deudaGenerator;
    }

    public Periodo getPeriodo() {
        return periodo;
    }

    public void setPeriodo(Periodo periodo) {
        this.periodo = periodo;
    }

}