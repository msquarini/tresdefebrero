package ar.com.jass.catastro.model.cuentacorriente;

/**
 * Conceptos que se aplican en la generacion de una deuda de cuenta corriente
 * 
 * @author mesquarini
 *
 */
public enum ConceptoCuentaCorriente {

	CUOTA("Cuota"),

	TASA_SEG("Tasa de servicios de seguridad"),

	TASA_MANT("Tasa de manteniento de Centros Culturales y Deportivos"),

	PAGO_ANUAL("Pago anual"),
	PAGO_ANUAL_BONIFICACION("Bonificacion pago anual"),
	
	PAGO_EXISTENTE("Pago existente"),
	
	AJUSTE_LINEA("Ajuste Valuacion"),
    
    TOPE("Ajuste por tope");

	private String nombre;

	ConceptoCuentaCorriente(String nombre) {
		this.nombre = nombre;
	}

	public String getNombre() {
		return nombre;
	}

}
