package ar.com.jass.catastro.business.deuda.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import ar.com.jass.catastro.business.deuda.ConceptoCuotaDTO;
import ar.com.jass.catastro.model.EntityBase;

/**
 * Template de cuota
 * Compuesto por los distintos conceptos y configuraciones
 * 
 * @author msquarini
 *
 */
@Entity
@Table(name = "template_cuota")
public class TemplateCuota extends EntityBase {

	private Long id;
	private String codigo;
	private String descripcion;
	private Date vigenteDesde;
	private Date vigenteHasta;
	
	/**
	 * Conceptos que componen la cuota
	 */
	private List<AbstractConceptoCuota> conceptos;
	

	public TemplateCuota() {
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_templatecuota_gen")
	@SequenceGenerator(name = "seq_templatecuota_gen", sequenceName = "seq_templatecuota", allocationSize = 1)
	@Column(name = "id_template")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "codigo")
	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	@Column(name = "descripcion")
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "vigencia_desde")
	public Date getVigenteDesde() {
		return vigenteDesde;
	}

	public void setVigenteDesde(Date vigenteDesde) {
		this.vigenteDesde = vigenteDesde;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "vigencia_hasta")
	public Date getVigenteHasta() {
		return vigenteHasta;
	}

	public void setVigenteHasta(Date vigenteHasta) {
		this.vigenteHasta = vigenteHasta;
	}

    @Override
    public String toString() {
        return "TemplateCuota [id=" + id + ", codigo=" + codigo + ", descripcion=" + descripcion + ", vigenteDesde="
                + vigenteDesde + ", vigenteHasta=" + vigenteHasta + "]";
    }

    @OneToMany(mappedBy = "template")
    @OrderBy("order_calculo")
    public List<AbstractConceptoCuota> getConceptos() {
        return conceptos;
    }

    public void setConceptos(List<AbstractConceptoCuota> conceptos) {
        this.conceptos = conceptos;
    }

    /**
     * Generar los conceptos de cuota
     * 
     * @param categoria
     * @param valuacion
     * @return
     */
    public void generarCuota(ConceptoCuotaDTO conceptoCuotaDTO) {        
        for (AbstractConceptoCuota concepto : getConceptos()) {                        
            concepto.crearConceptoCuota(conceptoCuotaDTO);
        }        
    }
}
