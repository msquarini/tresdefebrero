package ar.com.jass.catastro.batch.pagos;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import ar.com.jass.catastro.model.pagos.FilePago;

/**
 * Lanzador de JOBs de procesamiento de files de pagos
 * 
 * @author msquarini
 *
 */
@Service("pagosJobLauncher")
public class PagosJobLauncher {

    private static final Logger LOG = LoggerFactory.getLogger("PagosJobLauncher");

    @Autowired
    @Qualifier("jobLauncherAsync")
    private JobLauncher jobLauncher;

    public void launchJob(FilePago pago) {
        try {
            JobParameters param = new JobParametersBuilder().addLong("idFilePago", pago.getId()).toJobParameters();
            JobExecution execution = jobLauncher.run(pago.getOrigen().getJob(), param);
            LOG.info("Proceso de procesamiento de pagos - FilePago {} - Estado {} ", pago, execution.getStatus());
        } catch (Exception e) {
            LOG.error("Error en lanzamiento de procesamiento de file de Pagos {}", pago, e);
        }
    }
}
