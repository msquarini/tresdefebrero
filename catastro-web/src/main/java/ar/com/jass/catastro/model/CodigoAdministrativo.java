package ar.com.jass.catastro.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Codigos administrativos que se pueden asociar a una cuenta
 * 
 * @author msquarini
 *
 */
@Entity
@Table(name = "cod_administrativo")
public class CodigoAdministrativo extends EntityBase {

	private Long id;
	private String codigo;
	private String nombre;
	private Date vigenteDesde;
	private Date vigenteHasta;
	
	/**
	 * Flag para indicar si el codigo de movimiento genera movimientos de ajustes 
	 */
	private Boolean ajuste;

	public CodigoAdministrativo() {
	    ajuste = Boolean.FALSE;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_codadm_gen")
	@SequenceGenerator(name = "seq_codadm_gen", sequenceName = "seq_codadm", allocationSize = 1)
	@Column(name = "id_codadm")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "codigo")
	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	@Column(name = "nombre")
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "vigencia_desde")
	public Date getVigenteDesde() {
		return vigenteDesde;
	}

	public void setVigenteDesde(Date vigenteDesde) {
		this.vigenteDesde = vigenteDesde;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "vigencia_hasta")
	public Date getVigenteHasta() {
		return vigenteHasta;
	}

	public void setVigenteHasta(Date vigenteHasta) {
		this.vigenteHasta = vigenteHasta;
	}

	@Column(name = "ajuste")
    public Boolean getAjuste() {
        return ajuste;
    }

    public void setAjuste(Boolean ajuste) {
        this.ajuste = ajuste;
    }

}
