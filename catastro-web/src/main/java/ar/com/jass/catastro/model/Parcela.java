package ar.com.jass.catastro.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Random;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

@Entity
@Table(name = "Parcela")
public class Parcela extends EntityBase implements Serializable, IParcela {

  private Long id;

  private Boolean activa;

  /**
   * Partida de la parcela Unico
   */
  private Integer partida;

  /**
   * Nomenclatura catastral Unico
   */
  private Nomenclatura nomenclatura;

  /**
   * Cuenta relacionada a la parcela
   */
  private Cuenta cuenta;

  /**
   * Informacion de dominio de la parcela
   */
  private Dominio dominio;

  /**
   * Informacion de linea de valuacion vigente
   */
  private Linea lineaVigente;

  /**
   * Lineas valuatorias de la parcela
   */
  private List<Linea> lineas;

  /**
   * Titulares de la parcela
   */
  private List<TitularParcela> titulares;

  // Se traslada al resposansable de la cuenta
  //private Domicilio domicilioPostal;
  private Domicilio domicilioInmueble;

  private String observacion;

/*  *//**
   * A,B,C,D
   *//*
  @Deprecated
  private String zona;
*/
  /**
   * Indica si la parcela es de dominio municipal
   */
  private Boolean dominioMunicipal;

  /**
   * Destino de la parcela en caso de ser de dominio municipal
   */
  private String destino;
  
  public Parcela() {
    activa = Boolean.FALSE;    
    dominioMunicipal = Boolean.FALSE;
    lineaVigente = new Linea();
    titulares = new ArrayList<TitularParcela>();
  }
  
/*  public Parcela(Parcela origen) {      
      this.nomenclatura = new Nomenclatura(origen.getNomenclatura());
      this.lineas = new ArrayList<>(origen.getLineas());
      this.titulares = new ArrayList<>(origen.getTitulares());
      
      this.activa = origen.getActiva();
      this.dominioMunicipal = origen.getDominioMunicipal();
      this.categoria = origen.getCategoria();
      this.destino = origen.getDestino();
      this.domicilioInmueble = new Domicilio(origen.getDomicilioInmueble());
      //this.domicilioPostal = new Domicilio(origen.getDomicilioPostal());            
  }*/

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_parcela_gen")
  @SequenceGenerator(name = "seq_parcela_gen", sequenceName = "seq_parcela", allocationSize = 1)
  @Column(name = "id_parcela")
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  @Embedded
  public Nomenclatura getNomenclatura() {
    return nomenclatura;
  }

  public void setNomenclatura(Nomenclatura nomenclatura) {
    this.nomenclatura = nomenclatura;
  }

  // @OneToOne(fetch = FetchType.EAGER, optional = false)
  // @JoinColumn(name = "id_cuenta")
  // @Fetch(FetchMode.JOIN)
  // @org.hibernate.annotations.LazyToOne(org.hibernate.annotations.LazyToOneOption.NO_PROXY)
  // @Cascade(CascadeType.SAVE_UPDATE)
  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "id_cuenta", nullable = true)
  @Cascade(CascadeType.ALL)
  public Cuenta getCuenta() {
    return cuenta;
  }

  public void setCuenta(Cuenta cuenta) {
    this.cuenta = cuenta;
  }
  /*
   * @OneToOne(fetch = FetchType.LAZY, optional = true)
   * 
   * @JoinColumn(name = "id_cuenta_origen")
   * 
   * @Cascade(CascadeType.SAVE_UPDATE) public Cuenta getCuentaOrigen() { return cuentaOrigen; }
   * 
   * public void setCuentaOrigen(Cuenta cuentaOrigen) { this.cuentaOrigen = cuentaOrigen; }
   */

  @Column(name = "partida")
  public Integer getPartida() {
    return partida;
  }

  public void setPartida(Integer partida) {
    this.partida = partida;
  }

  //@ManyToMany(fetch = FetchType.LAZY, mappedBy = "parcelas")
  //@Cascade(CascadeType.SAVE_UPDATE)  
  @OneToMany(mappedBy = "parcela")
  public List<TitularParcela> getTitulares() {
    return titulares;
  }

  public void setTitulares(List<TitularParcela> titulares) {
    this.titulares = titulares;
  }

//  @OneToOne(fetch = FetchType.EAGER)
//  @JoinColumn(name = "id_parcela", referencedColumnName="id_parcela")
//  @Cascade(CascadeType.SAVE_UPDATE)
  @Embedded
  public Dominio getDominio() {
    return dominio;
  }

  public void setDominio(Dominio dominio) {
    this.dominio = dominio;
  }

  @OneToMany(fetch = FetchType.LAZY) //, orphanRemoval = true)
  @Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE})
  @JoinTable(name = "parcela_linea", joinColumns = @JoinColumn(name = "id_parcela"),
      inverseJoinColumns = @JoinColumn(name = "id_linea"))
  public List<Linea> getLineas() {
    return lineas;
  }

  public void setLineas(List<Linea> lineas) {
    this.lineas = lineas;
  }

  @OneToOne(fetch = FetchType.LAZY, optional = false)
  @JoinColumn(name = "id_domicilio_inmueble")
  @Cascade({org.hibernate.annotations.CascadeType.ALL})
  public Domicilio getDomicilioInmueble() {
    return domicilioInmueble;
  }

  public void setDomicilioInmueble(Domicilio domicilioInmueble) {
    this.domicilioInmueble = domicilioInmueble;
  }

  @Column(name = "observacion")
  public String getObservacion() {
    return observacion;
  }

  public void setObservacion(String observacion) {
    this.observacion = observacion;
  }

  public String toString() {
    return "Parcela id[" + getId() + "] Partida: [" + getPartida() + "] " + getNomenclatura()
        + " - " + getDominio();
    // + getDomicilioInmueble() + "\n" + getDomicilioPostal();
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.id, this.partida, this.nomenclatura);
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }
/*    if (!(obj instanceof Parcela)) {
        return false;
    }
*/
    if (!getClass().isAssignableFrom(obj.getClass()))
        return false;
    
    final Parcela other = (Parcela) obj;
    return Objects.equals(this.id, other.id) && Objects.equals(this.partida, other.partida)
        && Objects.equals(this.nomenclatura, other.nomenclatura);
  }

  // @OneToOne(mappedBy = "parcela", fetch = FetchType.LAZY)
  @OneToOne()
  @JoinColumn(name = "id_linea")
  @Cascade(CascadeType.ALL)
  public Linea getLineaVigente() {
    return lineaVigente;
  }

  public void setLineaVigente(Linea lineaVigente) {
    this.lineaVigente = lineaVigente;
  }

  @Transient
  public Integer getCategoria() {
    return getLineaVigente().getCategoria();
  }

  @Column(name = "activa")
  public Boolean getActiva() {
    return activa;
  }

  public void setActiva(Boolean activa) {
    this.activa = activa;
  }

  @Column(name = "dom_municipal")
  public Boolean getDominioMunicipal() {
    return dominioMunicipal;
  }

  public void setDominioMunicipal(Boolean dominioMunicipal) {
    this.dominioMunicipal = dominioMunicipal;
  }

  @Column(name = "destino")
  public String getDestino() {
    return destino;
  }

  public void setDestino(String destino) {
    this.destino = destino;
  }
}
