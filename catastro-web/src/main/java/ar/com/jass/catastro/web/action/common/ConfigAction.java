package ar.com.jass.catastro.web.action.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import ar.com.jass.catastro.business.TasaService;
import ar.com.jass.catastro.model.error.ErrorLog;
import ar.com.jass.catastro.web.action.BaseAction;

/**
 * Configuracion 
 * 
 * 
 * @author msquarini
 *
 */
public class ConfigAction extends BaseAction {

    private static final long serialVersionUID = 7353477345330099549L;

    private static final Logger LOG = LoggerFactory.getLogger(ConfigAction.class);

    @Autowired
    private TasaService tasaService;

    /*
     * (non-Javadoc)
     * 
     * @see com.opensymphony.xwork2.ActionSupport#input()
     */
    public String input() {
        return "input";
    }

    /**
     * Actualizar codigo administrativo
     * 
     * @return
     */
    public String update() {
        LOG.info("updating configuracion");
        clearMessages();
        try {
            tasaService.reload();
            setTituloError("Configuracion");
            addSuccessMessage("Actualizacion correcta");
        } catch (Exception e) {
            ErrorLog log = crearErrorLog("updating config", e.getMessage());
            LOG.error("{}", log, e);
            addActionError("error_update_config");
            return "errorJSON";
        }
        return "okJSON";
    }

}