package ar.com.jass.catastro.dao.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import ar.com.jass.catastro.dao.ParcelaDAO;
import ar.com.jass.catastro.model.Nomenclatura;
import ar.com.jass.catastro.model.Parcela;
import ar.com.jass.catastro.model.Titular;

@Repository("parcelaDAODummy")
public class ParcelaDAODummy extends GenericDAODummy<Parcela, Serializable>
		implements ParcelaDAO {

	private static final Logger LOG = LoggerFactory.getLogger("ParcelaDAODummy");

	@SuppressWarnings("unchecked")
	@Override
	public List<Parcela> findByNomenclatura(Nomenclatura nomenclatura) {

		LOG.info("findByNomenclatura {}", nomenclatura);
		
		List<Parcela> ps = new ArrayList<Parcela>();
		
		for (int x = 0; x < (int)(Math.random() * 15); x++) {
			ps.add(createParcela(x));
		}
		return ps;
	}

	@Override
	public List<Parcela> findByTitular(Titular titular) {
		List<Parcela> ps = new ArrayList<Parcela>();
		
		for (int x = 0; x < (int)(Math.random() * 15); x++) {
			ps.add(createParcela(x));
		}
		
		return ps;
	}
	
	@Override
	public List<Parcela> findByPartida(Integer partida) {
		List<Parcela> ps = new ArrayList<Parcela>();
		
		for (int x = 0; x < (int)(Math.random() * 15); x++) {
			ps.add(createParcela(x));
		}
		
		return ps;
	}
	
	@Override
	public List<Parcela> findByCuenta(String cuenta, String dv) {
		List<Parcela> ps = new ArrayList<Parcela>();	
		ps.add(createParcela(1));		
		return ps;
	}

	@Override
	public void save(Parcela entity) {

		
	}

	@Override
	public void saveOrUpdate(Parcela entity) {
		
	}

	@Override
	public void update(Parcela entity) {
		
	}

	@Override
	public void remove(Parcela entity) {
		
	}

	@Override
	public Parcela find(Class type, Serializable key) {
		return null;
	}

	@Override
	public List<Parcela> getAll(Class type) {
		return null;
	}
}
