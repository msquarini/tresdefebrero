package ar.com.jass.catastro.batch.da;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

import ar.com.jass.catastro.dao.GeneralDAO;
import ar.com.jass.catastro.model.Cuenta;
import ar.com.jass.catastro.model.da.DADetail;
import ar.com.jass.catastro.model.da.DANovedad;
import ar.com.jass.catastro.model.da.DebitoAutomatico;

/**
 * Genera files de deudas para las adhesiones a DA registradas en el sistema Se
 * genera un file por cada banco que presente adhesiones respetando el formato
 * de cada uno
 * 
 * @author msquarini
 *
 */
public class DAOutputWriter implements ItemWriter<InformeDebitoAutomatico> {

	private static final Logger LOG = LoggerFactory.getLogger("DAOutputWriter");

	@Autowired
	private GeneralDAO generalDAO;


	private Map<String, ItemWriter> outputs;
	
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.batch.item.ItemWriter#write(java.util.List)
	 */
	@Override
	public void write(List<? extends InformeDebitoAutomatico> items) throws Exception {
	
		for (InformeDebitoAutomatico dad : items) {
			
			
		}
	}

	public GeneralDAO getGeneralDAO() {
		return generalDAO;
	}

	public void setGeneralDAO(GeneralDAO generalDAO) {
		this.generalDAO = generalDAO;
	}

	public Map<String, ItemWriter> getOutputs() {
		return outputs;
	}

	public void setOutputs(Map<String, ItemWriter> outputs) {
		this.outputs = outputs;
	}

}
