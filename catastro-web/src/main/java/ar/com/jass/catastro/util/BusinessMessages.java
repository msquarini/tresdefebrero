package ar.com.jass.catastro.util;

/**
 * Constantes de mensajes en la web
 * 
 * @author msquarini
 *
 */
public interface BusinessMessages {

	String UNKNOWN_ERROR = "unknown_error";
	String TITULAR_SINCUIT = "titular_sin_cuit";

	String TITULARIDAD_PORCENTAJE = "titularidad_100";
	
	String SIN_RESPONSABLE_VIGENTE = "sin_responsable_vigente";
	
	String PARCELA_EN_ORIGENES = "parcela_en_origenes";
}
