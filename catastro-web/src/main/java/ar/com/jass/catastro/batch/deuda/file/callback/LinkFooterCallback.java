package ar.com.jass.catastro.batch.deuda.file.callback;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.file.FlatFileFooterCallback;
import org.springframework.beans.factory.annotation.Value;

import ar.com.jass.catastro.batch.deuda.file.DeudaDTO;
import ar.com.jass.catastro.business.impl.FileType;

/**
 * Footer para archivos a Link
 * 
 * @author mesquarini
 *
 */
public class LinkFooterCallback implements FlatFileFooterCallback, DeudaFileCallback {

    private static final Logger LOG = LoggerFactory.getLogger("LinkFooterCallback");
    
    private static final String CONTROL_HEADER= "HRPASCTRL%1$tY%1$tm%1$tdAPX%2$8s%3$010d%4$37s";
    private static final String CONTROL_DATOS= "LOTES00001%1$08d%2$016d%3$02d%2$016d%3$02d%4$018d   ";
    private static final String CONTROL_TRAILER = "FINAL%1$08d%2$016d%3$02d%2$016d%3$02d%4$018d%5$tY%5$tm%5$td";
    
    private static final String TRAILER = "TRFACTURACION%1$08d";
    private static final int FILLER = 56;

    @Value("${deuda.toBank.path}")
    private String deudaFilePath;
    
    private Date fechaVto;
    private Integer registros = 0;
    private BigDecimal total = BigDecimal.ZERO;

    @Override
    public void writeFooter(Writer writer) throws IOException {
        Integer parteEntero = Integer.valueOf(total.intValue());
        BigDecimal centavos = total.subtract(total.setScale(0, RoundingMode.FLOOR)).movePointRight(2);
        Integer parteDecimal = Integer.valueOf(centavos.intValue());

        String temp = String.format(TRAILER, registros + 2) + String.format("%1$016d%2$02d", parteEntero, parteDecimal)
                + String.format("%1$016d%2$02d", parteEntero, parteDecimal) // mismo total de
                                                                            // importes
                + String.format("%0" + 18 + "d", 0) // total 3rd vencimiento
                + String.format("%" + FILLER + "s", " "); // FILLER
        writer.write(temp);

        createControlFile();
    }

    private void createControlFile() throws IOException {
        LOG.info("Creando CONTROL FILE: {}", FileType.LINK.getControlFileName());
        
        String controlFilePath = deudaFilePath + File.separator + FileType.LINK.getControlFileName();
        
        Integer parteEntero = Integer.valueOf(total.intValue());
        BigDecimal centavos = total.subtract(total.setScale(0, RoundingMode.FLOOR)).movePointRight(2);
        Integer parteDecimal = Integer.valueOf(centavos.intValue());
        
        Writer w = new BufferedWriter(new FileWriter(controlFilePath));

        String header = String.format(CONTROL_HEADER, new Date(), FileType.LINK.getDeudaFileName(), registros * 131, " ");
        w.write(header);
        w.append(System.getProperty("line.separator"));
        String datos = String.format(CONTROL_DATOS, registros, parteEntero, parteDecimal, 0);
        w.write(datos);
        w.append(System.getProperty("line.separator"));
        String trailer = String.format(CONTROL_TRAILER, registros, parteEntero, parteDecimal, 0, fechaVto);
        w.write(trailer);
        w.close();
    }

    @Override
    public void notify(List<DeudaDTO> deudas) {
        for (DeudaDTO deuda : deudas) {
            registros++;
            total = total.add(deuda.getImporte());
            if (fechaVto == null) {
                fechaVto = deuda.getFechaVto();
            }
        }
    }
}
