package ar.com.jass.catastro.model.pagos.link;

import java.util.Date;

import ar.com.jass.catastro.model.pagos.FilePago;
import ar.com.jass.catastro.model.pagos.FilePagosRecord;
import ar.com.jass.catastro.model.pagos.PagoHeaderRecord;

/**
 * Header de file de pagos de Link
 * @author msquarini
 *
 */
public class LinkHeader extends PagoHeaderRecord implements FilePagosRecord {

	private Date fecha;

	@Override
	public void complete(FilePago filePago) {
		filePago.setFileHeader(getLine());
		filePago.setFechaFile(this.fecha);		
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

}
