package ar.com.jass.catastro.exceptions;

public class FilePagoTypeUnknownException extends BusinessException {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public FilePagoTypeUnknownException(String message) {
        super(message);
    }

}
