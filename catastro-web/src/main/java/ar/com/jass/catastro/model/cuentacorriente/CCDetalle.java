package ar.com.jass.catastro.model.cuentacorriente;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import ar.com.jass.catastro.business.deuda.model.AbstractConceptoCuota;
import ar.com.jass.catastro.model.EntityBase;

/**
 * Descripcion del detalle que se compone el movimiento de cuenta corriente
 * 
 * @author msquarini
 *
 */
@Entity
@Table(name = "CC_Detalle")
public class CCDetalle extends EntityBase implements Serializable{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private Long id;

    private String concepto;
    
    private String detalle;

    private BigDecimal monto;
    
    private CCDetalle() {
        
    }
    
    private CCDetalle(CCDetalleBuilder builder) {
        this.detalle = builder.detalle;
        this.monto = builder.monto;
        this.concepto = builder.concepto;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_cc_detalle_gen")
    @SequenceGenerator(name = "seq_cc_detalle_gen", sequenceName = "seq_cc_detalle", allocationSize = 1)
    @Column(name = "id_cc_detalle")
    public Long getId() {
        return id;
    }

    @Column(name = "detalle")
    public String getDetalle() {
        return detalle;
    }

    @Column(name = "monto")
    public BigDecimal getMonto() {
        return monto;
    }


    @Column(name = "concepto")
    public String getConcepto() {
        return concepto;
    }

    public static class CCDetalleBuilder {
        private BigDecimal monto;
        private String detalle;
        private String concepto;
        
        public CCDetalleBuilder monto(BigDecimal monto) {
            this.monto = monto;
            return this;
        }
        
        public CCDetalleBuilder concepto(AbstractConceptoCuota concepto) {
            this.concepto = concepto.getCodigo();
            this.detalle = concepto.getDescription();
            return this;
        }
                
        public CCDetalle build() {
            return new CCDetalle(this);
        }
    }

    public void setId(Long id) {
        this.id = id;
    }

    private void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    private void setMonto(BigDecimal monto) {
        this.monto = monto;
    }

    private void setConcepto(String concepto) {
        this.concepto = concepto;
    }
    
    /**
     * Copiar el detalle, con otro monto
     * @param importe
     * @return
     */
    public CCDetalle copyDetalle(BigDecimal importe) {
        CCDetalle copy = new CCDetalle();
        copy.setDetalle(getDetalle());
        copy.setMonto(importe);
        copy.setConcepto(getConcepto());
        return copy;
    }
}
