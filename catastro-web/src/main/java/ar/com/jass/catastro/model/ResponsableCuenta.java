package ar.com.jass.catastro.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Responsabilidad sobre la cuenta. Fecha desde y hasta
 * 
 * @author MESquarini
 * 
 */

@Entity
@Table(name = "Responsable_Cuenta")
public class ResponsableCuenta extends EntityBase implements Editable {

    /*private UUID respUUID = UUID.randomUUID();*/
    
	private Long id;

	private Cuenta cuenta;
	private Titular titular;

	private Date desde;
	private Date hasta;

	public ResponsableCuenta() {
	}

	public ResponsableCuenta(Cuenta cuenta, Titular titular, Date desde, Date hasta) {
		this.cuenta = cuenta;
		this.titular = titular;
		this.desde = desde;
		this.hasta = hasta;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_responsable_parcela_gen")
	@SequenceGenerator(name = "seq_responsable_parcela_gen", sequenceName = "seq_responsable_parcela", allocationSize = 1)
	@Column(name = "id_responsable_cuenta")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	//@ManyToOne(cascade = CascadeType.ALL)
	@ManyToOne()
	@JoinColumn(name = "id_cuenta")
	public Cuenta getCuenta() {
		return cuenta;
	}

	public void setCuenta(Cuenta cuenta) {
		this.cuenta = cuenta;
	}

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id_titular")
	public Titular getTitular() {
		return titular;
	}

	public void setTitular(Titular titular) {
		this.titular = titular;
	}

	@Column(name = "desde")
	@Temporal(TemporalType.DATE)
	public Date getDesde() {
		return desde;
	}

	public void setDesde(Date desde) {
		this.desde = desde;
	}

	@Column(name = "hasta")
	@Temporal(TemporalType.DATE)
	public Date getHasta() {
		return hasta;
	}

	public void setHasta(Date hasta) {
		this.hasta = hasta;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((desde == null) ? 0 : desde.hashCode());
		result = prime * result + ((hasta == null) ? 0 : hasta.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((cuenta == null) ? 0 : cuenta.hashCode());
		result = prime * result + ((titular == null) ? 0 : titular.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ResponsableCuenta other = (ResponsableCuenta) obj;
		if (desde == null) {
			if (other.desde != null)
				return false;
		} else if (!desde.equals(other.desde))
			return false;
		if (hasta == null) {
			if (other.hasta != null)
				return false;
		} else if (!hasta.equals(other.hasta))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (cuenta == null) {
			if (other.cuenta != null)
				return false;
		} else if (!cuenta.equals(other.cuenta))
			return false;
		if (titular == null) {
			if (other.titular != null)
				return false;
		} else if (!titular.equals(other.titular))
			return false;
		return true;
	}

    @Override
    public String toString() {
        return "ResponsableCuenta [id=" + id + ", cuenta=" + cuenta + ", titular=" + titular + ", desde=" + desde
                + ", hasta=" + hasta + "]";
    }

/*    @Override
    @Transient
    public UUID getUuid() {
        return respUUID;
    }

    @Transient
    public String getUuidString() {       
        return respUUID.toString();
    }*/
}