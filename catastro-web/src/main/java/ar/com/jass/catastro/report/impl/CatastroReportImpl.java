package ar.com.jass.catastro.report.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import ar.com.jass.catastro.exceptions.BusinessException;
import ar.com.jass.catastro.report.impl.ReportsHelper.ReportFormat;
import ar.com.jass.catastro.web.dto.ReportDTO;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;

/**
 * Generaror de reportes 
 * 
 * @author msquarini
 *
 */
public class CatastroReportImpl {

    private ReportsHelper reportsHelper;

    private String createUniqueFilename(String reportname) {
        return reportname.concat(UUID.randomUUID().toString());
    }

    private File exportReport(JasperPrint jasperPrint, String fileName, ReportFormat format)
            throws IOException, JRException {
        File outputFile = File.createTempFile(fileName, format.getExtension());
        JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(outputFile));
        return outputFile;
    }

    private ReportDTO createReport(ReportFormat format, String reportName, Map<String, Object> parameters,
            JRDataSource datasource) throws JRException, IOException {
        JasperReport report = reportsHelper.getReport(reportName);
        JasperPrint jasperPrint = JasperFillManager.fillReport(report, parameters, datasource);

        String filename = createUniqueFilename(reportName);
        return new ReportDTO(format.name(), reportName, filename, exportReport(jasperPrint, filename, format));
    }

    /**
     * Metodos de creacion de reportes !!!
     * 
     * @param format
     * @return
     */
    public ReportDTO reportA(ReportFormat format) throws BusinessException {
        Map<String, Object> params = new HashMap<String, Object>();
        // params.put("bean", bean);
        JRDataSource datasource = new JREmptyDataSource();

        try {
            return createReport(format, "XXXX", params, datasource);
        } catch (Exception e) {
            throw new BusinessException("error generando reporte", e);
        }
    }

}
