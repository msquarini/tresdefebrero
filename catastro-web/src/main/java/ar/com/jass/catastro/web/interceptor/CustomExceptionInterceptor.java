package ar.com.jass.catastro.web.interceptor;

import java.util.ArrayList;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.AccessDeniedException;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.Interceptor;
import com.opensymphony.xwork2.util.ValueStack;

public class CustomExceptionInterceptor implements Interceptor {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private static final Logger LOG = LoggerFactory.getLogger("CustomExceptionInterceptor");

    @Override
    public String intercept(ActionInvocation invocation) throws Exception {
        String result = null;
        try {
            result = invocation.invoke();
        } catch (AccessDeniedException ade) {
            LOG.error("Acceso denegado", ade);
            ValueStack vs = invocation.getStack();

            // Agregado de actionErrores!!!!
            /*ArrayList<String> actionErrors = new ArrayList<String>();
            actionErrors.add("ae1");
            vs.setValue("actionErrors", actionErrors);*/
            vs.setValue("tituloError", "titulo.acceso.denegado");
            vs.setValue("mensajeError", "mensaje.acceso.denegado");
            vs.setValue("status", "fail");
            result = "errorJSON";
            
        } catch (Exception e) {
            LOG.error("Error no controlado", e);
            ValueStack vs = invocation.getStack();
            if (e.getMessage() == null) {
                vs.setValue("mensajeError", "Internal system faliure.Please contact system administrator.");
            } else {
                vs.setValue("mensajeError", e.getMessage());
            }

            vs.setValue("status", "fail");
            result = "errorJSON";
        }

        return result;
    }

    @Override
    public void destroy() {
    }

    @Override
    public void init() {
        ActionContext.getContext().setLocale(Locale.ITALY);
        
        /**
         * Seteo el locale en argentina, para poder utilizar el formato con seperacion de decimales
         * por ,
         */
        /*
         * locale = new Locale("es", "AR");
         * 
         * Locale.setDefault(locale); ActionContext.getContext().setLocale(locale);
         */ }
}
