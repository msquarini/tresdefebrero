package ar.com.jass.catastro.model.pagos;

import java.math.BigDecimal;
import java.util.Date;

public class PagoDetailRecord implements FilePagosRecord {

    String line;

    String cuenta;
    String dv;
    BigDecimal importe;
    Integer cuota;
    Integer anio;
    Date fechaPago;
    
    /**
     * Opcional
     */
    Date fechaVto = null;

    @Override
    public void complete(FilePago filePago) {
        Pago p = new Pago();
        p.setFilePago(filePago);

        p.setFileDetail(getLine());
        p.setAnio(this.anio);
        p.setCuota(this.cuota);
        p.setCuenta(this.cuenta);
        p.setDv(this.dv);
        p.setImporte(this.importe);
        p.setFechaPago(this.fechaPago);        
        p.setFechaVto(this.fechaVto);

        filePago.addPago(p);
    }

    @Override
    public boolean isHeader() {
        return false;
    }

    @Override
    public boolean isDetail() {
        return true;
    }

    @Override
    public boolean isTrailer() {
        return false;
    }

    public String getLine() {
        return line;
    }

    public void setLine(String line) {
        this.line = line;
    }

    public String toString() {
        StringBuffer sb = new StringBuffer(getClass().getName());
        sb.append(" - line: ").append(getLine());
        return sb.toString();
    }

    public String getCuenta() {
        return cuenta;
    }

    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }

    public String getDv() {
        return dv;
    }

    public void setDv(String dv) {
        this.dv = dv;
    }

    public BigDecimal getImporte() {
        return importe;
    }

    public void setImporte(BigDecimal importe) {
        this.importe = importe;
    }

    public Integer getCuota() {
        return cuota;
    }

    public void setCuota(Integer cuota) {
        this.cuota = cuota;
    }

    public Integer getAnio() {
        return anio;
    }

    public void setAnio(Integer anio) {
        this.anio = anio;
    }

    public Date getFechaVto() {
        return fechaVto;
    }

    public void setFechaVto(Date fechaVto) {
        this.fechaVto = fechaVto;
    }

    public Date getFechaPago() {
        return fechaPago;
    }

    public void setFechaPago(Date fechaPago) {
        this.fechaPago = fechaPago;
    }

}