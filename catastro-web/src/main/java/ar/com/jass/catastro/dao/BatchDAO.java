package ar.com.jass.catastro.dao;

public interface BatchDAO {

	/**
	 * Chequea que no exista un file procesado con mismo hash de archivo
	 * 
	 * Evitar procesar archivos duplicados
	 * 
	 * @param checksum
	 * @return
	 */
	public boolean checkUniquePago(String checksum);

	// public void initialize(Object o);
}