package ar.com.jass.catastro.model;

/**
 * Estado de tramites
 * 
 * @author msquarini
 *
 */
public enum EstadoTramite {

    PENDIENTE, PROCESO, FINALIZADO, ERROR, CANCELADO;

    public boolean isFinalizado() {
        if (this.equals(FINALIZADO) || this.equals(CANCELADO)) {
            return true;
        }
        return false;
    }
}
