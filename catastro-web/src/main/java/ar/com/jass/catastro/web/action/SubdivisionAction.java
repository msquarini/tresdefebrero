package ar.com.jass.catastro.web.action;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;

import ar.com.jass.catastro.business.BusinessHelper;
import ar.com.jass.catastro.business.CatastroService;
import ar.com.jass.catastro.exceptions.BusinessException;
import ar.com.jass.catastro.model.Cuenta;
import ar.com.jass.catastro.model.Domicilio;
import ar.com.jass.catastro.model.Nomenclatura;
import ar.com.jass.catastro.model.Parcela;
import ar.com.jass.catastro.model.ResponsableCuenta;
import ar.com.jass.catastro.model.TipoTramite;
import ar.com.jass.catastro.model.TitularParcela;
import ar.com.jass.catastro.model.Tramite;
import ar.com.jass.catastro.model.error.ErrorLog;
import ar.com.jass.catastro.web.commons.WebConstants;

/**
 * Action de gestion de SubDivision de parcela
 * 
 * @author msquarini
 *
 */
public class SubdivisionAction extends BaseAction {

    private static final long serialVersionUID = 7353477345330099548L;

    private static final Logger LOG = LoggerFactory.getLogger(SubdivisionAction.class);

    private static final String EDIT_PARCELA = "edit_parcela";

    @Autowired
    private CatastroService catastroService;

    @Autowired
    private BusinessHelper businessHelper;

    /**
     * Datos de nueva parcela (formulario)
     */
    private Parcela nuevaParcela;

    /**
     * Indentificador de parcela para edicion
     */
    //private Integer partida;
    private String uuid;

    /**
     * Identificar de cuenta origen a subdividir
     */
    private Long idCuentaOrigen;

    /**
     * Cuenta origen
     */
    private Cuenta cuentaOrigen;

    private Integer tipoSubdivision;

    private Tramite tramite;

    /**
     * Procesamiento de Subdivision Tomando datos de la cuenta origen, datos de subdivision y nuevas
     * parcelas, se procede a realizar logica de negocio
     * 
     * @return
     */
    @SuppressWarnings("unchecked")
    @PreAuthorize("hasAuthority('catastro_supervisor')")
    public String process() {
        try {
            clearMessages();
            
            tramite = actualizarTramite(tramite);
            if (tramite == null) {
                throw new IllegalStateException();
            }
            cuentaOrigen = (Cuenta) getFromSession(WebConstants.CUENTA_ORIGEN);

            LOG.info("Procesando Subdivision Cuenta: {}, Tramite: {}", cuentaOrigen, tramite);

            /**
             * Si no es el tramite por (6) Plano sin apertura en ARBA Validamos que no exista deuda
             * en la cuenta origen
             */
            if (!tramite.getOficio()) {
            //if (!tramite.getCodigo().equals(6)) {
                if (catastroService.presentaDeuda(cuentaOrigen)) {
                    LOG.warn("Cuenta presenta deuda: {}", cuentaOrigen);
                    addActionError("Cuenta presenta deuda");
                    return "process_error";
                }
            }

            List<TitularParcela> titulares = new ArrayList<>(
                    (Set<TitularParcela>) getFromSession(WebConstants.TITULARES_ADD));
            // Chequear que los Titulares presenten CUIT/CUIL cargado/actualizado
            businessHelper.checkTitularesConCuit(titulares);

            List<Parcela> nuevasParcelas = (List<Parcela>) getFromSession(WebConstants.PARCELAS_DATA);

            List<ResponsableCuenta> responsables = new ArrayList<>(
                    (Set<ResponsableCuenta>) getFromSession(WebConstants.RESPONSABLES_ADD));

            /**
             * Llamado a logica de negocio de subdivision
             */
            if (nuevasParcelas.size() > 1) {
                tramite = catastroService.grabarTramite(tramite, nuevasParcelas, titulares, responsables, Boolean.TRUE);
            } else {
                addActionError("Deben existir al menos 2 nuevas parcelas para procesar la subdivision");
                return "process_error";
            }

            // Solo para testing de resultado
            // tramite = consultaService.findTramite(40L, "cuentas.parcelas");
            removeFromSession(WebConstants.TRAMITE);
            
            addActionMessage(getText("tramite.process.success"));
        } catch (BusinessException be) {
            ErrorLog log = crearErrorLog("process", be.getMessage());
            LOG.error("error de negocio en procesamiento de subdivision, {}", log, be);
            addActionError(getText(be.getMessage()));
            return "process_error";
        } catch (Exception e) {
            ErrorLog log = crearErrorLog("process", e.getMessage());
            LOG.error("error en procesamiento de subdivision, {}", log, e);
            addActionError(getText("error.unknown"));
            return "process_error";
        }
        return "success";
    }

    /**
     * Ingreso a la pantalla de subdivision. Se presenta formulario de busqueda de cuenta
     * 
     */
    @PreAuthorize("hasAuthority('catastro_operador')")
    public String input() {
        LOG.info("Ingreso a subdivision de parcela");

        initCustomData();
        return "input";
    }

    /**
     * Busqueda de la cuenta origen a partir de ID de la cuenta seleccionada. Se redirije a la
     * pantalla principal de subdivision para la carga de datos generales.
     * 
     * @return
     */
    public String init() {
        LOG.info("Inicio subdivision Cuenta ID: {}", idCuentaOrigen);

        // Busqueda de cuenta origen a partir de ID
        cuentaOrigen = getConsultaService().findCuentaById(idCuentaOrigen, "responsables", "parcela.titulares",
                "parcela.domicilioInmueble");

        if (cuentaOrigen == null) {
            LOG.warn("No se encontro cuenta ID: {}", idCuentaOrigen);
            addActionError(getText("error.cuenta.notfound"));
            return "input";
        }

        if (!cuentaOrigen.getActiva()) {
            LOG.warn("Cuenta no activa: {}", cuentaOrigen);
            addActionError(getText("error.cuenta.inactiva"));
            return "errorJSON";
        }
        if (cuentaOrigen.isBloqueada()) {
            LOG.warn("Cuenta bloqueada: {}", cuentaOrigen);
            addActionError(getText("error.cuenta.bloqueada"));
            return "errorJSON";
        }
        if (cuentaOrigen.getParcela().getNomenclatura().isUF()) {
            LOG.warn("Parcela es UF: {}", cuentaOrigen);
            addActionError(getText("error.subdivision.uf"));
            return "errorJSON";
        }

        // Validacion si presenta cuenta y pasar la decision de continuar al operador
        String hashinsession = (String) getFromSession("hash");
        if (hashinsession != null && hashinsession.equals(hash)) {
            LOG.info("Cuenta presenta deuda, el operador decide continuar con el trámite, {}", cuentaOrigen);
            setInSession(WebConstants.CUENTA_CON_DEUDA, Boolean.TRUE);
            removeFromSession("hash");
        } else {
            setInSession(WebConstants.CUENTA_CON_DEUDA, Boolean.FALSE);
            if (catastroService.presentaDeuda(cuentaOrigen)) {
                hash = UUID.randomUUID().toString();
                setInSession("hash", hash);
                LOG.warn("Cuenta presenta deuda: {}", cuentaOrigen);
                addActionError("Cuenta presenta deuda");
                return "errorDeuda";
            }
        }

        // Cuenta para subdividir
        setInSession(WebConstants.CUENTA_ORIGEN, cuentaOrigen);
        // Titulares en session para completar CUITCUIL
        setInSession(WebConstants.TITULARES_ADD, new HashSet<TitularParcela>(cuentaOrigen.getParcela().getTitulares()));
        setInSession(WebConstants.RESPONSABLES_ADD, new HashSet<ResponsableCuenta>(cuentaOrigen.getResponsables()));

        // Creo la lista con las nuevas parcelas a crear (partido,Parcela)
        setInSession(WebConstants.PARCELAS_DATA, new ArrayList<Parcela>());

        /*
         * 20180220 Prueba CREAMOS el tramite y lo mantemos en sesion
         */
        tramite = new Tramite(TipoTramite.SUB, new ArrayList<Cuenta>(), Arrays.asList(cuentaOrigen));
        try {
            tramite = catastroService.grabarTramite(tramite, new ArrayList<Parcela>(), null, null, Boolean.FALSE);
        } catch (Exception e) {
            ErrorLog log = crearErrorLog("init", e.getMessage());
            LOG.error("{}", log, e);
            return "process_error";
        }
        setInSession(WebConstants.TRAMITE, tramite);
        return "inprocess";
    }

    /**
     * Grabar el tramite para un posterior tratamiento (Sin procesar!!!)
     * 
     * @return
     */
    @SuppressWarnings("unchecked")
    public String saveTramite() {
        LOG.info("Grabando tramite de subdivision");

        try {
            cuentaOrigen = (Cuenta) getFromSession(WebConstants.CUENTA_ORIGEN);

            /**
             * Si no es el tramite por (6) Plano sin apertura en ARBA Validamos que no exista deuda
             * en la cuenta origen (AVISO para evitar avanzar y al momento de procesar darse cuenta
             * de la deuda)
             */
/*            if (!tramite.getCodigo().equals(6)) {
                if (catastroService.presentaDeuda(cuentaOrigen)) {
                    LOG.warn("Cuenta presenta deuda: {}", cuentaOrigen);
                    addActionError("Cuenta presenta deuda");
                    return "process_error";
                }
            }*/

            List<Parcela> nuevasParcelas = (List<Parcela>) getFromSession(WebConstants.PARCELAS_DATA);
            List<TitularParcela> titulares = new ArrayList<>(
                    (Set<TitularParcela>) getFromSession(WebConstants.TITULARES_ADD));
            List<ResponsableCuenta> responsables = new ArrayList<>(
                    (Set<ResponsableCuenta>) getFromSession(WebConstants.RESPONSABLES_ADD));

            Tramite t = actualizarTramite(tramite);            
            tramite = catastroService.grabarTramite(t, nuevasParcelas, titulares, responsables, Boolean.FALSE);
            
            setInSession(WebConstants.TRAMITE, tramite);
            setInSession(WebConstants.PARCELAS_DATA, tramite.getParcelas());

            addActionMessage(getText("tramite.save.success"));
            return "success";
        } catch (Exception e) {
            ErrorLog log = crearErrorLog("saveTramite", e.getMessage());
            LOG.error("{}", log, e);
            addActionError(getText(e.getMessage()));
            return "process_error";
        }
    }

    /**
     * Continuar la gestion de subdivision desde un tramite previo
     * 
     * @return
     */
    public String initFromTramite() {
        tramite = (Tramite) getFromSession(WebConstants.TRAMITE);
        cuentaOrigen = tramite.getCuentasOrigen().get(0);
        // Cuenta para subdividir
        setInSession(WebConstants.CUENTA_ORIGEN, cuentaOrigen);

        // Titulares en session para completar CUITCUIL
        setInSession(WebConstants.TITULARES_ADD, new HashSet<TitularParcela>(cuentaOrigen.getParcela().getTitulares()));
        setInSession(WebConstants.RESPONSABLES_ADD, new HashSet<ResponsableCuenta>(cuentaOrigen.getResponsables()));

        setInSession(WebConstants.PARCELAS_DATA, tramite.getParcelas());
        return "inprocess";
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.opensymphony.xwork2.ActionSupport#input()
     */
    public String inputNuevaParcela() {
        LOG.info("Nueva parcela de subdivision, ingreso a carga de datos");

        // Setea datos de la parcela ORIGINAL en la nueva parcela
        copyDatosNuevaParcela();

        // Modo Alta
        setInSession(EDIT_PARCELA, null);

        return "input_nueva_parcela";
    }

    /**
     * Busco en el mapa de parcelas de la subdivision para obtener los datos de la parcela a
     * editar!!
     * 
     * @return
     */
    @SuppressWarnings("unchecked")
    public String editParcela() {
        LOG.info("Editar parcela de subdivision: {}", getUuid());
        /*
         * Busco la parcela seleccionada en la lista para su edicion.
         */
        UUID uuid = UUID.fromString(getUuid());
        
        List<Parcela> parcelas = (List<Parcela>) getFromSession(WebConstants.PARCELAS_DATA);
        for (Parcela p : parcelas) {
            //if (p.getPartida().equals(partida)) {
            if (p.getUuid().equals(uuid)) {
                nuevaParcela = p;
                setInSession(EDIT_PARCELA, uuid);
                break;
            }
        }
        return "input_nueva_parcela";
    }

    @SuppressWarnings("unchecked")
    public String deleteParcela() {
        LOG.info("Eliminar parcela de subdivision: {}", getUuid());
        UUID uuid = UUID.fromString(getUuid());
        
        List<Parcela> parcelas = (List<Parcela>) getFromSession(WebConstants.PARCELAS_DATA);
        for (Parcela p : parcelas) {
            //if (p.getPartida().equals(partida)) {
            if (p.getUuid().equals(uuid)) {
                parcelas.remove(p);
                break;
            }
        }
        return "success_nueva_parcela";
    }

    /**
     * 
     * Agrego/Actualizo nueva parcela a la lista de subdivision de la cuenta origen
     * 
     * @return
     */
    public String cargaParcela() {

        LOG.info("Carga nueva parcela a subdivision: {}", nuevaParcela);

        // Validacion de datos
        // validarParcela();

        // Verificar que la nomenclatura no existe en la BD
        if (getConsultaService().exists(nuevaParcela)) {
            LOG.warn("Nomenclatura existente en BD, {}", nuevaParcela);
            addActionError(getText("nomenclatura.existente"));
        } else if (existsInParcelaList(nuevaParcela)) {
            LOG.warn("Nomenclatura existente en proceso, {}", nuevaParcela);
            addActionError(getText("nomenclatura.existente.subdivision"));
        }

        if (hasFieldErrors() || hasActionErrors()) {
            addActionError(getText("error.cargaparcela"));
            return "errorJSON";
        }

        // Poner en session la nueva parcela
        addParcelaToParcelas(nuevaParcela);
        clearMessages();
        addActionMessage(getText("subdivision.edicion.ok"));

        return "success_nueva_parcela";
    }

    /**
     * Agrega la parcela a la lista de parcelas de la subdivision. Se utiliza la partida como clave
     * en el mapa
     * 
     * @param p
     */
    @SuppressWarnings("unchecked")
    private void addParcelaToParcelas(Parcela nuevaParcela) {
        List<Parcela> parcelas = (List<Parcela>) getFromSession(WebConstants.PARCELAS_DATA);
        UUID uuid = (UUID) getFromSession(EDIT_PARCELA);
        
        // Modo Alta si partida es nulo
        if (uuid == null) {
            LOG.info("Se agrego la parcela a la lista de subdivision: {}", nuevaParcela);
            parcelas.add(nuevaParcela);
        } else {
            for (Parcela p : parcelas) {
                // Modo edicion, busco por la partida que se estaba editando
                //if (p.getPartida().equals(partida)) {
                if (p.getUuid().equals(uuid)) {
                    LOG.info("Se modifico la parcela en la lista de subdivision: {}", nuevaParcela);
                    parcelas.remove(p);
                    parcelas.add(nuevaParcela);
                    break;
                }
                setInSession(EDIT_PARCELA, null);
            }
        }

    }

    /**
     * Replica en la nueva parcela, los datos de la parcela de la cuenta origen
     */
    private void copyDatosNuevaParcela() {
        cuentaOrigen = (Cuenta) getFromSession(WebConstants.CUENTA_ORIGEN);
        nuevaParcela = new Parcela();
        nuevaParcela.setNomenclatura(new Nomenclatura(cuentaOrigen.getParcela().getNomenclatura()));
        nuevaParcela.setDomicilioInmueble(new Domicilio(cuentaOrigen.getParcela().getDomicilioInmueble()));
    }

    /**
     * Verifica si existe en la lista de parcelas de subdivision nomenclaturas duplicadas
     * 
     * @param parcela
     * @return
     */
    @SuppressWarnings("unchecked")
    private boolean existsInParcelaList(Parcela parcela) {
        List<Parcela> parcelas = (List<Parcela>) getFromSession(WebConstants.PARCELAS_DATA);
        UUID uuid = ((UUID) getFromSession(EDIT_PARCELA));
        for (Parcela p : parcelas) {
            if (p.getNomenclatura().equals(parcela.getNomenclatura()) && !p.getUuid().equals(uuid)) {
                return true;
            } else if (p.getPartida() == parcela.getPartida() && !p.getUuid().equals(uuid)) {
                return true;
            }
        }
        return false;
    }

    public Parcela getNuevaParcela() {
        return nuevaParcela;
    }

    public void setNuevaParcela(Parcela nuevaParcela) {
        this.nuevaParcela = nuevaParcela;
    }

    public CatastroService getCatastroService() {
        return catastroService;
    }

    public void setCatastroService(CatastroService catastroService) {
        this.catastroService = catastroService;
    }

    public static Logger getLog() {
        return LOG;
    }

    public Long getIdCuentaOrigen() {
        return idCuentaOrigen;
    }

    public void setIdCuentaOrigen(Long idCuentaOrigen) {
        this.idCuentaOrigen = idCuentaOrigen;
    }

    public Cuenta getCuentaOrigen() {
        return cuentaOrigen;
    }

    public void setCuentaOrigen(Cuenta cuentaOrigen) {
        this.cuentaOrigen = cuentaOrigen;
    }

    public Integer getTipoSubdivision() {
        return tipoSubdivision;
    }

    public void setTipoSubdivision(Integer tipoSubdivision) {
        this.tipoSubdivision = tipoSubdivision;
    }

    public Tramite getTramite() {
        return tramite;
    }

    public void setTramite(Tramite tramite) {
        this.tramite = tramite;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}