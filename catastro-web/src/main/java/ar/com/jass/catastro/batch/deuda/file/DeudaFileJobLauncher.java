package ar.com.jass.catastro.batch.deuda.file;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import ar.com.jass.catastro.business.CuentaCorrienteService;
import ar.com.jass.catastro.business.impl.FileType;
import ar.com.jass.catastro.model.cuentacorriente.Periodo;

/**
 * Lanzador de JOBs de procesamiento de files de deudas para entidades
 * 
 * @author msquarini
 *
 */
@Service("deudaFileJobLauncher")
public class DeudaFileJobLauncher {

    private static final Logger LOG = LoggerFactory.getLogger("DeudaFileJobLauncher");

    @Autowired
    private CuentaCorrienteService ccService;
    
    @Autowired
    @Qualifier("jobLauncher")
    private JobLauncher jobLauncher;

    @Autowired
    @Qualifier("deuda-file-JOB")
    private Job job;

    public void launchJob(FileType[] destinos, Periodo periodo) {
        try {
            for (FileType destino : destinos) {
                
                /*
                 * Parametros para el JOB
                 * id del periodo
                 * destino para indicar tipo de file a generar (PMC|LINK|Impresion)
                 */
                JobParameters param = new JobParametersBuilder().addLong("idPeriodo", periodo.getId())
                        .addString("destino", destino.toString()).toJobParameters();

                JobExecution execution = jobLauncher.run(job, param);
                LOG.info("Generacion de archivo de deuda para {}, resultado {}", destino, execution.getStatus());

                if (execution.getStatus().equals(BatchStatus.COMPLETED)) {
                    ccService.createFileDeuda(periodo, destino);
                }
            }
        } catch (Exception e) {
            LOG.error("Error en lanzamiento de generacion de file de deudas, {}", periodo, e);
        }
    }

    public CuentaCorrienteService getCcService() {
        return ccService;
    }

    public void setCcService(CuentaCorrienteService ccService) {
        this.ccService = ccService;
    }
}