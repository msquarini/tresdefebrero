package ar.com.jass.catastro.model.pagos.pmc;

import java.util.Date;

import ar.com.jass.catastro.model.pagos.FilePago;
import ar.com.jass.catastro.model.pagos.FilePagosRecord;
import ar.com.jass.catastro.model.pagos.PagoHeaderRecord;

/**
 * Header de file de pagos de Pago Mis Cuentas
 * @author msquarini
 *
 */
public class PMCHeader extends PagoHeaderRecord implements FilePagosRecord {

	private Date fecha;

	@Override
	public void complete(FilePago filePago) {
		filePago.setFileHeader(getLine());
		filePago.setFechaFile(this.fecha);		
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

}
