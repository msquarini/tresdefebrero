package ar.com.jass.catastro.web.action.deuda;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import ar.com.jass.catastro.batch.deuda.BatchSemaphore;
import ar.com.jass.catastro.batch.deuda.BatchSemaphore.BATCH_PROCESS;
import ar.com.jass.catastro.business.BoletaService;
import ar.com.jass.catastro.business.CuentaCorrienteService;
import ar.com.jass.catastro.model.cuentacorriente.BoletaPeriodoStatus;
import ar.com.jass.catastro.model.cuentacorriente.DeudaPeriodoStatus;
import ar.com.jass.catastro.model.cuentacorriente.FileDeuda;
import ar.com.jass.catastro.model.cuentacorriente.Periodo;
import ar.com.jass.catastro.web.action.GridBaseAction;
import ar.com.jass.catastro.web.dto.ResumenGeneracionDeuda;

/**
 * Gestion de DEUDA
 * 
 * Generacion / Procesamiento
 * 
 * Consultas de periodos
 * 
 * @author msquarini
 *
 */
public class PeriodoAction extends GridBaseAction<Periodo> {

    private static final long serialVersionUID = 7353477345330099548L;

    private static final Logger LOG = LoggerFactory.getLogger("PeriodoAction");

    @Value("${deuda.toBank.path}")
    private String deudaFilePath;

    /**
     * Periodo para trabajar
     */
    private Long idPeriodo;

    /**
     * Consulta de periodos por anio
     */
    private Integer anio;

    @Autowired
    private CuentaCorrienteService ccService;

    @Autowired
    private BoletaService boletaService;

    private List<Periodo> periodos;

    /**
     * DTO con informacion de detalle del periodo
     */
    private ResumenGeneracionDeuda periodoInfo;

    /**
     * Id de file de deuda para descargar
     */
    private Long idFileDeuda;

    /*
     * (non-Javadoc)
     * 
     * @see com.opensymphony.xwork2.ActionSupport#input()
     */
    public String input() {

        return "input";
    }

    /**
     * Busca los periodos por anio
     * 
     * @return
     */
    public String busquedaPeriodos() {
        if (getAnio() == null) {
            setAnio(Calendar.getInstance().get(Calendar.YEAR));
        }
        periodos = ccService.findPeriodos(getAnio(), "fileDeuda");

        setInSession("periodos", periodos);
        return "periodos-data";
    }

    /**
     * Informacion resumida de agregadores del resultado del periodo
     * 
     * @return
     */
    public String infoPeriodo() {
        Periodo selectedPeriodo = getSelectedPeriodo();

        periodoInfo = ccService.resumenGeneracionCC(selectedPeriodo);

        // setInSession("periodo", periodoInfo.getPeriodo());
        // setInSession(WebConstants.CC_DATA, periodoInfo.getCuentaCorriente());
        // setInSession(WebConstants.PAGO_DATA, periodoInfo.getPagos());

        return "info-periodo";
    }

    /**
     * Emision de deuda Anual. Modifica todos los periodos en estado <> GENERADO a procesamiento Y
     * lanza el job de emision
     * 
     * @return
     */
    public String emision() {
        try {
            // if (EmisionDeudaSemaphore.puedoProcesar()) {
            if (BatchSemaphore.puedoProcesar(BATCH_PROCESS.DEUDA)) {
                // Pasamos los periodos de todo el anio vigente A PROCESAR
                ccService.cambioEstadoProcesoGeneracionDeuda(null, DeudaPeriodoStatus.A_PROCESAR);
                Calendar now = Calendar.getInstance();
                List<Periodo> periodos = ccService.findPeriodos(now.get(Calendar.YEAR));
                // Lanzamos el procesamiento en modo async!!
                ccService.emisionDeudaAnual(periodos);
            } else {
                setMensaje("Error en ejecucion proceso de emisión");
                addActionError(getText("emision.en.ejecucion"));
                return "errorJSON";
            }
        } catch (Exception e) {
            LOG.error("Error en emision anual", e);

            setMensaje("Error en ejecucion proceso de emisión");
            addActionError(getText(e.getMessage()));
            return "errorJSON";
        }
        return "generar_ok";
    }

    /**
     * Lanzar proceso de generacion de boletas para el periodo seleccionado
     * 
     * @return
     */
    public String generarBoletaPeriodo() {
        LOG.info("generar boletas Periodo {}", getIdPeriodo());
        try {
            if (BatchSemaphore.puedoProcesar(BATCH_PROCESS.BOLETA)) {
                Periodo periodo = boletaService.cambioEstadoPeriodoGeneracionBoleta(getIdPeriodo(),
                        BoletaPeriodoStatus.A_PROCESAR);
                if (periodo == null) {
                    throw new IllegalStateException("periodo.notfound");
                }
                
                boletaService.generacionBoletasBatch(Arrays.asList(periodo));
            } else {
                setMensaje("Error en ejecucion proceso de generacion de boletas");
                addActionError(getText("boleta.en.ejecucion"));
                return "errorJSON";
            }

        } catch (Exception e) {
            LOG.error("Error en generacion de boletas para idPeriodo {}", idPeriodo, e);
            setMensaje("error en inicio de generacion de boletas.");
            addActionError("error en inicio de generacion de boletas.");
            return "errorJSON";
        }
        return "generar_ok";
    }

    /**
     * Busca en la lista de periodos en session el correspondiente al id seleccionado
     * 
     * @return
     */
    private Periodo getSelectedPeriodo() {
        List<Periodo> periodos = (List<Periodo>) getFromSession("periodos");

        Periodo selected = null;
        for (Periodo p : periodos) {
            if (p.getId().equals(getIdPeriodo())) {
                selected = p;
                break;
            }
        }
        return selected;
    }

    public String download() {
        LOG.info("Download file deuda {}", idFileDeuda);
        FileDeuda fileDeuda = ccService.findFileDeuda(idFileDeuda);
        setFilename(fileDeuda.getFilename());
        try {
            String path = deudaFilePath + File.separator + getFilename();
            setInputStream(new FileInputStream(path));
        } catch (FileNotFoundException e) {
            LOG.error("error en descarga de file de deuda", e);
            return "errorJSON";
        }
        return "download";
    }

    public Long getIdPeriodo() {
        return idPeriodo;
    }

    public void setIdPeriodo(Long idPeriodo) {
        this.idPeriodo = idPeriodo;
    }

    public Integer getAnio() {
        return anio;
    }

    public void setAnio(Integer anio) {
        this.anio = anio;
    }

    public List<Periodo> getPeriodos() {
        return periodos;
    }

    public void setPeriodos(List<Periodo> periodos) {
        this.periodos = periodos;
    }

    public ResumenGeneracionDeuda getPeriodoInfo() {
        return periodoInfo;
    }

    public void setPeriodoInfo(ResumenGeneracionDeuda periodoInfo) {
        this.periodoInfo = periodoInfo;
    }

    public Long getIdFileDeuda() {
        return idFileDeuda;
    }

    public void setIdFileDeuda(Long idFileDeuda) {
        this.idFileDeuda = idFileDeuda;
    }

    public String getDeudaFilePath() {
        return deudaFilePath;
    }

    public void setDeudaFilePath(String deudaFilePath) {
        this.deudaFilePath = deudaFilePath;
    }

}