package ar.com.jass.catastro.business.deuda.model;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import ar.com.jass.catastro.business.deuda.ConceptoCuotaDTO;
import ar.com.jass.catastro.model.cuentacorriente.CCDetalle;
import ar.com.jass.catastro.model.cuentacorriente.CCDetalle.CCDetalleBuilder;

@Entity
@DiscriminatorValue("seguridad")
public class ConceptoTasaSeguridad extends AbstractConceptoCuota {

    private Set<Integer> tasaSeguridadFija;

    public ConceptoTasaSeguridad() {
        tasaSeguridadFija = new HashSet<Integer>(Arrays.asList(1, 4, 6, 7));
    }

    @Override
    public void crearConceptoCuota(ConceptoCuotaDTO conceptoCuota) {
        BigDecimal tasa = getMinValue();
        
        // Se evalua la categoria que aplica el minimo, si no corresponse
        // se calcula por el porcentaje correspondiente
        if (!tasaSeguridadFija.contains(conceptoCuota.getCategoria())) {
            tasa = conceptoCuota.getMontoCuota().multiply(BigDecimal.valueOf(getPercentaje()));
            if (tasa.compareTo(getMinValue()) < 0) {
                tasa = getMinValue();
            }           
        }
        conceptoCuota.addDetalle(new CCDetalleBuilder().monto(tasa).concepto(this).build());
    }

}
