package ar.com.jass.catastro.model.cuentacorriente;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import ar.com.jass.catastro.model.Cuenta;
import ar.com.jass.catastro.model.pagos.Pago;

/**
 * Cuotas anuales de deuda y pagos
 * 
 * @author msquarini
 *
 */
@Entity
@Table(name = "Cuota_Anual")
public class OLDCuotaAnual implements Deuda, Serializable {

    private Long id;

    /**
     * Cuenta a la que pertenece
     */
    private Cuenta cuenta;

    /**
     * Fecha de vencimiento de la deuda (En los creditos siempre null)
     */
    private Date fechaVto;

    /**
     * Fecha de pago de la deuda
     * 
     */
    private Date fechaPago;

    /**
     * Monto del movimiento
     */
    private BigDecimal importe;

    private Integer anio;
    private Integer cuota;

    /**
     * Lista de movimientos de debito que se agrupan en la cuota anual
     */
    private List<CuentaCorriente> movimientos;

    private Integer cantidadPeriodos;

    /**
     * Tipo de movimiento DEBITO / CREDITO
     */
    private TipoMovimiento tipo;

    private Boolean aplicaDescuento = Boolean.FALSE;

    private List<CCDetalle> detalle;

    /**
     * Referencia al pago del moviemiento
     */
    private Pago pago;

    /**
     * Constructor default de movimiento de debito
     */
    public OLDCuotaAnual() {
        this.importe = BigDecimal.ZERO;
        this.detalle = new ArrayList<CCDetalle>();
        this.movimientos = new ArrayList<CuentaCorriente>();
        this.tipo = TipoMovimiento.D;
    }

    public OLDCuotaAnual(TipoMovimiento tipo, Periodo periodo) {
        this();
        this.tipo = tipo;
        this.anio = periodo.getAnio();
        this.cuota = periodo.getCuota();
        this.fechaVto = periodo.getFechaVto();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_cuotaanual_gen")
    @SequenceGenerator(name = "seq_cuotaanual_gen", sequenceName = "seq_cuota_anual", allocationSize = 1)
    @Column(name = "id_cuota_anual")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_cuenta")
    public Cuenta getCuenta() {
        return cuenta;
    }

    public void setCuenta(Cuenta cuenta) {
        this.cuenta = cuenta;
    }

    @Column(name = "fecha_vto")
    public Date getFechaVto() {
        return fechaVto;
    }

    public void setFechaVto(Date fechaVto) {
        this.fechaVto = fechaVto;
    }

    @Column(name = "importe")
    public BigDecimal getImporte() {
        return importe;
    }

    public void setImporte(BigDecimal importe) {
        this.importe = importe;
    }

    @Column(name = "anio")
    public Integer getAnio() {
        return anio;
    }

    public void setAnio(Integer anio) {
        this.anio = anio;
    }

    @Column(name = "cuota")
    public Integer getCuota() {
        return cuota;
    }

    public void setCuota(Integer cuota) {
        this.cuota = cuota;
    }

    @Column(name = "tipo")
    @Enumerated(javax.persistence.EnumType.STRING)
    public TipoMovimiento getTipo() {
        return tipo;
    }

    public void setTipo(TipoMovimiento tipo) {
        this.tipo = tipo;
    }

    @Column(name = "fecha_pago")
    public Date getFechaPago() {
        return fechaPago;
    }

    public void setFechaPago(Date fechaPago) {
        this.fechaPago = fechaPago;
    }

    @OneToMany()
    @Cascade(CascadeType.ALL)
    @JoinTable(
            name = "CA_CCDETALLE", joinColumns = @JoinColumn(name = "id_cuota_anual"),
            inverseJoinColumns = @JoinColumn(name = "id_cc_detalle"))
    public List<CCDetalle> getDetalle() {
        return detalle;
    }

    public void setDetalle(List<CCDetalle> detalle) {
        this.detalle = detalle;
    }

    public void addDetalle(CCDetalle ccDetalle) {
        getDetalle().add(ccDetalle);
        setImporte(getImporte().add(ccDetalle.getMonto()));
    }

    @Column(name = "cantidad_periodos")
    public Integer getCantidadPeriodos() {
        return cantidadPeriodos;
    }

    public void setCantidadPeriodos(Integer cantidadPeriodos) {
        this.cantidadPeriodos = cantidadPeriodos;
    }

    /*
     * @Override public List<CuentaCorriente> generarMovimientosCredito(Pago pago) {
     * List<CuentaCorriente> creditos = new ArrayList<>();
     * 
     *//**
       * Si la cuota ya presenta pago, entonces se genera el registro de credito ya que es un pago
       * doble
       */

    /*
     * if (this.getPago() != null) { CuentaCorriente ccCredito = new CuentaCorriente();
     * ccCredito.setAnio(pago.getAnio()); ccCredito.setCuota(pago.getCuota());
     * ccCredito.setCuenta(this.cuenta); ccCredito.setTipo(TipoMovimiento.C);
     * ccCredito.setFechaVto(pago.getFechaVto()); ccCredito.setImporte(pago.getImporte());
     * ccCredito.setFechaPago(pago.getFechaPago()); ccCredito.addDetalle(new
     * CCDetalleBuilder().monto(pago.getImporte())
     * .concepto(ConceptoCuentaCorriente.PAGO_EXISTENTE).build());
     * 
     * creditos.add(ccCredito); } else {
     *//**
       * OBSERVACION, todas la cuotas son del mismo monto, de esta manera el pago de la cuota anual,
       * se divide los montos por cantidad de cuotas para crear los movimientos de credito
       *//*
         * 
         * BigDecimal montoCuota =
         * this.getImporte().divide(BigDecimal.valueOf(movimientos.size())).setScale(2,
         * RoundingMode.HALF_DOWN);
         * 
         * for (CuentaCorriente debito : movimientos) { // ccCredito.addDetalle(new //
         * CCDetalleBuilder().monto(getImporte()).concepto(ConceptoCuentaCorriente.PAGO_ANUAL).build
         * ()); // 
         * creditos.addAll(debito.generarMovimientosCredito(pago)); }
         * 
         * this.setFechaPago(pago.getFilePago().getFechaFile()); this.setPago(pago); }
         * 
         * return creditos; }
         */

    /*
     * @Override public boolean equals(Object obj) { if (this == obj) return true; if (obj == null)
     * return false; if (getClass() != obj.getClass()) return false; CuotaAnual other = (CuotaAnual)
     * obj; if (anio == null) { if (other.anio != null) return false; } else if
     * (!anio.equals(other.anio)) return false; if (cantidadPeriodos == null) { if
     * (other.cantidadPeriodos != null) return false; } else if
     * (!cantidadPeriodos.equals(other.cantidadPeriodos)) return false; if (cuenta == null) { if
     * (other.cuenta != null) return false; } else if (!cuenta.equals(other.cuenta)) return false;
     * if (cuota == null) { if (other.cuota != null) return false; } else if
     * (!cuota.equals(other.cuota)) return false; if (fechaVto == null) { if (other.fechaVto !=
     * null) return false; } else if (!fechaVto.equals(other.fechaVto)) return false; if (id ==
     * null) { if (other.id != null) return false; } else if (!id.equals(other.id)) return false; if
     * (importe == null) { if (other.importe != null) return false; } else if
     * (!importe.equals(other.importe)) return false; if (tipo != other.tipo) return false; return
     * true; }
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((anio == null) ? 0 : anio.hashCode());
        result = prime * result + ((cantidadPeriodos == null) ? 0 : cantidadPeriodos.hashCode());
        result = prime * result + ((cuenta == null) ? 0 : cuenta.hashCode());
        result = prime * result + ((cuota == null) ? 0 : cuota.hashCode());
        result = prime * result + ((fechaVto == null) ? 0 : fechaVto.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((importe == null) ? 0 : importe.hashCode());
        result = prime * result + ((tipo == null) ? 0 : tipo.hashCode());
        return result;
    }

    @Override
    public String toString() {
        return "CuotaAnual [id=" + id + ", cuenta=" + cuenta + ", fechaVto=" + fechaVto + ", fechaPago=" + fechaPago
                + ", importe=" + importe + ", anio=" + anio + ", cuota=" + cuota + ", cantidadPeriodos="
                + cantidadPeriodos + ", tipo=" + tipo + "]";
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_pago", nullable = true)
    public Pago getPago() {
        return pago;
    }

    public void setPago(Pago pago) {
        this.pago = pago;
    }

    @OneToMany()
    @Cascade(CascadeType.ALL)
    @JoinTable(
            name = "cuotaanual_movimiento", joinColumns = @JoinColumn(name = "id_cuota_anual"),
            inverseJoinColumns = @JoinColumn(name = "id_cc"))
    public List<CuentaCorriente> getMovimientos() {
        return movimientos;
    }

    public void setMovimientos(List<CuentaCorriente> movimientos) {
        this.movimientos = movimientos;
    }

    public void addMovimiento(CuentaCorriente movimiento) {
        this.movimientos.add(movimiento);
    }
    /*
     * @Transient public Boolean getAplicaDescuento() { return aplicaDescuento; }
     * 
     * public void setAplicaDescuento(Boolean aplicaDescuento) { this.aplicaDescuento =
     * aplicaDescuento; }
     */
}
