package ar.com.jass.catastro.batch.pagos;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

import ar.com.jass.catastro.dao.GeneralDAO;
import ar.com.jass.catastro.model.pagos.EstadoFilePagos;
import ar.com.jass.catastro.model.pagos.FilePago;

/**
 * Persiste registro de file de pagos y la lista detalle de pagos
 * 
 * @author msquarini
 *
 */
public class PagosWriter implements ItemWriter<FilePago> {

    private static final Logger LOG = LoggerFactory.getLogger("PagosWriter");

    @Autowired
    private GeneralDAO generalDAO;

    /*
     * @AfterStep public void retrieveInterstepData(StepExecution stepExecution) { JobExecution
     * jobExecution = stepExecution.getJobExecution(); ExecutionContext jobContext =
     * jobExecution.getExecutionContext(); //jobContext.put("fallidos", fallidos); }
     */

    @Override
    public void write(List<? extends FilePago> items) throws Exception {
        for (FilePago fp : items) {
            if (fp.getProcesados() == fp.getCantidadRegistros()) {
                fp.setEstado(EstadoFilePagos.FINALIZADO);
            } else {
                fp.setEstado(EstadoFilePagos.ERROR);
            }

            LOG.info("(FINPAGO) {}", fp);
            generalDAO.update(fp);
        }
    }

    public GeneralDAO getGeneralDAO() {
        return generalDAO;
    }

    public void setGeneralDAO(GeneralDAO generalDAO) {
        this.generalDAO = generalDAO;
    }
}