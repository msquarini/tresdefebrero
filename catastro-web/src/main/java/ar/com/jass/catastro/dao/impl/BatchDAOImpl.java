package ar.com.jass.catastro.dao.impl;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import ar.com.jass.catastro.dao.BatchDAO;
import ar.com.jass.catastro.model.pagos.FilePago;

@SuppressWarnings("unchecked")
@Repository("bacthDAO")
public class BatchDAOImpl implements BatchDAO {

	private static final Logger LOG = LoggerFactory.getLogger("BatchDAOImpl");

	@Autowired
	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public boolean checkUniquePago(String checksum) {
		Criteria c = getSessionFactory().getCurrentSession().createCriteria(FilePago.class);
		c.add(Restrictions.eq("hash", checksum));
		
		return c.list().isEmpty();
	}
}
