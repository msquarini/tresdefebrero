package ar.com.jass.catastro.batch.deuda.boleta;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;

import ar.com.jass.catastro.batch.HelperTasklet;
import ar.com.jass.catastro.batch.deuda.BatchSemaphore;
import ar.com.jass.catastro.batch.deuda.BatchSemaphore.BATCH_PROCESS;
import ar.com.jass.catastro.dao.GeneralDAO;
import ar.com.jass.catastro.model.cuentacorriente.Periodo;

/**
 * 
 * 
 * @author msquarini
 *
 */
public class FinishPeriodoTasklet extends HelperTasklet implements Tasklet {

    private static final Logger LOG = LoggerFactory.getLogger("FinishPeriodoTasklet");

    @Autowired
    private GeneralDAO generalDAO;

    @Override
    public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) throws Exception {

        Long idPeriodo = getJobLongParameter(chunkContext, "idPeriodo");

        // Tomo el periodo de los parametros del Job
        Periodo periodo = (Periodo) getExecutionParameter(chunkContext, "periodo_" + idPeriodo);

        LOG.info("Finalizacion de proceso de generacion de deuda, {}", periodo);
        generalDAO.update(periodo);

        BatchSemaphore.finProcesamiento(BATCH_PROCESS.BOLETA);
        return RepeatStatus.FINISHED;
    }

}
