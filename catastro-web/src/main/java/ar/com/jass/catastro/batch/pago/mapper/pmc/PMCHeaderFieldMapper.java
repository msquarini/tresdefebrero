package ar.com.jass.catastro.batch.pago.mapper.pmc;

import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;

import ar.com.jass.catastro.model.pagos.pmc.PMCHeader;

public class PMCHeaderFieldMapper implements FieldSetMapper<PMCHeader> {

	public PMCHeader mapFieldSet(FieldSet fs) {

		if (fs == null) {
			return null;
		}

		PMCHeader header = new PMCHeader();
		header.setFecha(fs.readDate("fecha", "yyyyMMdd"));
		header.setLine(fs.readString("linea"));
		return header;
	}

}
