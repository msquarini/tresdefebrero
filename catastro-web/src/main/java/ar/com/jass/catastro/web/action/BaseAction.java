package ar.com.jass.catastro.web.action;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;

import ar.com.jass.catastro.business.CatastroService;
import ar.com.jass.catastro.business.ConsultaService;
import ar.com.jass.catastro.business.ErrorService;
import ar.com.jass.catastro.model.Barrio;
import ar.com.jass.catastro.model.CodigoAdministrativo;
import ar.com.jass.catastro.model.Cuenta;
import ar.com.jass.catastro.model.Destino;
import ar.com.jass.catastro.model.Domicilio;
import ar.com.jass.catastro.model.Nomenclatura;
import ar.com.jass.catastro.model.Parcela;
import ar.com.jass.catastro.model.Titular;
import ar.com.jass.catastro.model.Tramite;
import ar.com.jass.catastro.model.error.ErrorLog;
import ar.com.jass.catastro.web.commons.WebConstants;

/**
 * Action base como acceso a funciones y objetos de uso comun
 * 
 * @author msquarini
 *
 */
public abstract class BaseAction extends ActionSupport implements SessionAware {

    private static final long serialVersionUID = 7353477345330099548L;

    // private static final Logger LOG = LoggerFactory.getLogger("BaseAction");

    private String mensaje;
    private String tituloError;
    private String mensajeError;
    private String status;

    private Collection<String> successMessages = new ArrayList<String>();
    private Collection<String> infoMessages = new ArrayList<String>();;

    private Map<String, Object> session;

    /**
     * Usado para download
     */
    private InputStream inputStream;

    /**
     * Nombre del file descargado
     */
    private String filename;

    /**
     * Control de seguridad para iniciar tramite con cuenta con deuda
     */
    protected String hash;

    /**
     * Identificar de cuenta origen a cambiar titulares
     */
    protected Long idCuentaOrigen;

    /**
     * Cuenta origen
     */
    protected Cuenta cuentaOrigen;

    @Autowired
    private ConsultaService consultaService;

    @Autowired
    private CatastroService catastroService;

    @Autowired
    private ErrorService errorService;

    @Override
    public void setSession(Map<String, Object> session) {
        this.session = session;
    }

    protected void removeFromSession(String key) {
        this.session.remove(key);
    }

    protected void setInSession(String key, Object value) {
        this.session.put(key, value);
    }

    protected Object getFromSession(String key) {
        return this.session.get(key);
    }

    private Boolean existKeyInSession(String key) {
        return this.session.containsKey(key);
    }

    /**
     * Eliminar datos de session
     */
    protected void removeTramiteDataFromSession() {
        removeFromSession(WebConstants.TRAMITE);
        removeFromSession(WebConstants.CUENTA);
        removeFromSession(WebConstants.PARCELAS_DATA);
        removeFromSession(WebConstants.TITULARES_DATA);
        removeFromSession(WebConstants.RESPONSABLES_ADD);
        removeFromSession(WebConstants.CUENTA_CON_DEUDA);
        removeFromSession(WebConstants.LINEAS_DATA);
        removeFromSession(WebConstants.NOMENCLATURA);
    }

    protected Tramite actualizarTramite(Tramite tramite) {
        Tramite t = (Tramite) getFromSession(WebConstants.TRAMITE);
        if (t != null) {
            t.setNumero(tramite.getNumero());
            t.setAnio(tramite.getAnio());
            t.setCodigo(tramite.getCodigo());
            t.setObservacion(tramite.getObservacion());
            t.setOficio(tramite.getOficio());
            t.setVigencia(tramite.getVigencia());
        }
        return t;
    }

    protected void initCustomData() {
        if (!existKeyInSession("barrios")) {
            LOG.info("Cargando en session datos comunes...");

            setInSession("barrios", (List<Barrio>) getConsultaService().findAll(Barrio.class));
            setInSession("destinos", getConsultaService().findAll(Destino.class));

            // Códigos administrativos en session para la gestion de tramites
            setInSession(WebConstants.COD_ADM_DATA,
                    (List<CodigoAdministrativo>) getConsultaService().findAll(CodigoAdministrativo.class));

        }
    }

    /**
     * Replica en la nueva parcela, los datos de la parcela de la cuenta origen
     */
    protected Parcela createParcelaFromCuenta(Cuenta from) {
        // cuentaOrigen = (Cuenta) getFromSession(CUENTA_ORIGEN);
        Parcela nuevaParcela = new Parcela();
        nuevaParcela.setNomenclatura(new Nomenclatura(from.getParcela().getNomenclatura()));
        nuevaParcela.setDomicilioInmueble(new Domicilio(from.getParcela().getDomicilioInmueble()));
        // nuevaParcela.setDomicilioPostal(new
        // Domicilio(from.getParcela().getDomicilioPostal()));

        return nuevaParcela;
    }

    /**
     * Creacion de log de error
     * 
     * @param accion
     * @param mensaje
     * @return
     */
    protected ErrorLog crearErrorLog(String accion, String mensaje) {
        clearErrorsAndMessages();
        ErrorLog log = null;
        try {
            log = getErrorService().createErrorLog(this.getClass().getName(), accion, mensaje);
        } catch (Exception e) {
            log = new ErrorLog("error interno", accion, mensaje, "");
        }
        addActionError("Error #" + log.getId());
        return log;
    }

    /**
     * Copiar los valores de un titular a otro
     * 
     * @param from
     * @param to
     */
    protected void updateTitularValues(Titular from, Titular to) {
        to.setCuitcuil(from.getCuitcuil());
        to.setApellido(from.getApellido());
        to.setNombre(from.getNombre());
        to.setDocumento(from.getDocumento());
        to.setDomicilioPostal(from.getDomicilioPostal());
        to.setRazonSocial(from.getRazonSocial());
        to.setObservacion(from.getObservacion());
        to.setPersonaJuridica(from.getPersonaJuridica());
        to.setTipoDocumento(from.getTipoDocumento());
        to.setMail(from.getMail());
    }

    public ConsultaService getConsultaService() {
        return consultaService;
    }

    public void setConsultaService(ConsultaService consultaService) {
        this.consultaService = consultaService;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getTituloError() {
        return tituloError;
    }

    public void setTituloError(String tituloError) {
        this.tituloError = tituloError;
    }

    public String getMensajeError() {
        return mensajeError;
    }

    public void setMensajeError(String mensajeError) {
        this.mensajeError = mensajeError;
    }

    public InputStream getInputStream() {
        return inputStream;
    }

    public void setInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public Long getIdCuentaOrigen() {
        return idCuentaOrigen;
    }

    public void setIdCuentaOrigen(Long idCuentaOrigen) {
        this.idCuentaOrigen = idCuentaOrigen;
    }

    public Cuenta getCuentaOrigen() {
        return cuentaOrigen;
    }

    public void setCuentaOrigen(Cuenta cuentaOrigen) {
        this.cuentaOrigen = cuentaOrigen;
    }

    public CatastroService getCatastroService() {
        return catastroService;
    }

    public void setCatastroService(CatastroService catastroService) {
        this.catastroService = catastroService;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ErrorService getErrorService() {
        return errorService;
    }

    public void setErrorService(ErrorService errorService) {
        this.errorService = errorService;
    }

    public void clearMessages() {
        clearErrorsAndMessages();
        infoMessages.clear();
        successMessages.clear();
    }

    public void addInfoMessage(String message) {
        infoMessages.add(message);
    }

    public void addSuccessMessage(String message) {
        successMessages.add(message);
    }

    public Collection<String> getSuccessMessages() {
        return successMessages;
    }

    public Collection<String> getInfoMessages() {
        return infoMessages;
    }
}