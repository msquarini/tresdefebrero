package ar.com.jass.catastro.model.pagos;

public abstract class PagoHeaderRecord implements FilePagosRecord {

	String line;

	@Override
	public boolean isHeader() {
		return true;
	}

	@Override
	public boolean isDetail() {
		return false;
	}

	@Override
	public boolean isTrailer() {
		return false;
	}

	public String getLine() {
		return line;
	}

	public void setLine(String line) {
		this.line = line;
	}
	
	public String toString() {
		StringBuffer sb = new StringBuffer(getClass().getName());
		sb.append(" - line: ").append(getLine());
		return sb.toString();
	}
}
