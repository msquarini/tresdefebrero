package ar.com.jass.catastro.business;

import ar.com.jass.catastro.model.error.ErrorLog;

public interface ErrorService {

    public ErrorLog createErrorLog(String operacion, String accion, String mensaje);
}
