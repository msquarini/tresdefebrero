package ar.com.jass.catastro.model.cuentacorriente;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import ar.com.jass.catastro.business.deuda.model.ConceptoCustom;
import ar.com.jass.catastro.model.cuentacorriente.CCDetalle.CCDetalleBuilder;
import ar.com.jass.catastro.model.pagos.Pago;

/**
 * Composicion de una boleta Agrupa movimientos de cuenta corriente del periodo (cuota-anio) calcula
 * el importe sobre la diferencia entre Creditos y Debitos
 * 
 * @author msquarini
 *
 */
@Entity
@Table(name = "Boleta_Detalle")
public class BoletaDetalle {

    private Long id;

    private Boleta boleta;

    private Integer cuota;

    private Integer anio;

    private BigDecimal importe;

    private List<CuentaCorriente> movimientos;

    public BoletaDetalle() {
    }

    /**
     * Crear el detalle de boleta para los movimientos existentes de la cuenta en el periodo
     * 
     * @param movimientos
     * @param boleta
     */
    public BoletaDetalle(Periodo periodo, List<CuentaCorriente> movimientos, Boleta boleta) {
        //this.cuota = boleta.getCuota();
        //this.anio = boleta.getAnio();
        this.cuota = periodo.getCuota();
        this.anio = periodo.getAnio();
        
        this.boleta = boleta;
        this.importe = BigDecimal.ZERO;
        this.movimientos = new ArrayList<CuentaCorriente>();

        for (CuentaCorriente movimiento : movimientos) {
            this.movimientos.add(movimiento);
            // Aplicar Interes al movimiento

            // Calcular importe
            if (TipoMovimiento.D.equals(movimiento.getTipo())) {
                this.importe = this.importe.add(movimiento.getMontoTotal());
            } else {
                this.importe = this.importe.subtract(movimiento.getMontoTotal());
            }
        }
    }

    /**
     * Crea los movimientos de credito para el periodo
     * @param pago
     * @param pagoDoble
     * @param cuotaAnual
     * @param porcentajeBonificacion
     * @return
     */
    public List<CuentaCorriente> procesarPago(Pago pago, Boolean pagoDoble, Boolean cuotaAnual,
            BigDecimal porcentajeBonificacion) {
        List<CuentaCorriente> creditos = new ArrayList<CuentaCorriente>();

        CuentaCorriente ccCredito = new CuentaCorriente();
        ccCredito.setAnio(pago.getAnio());
        ccCredito.setCuota(pago.getCuota());
        ccCredito.setCuenta(getBoleta().getCuenta());
        ccCredito.setTipo(TipoMovimiento.C);
        ccCredito.setFechaVto(pago.getFechaVto());
        ccCredito.setImporte(getImporte());
        ccCredito.setFechaPago(pago.getFechaPago());

        if (pagoDoble) {
            ccCredito.addDetalle(new CCDetalleBuilder().monto(pago.getImporte())
                    .concepto(ConceptoCustom.PAGO_EXISTENTE).build());
        }

        creditos.add(ccCredito);

        // Si es cuota anual, entonces calculo la bonificacion para el periodo
        if (cuotaAnual) {
            BigDecimal importeBonificado = getImporte().multiply(porcentajeBonificacion).setScale(2, RoundingMode.DOWN);

            // Actualizo el monto del credito por pago anual del periodo
            ccCredito.setImporte(getImporte().subtract(importeBonificado));
            ccCredito.addDetalle(new CCDetalleBuilder().monto(pago.getImporte())
                    .concepto(ConceptoCustom.PAGO_ANUAL).build());

            CuentaCorriente ccBonificacion = new CuentaCorriente();
            ccBonificacion.setAnio(pago.getAnio());
            ccBonificacion.setCuota(pago.getCuota());
            ccBonificacion.setCuenta(getBoleta().getCuenta());
            ccBonificacion.setTipo(TipoMovimiento.C);
            ccBonificacion.setFechaVto(pago.getFechaVto());
            ccBonificacion.setFechaPago(pago.getFechaPago());
            ccBonificacion.setImporte(importeBonificado);
            ccBonificacion.addDetalle(new CCDetalleBuilder().monto(pago.getImporte())
                    .concepto(ConceptoCustom.PAGO_ANUAL_BONIFICACION).build());

            creditos.add(ccBonificacion);
        }
        return creditos;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_boletadetalle_gen")
    @SequenceGenerator(name = "seq_boletadetalle_gen", sequenceName = "seq_boletadetalle", allocationSize = 1)
    @Column(name = "id_boletadetalle")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_boleta")
    public Boleta getBoleta() {
        return boleta;
    }

    public void setBoleta(Boleta boleta) {
        this.boleta = boleta;
    }

    @Column(name = "cuota")
    public Integer getCuota() {
        return cuota;
    }

    public void setCuota(Integer cuota) {
        this.cuota = cuota;
    }

    @Column(name = "anio")
    public Integer getAnio() {
        return anio;
    }

    public void setAnio(Integer anio) {
        this.anio = anio;
    }

    @Column(name = "importe")
    public BigDecimal getImporte() {
        return importe;
    }

    public void setImporte(BigDecimal importe) {
        this.importe = importe;
    }

    @OneToMany()
    @JoinTable(
            name = "cc_boletadetalle", joinColumns = @JoinColumn(name = "id_boletadetalle"),
            inverseJoinColumns = @JoinColumn(name = "id_cc"))
    public List<CuentaCorriente> getMovimientos() {
        return movimientos;
    }

    public void setMovimientos(List<CuentaCorriente> movimientos) {
        this.movimientos = movimientos;
    }

    @Override
    public String toString() {
        return "BoletaDetalle [id=" + id + ", boleta=" + boleta + ", cuota=" + cuota + ", anio=" + anio + ", importe="
                + importe + "]";
    }

}