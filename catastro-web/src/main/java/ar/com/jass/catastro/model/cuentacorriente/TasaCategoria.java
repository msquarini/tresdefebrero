package ar.com.jass.catastro.model.cuentacorriente;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import ar.com.jass.catastro.model.EntityBase;

/**
 * Tasa por categoria 
 * 
 * @author msquarini
 *
 */
@Entity
@Table(name = "Tasa_Categoria")
public class TasaCategoria extends EntityBase implements Serializable{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private Long id;

    private Integer categoria;
    
    private Double tasa;
    
    private Integer anio;
    
    private TasaCategoria() {
        
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_tasa_categoria_gen")
    @SequenceGenerator(name = "seq_tasa_categoria_gen", sequenceName = "seq_tasa_categoria", allocationSize = 1)
    @Column(name = "id_tasa_categoria")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "categoria")
    public Integer getCategoria() {
        return categoria;
    }

    public void setCategoria(Integer categoria) {
        this.categoria = categoria;
    }

    @Column(name = "tasa")
    public Double getTasa() {
        return tasa;
    }

    public void setTasa(Double tasa) {
        this.tasa = tasa;
    }

    @Column(name = "anio")
    public Integer getAnio() {
        return anio;
    }

    public void setAnio(Integer anio) {
        this.anio = anio;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((anio == null) ? 0 : anio.hashCode());
        result = prime * result + ((categoria == null) ? 0 : categoria.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((tasa == null) ? 0 : tasa.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        TasaCategoria other = (TasaCategoria) obj;
        if (anio == null) {
            if (other.anio != null)
                return false;
        } else if (!anio.equals(other.anio))
            return false;
        if (categoria == null) {
            if (other.categoria != null)
                return false;
        } else if (!categoria.equals(other.categoria))
            return false;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (tasa == null) {
            if (other.tasa != null)
                return false;
        } else if (!tasa.equals(other.tasa))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "TasaCategoria [id=" + id + ", categoria=" + categoria + ", tasa=" + tasa + ", anio=" + anio + "]";
    }
}
