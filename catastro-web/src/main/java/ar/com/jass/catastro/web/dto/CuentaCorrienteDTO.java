package ar.com.jass.catastro.web.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import ar.com.jass.catastro.model.cuentacorriente.CuentaCorriente;
import ar.com.jass.catastro.model.cuentacorriente.TipoMovimiento;

/**
 * DTO informacion de grilla de CC con informacion de movimientos agrupados por anio y cuota
 * 
 * @author msquarini
 *
 */
public class CuentaCorrienteDTO implements Serializable {

	private Long id;
    private Integer anio;
    private Integer cuota;
    private Integer cantidadDebitos = 0;
    private Integer cantidadCreditos = 0;
    private BigDecimal totalCredito = BigDecimal.ZERO;
    private BigDecimal totalDebito = BigDecimal.ZERO;
    private List<CuentaCorriente> movimientos;

    public CuentaCorrienteDTO(Long id, Integer anio, Integer cuota) {
        this.id = id;
    	this.anio = anio;
        this.cuota = cuota;
        this.movimientos = new ArrayList<CuentaCorriente>();
    }
    
    public void addMovimiento(CuentaCorriente cc) {
        if (cc.getTipo().equals(TipoMovimiento.C)) {
            cantidadCreditos++;
            totalCredito = totalCredito.add(cc.getMontoTotal());
        } else {
            cantidadDebitos++;
            totalDebito = totalDebito.add(cc.getMontoTotal());
        }
        movimientos.add(cc);
    }
    
    public Integer getAnio() {
        return anio;
    }

    public void setAnio(Integer anio) {
        this.anio = anio;
    }

    public Integer getCuota() {
        return cuota;
    }

    public void setCuota(Integer cuota) {
        this.cuota = cuota;
    }

    public Integer getCantidadCreditos() {
        return cantidadCreditos;
    }

    public void setCantidadCreditos(Integer cantidadCreditos) {
        this.cantidadCreditos = cantidadCreditos;
    }

    public Integer getCantidadDebitos() {
        return cantidadDebitos;
    }

    public void setCantidadDebitos(Integer cantidadDebitos) {
        this.cantidadDebitos = cantidadDebitos;
    }

    public BigDecimal getTotalCredito() {
        return totalCredito;
    }

    public void setTotalCredito(BigDecimal totalCredito) {
        this.totalCredito = totalCredito;
    }

    public BigDecimal getTotalDebito() {
        return totalDebito;
    }

    public void setTotalDebito(BigDecimal totalDebito) {
        this.totalDebito = totalDebito;
    }

    public BigDecimal getSaldo() {
        return getTotalDebito().subtract(getTotalCredito());
    }

    public Integer getStatus() {
        if (getSaldo().compareTo(BigDecimal.ZERO) > 0) {
            return 2;
        }
        return 0;
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

    public List<CuentaCorriente> getMovimientos() {
        return movimientos;
    }
}
