package ar.com.jass.catastro.batch.pago.mapper.pmc;

import java.math.BigDecimal;

import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;

import ar.com.jass.catastro.model.pagos.pmc.PMCTrailer;

public class PMCTrailerFieldMapper implements FieldSetMapper<PMCTrailer> {

    public PMCTrailer mapFieldSet(FieldSet fs) {

        if (fs == null) {
            return null;
        }

        PMCTrailer trailer = new PMCTrailer();       
        trailer.setLine(fs.readString("linea"));

        trailer.setCantidadRegistros(fs.readInt("registros"));

        String cadenaImporte = fs.readString("total");
        String entero = cadenaImporte.substring(0, 9);
        String decimal = cadenaImporte.substring(9);

        trailer.setTotal(new BigDecimal(entero + "." + decimal));

        return trailer;
    }

}
