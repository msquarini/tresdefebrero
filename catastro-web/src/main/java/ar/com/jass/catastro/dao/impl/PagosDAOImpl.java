package ar.com.jass.catastro.dao.impl;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityNotFoundException;

import org.hibernate.Criteria;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.web.firewall.RequestRejectedException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ar.com.jass.catastro.dao.PagosDAO;
import ar.com.jass.catastro.model.cuentacorriente.CuentaCorriente;
import ar.com.jass.catastro.model.pagos.EstadoFilePagos;
import ar.com.jass.catastro.model.pagos.FilePago;
import ar.com.jass.catastro.util.DateHelper;

@Repository("pagosDAO")
public class PagosDAOImpl extends GeneralDAOImpl implements PagosDAO {

    private static final Logger LOG = LoggerFactory.getLogger("PagosDAOImpl");

    public FilePago findFilePago(String hash) {
        Criteria criteria = currentSession().createCriteria(FilePago.class);
        criteria.add(Restrictions.eq("hash", hash));

        try {
            return (FilePago) criteria.list().get(0);
        } catch (IndexOutOfBoundsException iobe) {
            throw new EntityNotFoundException();
        }
    }

    public List<FilePago> getFilePagos(Date from, Date to, EstadoFilePagos[] estados) {
        Criteria criteria = currentSession().createCriteria(FilePago.class);

        if (from != null) {
            criteria.add(Restrictions.ge("createdDate", DateHelper.getFormattedFromDateTime(from)));
        }
        if (to != null) {
            criteria.add(Restrictions.le("createdDate", DateHelper.getFormattedToDateTime(to)));
        }
        
        if (estados != null) {
            Disjunction estadosCriteria = Restrictions.disjunction();
            for (EstadoFilePagos e : estados) {
                estadosCriteria.add(Restrictions.eq("estado", e));
            }
            criteria.add(estadosCriteria);
        }
        /*
         * if (errores) { criteria.add(Restrictions.gt("errores", 0)); }
         */

        return criteria.list();
    }

    @Override
    @Transactional
    public List<FilePago> getFilePagos(EstadoFilePagos[] estados) {
        Criteria criteria = currentSession().createCriteria(FilePago.class);

        if (estados != null) {
            Disjunction estadosCriteria = Restrictions.disjunction();
            for (EstadoFilePagos e : estados) {
                estadosCriteria.add(Restrictions.eq("estado", e));
            }
            criteria.add(estadosCriteria);
        }
        return criteria.list();
    }
}