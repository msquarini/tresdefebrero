package ar.com.jass.catastro.model.temporal;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import ar.com.jass.catastro.model.EntityBase;
import ar.com.jass.catastro.model.Linea;
import ar.com.jass.catastro.model.Tramite;

/**
 * Entidad temporal para la gestion de ajuste de linea. Se mantiene hasta el procesamiento del
 * tramite
 * 
 * @author msquarini
 *
 */
@Entity
@Table(name = "linea_temporal")
public class LineaTemporal extends EntityBase implements Serializable {

    private Integer ajusteCategoria;
    private Tramite tramite;

    private Long id;

    private Double superficieTerreno;
    private Double superficieCubierta;
    private Double superficieSemi;

    private BigDecimal valorTierra;
    private BigDecimal valorComun;
    private BigDecimal valorEdificio;

    private Date fecha;

    public LineaTemporal() {

    }

    public LineaTemporal(Linea linea, Integer ajusteCategoria, Tramite tramite) {
        this.superficieCubierta = linea.getSuperficieCubierta();
        this.superficieSemi = linea.getSuperficieSemi();
        this.superficieTerreno = linea.getSuperficieTerreno();

        this.valorComun = linea.getValorComun();
        this.valorEdificio = linea.getValorEdificio();
        this.valorTierra = linea.getValorTierra();

        this.fecha = linea.getFecha();

        this.ajusteCategoria = ajusteCategoria;
        this.tramite = tramite;
    }

    @Transient
    public Linea createLinea() {
        Linea linea = new Linea();
        linea.setSuperficieCubierta(getSuperficieCubierta());
        linea.setSuperficieSemi(getSuperficieSemi());
        linea.setSuperficieTerreno(getSuperficieTerreno());

        linea.setValorComun(getValorComun());
        linea.setValorEdificio(getValorEdificio());
        linea.setValorTierra(getValorTierra());
        linea.setCategoria(getAjusteCategoria());

        linea.setFecha(getFecha());

        return linea;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_linea_temp_gen")
    @SequenceGenerator(name = "seq_linea_temp_gen", sequenceName = "seq_linea_temp", allocationSize = 1)
    @Column(name = "id_linea")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "ajuste_categoria")
    public Integer getAjusteCategoria() {
        return ajusteCategoria;
    }

    public void setAjusteCategoria(Integer ajusteCategoria) {
        this.ajusteCategoria = ajusteCategoria;
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_tramite")
    public Tramite getTramite() {
        return tramite;
    }

    public void setTramite(Tramite tramite) {
        this.tramite = tramite;
    }

    @Column(name = "sup_terreno")
    public Double getSuperficieTerreno() {
        return superficieTerreno;
    }

    public void setSuperficieTerreno(Double superficieTerreno) {
        this.superficieTerreno = superficieTerreno;
    }

    @Column(name = "sup_cubierta")
    public Double getSuperficieCubierta() {
        return superficieCubierta;
    }

    public void setSuperficieCubierta(Double superficieCubierta) {
        this.superficieCubierta = superficieCubierta;
    }

    @Column(name = "sup_semi")
    public Double getSuperficieSemi() {
        return superficieSemi;
    }

    public void setSuperficieSemi(Double superficieSemi) {
        this.superficieSemi = superficieSemi;
    }

    @Column(name = "valor_tierra")
    public BigDecimal getValorTierra() {
        return valorTierra;
    }

    public void setValorTierra(BigDecimal valorTierra) {
        this.valorTierra = valorTierra;
    }

    @Column(name = "valor_comun")
    public BigDecimal getValorComun() {
        return valorComun;
    }

    public void setValorComun(BigDecimal valorComun) {
        this.valorComun = valorComun;
    }

    @Column(name = "valor_edificio")
    public BigDecimal getValorEdificio() {
        return valorEdificio;
    }

    public void setValorEdificio(BigDecimal valorEdificio) {
        this.valorEdificio = valorEdificio;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fecha")
    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((tramite == null) ? 0 : tramite.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        LineaTemporal other = (LineaTemporal) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (tramite == null) {
            if (other.tramite != null)
                return false;
        } else if (!tramite.equals(other.tramite))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "LineaTemporal [ajusteCategoria=" + ajusteCategoria + ", tramite=" + tramite + ", id=" + id
                + ", superficieTerreno=" + superficieTerreno + ", superficieCubierta=" + superficieCubierta
                + ", superficieSemi=" + superficieSemi + ", valorTierra=" + valorTierra + ", valorComun=" + valorComun
                + ", valorEdificio=" + valorEdificio + ", fecha=" + fecha + "]";
    }

}
