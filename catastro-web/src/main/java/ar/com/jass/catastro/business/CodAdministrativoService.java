package ar.com.jass.catastro.business;

import ar.com.jass.catastro.model.CodigoAdministrativo;

public interface CodAdministrativoService {

    public Integer getCodigoCuota();
    
    public CodigoAdministrativo find(Long id);
    
    public void saveOrUpdate(CodigoAdministrativo codigoAdministrativo);
}
