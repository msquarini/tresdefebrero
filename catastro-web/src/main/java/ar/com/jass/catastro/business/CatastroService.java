package ar.com.jass.catastro.business;

import java.util.List;

import ar.com.jass.catastro.exceptions.BusinessException;
import ar.com.jass.catastro.model.Cuenta;
import ar.com.jass.catastro.model.Linea;
import ar.com.jass.catastro.model.Parcela;
import ar.com.jass.catastro.model.ResponsableCuenta;
import ar.com.jass.catastro.model.TitularParcela;
import ar.com.jass.catastro.model.Tramite;

/**
 * @author msquarini
 *
 */
public interface CatastroService {

    /**
     * Subdivision de una cuenta en N parcelas. Se generan las nuevas N cuentas y se asignan mismos
     * titulares que cuenta origen
     * 
     * @param cuentaOrigen
     * @param nuevasParcelas
     * @param tramite
     * @param titulares
     */
    Tramite subdividir(Cuenta cuentaOrigen, List<Parcela> nuevasParcelas, Tramite tramite,
            List<TitularParcela> titulares, List<ResponsableCuenta> responsables) throws BusinessException;

    /**
     * Unificacion de N cuentas en 1 nueva cuenta, desactivando las N cuentas y parcelas
     * correspondientes
     * 
     * @param origenes
     * @param nuevaParcela
     * @param titulares
     * @param tramite
     */
    Tramite unificar(List<Parcela> origenes, Parcela nuevaParcela, List<TitularParcela> titulares,
            List<ResponsableCuenta> responsables, Tramite tramite) throws BusinessException;

    /**
     * Gestion de primera inscripcion
     * 
     * @param tramite
     * @param parcela
     * @param titulares
     * @return
     */
    Tramite inscripcion(Tramite tramite, Parcela parcela, List<TitularParcela> titulares) throws BusinessException;

    /**
     * @param tramite
     * @param cuentaOrigen
     * @param parcela
     * @param titulares
     * @return
     */
    Tramite desdoblar(Tramite tramite, Cuenta cuentaOrigen, Parcela parcela, List<TitularParcela> titulares,
            List<ResponsableCuenta> responsables) throws BusinessException;

    /**
     * Tramite de cambio de titulares
     * 
     * @param tramite
     * @param origen
     * @param titulares
     * @return
     */
    Tramite updateTitulares(Tramite tramite, Cuenta origen, List<TitularParcela> titulares);

    /**
     * Busqueda de cuenta por cuenta/dv
     * 
     * @param cuenta
     * @param dv
     * @param initialize
     * @return
     */
    Cuenta findCuenta(String cuenta, String dv, String... initialize);

    /**
     * Verifica si la cuenta presenta deuda a la fecha
     * 
     * @param cuenta
     * @return
     */
    public boolean presentaDeuda(Cuenta cuenta);

    /**
     * Grabar un tramite e indicar si se debe procesar
     * 
     * @param tramite
     * @param nuevasParcelas
     * @param titulares
     * @param responsables
     * @param procesar
     * @return
     * @throws BusinessException
     */
    public Tramite grabarTramite(Tramite tramite, List<Parcela> nuevasParcelas, List<TitularParcela> titulares,
            List<ResponsableCuenta> responsables, Boolean procesar) throws BusinessException;

    /**
     * Grabar tramite de ajuste de linea
     * 
     * @param tramite
     * @param nuevaLinea
     * @param ajusteCategoria
     * @return
     */
    public Tramite grabarTramite(Tramite tramite, Linea nuevaLinea, Integer ajusteCategoria);
    
    /**
     * Cancelar un tramite
     * Desbloquear las cuentas participantes
     * 
     * @param idTramite
     */
    public void cancelarTramite(Long idTramite);
    
    /**
     * Tramite de actualizacion de responsables de cuenta
     * 
     * @param tramite
     * @param origen
     * @param responsables
     * @return
     */
    public Tramite updateResponsables(Tramite tramite, Cuenta origen, List<ResponsableCuenta> responsables);

    /**
     * Nueva linea vigente en una parcela
     * 
     * @param tramite
     * @param origen
     * @param nuevaLinea
     * @param ajusteCategoria
     */
    public Tramite updateLinea(Tramite tramite, Cuenta origen, Linea nuevaLinea, Integer ajusteCategoria);
        
}
