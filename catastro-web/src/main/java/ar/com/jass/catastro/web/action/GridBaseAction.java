package ar.com.jass.catastro.web.action;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Grid Action Abstract
 * 
 * Los actions que manejen grillas deben extender de esta clase
 * 
 * @author msquarini
 * 
 */
public abstract class GridBaseAction<T> extends BaseAction {

	private static final long serialVersionUID = 7353477345330099548L;

	private static final Logger LOG = LoggerFactory.getLogger("GridBaseAction");

	private List<T> rows;

	private Integer current;
	private Integer total;

	private String id;
	private Integer rowCount;
	private String sortColumn;
	private String sortOrder;
	private String searchPhrase;

	/**
	 * Seteo de resultado de consulta para visualizar en grilla
	 * 
	 * @param data
	 */
	protected void setData(List<T> data) {
	    if (data == null) {
	        data = new ArrayList<>();
	    }
		LOG.debug("Devolviendo resultado de grilla, size {}", data.size());
		// Aplico ordenamiento
		sort(data);
		
		// Tomo solo los datos de la pagina en cuestion
		setRows(data);
	}

	public List<T> getRows() {
		return rows;
	}

	public void setRows(List<T> rows) {
		if (rows != null && !rows.isEmpty()) {

			int from = (getCurrent() - 1) * getRowCount();
			int to = (from + getRowCount() > rows.size()) ? rows.size() : from + getRowCount();

			this.rows = rows.subList(from, to);
		} else {
			this.rows = new ArrayList<T>();
		}
		setTotal(rows.size());
	}

	public Integer getCurrent() {
		return current;
	}

	public void setCurrent(Integer current) {
		this.current = current;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getRowCount() {
		return rowCount;
	}

	public void setRowCount(Integer rowCount) {
		this.rowCount = rowCount;
	}

	public String getSearchPhrase() {
		return searchPhrase;
	}

	public void setSearchPhrase(String searchPhrase) {
		this.searchPhrase = searchPhrase;
	}

	public String getSortColumn() {
		return sortColumn;
	}

	public void setSortColumn(String sortColumn) {
		this.sortColumn = sortColumn;
	}

	public String getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}

	/**
	 * Ordenamiento generico
	 * 
	 * @param rows
	 */
	private void sort(List<T> rows) {
		LOG.debug("ordenando grilla, por columna {} {}", sortColumn, sortOrder);
		
		if (sortColumn != null && sortOrder != null) {
			Comparator<T> comparator = new Comparator<T>() {
				String methodToSort = "get" + sortColumn.substring(0, 1).toUpperCase() + sortColumn.substring(1);

				@SuppressWarnings({ "rawtypes", "unchecked" })
                @Override
				public int compare(T o1, T o2) {
					try {
					    Comparable a = (Comparable)o1.getClass().getMethod(methodToSort).invoke(o1);
					    Comparable b = (Comparable)o2.getClass().getMethod(methodToSort).invoke(o2);						
						if ("desc".equalsIgnoreCase(sortOrder)) {
							return b.compareTo(a);
						} else {
							return a.compareTo(b);
						}
					} catch (Exception ex) {
						LOG.error("Error en ordenamiento", ex);
						return 0;
					}
				}
			};
			Collections.sort(rows, comparator);
		}
	}
	
}