package ar.com.jass.catastro.batch.deuda.file.callback;

import java.io.IOException;
import java.io.Writer;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.List;

import org.springframework.batch.item.file.FlatFileFooterCallback;

import ar.com.jass.catastro.batch.deuda.file.DeudaDTO;

/**
 * Footer para archivos a PMC
 * 
 * @author mesquarini
 *
 */
public class PMCFooterCallback implements FlatFileFooterCallback, DeudaFileCallback {

    public static final String trailer = "94002337%1$tY%1$tm%1$td";
    public static final int FILLER = 239;

    public Integer registros = 0;
    public BigDecimal total = BigDecimal.ZERO;

    @Override
    public void writeFooter(Writer writer) throws IOException {
        Integer parteEntero =  Integer.valueOf(total.intValue());
        BigDecimal centavos = total.subtract(total.setScale(0, RoundingMode.FLOOR)).movePointRight(2);
        Integer parteDecimal = Integer.valueOf(centavos.intValue());
        
        String temp = String.format(trailer, new Date()) + String.format("%1$07d", registros)
                + String.format("%0" + 7 + "d", 0) + String.format("%1$09d%2$02d", parteEntero, parteDecimal)
                + String.format("%0" + FILLER + "d", 0);
        writer.write(temp);
    }

    public static void main(String[] args) {
        String blancos25 = String.format("%" + 26 + "s", " ");
        String ceros29 = String.format("%0" + 29 + "d", 0);
        String total = String.format(
                "5%1$06d%2$1d100000000000TSGCM%3$4d%4$03d        0%5$tY%5$tm%5$td%6$09d%7$02d%5$tY%5$tm%5$td%6$09d%7$02d"
                        + "00000000000000000000000000000000000000%1$06d%2$1d100000000000"
                        + "M3F - SERVICIOS GENERALES CUOTA %3$4d %4$03dM3F SG %3$4d %4$03d%3$4d%4$03d%1$06d%2$1d0%6$09d%7$02d%5$tY%5$tm%5$td%8$26s%9$029d",
                123456, 9, 2018, 3, new Date(), 2345, 67, " ", 0, 344, "ss");
        System.out.println(total);

    }

    @Override
    public void notify(List<DeudaDTO> deudas) {
        for (DeudaDTO deuda : deudas) {
            registros++;
            total = total.add(deuda.getImporte());
        }
    }
}
