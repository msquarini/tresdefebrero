package ar.com.jass.catastro.web.action.deuda;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import ar.com.jass.catastro.business.BoletaService;
import ar.com.jass.catastro.business.ConsultaService;
import ar.com.jass.catastro.business.CuentaCorrienteService;
import ar.com.jass.catastro.model.Cuenta;
import ar.com.jass.catastro.model.cuentacorriente.Boleta;
import ar.com.jass.catastro.model.cuentacorriente.CuentaCorriente;
import ar.com.jass.catastro.model.cuentacorriente.TipoMovimiento;
import ar.com.jass.catastro.web.action.GridBaseAction;
import ar.com.jass.catastro.web.commons.WebConstants;
import ar.com.jass.catastro.web.dto.CCTotalizadorDTO;
import ar.com.jass.catastro.web.dto.CuentaCorrienteDTO;

/**
 * Action de gestion de boleta de pago de cuentas
 * 
 * @author msquarini
 *
 */
public class BoletaAction extends GridBaseAction<Boleta> {

    private static final long serialVersionUID = 7353477345330099548L;

    private static final Logger LOG = LoggerFactory.getLogger("BoletaAction");

    @Autowired
    private CuentaCorrienteService ccService;

    @Autowired
    private ConsultaService consultaService;

    /**
     * Entidad Cuenta para gestion de cuenta corriente
     */
    private Cuenta cuenta;

    /**
     * Filtro de Cuenta Corriente
     */
    private Integer anio;
    
    /**
     * Id de boleta para informacion detallada
     */
    private Long idBoleta;

    /**
     * Boletas que no presentan pago
     */
    private Boolean sinPago;

    private Boolean generic = Boolean.FALSE;

    /**
     * Inicializar los datos al ingresar a boleta
     */
    public String input() {
        removeFromSession(WebConstants.BOLETA_DATA);
        removeFromSession(WebConstants.CUENTA);
        anio = Calendar.getInstance().get(Calendar.YEAR);
        // Ingreso a CC para uso generico (habilitar busqueda por #cuenta!!
        generic = Boolean.TRUE;

        return "input";
    }

    /**
     * Llamada AJAX para obtener la informacion para la grilla en formato JSON
     * 
     * Se toma la cuenta en session para la busqueda de movimientos
     * 
     * @return
     */
    @SuppressWarnings("unchecked")
    public String getData() {
        try {
            List<Boleta> data = (List<Boleta>) getFromSession(WebConstants.BOLETA_DATA);
            setData(data);
        } catch (Exception e) {
            LOG.error("error en getData cuenta corriente", e);
        }
        return "dataJSON";
    }

    /**
     * Busqueda de la cuenta origen a partir de ID de la cuenta seleccionada. Se redirije a la
     * pantalla principal de cuenta corriente.
     * 
     * @return
     */
    public String search() {

        if (cuenta != null && cuenta.getCuenta().length() > 0) {
            try {
                cuenta = consultaService.findCuenta(cuenta.getCuenta(), null);
            } catch (Exception e) {
                LOG.error("Cuenta no encontrada {}", cuenta.getCuenta(), e);
                setTituloError("Busqueda de cuenta");
                setMensajeError("No se encontro cuenta con Nro.: " + cuenta.getCuenta());
                return "errorJSON";
            }
        } else {
            cuenta = (Cuenta) getFromSession(WebConstants.CUENTA);
        }
        
        LOG.info("Search Boleta filtros: {}, {} {}", cuenta, anio, sinPago);
        
        /**
         * Si estamos con una cuenta realizamos la busqueda!
         */
        if (cuenta != null) {
            List<Boleta> boletas = ccService.findBoletas(cuenta.getId(), anio, null, sinPago);
            setInSession(WebConstants.BOLETA_DATA, boletas);
        }
        return "json";
    }

    public String detail() {
        LOG.info("Detalle BOLERTA Id: {} ", idBoleta);
        //cc = ccService.findMovimientoCC(idCC);
        return "info-boleta";
    }
    
    public static Logger getLog() {
        return LOG;
    }

    public ConsultaService getConsultaService() {
        return consultaService;
    }

    public void setConsultaService(ConsultaService consultaService) {
        this.consultaService = consultaService;
    }

    public Cuenta getCuenta() {
        return cuenta;
    }

    public void setCuenta(Cuenta cuenta) {
        this.cuenta = cuenta;
    }

    public CuentaCorrienteService getCcService() {
        return ccService;
    }

    public void setCcService(CuentaCorrienteService ccService) {
        this.ccService = ccService;
    }

    public Integer getAnio() {
        return anio;
    }

    public void setAnio(Integer anio) {
        this.anio = anio;
    }

    public Boolean getSinPago() {
        return sinPago;
    }

    public void setSinPago(Boolean sinPago) {
        this.sinPago = sinPago;
    }

    public Boolean getGeneric() {
        return generic;
    }

    public void setGeneric(Boolean generic) {
        this.generic = generic;
    }

    public Long getIdBoleta() {
        return idBoleta;
    }

    public void setIdBoleta(Long idBoleta) {
        this.idBoleta = idBoleta;
    }

}
