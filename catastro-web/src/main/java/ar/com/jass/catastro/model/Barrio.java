package ar.com.jass.catastro.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Barrio. Tipificacion para los domicilios
 * 
 * @author MESquarini
 * 
 */
@Entity
@Table(name = "Barrio")
public class Barrio implements Serializable {

	private Long id;
	private String nombre;
	private Integer codigoPostal;

	public Barrio() {
		
	}
	
	public Barrio(Barrio b) {
		this.id = b.getId();
		this.nombre = b.getNombre();
		this.codigoPostal = b.getCodigoPostal();
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_barrio_gen")
	@SequenceGenerator(name = "seq_barrio_gen", sequenceName = "seq_barrio", allocationSize = 1)
	@Column(name = "id_barrio")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name="nombre")
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Column(name="codigo_postal")
	public Integer getCodigoPostal() {
		return codigoPostal;
	}

	public void setCodigoPostal(Integer codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

}
