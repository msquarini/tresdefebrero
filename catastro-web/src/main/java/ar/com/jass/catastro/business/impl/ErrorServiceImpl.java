package ar.com.jass.catastro.business.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.com.jass.catastro.business.ErrorService;
import ar.com.jass.catastro.dao.GeneralDAO;
import ar.com.jass.catastro.model.error.ErrorLog;

@Service
public class ErrorServiceImpl implements ErrorService {

    @Autowired
    @Qualifier("generalDAO")
    private GeneralDAO dao;

    @Transactional
    public ErrorLog createErrorLog(String operacion, String accion, String mensaje) {
        // Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        // String usuario = authentication.getName();
        String usuario = "temporal";

        ErrorLog error = new ErrorLog(operacion, accion, mensaje, usuario);
        dao.save(error);
        return error;
    }
}
