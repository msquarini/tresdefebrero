package ar.com.jass.catastro.batch.deuda;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;

import ar.com.jass.catastro.batch.HelperTasklet;
import ar.com.jass.catastro.dao.CuentaCorrienteDAO;
import ar.com.jass.catastro.dao.GeneralDAO;
import ar.com.jass.catastro.model.cuentacorriente.DeudaPeriodoStatus;
import ar.com.jass.catastro.model.cuentacorriente.GeneracionDeudaDTO;
import ar.com.jass.catastro.model.cuentacorriente.Periodo;

/**
 * Inicializar la ejecucion del JOB de generacion de deuda
 * 
 * Busca el primer periodo (unico) en estado A_PROCESAR y realiza la lectura de las cuentas a
 * generar deuda.
 * 
 * TODO Segmentar la consulta de cuentas para bajar el consumo de memoria
 * 
 * @author msquarini
 *
 */
public class InitDeudaGeneradorTasklet extends HelperTasklet implements Tasklet {

    private static final Logger LOG = LoggerFactory.getLogger(InitDeudaGeneradorTasklet.class);

    @Autowired
    private CuentaCorrienteDAO ccDAO;

    @Autowired
    private GeneralDAO generalDAO;

    @Override
    public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) throws Exception {

        LOG.info("Inicio de proceso de generacion de deuda (DEUDAINIT)");

        try {
            Periodo periodo = generalDAO.find(Periodo.class, getJobLongParameter(chunkContext, "idPeriodo"));

            // Flag para indicar si es emision de cuota anual
            String cuotaAnual = getJobStringParameter(chunkContext, "cuotaAnual");

            LOG.info("Procesando - {}", periodo);

            // Implementacion simple, sin segmentacion por DV
            List<GeneracionDeudaDTO> dtos;
            if (cuotaAnual == null) {
                dtos = ccDAO.getCuentasGeneracionDeudaDTO(periodo, null, Boolean.FALSE);
            } else {
                dtos = ccDAO.getCuentasGeneracionDeudaDTO(periodo, null, Boolean.TRUE);
            }

            LOG.info("Cuentas a procesar {}, periodo {} ", dtos.size(), periodo);

            periodo.setEstado(DeudaPeriodoStatus.PROCESANDO);
            generalDAO.save(periodo);

            putExecutionParameter(chunkContext, "dtos_"+periodo.getId(), dtos);
            putExecutionParameter(chunkContext, "periodo_"+periodo.getId(), periodo);

        } catch (Exception e) {
            throw new IllegalStateException("No existe Periodo en estado para procesar");

        }
        return RepeatStatus.FINISHED;
    }
}