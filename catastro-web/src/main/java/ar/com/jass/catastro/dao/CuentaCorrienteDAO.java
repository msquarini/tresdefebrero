package ar.com.jass.catastro.dao;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import ar.com.jass.catastro.business.deuda.model.TemplateCuota;
import ar.com.jass.catastro.model.Cuenta;
import ar.com.jass.catastro.model.cuentacorriente.Boleta;
import ar.com.jass.catastro.model.cuentacorriente.CuentaCorriente;
import ar.com.jass.catastro.model.cuentacorriente.DeudaPeriodoStatus;
import ar.com.jass.catastro.model.cuentacorriente.GeneracionDeudaDTO;
import ar.com.jass.catastro.model.cuentacorriente.Periodo;
import ar.com.jass.catastro.model.cuentacorriente.TasaCategoria;
import ar.com.jass.catastro.model.cuentacorriente.TipoMovimiento;
import ar.com.jass.catastro.web.dto.ResumenGeneracionDeuda;

public interface CuentaCorrienteDAO {

    public List<Cuenta> hqlsample();

    /**
     * Devuelve la lista de cuentas en condiciones de generar deuda
     * 
     * @return
     */
    @Deprecated
    // public List<Cuenta> getCuentasGeneracionDeuda();

    /**
     * Resumen de generacion de deuda en cuenta corriente para un periodo
     * 
     * @param periodo
     * @return
     */
    public ResumenGeneracionDeuda getResumenGeneracion(Periodo periodo);

    /**
     * DTO con las cuentas en condiciones de generar deuda para el periodo
     * 
     * Cuentas activas y que no presentan deuda ya generada en el periodo
     * 
     * @param periodo
     * @param dv
     * @param cuotaAnual
     *            Flag que indica si es emision de cuota anual
     * @return
     */
    public List<GeneracionDeudaDTO> getCuentasGeneracionDeudaDTO(Periodo periodo, String dv, Boolean cuotaAnual);

    /**
     * Obtiene los periodos en el estado indicado
     * 
     * @param status
     * @return
     */
    public List<Periodo> getPeriodoInState(DeudaPeriodoStatus status);

    /**
     * Busca el periodo de deuda vigente
     * 
     * @return
     */
    public Periodo findPeriodoVigente();

    /**
     * Buscar todos los periodos de deuda del anio
     * 
     * @param anio
     * @return
     */
    public List<Periodo> findPeriodos(Integer anio);
    
    /**
     * Periodos comprendidos entre desde y hasta (exclusive)
     * 
     * @param desde
     * @param hasta
     * @return
     */
    public List<Periodo> findPeriodos(Periodo desde, Periodo hasta);

    /**
     * Periodo correspondiente al anio y cuota
     * 
     * @param anio
     * @param mes
     * @return
     */
    public Periodo findPeriodo(Integer anio, Integer mes);

    /**
     * Periodos a partir del anio y cuota
     * 
     * @param anio
     * @param cuota
     * @return
     */
    public List<Periodo> findPeriodosDesde(Integer anio, Integer cuota);

    /**
     * Devuelve los moviemientos de la cuenta en el anio indicado y mayor a cuota si es != null Se
     * puede agregar filtro por ajuste y tipo si son != null
     * 
     * @param idCuenta
     * @param anio
     * @param cuota
     * @param ajuste
     * @param tipo
     * @return
     */
    public List<CuentaCorriente> findMovimientosEnAnio(Long idCuenta, Integer anio, Integer cuota, Integer ajuste,
            TipoMovimiento tipo);

    /**
     * Consulta de moviemientos de cuenta corriente
     * 
     * @param cuentaId
     * @param anio
     * @param tipo
     * @return
     */
    public List<CuentaCorriente> findMovimientos(Long cuentaId, Integer anio, TipoMovimiento tipo);

    /**
     * Consulta de movimientos para la cuenta en el anio y cuota
     * 
     * @param cuentaId
     * @param anio
     * @param cuota
     * @return
     */
    public List<CuentaCorriente> findMovimientos(Long cuentaId, Integer anio, Integer cuota);

    /**
     * Buscar los movimientos de cuenta corriente de una cuenta desde un periodo y de un tipo y
     * ajuste
     * 
     * @param cuenta
     * @param desde
     * @param tipo
     *            filtro por tipo si es != null
     * @param ajuste
     *            filtro por #ajuste si es != null
     * @return
     */
    public List<CuentaCorriente> findMovimientos(Long idCuenta, Periodo desde, TipoMovimiento tipo, Integer ajuste);

    /**
     * Buscar los movimientos de CC desde un periodo desde hasta otro periodo
     * 
     * @param idCuenta
     * @param desde
     * @param hasta
     * @param tipo
     * @param ajuste
     * @return
     */
    public List<CuentaCorriente> findMovimientos(Long idCuenta, Periodo desde, Periodo hasta, TipoMovimiento tipo,
            Integer ajuste);

    public List<CuentaCorriente> findMovimientos(Long idCuenta, Integer desdeAnio, Integer desdeCuota,
            Integer hastaAnio, Integer hastaCuota, TipoMovimiento tipo, Integer ajuste);

    public CuentaCorriente findCuotaByCuentaId(Long idCuenta, Integer anio, Integer cuota, Integer ajuste,
            TipoMovimiento tipo);

    /**
     * Movimientos de cuenta corriente de deuda que esten vencidos SIN PAGO, codADM != 0, fechaVTO <
     * NOW,
     * 
     * @param idCuenta
     * @return
     */
    public List<CuentaCorriente> findMovimientosVencidos(Long idCuenta);

    public List<CuentaCorriente> findDeudaMensual(Long idCuenta, Integer anio, Integer cuota, Boolean sinPago);

    /*
     * public List<CuotaAnual> findDeudaAnual(Long idCuenta, Integer anio, Integer cuota, Boolean
     * sinPago);
     */

    public List<TasaCategoria> findTasas(Integer anio);

    /**
     * Busqueda de boleta para un determinado pago
     * 
     * @param anio
     * @param cuota
     * @param cuenta
     * @param dv
     * @param importe
     * @param fechaVto
     * @return
     */
    public Boleta findBoleta(Integer anio, Integer cuota, String cuenta, String dv, BigDecimal importe, Date fechaVto);

    /**
     * Busqueda de boletas para una cuenta en un periodo Pueden ser mas de una (cuota periodo, cuota
     * anual, deudas)
     * 
     * @param idCuenta
     * @param periodo
     * @return
     */
    public List<Boleta> findBoletas(Long idCuenta, Periodo periodo);

    /**
     * Busqueda generica de boletas de pago
     * 
     * @param idCuenta
     * @param anio
     * @param cuota
     * @param sinPago
     * @return
     */
    public List<Boleta> findBoletas(Long idCuenta, Integer anio, Integer cuota, Boolean sinPago);

    /**
     * Buscar templates de cuotas ordernados por fecha de vigencia descendente
     * 
     * @return
     */
    public List<TemplateCuota> findTemplateCuota();
}