package ar.com.jass.catastro.web.action;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import ar.com.jass.catastro.business.BusinessHelper;
import ar.com.jass.catastro.exceptions.BusinessException;
import ar.com.jass.catastro.model.Cuenta;
import ar.com.jass.catastro.model.ResponsableCuenta;
import ar.com.jass.catastro.model.TipoTramite;
import ar.com.jass.catastro.model.Titular;
import ar.com.jass.catastro.model.TitularParcela;
import ar.com.jass.catastro.model.Tramite;
import ar.com.jass.catastro.model.error.ErrorLog;
import ar.com.jass.catastro.web.commons.WebConstants;

/**
 * Gestion ABM de Resposanbles de cuenta
 * 
 * 
 * @author msquarini
 *
 */
public class ResponsableABMAction extends GridBaseAction<ResponsableCuenta> {

	private static final long serialVersionUID = 7353477345330099541L;

	private static final Logger LOG = LoggerFactory.getLogger("ResponsableABMAction");

	/**
	 * Se utiliza para tener en session el ID o Cuit del titular en edicion
	 */
	private static final String EDIT_TITULAR = "titular_edition";

	/**
	 * Se utiliza para indicar si es modo edicion o alta de titular
	 */
	private static final String EDIT_MODE = "edit_mode";

	/**
	 * ID de titular para agregar a la lista de Responsables
	 */
	private Long idTitular;

	private String uuidTP;

	/**
	 * Nuevo Responsable
	 */
	private ResponsableCuenta responsable;

	@Autowired
	private BusinessHelper businessHelper;

	private Tramite tramite;
	private List<ResponsableCuenta> responsables;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.opensymphony.xwork2.ActionSupport#input()
	 */
	public String input() {
		return "input";
	}

	/**
	 * Inicializar la gestion de cambio de responsables
	 * 
	 * @return
	 */
	public String init() {
		// Busqueda de cuenta origen a partir de ID
		cuentaOrigen = getConsultaService().findCuentaById(idCuentaOrigen, "responsables", "parcela.titulares",
				"parcela.domicilioInmueble");

		// Validacion si presenta deuda y pasar la decision de continuar al operador
		String hashinsession = (String) getFromSession("hash");
		if (hashinsession != null && hashinsession.equals(hash)) {
			LOG.info("Cuenta presenta deuda, el operador decide continuar con el trámite, {}", cuentaOrigen);
			removeFromSession("hash");
		} else {
			if (getCatastroService().presentaDeuda(cuentaOrigen)) {
				hash = "RES-" + UUID.randomUUID().toString();
				setInSession("hash", hash);
				LOG.warn("Cuenta presenta deuda: {}", cuentaOrigen);
				addActionError("Cuenta presenta deuda");
				return "errorDeuda";
			}
		}
		tramite = new Tramite(TipoTramite.RES);
		setInSession(WebConstants.TRAMITE, tramite);
		setInSession(WebConstants.CUENTA_ORIGEN, cuentaOrigen);
		// Titulares en session
		setInSession(WebConstants.TITULARES_ADD, new HashSet<TitularParcela>(cuentaOrigen.getParcela().getTitulares()));
		setInSession(WebConstants.RESPONSABLES_ADD, new HashSet<ResponsableCuenta>(cuentaOrigen.getResponsables()));
		setInSession(WebConstants.TITULARES_DATA, null);
		return "inprocess";
	}

	public String initFromTramite() {
		tramite = (Tramite) getFromSession(WebConstants.TRAMITE);
		cuentaOrigen = tramite.getCuentasOrigen().get(0);

		setInSession(WebConstants.CUENTA_ORIGEN, cuentaOrigen);
		// Titulares en session
		setInSession(WebConstants.TITULARES_ADD, new HashSet<TitularParcela>(cuentaOrigen.getParcela().getTitulares()));
		setInSession(WebConstants.RESPONSABLES_ADD, new HashSet<ResponsableCuenta>(cuentaOrigen.getResponsables()));
		setInSession(WebConstants.TITULARES_DATA, null);
		return "inprocess";
	}

	/**
	 * Llamada AJAX para obtener la informacion para la grilla en formato JSON
	 * Grilla de edicion de titulares
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public String getData() {
		try {
			List<ResponsableCuenta> data = new ArrayList<ResponsableCuenta>(
					(Set<ResponsableCuenta>) getFromSession(WebConstants.RESPONSABLES_ADD));
			setData(data);
		} catch (Exception e) {
			LOG.error("error en getData responsables", e);
		}
		return "dataJSON";
	}

	/**
	 * Redirije a pagina de alta
	 * 
	 * @return
	 */
	public String goToAddTitular() {
		setInSession(EDIT_MODE, false);
		return "add_responsable_input";
	}

	/**
	 * Buscar el titular por el cuitcuil y setearlo para editar
	 * 
	 * @return
	 */
	public String goToEditTitular() {
		Set<ResponsableCuenta> responsables = (Set<ResponsableCuenta>) getFromSession(WebConstants.RESPONSABLES_ADD);
		UUID uuid = UUID.fromString(getUuidTP());
		for (ResponsableCuenta rc : responsables) {
			if (uuid.equals(rc.getUuid())) {
				responsable = rc;
				setInSession(EDIT_TITULAR, uuid);
				break;
			}
		}

		setInSession(EDIT_MODE, true);
		return "add_responsable_input";

	}

	/**
	 * Agrega responsable ya existente a la grilla ABM de responsables de cuenta.
	 * Valido que el titular no este presente en la lista
	 * 
	 * @return
	 */
	public String addSelectedTitular() {
		LOG.info("Agregando Responsable ID [{}]", getIdTitular());
		Set<ResponsableCuenta> resposablesCuenta = (Set<ResponsableCuenta>) getFromSession(
				WebConstants.RESPONSABLES_ADD);
		if (!resposablesCuenta.isEmpty()) {
			// Verificar que no exista en la grilla por el ID del titular
			for (ResponsableCuenta rc : resposablesCuenta) {

				// Si existe el titular dentro de los responsable de cuenta, vuelvo sin
				// agregarlo
				if (getIdTitular().equals(rc.getTitular().getId())) {
					return "update_ok";
				}
			}
		}
		Titular titular = getConsultaService().findTitularById(getIdTitular());
		ResponsableCuenta newResponsableCuenta = new ResponsableCuenta();
		newResponsableCuenta.setTitular(titular);
		resposablesCuenta.add(newResponsableCuenta);
		return "update_ok";
	}

	/**
	 * Actualiza la grilla de responsables en la gestion de tramite
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public String addResponsable() {
		boolean isEditMode = (Boolean) getFromSession(EDIT_MODE);
		LOG.info("AddTitular ModoEdicion {}, {}", isEditMode, responsable);
		Set<ResponsableCuenta> responsablesCuenta = (Set<ResponsableCuenta>) getFromSession(
				WebConstants.RESPONSABLES_ADD);

		try { // Si no es modo edit -> es ALTA
			if (!isEditMode) {
				responsablesCuenta.add(responsable);
			} else {
				for (ResponsableCuenta rc : responsablesCuenta) {
					if (isResponsableCuentaInEdition(rc)) {
						LOG.info("Se modifico el Responsable de cuenta: {}", rc);
						updateValues(rc);
						break;
					}
				}
				setInSession(EDIT_TITULAR, null);
			}
		} catch (Exception e) {
			ErrorLog log = crearErrorLog("addResponsable", e.getMessage());
			addActionError("error en ALTA de responsable en grilla de edicion!!!");
			LOG.error("error agregando responsable, {}", log, e);
			return "errorJSON";
		}
		return "update_ok";
	}

	/**
	 * Actualiza los datos del responsable
	 * 
	 * @param t
	 */
	private void updateValues(ResponsableCuenta rc) {
		updateTitularValues(responsable.getTitular(), rc.getTitular());
		rc.setDesde(responsable.getDesde());
		rc.setHasta(responsable.getHasta());
	}

	/**
	 * Valida que el titular es el que esta en modo edicion.
	 * 
	 * @param t
	 * @return
	 */
	private boolean isResponsableCuentaInEdition(ResponsableCuenta rc) {
		return ((UUID) getFromSession(EDIT_TITULAR)).equals(rc.getUuid());
	}

	/**
	 * Procesar el tramite de gestion de responsables
	 * 
	 * @return
	 */
	public String process() {
		try {
			cuentaOrigen = (Cuenta) getFromSession(WebConstants.CUENTA_ORIGEN);

			tramite = (Tramite) getFromSession(WebConstants.TRAMITE);
			if (tramite == null) {
				throw new IllegalStateException();
			}

			responsables = new ArrayList<>((Set<ResponsableCuenta>) getFromSession(WebConstants.RESPONSABLES_ADD));

			// Chequear que los Titulares presenten CUIT/CUIL cargado/actualizado
			businessHelper.checkResponsablesConCuit(responsables);

			tramite = getCatastroService().updateResponsables(tramite, cuentaOrigen, responsables);

			addActionMessage(getText("tramite.process.success"));
			removeFromSession(WebConstants.TRAMITE);
		} catch (BusinessException e) {
			ErrorLog log = crearErrorLog("process", e.getMessage());
			LOG.error("error en procesamiento de tramite de actualizacion de responsables, {}", log, e);
			addActionError(getText(e.getMessage()));
			return "errorJSON";
		} catch (Exception e) {
			ErrorLog log = crearErrorLog("process", e.getMessage());
			LOG.error("error en procesamiento de tramite de actualizacion de responsables, {}", log, e);
			addActionError(getText("error.unknown"));
			return "errorJSON";
		}
		// Ticket del tramite!
		return "success";
	}

	public Long getIdTitular() {
		return idTitular;
	}

	public void setIdTitular(Long idTitular) {
		this.idTitular = idTitular;
	}

	public String getUuidTP() {
		return uuidTP;
	}

	public void setUuidTP(String uuidTP) {
		this.uuidTP = uuidTP;
	}

	public ResponsableCuenta getResponsable() {
		return responsable;
	}

	public void setResponsable(ResponsableCuenta responsable) {
		this.responsable = responsable;
	}

	public Tramite getTramite() {
		return tramite;
	}

	public void setTramite(Tramite tramite) {
		this.tramite = tramite;
	}

	public List<ResponsableCuenta> getResponsables() {
		return responsables;
	}

	public void setResponsables(List<ResponsableCuenta> responsables) {
		this.responsables = responsables;
	}

}