package ar.com.jass.catastro.exceptions;

import ar.com.jass.catastro.model.pagos.FilePago;

public class FilePagoDuplicatedException extends BusinessException {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    FilePago filePago;

    public FilePagoDuplicatedException(FilePago fp) {
        this.filePago = fp;
    }

    public FilePago getFilePago() {
        return filePago;
    }

    public void setFilePago(FilePago filePago) {
        this.filePago = filePago;
    }
}
