package ar.com.jass.catastro.exceptions;

public class BusinessException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private String message;

    public BusinessException() {
    }

    public BusinessException(String m, Throwable e) {        
        super(e);
        this.message = m;
    }

    public BusinessException(String m) {
        this.message = m;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
