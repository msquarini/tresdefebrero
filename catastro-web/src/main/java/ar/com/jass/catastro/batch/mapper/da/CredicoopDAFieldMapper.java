package ar.com.jass.catastro.batch.mapper.da;

import java.util.Calendar;

import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;

import ar.com.jass.catastro.model.da.DADetail;
import ar.com.jass.catastro.model.da.DANovedad;

/**
 * Mapeador de entradas de archivo de DA de credicoop
 * @author msquarini
 *
 */
public class CredicoopDAFieldMapper implements FieldSetMapper<DADetail> {
	
	public DADetail mapFieldSet(FieldSet fs) {

		if (fs == null) {
			return null;
		}

		DADetail detail = new DADetail("Credicoop");
	
		Calendar c = Calendar.getInstance();
		c.set(Calendar.DATE, fs.readInt("dia"));
		c.set(Calendar.MONTH, fs.readInt("mes"));
		c.set(Calendar.YEAR, fs.readInt("anio"));
		
		detail.setFecha(c.getTime());
		detail.setCuenta(fs.readString("cuenta"));
		detail.setDv(fs.readString("dv"));
		detail.setCuentaBanco(fs.readString("cuentabanco"));
		
		String novedad = fs.readString("novedad");	
		if ("01".equals(novedad)) {
			detail.setNovedad(DANovedad.ALTA);	
		} else if ("02".equals(novedad)) {
			detail.setNovedad(DANovedad.BAJA);
		}
			
		detail.setLine(fs.readString("linea"));

		return detail;
	}

}