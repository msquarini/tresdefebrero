package ar.com.jass.catastro.business;

import java.io.File;
import java.util.Date;
import java.util.List;

import ar.com.jass.catastro.model.pagos.EstadoFilePagos;
import ar.com.jass.catastro.model.pagos.FilePago;

/**
 * Procesamiento de files de pagos
 * 
 * @author msquarini
 *
 */
public interface PagosService {


    /**
     * Validar que el archivo no existe en el sistema Se utiliza un hash del archivo para unicidad
     * 
     * De no existir se crea y se persiste el regitro
     * 
     * @return
     */
    FilePago prepocess(String filename, File pagos) throws Exception;

    /**
     * Consulta de files procesados entre fechas y si filtra solo los que tuvieron errores
     * 

     * @return
     */
    List<FilePago> getFilePagos(Date from, Date to, EstadoFilePagos[] estados);

    /**
     * Setear en estado ENPROCE todos los registros PENDINTE
     */
    void processAll();

    FilePago getFilePago(Long id, String... initialize);
}
