package ar.com.jass.catastro.model;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Transient;

/**
 * Nomenclatura catastral
 * 
 * @author MESquarini
 * 
 */
@Embeddable
public class Nomenclatura {

    private Integer circunscripcion;
    private String seccion;
    private Integer fraccion;
    private String fraccionLetra;
    private Integer manzana;
    private String manzanaLetra;
    private Integer parcela;
    private String parcelaLetra;
    private String unidadFuncional;

    public Nomenclatura() {

    }

    public Nomenclatura(Nomenclatura n) {
        this.circunscripcion = n.getCircunscripcion();
        this.seccion = n.getSeccion();
        this.fraccion = n.getFraccion();
        this.fraccionLetra = n.getFraccionLetra();
        this.manzana = n.getManzana();
        this.manzanaLetra = n.getManzanaLetra();
        this.parcela = n.getParcela();
        this.parcelaLetra = n.getParcelaLetra();
        this.unidadFuncional = n.getUnidadFuncional();
    }

    @Column(name = "circ")
    public Integer getCircunscripcion() {
        return circunscripcion;
    }

    public void setCircunscripcion(Integer circunscripcion) {
        this.circunscripcion = circunscripcion;
    }

    @Column(name = "sec")
    public String getSeccion() {
        return seccion;
    }

    public void setSeccion(String seccion) {
        this.seccion = seccion;
    }

    @Column(name = "fr")
    public Integer getFraccion() {
        return fraccion;
    }

    public void setFraccion(Integer fraccion) {
        this.fraccion = fraccion;
    }

    @Column(name = "frl")
    public String getFraccionLetra() {
        return fraccionLetra;
    }

    public void setFraccionLetra(String fraccionLetra) {
        this.fraccionLetra = fraccionLetra;
    }

    @Column(name = "mz")
    public Integer getManzana() {
        return manzana;
    }

    public void setManzana(Integer manzana) {
        this.manzana = manzana;
    }

    @Column(name = "mzl")
    public String getManzanaLetra() {
        return manzanaLetra;
    }

    public void setManzanaLetra(String manzanaLetra) {
        this.manzanaLetra = manzanaLetra;
    }

    @Column(name = "pc")
    public Integer getParcela() {
        return parcela;
    }

    public void setParcela(Integer parcela) {
        this.parcela = parcela;
    }

    @Column(name = "pcl")
    public String getParcelaLetra() {
        return parcelaLetra;
    }

    public void setParcelaLetra(String parcelaLetra) {
        this.parcelaLetra = parcelaLetra;
    }

    @Column(name = "uf")
    public String getUnidadFuncional() {
        return unidadFuncional;
    }

    public void setUnidadFuncional(String unidadFuncional) {
        this.unidadFuncional = unidadFuncional;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("C: ").append(circunscripcion);
        sb.append(" S: ").append(seccion);
        sb.append(" Fr: ").append(fraccion).append(" FrL: ").append(fraccionLetra);
        sb.append(" Mz: ").append(manzana).append(" MzL: ").append(manzanaLetra);
        sb.append(" Pc: ").append(parcela).append(" PcL: ").append(parcelaLetra);
        sb.append(" UF: ").append(unidadFuncional);
        return sb.toString();
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.circunscripcion, this.seccion, this.fraccion, this.fraccionLetra, this.manzana,
                this.manzanaLetra, this.parcela, this.parcelaLetra, this.unidadFuncional);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Nomenclatura other = (Nomenclatura) obj;
        return Objects.equals(this.circunscripcion, other.circunscripcion)
                && Objects.equals(this.seccion, other.seccion) && Objects.equals(this.fraccion, other.fraccion)
                && Objects.equals(this.fraccionLetra, other.fraccionLetra)
                && Objects.equals(this.manzana, other.manzana) && Objects.equals(this.manzanaLetra, other.manzanaLetra)
                && Objects.equals(this.parcela, other.parcela) && Objects.equals(this.parcelaLetra, other.parcelaLetra)
                && Objects.equals(this.unidadFuncional, other.unidadFuncional);
    }

    /**
     * Indica si la nomenclatura pertenece a una Unidad Funcional
     * 
     * @return
     */
    @Transient
    public boolean isUF() {
        if (getUnidadFuncional() == null) {
            return false;
        }
        if (getUnidadFuncional().equals("0000")) {
            return false;
        }
        return true;
    }

    @Transient
    public boolean mismoMacizo(Nomenclatura n) {
        return Objects.equals(this.circunscripcion, n.circunscripcion) && Objects.equals(this.seccion, n.seccion)
                && Objects.equals(this.fraccion, n.fraccion) && Objects.equals(this.fraccionLetra, n.fraccionLetra)
                && Objects.equals(this.manzana, n.manzana) && Objects.equals(this.manzanaLetra, n.manzanaLetra);
    }
}
