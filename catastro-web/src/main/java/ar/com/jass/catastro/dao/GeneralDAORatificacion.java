package ar.com.jass.catastro.dao;


import java.util.List;

import ar.com.jass.catastro.model.CuentaRatificacion;
import ar.com.jass.catastro.model.Nomenclatura;
import ar.com.jass.catastro.model.Parcela;
import ar.com.jass.catastro.model.ParcelaRatificacion;
import ar.com.jass.catastro.model.TitularRatificacion;
import ar.com.jass.catastro.model.Tramite;

public interface GeneralDAORatificacion {

    public List<Parcela> findParcelas(Nomenclatura nomenclatura);
    
    public List<Parcela> findParcelasPH(Nomenclatura nomenclatura);
    
    public List<ParcelaRatificacion> findParcelasRatificacion(Nomenclatura nomenclatura);
    
	public List<ParcelaRatificacion> findParcelasRatificacionRemove(Nomenclatura nomenclatura);
	
	public List<ParcelaRatificacion> findParcelasRatificacionNew(Nomenclatura nomenclatura);
    
    public CuentaRatificacion findCuentaRatificacionByCuenta(String cuenta);
    
    public List<CuentaRatificacion> findCuentasRatificacionByTramite(Tramite tramite);
    
    public List<TitularRatificacion> findTitulares(TitularRatificacion titular);
    
    public boolean exists(Nomenclatura nomenclatura);

}