package ar.com.jass.catastro.web.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;

import ar.com.jass.catastro.business.ConsultaService;
import ar.com.jass.catastro.model.error.ErrorLog;

/**
 * Login y Logout de la apliacion
 * 
 * @author msquarini
 *
 */
public class LoginAction extends BaseAction {

	private static final long serialVersionUID = 7353477345330099548L;

	private static final Logger LOG = LoggerFactory.getLogger("LoginAction");

	@Autowired
	private ConsultaService consultaService;

	private String username;

	private String password;

	// @Autowired
	private AuthenticationManager authenticationManager;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.opensymphony.xwork2.ActionSupport#input()
	 */
	public String input() {
		password = "";
		username = "";

		return "input";
	}

	/**
	 * Login
	 * 
	 * @return
	 */
	public String login() {
		LOG.info("catastro - login username {}", getUsername());

		Authentication authenticated = null;

		try {
			// Limpieza e invalida la session
			// invalidarSession();

			// Descomentar para autenticar
			/*
			 * KeycloakAuthenticationToken auth = (KeycloakAuthenticationToken)
			 * SecurityContextHolder.getContext().getAuthentication(); SimpleKeycloakAccount
			 * account = (SimpleKeycloakAccount) auth.getAccount();
			 * 
			 * System.out.println(account.getKeycloakSecurityContext().getIdToken().
			 * getPreferredUsername()); username =
			 * account.getKeycloakSecurityContext().getIdToken().getGivenName();
			 */
			// Seteo en session de informacion de uso multiple!!!!
			initCustomData();

			return "success";
		} catch (Exception e) {
			ErrorLog log = crearErrorLog("login", e.getMessage());
			LOG.error("login invalido, username: {}, {}", getUsername(), log, e);
			addActionError("Datos inválidados, intente nuevamente.");
			return "input";
		}

	}

	/**
	 * Logout
	 * 
	 * @return
	 */
	public String logout() {
		LOG.info("catastro - logout username {}", getUsername());

		try {
			// Limpieza e invalida la session
			invalidarSession();

			return "input";

		} catch (Exception e) {
			ErrorLog log = crearErrorLog("logout", e.getMessage());
			LOG.error("logout invalido, username: {}, {}", getUsername(), log, e);
			return "input";
		}
	}

	/**
	 * Remueve datos de session y la invalida
	 */
	private void invalidarSession() {
//		getSession().clear();
//		((SessionMap) getSession()).invalidate();
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public AuthenticationManager getAuthenticationManager() {
		return authenticationManager;
	}

	public void setAuthenticationManager(AuthenticationManager authenticationManager) {
		this.authenticationManager = authenticationManager;
	}

	public ConsultaService getConsultaService() {
		return consultaService;
	}

	public void setConsultaService(ConsultaService consultaService) {
		this.consultaService = consultaService;
	}
}
