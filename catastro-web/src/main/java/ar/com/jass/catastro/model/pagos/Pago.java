package ar.com.jass.catastro.model.pagos;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import ar.com.jass.catastro.model.EntityBase;

/**
 * Detalle de pago informado
 * 
 * @author MESquarini
 * 
 */
@Entity
@Table(name = "Pago")
public class Pago extends EntityBase implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -2828967031731919455L;

    /**
     * Id unico
     */
    private Long id;

    /**
     * Registro del file de pagos
     */
    private String fileDetail;

    private Date fechaPago;
    private Boolean procesado;
    // private Boolean valido;

    /**
     * Importe del pago
     */
    private BigDecimal importe;

    /**
     * Datos correspondientes a la deuda que se paga
     */
    private Date fechaVto;
    private Integer anio;
    private Integer cuota;
    private String cuenta;
    private String dv;

    /**
     * File en cual se informo el pago
     */
    private FilePago filePago;

    private Boolean sinDeudaRegistrada = Boolean.FALSE;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_pago_gen")
    @SequenceGenerator(name = "seq_pago_gen", sequenceName = "seq_pago", allocationSize = 1)
    @Column(name = "id_pago")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "procesado")
    public Boolean getProcesado() {
        return procesado;
    }

    public void setProcesado(Boolean procesado) {
        this.procesado = procesado;
    }

    @ManyToOne()
    @JoinColumn(name = "id_filepago")
    public FilePago getFilePago() {
        return filePago;
    }

    public void setFilePago(FilePago filePago) {
        this.filePago = filePago;
    }

    /*
     * @Column(name = "valido") public Boolean getValido() { return valido; }
     * 
     * public void setValido(Boolean valido) { this.valido = valido; }
     */

    @Column(name = "importe")
    public BigDecimal getImporte() {
        return importe;
    }

    public void setImporte(BigDecimal importe) {
        this.importe = importe;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "fecha_vto")
    public Date getFechaVto() {
        return fechaVto;
    }

    public void setFechaVto(Date fechaVto) {
        this.fechaVto = fechaVto;
    }

    @Column(name = "anio")
    public Integer getAnio() {
        return anio;
    }

    public void setAnio(Integer anio) {
        this.anio = anio;
    }

    @Column(name = "cuota")
    public Integer getCuota() {
        return cuota;
    }

    public void setCuota(Integer cuota) {
        this.cuota = cuota;
    }

    @Column(name = "cuenta")
    public String getCuenta() {
        return cuenta;
    }

    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }

    @Column(name = "dv")
    public String getDv() {
        return dv;
    }

    public void setDv(String dv) {
        this.dv = dv;
    }

    @Column(name = "file_line")
    public String getFileDetail() {
        return fileDetail;
    }

    public void setFileDetail(String fileDetail) {
        this.fileDetail = fileDetail;
    }

    @Column(name = "sin_deuda")
    public Boolean getSinDeudaRegistrada() {
        return sinDeudaRegistrada;
    }

    public void setSinDeudaRegistrada(Boolean sinDeudaRegistrada) {
        this.sinDeudaRegistrada = sinDeudaRegistrada;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "fecha_pago")
    public Date getFechaPago() {
        return fechaPago;
    }

    public void setFechaPago(Date fechaPago) {
        this.fechaPago = fechaPago;
    }

    @Override
    public String toString() {
        return "Pago [id=" + id + ", fileDetail=" + fileDetail + ", fechaPago=" + fechaPago + ", procesado=" + procesado
                + ", importe=" + importe + ", fechaVto=" + fechaVto + ", anio=" + anio + ", cuota=" + cuota
                + ", cuenta=" + cuenta + ", dv=" + dv + ", sinDeudaRegistrada=" + sinDeudaRegistrada + "]";
    }

}