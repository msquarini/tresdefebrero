package ar.com.jass.catastro.batch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;

/**
 * Lanzador de JOBs custom
 * 
 * @author msquarini
 *
 */
public class AsyncRunBatch {

    private static final Logger LOG = LoggerFactory.getLogger("AsyncRunBatch");

    private JobLauncher jobLauncher;
    private Job job;
    
    public void run() {
        try {
        	LOG.info("Lanzando: Job {} ", job);
            JobParameters param = new JobParametersBuilder().toJobParameters();
            JobExecution execution = jobLauncher.run(getJob(), param);
            
            LOG.info("Proceso: Job {} - Estado {} ", job, execution.getStatus());
        } catch (Exception e) {
            LOG.error("Error en lanzamiento de job {}", job, e);
        }
    }

    public JobLauncher getJobLauncher() {
        return jobLauncher;
    }

    public void setJobLauncher(JobLauncher jobLauncher) {
        this.jobLauncher = jobLauncher;
    }

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }
}
