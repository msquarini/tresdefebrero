package ar.com.jass.catastro.model;

public interface IParcela {

    public Long getId();

    public Nomenclatura getNomenclatura();
}
