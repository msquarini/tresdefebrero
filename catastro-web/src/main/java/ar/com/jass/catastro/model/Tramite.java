package ar.com.jass.catastro.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

/**
 * Gestion sobre una cuenta Informacion del tramite en cuestion con la/s cuentas origenes y
 * referencia a las nuevas cuentas
 * 
 * @author MESquarini
 * 
 */
/**
 * @author msquarini
 *
 */
@Entity
@Table(name = "Tramite")
public class Tramite extends EntityBase implements Serializable {

    /**
     * Id unico de gestion
     */
    private Long id;

    /**
     * Codigo de tramite
     */
    private Integer codigo;

    private TipoTramite tipo;

    /**
     * Observaciones del tramite
     */
    private String observacion;

    /**
     * FIJO
     */
    private Integer partido;

    private Integer numero;
    private Integer anio;
    private Integer codOrganismo;
    private Integer folio;

    /**
     * Cuenta/s origen para iniciar el tramite 0 | 1 | N
     */
    private List<Cuenta> cuentasOrigen;

    /**
     * Cuentas generadas a partir del tramite
     */
    private List<Cuenta> cuentas;

    private EstadoTramite estado;

    /**
     * Fecha de vigencia de las parcelas del tramite
     */
    private Date vigencia;

    /**
     * Indica si se trata de un tramite de oficio
     */
    private Boolean oficio;

    public Tramite() {
        this.partido = 111;
        this.estado = EstadoTramite.PENDIENTE;
        this.vigencia = new Date();
        this.oficio = Boolean.FALSE;

        // this.cuentasOrigen = new ArrayList<Cuenta>();
        // this.cuentas = new ArrayList<Cuenta>();
    }

    public Tramite(TipoTramite tipo) {
        this();
        this.tipo = tipo;
        // this.codigo = tipo.getCodigo();
        // this.descripcion = tipo.getDescripcion();
    }

    public Tramite(TipoTramite tipo, List<Cuenta> cuentas, List<Cuenta> origenes) {
        this(tipo);
        for (Cuenta c : cuentas) {
            c.setBloqueada(Boolean.TRUE);
        }
        this.cuentas = cuentas;
        for (Cuenta c : origenes) {
            c.setBloqueada(Boolean.TRUE);
        }
        this.cuentasOrigen = origenes;
        this.vigencia = Calendar.getInstance().getTime();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_tramite_gen")
    @SequenceGenerator(name = "seq_tramite_gen", sequenceName = "seq_tramite", allocationSize = 1)
    @Column(name = "id_tramite")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "partido")
    public Integer getPartido() {
        return partido;
    }

    public void setPartido(Integer partido) {
        this.partido = partido;
    }

    @Column(name = "numero")
    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    @Column(name = "anio")
    public Integer getAnio() {
        return anio;
    }

    public void setAnio(Integer anio) {
        this.anio = anio;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id, this.codigo, this.numero);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Tramite other = (Tramite) obj;
        return Objects.equals(this.id, other.id) && Objects.equals(this.numero, other.numero)
                && Objects.equals(this.codigo, other.codigo);
    }

    @Column(name = "cod_organismo")
    public Integer getCodOrganismo() {
        return codOrganismo;
    }

    public void setCodOrganismo(Integer codOrganismo) {
        this.codOrganismo = codOrganismo;
    }

    @Column(name = "folio")
    public Integer getFolio() {
        return folio;
    }

    public void setFolio(Integer folio) {
        this.folio = folio;
    }

    @Column(name = "codigo")
    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "tramite_cuenta_origen",
            joinColumns = { @JoinColumn(name = "id_tramite", nullable = false, updatable = false) },
            inverseJoinColumns = { @JoinColumn(name = "id_cuenta", nullable = false, updatable = false) })
    @Cascade(CascadeType.ALL)
    public List<Cuenta> getCuentasOrigen() {
        return cuentasOrigen;
    }

    public void setCuentasOrigen(List<Cuenta> cuentasOrigen) {
        this.cuentasOrigen = cuentasOrigen;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "tramite", orphanRemoval = false)
    // @Cascade(CascadeType.DELETE)
    public List<Cuenta> getCuentas() {
        return cuentas;
    }

    public void setCuentas(List<Cuenta> cuentas) {
        this.cuentas = cuentas;
    }

    /*
     * @Column(name = "descripcion") public String getDescripcion() { return descripcion; }
     * 
     * public void setDescripcion(String descripcion) { this.descripcion = descripcion; }
     */

    @Enumerated
    @Column(name = "tipo")
    public TipoTramite getTipo() {
        return tipo;
    }

    public void setTipo(TipoTramite tipo) {
        this.tipo = tipo;
        // this.codigo = tipo.getCodigo();
        // this.descripcion = tipo.getDescripcion();
    }

    @Override
    public String toString() {
        return "Tramite [id=" + id + ", codigo=" + codigo + ", tipo=" + tipo + ", partido=" + partido + ", numero="
                + numero + ", anio=" + anio + ", codOrganismo=" + codOrganismo + ", oficio=" + oficio + ", folio="
                + folio + "]";
    }

    @Enumerated(EnumType.STRING)
    @Column(name = "estado")
    public EstadoTramite getEstado() {
        return estado;
    }

    public void setEstado(EstadoTramite estado) {
        this.estado = estado;
    }

    /**
     * Nuevas parcelas a partir del tramite
     * 
     * @return
     */
    @Transient
    public List<Parcela> getParcelas() {
        List<Parcela> parcelas = new ArrayList<>();
        for (Cuenta c : cuentas) {
            parcelas.add(c.getParcela());
        }
        return parcelas;
    }

    /**
     * Parcelas origen del tramite
     * 
     * @return
     */
    @Transient
    public List<Parcela> getParcelasOrigen() {
        List<Parcela> parcelas = new ArrayList<>();
        for (Cuenta c : cuentasOrigen) {
            parcelas.add(c.getParcela());
        }
        return parcelas;
    }

    @Column(name = "vigencia")
    public Date getVigencia() {
        return vigencia;
    }

    public void setVigencia(Date vigencia) {
        this.vigencia = vigencia;
    }

    @Column(name = "oficio")
    public Boolean getOficio() {
        return oficio;
    }

    public void setOficio(Boolean oficio) {
        this.oficio = oficio;
    }

    @Column(name = "observacion")
    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }
}
