package ar.com.jass.catastro.util;

import org.hibernate.Session;
import org.hibernate.tuple.ValueGenerator;

public class LoggedUserGenerator implements ValueGenerator<String> {

    @Override
    public String generateValue(Session session, Object owner) {
        // return LoggedUser.get();
        return "Martin" + Math.random();
    }
}
