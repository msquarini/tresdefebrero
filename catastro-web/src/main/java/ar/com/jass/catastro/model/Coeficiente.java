package ar.com.jass.catastro.model;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Coeficientes a utilizar por categoria para el calculo de tasa
 * 
 * Por anio y fecha de vigencia Podria haber cambios de coeficientes en el mismo
 * anio
 * 
 * @author mesquarini
 *
 */
public class Coeficiente {

	private String categoria;

	private BigDecimal coeficiente;

	private Integer anio;

	private Date fechaVigencia;

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public BigDecimal getCoeficiente() {
		return coeficiente;
	}

	public void setCoeficiente(BigDecimal coeficiente) {
		this.coeficiente = coeficiente;
	}

	public Integer getAnio() {
		return anio;
	}

	public void setAnio(Integer anio) {
		this.anio = anio;
	}

	public Date getFechaVigencia() {
		return fechaVigencia;
	}

	public void setFechaVigencia(Date fechaVigencia) {
		this.fechaVigencia = fechaVigencia;
	}

}
