package ar.com.jass.catastro.batch.pagos;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.core.io.Resource;
import org.springframework.util.Assert;

import ar.com.jass.catastro.model.pagos.EstadoFilePagos;
import ar.com.jass.catastro.model.pagos.FilePago;
import ar.com.jass.catastro.model.pagos.FilePagosRecord;
import ar.com.jass.catastro.model.pagos.Pago;

/**
 * Lectura de archivo de pagos. Completa la lectura de todo el archivo y valida
 * consistencia de estructura Cantidad de registros x file baja (100 aprox)
 * 
 * @author msquarini
 *
 */
public class PagosReader implements ItemReader<FilePago> {

	private static final Logger LOG = LoggerFactory.getLogger("PagosReader");

	private FilePago fileToProcess;

	private FlatFileItemReader<FilePagosRecord> delegate;

	@BeforeStep
	public void retrieveInterstepData(StepExecution stepExecution) {
		JobExecution jobExecution = stepExecution.getJobExecution();
		ExecutionContext jobContext = jobExecution.getExecutionContext();

		fileToProcess = (FilePago) jobContext.get("filepago");
		Resource resource = new org.springframework.core.io.FileSystemResource((String) jobContext.get("pathtofile"));
		delegate.setResource(resource);
		
		fileToProcess.setPagos(new ArrayList<Pago>());
	    LOG.info("(INIT) Leyendo {}", fileToProcess);
	}

	public FilePago read() throws Exception {
		Boolean header = Boolean.FALSE;

		for (FilePagosRecord record = null; (record = this.delegate.read()) != null;) {
			LOG.info("(REGPAGO), {}", record);

			if (record.isHeader()) {
				header = Boolean.TRUE;
				record.complete(fileToProcess);
			} else if (record.isDetail()) {
				Assert.isTrue(header, "No header was found.");
				record.complete(fileToProcess);
			} else if (record.isTrailer()) {
				Assert.isTrue(header, "No header was found.");
				record.complete(fileToProcess);

				if (!fileToProcess.getCantidadRegistros().equals(fileToProcess.getPagos().size())) {
					fileToProcess.setEstado(EstadoFilePagos.ERROR);
					throw new IllegalStateException("No coinciden registros informados y pagos");
				}
				
				return fileToProcess;
			}
		}
		Assert.notNull(fileToProcess.getFileTrailer(), "No 'Trailer' was found.");
		return null;
	}

	public FlatFileItemReader<FilePagosRecord> getDelegate() {
		return delegate;
	}

	public void setDelegate(FlatFileItemReader<FilePagosRecord> delegate) {
		this.delegate = delegate;
	}

	public FilePago getFileToProcess() {
		return fileToProcess;
	}

	public void setFileToProcess(FilePago fileToProcess) {
		this.fileToProcess = fileToProcess;
	}

}