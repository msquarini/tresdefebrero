package ar.com.jass.catastro.web.action;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;

import ar.com.jass.catastro.business.CatastroService;
import ar.com.jass.catastro.business.ConsultaService;
import ar.com.jass.catastro.model.EstadoTramite;
import ar.com.jass.catastro.model.TipoTramite;
import ar.com.jass.catastro.model.Tramite;
import ar.com.jass.catastro.model.error.ErrorLog;
import ar.com.jass.catastro.web.commons.WebConstants;

/**
 * Gestion de tramites
 * 
 * @author msquarini
 *
 */
public class TramiteAction extends GridBaseAction<Tramite> {

    private static final long serialVersionUID = 7353477345330099548L;

    private static final Logger LOG = LoggerFactory.getLogger("TramiteAction");

    @Autowired
    private ConsultaService consultaService;

    @Autowired
    private CatastroService catastroService;

    /**
     * Indica si la busqueda es por fecha, tipo, usuario
     */
    private Integer searchBy;

    /**
     * Busquedas
     */
    private String usuario;
    private EstadoTramite estado;
    private Date from;
    private Date to;

    /**
     * ID para buscar el detalle del tramite
     */
    private Long idTramite;
    private Tramite tramite;

    /*
     * (non-Javadoc)
     * 
     * @see com.opensymphony.xwork2.ActionSupport#input()
     */
   
    @PreAuthorize("hasAnyAuthority('catastro_consulta', 'catastro_operador')")   
    public String input() {
        usuario = "";
        from = new Date();
        to = new Date();
        search();
        return "input";
    }

    @PreAuthorize("hasAnyAuthority('catastro_supervisor', 'catastro_operador')")
    public String cargar() {
        LOG.info("Cargando tramite id: {}", idTramite);

        tramite = consultaService.findTramite(idTramite, "cuentas.parcela", "cuentasOrigen.responsables",
                "cuentasOrigen.parcela.titulares", "cuentasOrigen.parcela.domicilioInmueble",
                "cuentas.parcela.domicilioInmueble", "cuentasOrigen.parcela.lineas");
        setInSession(WebConstants.TRAMITE, tramite);

        if (TipoTramite.SUB.equals(tramite.getTipo())) {
            return "goToSubdivision";
        }
        if (TipoTramite.UNI.equals(tramite.getTipo())) {
            return "goToUnificacion";
        }
        if (TipoTramite.RAT.equals(tramite.getTipo())) {
            return "goToRatificacion";
        }
        if (TipoTramite.TIT.equals(tramite.getTipo())) {
            return "goToTitular";
        }
        if (TipoTramite.RES.equals(tramite.getTipo())) {
            return "goToResponsable";
        }
        if (TipoTramite.LIN.equals(tramite.getTipo())) {
            return "goToLinea";
        }
        return "error";
    }

    /**
     * Busqueda de tramites con filtros
     * 
     * @return
     */
    public String search() {
        LOG.info("Searching tramites: desde {} hasta {} estado {} usuario {}", from, to, estado, usuario);
        List<Tramite> tramites = consultaService.getTramites(from, to, estado, usuario);
        setInSession(WebConstants.TRAMITES_DATA, tramites);
        return "json";
    }

    /**
     * Info detallada del tramite, obtenido de los tramites en grilla
     * 
     * @return
     */
    public String info() {
        LOG.info("Detalle de tramite ID {}", getIdTramite());
        List<Tramite> data = (List<Tramite>) getFromSession(WebConstants.TRAMITES_DATA);
        for (Tramite t : data) {
            if (t.getId().equals(getIdTramite())) {
                tramite = t;
                break;
            }
        }
        return "info-tramite";
    }

    @PreAuthorize("hasAuthority('catastro_supervisor')")
    public String cancelar() {
        LOG.info("CANCELANDO tramite id: {}", idTramite);
        try {
            // TODO para probar mensajes de error
            // int i = 1/0;
            catastroService.cancelarTramite(idTramite);
        } catch (Exception e) {            
            ErrorLog log = crearErrorLog("cancelar", e.getMessage());
            addActionError("error en cancelacion de tramite");
            LOG.error("{}", log, e);
            return "errorJSON";
        }

        return "cancelar";
    }

    /**
     * Llamada AJAX para obtener la informacion para la grilla en formato JSON
     * 
     * @return
     */
    @SuppressWarnings("unchecked")
    public String getData() {
        LOG.info("getData");
        try {
            List<Tramite> data = (List<Tramite>) getFromSession(WebConstants.TRAMITES_DATA);
            setData(data);
        } catch (Exception e) {
            LOG.error("error en getData tramites", e);
        }
        return "dataJSON";
    }

    public Integer getSearchBy() {
        return searchBy;
    }

    public void setSearchBy(Integer searchBy) {
        this.searchBy = searchBy;
    }

    public Long getIdTramite() {
        return idTramite;
    }

    public void setIdTramite(Long idTramite) {
        this.idTramite = idTramite;
    }

    public Tramite getTramite() {
        return tramite;
    }

    public void setTramite(Tramite tramite) {
        this.tramite = tramite;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public EstadoTramite getEstado() {
        return estado;
    }

    public void setEstado(EstadoTramite estado) {
        this.estado = estado;
    }

    public Date getFrom() {
        return from;
    }

    public void setFrom(Date from) {
        this.from = from;
    }

    public Date getTo() {
        return to;
    }

    public void setTo(Date to) {
        this.to = to;
    }

    public ConsultaService getConsultaService() {
        return consultaService;
    }

    public void setConsultaService(ConsultaService consultaService) {
        this.consultaService = consultaService;
    }

    public CatastroService getCatastroService() {
        return catastroService;
    }

    public void setCatastroService(CatastroService catastroService) {
        this.catastroService = catastroService;
    }
}