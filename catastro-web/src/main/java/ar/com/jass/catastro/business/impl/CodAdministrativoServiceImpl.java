package ar.com.jass.catastro.business.impl;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.com.jass.catastro.business.CodAdministrativoService;
import ar.com.jass.catastro.dao.CuentaCorrienteDAO;
import ar.com.jass.catastro.dao.GeneralDAO;
import ar.com.jass.catastro.model.CodigoAdministrativo;

@Service
public class CodAdministrativoServiceImpl implements CodAdministrativoService {

	private static final Logger LOG = LoggerFactory.getLogger(CodAdministrativoServiceImpl.class);

	@Autowired
	private GeneralDAO generalDAO;

	@Autowired
	private CuentaCorrienteDAO ccDAO;

	@PostConstruct
	public void initialize() {
		/*
		 * tasaPorCategoria = new HashMap<Integer, TasaCategoria>(); reload();
		 */
	}

	public Integer getCodigoCuota() {
		return 0;
	}

	public CuentaCorrienteDAO getCcDAO() {
		return ccDAO;
	}

	public void setCcDAO(CuentaCorrienteDAO ccDAO) {
		this.ccDAO = ccDAO;
	}

	@Override
	@Transactional
	public CodigoAdministrativo find(Long id) {
		return generalDAO.find(CodigoAdministrativo.class, id);
	}

	@Override
	@Transactional
	public void saveOrUpdate(CodigoAdministrativo codigoAdministrativo) {
		generalDAO.saveOrUpdate(codigoAdministrativo);
	}

	/*
	 * public void reload() { LOG.info("Reloading tasas por categoria...");
	 * 
	 * tasaPorCategoria.clear();
	 * 
	 * Calendar now = Calendar.getInstance(); List<TasaCategoria> tasas =
	 * ccDAO.findTasas(now.get(Calendar.YEAR)); for (TasaCategoria tasa : tasas) {
	 * tasaPorCategoria.put(tasa.getCategoria(), tasa); } }
	 */

}
