package ar.com.jass.catastro.util;

import java.util.Calendar;
import java.util.Date;

public class DateHelper {

    public static void main(String[] args) {
        Date d = new Date();
        Date h = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(d);
        c.set(Calendar.MONTH, 0);
        c.set(Calendar.DATE, 31);
        System.out.println(mesesEntreFechas(c.getTime(), h));
    }

    public static Date getFormattedFromDateTime(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        return cal.getTime();
    }

    public static Date getFormattedToDateTime(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        return cal.getTime();
    }

    public static int mesesEntreFechas(Date from, Date to) {
        Calendar fromCal = Calendar.getInstance();
        fromCal.setTime(from);
        fromCal.set(Calendar.DATE, 1);

        Calendar toCal = Calendar.getInstance();
        toCal.setTime(to);

        // Calculo de meses de diferencia para aplicar interes
        int diffYear = toCal.get(Calendar.YEAR) - fromCal.get(Calendar.YEAR);
        int diffMonth = (diffYear * 12) + toCal.get(Calendar.MONTH) - fromCal.get(Calendar.MONTH);

        return diffMonth;
    }

    public static int getAnioFecha(Date fecha) {
        Calendar c = Calendar.getInstance();
        c.setTime(fecha);
        return c.get(Calendar.YEAR);
    }

    public static int getMesFecha(Date fecha) {
        Calendar c = Calendar.getInstance();
        c.setTime(fecha);
        return c.get(Calendar.MONTH) + 1;
    }
}
