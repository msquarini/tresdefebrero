package ar.com.jass.catastro.batch.deuda.file;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;

import ar.com.jass.catastro.business.CuentaCorrienteService;
import ar.com.jass.catastro.dao.GeneralDAO;
import ar.com.jass.catastro.model.cuentacorriente.Boleta;
import ar.com.jass.catastro.model.cuentacorriente.Periodo;

/**
 * Buscar los registros de deuda para la cuenta
 * 
 * @author mesquarini
 *
 */
public class DeudaFileProcessor implements ItemProcessor<CuentaDTO, CuentaDTO> {

    private static final Logger LOG = LoggerFactory.getLogger("DeudaFileProcessor");

    @Autowired
    private GeneralDAO generalDAO;

    @Autowired
    private CuentaCorrienteService ccService;

    private Periodo periodo;

    @BeforeStep
    public void retrieveInterstepData(StepExecution stepExecution) {
        JobExecution jobExecution = stepExecution.getJobExecution();
        ExecutionContext jobContext = jobExecution.getExecutionContext();
        setPeriodo((Periodo) jobContext.get("periodo"));
    }

    @Override
    public CuentaDTO process(CuentaDTO item) throws Exception {
        LOG.debug("Procesando Generacion File Deuda, {}", item);

        /**
         * BUscamos las boletas generadas para el periodo
         */

        List<Boleta> boletas = (List<Boleta>) ccService.findBoletas(item.getId(), periodo);
        // Agrgar en el DTO las boletas!

        return item;
    }

    public GeneralDAO getGeneralDAO() {
        return generalDAO;
    }

    public void setGeneralDAO(GeneralDAO generalDAO) {
        this.generalDAO = generalDAO;
    }

    public CuentaCorrienteService getCcService() {
        return ccService;
    }

    public void setCcService(CuentaCorrienteService ccService) {
        this.ccService = ccService;
    }

    public Periodo getPeriodo() {
        return periodo;
    }

    public void setPeriodo(Periodo periodo) {
        this.periodo = periodo;
    }
}
