package ar.com.jass.catastro.business;

import java.math.BigDecimal;
import java.util.List;

import ar.com.jass.catastro.model.cuentacorriente.AjusteDTO;
import ar.com.jass.catastro.model.cuentacorriente.CuentaCorriente;
import ar.com.jass.catastro.model.cuentacorriente.GeneracionDeudaDTO;
import ar.com.jass.catastro.model.cuentacorriente.Periodo;
import ar.com.jass.catastro.web.dto.CCTotalizadorDTO;

/**
 * Servicios de generacion de cuotas
 * 
 * @author msquarini
 *
 */
public interface DeudaService {

    /**
     * Calculo del valor total de cuota para una categoria y valuacion
     * 
     * @param categoria
     * @param valuacion
     * @return
     */
    //public BigDecimal calculoValorCuotaTotal(Integer categoria, BigDecimal valuacion);

    /**
     * Realiza el calculo de cuota pura segun Categoria y valuacion
     * 
     * @param categoria
     * @param valuacion
     * @return
     */
    //public BigDecimal calculoValorCuota(Integer categoria, BigDecimal valuacion);

    /**
     * Calculo de la tasa de seguridad
     * 
     * @param categoria
     * @param monto
     * @return
     */
    //public BigDecimal calculoValorTasaSeguridad(Integer categoria, BigDecimal monto);

    /**
     * Crea el movimiento de debito de la cuota del periodo para la cuenta DTO
     * 
     * @param dto
     * @param periodo
     * @return
     */
    public CuentaCorriente crearCuotaCuenta(GeneracionDeudaDTO dto, Periodo periodo);

    /**
     * Calcular credito de la diferencia entre el calculo de deuda y deuda año anterior. Para
     * cuentas que presentan flag CONTOPE
     * 
     * @param deuda
     * @param periodo
     * @return
     */
    public CuentaCorriente aplicarTope(CuentaCorriente deuda, Periodo periodo);

    public CuentaCorriente crearCuotaCuenta(Periodo periodo, Integer categoria, Long idCuenta, BigDecimal valuacion);

    /**
     * @param original
     * @param ajusteDTO
     * @param vigente
     * @return
     */
    // public List<CuentaCorriente> crearMovimientoAjuste(CuentaCorriente original, AjusteDTO
    // ajusteDTO, Periodo vigente);
    public List<CuentaCorriente> crearMovimientoAjuste(AjusteDTO ajusteDTO, Periodo vigente);

}
