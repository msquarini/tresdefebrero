package ar.com.jass.catastro.batch.deuda;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;

import ar.com.jass.catastro.batch.deuda.BatchSemaphore.BATCH_PROCESS;
import ar.com.jass.catastro.model.cuentacorriente.DeudaPeriodoStatus;
import ar.com.jass.catastro.model.cuentacorriente.Periodo;

/**
 * Lanzador de JOB de generacion de deuda
 * 
 * Se procesan los movimientos de debito de todas la cuentas para todos los periodos en estado
 * A_PROCESAR
 * 
 * @author msquarini
 *
 */
public class GeneracionDeudaRunBatch implements Runnable {

    private static final Logger LOG = LoggerFactory.getLogger(GeneracionDeudaRunBatch.class);

    private JobLauncher jobLauncher;
    private Job job;

    private List<Periodo> periodos;

    @Override
    public void run() {

        // if (EmisionDeudaSemaphore.puedoProcesar()) {
        if (BatchSemaphore.puedoProcesar(BATCH_PROCESS.DEUDA)) {
            LOG.info("Lanzando: Job {} ", job);

            for (Periodo periodo : periodos) {
                try {
                    if (periodo.getEstado().equals(DeudaPeriodoStatus.A_PROCESAR)) {
                        BatchSemaphore.startProceso(BATCH_PROCESS.DEUDA);

                        JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
                        jobParametersBuilder.addLong("idPeriodo", periodo.getId());
                        jobParametersBuilder.addLong("unique", System.currentTimeMillis());
                        JobParameters params = jobParametersBuilder.toJobParameters();
                        JobExecution execution = jobLauncher.run(getJob(), params);

                        LOG.info("Proceso: Job {} - Estado {} ", job, execution.getStatus());
                    }
                } catch (Exception e) {
                    LOG.error("Error en lanzamiento de job {}", job, e);
                    BatchSemaphore.finProcesamiento(BATCH_PROCESS.DEUDA);
                }
            }
            LOG.info("Finalizacion: Job {} ", job);
            BatchSemaphore.finProcesamiento(BATCH_PROCESS.DEUDA);
        } else {
            LOG.warn("Procesamiento de deuda en ejecucion");
            throw new IllegalStateException("emision.en.ejecucion");
        }        
    }

    /**
     * Emision de ajuste cuota 1 Una vez finalizac
     */

    /**
     * COMO procesar la cuota anual
     * 
     * NO HAY CUOTA ANUAL, se genera la boleta anual, con la deuda de los periodos pendientes!
     */
    @Deprecated
    public void runCuotaAnual(Periodo periodo) {
        try {
            LOG.info("Lanzando: Job {} ", job);

            if (periodo.isGenerarAnual()) {
                JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
                jobParametersBuilder.addLong("idPeriodo", periodo.getId());
                jobParametersBuilder.addString("cuotaAnual", "true");

                JobParameters params = jobParametersBuilder.toJobParameters();
                JobExecution execution = jobLauncher.run(getJob(), params);

                LOG.info("Proceso: Job {} - Estado {} ", job, execution.getStatus());
            } else {
                LOG.info("Periodo no genera cuota anual, {}", periodo);
            }
        } catch (Exception e) {
            LOG.error("Error en lanzamiento de job {}", job, e);
        }
    }

    public JobLauncher getJobLauncher() {
        return jobLauncher;
    }

    public void setJobLauncher(JobLauncher jobLauncher) {
        this.jobLauncher = jobLauncher;
    }

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }

    public void run(List<Periodo> periodos) {
        if (BatchSemaphore.puedoProcesar(BATCH_PROCESS.DEUDA)) {
            this.periodos = periodos;
        } else {
            throw new IllegalStateException("emision.en.ejecucion");
        }
    }
}
