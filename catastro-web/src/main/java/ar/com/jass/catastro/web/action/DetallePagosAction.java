package ar.com.jass.catastro.web.action;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import ar.com.jass.catastro.business.ConsultaService;
import ar.com.jass.catastro.business.PagosService;
import ar.com.jass.catastro.model.pagos.FilePago;
import ar.com.jass.catastro.model.pagos.Pago;
import ar.com.jass.catastro.web.commons.WebConstants;

/**
 * Informacion de detalle de archivos de pagos
 * 
 * @author msquarini
 *
 */
public class DetallePagosAction extends GridBaseAction<Pago> {

	private static final long serialVersionUID = 7353477345330099548L;

	private static final Logger LOG = LoggerFactory.getLogger("DetallePagosAction");

    @Autowired
    private PagosService pagosService;

    @Autowired
    private ConsultaService consultaService;
    
	private Long idFilePago;
	private FilePago filePago;
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.opensymphony.xwork2.ActionSupport#input()
	 */
	public String input() {
	    LOG.info("Detalle de archivo de pagos, ID {}", idFilePago);
	    
	    filePago = pagosService.getFilePago(idFilePago, "pagos");
	    
	    setInSession(WebConstants.PAGOS_DATA, filePago.getPagos());
		return "grid";
	}

	/**
	 * Llamada AJAX para obtener la informacion para la grilla en formato JSON
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public String getData() {
		LOG.info("getData");
		try {
			List<Pago> data = (List<Pago>) getFromSession(WebConstants.PAGOS_DATA);
			setData(data);
		} catch (Exception e) {
			LOG.error("error en getData detalle de archivos de pagos", e);
		}
		return "dataJSON";
	}

    public Long getIdFilePago() {
        return idFilePago;
    }

    public void setIdFilePago(Long idFilePago) {
        this.idFilePago = idFilePago;
    }

    public FilePago getFilePago() {
        return filePago;
    }

    public void setFilePago(FilePago filePago) {
        this.filePago = filePago;
    }

}