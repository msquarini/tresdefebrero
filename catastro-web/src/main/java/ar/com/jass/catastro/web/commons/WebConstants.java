package ar.com.jass.catastro.web.commons;

public interface WebConstants {

    public static final String TRAMITE = "tramite";
    
    /**
     * Cuenta con la que se opera
     */
    public static final String CUENTA = "cuenta";
    
    /**
     * Flag que indica si la cuenta presenta deuda
     */
    public static final String CUENTA_CON_DEUDA = "cuenta_con_deuda";

    /**
     * Cuenta origen en un tramite
     */
    public static final String CUENTA_ORIGEN = "cuenta_origen";
    public static final String PARCELAS_DATA = "grid-parcelas";

    /**
     * Grilla de titulares para consultas y seleccion
     */
    public static final String TITULARES_DATA = "grid-titulares";

    /**
     * Grilla de resonsables de cuenta
     */
    public static final String RESPONSABLES_ADD = "grid-responsables";

    /**
     * Grilla de tramites para consultas y seleccion
     */
    public static final String TRAMITES_DATA = "grid-tramites";

    /**
     * Grilla de lineas valuatorias
     */
    public static final String LINEAS_DATA = "grid-lineas";

    /**
     * Grilla con titulares para persistir, ya sea actualizar o nuevos
     */
    public static final String TITULARES_ADD = "grid-titulares_add";

    // private static final String EDIT_TITULAR = "titular_edition";

    /**
     * Grilla de files de pagos
     */
    public static final String FILEPAGOS_DATA = "grid-filepagos";
    public static final String PAGOS_DATA = "grid-pagos";

    public static final String NOMENCLATURA = "nomenclatura";

    /**
     * Grilla de cuenta corriente
     */
    public static final String CC_DATA = "grid-cc_grupador";

    public static final String CC_MOV_DATA = "grid-cc_movimientos";

    /**
     * Grilla de boletas de pago
     */
    public static final String BOLETA_DATA = "grid-boletas";
    
    /**
     * Grilla de codigos administrativos
     */
    public static final String COD_ADM_DATA = "grid-codigosAdministativos";
    
    /**
     * Codigo Administrativo bean para edicion 
     */
    public static final String COD_ADM = "codadm";
    
}
