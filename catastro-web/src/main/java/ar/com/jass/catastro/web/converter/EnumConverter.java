package ar.com.jass.catastro.web.converter;

import java.util.Map;

import org.apache.struts2.util.StrutsTypeConverter;

/**
 * Custom Enum converter Convierte desde la representacion String del
 * enumerativo o el valor ordinal del mismo
 * 
 * @author mesquarini
 *
 */
public class EnumConverter extends StrutsTypeConverter {

	@Override
	public Object convertFromString(Map context, String[] values, Class toClass) {
		String v = values[0];
		// Si la busqueda es por ordinal
		try {
			Integer ord = new Integer(v);
			return toClass.getEnumConstants()[ord];
		} catch (Exception e) {
		}

		return Enum.valueOf(toClass, values[0]);
	}

	@Override
	public String convertToString(Map context, Object o) {
		return o.toString();
	}

}