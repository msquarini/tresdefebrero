package ar.com.jass.catastro.batch.deuda.file.callback;

import java.util.List;

import ar.com.jass.catastro.batch.deuda.file.DeudaDTO;

public interface DeudaFileCallback {

    public void notify(List<DeudaDTO> deudas);
}
