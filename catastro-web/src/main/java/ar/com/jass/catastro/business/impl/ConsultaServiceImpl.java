package ar.com.jass.catastro.business.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.com.jass.catastro.business.ConsultaService;
import ar.com.jass.catastro.dao.GeneralDAO;
import ar.com.jass.catastro.model.Cuenta;
import ar.com.jass.catastro.model.EstadoTramite;
import ar.com.jass.catastro.model.IParcela;
import ar.com.jass.catastro.model.Nomenclatura;
import ar.com.jass.catastro.model.Parcela;
import ar.com.jass.catastro.model.Titular;
import ar.com.jass.catastro.model.Tramite;
import ar.com.jass.catastro.model.temporal.LineaTemporal;
import ar.com.jass.catastro.util.HibernateHelper;

@Service
public class ConsultaServiceImpl implements ConsultaService {

    private static final Logger LOG = LoggerFactory.getLogger("ConsultaServiceImpl");

    @Autowired
    @Qualifier("generalDAO")
    private GeneralDAO generalDAO;

    @Override
    @Transactional
    public List<?> findAll(Class<?> clazz) {
        return generalDAO.getAll(clazz);
    }

    @Override
    @Transactional
    public Tramite findTramite(Long id, String... initialize) {
        Tramite t = generalDAO.find(Tramite.class, id);
        HibernateHelper.initialize(t, initialize);
        return t;
    }

    @Override
    @Transactional
    public Cuenta findCuenta(String cuenta, String dv, String... initialize) {
        Cuenta c = generalDAO.findCuenta(cuenta, dv);
        HibernateHelper.initialize(c, initialize);
        return c;
    }

    @Override
    @Transactional
    public List<Cuenta> findCuentasLike(String cuenta) {
        return generalDAO.findCuentasLike(cuenta);
    }

    @Override
    @Transactional
    public Cuenta findCuentaById(Long id, String... initialize) {
        Cuenta c = generalDAO.find(Cuenta.class, id);
        HibernateHelper.initialize(c, initialize);
        return c;
    }

    @Override
    @Transactional
    public Titular findTitularById(Long id, String... initialize) {
        Titular t = generalDAO.find(Titular.class, id);
        HibernateHelper.initialize(t, initialize);
        return t;
    }

    @Override
    @Transactional
    public List<Titular> findTitulares(Titular titular) {
        return generalDAO.findTitulares(titular);
    }

    @Override
    @Transactional
    public List<Parcela> findParcelas(Nomenclatura nomenclatura, String... initialize) {
        List<Parcela> parcelas = generalDAO.findParcelas(nomenclatura);

        HibernateHelper.initialize(parcelas, initialize);

        return parcelas;
    }

    @Override
    @Transactional
    public List<Tramite> findTramitesByCuenta(Cuenta cuenta, String... initialize) {
        List<Tramite> tramites = generalDAO.findTramites(cuenta.getId());

        // Se agrega el tramite que origino la cuenta en cuestion
        if (cuenta.getTramite() != null) {            
            tramites.add(cuenta.getTramite());
        }

        HibernateHelper.initialize(tramites, initialize);

        return tramites;
    }

    @Override
    @Transactional
    public Parcela findParcelaById(Long id, String... initialize) {
        Parcela p = generalDAO.find(Parcela.class, id);
        HibernateHelper.initialize(p, initialize);
        return p;
    }

    @Override
    @Transactional
    public Parcela findParcelaByPartida(Integer partida) {
        return generalDAO.findParcelaByPartida(partida);
    }

    /*
     * (non-Javadoc)
     * 
     * @see ar.com.jass.catastro.business.ConsultaService#exists(ar.com.jass.catastro.
     * model.Nomenclatura)
     */
    @Override
    @Transactional
    public boolean exists(IParcela parcela) {
        Parcela p = generalDAO.findParcela(parcela.getNomenclatura());
        if (p == null) {
            return false;
        } else {
            if (p.getId().equals(parcela.getId())) {
                return false;
            } else {
                return true;
            }
        }
    }

    @Override
    @Transactional
    public List<Tramite> getTramites(Date from, Date to, EstadoTramite estado, String usuario) {
        if (from == null || to == null) {
            Calendar today = Calendar.getInstance();
            to = today.getTime();
            // Un mes atras
            today.add(Calendar.MONTH, -1);
            from = today.getTime();
        }
        LOG.info("getTramites params: {} {}, {}", from, to, estado);
        List<Tramite> tramites = generalDAO.getTramites(from, to, estado, usuario);
        HibernateHelper.initialize(tramites, "cuentasOrigen");
        return tramites;
    }

    @Override
    @Transactional
    public LineaTemporal findLineaTemporal(Tramite tramite) {
        return generalDAO.findLineaTemporalByTramite(tramite.getId());
    }
}
