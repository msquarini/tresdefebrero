package ar.com.jass.catastro.business.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.security.MessageDigest;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.com.jass.catastro.batch.pagos.PagosJobLauncher;
import ar.com.jass.catastro.business.PagosService;
import ar.com.jass.catastro.dao.GeneralDAO;
import ar.com.jass.catastro.dao.PagosDAO;
import ar.com.jass.catastro.exceptions.FilePagoDuplicatedException;
import ar.com.jass.catastro.exceptions.FilePagoTypeUnknownException;
import ar.com.jass.catastro.model.pagos.EstadoFilePagos;
import ar.com.jass.catastro.model.pagos.FilePago;
import ar.com.jass.catastro.util.HibernateHelper;

/**
 * Implementacion de funciones de pagos
 * 
 * @author MESquarini
 *
 */
@Service
public class PagosServiceImpl implements PagosService {

    private static final Logger LOG = LoggerFactory.getLogger("PagosServiceImpl");

    @Autowired
    @Qualifier("pagosDAO")
    private PagosDAO pagosDAO;

    @Autowired
    @Qualifier("generalDAO")
    private GeneralDAO generalDAO;

    @Autowired
    @Qualifier("pagosJobLauncher")
    private PagosJobLauncher jobLauncher;

    public GeneralDAO getGeneralDAO() {
        return generalDAO;
    }

    public void setGeneralDAO(GeneralDAO generalDAO) {
        this.generalDAO = generalDAO;
    }

    @Override
    @Transactional
    public FilePago getFilePago(Long id, String... initialize) {
        FilePago fp = generalDAO.find(FilePago.class, id);
        HibernateHelper.initialize(fp, initialize);
        return fp;
    }
    /*
     * (non-Javadoc)
     * 
     * @see ar.com.jass.catastro.business.PagosService#prepocess(java.lang.String, java.io.File)
     */
    @Override
    @Transactional
    public FilePago prepocess(String filename, File pagos)
            throws Exception {
        LOG.info("Preproceso de FilePago: {}", filename);

        // Creo el nuevo registro de file de pagos
        FilePago newFilePago = new FilePago(filename);
        newFilePago.setNombre(filename);
        newFilePago.setFileHeader("");
        newFilePago.setFileTrailer("");

        try {
            // Primera linea del file para identificar tipo y fecha
            String headerLine = getHeader(pagos);
            // Calculo de hash para evitar doble procesamiento
            String hash = generateHash(pagos);
            newFilePago.setHash(hash);
            newFilePago.setOrigen(FileType.getFileType(headerLine));
            newFilePago.setFechaFile(newFilePago.getOrigen().getFechaFile(headerLine));

            FilePago fp = getPagosDAO().findFilePago(hash);
            // Si encuentra entonces esta duplicado
            throw new FilePagoDuplicatedException(fp);

        } catch (JpaObjectRetrievalFailureException enfe) {
            LOG.info("Persistiendo nuevo registro {}", newFilePago);
            generalDAO.save(newFilePago);
            return newFilePago;
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see ar.com.jass.catastro.business.PagosService#processAll()
     */
    @Override
    public void processAll() {
        List<FilePago> pagos = pagosDAO.getFilePagos(new EstadoFilePagos[] { EstadoFilePagos.PENDIENTE });
        for (FilePago pago : pagos) {
            jobLauncher.launchJob(pago);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see ar.com.jass.catastro.business.PagosService#getFilePagos(java.util.Date, java.util.Date,
     * ar.com.jass.catastro.model.pagos.EstadoFilePagos[])
     */
    @Override
    @Transactional
    public List<FilePago> getFilePagos(Date from, Date to, EstadoFilePagos[] estados) {
        if (from == null && to == null) {
            Calendar today = Calendar.getInstance();
            to = today.getTime();
            // Un mes atras
            today.add(Calendar.MONTH, -1);
            from = today.getTime();
        }
        LOG.info("getfilespagos params: {} {}, {}", from, to, estados);
        return pagosDAO.getFilePagos(from, to, estados);
    }

    /**
     * Calculo de hash del archivo
     * 
     * @param filename
     * @return
     * @throws Exception
     */
    private String generateHash(File file) throws Exception {
        LOG.info("Calculando HASH para el file {}", file.getAbsolutePath());

        FileInputStream fis = new FileInputStream(file);
        byte[] dataBytes = new byte[1024];

        int nread = 0;
        // convert the byte to hex format
        StringBuffer sb = new StringBuffer("");

        try {
            MessageDigest md = MessageDigest.getInstance("SHA1");

            while ((nread = fis.read(dataBytes)) != -1) {
                md.update(dataBytes, 0, nread);
            }
            byte[] mdbytes = md.digest();

            for (int i = 0; i < mdbytes.length; i++) {
                sb.append(Integer.toString((mdbytes[i] & 0xff) + 0x100, 16).substring(1));
            }

            // System.out.println("Digest(in hex format):: " + sb.toString());

        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    LOG.error("", e);
                }
            }
        }
        return sb.toString();
    }

    /**
     * Devuelve la primer linea del archivo
     * 
     * @param file
     * @return
     * @throws Exception
     */
    private String getHeader(File file) throws Exception {
        BufferedReader reader = new BufferedReader(new FileReader(file));
        try {
            return reader.readLine();
        } finally {
            reader.close();
        }
    }

    public PagosDAO getPagosDAO() {
        return pagosDAO;
    }

    public void setPagosDAO(PagosDAO pagosDAO) {
        this.pagosDAO = pagosDAO;
    }
}
