package ar.com.jass.catastro.web.action.deuda;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import ar.com.jass.catastro.business.ConsultaService;
import ar.com.jass.catastro.business.CuentaCorrienteService;
import ar.com.jass.catastro.model.Cuenta;
import ar.com.jass.catastro.model.cuentacorriente.CuentaCorriente;
import ar.com.jass.catastro.model.cuentacorriente.TipoMovimiento;
import ar.com.jass.catastro.web.action.GridBaseAction;
import ar.com.jass.catastro.web.commons.WebConstants;
import ar.com.jass.catastro.web.dto.CuentaCorrienteDTO;

/**
 * Action de grilla de movimientos de cuenta corriente
 * 
 * @author msquarini
 *
 */
public class CuentaCorrienteMovimientoAction extends GridBaseAction<CuentaCorriente> {

    private static final long serialVersionUID = 7353477345330099548L;

    private static final Logger LOG = LoggerFactory.getLogger(CuentaCorrienteMovimientoAction.class);

    @Autowired
    private CuentaCorrienteService ccService;

    @Autowired
    private ConsultaService consultaService;

    /**
     * Entidad Cuenta para gestion de cuenta corriente
     */
    private Cuenta cuenta;

    /**
     * Filtro de Cuenta Corriente
     */
    private Integer anio;
    private Integer cuota;

    private TipoMovimiento tipo;

    private Long idCC;

    private CuentaCorriente cc;

    /**
     * Llamada AJAX para obtener la informacion para la grilla en formato JSON
     * 
     * Se toma la cuenta en session para la busqueda de movimientos
     * 
     * @return
     */
    @SuppressWarnings("unchecked")
    public String getData() {
        try {
            List<CuentaCorriente> data = (List<CuentaCorriente>) getFromSession(WebConstants.CC_MOV_DATA);
            setData(data);
        } catch (Exception e) {
            LOG.error("error en getData movimientos de cuenta corriente", e);
        }
        return "dataJSON";
    }

    public String movimientos() {
        LOG.info("Movimientos cuenta: {}, anio: {}, cuota: {} ", cuenta, anio, cuota);
        Map<String, CuentaCorrienteDTO> agrupador = (Map<String, CuentaCorrienteDTO>) getFromSession(
                WebConstants.CC_DATA);
        String key = anio + "-" + cuota;
        CuentaCorrienteDTO ccDTO = agrupador.get(key);
        setInSession(WebConstants.CC_MOV_DATA, ccDTO.getMovimientos());
        return "info-movimientos";
    }

    public String detail() {
        LOG.info("Detalle CC Id: {} ", idCC);
        cc = ccService.findMovimientoCC(idCC);
        return "info-detalle";
    }

    public static Logger getLog() {
        return LOG;
    }

    public ConsultaService getConsultaService() {
        return consultaService;
    }

    public void setConsultaService(ConsultaService consultaService) {
        this.consultaService = consultaService;
    }

    public Cuenta getCuenta() {
        return cuenta;
    }

    public void setCuenta(Cuenta cuenta) {
        this.cuenta = cuenta;
    }

    public CuentaCorrienteService getCcService() {
        return ccService;
    }

    public void setCcService(CuentaCorrienteService ccService) {
        this.ccService = ccService;
    }

    public Integer getAnio() {
        return anio;
    }

    public void setAnio(Integer anio) {
        this.anio = anio;
    }

    public TipoMovimiento getTipo() {
        return tipo;
    }

    public void setTipo(TipoMovimiento tipo) {
        this.tipo = tipo;
    }

    public Long getIdCC() {
        return idCC;
    }

    public void setIdCC(Long idCC) {
        this.idCC = idCC;
    }

    public CuentaCorriente getCc() {
        return cc;
    }

    public void setCc(CuentaCorriente cc) {
        this.cc = cc;
    }

    public Integer getCuota() {
        return cuota;
    }

    public void setCuota(Integer cuota) {
        this.cuota = cuota;
    }

}
