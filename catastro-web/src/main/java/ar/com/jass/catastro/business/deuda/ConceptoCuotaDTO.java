package ar.com.jass.catastro.business.deuda;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import ar.com.jass.catastro.model.cuentacorriente.CCDetalle;
import ar.com.jass.catastro.model.cuentacorriente.Periodo;

/**
 * Holder para los datos necesarios para el calculo de cuota
 * @author msquarini
 *
 */
public class ConceptoCuotaDTO {

    private Integer categoria;
    private Periodo periodo;
    private BigDecimal valuacion;
    
    /**
     * Valor base de cuenta que no presenta valuación 
     */
    private BigDecimal base;
    private Double tasa;

    /**
     * Valor de cuota pura (generado en el proceso)
     */
    private BigDecimal montoCuota;
    private List<CCDetalle> detalles;

    public ConceptoCuotaDTO(Periodo periodo, Integer categoria, BigDecimal valuacion, Double tasa, BigDecimal base) {
        this.periodo = periodo;
        this.categoria = categoria;
        this.valuacion = valuacion;
        this.tasa = tasa;
        detalles = new ArrayList<CCDetalle>();
    }
    
    public Integer getCategoria() {
        return categoria;
    }

    public void setCategoria(Integer categoria) {
        this.categoria = categoria;
    }

    public Periodo getPeriodo() {
        return periodo;
    }

    public void setPeriodo(Periodo periodo) {
        this.periodo = periodo;
    }

    public BigDecimal getValuacion() {
        return valuacion;
    }

    public void setValuacion(BigDecimal valuacion) {
        this.valuacion = valuacion;
    }

    public BigDecimal getMontoCuota() {
        return montoCuota;
    }

    public void setMontoCuota(BigDecimal montoCuota) {
        this.montoCuota = montoCuota;
    }

    public Double getTasa() {
        return tasa;
    }

    public void setTasa(Double tasa) {
        this.tasa = tasa;
    }

    public List<CCDetalle> getDetalles() {
        return detalles;
    }

    public void setDetalles(List<CCDetalle> detalles) {
        this.detalles = detalles;
    }
    
    public void addDetalle(CCDetalle detalle) {
        getDetalles().add(detalle);
    }

    public BigDecimal getBase() {
        return base;
    }

    public void setBase(BigDecimal base) {
        this.base = base;
    }
}
