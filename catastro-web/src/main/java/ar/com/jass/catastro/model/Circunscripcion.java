package ar.com.jass.catastro.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.OrderBy;

/**
 * Circunscripciones y sus correspondientes secciones
 * 
 * @author mesquarini
 *
 */
@Entity
@Table(name = "Circunscripcion")
public class Circunscripcion {

	public Integer circunscripcion;
	public List<Seccion> secciones;

	public Circunscripcion() {
		// Empty constructor
	}

	@Id
	@Column(name = "circ", nullable = false)
	public Integer getCircunscripcion() {
		return circunscripcion;
	}

	public void setCircunscripcion(Integer circunscripcion) {
		this.circunscripcion = circunscripcion;
	}

	@OneToMany(fetch = FetchType.EAGER)
	@JoinColumn(name = "circ")
	@OrderBy(clause = "seccion ASC")
	public List<Seccion> getSecciones() {
		return secciones;
	}

	public void setSecciones(List<Seccion> secciones) {
		this.secciones = secciones;
	}

}
