package ar.com.jass.catastro.model.cuentacorriente;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import ar.com.jass.catastro.business.impl.FileType;
import ar.com.jass.catastro.model.EntityBase;

/**
 * Entidad de files de deuda generados PMC y LINK
 * 
 * @author msquarini
 *
 */
@Entity
@Table(name = "File_Deuda")
public class FileDeuda extends EntityBase implements Serializable {

    private Long id;

    /**
     * Periodo de deuda que corresponde
     */
    private Periodo periodo;

    /**
     * Tipo de file generado
     */
    private FileType tipo;

    /**
     * Nombre de archivo
     */
    private String filename;

    /**
     * Constructor default de movimiento de debito
     */
    public FileDeuda() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_file_deuda_gen")
    @SequenceGenerator(name = "seq_file_deuda_gen", sequenceName = "seq_file_deuda", allocationSize = 1)
    @Column(name = "id_file_deuda")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "tipo")
    public FileType getTipo() {
        return tipo;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_periodo", nullable = false)
    public Periodo getPeriodo() {
        return periodo;
    }

    public void setPeriodo(Periodo periodo) {
        this.periodo = periodo;
    }

    @Column(name = "filename")
    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public void setTipo(FileType tipo) {
        this.tipo = tipo;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((filename == null) ? 0 : filename.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((periodo == null) ? 0 : periodo.hashCode());
        result = prime * result + ((tipo == null) ? 0 : tipo.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        FileDeuda other = (FileDeuda) obj;
        if (filename == null) {
            if (other.filename != null)
                return false;
        } else if (!filename.equals(other.filename))
            return false;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (periodo == null) {
            if (other.periodo != null)
                return false;
        } else if (!periodo.equals(other.periodo))
            return false;
        if (tipo != other.tipo)
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "FileDeuda [id=" + id + ", periodo=" + periodo + ", tipo=" + tipo + ", filename=" + filename + "]";
    }

}