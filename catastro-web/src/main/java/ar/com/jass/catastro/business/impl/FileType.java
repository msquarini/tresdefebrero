package ar.com.jass.catastro.business.impl;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;

import ar.com.jass.catastro.batch.deuda.file.callback.DeudaFileCallback;
import ar.com.jass.catastro.exceptions.FilePagoTypeUnknownException;
import ar.com.jass.catastro.util.ApplicationContextHelper;

/**
 * Tipos de files de pagos y deuda que se pueden procesar.
 * 
 * En base al registro inicial del archivo detectamos que tipo es.
 * 
 * @author msquarini
 *
 */
public enum FileType {

    PMC("PagoMisCuentas", 1) {

        @Override
        public String getDeudaFileName() {
            DateFormat sdf = new SimpleDateFormat("ddMMyyyy");
            return "FAC2337." + sdf.format(new Date());
        }

        @Override
        protected boolean headerIs(String line) {
            return line.startsWith("0400");
        }

        @Override
        protected String getJobName() {
            return "pmc-pagos";
        }

        @Override
        public Date getFechaFile(String line) {
            DateFormat sfd = new SimpleDateFormat("yyyyMMdd");
            try {
                return sfd.parse(line.substring(8, 16));
            } catch (ParseException e) {
                LOG.error("Error de parseo de fecha de file de pagos", e);
            }
            return null;
        }
    },
    LINK("Link", 2) {

        private String getMesDigit(Integer mes) {
            String mesDigit;
            switch (mes) {
            case 10:
                mesDigit = "A";
                break;
            case 11:
                mesDigit = "B";
                break;
            case 12:
                mesDigit = "C";
                break;
            default:
                mesDigit = String.valueOf(mes);
                break;
            }
            return mesDigit;
        }
        
        @Override
        public String getDeudaFileName() {
            DateFormat sdf = new SimpleDateFormat("dd");
            Calendar now = Calendar.getInstance();
            int mes = now.get(Calendar.MONTH) + 1;
            String mesDigit = getMesDigit(mes);            
            return "PAPX1" + mesDigit + sdf.format(now.getTime());
        }
        
        @Override
        public String getControlFileName() {
            DateFormat sdf = new SimpleDateFormat("dd");
            Calendar now = Calendar.getInstance();
            int mes = now.get(Calendar.MONTH) + 1;
            String mesDigit = getMesDigit(mes);
            return "CAPX1" + mesDigit + sdf.format(now.getTime());
        }

        @Override
        protected boolean headerIs(String line) {
            return line.startsWith("0APX");
        }

        @Override
        protected String getJobName() {
            return "link-pagos";
        }

        @Override
        public Date getFechaFile(String line) {
            return null;
        }

    },
    BAPRO("Bco Provincia", 3) {

        @Override
        protected boolean headerIs(String line) {
            return line.endsWith("04001");
        }

        @Override
        public Date getFechaFile(String line) {
            return null;
        }

    },
    BNA("Bco Nacion", 4) {

        @Override
        protected boolean headerIs(String line) {
            return line.length() == 160;
        }

        @Override
        public Date getFechaFile(String line) {
            return null;
        }
    },
    RAPI("RapiPago", 5) {

        @Override
        protected boolean headerIs(String line) {
            return line.endsWith("08001");
        }

        @Override
        public Date getFechaFile(String line) {
            return null;
        }
    },
    CREDICOOP("Bco Credicoop", 6) {

        @Override
        protected boolean headerIs(String line) {
            return line.endsWith("05001");
        }

        @Override
        public Date getFechaFile(String line) {
            return null;
        }

    },
    MUNI("Caja Municipal", 7) {

        @Override
        protected boolean headerIs(String line) {
            return line.endsWith("00200");
        }

        @Override
        public Date getFechaFile(String line) {
            return null;
        }
    };

    private static final Logger LOG = LoggerFactory.getLogger("FileType");

    private String descripcion;
    private Integer codigo;

    FileType(String descripcion, Integer codigo) {
        this.descripcion = descripcion;
        this.codigo = codigo;
    }

    /**
     * Valida si la linea pertenece al tipo
     * 
     * @param line
     * @return
     */
    protected abstract boolean headerIs(String line);

    /**
     * Devuelve la fecha del archivo de pagos, si existe
     * 
     * @param line
     * @return
     */
    public abstract Date getFechaFile(String line);

    /**
     * Nombre del archivo de deuda
     * 
     * @return
     */
    public String getDeudaFileName() {
        return name();
    }

    /**
     * Archivo de control (solo utilizado x LINK)
     * @return
     */
    public String getControlFileName() {
        return "ctrl" + name();
    }
    /**
     * 
     * @return
     */
    protected String getJobName() {
        return "common-pagos";
    }

    /**
     * Retorna el bean que representa el JOB para procesar cada tipo de archivo de pagos
     * 
     * @return
     */
    public Job getJob() {
        return (Job) ApplicationContextHelper.getApplicationContext().getBean(getJobName() + "-JOB");
    }

    /**
     * Determina el origen del archivo de pagos de acuerdo al formato de linea
     * 
     * @param line
     * @return
     * @throws FilePagoTypeUnknownException
     */
    public static FileType getFileType(String line) throws FilePagoTypeUnknownException {
        for (FileType type : FileType.values()) {
            if (type.headerIs(line)) {
                return type;
            }
        }
        throw new FilePagoTypeUnknownException("Tipo de archivo desconocido");
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public DeudaFileCallback getDeudaFileHeaderCallback() {
        return (DeudaFileCallback) ApplicationContextHelper.getApplicationContext().getBean(name() + "-header");
    }

    public DeudaFileCallback getDeudaFileFooterCallback() {
        return (DeudaFileCallback) ApplicationContextHelper.getApplicationContext().getBean(name() + "-footer");
    }
}
