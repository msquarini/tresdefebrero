package ar.com.jass.catastro.business.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.com.jass.catastro.batch.deuda.boleta.GeneracionBoletaBatch;
import ar.com.jass.catastro.business.BoletaService;
import ar.com.jass.catastro.business.CuentaCorrienteService;
import ar.com.jass.catastro.business.TasaService;
import ar.com.jass.catastro.dao.GeneralDAO;
import ar.com.jass.catastro.model.Cuenta;
import ar.com.jass.catastro.model.cuentacorriente.Boleta;
import ar.com.jass.catastro.model.cuentacorriente.BoletaPeriodoStatus;
import ar.com.jass.catastro.model.cuentacorriente.CuentaCorriente;
import ar.com.jass.catastro.model.cuentacorriente.DeudaPeriodoStatus;
import ar.com.jass.catastro.model.cuentacorriente.Periodo;

/**
 * Generacion de boletas. Agrupa movimientos de CC y calcula montos a pagar. Genera boleta cuota
 * anual si corresponde al periodo
 * 
 * @author msquarini
 *
 */
@Service
public class BoletaServiceImpl implements BoletaService {

    private static final Logger LOG = LoggerFactory.getLogger("BoletaServiceImpl");

    @Autowired
    @Qualifier("generalDAO")
    private GeneralDAO generalDAO;

    @Autowired
    private CuentaCorrienteService ccService;

    @Autowired
    private GeneracionBoletaBatch generacionBoletaBatch;

    @Autowired
    private TasaService tasaService;

    public TasaService getTasaService() {
        return tasaService;
    }

    public void setTasaService(TasaService tasaService) {
        this.tasaService = tasaService;
    }

    public CuentaCorrienteService getCcService() {
        return ccService;
    }

    public void setCcService(CuentaCorrienteService ccService) {
        this.ccService = ccService;
    }

    @Transactional
    @Override
    public Periodo cambioEstadoPeriodoGeneracionBoleta(Long idPeriodo, BoletaPeriodoStatus estado) {

        Periodo periodo = (Periodo) generalDAO.find(Periodo.class, idPeriodo);

        // Validamos que el periodo tenga deuda generada
        if (!periodo.getEstado().equals(DeudaPeriodoStatus.GENERADO)) {
            return null;
        }

        if (!periodo.getBoletaStatus().equals(estado) && !periodo.getEstado().equals(DeudaPeriodoStatus.GENERADO)) {
            periodo.setBoletaStatus(estado);
            generalDAO.update(periodo);
            LOG.info("Cambiando estado generacion de boletas del periodo {} a {}", periodo, estado);
        }

        return periodo;
    }

    @Override
    public void generacionBoletasBatch(List<Periodo> periodos) {
        generacionBoletaBatch.run(periodos);
    }

    /**
     * Crea una boleta de pago, con los conceptos del periodo e indica si corresponde aplicar
     * intereses
     * 
     * @param cuenta
     * @param movimientos
     * @param periodo
     * @param aplicaInteres
     * @return
     */
    private Boleta crearBoleta(Cuenta cuenta, List<CuentaCorriente> movimientos, Periodo periodo,
            Boolean aplicaInteres) {
        if (aplicaInteres) {
            for (CuentaCorriente movimiento : movimientos) {
                movimiento.aplicarInteres(periodo, getTasaService().getTasaInteresMora());
                getCcService().save(movimiento);
            }
        }
        Boleta boleta = new Boleta(cuenta, periodo, movimientos);
        boleta.calcularTotales();
        return boleta;
    }

    /**
     * Crea la boleta de pago anual para el periodo
     * 
     * @param cuenta
     * @param periodo
     * @return
     */
    private Boleta crearBoletaAnual(Cuenta cuenta, Periodo periodo) {
        if (periodo.isGenerarAnual() && permiteCuotaAnual(cuenta)) {
            Boleta boletaAnual = new Boleta(cuenta, periodo, true);

            // Movimientos de CC para la cuenta en cada periodo del anio
            List<Periodo> periodosAnio = ccService.findPeriodos(periodo.getAnio());
            for (Periodo p : periodosAnio) {
                // Si el periodo es el vigente o posterior -> Se incorpora en la cuota anual
                if (p.getCuota() >= periodo.getCuota()) {
                    List<CuentaCorriente> movimientos = ccService.findMovimientosCC(cuenta, p.getAnio(), p.getCuota());
                    boletaAnual.addDetalle(p, movimientos);
                }
            }
            boletaAnual.calcularTotales();
            return boletaAnual;
        }
        return null;
    }

    /**
     * Creacion de boleta de periodos vencidos
     * 
     * @param cuenta
     * @param periodo hasta el periodo excluyente
     * @return
     */
    private Boleta crearBoletaVencidas(Cuenta cuenta, Periodo periodo) {

        Boleta boleta = new Boleta(cuenta, periodo, false);

        // Periodos de deuda vencida
        Periodo desde = new Periodo(2018, 1);
        List<Periodo> periodosVencidos = ccService.findPeriodos(desde, periodo);

        for (Periodo p : periodosVencidos) {
            List<CuentaCorriente> movimientos = ccService.findMovimientosCC(cuenta, p.getAnio(), p.getCuota());
            boleta.addDetalle(p, movimientos);
        }
        
        boleta.calcularTotales();
        return boleta;
    }

    /**
     * Generar boleta de pago para los movimientos del periodo.
     * 
     * 
     * @param cuenta
     * @param periodo
     * @param aplicaInteres
     *            Si es true, se aplica interes desde la fecha de vencimiento del movimiento
     * @param cuotaAnual
     *            si es true, se genera la boleta anual incluyendo movimientos desde el periodo
     * @param vencidas
     *            si es true, se genera boleta con la deuda de toda la cuenta
     * @return
     */
    @Transactional
    @Override
    public List<Boleta> generarBoleta(Cuenta cuenta, Periodo periodo, Boolean aplicaInteres, Boolean cuotaAnual,
            Boolean vencidas) {
        LOG.info("Generando boleta, {} {}", periodo, cuenta);

        if (!periodo.getEstado().equals(DeudaPeriodoStatus.GENERADO)) {
            throw new IllegalStateException("periodo.emision.notexists");
        }

        // TODO: Calcular intereses para movimientos vencidos!
        // ccService.ajustarIntereses(Periodo periodo, cuenta);

        // CREAR 3 BOLETAS (Una para el periodo, una para anual y una para deuda vencida!)

        List<Boleta> boletas = new ArrayList<Boleta>();

        // Movimientos de CC para la cuenta en el periodo
        List<CuentaCorriente> movimientos = ccService.findMovimientosCC(cuenta, periodo.getAnio(), periodo.getCuota());

        Boleta boleta = crearBoleta(cuenta, movimientos, periodo, aplicaInteres);
        boletas.add(boleta);

        // Si el periodo genera cuota anual y la cuenta esta en condiciones -> generamos la boleta
        // de pago anual
        if (cuotaAnual) {
            Boleta boletaAnual = crearBoletaAnual(cuenta, periodo);
            if (boletaAnual != null) {
                boletas.add(boletaAnual);
            }
        }

        /**
         * Indica si se crea la boleta para toda la deuda existente, hasta el periodo vigente
         */
        if (vencidas) {
            Boleta boletaVencidas = crearBoletaVencidas(cuenta, periodo);
            if (boletaVencidas != null) {
                boletas.add(boletaVencidas);
            }
        }

        // Grabar las boletas generadas
        for (Boleta b : boletas) {
            ccService.save(b);
        }

        return boletas;
    }

    private Boolean permiteCuotaAnual(Cuenta cuenta) {
        // REGLAS para que se genere cuota anual a una cuenta
        return Boolean.TRUE;
    }

    public GeneralDAO getGeneralDAO() {
        return generalDAO;
    }

    public void setGeneralDAO(GeneralDAO generalDAO) {
        this.generalDAO = generalDAO;
    }

}
