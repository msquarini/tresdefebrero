package ar.com.jass.catastro.batch.deuda.file.callback;

import java.io.IOException;
import java.io.Writer;
import java.util.Date;
import java.util.List;

import org.springframework.batch.item.file.FlatFileHeaderCallback;

import ar.com.jass.catastro.batch.deuda.file.DeudaDTO;

/**
 * Header para archivos a LINK
 * 
 * @author mesquarini
 *
 */
public class LinkHeaderCallback implements FlatFileHeaderCallback, DeudaFileCallback {

    public static final String HEADER = "HRFACTURACIONAPX%1$ty%1$tm%1$td00001";
    public static final int FILLER = 104;
    
	@Override
	public void writeHeader(Writer writer) throws IOException {
	    String temp = String.format(HEADER, new Date()) + String.format("%" + FILLER + "s", " ");	
		writer.write(temp);
	}

	public static void main(String[] args) {
	    LinkHeaderCallback c = new LinkHeaderCallback();
	    try {
            c.writeHeader(null);
        } catch (IOException e) {
            e.printStackTrace();
        }
	}

    @Override
    public void notify(List<DeudaDTO> deudas) {
        //noop
    }
}
