package ar.com.jass.catastro.business.deuda;

import java.math.BigDecimal;

import ar.com.jass.catastro.model.cuentacorriente.CCDetalle;
import ar.com.jass.catastro.model.cuentacorriente.CuentaCorriente;
import ar.com.jass.catastro.model.cuentacorriente.Periodo;
import ar.com.jass.catastro.model.cuentacorriente.TipoMovimiento;

public interface TemplateCuotaService {

    public CuentaCorriente generarCuota(TipoMovimiento tipo, Periodo periodo, Integer categoria, BigDecimal valuacion,
            BigDecimal base);
    
    public CCDetalle calculoValorCuotaPuro(Integer categoria, BigDecimal valuacion, BigDecimal base);
}
