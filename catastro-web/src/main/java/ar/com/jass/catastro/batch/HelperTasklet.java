package ar.com.jass.catastro.batch;

import org.springframework.batch.core.scope.context.ChunkContext;

public abstract class HelperTasklet {

    /**
     * @param chunkContext
     * @param paramName
     * @return
     */
    public Object getExecutionParameter(ChunkContext chunkContext, String paramName) {
        return chunkContext.getStepContext().getStepExecution().getJobExecution().getExecutionContext().get(paramName);
    }

    /**
     * Mapa de objetos de la ejecucion de jobs. Ojo con la keys para no pisarse entre instancias de jobs!!
     * @param chunkContext
     * @param paramName
     * @param paramValue
     */
    public void putExecutionParameter(ChunkContext chunkContext, String paramName, Object paramValue) {
        chunkContext.getStepContext().getStepExecution().getJobExecution().getExecutionContext().put(paramName,
                paramValue);
    }
    
    public Long getJobLongParameter(ChunkContext chunkContext, String paramName) {
        return chunkContext.getStepContext().getStepExecution().getJobExecution().getJobParameters().getLong(paramName);
    }
    
    public String getJobStringParameter(ChunkContext chunkContext, String paramName) {
        return chunkContext.getStepContext().getStepExecution().getJobExecution().getJobParameters().getString(paramName);
    }
}
