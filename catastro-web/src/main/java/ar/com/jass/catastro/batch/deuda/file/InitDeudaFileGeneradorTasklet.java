package ar.com.jass.catastro.batch.deuda.file;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import ar.com.jass.catastro.batch.HelperTasklet;
import ar.com.jass.catastro.business.impl.FileType;
import ar.com.jass.catastro.dao.GeneralDAO;
import ar.com.jass.catastro.model.cuentacorriente.Periodo;

/**
 * Inicializacion de JOB para la generacion de archivos de deudas a bancos
 * 
 * @author msquarini
 *
 */
public class InitDeudaFileGeneradorTasklet extends HelperTasklet implements Tasklet {

	private static final Logger LOG = LoggerFactory.getLogger("InitDeudaFileGeneradorTasklet");

	@Value("${deuda.toBank.path}")
	private String deudaFilePath;

    @Autowired
    private GeneralDAO generalDAO;
    
	@Override
	public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) throws Exception {

		FileType ft = FileType.valueOf(getJobStringParameter(chunkContext, "destino"));
		// PATH de salida del archivo
		String path = deudaFilePath + File.separator + ft.getDeudaFileName();
		putExecutionParameter(chunkContext, "path", path);
		putExecutionParameter(chunkContext, "destino", ft);

		LOG.info("Inicio generacion archivo de deuda para {}, path: {}", ft, path);

		Periodo periodo = generalDAO.find(Periodo.class, getJobLongParameter(chunkContext, "idPeriodo")); 
        putExecutionParameter(chunkContext, "periodo", periodo);
        
		return RepeatStatus.FINISHED;
	}

	public String getDeudaFilePath() {
		return deudaFilePath;
	}

	public void setDeudaFilePath(String deudaFilePath) {
		this.deudaFilePath = deudaFilePath;
	}

	public GeneralDAO getGeneralDAO() {
		return generalDAO;
	}

	public void setGeneralDAO(GeneralDAO generalDAO) {
		this.generalDAO = generalDAO;
	}

}
