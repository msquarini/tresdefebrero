package ar.com.jass.catastro.model.da;

/**
 * Tipo de cuenta corriente (Debito/Credito)
 * @author msquarini
 *
 */
public enum DANovedad {

	ALTA, BAJA, MODIFICACION;

}
