package ar.com.jass.catastro.web.commons;

public interface WebConstantsRatificacion {


    public static final String CUENTA_RATIFICACION = "cuenta_ratificacion";

    public static final String CUENTA_RATIFICACION_ORIGEN = "cuenta_ratificacion_origen";

    public static final String RESPONSABLES_RATIFICACION_ADD = "responsables_ratificacion";

    public static final String TITULARES_RATIFICACION_ADD = "titulares_ratificacion_add";
    
    public static final String PARCELA_RATIFICACION_NEW = "parcela_ratificacion_new";
    
    public static final String RATIFICACION_LIST_PARCELAS = "lista_parcelas";
    
    public static final String RATIFICACION_LIST_PARCELAS_REMOVE = "lista_parcelas_a_eliminar";
    
    public static final String RATIFICACION_LIST_PARCELAS_NEW = "lista_parcelas_nuevas";
    
    public static final String RATIFICACION_PARCELA_UPDATE = "parcela_a_actualizar";
    
    public static final String RATIFICACION_GRID_DATA = "parcelas";

}
