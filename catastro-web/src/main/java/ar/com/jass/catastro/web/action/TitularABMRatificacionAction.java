package ar.com.jass.catastro.web.action;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import ar.com.jass.catastro.business.BusinessHelper;
import ar.com.jass.catastro.business.CatastroService;
import ar.com.jass.catastro.business.ConsultaService;
import ar.com.jass.catastro.business.RatificacionService;
import ar.com.jass.catastro.model.CuentaRatificacion;
import ar.com.jass.catastro.model.TitularParcelaRatificacion;
import ar.com.jass.catastro.model.TitularRatificacion;
import ar.com.jass.catastro.model.Tramite;
import ar.com.jass.catastro.model.ResponsableCuentaRatificacion;
import ar.com.jass.catastro.model.Titular;
import ar.com.jass.catastro.model.TitularParcela;
import ar.com.jass.catastro.web.commons.WebConstants;
import ar.com.jass.catastro.web.commons.WebConstantsRatificacion;

/**
 * Gestion ABM de titulares2
 * 
 * Alta y edicion de TitularParcela
 * 
 * @author msquarini
 *
 */
public class TitularABMRatificacionAction extends GridBaseAction<TitularParcelaRatificacion> {

    private static final long serialVersionUID = 7353477345330099548L;

    private static final Logger LOG = LoggerFactory.getLogger("TitularABMRatificacionAction");

    /**
     * Se utiliza para tener en session el ID o Cuit del titular en edicion
     */
    private static final String EDIT_TITULAR = "titular_edition";

    /**
     * Se utiliza para indicar si es modo edicion o alta de titular
     */
    private static final String EDIT_MODE = "edit_mode";

    /**
     * ID de titular para agregar a la lista de TitularParcela
     */
    private Long idTitular;

    /**
     * Cuit/Cuil de titular a editar
     */
    private String cuitcuil;

    private String uuidTP;

    /**
     * Nuevo TitularParcela
     */
    private TitularParcelaRatificacion titularParcela;

    @Autowired
    private BusinessHelper businessHelper;
    
    @Autowired
    private RatificacionService ratificacionService;
    
    @Autowired
    private ConsultaService consultaService;


    private Tramite tramite;
    private List<TitularParcelaRatificacion> titulares;
    
    private CuentaRatificacion cuentaRatificacionOrigen;

    /*
     * (non-Javadoc)
     * 
     * @see com.opensymphony.xwork2.ActionSupport#input()
     */
    public String input() {
        return "input";
    }

    /**
     * Inicializar la gestion de cambio de titulares
     * 
     * @return
     */
    public String init() {
        // Busqueda de cuenta origen a partir de ID
    	cuentaRatificacionOrigen =  ratificacionService.findCuentaRatificacionById(idCuentaOrigen, "responsables", "parcela.titulares",
                "parcela.domicilioInmueble");



        setInSession(WebConstantsRatificacion.CUENTA_RATIFICACION_ORIGEN, cuentaRatificacionOrigen);
        // Titulares en session para editar
        setInSession(WebConstantsRatificacion.TITULARES_RATIFICACION_ADD, new HashSet<TitularParcelaRatificacion>(cuentaRatificacionOrigen.getParcela().getTitulares()));
        setInSession(WebConstantsRatificacion.RESPONSABLES_RATIFICACION_ADD, new HashSet<ResponsableCuentaRatificacion>(cuentaRatificacionOrigen.getResponsables()));
        setInSession(WebConstants.TITULARES_DATA, null); //???
        return "inprocess";
    }

    public String initFromTramite() {
        tramite = (Tramite) getFromSession(WebConstants.TRAMITE);
        //cuentaOrigen = tramite.getCuentasOrigen().get(0);
        
        
        
        cuentaRatificacionOrigen = ratificacionService.findCuentaRatificacionByCuenta(tramite.getCuentasOrigen().get(0).getCuenta(), "responsables", "parcela.titulares",
                "parcela.domicilioInmueble");
        
        setInSession(WebConstantsRatificacion.CUENTA_RATIFICACION_ORIGEN, cuentaRatificacionOrigen);
        // Titulares en session
        setInSession(WebConstantsRatificacion.TITULARES_RATIFICACION_ADD, new HashSet<TitularParcelaRatificacion>(cuentaRatificacionOrigen.getParcela().getTitulares()));
        setInSession(WebConstantsRatificacion.RESPONSABLES_RATIFICACION_ADD, new HashSet<ResponsableCuentaRatificacion>(cuentaRatificacionOrigen.getResponsables()));
        setInSession(WebConstants.TITULARES_DATA, null);
        return "inprocess";
    }
    
    /**
     * Actualizar CUITCUIL de un titular en la grilla de datos de session
     * 
     * 
     * @return
     */
    @SuppressWarnings("unchecked")
	public String updateCuitCuil() {
        LOG.info("updateCuitCuil titular, UUID [{}], CUIT [{}]", getUuidTP(), getCuitcuil());

        try {
            UUID uuid = UUID.fromString(getUuidTP());

			Set<TitularParcelaRatificacion> data = (Set<TitularParcelaRatificacion>) getFromSession(WebConstantsRatificacion.TITULARES_RATIFICACION_ADD);
            for (TitularParcelaRatificacion tp : data) {
                if (tp.getUuid().equals(uuid)) {
                    LOG.info("updateCuitCuil OK");
                    tp.getTitular().setCuitcuil(getCuitcuil());
                    break;
                }
            }
            // Persisto el titular
            // dao.save(tp.getTitular());
        } catch (Exception e) {
            LOG.error("", e);
            addActionError("error_update_cuit_titular");
            return "errorJSON";
        }
        return "update_ok";
    }

    /**
     * Llamada AJAX para obtener la informacion para la grilla en formato JSON Grilla de edicion de
     * titulares
     * 
     * @return
     */
    @SuppressWarnings("unchecked")
    public String getData() {
        LOG.info("getData");

        try {
            List<TitularParcelaRatificacion> data =  new ArrayList<TitularParcelaRatificacion>((Set<TitularParcelaRatificacion>)getFromSession(WebConstantsRatificacion.TITULARES_RATIFICACION_ADD));
            setData(data);

        } catch (Exception e) {
            LOG.error("error en getData titulares", e);
        }
        return "dataJSON";
    }

    /**
     * Redirije a pagina de alta
     * 
     * @return
     */
    public String goToAddTitular() {
        setInSession(EDIT_MODE, false);
        return "add_titular_input";
    }

    /**
     * Buscar el titular por el cuitcuil y setearlo para editar
     * 
     * @return
     */
    @SuppressWarnings("unchecked")
	public String goToEditTitular() {
        Set<TitularParcelaRatificacion> titularesParcela = (Set<TitularParcelaRatificacion>) getFromSession(WebConstantsRatificacion.TITULARES_RATIFICACION_ADD);
        UUID uuid = UUID.fromString(getUuidTP());
        for (TitularParcelaRatificacion tp : titularesParcela) {
            if (uuid.equals(tp.getUuid())) {
                titularParcela = tp;
                setInSession(EDIT_TITULAR, uuid);
                break;
            }
        }

        setInSession(EDIT_MODE, true);
        return "add_titular_input";

    }

    /**
     * Agrega titular ya existente a la grilla ABM de titularesparcela. Valido que el titular no
     * este presente en la lista
     * 
     * @return
     */
    @SuppressWarnings("unchecked")
	public String addSelectedTitular() {
        LOG.info("Agregando Titular ID [{}]", getIdTitular());
        
        Set<TitularParcelaRatificacion> titularesParcela = (Set<TitularParcelaRatificacion>) getFromSession(WebConstantsRatificacion.TITULARES_RATIFICACION_ADD) ;

        if (!titularesParcela.isEmpty()) {
            // UUID uuid = UUID.fromString(getUuidTP());

            // Verificar que no exista en la grilla por el ID del titular
            for (TitularParcelaRatificacion tp : titularesParcela) {
            	
            	System.out.println(tp.hashCode());

                // Si existe el titular dentro de los titularesParcela, vuelvo sin agregarlo
                if (getIdTitular().equals(tp.getTitular().getId())) {
                	
                	
                	
                    return "update_ok";
                }
            }
        }
        

        Titular titular = consultaService.findTitularById(getIdTitular(), "domicilioPostal.barrio");
        TitularParcelaRatificacion newTitularParcela = new TitularParcelaRatificacion();
        TitularRatificacion titularRatificacion = new TitularRatificacion(titular);
        
        newTitularParcela.setTitular(titularRatificacion);
        System.out.println(newTitularParcela.hashCode());
        
        
        titularesParcela.add(newTitularParcela);
        return "update_ok";
    }

    /**
     * Actualiza la grilla de titulares en la gestion de tramite
     * 
     * @return
     */
    @SuppressWarnings("unchecked")
	public String addTitular() {
        boolean isEditMode = (Boolean) getFromSession(EDIT_MODE);

        LOG.info("AddTitular ModoEdicion [{}], TitularParcela [{}]", isEditMode, titularParcela);

        Set<TitularParcelaRatificacion> titularesParcela = (Set<TitularParcelaRatificacion>) getFromSession(WebConstantsRatificacion.TITULARES_RATIFICACION_ADD);

        try {
            // Si no es modo edit -> es ALTA
            if (!isEditMode) {
                // TitularParcela tp = new TitularParcela();
                titularesParcela.add(titularParcela);
            } else {
                for (TitularParcelaRatificacion tp : titularesParcela) {
                    if (isTitularParcelaInEdition(tp)) {
                        LOG.info("Se modifico el TitularParcela: {}", tp);

                        // Cambiar para copiar los valores y no reemplazar la entidad, se pierden
                        // las
                        // relaciones!!!
                        updateValues(tp);
                        break;
                    }
                }
                setInSession(EDIT_TITULAR, null);
            }
        } catch (Exception e) {
            addActionError("error en ALTA de titular en grilla de edicion!!!");
            LOG.error("error agregando titular", e);
            return "errorJSON";
        }

        return "update_ok";
    }

    /**
     * Actualiza los datos del titular
     * 
     * @param t
     */
    private void updateValues(TitularParcelaRatificacion tp) {
        // tp.getTitular().setCuitcuil(titularParcela.getTitular().getCuitcuil());
        // tp.getTitular().setApellido(titularParcela.getTitular().getApellido());
        // tp.getTitular().setNombre(titularParcela.getTitular().getNombre());
        // tp.getTitular().setDocumento(titularParcela.getTitular().getDocumento());
        // tp.getTitular().setDomicilioPostal(titularParcela.getTitular().getDomicilioPostal());
        // tp.getTitular().setRazonSocial(titularParcela.getTitular().getRazonSocial());
        // tp.getTitular().setObservacion(titularParcela.getTitular().getObservacion());
        // tp.getTitular().setPersonaJuridica(titularParcela.getTitular().getPersonaJuridica());
        // tp.getTitular().setTipoDocumento(titularParcela.getTitular().getTipoDocumento());
        // tp.getTitular().setMail(titularParcela.getTitular().getMail());
        updateTitularRatificacionValues(titularParcela.getTitular(), tp.getTitular()); //metodo definido lineas abajo
        tp.setPorcentaje(titularParcela.getPorcentaje());
        tp.setDesde(titularParcela.getDesde());
        tp.setHasta(titularParcela.getHasta());
    }
    
    /*Lo pongo aca para no tocar la clase BaseAction*/
	private void updateTitularRatificacionValues(TitularRatificacion from, TitularRatificacion to) {
		to.setCuitcuil(from.getCuitcuil());
		to.setApellido(from.getApellido());
		to.setNombre(from.getNombre());
		to.setDocumento(from.getDocumento());
		to.setDomicilioPostal(from.getDomicilioPostal());
		to.setRazonSocial(from.getRazonSocial());
		to.setObservacion(from.getObservacion());
		to.setPersonaJuridica(from.getPersonaJuridica());
		to.setTipoDocumento(from.getTipoDocumento());
		to.setMail(from.getMail());
	}

    /**
     * Valida que el titular es el que esta en modo edicion.
     * 
     * @param t
     * @return
     */
    private boolean isTitularParcelaInEdition(TitularParcelaRatificacion tp) {
        return ((UUID) getFromSession(EDIT_TITULAR)).equals(tp.getUuid());
    }

    public String getCuitcuil() {
        return cuitcuil;
    }

    public void setCuitcuil(String cuitcuil) {
        this.cuitcuil = cuitcuil;
    }

    public TitularParcelaRatificacion getTitularParcela() {
        return titularParcela;
    }

    public void setTitularParcela(TitularParcelaRatificacion titularParcela) {
        this.titularParcela = titularParcela;
    }

    public Long getIdTitular() {
        return idTitular;
    }

    public void setIdTitular(Long idTitular) {
        this.idTitular = idTitular;
    }

    public String getUuidTP() {
        return uuidTP;
    }

    public void setUuidTP(String uuidTP) {
        this.uuidTP = uuidTP;
    }

    public Tramite getTramite() {
        return tramite;
    }

    public void setTramite(Tramite tramite) {
        this.tramite = tramite;
    }

    public List<TitularParcelaRatificacion> getTitulares() {
        return titulares;
    }

    public void setTitulares(List<TitularParcelaRatificacion> titulares) {
        this.titulares = titulares;
    }

	public CuentaRatificacion getCuentaRatificacionOrigen() {
		return cuentaRatificacionOrigen;
	}

	public void setCuentaRatificacionOrigen(CuentaRatificacion cuentaRatificacionOrigen) {
		this.cuentaRatificacionOrigen = cuentaRatificacionOrigen;
	}

}