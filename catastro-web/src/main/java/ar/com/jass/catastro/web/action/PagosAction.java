package ar.com.jass.catastro.web.action;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import ar.com.jass.catastro.business.ConsultaService;
import ar.com.jass.catastro.business.PagosService;
import ar.com.jass.catastro.exceptions.FilePagoDuplicatedException;
import ar.com.jass.catastro.exceptions.FilePagoTypeUnknownException;
import ar.com.jass.catastro.model.error.ErrorLog;
import ar.com.jass.catastro.model.pagos.EstadoFilePagos;
import ar.com.jass.catastro.model.pagos.FilePago;
import ar.com.jass.catastro.web.commons.WebConstants;

/**
 * Action de upload de files de pagos y procesamiento de los mismos
 * Los archivos pueden subirse en forma async en multiples llamadas
 * 
 * @author msquarini
 *
 */
/**
 * @author msquarini
 *
 */
public class PagosAction extends GridBaseAction<FilePago> {

	private static final long serialVersionUID = 7353477345330099548L;

	private static final Logger LOG = LoggerFactory.getLogger("PagosAction");

	private static final String GRID_DATA = "pagos";

	@Value("${pagos.toprocess.path}")
	private String pagosAProcesarPath;

	@Autowired
	private PagosService pagosService;

	@Autowired
	private ConsultaService consultaService;

	/**
	 * Archivos a subir
	 */
	private File pagos;
	private String pagosContentType;
	private String pagosFileName;

	/**
	 * Respuesta
	 */
	private String error;

	/**
	 * Indices de archivos con errores
	 */
	private Integer[] errorkeys;

	/**
	 * Filtros de consulta de files de pagos
	 */
	private Date from, to;
	private Boolean errores = Boolean.FALSE;
	private EstadoFilePagos[] estados;

	/**
	 * Info detallada del file de pagos
	 */
	private Long idFilePago;
	private FilePago filePago;

	/**
	 * Numero de tab para redirigir el input
	 */
	private Integer tab;

	/**
	 * Upload de files y actualizacion en BD de los registros
	 * 
	 * @return
	 */
	public String upload() {
		LOG.info("Upload de archivo de pagos, {}", getPagosFileName());
		try {
			// Crea el registro de archivos de pagos y lo persiste
			// Si ya existe lanza exception
			FilePago filePago = pagosService.prepocess(getPagosFileName(), getPagos());

			// Copiar el file al directorio de archivos a procesar
			// TODO: Apendear algo al name para que sea unico y no se pise con otra
			// subida!!!
			copyFile(filePago.getUniqueName());
		} catch (FilePagoTypeUnknownException fpte) {
			ErrorLog log = crearErrorLog("upload", fpte.getMessage());
			LOG.error("error en preprocesamiento de pagos, {}", log, fpte);
			error = "Tipo de archivo desconocido: " + getPagosFileName();
		} catch (FilePagoDuplicatedException fpde) {
			ErrorLog log = crearErrorLog("upload", fpde.getMessage());
			LOG.error("error en preprocesamiento de pagos, {}", log, fpde);
			// errorkeys = new Integer[] { 0 };
			error = "archivo ya procesado " + fpde.getFilePago().getHash();
		} catch (Exception e) {
			ErrorLog log = crearErrorLog("upload", e.getMessage());
			LOG.error("error en preprocesamiento de pagos, {}", log, e);
			error = "Fallo en la subida de archivos";
		}
		return "uploadresult";
	}

	private void copyFile(String destFileName) throws IOException {
		LOG.info("copiando file {} al directorio {}", pagosFileName, getPagosAProcesarPath());

		File destFile = new File(getPagosAProcesarPath(), destFileName);
		FileUtils.copyFile(pagos, destFile);
	}

	/**
	 * Ingreso a la pantalla de carga de pagos de cuenta
	 * 
	 */
	public String input() {
		LOG.info("Ingreso a gestion de pagos");
		return "input";
	}

	/**
	 * Consulta de files de pagos procesados Mostramos una grilla con el status de
	 * cada file procesado y la posibilidad de descarga los registros erroneos
	 * 
	 * @return
	 */
	public String search() {
		LOG.info("Searching files de pagos: {} {} {}", from, to, estados);
		List<FilePago> filesPago = pagosService.getFilePagos(from, to, estados);
		setInSession(WebConstants.FILEPAGOS_DATA, filesPago);
		return "grid";
	}

	public String infoFilePago() {
		LOG.info("Info file de pagos: {}", idFilePago);

		List<FilePago> data = (List<FilePago>) getFromSession(WebConstants.FILEPAGOS_DATA);
		for (FilePago fp : data) {
			if (fp.getId().equals(getIdFilePago())) {
				filePago = fp;
				break;
			}
		}
		return "info";
	}

	public String procesar() {
		LOG.info("procesando files de pagos");

		pagosService.processAll();

		try {
			Thread.currentThread().sleep(5000);
		} catch (InterruptedException e) {
			LOG.error("error en thread de espera de procesamiento de archivos de pagos", e);
		}

		// TODO: respuesta JSON y reload de grilla??
		return "dataJSON";
	}

	public static Logger getLog() {
		return LOG;
	}

	public ConsultaService getConsultaService() {
		return consultaService;
	}

	public void setConsultaService(ConsultaService consultaService) {
		this.consultaService = consultaService;
	}

	public PagosService getPagosService() {
		return pagosService;
	}

	public void setPagosService(PagosService pagosService) {
		this.pagosService = pagosService;
	}

	public Date getFrom() {
		return from;
	}

	public void setFrom(Date from) {
		this.from = from;
	}

	public Date getTo() {
		return to;
	}

	public void setTo(Date to) {
		this.to = to;
	}

	public Boolean getErrores() {
		return errores;
	}

	public void setErrores(Boolean errores) {
		this.errores = errores;
	}

	public String getData() {
		LOG.info("getData");
		try {
			List<FilePago> data = (List<FilePago>) getFromSession(WebConstants.FILEPAGOS_DATA);
			setData(data);
		} catch (Exception e) {
			LOG.error("error en getData", e);
		}
		return "dataJSON";
	}

	public File getPagos() {
		return pagos;
	}

	public void setPagos(File pagos) {
		this.pagos = pagos;
	}

	public String getPagosContentType() {
		return pagosContentType;
	}

	public void setPagosContentType(String pagosContentType) {
		this.pagosContentType = pagosContentType;
	}

	public String getPagosFileName() {
		return pagosFileName;
	}

	public void setPagosFileName(String pagosFileName) {
		this.pagosFileName = pagosFileName;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public Integer[] getErrorkeys() {
		return errorkeys;
	}

	public void setErrorkeys(Integer[] errorkeys) {
		this.errorkeys = errorkeys;
	}

	public String getPagosAProcesarPath() {
		return pagosAProcesarPath;
	}

	public void setPagosAProcesarPath(String pagosAProcesarPath) {
		this.pagosAProcesarPath = pagosAProcesarPath;
	}

	public EstadoFilePagos[] getEstados() {
		return estados;
	}

	public void setEstados(EstadoFilePagos[] estados) {
		this.estados = estados;
	}

	public Long getIdFilePago() {
		return idFilePago;
	}

	public void setIdFilePago(Long idFilePago) {
		this.idFilePago = idFilePago;
	}

	public FilePago getFilePago() {
		return filePago;
	}

	public void setFilePago(FilePago filePago) {
		this.filePago = filePago;
	}

	public Integer getTab() {
		return tab;
	}

	public void setTab(Integer tab) {
		this.tab = tab;
	}
}