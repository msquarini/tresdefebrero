package ar.com.jass.catastro.business;

import java.util.List;
import java.util.Set;

import ar.com.jass.catastro.exceptions.BusinessException;
import ar.com.jass.catastro.model.Cuenta;
import ar.com.jass.catastro.model.CuentaRatificacion;
import ar.com.jass.catastro.model.Nomenclatura;
import ar.com.jass.catastro.model.Parcela;
import ar.com.jass.catastro.model.ParcelaRatificacion;
import ar.com.jass.catastro.model.ResponsableCuenta;
import ar.com.jass.catastro.model.ResponsableCuentaRatificacion;
import ar.com.jass.catastro.model.TitularParcela;
import ar.com.jass.catastro.model.TitularParcelaRatificacion;
import ar.com.jass.catastro.model.TitularRatificacion;
import ar.com.jass.catastro.model.Tramite;

public interface RatificacionService {

	Tramite crearTramiteRatificacion(Cuenta cuentaOrigen) throws Exception;
	
	Tramite procesarTramiteRatificacion(Tramite tramite) throws Exception;
	
    List<Parcela> findParcelasPH(Nomenclatura nomenclatura, String... initialize);
    
    List<ParcelaRatificacion> findParcelasRatificacion(Nomenclatura nomenclatura, String... initialize);
    
    List<ParcelaRatificacion> findParcelasRatificacionRemove(Nomenclatura nomenclatura, String... initialize);
    
    List<ParcelaRatificacion> findParcelasRatificacionNew(Nomenclatura nomenclatura, String... initialize);
    
    ParcelaRatificacion findParcelaRatificacionById(Long id, String... initialize);
    
    CuentaRatificacion findCuentaRatificacionById(Long id, String... initialize);
    
    CuentaRatificacion findCuentaRatificacionByCuenta(String cuenta, String... initialize);
    
    void updateParcelaRatificacion(ParcelaRatificacion parcelaRatificacion);
    
    void updateParcelaRatificacion(Long id_parcela_origen, Long id_cuenta_origen, ParcelaRatificacion parcelaRatificacion, List<TitularParcelaRatificacion> titularesRatificacion, List<ResponsableCuentaRatificacion> responsablesRatificacion, List<Parcela> parcelasOrigen);
    
	void saveParcelaRatificacion(ParcelaRatificacion parcelaRatificacion, List<TitularParcelaRatificacion> titularesRatificacion, List<ResponsableCuentaRatificacion> responsablesRatificacion,List<Parcela> parcelasOrigen, Tramite tramite);

    TitularRatificacion findTitularRatificacionById(Long idTitular,  String... initialize);
	
	List<TitularRatificacion> findTitulares(TitularRatificacion searchTitular);
	
	void saveParcelaRatificacion(ParcelaRatificacion parcelaRatificacion);
	
	void saveCuentaRatificacion(CuentaRatificacion cuentaRatificacion);
	
	void saveTitulares(CuentaRatificacion origenRatificacion, List<TitularParcelaRatificacion> titularesRatificacion,List<ResponsableCuentaRatificacion> responsablesRatificacion);

	List<CuentaRatificacion> findCuentasRatificacionByTramite(Tramite tramite, String... initialize);
	
	boolean existUF(Nomenclatura nomencla);

	
}
