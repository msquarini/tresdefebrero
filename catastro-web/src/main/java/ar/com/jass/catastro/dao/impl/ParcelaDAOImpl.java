package ar.com.jass.catastro.dao.impl;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import ar.com.jass.catastro.dao.ParcelaDAO;
import ar.com.jass.catastro.model.Nomenclatura;
import ar.com.jass.catastro.model.Parcela;
import ar.com.jass.catastro.model.Titular;

@Repository("parcelaDAO")
public class ParcelaDAOImpl extends GenericDAOImpl<Parcela, Serializable>
		implements ParcelaDAO {

	private static final Logger LOG = LoggerFactory.getLogger("ParcelaDAOImpl");

	@SuppressWarnings("unchecked")
	@Override
	public List<Parcela> findByNomenclatura(Nomenclatura nomenclatura) {
		LOG.info("findByNomenclatura {}", nomenclatura);

		Criteria criteria = currentSession().createCriteria(Parcela.class);

		if (nomenclatura.getCircunscripcion() != null) {
			criteria.add(Restrictions.eq("nomenclatura.cicunscripcion",
					nomenclatura.getCircunscripcion()));
		}
		if (nomenclatura.getSeccion() != null) {
			criteria.add(Restrictions.eq("nomenclatura.seccion",
					nomenclatura.getSeccion()));
		}
		if (nomenclatura.getFraccion() != null) {
			criteria.add(Restrictions.eq("nomenclatura.fraccion",
					nomenclatura.getFraccion()));
		}
		if (nomenclatura.getFraccionLetra() != null) {
			criteria.add(Restrictions.eq("nomenclatura.fraccionLetra",
					nomenclatura.getFraccionLetra()));
		}
		if (nomenclatura.getManzana() != null) {
			criteria.add(Restrictions.eq("nomenclatura.manzana",
					nomenclatura.getManzana()));
		}
		if (nomenclatura.getManzanaLetra() != null) {
			criteria.add(Restrictions.eq("nomenclatura.manzanaLetra",
					nomenclatura.getManzanaLetra()));
		}
		if (nomenclatura.getParcela() != null) {
			criteria.add(Restrictions.eq("nomenclatura.parcela",
					nomenclatura.getParcela()));
		}
		if (nomenclatura.getParcelaLetra() != null) {
			criteria.add(Restrictions.eq("nomenclatura.parcelaLetra",
					nomenclatura.getParcelaLetra()));
		}
		return criteria.list();
	}

	// @Override
	// public List<Parcela> findByTitular(Titular titular) {
	// Criteria criteria = currentSession().createCriteria(Titular.class);
	//
	// try {
	// if (titular.getApellido() != null) {
	// criteria.add(Restrictions.like("apellido", titular.getApellido()));
	// }
	// if (titular.getDocumento() != null) {
	// criteria.add(Restrictions.eq("documento", titular.getDocumento()));
	// }
	// return ((Titular) criteria.list().get(0)).getParcelas();
	//
	// } catch (IndexOutOfBoundsException e) {
	// LOG.error("busqueda x Titular no encontro resultado, {}", titular);
	// throw new EntityNotFoundException("titular");
	// }
	// }

	@Override
	public List<Parcela> findByTitular(Titular titular) {
		Criteria criteria = currentSession().createCriteria(Parcela.class);
		criteria.createAlias("titulares", "titular");
		criteria.add(Restrictions.eq("titular.id", titular.getId()));

		return criteria.list();
	}
	
	@Override
	public List<Parcela> findByPartida(Integer partida) {
		Criteria criteria = currentSession().createCriteria(Parcela.class);		
		criteria.add(Restrictions.eq("partida", partida));

		return criteria.list();
	}
	
	@Override
	public List<Parcela> findByCuenta(String cuenta, String dv) {
		Criteria criteria = currentSession().createCriteria(Parcela.class);		
		criteria.add(Restrictions.eq("cuenta.cuenta", cuenta));
		criteria.add(Restrictions.eq("cuenta.dv", dv));

		return criteria.list();
	}
}
