package ar.com.jass.catastro.web.dto;

import java.math.BigDecimal;

import javax.persistence.Transient;

import ar.com.jass.catastro.model.cuentacorriente.CuentaCorriente;
import ar.com.jass.catastro.model.cuentacorriente.TipoMovimiento;

/**
 * DTO informacion de grilla de consulta de movimientos de cuenta corriente
 * 
 * @author msquarini
 *
 */
public class CCTotalizadorDTO {

    private Integer cantidadCreditos = 0;
    private Integer cantidadDebitos = 0;
    private BigDecimal totalCredito = BigDecimal.ZERO;
    private BigDecimal totalDebito = BigDecimal.ZERO;

    public void addMovimiento(CuentaCorriente cc) {
        if (cc.getTipo().equals(TipoMovimiento.C)) {
            cantidadCreditos++;
            totalCredito = totalCredito.add(cc.getImporte());
        } else {
            cantidadDebitos++;
            totalDebito = totalDebito.add(cc.getImporte());
        }
    }

    public Integer getCantidadCreditos() {
        return cantidadCreditos;
    }

    public void setCantidadCreditos(Integer cantidadCreditos) {
        this.cantidadCreditos = cantidadCreditos;
    }

    public Integer getCantidadDebitos() {
        return cantidadDebitos;
    }

    public void setCantidadDebitos(Integer cantidadDebitos) {
        this.cantidadDebitos = cantidadDebitos;
    }

    public BigDecimal getTotalCredito() {
        return totalCredito;
    }

    public void setTotalCredito(BigDecimal totalCredito) {
        this.totalCredito = totalCredito;
    }

    public BigDecimal getTotalDebito() {
        return totalDebito;
    }

    public void setTotalDebito(BigDecimal totalDebito) {
        this.totalDebito = totalDebito;
    }

    public BigDecimal getSaldo() {
        return getTotalDebito().subtract(getTotalCredito());
    }

    public Integer getStatus() {
        if (getSaldo().compareTo(BigDecimal.ZERO) > 0) {
            return 2;
        }
        return 0;
    }
}
