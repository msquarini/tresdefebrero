package ar.com.jass.catastro.util;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.reflect.MethodUtils;
import org.hibernate.collection.spi.PersistentCollection;
import org.hibernate.proxy.HibernateProxy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HibernateHelper {

	private static Logger logger = LoggerFactory.getLogger(HibernateHelper.class);

	public HibernateHelper() {
	}

	/**
	 * Este método inicializa los objetos vinculados al consultado por medio de
	 * relaciones con estrategia del tipo FetchType.LAZY, los parámetros de
	 * inicialización obtiene de la llamada a la interfaz del método donde, el
	 * ultimo parámetro indica el nombre de las propiedades a inicializar
	 * (String… initialize). Soporta propiedades anidadas y complejas. Around
	 * 
	 * @param joinPoint
	 * @throws Throwable
	 */
	// public Object initialize(ProceedingJoinPoint joinPoint) throws Throwable
	// {
	// Object result = joinPoint.proceed();
	// Object[] args = joinPoint.getArgs();
	// logger.debug(
	// "Around -> method: {}, args: {}, result: {}.",
	// new Object[] { joinPoint.getSignature().getName(), args, result });
	// if (args.length > 0 && args[args.length - 1] instanceof String[]) {
	// logger.debug("{} -> initialize: {}.", new Object[] { result,
	// args[args.length - 1] });
	// for (String[] arg : toMatrixGetters((String[]) args[args.length - 1])) {
	// initialize(result, arg);
	// }
	// }
	// return result;
	// }

	public static void initialize(Object proxy, String... properties) {
		for (String[] arg : toMatrixGetters(properties)) {
			initializeIntern(proxy, arg);
		}
	}

	/**
	 * Este método realiza el getter de la propiedad atómica a inicializar del
	 * objeto.
	 * 
	 * @param proxy
	 * @param getters
	 */
	private static void initializeIntern(Object proxy, String[] getters) {
		Object object = null;
		for (int i = 0; i < getters.length; i++) {
			object = i == 0 ? proxy : object;
			if (object != null) {
				if (object instanceof Collection<?>) {
					logger.debug("{} instanceof Collection<?>.", new Object[] { object });
					for (Object objectLocal : (Collection<?>) object) {
					    initializeIntern(objectLocal, ArrayUtils.subarray(getters, i, getters.length));
					}
					break;
				} else {
					logger.debug("{} -> initialize: {}.", new Object[] { object, getters[i] });
					try {
						object = MethodUtils.invokeMethod(object, getters[i], new Object[] {});
						initializeSingleProxy(object, getters[i]);
					} catch (NoSuchMethodException exception) {
						logger.error("In: {} -> NoSuchMethodException: {}.", new Object[] { object, getters[i] },
								exception);
					} catch (IllegalAccessException exception) {
						logger.error("In: {} -> IllegalAccessException: {}.", new Object[] { object, getters[i] },
								exception);
					} catch (InvocationTargetException exception) {
						logger.error("In: {} -> InvocationTargetException: {}.", new Object[] { object, getters[i] },
								exception);
					}
					logger.debug("{} -> result: {}.", new Object[] { getters[i], object });
				}
			}
		}
	}

	/**
	 * Este método realiza el split de las propiedades.
	 * 
	 * @param args
	 * @return String[][]
	 */
	private static String[][] toMatrixGetters(String[] args) {
		ArrayList<String[]> matrix = new ArrayList<String[]>();
		for (String arg : args) {
			String[] getters = arg.replaceAll("\n", "").replaceAll("\t", "").replaceAll(" ", "").split("\\.");
			for (int i = 0; i < getters.length; i++) {
				getters[i] = "get" + getters[i].substring(0, 1).toUpperCase() + getters[i].substring(1);
			}
			matrix.add(getters);
		}
		return matrix.toArray(new String[0][0]);
	}

	/**
	 * Este método inicializa proxies, instancias de HibernateProxy o
	 * PersistentCollection.
	 * 
	 * @param proxy
	 */
	@SuppressWarnings("unused")
	protected static void initializeSingleProxy(final Object proxy, String method) {
		if (proxy != null) {
			if (proxy instanceof HibernateProxy || proxy instanceof PersistentCollection) {
				try {
					if (proxy == null) {
						return;
					} else if (proxy instanceof HibernateProxy) {
						((HibernateProxy) proxy).getHibernateLazyInitializer().initialize();
					} else if (proxy instanceof PersistentCollection) {
						((PersistentCollection) proxy).forceInitialization();
					}
				} catch (Exception exception) {
					logger.error("Ha ocurrido un error al tratar de inicializar el objeto {}.", proxy, exception);
				}
			} else {
				logger.debug(
						"El objeto {} no es una instancia de HibernateProxy o PersistentCollection, es aconsejable quitar la propiedad {} del archivo de configuración.",
						proxy.getClass().getName(), method);
			}
		} else {
			logger.debug("El getter {} retornó null.", method);
		}
	}
}
