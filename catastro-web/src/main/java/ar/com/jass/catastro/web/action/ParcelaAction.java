package ar.com.jass.catastro.web.action;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import ar.com.jass.catastro.business.CatastroService;
import ar.com.jass.catastro.model.Cuenta;
import ar.com.jass.catastro.model.Parcela;
import ar.com.jass.catastro.web.commons.WebConstants;

/**
 * Consulta de parcelas en session 
 * 
 * Utilizado para la subdivision y ratificacion!!!
 * @author msquarini
 *
 */
public class ParcelaAction extends GridBaseAction<Parcela> {

	private static final long serialVersionUID = 7353477345330099548L;

	private static final Logger LOG = LoggerFactory.getLogger("ParcelaAction");

	private String errorMessage;
	private String status;
	
	@Autowired
	private CatastroService catastroService;
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.opensymphony.xwork2.ActionSupport#input()
	 */
	public String input() {
		return "input";
	}

	/**
	 * Obtiene la lista de parcelas en session
	 * 
	 * @return
	 */
	public String getData() {
		LOG.info("parcela - getData");

		try {
			List<Parcela> r = (List<Parcela>) getFromSession(WebConstants.PARCELAS_DATA);
			
			setTotal(r.size());
			
			if (r.size() > getRowCount()) {
				setRows(r.subList(getCurrent() * getRowCount(), getCurrent() * getRowCount() + getRowCount()));
			} else {
				setRows(r);
			}
			
			if (getRows() == null) {
				setRows(new ArrayList<Parcela>());
			}
			
		} catch (Exception e) {
			LOG.error("Error en getData parcelas", e);
		}
		return "dataJSON";
	}

	public CatastroService getCatastroService() {
		return catastroService;
	}

	public void setCatastroService(CatastroService catastroService) {
		this.catastroService = catastroService;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
