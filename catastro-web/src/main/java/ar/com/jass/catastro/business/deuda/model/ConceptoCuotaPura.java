package ar.com.jass.catastro.business.deuda.model;

import java.math.BigDecimal;
import java.math.RoundingMode;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import ar.com.jass.catastro.business.deuda.ConceptoCuotaDTO;
import ar.com.jass.catastro.model.cuentacorriente.CCDetalle.CCDetalleBuilder;

@Entity
@DiscriminatorValue("cuota")
public class ConceptoCuotaPura extends AbstractConceptoCuota {

    @Override
    public void crearConceptoCuota(ConceptoCuotaDTO conceptoCuotaDTO) {

        BigDecimal monto = conceptoCuotaDTO.getBase();

        // Si presenta valuacion -> aplica el cálculo
        if (conceptoCuotaDTO.getValuacion() != null) {
            Double tasa = conceptoCuotaDTO.getTasa();
            monto = conceptoCuotaDTO.getValuacion().multiply(BigDecimal.valueOf(tasa))
                    .divide(CIEN, 4, RoundingMode.HALF_DOWN).divide(DOCE, 4, RoundingMode.HALF_DOWN);
        }
        conceptoCuotaDTO.setMontoCuota(monto);
        conceptoCuotaDTO.addDetalle(new CCDetalleBuilder().monto(monto).concepto(this).build());
    }

}