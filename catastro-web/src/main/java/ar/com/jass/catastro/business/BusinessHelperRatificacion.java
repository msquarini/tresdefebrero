package ar.com.jass.catastro.business;

import java.util.List;

import ar.com.jass.catastro.exceptions.BusinessException;
import ar.com.jass.catastro.model.ParcelaRatificacion;
import ar.com.jass.catastro.model.ResponsableCuentaRatificacion;
import ar.com.jass.catastro.model.TitularParcelaRatificacion;

public interface BusinessHelperRatificacion {

    public void checkResponsablesConCuit(List<ResponsableCuentaRatificacion> responsables) throws BusinessException;
    
	public void checkTitularesConCuit(List<TitularParcelaRatificacion> titulares) throws BusinessException;

	/**
	 * Check si estan la titularidad a 100% y los titulares presenten CUITs
	 * 
	 * @param titulares
	 * @throws BusinessException
	 */
	public void checkTitulares(List<TitularParcelaRatificacion> titulares) throws BusinessException;
	
	public void checkParcelaEnOrigenes(ParcelaRatificacion parcelaRatificacion, List<ParcelaRatificacion> parcelas) throws BusinessException;


}
