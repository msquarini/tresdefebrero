package ar.com.jass.catastro.batch;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;

import ar.com.jass.catastro.dao.BatchDAO;

/**
 * Step para validar la unicidad del procesamiento de archivos de pagos
 * 
 * @author msquarini
 *
 */
@Deprecated
public class ChecksumTasklet implements Tasklet {

	private static final Logger LOG = LoggerFactory.getLogger("ChecksumTasklet");

	@Autowired
	private BatchDAO dao;
	
	@Override
	public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) throws Exception {
		Map<String, Object> params = chunkContext.getStepContext().getJobParameters();

		String fileLocation = (String) params.get("fileLocation");
		String result = checksum(fileLocation);
		
		if (!dao.checkUniquePago(result)) {
			throw new IllegalStateException("Archivo de pagos ya procesado");
		}
		
		LOG.info("ChecksumTasklet - file {} - Value {}", fileLocation, result);

		// Agrego a los parametros del JOB el valor calculado
		chunkContext.getStepContext().getStepExecution().getJobExecution().getExecutionContext().put("checksum", result);
		
		return RepeatStatus.FINISHED;
	}

	/**
	 * Calculo de hash del archivo de pagos
	 * 
	 * @param filename
	 * @return
	 * @throws Exception
	 */
	private String checksum(String filename) throws Exception {
		File file = new File(filename);
		FileInputStream fis = new FileInputStream(file);
		byte[] dataBytes = new byte[1024];

		int nread = 0;
		// convert the byte to hex format
		StringBuffer sb = new StringBuffer("");

		try {
			MessageDigest md = MessageDigest.getInstance("SHA1");

			while ((nread = fis.read(dataBytes)) != -1) {
				md.update(dataBytes, 0, nread);
			}
			byte[] mdbytes = md.digest();

			for (int i = 0; i < mdbytes.length; i++) {
				sb.append(Integer.toString((mdbytes[i] & 0xff) + 0x100, 16).substring(1));
			}

			System.out.println("Digest(in hex format):: " + sb.toString());

		} finally {
			if (fis != null) {
				try {
					fis.close();
				} catch (IOException e) {
					LOG.error("", e);
				}
			}
		}
		return sb.toString();
	}
}
