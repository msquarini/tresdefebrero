package ar.com.jass.catastro.web.action.common;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import ar.com.jass.catastro.business.CodAdministrativoService;
import ar.com.jass.catastro.model.CodigoAdministrativo;
import ar.com.jass.catastro.model.error.ErrorLog;
import ar.com.jass.catastro.web.action.GridBaseAction;
import ar.com.jass.catastro.web.commons.WebConstants;

/**
 * Gestion de codigos administrativos
 * 
 * Alta y edicion de TitularParcela
 * 
 * @author msquarini
 *
 */
public class CodigoAdministrativoAction extends GridBaseAction<CodigoAdministrativo> {

	private static final long serialVersionUID = 7353477345330099549L;

	private static final Logger LOG = LoggerFactory.getLogger(CodigoAdministrativoAction.class);

	/**
	 * Se utiliza para indicar si es modo edicion o alta de titular
	 */
	private static final String EDIT_MODE = "edit_mode";

	/**
	 * Id de codigo administrativo
	 */
	private Long idCodAdm;

	/**
	 * Nuevo TitularParcela
	 */
	private CodigoAdministrativo codigoAdministrativo;

	@Autowired
	private CodAdministrativoService codAdministrativoService;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.opensymphony.xwork2.ActionSupport#input()
	 */
	public String input() {
		return "input";
	}

	@SuppressWarnings("unchecked")
	public String search() {
		LOG.info("busqueda de codigos administrativos");

		try {
			List<CodigoAdministrativo> data = (List<CodigoAdministrativo>) getConsultaService()
					.findAll(CodigoAdministrativo.class);
			setInSession(WebConstants.COD_ADM_DATA, data);

			return "okJSON";
		} catch (Exception e) {
			ErrorLog log = crearErrorLog("search", e.getMessage());
			LOG.error("Error en busqueda de codigos administrativos, {}", log, e);
			addActionError("Error en proceso de búsqueda, intente nuevamente.");
			return "errorJSON";
		}

	}

	/**
	 * Actualizar codigo administrativo
	 * 
	 * @return
	 */
	public String update() {
		LOG.info("update Cod Administrivo, ID[{}], [{}]", getIdCodAdm(), getCodigoAdministrativo());

		try {
		    if ((Boolean)getFromSession(EDIT_MODE)) {
		        CodigoAdministrativo codAdm = (CodigoAdministrativo)getFromSession(WebConstants.COD_ADM);
		        codAdm.setCodigo(codigoAdministrativo.getCodigo());
		        codAdm.setNombre(codigoAdministrativo.getNombre());
		        codAdm.setVigenteDesde(codigoAdministrativo.getVigenteDesde());
		        codAdm.setVigenteHasta(codigoAdministrativo.getVigenteHasta());
		        codigoAdministrativo = codAdm;
		    }
			codAdministrativoService.saveOrUpdate(codigoAdministrativo);
		} catch (Exception e) {
			ErrorLog log = crearErrorLog("update CodAdm", e.getMessage());
			LOG.error("{}", log, e);
			addActionError("error_update_cod_administrativo");
			return "errorJSON";
		}
		return "okJSON";
	}

	/**
	 * Llamada AJAX para obtener la informacion para la grilla en formato JSON
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public String getData() {
		try {
			List<CodigoAdministrativo> data = (List<CodigoAdministrativo>) getFromSession(WebConstants.COD_ADM_DATA);
			setData(data);
		} catch (Exception e) {
			LOG.error("error en getData codigos administrativos", e);
		}
		return "dataJSON";
	}

	/**
	 * Redirije a pagina de alta
	 * 
	 * @return
	 */
	public String goToAdd() {
		codigoAdministrativo = new CodigoAdministrativo();
		setInSession(EDIT_MODE, false);
		return "codigo_administrativo_input";
	}

	/**
	 * Buscar el titular por el cuitcuil y setearlo para editar
	 * 
	 * @return
	 */
	public String goToEdit() {
		codigoAdministrativo = codAdministrativoService.find(getIdCodAdm());
		if (codigoAdministrativo == null) {
			ErrorLog log = crearErrorLog("goToEdit", "no se encontro CodAdm");
			LOG.error("{}", log);
			addActionError("error_update_cod_administrativo");
			return "errorJSON";
		}
		setInSession(EDIT_MODE, true);
		setInSession(WebConstants.COD_ADM, codigoAdministrativo);
		return "codigo_administrativo_input";
	}

	public CodAdministrativoService getCodAdministrativoService() {
		return codAdministrativoService;
	}

	public void setCodAdministrativoService(CodAdministrativoService codAdministrativoService) {
		this.codAdministrativoService = codAdministrativoService;
	}

	public Long getIdCodAdm() {
		return idCodAdm;
	}

	public void setIdCodAdm(Long idCodAdm) {
		this.idCodAdm = idCodAdm;
	}

	public CodigoAdministrativo getCodigoAdministrativo() {
		return codigoAdministrativo;
	}

	public void setCodigoAdministrativo(CodigoAdministrativo codigoAdministrativo) {
		this.codigoAdministrativo = codigoAdministrativo;
	}
}