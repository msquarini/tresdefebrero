package ar.com.jass.catastro.model.pagos;

public enum EstadoFilePagos {

    PENDIENTE {
        @Override
        public boolean puedeProcesar() {
            return Boolean.TRUE;
        }
    },
    PROCESANDO, ERROR, FINALIZADO;

    public boolean puedeProcesar() {
        return Boolean.FALSE;
    }
}
