package ar.com.jass.catastro.web.action;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import ar.com.jass.catastro.business.BusinessHelper;
import ar.com.jass.catastro.business.ConsultaService;
import ar.com.jass.catastro.business.RatificacionService;
import ar.com.jass.catastro.model.CuentaRatificacion;
import ar.com.jass.catastro.model.ResponsableCuentaRatificacion;
import ar.com.jass.catastro.model.Titular;
import ar.com.jass.catastro.model.TitularParcelaRatificacion;
import ar.com.jass.catastro.model.TitularRatificacion;
import ar.com.jass.catastro.model.Tramite;
import ar.com.jass.catastro.web.commons.WebConstants;
import ar.com.jass.catastro.web.commons.WebConstantsRatificacion;

/**
 * Gestion ABM de Resposanbles de cuenta
 * 
 * 
 * @author msquarini
 *
 */
public class ResponsableABMRatificacionAction extends GridBaseAction<ResponsableCuentaRatificacion> {

    private static final long serialVersionUID = 7353477345330099541L;

    private static final Logger LOG = LoggerFactory.getLogger("ResponsableABMAction");

    /**
     * Se utiliza para tener en session el ID o Cuit del titular en edicion
     */
    private static final String EDIT_TITULAR = "titular_edition";

    /**
     * Se utiliza para indicar si es modo edicion o alta de titular
     */
    private static final String EDIT_MODE = "edit_mode";

    /**
     * ID de titular para agregar a la lista de Responsables
     */
    private Long idTitular;

    private String uuidTP;

    /**
     * Nuevo TitularParcela
     */
    private ResponsableCuentaRatificacion responsable;

    @Autowired
    private BusinessHelper businessHelper;
    
    @Autowired
    private ConsultaService consultaService;
    
    @Autowired
    private RatificacionService ratificacionService;

    private Tramite tramite;
    private List<ResponsableCuentaRatificacion> responsables;
    
    private CuentaRatificacion cuentaRatificacionOrigen;

    /*
     * (non-Javadoc)
     * 
     * @see com.opensymphony.xwork2.ActionSupport#input()
     */
    public String input() {
        return "input";
    }

    /**
     * Inicializar la gestion de cambio de responsables
     * 
     * @return
     */
    public String init() {
        // Busqueda de cuenta origen a partir de ID
    	cuentaRatificacionOrigen =  ratificacionService.findCuentaRatificacionById(idCuentaOrigen, "responsables", "parcela.titulares",
                "parcela.domicilioInmueble");



        setInSession(WebConstantsRatificacion.CUENTA_RATIFICACION_ORIGEN, cuentaRatificacionOrigen);
        // Titulares en session para editar
        setInSession(WebConstantsRatificacion.TITULARES_RATIFICACION_ADD, new HashSet<TitularParcelaRatificacion>(cuentaRatificacionOrigen.getParcela().getTitulares()));
        setInSession(WebConstantsRatificacion.RESPONSABLES_RATIFICACION_ADD, new HashSet<ResponsableCuentaRatificacion>(cuentaRatificacionOrigen.getResponsables()));
        setInSession(WebConstants.TITULARES_DATA, null); //???
        return "inprocess";
    }

    public String initFromTramite() {
        tramite = (Tramite) getFromSession(WebConstants.TRAMITE);
        //cuentaOrigen = tramite.getCuentasOrigen().get(0);
        
        
        
        cuentaRatificacionOrigen = ratificacionService.findCuentaRatificacionByCuenta(tramite.getCuentasOrigen().get(0).getCuenta(), "responsables", "parcela.titulares",
                "parcela.domicilioInmueble");
        
        setInSession(WebConstantsRatificacion.CUENTA_RATIFICACION_ORIGEN, cuentaRatificacionOrigen);
        // Titulares en session
        setInSession(WebConstantsRatificacion.TITULARES_RATIFICACION_ADD, new HashSet<TitularParcelaRatificacion>(cuentaRatificacionOrigen.getParcela().getTitulares()));
        setInSession(WebConstantsRatificacion.RESPONSABLES_RATIFICACION_ADD, new HashSet<ResponsableCuentaRatificacion>(cuentaRatificacionOrigen.getResponsables()));
        setInSession(WebConstants.TITULARES_DATA, null);
        return "inprocess";
    }

    /**
     * Llamada AJAX para obtener la informacion para la grilla en formato JSON Grilla de edicion de
     * titulares
     * 
     * @return
     */
    @SuppressWarnings("unchecked")
    public String getData() {
        try {
            // List<ResponsableCuenta> data = (List<ResponsableCuenta>)
            // getFromSession(WebConstants.RESPONSABLES_ADD);
            List<ResponsableCuentaRatificacion> data = new ArrayList<ResponsableCuentaRatificacion>(
                    (Set<ResponsableCuentaRatificacion>) getFromSession(WebConstantsRatificacion.RESPONSABLES_RATIFICACION_ADD));
            setData(data);
        } catch (Exception e) {
            LOG.error("error en getData responsables", e);
        }
        return "dataJSON";
    }

    /**
     * Redirije a pagina de alta
     * 
     * @return
     */
    public String goToAddTitular() {
        setInSession(EDIT_MODE, false);
        return "add_responsable_input";
    }

    /**
     * Buscar el titular por el cuitcuil y setearlo para editar
     * 
     * @return
     */
    public String goToEditTitular() {
        Set<ResponsableCuentaRatificacion> responsables = (Set<ResponsableCuentaRatificacion>) getFromSession(WebConstantsRatificacion.RESPONSABLES_RATIFICACION_ADD);
        UUID uuid = UUID.fromString(getUuidTP());
        for (ResponsableCuentaRatificacion rc : responsables) {
            if (uuid.equals(rc.getUuid())) {
                responsable = rc;
                setInSession(EDIT_TITULAR, uuid);
                break;
            }
        }

        setInSession(EDIT_MODE, true);
        return "add_responsable_input";

    }

    /**
     * Agrega responsable ya existente a la grilla ABM de responsables de cuenta. Valido que el
     * titular no este presente en la lista
     * 
     * @return
     */
    @SuppressWarnings("unchecked")
	public String addSelectedTitular() {
        LOG.info("Agregando Responsable ID [{}]", getIdTitular());
        Set<ResponsableCuentaRatificacion> resposablesCuenta = (Set<ResponsableCuentaRatificacion>) getFromSession(WebConstantsRatificacion.RESPONSABLES_RATIFICACION_ADD);
        if (!resposablesCuenta.isEmpty()) {
            // Verificar que no exista en la grilla por el ID del titular
            for (ResponsableCuentaRatificacion rc : resposablesCuenta) {

                // Si existe el titular dentro de los responsable de cuenta, vuelvo sin
                // agregarlo
                if (getIdTitular().equals(rc.getTitular().getId())) {
                    return "update_ok";
                }
            }
        }
        Titular titular = consultaService.findTitularById(getIdTitular(), "domicilioPostal.barrio");
        ResponsableCuentaRatificacion newResponsableCuenta = new ResponsableCuentaRatificacion();
        TitularRatificacion titularRatificacion = new TitularRatificacion(titular);
        newResponsableCuenta.setTitular(titularRatificacion);
        newResponsableCuenta.setCuenta((CuentaRatificacion)getFromSession(WebConstantsRatificacion.CUENTA_RATIFICACION_ORIGEN));
        resposablesCuenta.add(newResponsableCuenta);
        return "update_ok";
    }

    /**
     * Actualiza la grilla de responsables en la gestion de tramite
     * 
     * @return
     */
    @SuppressWarnings("unchecked")
    public String addResponsable() {
        boolean isEditMode = (Boolean) getFromSession(EDIT_MODE);
        LOG.info("AddTitular ModoEdicion {}, {}", isEditMode, responsable);
        Set<ResponsableCuentaRatificacion> responsablesCuenta = (Set<ResponsableCuentaRatificacion>) getFromSession(WebConstantsRatificacion.RESPONSABLES_RATIFICACION_ADD);
        
        responsable.setCuenta((CuentaRatificacion)getFromSession(WebConstantsRatificacion.CUENTA_RATIFICACION_ORIGEN));

        try { // Si no es modo edit -> es ALTA
            if (!isEditMode) {
                responsablesCuenta.add(responsable);
            } else {
                for (ResponsableCuentaRatificacion rc : responsablesCuenta) {
                    if (isResponsableCuentaInEdition(rc)) {
                        LOG.info("Se modifico el Responsable de cuenta: {}", rc);
                        updateValues(rc);
                        break;
                    }
                }
                setInSession(EDIT_TITULAR, null);
            }
        } catch (Exception e) {
            addActionError("error en ALTA de responsable en grilla de edicion!!!");
            LOG.error("error agregando responsable", e);
            return "errorJSON";
        }
        return "update_ok";
    }

    /**
     * Actualiza los datos del responsable
     * 
     * @param t
     */
    private void updateValues(ResponsableCuentaRatificacion rc) {
        updateTitularValues(responsable.getTitular(), rc.getTitular());
        rc.setDesde(responsable.getDesde());
        rc.setHasta(responsable.getHasta());        
    }
    
    /*Para no toca Base Action*/
	private void updateTitularValues(TitularRatificacion from, TitularRatificacion to) {
		to.setCuitcuil(from.getCuitcuil());
		to.setApellido(from.getApellido());
		to.setNombre(from.getNombre());
		to.setDocumento(from.getDocumento());
		to.setDomicilioPostal(from.getDomicilioPostal());
		to.setRazonSocial(from.getRazonSocial());
		to.setObservacion(from.getObservacion());
		to.setPersonaJuridica(from.getPersonaJuridica());
		to.setTipoDocumento(from.getTipoDocumento());
		to.setMail(from.getMail());
	}
    
    

    /**
     * Valida que el titular es el que esta en modo edicion.
     * 
     * @param t
     * @return
     */
    private boolean isResponsableCuentaInEdition(ResponsableCuentaRatificacion rc) {
        return ((UUID) getFromSession(EDIT_TITULAR)).equals(rc.getUuid());
    }

    /**
     * Procesar el tramite de gestion de responsables
     * 
     * @return
     */
    /*public String process() {
        try {
            cuentaOrigen = (Cuenta) getFromSession(WebConstants.CUENTA_ORIGEN);

            tramite = (Tramite) getFromSession(WebConstants.TRAMITE);
            if (tramite == null) {
                throw new IllegalStateException();
            }

            responsables = new ArrayList<>((Set<ResponsableCuenta>) getFromSession(WebConstants.RESPONSABLES_ADD));

            // Chequear que los Titulares presenten CUIT/CUIL cargado/actualizado
            businessHelper.checkResponsablesConCuit(responsables);

            tramite = getCatastroService().updateResponsables(tramite, cuentaOrigen, responsables);

            addActionMessage(getText("tramite.process.success"));
            removeFromSession(WebConstants.TRAMITE);
        } catch (BusinessException e) {
            LOG.error("error en procesamiento de tramite de actualizacion de responsables", e);
            addActionError(getText(e.getMessage()));
            return "errorJSON";
        } catch (Exception e) {
            LOG.error("error en procesamiento de tramite de actualizacion de responsables", e);
            addActionError(getText("error.unknown"));
            return "errorJSON";
        }
        // Ticket del tramite!
        return "success";
    }*/

    public Long getIdTitular() {
        return idTitular;
    }

    public void setIdTitular(Long idTitular) {
        this.idTitular = idTitular;
    }

    public String getUuidTP() {
        return uuidTP;
    }

    public void setUuidTP(String uuidTP) {
        this.uuidTP = uuidTP;
    }

    public ResponsableCuentaRatificacion getResponsable() {
        return responsable;
    }

    public void setResponsable(ResponsableCuentaRatificacion responsable) {
        this.responsable = responsable;
    }

    public Tramite getTramite() {
        return tramite;
    }

    public void setTramite(Tramite tramite) {
        this.tramite = tramite;
    }

    public List<ResponsableCuentaRatificacion> getResponsables() {
        return responsables;
    }

    public void setResponsables(List<ResponsableCuentaRatificacion> responsables) {
        this.responsables = responsables;
    }

	public CuentaRatificacion getCuentaRatificacionOrigen() {
		return cuentaRatificacionOrigen;
	}

	public void setCuentaRatificacionOrigen(CuentaRatificacion cuentaRatificacionOrigen) {
		this.cuentaRatificacionOrigen = cuentaRatificacionOrigen;
	}

}