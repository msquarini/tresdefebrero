package ar.com.jass.catastro.dao.impl;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityNotFoundException;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.AliasToEntityMapResultTransformer;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ar.com.jass.catastro.business.deuda.model.TemplateCuota;
import ar.com.jass.catastro.dao.CuentaCorrienteDAO;
import ar.com.jass.catastro.model.Cuenta;
import ar.com.jass.catastro.model.cuentacorriente.Boleta;
import ar.com.jass.catastro.model.cuentacorriente.CuentaCorriente;
import ar.com.jass.catastro.model.cuentacorriente.DeudaPeriodoStatus;
import ar.com.jass.catastro.model.cuentacorriente.GeneracionDeudaDTO;
import ar.com.jass.catastro.model.cuentacorriente.Periodo;
import ar.com.jass.catastro.model.cuentacorriente.TasaCategoria;
import ar.com.jass.catastro.model.cuentacorriente.TipoMovimiento;
import ar.com.jass.catastro.web.dto.ResumenGeneracionDeuda;

@Repository("ccDAO")
public class CuentaCorrienteDAOImpl extends GeneralDAOImpl implements CuentaCorrienteDAO {

    private static final Logger LOG = LoggerFactory.getLogger(CuentaCorrienteDAOImpl.class);

    private static final String CC_VALORES = "select min(importe) as minImporte, max(importe) as maxImporte, avg(importe) as avgImporte, "
            + " count(*) as cantidadCC from cc where anio = :anio and cuota = :cuota";

    private static final String CA_VALORES = "select min(importe) as minImporte, max(importe) as maxImporte, avg(importe) as avgImporte, "
            + " count(*) as cantidadCA from cuota_anual where anio = :anio and cuota = :cuota";

    /**
     * Consulta de cuentas para la generacion de deuda Excluye las cuentas que ya presentan deuda
     * generada para el periodo
     */
    private static final String GENERACION_DEUDA = "select  c.id_cuenta as idCuenta, c.cuenta as cuenta, c.dv as dv, false as exenta, l.categoria as categoria, "
            + " l.valor_tierra as valorTierra, l.valor_comun as valorComun, l.valor_edificio as valorEdificio, c.con_tope as conTope "
            + " from Cuenta c inner join Parcela p ON c.id_cuenta = p.id_cuenta left outer join Linea l ON p.id_linea = l.id_linea "
            + " where c.activa = true and not exists (select 1 from cc where cc.id_cuenta = c.id_cuenta and cc.anio = :anio and cc.cuota = :cuota and cc.tipo = 'D') ";

    private static final String GENERACION_DEUDA_DV = "select  c.id_cuenta as idCuenta, c.cuenta as cuenta, c.dv as dv, false as exenta, l.categoria as categoria, "
            + " l.valor_tierra as valorTierra, l.valor_comun as valorComun, l.valor_edificio as valorEdificio, c.con_tope as conTope  "
            + " from Cuenta c inner join Parcela p ON c.id_cuenta = p.id_cuenta inner join Linea l ON p.id_linea = l.id_linea "
            + " where c.dv = :dv and c.activa = true and not exists (select 1 from cc where cc.id_cuenta = c.id_cuenta and cc.anio = :anio and cc.cuota = :cuota and cc.tipo = 'D') ";

    /**
     * Consulta de Cuentas para la generacion de cuota anual en un periodo
     */
    private static final String GENERACION_CUOTA_ANUAL = "select  c.id_cuenta as idCuenta, c.cuenta as cuenta, c.dv as dv, false as exenta, l.categoria as categoria, "
            + " l.valor_tierra as valorTierra, l.valor_comun as valorComun, l.valor_edificio as valorEdificio, c.con_tope as conTope  "
            + " from Cuenta c inner join Parcela p ON c.id_cuenta = p.id_cuenta inner join Linea l ON p.id_linea = l.id_linea "
            + " where c.activa = true and not exists (select 1 from cuota_anual ca where ca.id_cuenta = c.id_cuenta and ca.anio = :anio and ca.cuota = :cuota and ca.tipo = 'D') ";

    /*
     * @SuppressWarnings("unchecked")
     * 
     * @Override
     * 
     * @Deprecated public List<Cuenta> getCuentasGeneracionDeuda() { Criteria criteria =
     * currentSession().createCriteria(Cuenta.class); criteria.setFetchMode("parcelas.lineaVigente",
     * FetchMode.JOIN); criteria.add(Restrictions.eq("activa", Boolean.TRUE)); //
     * criteria.add(Restrictions.eq("exenta", Boolean.FALSE)); return criteria.list(); }
     */

    public ResumenGeneracionDeuda getResumenGeneracion(ar.com.jass.catastro.model.cuentacorriente.Periodo periodo) {
        ResumenGeneracionDeuda resumen;

        SQLQuery sqlQuery = currentSession().createSQLQuery(CC_VALORES);
        sqlQuery.setInteger("anio", periodo.getAnio());
        sqlQuery.setInteger("cuota", periodo.getCuota());

        sqlQuery.addScalar("minImporte", StandardBasicTypes.BIG_DECIMAL);
        sqlQuery.addScalar("maxImporte", StandardBasicTypes.BIG_DECIMAL);
        sqlQuery.addScalar("avgImporte", StandardBasicTypes.BIG_DECIMAL);
        sqlQuery.addScalar("cantidadCC", StandardBasicTypes.INTEGER);

        sqlQuery.setResultTransformer(Transformers.aliasToBean(ResumenGeneracionDeuda.class));
        resumen = (ResumenGeneracionDeuda) sqlQuery.list().get(0);

        // Calculo de los agregadores de CUOTA ANUAL
        sqlQuery = currentSession().createSQLQuery(CA_VALORES);
        sqlQuery.setInteger("anio", periodo.getAnio());
        sqlQuery.setInteger("cuota", periodo.getCuota());

        sqlQuery.addScalar("minImporte", StandardBasicTypes.BIG_DECIMAL);
        sqlQuery.addScalar("maxImporte", StandardBasicTypes.BIG_DECIMAL);
        sqlQuery.addScalar("avgImporte", StandardBasicTypes.BIG_DECIMAL);
        sqlQuery.addScalar("cantidadCA", StandardBasicTypes.INTEGER);

        sqlQuery.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
        try {
            Map<String, Object> cuotaAnualResumen = (Map<String, Object>) sqlQuery.list().get(0);

            resumen.setCantidadCA((Integer) cuotaAnualResumen.get("cantidadCA"));
            resumen.setAvgImporteCA((BigDecimal) cuotaAnualResumen.get("avgImporte"));
            resumen.setMinImporteCA((BigDecimal) cuotaAnualResumen.get("minImporte"));
            resumen.setMaxImporteCA((BigDecimal) cuotaAnualResumen.get("maxImporte"));
        } catch (Exception e) {
            LOG.error("error consultado datos agregados de cuota anual", e);
        }

        return resumen;
    }

    /**
     * Consulta de cuentas optimizada para la generacion de deudas
     * 
     * @return
     */
    public List<GeneracionDeudaDTO> getCuentasGeneracionDeudaDTO(Periodo periodo, String dv, Boolean cuotaAnual) {
        SQLQuery sqlQuery;

        if (cuotaAnual) {
            sqlQuery = currentSession().createSQLQuery(GENERACION_CUOTA_ANUAL);
        } else {
            // Filtro x DV para acotar el resultado
            if (dv != null) {
                sqlQuery = currentSession().createSQLQuery(GENERACION_DEUDA_DV);
                sqlQuery.setString("dv", dv);
            } else {
                sqlQuery = currentSession().createSQLQuery(GENERACION_DEUDA);
            }
        }

        // Filtros x periodo
        sqlQuery.setInteger("anio", periodo.getAnio());
        sqlQuery.setInteger("cuota", periodo.getCuota());

        // Resultado
        sqlQuery.addScalar("idCuenta", StandardBasicTypes.LONG);
        sqlQuery.addScalar("cuenta", StandardBasicTypes.STRING);
        sqlQuery.addScalar("dv", StandardBasicTypes.STRING);
        sqlQuery.addScalar("exenta", StandardBasicTypes.BOOLEAN);
        sqlQuery.addScalar("conTope", StandardBasicTypes.BOOLEAN);
        sqlQuery.addScalar("categoria", StandardBasicTypes.INTEGER);
        sqlQuery.addScalar("valorTierra", StandardBasicTypes.BIG_DECIMAL);
        sqlQuery.addScalar("valorComun", StandardBasicTypes.BIG_DECIMAL);
        sqlQuery.addScalar("valorEdificio", StandardBasicTypes.BIG_DECIMAL);

        sqlQuery.setResultTransformer(Transformers.aliasToBean(GeneracionDeudaDTO.class));
        return sqlQuery.list();
    }

    /*
     * (non-Javadoc)
     * 
     * @see ar.com.jass.catastro.dao.CuentaCorrienteDAO#getPeriodoInState(ar.com.jass.
     * catastro.model.cuentacorriente.DeudaPeriodoStatus)
     */
    public List<Periodo> getPeriodoInState(DeudaPeriodoStatus status) {
        Criteria criteria = currentSession().createCriteria(Periodo.class);
        criteria.add(Restrictions.eq("estado", status));

        try {
            return criteria.list();
        } catch (IndexOutOfBoundsException iobe) {
            throw new EntityNotFoundException();
        }
    }

    @Override
    public Periodo findPeriodoVigente() {
        Calendar now = Calendar.getInstance();
        now.add(Calendar.MONTH, 1);
        Criteria criteria = currentSession().createCriteria(Periodo.class);
        criteria.add(Restrictions.eq("anio", now.get(Calendar.YEAR)));
        criteria.add(Restrictions.eq("cuota", now.get(Calendar.MONTH) + 1));

        try {
            return (Periodo) criteria.uniqueResult();
        } catch (IndexOutOfBoundsException iobe) {
            throw new EntityNotFoundException();
        }

    }

    public List<Periodo> findPeriodos(Integer anio) {
        Criteria criteria = currentSession().createCriteria(Periodo.class);
        criteria.add(Restrictions.eq("anio", anio));
        criteria.addOrder(Order.asc("cuota"));

        try {
            return criteria.list();
        } catch (IndexOutOfBoundsException iobe) {
            throw new EntityNotFoundException();
        }
    }

    public List<Periodo> findPeriodos(Periodo desde, Periodo hasta) {
        Criteria criteria = currentSession().createCriteria(Periodo.class);
        criteria.add(Restrictions.ge("anio", desde.getAnio()));
        criteria.add(Restrictions.ge("cuota", desde.getCuota()));
        criteria.add(Restrictions.le("anio", hasta.getAnio()));
        criteria.add(Restrictions.lt("cuota", hasta.getCuota()));
        criteria.addOrder(Order.asc("anio"));
        criteria.addOrder(Order.asc("cuota"));

        try {
            return criteria.list();
        } catch (IndexOutOfBoundsException iobe) {
            throw new EntityNotFoundException();
        }
    }

    public Periodo findPeriodo(Integer anio, Integer cuota) {
        Criteria criteria = currentSession().createCriteria(Periodo.class);
        criteria.add(Restrictions.eq("anio", anio));
        criteria.add(Restrictions.eq("cuota", cuota));

        try {
            return (Periodo) criteria.uniqueResult();
        } catch (IndexOutOfBoundsException iobe) {
            throw new EntityNotFoundException();
        }
    }

    public List<Periodo> findPeriodosDesde(Integer anio, Integer cuota) {
        Criteria criteria = currentSession().createCriteria(Periodo.class);
        
        criteria.add(Restrictions.or(
                Restrictions.and(Restrictions.eq("anio", anio), Restrictions.ge("cuota", cuota)),
                Restrictions.gt("anio", anio)));
        
        criteria.addOrder(Order.asc("anio"));
        criteria.addOrder(Order.asc("cuota"));

        try {
            return criteria.list();
        } catch (IndexOutOfBoundsException iobe) {
            throw new EntityNotFoundException();
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<CuentaCorriente> findMovimientos(Long idCuenta, Integer anio, TipoMovimiento tipo) {
        Criteria criteria = currentSession().createCriteria(CuentaCorriente.class);
        criteria.addOrder(Order.desc("anio"));
        criteria.addOrder(Order.desc("cuota"));

        criteria.add(Restrictions.eq("cuenta.id", idCuenta));

        if (anio != null) {
            criteria.add(Restrictions.eq("anio", anio));
        }
        if (tipo != null) {
            criteria.add(Restrictions.eq("tipo", tipo));
        }

        return criteria.list();
    }

    public List<CuentaCorriente> findMovimientosEnAnio(Long idCuenta, Integer anio, Integer cuota, Integer ajuste,
            TipoMovimiento tipo) {
        Criteria criteria = currentSession().createCriteria(CuentaCorriente.class);
        criteria.addOrder(Order.asc("anio"));
        criteria.addOrder(Order.asc("cuota"));

        criteria.add(Restrictions.eq("cuenta.id", idCuenta));
        criteria.add(Restrictions.eq("anio", anio));

        if (cuota != null) {
            criteria.add(Restrictions.ge("cuota", cuota));
        }
        if (ajuste != null) {
            criteria.add(Restrictions.eq("ajuste", ajuste));
        }
        if (tipo != null) {
            criteria.add(Restrictions.eq("tipo", tipo));
        }

        return criteria.list();
    }

    public List<CuentaCorriente> findMovimientos(Long cuentaId, Integer anio, Integer cuota) {
        Criteria criteria = currentSession().createCriteria(CuentaCorriente.class);
        criteria.addOrder(Order.asc("anio"));
        criteria.addOrder(Order.asc("cuota"));

        criteria.add(Restrictions.eq("cuenta.id", cuentaId));

        if (anio != null) {
            criteria.add(Restrictions.eq("anio", anio));
        }
        if (cuota != null) {
            criteria.add(Restrictions.eq("cuota", cuota));
        }

        return criteria.list();
    }

    public CuentaCorriente findCuotaByCuentaId(Long idCuenta, Integer anio, Integer cuota, Integer ajuste,
            TipoMovimiento tipo) {
        Criteria criteria = currentSession().createCriteria(CuentaCorriente.class);
        criteria.addOrder(Order.asc("anio"));
        criteria.addOrder(Order.asc("cuota"));

        criteria.add(Restrictions.eq("cuenta.id", idCuenta));
        criteria.add(Restrictions.eq("anio", anio));
        criteria.add(Restrictions.eq("cuota", cuota));
        criteria.add(Restrictions.eq("tipo", tipo));
        if (ajuste != null) {
            criteria.add(Restrictions.eq("ajuste", ajuste));
        }
        try {
            return (CuentaCorriente) criteria.uniqueResult();
        } catch (IndexOutOfBoundsException iobe) {
            throw new EntityNotFoundException();
        }
    }

    public List<CuentaCorriente> findMovimientos(Long idCuenta, Periodo desde, TipoMovimiento tipo, Integer ajuste) {
        return findMovimientos(idCuenta, (desde != null) ? desde.getAnio() : null,
                (desde != null) ? desde.getCuota() : null, null, null, tipo, ajuste);
    }
    
    public List<CuentaCorriente> findMovimientos(Long idCuenta, Periodo desde, Periodo hasta, TipoMovimiento tipo, Integer ajuste) {
        return findMovimientos(idCuenta, (desde != null) ? desde.getAnio() : null,
                (desde != null) ? desde.getCuota() : null, (hasta != null) ? hasta.getAnio() : null,
                        (hasta != null) ? hasta.getCuota() : null, tipo, ajuste);
    }

    public List<CuentaCorriente> findMovimientos(Long idCuenta, Integer desdeAnio, Integer desdeCuota,
            Integer hastaAnio, Integer hastaCuota,
            TipoMovimiento tipo, Integer ajuste) {
        Criteria criteria = currentSession().createCriteria(CuentaCorriente.class);
        criteria.addOrder(Order.asc("anio"));
        criteria.addOrder(Order.asc("cuota"));

        criteria.add(Restrictions.eq("cuenta.id", idCuenta));

        if (desdeAnio != null) {
            // Anio > desdeAnio or (anio = desdeAnio and cuota >= desdecuota)
            criteria.add(Restrictions.or(
                    Restrictions.and(Restrictions.eq("anio", desdeAnio), Restrictions.ge("cuota", desdeCuota)),
                    Restrictions.gt("anio", desdeAnio)));            
        }
        if (hastaAnio != null) {
            // Anio < hastaAnio or (anio = hastaAnio and cuota <= hastaCuota)
            criteria.add(Restrictions.or(
                    Restrictions.and(Restrictions.eq("anio", hastaAnio), Restrictions.le("cuota", hastaCuota)),
                    Restrictions.lt("anio", hastaAnio)));
        }

        if (tipo != null) {
            criteria.add(Restrictions.eq("tipo", tipo));
        }
        if (ajuste != null) {
            criteria.add(Restrictions.eq("ajuste", ajuste));
        }
        return criteria.list();
    }

    @Override
    public List<CuentaCorriente> findMovimientosVencidos(Long idCuenta) {
        Criteria criteria = currentSession().createCriteria(CuentaCorriente.class);

        // Fecha de vencimiento menor A
        criteria.add(Restrictions.lt("fechaVto", new Date()));
        // Solo movimiento de Deuda (DEBITO)
        criteria.add(Restrictions.eq("tipo", TipoMovimiento.D));
        // Sin pago
        criteria.add(Restrictions.isNull("pago"));
        // Cod Adm es 0
        criteria.add(Restrictions.eq("codAdministrativo", 0));

        return criteria.list();
    }

    public List<CuentaCorriente> findDeudaMensual(Long idCuenta, Integer anio, Integer cuota, Boolean sinPago) {
        Criteria criteria = currentSession().createCriteria(CuentaCorriente.class);
        criteria.add(Restrictions.eq("cuenta.id", idCuenta));
        criteria.add(Restrictions.eq("anio", anio));
        criteria.add(Restrictions.eq("cuota", cuota));
        criteria.add(Restrictions.eq("tipo", TipoMovimiento.D));

        if (sinPago) {
            criteria.add(Restrictions.isNull("pago"));
        }
        return criteria.list();
    }

    /*
     * public List<CuotaAnual> findDeudaAnual(Long idCuenta, Integer anio, Integer cuota, Boolean
     * sinPago) { Criteria criteria = currentSession().createCriteria(CuotaAnual.class);
     * criteria.add(Restrictions.eq("anio", anio)); criteria.add(Restrictions.eq("cuota", cuota));
     * criteria.add(Restrictions.eq("tipo", TipoMovimiento.D));
     * criteria.add(Restrictions.eq("cuenta.id", idCuenta)); if (sinPago) {
     * criteria.add(Restrictions.isNull("pago")); } return criteria.list(); }
     */

    @Transactional
    public List<TasaCategoria> findTasas(Integer anio) {
        Criteria criteria = currentSession().createCriteria(TasaCategoria.class);
        criteria.add(Restrictions.eq("anio", anio));
        return criteria.list();
    }

    @Override
    public Boleta findBoleta(Integer anio, Integer cuota, String cuenta, String dv, BigDecimal importe, Date fechaVto) {
        LOG.debug("Buscando Boleta {} {} {} {}", anio, cuota, cuenta, dv);

        Criteria c = getSessionFactory().getCurrentSession().createCriteria(Boleta.class);
        c.add(Restrictions.eq("anio", anio));
        c.add(Restrictions.eq("cuota", cuota));
        c.createAlias("cuenta", "c");
        c.add(Restrictions.eq("c.cuenta", cuenta));
        c.add(Restrictions.eq("c.dv", dv));
        c.add(Restrictions.eq("importe", importe));
        c.add(Restrictions.eq("fechaVto", fechaVto));

        try {
            return (Boleta) c.list().get(0);
        } catch (IndexOutOfBoundsException iobe) {
            throw new EntityNotFoundException();
        }
    }

    public List<Boleta> findBoletas(Long idCuenta, Periodo periodo) {
        LOG.debug("Buscando Boletas Cuenta {} - {}", idCuenta, periodo);

        Criteria c = getSessionFactory().getCurrentSession().createCriteria(Boleta.class);
        c.createAlias("cuenta", "c");
        c.add(Restrictions.eq("c.id", idCuenta));
        if (periodo != null) {
            c.add(Restrictions.eq("cuota", periodo.getCuota()));
            c.add(Restrictions.eq("anio", periodo.getAnio()));
        }

        return c.list();
    }

    public List<Boleta> findBoletas(Long idCuenta, Integer anio, Integer cuota, Boolean sinPago) {
        LOG.debug("Buscando Boletas Cuenta {} - Anio {} - Cuota {} - No Pagas {}", idCuenta, anio, cuota, sinPago);

        Criteria c = getSessionFactory().getCurrentSession().createCriteria(Boleta.class);
        c.createAlias("cuenta", "c");
        c.add(Restrictions.eq("c.id", idCuenta));
        if (anio != null) {
            c.add(Restrictions.eq("anio", anio));
        }
        if (cuota != null) {
            c.add(Restrictions.eq("cuota", cuota));
        }
        if (sinPago != null && sinPago) {
            c.add(Restrictions.isNotNull("pago"));
        }

        return c.list();
    }

    public List<Cuenta> hqlsample() {
        String hql = "select c from Cuenta c where c.activa = 'true' and not exists (select 1 from Boleta b where b.cuenta = c and b.anio = :anio and b.cuota = :cuota)";
        Query q = getSessionFactory().getCurrentSession().createQuery(hql);
        q.setParameter("anio", 2018);
        q.setParameter("cuota", 2);
        return q.list();
    }

    public List<TemplateCuota> findTemplateCuota() {
        Criteria criteria = currentSession().createCriteria(TemplateCuota.class);
        criteria.addOrder(Order.desc("vigenteHasta"));

        try {
            return criteria.list();
        } catch (IndexOutOfBoundsException iobe) {
            throw new EntityNotFoundException();
        }
    }

}