package ar.com.jass.catastro.web.action;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;

import ar.com.jass.catastro.business.BusinessHelper;
import ar.com.jass.catastro.business.CatastroService;
import ar.com.jass.catastro.business.ConsultaService;
import ar.com.jass.catastro.model.Circunscripcion;
import ar.com.jass.catastro.model.Cuenta;
import ar.com.jass.catastro.model.Nomenclatura;
import ar.com.jass.catastro.model.Parcela;
import ar.com.jass.catastro.model.ResponsableCuenta;
import ar.com.jass.catastro.model.Seccion;
import ar.com.jass.catastro.model.TitularParcela;
import ar.com.jass.catastro.model.Tramite;
import ar.com.jass.catastro.model.error.ErrorLog;
import ar.com.jass.catastro.util.HibernateHelper;
import ar.com.jass.catastro.web.commons.WebConstants;

/**
 * Consulta de cuentas/parcelas
 * 
 * @author msquarini
 *
 */
public class ConsultaAction extends GridBaseAction<Parcela> {

	private static final long serialVersionUID = 7353477345330099548L;

	private static final Logger LOG = LoggerFactory.getLogger("ConsultaAction");

	private static final String GRID_DATA = "parcelas";

	/**
	 * Indica si la busqueda es por Cuenta o Nomenclatura
	 */
	private Integer searchBy;

	/**
	 * Busqueda x nro de cuenta y DV
	 */
	private String cuenta;
	private String dv;

	/**
	 * Cuenta ID para la busqueda rapida
	 */
	private Long cuentaId;

	/**
	 * Busqueda x nomenclatura
	 */
	private Nomenclatura nomenclatura;

	@Autowired
	private BusinessHelper businessHelper;

	private List<Circunscripcion> circunscripciones;
	private List<Seccion> secciones;

	private List<Cuenta> cuentas;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.opensymphony.xwork2.ActionSupport#input()
	 */
	public String input() {
		cuenta = "";
		dv = "";

		return "input";
	}

	public String cargaCircunscripciones() {
		// circunscripciones =
		// (List<Circunscripcion>)consultaService.findAll(Circunscripcion.class);
		circunscripciones = businessHelper.getCircunscripciones();

		for (Circunscripcion c : circunscripciones) {
			if (c.getCircunscripcion().equals(getNomenclatura().getCircunscripcion())) {
				secciones = c.getSecciones();
				break;
			}
		}
		return "cargaCircunscripciones";
	}

	/**
	 * Busqueda generica de cuenta o parcela con informacion de cuenta corriente y
	 * tramites
	 * 
	 * @return
	 */
	public String genericSearch() {
		try {
			Cuenta c = getConsultaService().findCuenta(cuenta, null, "parcelas.titulares.titular",
					"parcelas.domicilioInmueble", "parcelas.lineas", "responsables", "tramite.cuentasOrigen");
			
			// Busqueda de tramites donde partipa la cuenta
			List<Tramite> tramites = getConsultaService().findTramitesByCuenta(c, "cuentasOrigen");

			setInSession(WebConstants.CUENTA, c);
			setInSession(WebConstants.CUENTA_CON_DEUDA, getCatastroService().presentaDeuda(c));
			setInSession(WebConstants.TITULARES_ADD, new HashSet<TitularParcela>(c.getParcela().getTitulares()));
			setInSession(WebConstants.TRAMITES_DATA, tramites);
			setInSession(WebConstants.LINEAS_DATA, c.getParcela().getLineas());
			setInSession(WebConstants.RESPONSABLES_ADD, new HashSet<ResponsableCuenta>(c.getResponsables()));

		} catch (Exception e) {
			ErrorLog log = crearErrorLog("genericSearch", e.getMessage());
			LOG.error("{}", log, e);
			setTituloError("Busqueda de cuenta");
			setMensajeError("No se encontro cuenta Nro.: " + cuenta);
			return "error_pop_up";
		}
		return "info-cuenta";
	}

	/**
	 * Busqueda de cuentas para el autocomplete
	 * 
	 * @return
	 */
	public String autocomplete() {
		cuentas = getConsultaService().findCuentasLike(cuenta);

		return "jsonAutocomplete";
	}

	/**
	 * Consulta de cuenta/parcela por nomenclatura o cuenta&dv
	 * 
	 * @return
	 */
	public String search() {
		LOG.info("cuenta {} / {}, Nomenclatura {}", getCuenta(), getDv(), getNomenclatura());
		List<Parcela> parcelas = new ArrayList<Parcela>();
		try {
			// Valida datos

			// Consulta de parcelas segun el criterio de busqueda
			if (searchBy.equals(1)) {
				Cuenta c = getCatastroService().findCuenta(cuenta, dv, "parcelas");
				parcelas.add(c.getParcela());
			} else {
				parcelas = getConsultaService().findParcelas(getNomenclatura());
			}
			setInSession(GRID_DATA, parcelas);

			return "grid";
		} catch (JpaObjectRetrievalFailureException e) {
			setInSession(GRID_DATA, parcelas);
			LOG.warn("Datos no encontrados en busqueda de cuentas/parcelas", e.getMessage());
			return "grid";
		} catch (Exception e) {
			ErrorLog log = crearErrorLog("search", e.getMessage());
			LOG.error("Error en busqueda de cuentas/parcelas, {}", log, e);
			addActionError("Error en proceso de búsqueda, intente nuevamente.");
			return "errorJSON";
		}
	}

	/**
	 * Llamada AJAX para obtener la informacion para la grilla en formato JSON
	 * 
	 * @return
	 */

	@SuppressWarnings("unchecked")
	public String getData() {
		LOG.info("getData");
		try {
			List<Parcela> data = (List<Parcela>) getFromSession(GRID_DATA);
			setData(data);
		} catch (Exception e) {
			LOG.error("error en getData parcelas", e);
		}
		return "dataJSON";
	}

	public String getCuenta() {
		return cuenta;
	}

	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}

	public String getDv() {
		return dv;
	}

	public void setDv(String dv) {
		this.dv = dv;
	}

	public Nomenclatura getNomenclatura() {
		return nomenclatura;
	}

	public void setNomenclatura(Nomenclatura nomenclatura) {
		this.nomenclatura = nomenclatura;
	}

	public Integer getSearchBy() {
		return searchBy;
	}

	public void setSearchBy(Integer searchBy) {
		this.searchBy = searchBy;
	}

	public void setCircunscripciones(List<Circunscripcion> circunscripciones) {
		this.circunscripciones = circunscripciones;
	}

	public List<Circunscripcion> getCircunscripciones() {
		return circunscripciones;
	}

	public List<Seccion> getSecciones() {
		return secciones;
	}

	public void setSecciones(List<Seccion> secciones) {
		this.secciones = secciones;
	}

	public List<Cuenta> getCuentas() {
		return cuentas;
	}

	public void setCuentas(List<Cuenta> cuentas) {
		this.cuentas = cuentas;
	}

	public Long getCuentaId() {
		return cuentaId;
	}

	public void setCuentaId(Long cuentaId) {
		this.cuentaId = cuentaId;
	}
}
