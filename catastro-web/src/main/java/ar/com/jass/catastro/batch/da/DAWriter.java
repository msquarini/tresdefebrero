package ar.com.jass.catastro.batch.da;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

import ar.com.jass.catastro.dao.GeneralDAO;
import ar.com.jass.catastro.model.Cuenta;
import ar.com.jass.catastro.model.da.DADetail;
import ar.com.jass.catastro.model.da.DANovedad;
import ar.com.jass.catastro.model.da.DebitoAutomatico;

/**
 * Persiste registro de novedades de DA de las entidades
 * 
 * En caso de alta se agrega el registro En caso de modificacion y baja, se
 * actualiza el registro existente
 * 
 * @author msquarini
 *
 */
public class DAWriter implements ItemWriter<DADetail> {

	private static final Logger LOG = LoggerFactory.getLogger("DAWriter");

	@Autowired
	private GeneralDAO generalDAO;

	private Integer errores = 0;

	private List<DADetail> fallidos = new ArrayList<DADetail>();

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.batch.item.ItemWriter#write(java.util.List)
	 */
	@Override
	public void write(List<? extends DADetail> items) throws Exception {

		DebitoAutomatico da;
		for (DADetail dad : items) {
			LOG.info("Persistiendo novedad de DA {}", dad);

			if (dad.getNovedad().equals(DANovedad.ALTA)) {
				
				da = new DebitoAutomatico(dad);
				Cuenta c = generalDAO.findCuenta(dad.getCuenta(), dad.getDv());

				if (c != null) {
					LOG.info("Persistiendo novedad ALTA DA, {}", dad);
					da.setCuenta(c);
					generalDAO.save(da);
				} else {
					LOG.error("Error procesando novedad ALTA DA, {}", dad);
					errores++;
					fallidos.add(dad);
				}
			} else {
				da = generalDAO.findDA(dad.getCuenta(), dad.getDv());
				da.setNovedad(dad.getNovedad());
				da.setFecha(dad.getFecha());

				/*
				 * Si coincide el banco informante se aplica el cambio
				 */
				if (da.getBanco().equals(dad.getBanco())) {
					LOG.info("Persistiendo novedad DA, {}", dad);
					generalDAO.save(da);
				} else {
					LOG.error("Error procesando novedad DA, {}", dad);
					errores++;
					fallidos.add(dad);
				}
			}
		}
	}

	public GeneralDAO getGeneralDAO() {
		return generalDAO;
	}

	public void setGeneralDAO(GeneralDAO generalDAO) {
		this.generalDAO = generalDAO;
	}
}
