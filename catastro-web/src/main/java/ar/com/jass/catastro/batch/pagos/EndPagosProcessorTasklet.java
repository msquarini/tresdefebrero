package ar.com.jass.catastro.batch.pagos;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;

import ar.com.jass.catastro.batch.HelperTasklet;
import ar.com.jass.catastro.dao.GeneralDAO;
import ar.com.jass.catastro.model.pagos.EstadoFilePagos;
import ar.com.jass.catastro.model.pagos.FilePago;

/**
 * Finaliza el procesamiento de archivos de pagos si el estado es PROCENSANDO lo cambia a ERROR ya
 * que es un estado inconsistente parametro
 * 
 * @author msquarini
 *
 */
public class EndPagosProcessorTasklet extends HelperTasklet implements Tasklet {

    private static final Logger LOG = LoggerFactory.getLogger("EndPagosProcessorTasklet");
    
    @Autowired
    private GeneralDAO generalDAO;

    @Override
    public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) throws Exception {
        FilePago fp = (FilePago) getExecutionParameter(chunkContext, "filepago");

        if (fp.getEstado().equals(EstadoFilePagos.PROCESANDO)) {
            fp.setEstado(EstadoFilePagos.ERROR);
            generalDAO.update(fp);
        }

        return RepeatStatus.FINISHED;
    }

}
