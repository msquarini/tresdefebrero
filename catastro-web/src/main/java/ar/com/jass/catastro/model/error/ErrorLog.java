package ar.com.jass.catastro.model.error;

import java.util.Date;
import java.util.List;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;

import com.ibm.icu.util.Calendar;

import ar.com.jass.catastro.model.Domicilio;
import ar.com.jass.catastro.model.TitularParcela;

/**
 * Titular o Responsable de una parcela
 * 
 * Junto con TitularParcela se representa la titularidad en ARBA de la parcela
 * 
 * La relacion con cuenta de Responsable es a fines de pago de deuda, solo con fecha desde y hasta
 * 
 * @author MESquarini
 * 
 */

@Entity
@Table(name = "error_log")
public class ErrorLog {

    /**
     * Identificador unico de titular
     */
    private Long id;

    private String operacion;
    private String accion;
    private String mensaje;
    private String usuario;
    private Date fecha;

    public ErrorLog() {
    }

    public ErrorLog(String operacion, String accion, String mensaje, String usuario) {
        fecha = Calendar.getInstance().getTime();
        this.operacion = operacion.substring(0, Math.min(100, operacion.length()));
        this.accion = accion.substring(0, Math.min(100, accion.length()));
        if (mensaje != null) {
            this.mensaje = mensaje.substring(0, Math.min(250, mensaje.length()));
        }
        this.usuario = usuario;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_errorlog_gen")
    @SequenceGenerator(name = "seq_errorlog_gen", sequenceName = "seq_errorlog", allocationSize = 1)
    @Column(name = "id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "operacion")
    public String getOperacion() {
        return operacion;
    }

    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    @Column(name = "accion")
    public String getAccion() {
        return accion;
    }

    public void setAccion(String accion) {
        this.accion = accion;
    }

    @Column(name = "mensaje")
    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    @Column(name = "usuario")
    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    @Column(name = "created_date")
    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @Override
    public String toString() {
        return "ErrorLog [id=" + id + ", operacion=" + operacion + ", accion=" + accion + ", mensaje=" + mensaje
                + ", usuario=" + usuario + ", fecha=" + fecha + "]";
    }

}