package ar.com.jass.catastro.web.converter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Map;

import org.apache.struts2.util.StrutsTypeConverter;

import com.opensymphony.xwork2.conversion.TypeConversionException;

/**
 * Custom Date converter
 * 
 * @author mesquarini
 *
 */
public class DateConverter extends StrutsTypeConverter {

	private static final String DATE_PATTERN = "dd/MM/yyyyy";
	private static final String DATE_TIME_PATTERN = "dd/MM/yyyyy HH:mm:ss";

	@Override
	public Object convertFromString(Map context, String[] values, Class toClass) {
		DateFormat format = new SimpleDateFormat(DATE_PATTERN);
		format.setLenient(false);
		try {
			return format.parse(values[0]);
		} catch (Exception e) {
			throw new TypeConversionException(e.getMessage());
		}
	}

	@Override
	public String convertToString(Map context, Object o) {
		DateFormat format = new SimpleDateFormat(DATE_TIME_PATTERN);
		format.setLenient(false);
		try {
			return format.format(o);
		} catch (Exception e) {
			throw new TypeConversionException(e.getMessage());
		}
	}

}