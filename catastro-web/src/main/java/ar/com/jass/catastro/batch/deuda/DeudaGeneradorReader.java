package ar.com.jass.catastro.batch.deuda;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemReader;

import ar.com.jass.catastro.model.cuentacorriente.GeneracionDeudaDTO;

/**
 * Lectura de registros de cuentas que no presentan deuda generada para el periodo
 * 
 * @author msquarini
 *
 */
public class DeudaGeneradorReader implements ItemReader<GeneracionDeudaDTO> {

    private static final Logger LOG = LoggerFactory.getLogger(DeudaGeneradorReader.class);

    private Iterator<GeneracionDeudaDTO> iterator;

    @BeforeStep
    public void retrieveInterstepData(StepExecution context) {
        JobExecution jobExecution = context.getJobExecution();
        ExecutionContext jobContext = jobExecution.getExecutionContext();
        
        // Id del periodo que ejecuta el JOB
        Long idPeriodo = jobExecution.getJobParameters().getLong("idPeriodo");
        // Obtener la lista de cuentas para el periodo que procesa el JOB
        List<GeneracionDeudaDTO> lista = (List<GeneracionDeudaDTO>) jobContext.get("dtos_"+idPeriodo);
        iterator = lista.iterator();
        LOG.info("BS-Reader {} {}", idPeriodo, lista.size());
    }

    public GeneracionDeudaDTO read() throws Exception {
        
        try {      
            //int i = 1/0;
            return iterator.next();
        } catch (NoSuchElementException e) {
            LOG.warn("No mas cuentas para generacion de deuda.");
            return null;
        }
    }
}