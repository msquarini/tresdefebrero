package ar.com.jass.catastro.business.impl;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import ar.com.jass.catastro.business.CatastroService;
import ar.com.jass.catastro.business.CuentaCorrienteService;
import ar.com.jass.catastro.business.RatificacionService;
import ar.com.jass.catastro.dao.CuentaCorrienteDAO;
import ar.com.jass.catastro.dao.GeneralDAO;
import ar.com.jass.catastro.dao.GeneralDAORatificacion;
import ar.com.jass.catastro.model.Cuenta;
import ar.com.jass.catastro.model.CuentaRatificacion;
import ar.com.jass.catastro.model.EstadoTramite;
import ar.com.jass.catastro.model.Nomenclatura;
import ar.com.jass.catastro.model.Parcela;
import ar.com.jass.catastro.model.ParcelaRatificacion;
import ar.com.jass.catastro.model.ResponsableCuenta;
import ar.com.jass.catastro.model.ResponsableCuentaRatificacion;
import ar.com.jass.catastro.model.TipoTramite;
import ar.com.jass.catastro.model.TitularParcelaRatificacion;
import ar.com.jass.catastro.model.TitularRatificacion;
import ar.com.jass.catastro.model.Tramite;
import ar.com.jass.catastro.model.cuentacorriente.CuentaCorriente;
import ar.com.jass.catastro.util.DateHelper;
import ar.com.jass.catastro.util.HibernateHelper;

@Service
public class RatificacionServiceImpl implements RatificacionService {

    private static final Logger LOG = LoggerFactory.getLogger(RatificacionServiceImpl.class);

    @Autowired
    private CatastroService catastroService;

    @Autowired
    private SessionFactory sessionFactory;

    @Autowired
    @Qualifier("generalDAORatificacion")
    private GeneralDAORatificacion generalDAORatificacion;

    @Autowired
    @Qualifier("ccDAO")
    private CuentaCorrienteDAO cuentaCorrienteDAO;

    @Autowired
    private CuentaCorrienteService ccService;

    @Autowired
    private DeudaGeneratorStrategy deudaGenerator;

    @Autowired
    @Qualifier("generalDAO")
    private GeneralDAO generalDAO;

    @Transactional
    public Tramite crearTramiteRatificacion(Cuenta cuentaOrigen) throws Exception {

        List<Cuenta> cuentasOrigen = new ArrayList<Cuenta>();

        List<Parcela> parcelasPh = findParcelasPH(cuentaOrigen.getParcela().getNomenclatura());

        for (Parcela p : parcelasPh)
            cuentasOrigen.add(p.getCuenta());

        Tramite tramite = new Tramite(TipoTramite.RAT, new ArrayList<Cuenta>(), cuentasOrigen);

        // tramite.setNumero(obtenerNumeroTramite());

        tramite = catastroService.grabarTramite(tramite, new ArrayList<Parcela>(), null, null, Boolean.FALSE);

        /*
         * Se genero el tramite entonces cargar la info en las tablas temporales de ratificacion
         */

        loadRatificacionInTables(parcelasPh, tramite);

        return tramite;

    }

    @Transactional
    public Tramite procesarTramiteRatificacion(Tramite tramite) throws Exception {

        LOG.info("Procesando Ratificacion: {}", tramite);

        // Cargar nuevas parcelas ratificacion
        List<CuentaRatificacion> cuentasRatificacion = findCuentasRatificacionByTramite(tramite);
        List<CuentaRatificacion> toDelete = new ArrayList<CuentaRatificacion>();
        Map<Parcela, List<CuentaRatificacion>> toSubdividir = new HashMap<Parcela, List<CuentaRatificacion>>();
        List<CuentaRatificacion> toUnificar = new ArrayList<CuentaRatificacion>();

        // Recorrer las cuentas separando las cuentas a eliminar, sub y unificar
        for (CuentaRatificacion cr : cuentasRatificacion) {
            if (cr.getParcela().getDeleted()) {
                toDelete.add(cr);
                continue;
            }

            if (cr.getParcela().getNewuf()) {
                // Subdivision, agrupamos todas las nuevas parcelas con la parcela origen
                if (cr.getParcela().getParcelasOrigen().size() == 1) {
                    Parcela parcelaOrigen = cr.getParcela().getParcelasOrigen().get(0);
                    List<CuentaRatificacion> cuentasSubdivision = toSubdividir.get(parcelaOrigen);
                    if (cuentasSubdivision == null) {
                        cuentasSubdivision = new ArrayList<CuentaRatificacion>();
                        toSubdividir.put(parcelaOrigen, cuentasSubdivision);
                    }
                    cuentasSubdivision.add(cr);

                    continue;
                }

                // Unificacion
                if (cr.getParcela().getParcelasOrigen().size() > 1) {
                    toUnificar.add(cr);
                }
            }
        }
        LOG.info("{}, eliminar {}, subdividir {}, unificar {}", tramite, toDelete.size(), toSubdividir.size(),
                toUnificar.size());

        procesarCuentasAEliminar(toDelete);
        procesarSubdivisiones(tramite, toSubdividir);
        procesarUnificaciones(tramite, toUnificar);

        /**
         * Desactivamos cuentas origen - Cambiamos estado tramite
         */
        List<Cuenta> cuentasOrigen = tramite.getCuentasOrigen();
        for (Cuenta c : cuentasOrigen) {
            c.setBloqueada(Boolean.FALSE);
        }
        tramite.setEstado(EstadoTramite.FINALIZADO);

        // Peristir cambios
        generalDAO.save(tramite);

        return tramite;
    }

    /**
     * Marcas las cuentas originales como eliminadas
     * 
     * @param cuentas
     */
    private void procesarCuentasAEliminar(List<CuentaRatificacion> cuentas) {
        for (CuentaRatificacion cr : cuentas) {
            LOG.debug("Eliminando cuenta, {}", cr);
            cr.getParcela().getOrigen().getCuenta().desactivar();
            generalDAO.save(cr.getParcela().getOrigen().getCuenta());
        }
    }

    /**
     * Crear las cuentas para cada subdivision y realizar las distribucion de la CC entre las nuevas
     * cuentas
     * 
     * @param cuentas
     */
    private void procesarSubdivisiones(Tramite tramite, Map<Parcela, List<CuentaRatificacion>> cuentas) {
        // TODO fecha de vencimientos de los ajustes CC por el procesamiento de tramite (si es oficio o no)
        Date fechaVencimiento = null;
        Integer codigoAdministrativo = 0;

        Integer anio = DateHelper.getAnioFecha(tramite.getVigencia());
        Integer cuota = DateHelper.getMesFecha(tramite.getVigencia());

        for (Entry<Parcela, List<CuentaRatificacion>> subdivision : cuentas.entrySet()) {
            LOG.debug("Ratificacion - Subdivision cuenta, {} en: {} , anio-mes vigencia: {}-{}",
                    subdivision.getKey().getCuenta(), subdivision.getValue().size(), anio, cuota);

            List<Parcela> ufs = new ArrayList<Parcela>();

            for (CuentaRatificacion cr : subdivision.getValue()) {
                Cuenta nuevaCuenta = crearCuenta(cr);

                // Lista de nuevas parcelas para el recalculo de cuentacorriente
                ufs.add(nuevaCuenta.getParcela());

                // Agregamos al tramite la nueva cuenta generada
                tramite.getCuentas().add(nuevaCuenta);
                nuevaCuenta.setTramite(tramite);
            }

            // Realizamos el ajuste de cuenta corriente de cada subdivision en las nuevas cuentas.
            List<CuentaCorriente> movimientosCuentaOrigen = cuentaCorrienteDAO
                    .findMovimientos(subdivision.getKey().getCuenta().getId(), anio, cuota, null, null, null, null);

            List<CuentaCorriente> ajustes = deudaGenerator.generarDeudaRetroactivaSubdivision(movimientosCuentaOrigen,
                    ufs, fechaVencimiento, codigoAdministrativo);
            if (ajustes.size() > 0) {
                generalDAO.saveList(ajustes);
            }
        }
    }

    private void procesarUnificaciones(Tramite tramite, List<CuentaRatificacion> cuentas) {
        // TODO fecha de vencimientos de los ajustes CC por el procesamiento de tramite
        Date fechaVencimiento = null;
        Integer codigoAdministrativo = 100;

        Integer anio = DateHelper.getAnioFecha(tramite.getVigencia());
        Integer cuota = DateHelper.getMesFecha(tramite.getVigencia());

        // Procesamos para cada unificacion, creacion de cuenta y ajuste de cuentacorriente
        for (CuentaRatificacion cr : cuentas) {
            Cuenta nuevaCuenta = crearCuenta(cr);

            // Agregamos al tramite la nueva cuenta generada
            tramite.getCuentas().add(nuevaCuenta);
            nuevaCuenta.setTramite(tramite);

            // Armamos la lista de las cuentas origen de la unificacion
            List<Cuenta> cuentasOrigen = new ArrayList<Cuenta>();
            for (Parcela p : cr.getParcela().getParcelasOrigen()) {
                cuentasOrigen.add(p.getCuenta());
            }
            
            // Realizamos el ajuste de cuenta corriente de la unificacion
            List<CuentaCorriente> ajustes = deudaGenerator.generarDeudaRetroactivaUnificacion(anio, cuota, nuevaCuenta,
                    cuentasOrigen, fechaVencimiento, codigoAdministrativo);
            if (ajustes.size() > 0) {
                generalDAO.saveList(ajustes);
            }
        }

    }

    /**
     * Creacion de las entidades del dominio a partir de ratificacion
     * 
     * @param cuentaRatificacion
     * @return
     */
    private Cuenta crearCuenta(CuentaRatificacion cuentaRatificacion) {
        // Creacion de la parcela
        Parcela nuevaParcela = cuentaRatificacion.getParcela().getParcela();

        // Creacion de la cuenta
        BigInteger numeroCuenta = generalDAO.obtenerNumeroCuenta();
        Cuenta cuenta = new Cuenta(numeroCuenta, nuevaParcela);
        cuenta.setActiva(Boolean.TRUE);
        cuenta.setBloqueada(Boolean.FALSE);

        List<ResponsableCuenta> responsables = cuentaRatificacion.getResponsablesCuenta();
        for (ResponsableCuenta rc : responsables) {
            rc.setCuenta(cuenta);
        }
        cuenta.setResponsables(responsables);
        cuenta.setTramite(cuentaRatificacion.getTramite());
        return cuenta;
    }

    @Transactional
    private void loadRatificacionInTables(List<Parcela> parcelasPh, Tramite tramite) {
        for (Parcela p : parcelasPh) {
            HibernateHelper.initialize(p, "lineas");
            ParcelaRatificacion pr = new ParcelaRatificacion(p);
            pr.getCuenta().setTramite(tramite);
            generalDAO.save(pr);
        }

    }

    @Override
    @Transactional
    public List<ParcelaRatificacion> findParcelasRatificacion(Nomenclatura nomenclatura, String... initialize) {
        List<ParcelaRatificacion> parcelas = generalDAORatificacion.findParcelasRatificacion(nomenclatura);

        HibernateHelper.initialize(parcelas, initialize);

        return parcelas;
    }

    @Override
    @Transactional
    public List<ParcelaRatificacion> findParcelasRatificacionRemove(Nomenclatura nomenclatura, String... initialize) {
        List<ParcelaRatificacion> parcelas = generalDAORatificacion.findParcelasRatificacionRemove(nomenclatura);

        HibernateHelper.initialize(parcelas, initialize);

        return parcelas;
    }

    @Override
    @Transactional
    public List<ParcelaRatificacion> findParcelasRatificacionNew(Nomenclatura nomenclatura, String... initialize) {
        List<ParcelaRatificacion> parcelas = generalDAORatificacion.findParcelasRatificacionNew(nomenclatura);

        HibernateHelper.initialize(parcelas, initialize);

        return parcelas;
    }

    @Override
    @Transactional
    public List<Parcela> findParcelasPH(Nomenclatura nomenclatura, String... initialize) {
        List<Parcela> parcelas = generalDAORatificacion.findParcelas(nomenclatura);

        HibernateHelper.initialize(parcelas, initialize);

        return parcelas;
    }

    @Override
    @Transactional
    public ParcelaRatificacion findParcelaRatificacionById(Long id, String... initialize) {
        ParcelaRatificacion p = generalDAO.find(ParcelaRatificacion.class, id);
        HibernateHelper.initialize(p, initialize);
        return p;
    }

    @Override
    @Transactional
    public CuentaRatificacion findCuentaRatificacionById(Long id, String... initialize) {
        CuentaRatificacion p = generalDAO.find(CuentaRatificacion.class, id);
        HibernateHelper.initialize(p, initialize);
        return p;
    }

    @Override
    @Transactional
    public void saveParcelaRatificacion(ParcelaRatificacion parcelaRatificacion) {
        generalDAO.save(parcelaRatificacion);

    }

    @Override
    @Transactional
    public void saveCuentaRatificacion(CuentaRatificacion cuentaRatificacion) {
        generalDAO.save(cuentaRatificacion);

    }

    @Override
    @Transactional
    public CuentaRatificacion findCuentaRatificacionByCuenta(String cuenta, String... initialize) {
        CuentaRatificacion cuentaRatificacion = generalDAORatificacion.findCuentaRatificacionByCuenta(cuenta);
        HibernateHelper.initialize(cuentaRatificacion, initialize);
        return cuentaRatificacion;
    }

    @Override
    @Transactional
    public TitularRatificacion findTitularRatificacionById(Long idTitular, String... initialize) {
        TitularRatificacion p = generalDAO.find(TitularRatificacion.class, idTitular);
        HibernateHelper.initialize(p, initialize);
        return p;
    }

    @Override
    @Transactional
    public List<TitularRatificacion> findTitulares(TitularRatificacion titular) {
        return generalDAORatificacion.findTitulares(titular);
    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    @Transactional
    public void updateParcelaRatificacion(Long id_parcela_origen, Long id_cuenta_origen,
            ParcelaRatificacion parcelaRatificacion, List<TitularParcelaRatificacion> titularesRatificacion,
            List<ResponsableCuentaRatificacion> responsablesRatificacion, List<Parcela> parcelasOrigen) {

        CuentaRatificacion cuenta = generalDAO.find(CuentaRatificacion.class, id_cuenta_origen);

        ParcelaRatificacion parcelaToSave = generalDAO.find(ParcelaRatificacion.class, id_parcela_origen);

        parcelaToSave.setObservacion(parcelaRatificacion.getObservacion());

        // parcelaToSave.setDomicilioInmueble(parcelaRatificacion.getDomicilioInmueble());

        parcelaToSave.getDomicilioInmueble().setBarrio(parcelaRatificacion.getDomicilioInmueble().getBarrio());
        parcelaToSave.getDomicilioInmueble().setCalle(parcelaRatificacion.getDomicilioInmueble().getCalle());
        parcelaToSave.getDomicilioInmueble()
                .setCodigoPostal(parcelaRatificacion.getDomicilioInmueble().getCodigoPostal());
        parcelaToSave.getDomicilioInmueble()
                .setDepartamento(parcelaRatificacion.getDomicilioInmueble().getDepartamento());
        parcelaToSave.getDomicilioInmueble().setNumero(parcelaRatificacion.getDomicilioInmueble().getNumero());
        parcelaToSave.getDomicilioInmueble().setPiso(parcelaRatificacion.getDomicilioInmueble().getPiso());

        parcelaToSave.setDominio(parcelaRatificacion.getDominio());
        parcelaToSave.setDominioMunicipal(parcelaRatificacion.getDominioMunicipal());

        // parcelaToSave.setLineaVigente(parcelaRatificacion.getLineaVigente());

        parcelaToSave.getLineaVigente()
                .setSuperficieCubierta(parcelaRatificacion.getLineaVigente().getSuperficieCubierta());
        parcelaToSave.getLineaVigente().setSuperficieSemi(parcelaRatificacion.getLineaVigente().getSuperficieSemi());
        parcelaToSave.getLineaVigente()
                .setSuperficieTerreno(parcelaRatificacion.getLineaVigente().getSuperficieTerreno());
        parcelaToSave.getLineaVigente().setValorComun(parcelaRatificacion.getLineaVigente().getValorComun());
        parcelaToSave.getLineaVigente().setValorEdificio(parcelaRatificacion.getLineaVigente().getValorEdificio());
        ;
        parcelaToSave.getLineaVigente().setValorTierra(parcelaRatificacion.getLineaVigente().getValorTierra());

        parcelaToSave.setPartida(parcelaRatificacion.getPartida());

        // Quitar titulares actuales
        parcelaToSave.getTitulares().clear();

        // Quitar responsables actuales
        cuenta.getResponsables().clear();

        // Cargamos los nuevos titulares
        cargarTitulares(parcelaToSave, titularesRatificacion);

        // Cargamos los nuevos
        cargarResponsables(cuenta, responsablesRatificacion);

        parcelaToSave.setCuenta(cuenta);

        // Actualizo lista de unidades funcionales origen

        parcelaToSave.setParcelasOrigen(parcelasOrigen);

        generalDAO.merge(parcelaToSave);

    }

    @Override
    @Transactional
    public void saveParcelaRatificacion(ParcelaRatificacion parcelaRatificacion,
            List<TitularParcelaRatificacion> titularesRatificacion,
            List<ResponsableCuentaRatificacion> responsablesRatificacion, List<Parcela> parcelasOrigen,
            Tramite tramite) {

        CuentaRatificacion cuenta = new CuentaRatificacion();

        ParcelaRatificacion parcelaToSave = new ParcelaRatificacion();

        parcelaToSave.setNomenclatura(parcelaRatificacion.getNomenclatura());

        parcelaToSave.setObservacion(parcelaRatificacion.getObservacion());
        parcelaToSave.setDomicilioInmueble(parcelaRatificacion.getDomicilioInmueble());
        parcelaToSave.setDominio(parcelaRatificacion.getDominio());
        parcelaToSave.setDominioMunicipal(parcelaRatificacion.getDominioMunicipal());
        parcelaToSave.setLineaVigente(parcelaRatificacion.getLineaVigente());
        parcelaToSave.setPartida(parcelaRatificacion.getPartida());
        parcelaToSave.setNewuf(Boolean.TRUE);
        parcelaToSave.setDeleted(Boolean.FALSE);
        parcelaToSave.setParcelasOrigen(parcelasOrigen);

        // inicializar las listas de titulares y responsables
        parcelaToSave.setTitulares(new ArrayList<TitularParcelaRatificacion>());

        cuenta.setResponsables(new ArrayList<ResponsableCuentaRatificacion>());

        // Cargamos los nuevos titulares
        cargarTitulares(parcelaToSave, titularesRatificacion);

        // Cargamos los nuevos

        cargarResponsables(cuenta, responsablesRatificacion);

        cuenta.setParcelas(new HashSet<ParcelaRatificacion>());
        cuenta.getParcelas().add(parcelaToSave);
        cuenta.setCuenta("");
        cuenta.setDv("");
        cuenta.setActiva(Boolean.TRUE);
        cuenta.setTramite(tramite);

        parcelaToSave.setCuenta(cuenta);

        generalDAO.save(parcelaToSave);

    }

    @Override
    @Transactional
    public void updateParcelaRatificacion(ParcelaRatificacion parcelaRatificacion) {

        generalDAO.update(parcelaRatificacion);

    }

    @Override
    @Transactional
    public void saveTitulares(CuentaRatificacion origenRatificacion,
            List<TitularParcelaRatificacion> titularesRatificacion,
            List<ResponsableCuentaRatificacion> responsablesRatificacion) {

        CuentaRatificacion cuenta = generalDAO.find(CuentaRatificacion.class, origenRatificacion.getId());

        // Inicializar titulares actuales
        cuenta.getParcela()
                .setTitulares((List<TitularParcelaRatificacion>) new ArrayList<TitularParcelaRatificacion>());

        // Inicializar responsables actuales
        cuenta.setResponsables((List<ResponsableCuentaRatificacion>) new ArrayList<ResponsableCuentaRatificacion>());

        // Cargamos los nuevos titulares
        cargarTitulares(cuenta.getParcela(), titularesRatificacion);

        // Cargamos los nuevos
        cuenta.setResponsables(responsablesRatificacion);

        generalDAO.saveList(cuenta.getParcela().getTitulares());

        generalDAO.saveList(cuenta.getResponsables());

    }

    /*
     * Desde una parcela ratificacion genera una lista de titular parcela teniendo en cuenta que el
     * titular puede ya existir en la tabla principal
     */

    /* En el caso de no existir crea un nuevo objeto por cada titular nuevo */

    // List<TitularParcela> getTitularesFromParcelaRatificacion(Parcela p, ParcelaRatificacion pr)
    // {
    // List<TitularParcela> returnValue = new ArrayList<TitularParcela>();
    //
    // TitularParcela tp;
    // Titular t;
    // for(TitularParcelaRatificacion tpr : pr.getTitulares())
    // {
    // /*Si el titular no existe crearlo*/
    // if(tpr.getTitular().getTitularOriginal()==null)
    // t = new Titular(tpr.getTitular());
    // else
    // t = generalDAO.find(Titular.class, tpr.getTitular().getTitularOriginal().getId());
    //
    // tp = new TitularParcela(p,t,tpr.getPorcentaje(),tpr.getDesde(),tpr.getHasta());
    // returnValue.add(tp);
    // }
    // return returnValue;
    // }

    /*
     * Desde una cuenta ratificacion genera una lista de responsable cuenta teniendo en cuenta que
     * el responsable puede ya existir en la tabla principal
     */

    /* En el caso de no existir crea un nuevo objeto por cada responsable nuevo */

    // List<ResponsableCuenta> getResponsablesCuentaFromCuentaRatificacion(Cuenta c,
    // CuentaRatificacion cr)
    // {
    // List<ResponsableCuenta> returnValue = new ArrayList<ResponsableCuenta>();
    // ResponsableCuenta rc;
    // Titular t;
    // for(ResponsableCuentaRatificacion rcr : cr.getResponsables())
    // {
    // if(rcr.getTitular().getTitularOriginal()==null)
    // t = new Titular(rcr.getTitular());
    // else
    // t = generalDAO.find(Titular.class, rcr.getTitular().getTitularOriginal().getId());
    //
    // rc = new ResponsableCuenta(c,t,rcr.getDesde(),rcr.getHasta());
    // returnValue.add(rc);
    // }
    // return returnValue;
    // }

    @Transactional
    private void cargarTitulares(ParcelaRatificacion parcelaRatificacion,
            List<TitularParcelaRatificacion> titularesRatificacion) {
        if (parcelaRatificacion.getTitulares() != null) {
            parcelaRatificacion.getTitulares().clear();
        }
        for (TitularParcelaRatificacion tp : titularesRatificacion) {
            tp.setParcela(parcelaRatificacion);
            parcelaRatificacion.getTitulares().add(tp);
        }
    }

    @Transactional
    private void cargarResponsables(CuentaRatificacion cuentaRatificacion,
            List<ResponsableCuentaRatificacion> responsablesRatificacion) {
        if (cuentaRatificacion.getResponsables() != null) {
            cuentaRatificacion.getResponsables().clear();
        }
        for (ResponsableCuentaRatificacion rc : responsablesRatificacion) {
            rc.setCuenta(cuentaRatificacion);
            cuentaRatificacion.getResponsables().add(rc);
        }
    }

    /**
     * Carga una lista de titulares en una nueva parcela
     * 
     * @param parcela
     * @param titulares
     */
    // private void cargarTitulares(Parcela parcela, ParcelaRatificacion parcelaRatificacion) {
    //
    // List<TitularParcela> titulares = new ArrayList<TitularParcela>();
    //
    // if (parcela.getTitulares() != null) {
    // parcela.getTitulares().clear();
    // }
    //
    // TitularParcela tp;
    // for (TitularParcelaRatificacion tpr : parcelaRatificacion.getTitulares()) {
    //
    //
    // if(tpr.getTitular().getTitularOriginal()==null)
    // tp = new TitularParcela(parcela,new
    // Titular(tpr.getTitular()),tpr.getPorcentaje(),tpr.getDesde(),tpr.getHasta());
    // else
    // tp = new
    // TitularParcela(parcela,tpr.getTitular().getTitularOriginal(),tpr.getPorcentaje(),tpr.getDesde(),tpr.getHasta());
    //
    // titulares.add(tp);
    // tp.setParcela(parcela);
    // parcela.getTitulares().add(tp);
    // }
    // }

    /**
     * Carga una lista de responsables en una nueva cuenta. Se agrega solo el/los responsables
     * activos (sin fecha de hasta)
     * 
     * @param parcela
     * @param titulares
     */
    // private void cargarResponsables(Cuenta cuenta, CuentaRatificacion cuentaRatificacion) {
    //
    // ResponsableCuenta newResponsable;
    // for (ResponsableCuentaRatificacion rcr : cuentaRatificacion.getResponsables()) {
    // if (rcr.getHasta() == null)
    // {
    // if(rcr.getTitular().getTitularOriginal()==null)
    //
    // newResponsable = new ResponsableCuenta(cuenta, new Titular(rcr.getTitular()), rcr.getDesde(),
    // rcr.getHasta());
    // else
    // newResponsable = new ResponsableCuenta(cuenta, rcr.getTitular().getTitularOriginal(),
    // rcr.getDesde(), rcr.getHasta());
    //
    // cuenta.getResponsables().add(newResponsable);
    // }
    // }
    // }

    @Override
    public List<CuentaRatificacion> findCuentasRatificacionByTramite(Tramite tramite, String... initialize) {
        List<CuentaRatificacion> cuentasRatificacion = generalDAORatificacion.findCuentasRatificacionByTramite(tramite);
        HibernateHelper.initialize(cuentasRatificacion, initialize);
        return cuentasRatificacion;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    private Integer obtenerNumeroTramite() {
        String seqNextValue = "SELECT cast(nextval('catastro.seq_tramite_numero') as integer);";
        Query sql = getSessionFactory().getCurrentSession().createSQLQuery(seqNextValue);
        Integer next = (Integer) sql.uniqueResult();

        return next;
    }

    @Override
    @Transactional
    public boolean existUF(Nomenclatura nomencla) {
        return generalDAORatificacion.exists(nomencla);
    }

}
