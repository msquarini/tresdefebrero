package ar.com.jass.catastro.batch.deuda.file;

import java.util.ArrayList;
import java.util.List;

import ar.com.jass.catastro.model.cuentacorriente.Deuda;

public class CuentaDTO {

    private Long id;
    private String cuenta;
    private String dv;
    private List<Deuda> deudas;

    public CuentaDTO() {
        deudas = new ArrayList<Deuda>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCuenta() {
        return cuenta;
    }

    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }

    public String getDv() {
        return dv;
    }

    public void setDv(String dv) {
        this.dv = dv;
    }

    public List<Deuda> getDeudas() {
        return deudas;
    }

    public void setDeudas(List<Deuda> deudas) {
        this.deudas = deudas;
    }

    public List<DeudaDTO> getDeudasDTO() {
        List<DeudaDTO> dtos = new ArrayList<DeudaDTO>(deudas.size());
        for (Deuda d : deudas) {
            DeudaDTO dto = new DeudaDTO(d);
            dtos.add(dto);
        }
        return dtos;
    }
}
