package ar.com.jass.catastro.model;

import java.util.UUID;

/**
 * Interface para objetos editables y poder identificarlos previo a la
 * persistencia con un UUID
 * 
 * @author mesquarini
 *
 */
public interface Editable {

	public UUID getUuid();
}
