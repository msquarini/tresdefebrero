package ar.com.jass.catastro.model.cuentacorriente;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import ar.com.jass.catastro.model.Cuenta;
import ar.com.jass.catastro.model.pagos.Pago;

/**
 * Interface que representa una deuda (Cuota / ANUAL)
 * 
 * @author msquarini
 *
 */
public interface Deuda {

    /**
     * Crear los registros de cuenta corriente de credito
     * 
     * @param pago
     * @return
     */
    //public List<CuentaCorriente> generarMovimientosCredito(Pago pago);

    /**
     * Devuelve la cuenta correspondiente a la deuda
     * 
     * @return
     */
    public Cuenta getCuenta();

    public BigDecimal getImporte();

    public Date getFechaVto();

    public Integer getCuota();

    public Integer getAnio();
}
