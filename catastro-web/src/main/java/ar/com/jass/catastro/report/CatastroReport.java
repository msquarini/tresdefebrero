package ar.com.jass.catastro.report;

import ar.com.jass.catastro.model.Cuenta;
import ar.com.jass.catastro.web.dto.ReportDTO;

public interface CatastroReport {

    public ReportDTO reporteCuenta(Cuenta cuenta);
}
