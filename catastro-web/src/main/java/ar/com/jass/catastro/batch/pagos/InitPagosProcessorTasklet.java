package ar.com.jass.catastro.batch.pagos;

import java.io.File;
import java.text.SimpleDateFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import ar.com.jass.catastro.batch.HelperTasklet;
import ar.com.jass.catastro.dao.GeneralDAO;
import ar.com.jass.catastro.model.pagos.EstadoFilePagos;
import ar.com.jass.catastro.model.pagos.FilePago;

/**
 * Inicia el procesamiento de archivos de pagos, buscar el registro en la base con el ID pasado como
 * parametro
 * 
 * @author msquarini
 *
 */
public class InitPagosProcessorTasklet extends HelperTasklet implements Tasklet {

    private static final Logger LOG = LoggerFactory.getLogger("InitPagosProcessorTasklet");

    @Value("${pagos.toprocess.path}")
    private String pagosAProcesarPath;

    @Autowired
    private GeneralDAO generalDAO;

    @Override
    public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) throws Exception {
        Long idFilePago = getJobLongParameter(chunkContext, "idFilePago");
        FilePago fp = generalDAO.find(FilePago.class, idFilePago);
        
        LOG.info("Inicio Procesamiento de archivo de pagos, {}" + fp);
         
        String path = pagosAProcesarPath + File.separator + fp.getUniqueName();

        // Validar si se puede procesar el file y actualiza el estado
        if (fp != null && fp.getEstado().puedeProcesar()) {
            fp.setEstado(EstadoFilePagos.PROCESANDO);
            generalDAO.save(fp);
            
            putExecutionParameter(chunkContext, "pathtofile", path);
            putExecutionParameter(chunkContext, "filepago", fp);

        } else {
            LOG.warn("No existe archivo de pagos para procesar con Id {}", idFilePago);
            throw new IllegalStateException("No existe archivo de pagos para procesar.");
        }
        return RepeatStatus.FINISHED;
    }

}
