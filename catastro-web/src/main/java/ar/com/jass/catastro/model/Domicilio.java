package ar.com.jass.catastro.model;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Entidad de domicilio
 * 
 * @author MESquarini
 *
 */
@Entity
@Table(name = "Domicilio")
public class Domicilio extends EntityBase implements Serializable {

    private Long id;
    private String calle;
    private String numero;
    private String piso;
    private String departamento;
    private Barrio barrio;
    private String codigoPostal;

    public Domicilio() {

    }

    /**
     * Crea un nuevo domicilio a partir de otro
     * 
     * @param d
     */
    public Domicilio(Domicilio d) {
        this.calle = d.getCalle();
        this.numero = d.getNumero();
        this.piso = d.getPiso();
        this.departamento = d.getDepartamento();
        this.barrio = d.getBarrio();
        this.codigoPostal = d.getCodigoPostal();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_domicilio_gen")
    @SequenceGenerator(name = "seq_domicilio_gen", sequenceName = "seq_domicilio", allocationSize = 1)
    @Column(name = "id_domicilio")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "calle")
    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    @Column(name = "numero")
    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    @Column(name = "piso")
    public String getPiso() {
        return piso;
    }

    public void setPiso(String piso) {
        this.piso = piso;
    }

    @Column(name = "depto")
    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    @JoinColumn(name = "id_barrio", referencedColumnName = "id_barrio", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    public Barrio getBarrio() {
        return barrio;
    }

    public void setBarrio(Barrio barrio) {
        this.barrio = barrio;
    }

    @Column(name = "codigo_postal")
    public String getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public String toString() {
        return "Domicilio id:" + getId() + " Calle:" + getCalle() + " Numero:" + getNumero() + " Piso:" + getPiso()
                + " Depto:" + getDepartamento() + " CP:" + getCodigoPostal();
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id, this.calle, this.numero, this.codigoPostal);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Domicilio other = (Domicilio) obj;
        return Objects.equals(this.calle, other.calle) && Objects.equals(this.numero, other.numero)
                && Objects.equals(this.codigoPostal, other.codigoPostal) && Objects.equals(this.id, other.id);
    }
}
