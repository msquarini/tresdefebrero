package ar.com.jass.catastro.business;

/**
 * Servicio de tasas por categoria
 * 
 * @author msquarini
 *
 */
public interface TasaService {

    public void reload();

    public Double getTasa(Integer categoria);

    public double getTasaInteresMora();
}
