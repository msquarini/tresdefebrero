package ar.com.jass.catastro.web.action;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import ar.com.jass.catastro.business.CatastroService;
import ar.com.jass.catastro.business.ConsultaService;
import ar.com.jass.catastro.model.Titular;
import ar.com.jass.catastro.model.error.ErrorLog;
import ar.com.jass.catastro.web.commons.WebConstants;

/**
 * Gestion de titulares
 * 
 * @author msquarini
 *
 */
public class TitularAction extends GridBaseAction<Titular> {

	private static final long serialVersionUID = 7353477345330099548L;

	private static final Logger LOG = LoggerFactory.getLogger("TitularAction");

	/**
	 * Indica si la busqueda es por Apellido o CUIT/CUIL o DNI
	 */
	private Integer searchBy;

	/**
	 * Busquedas
	 */
	private Titular searchTitular;
	private String apellido;
	private String cuitcuil;
	private Integer dni;

	/**
	 * Id titular??
	 */
	private Long idTitular;

	/**
	 * Nuevo titular
	 */
	private Titular titular;

	@Autowired
	private ConsultaService consultaService;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.opensymphony.xwork2.ActionSupport#input()
	 */
	public String input() {
		apellido = "";
		cuitcuil = "";

		return "input";
	}

	/**
	 * Consulta de titulares x apellido o cuitcuil
	 * 
	 * @return
	 */
	public String search() {
		LOG.info("apellido {} / cuitcuil {}", searchTitular.getApellido(), searchTitular.getCuitcuil());

		try {
			// Valida datos

			// Consulta de titulares segun el criterio de busqueda
			
			List<Titular> titulares = consultaService.findTitulares(searchTitular);
			setInSession(WebConstants.TITULARES_DATA, titulares);

			return "grid";

		} catch (Exception e) {
			ErrorLog log = crearErrorLog("search", e.getMessage());
			LOG.error("Error en busqueda de titulares, {}", log, e);
			addActionError("Error en proceso de búsqueda, intente nuevamente.");
			return "errorJSON";
		}
	}

	/**
	 * Llamada AJAX para obtener la informacion para la grilla en formato JSON
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public String getData() {
		try {
			List<Titular> data = (List<Titular>) getFromSession(WebConstants.TITULARES_DATA);
			setData(data);
		} catch (Exception e) {
			LOG.error("error en getData titulares", e);
		}
		return "dataJSON";
	}

	public ConsultaService getConsultaService() {
		return consultaService;
	}

	public void setConsultaService(ConsultaService consultaService) {
		this.consultaService = consultaService;
	}

	public Integer getSearchBy() {
		return searchBy;
	}

	public void setSearchBy(Integer searchBy) {
		this.searchBy = searchBy;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getCuitcuil() {
		return cuitcuil;
	}

	public void setCuitcuil(String cuitcuil) {
		this.cuitcuil = cuitcuil;
	}

	public Long getIdTitular() {
		return idTitular;
	}

	public void setIdTitular(Long idTitular) {
		this.idTitular = idTitular;
	}

	public Integer getDni() {
		return dni;
	}

	public void setDni(Integer dni) {
		this.dni = dni;
	}

	public Titular getSearchTitular() {
		return searchTitular;
	}

	public void setSearchTitular(Titular searchTitular) {
		this.searchTitular = searchTitular;
	}

	public Titular getTitular() {
		return titular;
	}

	public void setTitular(Titular titular) {
		this.titular = titular;
	}

}
