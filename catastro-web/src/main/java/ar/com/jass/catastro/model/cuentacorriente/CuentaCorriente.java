package ar.com.jass.catastro.model.cuentacorriente;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.Where;

import ar.com.jass.catastro.model.Cuenta;
import ar.com.jass.catastro.model.EntityBase;
import ar.com.jass.catastro.util.DateHelper;

/**
 * Entidad de movimientos de cuenta corriente. Deudas y creditos
 * 
 * @author msquarini
 *
 */
@Entity
@Table(name = "CC")
@Where(clause = "activo = true")
public class CuentaCorriente extends EntityBase implements Deuda, Serializable {

    private Long id;

    /**
     * Cuenta a la que pertenece el registro de CC
     */
    private Cuenta cuenta;

    /**
     * Fecha de vencimiento de la deuda (En los creditos siempre null)
     */
    private Date fechaVto;

    /**
     * Fecha de pago de la deuda
     * 
     */
    private Date fechaPago;

    /**
     * Monto del movimiento
     */
    private BigDecimal importe;

    private Integer anio;
    private Integer cuota;

    /**
     * Indica si se trata de una cuota anual
     */
    /*
     * @Deprecated private Boolean cuotaAnual;
     */

    /**
     * Indica si esta dentro de una moratoria segun el codigo que presente
     */
    private Integer moratoria;

    /**
     * Ajuste sobre cuota original
     */
    private Integer ajuste;

    /**
     * Indica si se trata de un movimiento de ajuste por variacion de linea o tramite
     */
    private Boolean flagAjuste;
    
    /**
     * Codigos de tipos de movimiento Exenta, etc.
     */
    private Integer codAdministrativo;

    /**
     * Interes que se aplicaron al movimiento
     */
    private BigDecimal interes;

    /**
     * Tipo de movimiento DEBITO / CREDITO
     */
    private TipoMovimiento tipo;

    private List<CCDetalle> detalle;

    /**
     * Referencia al pago del moviemiento
     */
    // private Pago pago;

    private Boolean activo;
    
    private Periodo periodo;

    /**
     * Constructor default de movimiento de debito
     */
    public CuentaCorriente() {
        this.importe = BigDecimal.ZERO;
        this.interes = BigDecimal.ZERO;
        // this.cuotaAnual = Boolean.FALSE;
        this.ajuste = 0;
        this.moratoria = 0;
        this.detalle = new ArrayList<>();
        this.tipo = TipoMovimiento.D;
        this.activo = Boolean.TRUE;
        this.flagAjuste = Boolean.FALSE;
    }

    public CuentaCorriente(TipoMovimiento tipo, Periodo periodo) {
        this();
        this.tipo = tipo;
        this.anio = periodo.getAnio();
        this.cuota = periodo.getCuota();
        this.fechaVto = periodo.getFechaVto();
        this.periodo = periodo;
    }
    
    public CuentaCorriente(CuentaCorriente origen) {
        this();
        this.tipo = origen.getTipo();
        this.anio = origen.getAnio();
        this.cuota = origen.getCuota();
        this.fechaVto = origen.getFechaVto();
        this.periodo = origen.getPeriodo();
        this.importe = origen.getImporte();        
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_cc_gen")
    @SequenceGenerator(name = "seq_cc_gen", sequenceName = "seq_cc", allocationSize = 1)
    @Column(name = "id_cc")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_cuenta")
    public Cuenta getCuenta() {
        return cuenta;
    }

    public void setCuenta(Cuenta cuenta) {
        this.cuenta = cuenta;
    }

    @Column(name = "fecha_vto")
    public Date getFechaVto() {
        return fechaVto;
    }

    public void setFechaVto(Date fechaVto) {
        this.fechaVto = fechaVto;
    }

    @Column(name = "importe")
    public BigDecimal getImporte() {
        return importe;
    }

    public void setImporte(BigDecimal importe) {
        this.importe = importe;
    }

    @Column(name = "anio")
    public Integer getAnio() {
        return anio;
    }

    public void setAnio(Integer anio) {
        this.anio = anio;
    }

    @Column(name = "cuota")
    public Integer getCuota() {
        return cuota;
    }

    public void setCuota(Integer cuota) {
        this.cuota = cuota;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id, this.anio, this.cuota);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CuentaCorriente other = (CuentaCorriente) obj;
        return Objects.equals(this.id, other.id) && Objects.equals(this.cuenta, other.cuenta)
                && Objects.equals(this.anio, other.anio) && Objects.equals(this.cuota, other.cuota)
                && Objects.equals(this.getTipo(), other.getTipo());
    }

    @Column(name = "moratoria")
    public Integer getMoratoria() {
        return moratoria;
    }

    public void setMoratoria(Integer moratoria) {
        this.moratoria = moratoria;
    }

    @Column(name = "ajuste")
    public Integer getAjuste() {
        return ajuste;
    }

    public void setAjuste(Integer ajuste) {
        this.ajuste = ajuste;
    }

    @Column(name = "tipo")
    @Enumerated(javax.persistence.EnumType.STRING)
    public TipoMovimiento getTipo() {
        return tipo;
    }

    public void setTipo(TipoMovimiento tipo) {
        this.tipo = tipo;
    }

    @Column(name = "fecha_pago")
    public Date getFechaPago() {
        return fechaPago;
    }

    public void setFechaPago(Date fechaPago) {
        this.fechaPago = fechaPago;
    }

    // @OneToMany(fetch = FetchType.LAZY, mappedBy = "cuentaCorriente")
    // @Cascade(CascadeType.ALL)
    // public List<CCDetalle> getDetalle() {
    // return detalle;
    // }
    @OneToMany()
    @Cascade(CascadeType.ALL)
    @JoinTable(
            name = "CC_CCDETALLE", joinColumns = @JoinColumn(name = "id_cc"),
            inverseJoinColumns = @JoinColumn(name = "id_cc_detalle"))
    public List<CCDetalle> getDetalle() {
        return detalle;
    }

    public void setDetalle(List<CCDetalle> detalle) {
        this.detalle = detalle;
    }

    /*
     * @Transient public Boolean getCuotaAnual() { return cuotaAnual; }
     * 
     * public void setCuotaAnual(Boolean cuotaAnual) { this.cuotaAnual = cuotaAnual; }
     */

    public void addDetalle(CCDetalle ccDetalle) {
        getDetalle().add(ccDetalle);
        setImporte(getImporte().add(ccDetalle.getMonto()));
    }

    
    /**
     * Status numerico del registro de cuenta corriente 2 vencido 1 sin pagar 0 pago y credito
     * 
     * @return
     */
    @Transient
    public Integer getStatus() {
     /*   if (getTipo().equals(TipoMovimiento.C)) {
            return 0;
        }
        if (isVencido()) {
            return 2;
        }
        if (getFechaPago() == null) {
            return 1;
        }*/
        return 0;
    }

    /*
     * @Transient public boolean isVencido() { if (getPago() == null &&
     * getFechaVto().before(Calendar.getInstance().getTime())) { return true; } return false; }
     */

    @Column(name = "cod_adm")
    public Integer getCodAdministrativo() {
        return codAdministrativo;
    }

    public void setCodAdministrativo(Integer codAdministrativo) {
        this.codAdministrativo = codAdministrativo;
    }

    @Column(name = "interes")
    public BigDecimal getInteres() {
        return interes;
    }

    public void setInteres(BigDecimal interes) {
        this.interes = interes;
    }

    @Column(name = "activo")
    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    @Transient
    public BigDecimal getMontoTotal() {
        return interes.add(getImporte());
    }

    /**
     * Aplicar el interes segun la tasa y el periodo
     * 
     * @param periodo
     * @param tasa
     */
    @Transient
    public void aplicarInteres(Periodo periodo, Double tasa) {
        int meses = DateHelper.mesesEntreFechas(getFechaVto(), periodo.getFechaVto()) + 1;
        double aplicarTasa = meses * tasa;
        setInteres(getImporte().multiply(BigDecimal.valueOf(aplicarTasa).setScale(2, java.math.RoundingMode.HALF_UP)));
    }

    /**
     * Como saber si el movimiento esta pago?
     * 
     * @return
     */
    /*
     * @Transient public Boolean isPagado() { return getPago() != null; }
     */

    /**
     * Devuelve el concepto de cuota pura de la cuota si lo tiene Devuelve NULL si no presenta
     * 
     * @return
     */
    @Transient
    public CCDetalle getCuotaPura() {
        CCDetalle cuotaPura = null;
        for (CCDetalle ccdetalle : getDetalle()) {
            if (ccdetalle.getConcepto().equals(ConceptoCuentaCorriente.CUOTA)) {
                cuotaPura = ccdetalle;
                break;
            }
        }
        return cuotaPura;
    }

    /**
     * Si el movimiento corresponde a un periodo previo al periodo indicado
     * 
     * @param periodo
     * @return
     */
    public Boolean esPrevio(Periodo periodo) {
        if (getAnio() < periodo.getAnio()) {
            return true;
        } else if ((getCuota() < periodo.getCuota()) && (getAnio().intValue() == periodo.getAnio().intValue())) {
            return true;
        }
        return false;
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_periodo")
    public Periodo getPeriodo() {
        return periodo;
    }

    public void setPeriodo(Periodo periodo) {
        this.periodo = periodo;
    }

    @Override
    public String toString() {
        return "CuentaCorriente [id=" + id + ", cuenta=" + cuenta + ", fechaVto=" + fechaVto + ", fechaPago="
                + fechaPago + ", importe=" + importe + ", anio=" + anio + ", cuota=" + cuota + ", moratoria="
                + moratoria + ", ajuste=" + ajuste + ", codAdministrativo=" + codAdministrativo + ", interes=" + interes
                + ", tipo=" + tipo + ", activo=" + activo + "]";
    }

    @Column(name = "flag_ajuste")
    public Boolean getFlagAjuste() {
        return flagAjuste;
    }

    public void setFlagAjuste(Boolean flagAjuste) {
        this.flagAjuste = flagAjuste;
    }

    /**
     * Si es movimiento de cuota original
     * @return
     */
    @Transient
    public Boolean esCuotaBase() {
        return (getTipo().isDebito() && getAjuste() == 0 && getMoratoria() == 0);
    }
    
    @Transient
    public Boolean esMoratoria() {
        return (getMoratoria() != 0);
    }
}