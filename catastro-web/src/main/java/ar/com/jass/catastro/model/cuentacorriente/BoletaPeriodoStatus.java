package ar.com.jass.catastro.model.cuentacorriente;

/**
 * Estados de generacion de boletas en un periodo
 * 
 * @author msquarini
 *
 */
public enum BoletaPeriodoStatus {

	PENDIENTE, A_PROCESAR, PROCESANDO, GENERADO, ERROR;

}
