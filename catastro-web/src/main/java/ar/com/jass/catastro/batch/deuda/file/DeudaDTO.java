package ar.com.jass.catastro.batch.deuda.file;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

import ar.com.jass.catastro.model.cuentacorriente.Deuda;
import ar.com.jass.catastro.web.dto.CodigoBarra;

/**
 * Datos de deuda para enviar al archivo de bancos
 * 
 * @author mesquarini
 *
 */
public class DeudaDTO {

    private String cuenta;
    private String dv;

    private BigDecimal importe;
    private Date fechaVto;

    private Integer cuota;
    private Integer anio;

    private String codigoBarra;

    public DeudaDTO(String cuenta, String dv) {
        this.cuenta = cuenta;
        this.dv = dv;
    }

    public DeudaDTO(Deuda d) {
        this.cuenta = d.getCuenta().getCuenta();
        this.dv = d.getCuenta().getDv();
        this.importe = d.getImporte();
        this.anio = d.getAnio();
        this.cuota = d.getCuota();
        this.fechaVto = d.getFechaVto();
        this.codigoBarra = CodigoBarra.getCodigoBarra(this);
    }

    public String getCuenta() {
        return cuenta;
    }
    
    public Integer getCuentaInteger() {
        return Integer.valueOf(cuenta);
    }

    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }

    public Integer getDvInteger() {
        if (dv.equals("-")) {
            return 0;
        } else {
            return Integer.valueOf(dv);
        }
    }

    public String getDv() {
        return dv;
    }
    
    public void setDv(String dv) {
        this.dv = dv;
    }

    public BigDecimal getImporte() {
        return importe;
    }

    public void setImporte(BigDecimal importe) {
        this.importe = importe;
    }

    public Date getFechaVto() {
        return fechaVto;
    }

    public void setFechaVto(Date fechaVto) {
        this.fechaVto = fechaVto;
    }

    public Integer getFillCero() {
        return 0;
    }

    public String getFillBlanco() {
        return " ";
    }

    public Integer getCuota() {
        return cuota;
    }

    public void setCuota(Integer cuota) {
        this.cuota = cuota;
    }

    public Integer getAnio() {
        return anio;
    }

    public void setAnio(Integer anio) {
        this.anio = anio;
    }

    public Integer getAnioCorto() {
        return anio - 2000;
    }

    public Integer getImporteEntero() {
        return Integer.valueOf(getImporte().intValue());
    }

    public Integer getImporteDecimal() {
        BigDecimal centavos = getImporte().subtract(getImporte().setScale(0, RoundingMode.FLOOR)).movePointRight(2);
        return Integer.valueOf(centavos.intValue());
    }

    public String getCodigoBarra() {
        return codigoBarra;
    }

    public void setCodigoBarra(String codigoBarra) {
        this.codigoBarra = codigoBarra;
    }

}
