package ar.com.jass.catastro.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

@Entity
@Table(name = "Parcela_Ratificacion")
public class ParcelaRatificacion extends EntityBase implements Serializable, IParcela {

    private Long id;

    private Boolean activa;

    /**
     * Partida de la parcela Unico
     */
    private Integer partida;

    /**
     * Nomenclatura catastral Unico
     */
    private Nomenclatura nomenclatura;

    /**
     * Cuenta relacionada a la parcela
     */
    private CuentaRatificacion cuenta;

    /**
     * Informacion de dominio de la parcela
     */
    private Dominio dominio;

    /**
     * Informacion de linea de valuacion vigente
     */
    private LineaRatificacion lineaVigente;

    /**
     * Lineas valuatorias de la parcela
     */
    private List<LineaRatificacion> lineas;

    /**
     * Titulares de la parcela
     */
    private List<TitularParcelaRatificacion> titulares;

    // Se traslada al resposansable de la cuenta
    // private Domicilio domicilioPostal;
    private DomicilioRatificacion domicilioInmueble;

    private String observacion;

    /**
     * 1,2,3,4,5,6,7,8
     */
    //private Integer categoria;

    /*  *//**
           * A,B,C,D
           *//*
              * @Deprecated private String zona;
              */
    /**
     * Indica si la parcela es de dominio municipal
     */
    private Boolean dominioMunicipal;

    /**
     * Destino de la parcela en caso de ser de dominio municipal
     */
    private String destino;

    private Boolean deleted;

    private Boolean newuf;

    private Parcela origen;

    private List<Parcela> parcelasOrigen;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "parcela_ratificacion_parcela_origen",
            joinColumns = { @JoinColumn(name = "id_parcela", nullable = false, updatable = false) },
            inverseJoinColumns = { @JoinColumn(name = "id_parcela_origen", nullable = false, updatable = false) })
    @Cascade(CascadeType.ALL)
    public List<Parcela> getParcelasOrigen() {
        return parcelasOrigen;
    }

    public void setParcelasOrigen(List<Parcela> parcelasOrigen) {
        this.parcelasOrigen = parcelasOrigen;
    }

    public ParcelaRatificacion() {
        activa = Boolean.FALSE;
        dominioMunicipal = Boolean.FALSE;
        lineaVigente = new LineaRatificacion();
    }

    public ParcelaRatificacion(Parcela origen) {

        this.nomenclatura = new Nomenclatura(origen.getNomenclatura());

        List<LineaRatificacion> lineas = new ArrayList<LineaRatificacion>();
        for (Linea l : origen.getLineas()) {
            LineaRatificacion lr = new LineaRatificacion(l);
            lineas.add(lr);
        }

        this.lineas = lineas;
        this.lineaVigente = new LineaRatificacion(origen.getLineaVigente());

        List<TitularParcelaRatificacion> titulares = new ArrayList<TitularParcelaRatificacion>();
        for (TitularParcela t : origen.getTitulares()) {
            TitularParcelaRatificacion tpr = new TitularParcelaRatificacion(this,
                    new TitularRatificacion(t.getTitular()), t.getPorcentaje(), t.getDesde(), t.getHasta());
            titulares.add(tpr);
        }

        this.titulares = titulares;
        this.partida = origen.getPartida();
        this.cuenta = new CuentaRatificacion(origen.getCuenta());
        this.activa = origen.getActiva();
        this.dominioMunicipal = origen.getDominioMunicipal();
        //this.categoria = origen.getCategoria();
        this.destino = origen.getDestino();
        this.domicilioInmueble = new DomicilioRatificacion(origen.getDomicilioInmueble());
        this.dominio = origen.getDominio();
        this.deleted = Boolean.FALSE;
        this.observacion = origen.getObservacion();
        this.origen = origen;
        this.newuf = Boolean.FALSE;
    }

    @Transient
    public Parcela getParcela() {
        Parcela nuevaParcela = new Parcela();
        nuevaParcela.setNomenclatura(new Nomenclatura(getNomenclatura()));
        nuevaParcela.setActiva(Boolean.TRUE);
        nuevaParcela.setLineaVigente(getLineaVigente().getLinea());
        
        // Lineas de la parcela origen (NO MODIFICADAS) si existe
        if (origen != null) {
            nuevaParcela.setLineas(getOrigen().getLineas());
        } else {
            List<Linea> lineas = new ArrayList<>();
            for (Linea l : getParcelasOrigen().get(0).getLineas()) {                
                lineas.add(new Linea(l));
            }
            nuevaParcela.setLineas(lineas);
        }

        List<TitularParcela> titulares = new ArrayList<TitularParcela>();
        for (TitularParcelaRatificacion tpr : getTitulares()) {
            TitularParcela tp = tpr.getTitularParcela();
            tp.setParcela(nuevaParcela);
            titulares.add(tp);
        }
        nuevaParcela.setTitulares(titulares);

        nuevaParcela.setPartida(this.getPartida());
        //nuevaParcela.setCategoria(this.getCategoria());
        nuevaParcela.setCreatedBy(this.getCreatedBy());
        nuevaParcela.setCreatedDate(this.getCreatedDate());
        nuevaParcela.setDestino(this.getDestino());
        nuevaParcela.setDomicilioInmueble(this.getDomicilioInmueble().getDomicilio());
        nuevaParcela.setDominio(this.getDominio());
        nuevaParcela.setDominioMunicipal(this.getDominioMunicipal());
        nuevaParcela.setModifiedBy(this.getModifiedBy());
        nuevaParcela.setModifiedDate(this.getModifiedDate());
        nuevaParcela.setObservacion(this.getObservacion());
        nuevaParcela.setPartida(this.getPartida());
        return nuevaParcela;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_parcela_ratificacion_gen")
    @SequenceGenerator(
            name = "seq_parcela_ratificacion_gen", sequenceName = "seq_parcela_ratificacion", allocationSize = 1)
    @Column(name = "id_parcela")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Embedded
    public Nomenclatura getNomenclatura() {
        return nomenclatura;
    }

    public void setNomenclatura(Nomenclatura nomenclatura) {
        this.nomenclatura = nomenclatura;
    }

    // @OneToOne(fetch = FetchType.EAGER, optional = false)
    // @JoinColumn(name = "id_cuenta")
    // @Fetch(FetchMode.JOIN)
    // @org.hibernate.annotations.LazyToOne(org.hibernate.annotations.LazyToOneOption.NO_PROXY)
    // @Cascade(CascadeType.SAVE_UPDATE)
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_cuenta", nullable = true)
    @Cascade(CascadeType.ALL)
    public CuentaRatificacion getCuenta() {
        return cuenta;
    }

    /*
     * @OneToOne(fetch = FetchType.LAZY, optional = true)
     * 
     * @JoinColumn(name = "id_cuenta_origen")
     * 
     * @Cascade(CascadeType.SAVE_UPDATE) public Cuenta getCuentaOrigen() { return cuentaOrigen; }
     * 
     * public void setCuentaOrigen(Cuenta cuentaOrigen) { this.cuentaOrigen = cuentaOrigen; }
     */

    @Column(name = "partida")
    public Integer getPartida() {
        return partida;
    }

    public void setPartida(Integer partida) {
        this.partida = partida;
    }

    // @ManyToMany(fetch = FetchType.LAZY, mappedBy = "parcelas")
    @Cascade(CascadeType.ALL)
    @OneToMany(mappedBy = "parcela", orphanRemoval = true)
    public List<TitularParcelaRatificacion> getTitulares() {
        return titulares;
    }

    public void setTitulares(List<TitularParcelaRatificacion> titulares) {
        this.titulares = titulares;
    }

    // @OneToOne(fetch = FetchType.EAGER)
    // @JoinColumn(name = "id_parcela", referencedColumnName="id_parcela")
    // @Cascade(CascadeType.SAVE_UPDATE)
    @Embedded
    public Dominio getDominio() {
        return dominio;
    }

    public void setDominio(Dominio dominio) {
        this.dominio = dominio;
    }

    @OneToMany(fetch = FetchType.LAZY) // , orphanRemoval = true)
    @Cascade({ org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @JoinTable(
            name = "parcela_linea_ratificacion", joinColumns = @JoinColumn(name = "id_parcela"),
            inverseJoinColumns = @JoinColumn(name = "id_linea"))
    public List<LineaRatificacion> getLineas() {
        return lineas;
    }

    public void setLineas(List<LineaRatificacion> lineas) {
        this.lineas = lineas;
    }

    @OneToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "id_domicilio_inmueble")
    @Cascade({ org.hibernate.annotations.CascadeType.ALL })
    public DomicilioRatificacion getDomicilioInmueble() {
        return domicilioInmueble;
    }

    public void setCuenta(CuentaRatificacion cuenta) {
        this.cuenta = cuenta;
    }

    public void setDomicilioInmueble(DomicilioRatificacion domicilioInmueble) {
        this.domicilioInmueble = domicilioInmueble;
    }

    @Column(name = "observacion")
    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String toString() {
        return "ParcelaRatificacion id[" + getId() + "] Partida: [" + getPartida() + "] " + getNomenclatura() + " - "
                + getDominio();
        // + getDomicilioInmueble() + "\n" + getDomicilioPostal();
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id, this.partida, this.nomenclatura);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        /*
         * if (!(obj instanceof Parcela)) { return false; }
         */
        if (!getClass().isAssignableFrom(obj.getClass()))
            return false;

        final ParcelaRatificacion other = (ParcelaRatificacion) obj;
        return Objects.equals(this.id, other.id) && Objects.equals(this.partida, other.partida)
                && Objects.equals(this.nomenclatura, other.nomenclatura);
    }

    // @OneToOne(mappedBy = "parcela", fetch = FetchType.LAZY)
    @OneToOne()
    @JoinColumn(name = "id_linea")
    @Cascade(CascadeType.ALL)
    public LineaRatificacion getLineaVigente() {
        return lineaVigente;
    }

    public void setLineaVigente(LineaRatificacion lineaVigente) {
        /*
         * this.lineaVigente.setSuperficieCubierta(lineaVigente.getSuperficieCubierta()) ;
         * this.lineaVigente.setSuperficieSemi(lineaVigente.getSuperficieSemi());
         * this.lineaVigente.setSuperficieTerreno(lineaVigente.getSuperficieTerreno());
         * this.lineaVigente.setValorComun(lineaVigente.getValorComun());
         * this.lineaVigente.setValorEdificio(lineaVigente.getValorEdificio());;
         * this.lineaVigente.setValorTierra(lineaVigente.getValorTierra());
         */
        this.lineaVigente = lineaVigente;
    }

//    @Column(name = "categoria")
//    public Integer getCategoria() {
//        return categoria;
//    }
//
//    public void setCategoria(Integer categoria) {
//        this.categoria = categoria;
//    }

    @Column(name = "activa")
    public Boolean getActiva() {
        return activa;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public void setActiva(Boolean activa) {
        this.activa = activa;
    }

    @Column(name = "dom_municipal")
    public Boolean getDominioMunicipal() {
        return dominioMunicipal;
    }

    public void setDominioMunicipal(Boolean dominioMunicipal) {
        this.dominioMunicipal = dominioMunicipal;
    }

    @Column(name = "destino")
    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_origen", nullable = true)
    public Parcela getOrigen() {
        return origen;
    }

    public void setOrigen(Parcela origen) {
        this.origen = origen;
    }

    public boolean hasOrigin() {
        return (this.origen != null);

    }

    public Boolean getNewuf() {
        return newuf;
    }

    public void setNewuf(Boolean newuf) {
        this.newuf = newuf;
    }
}
