package ar.com.jass.catastro.dao.impl;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityNotFoundException;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.StandardBasicTypes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import ar.com.jass.catastro.dao.GeneralDAO;
import ar.com.jass.catastro.model.Cuenta;
import ar.com.jass.catastro.model.EstadoTramite;
import ar.com.jass.catastro.model.Nomenclatura;
import ar.com.jass.catastro.model.Parcela;
import ar.com.jass.catastro.model.Titular;
import ar.com.jass.catastro.model.Tramite;
import ar.com.jass.catastro.model.da.DebitoAutomatico;
import ar.com.jass.catastro.model.temporal.LineaTemporal;
import ar.com.jass.catastro.util.DateHelper;

@SuppressWarnings("unchecked")
@Repository("generalDAO")
public class GeneralDAOImpl implements GeneralDAO {

    private static final Logger LOG = LoggerFactory.getLogger("GeneralDAOImpl");

    @Autowired
    private SessionFactory sessionFactory;

    protected Session currentSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public Serializable save(Object entity) {
        return getSessionFactory().getCurrentSession().save(entity);
    }

    @Override
    public <T> void saveList(List<T> entities) {
        for (T entity : entities) {
            getSessionFactory().getCurrentSession().save(entity);
        }
    }

    @Override
    public <T> void updateList(List<T> entities) {
        for (T entity : entities) {
            getSessionFactory().getCurrentSession().saveOrUpdate(entity);
        }
    }

    @Override
    public void saveOrUpdate(Object entity) {
        getSessionFactory().getCurrentSession().saveOrUpdate(entity);
    }

    @Override
    public Object merge(Object entity) {
        return getSessionFactory().getCurrentSession().merge(entity);
    }

    @Override
    public void update(Object entity) {
        getSessionFactory().getCurrentSession().update(entity);
    }

    @Override
    public void remove(Object entity) {
        getSessionFactory().getCurrentSession().delete(entity);
    }

    @Override
    public <T> T find(Class<T> type, Serializable key) {
        return (T) getSessionFactory().getCurrentSession().get(type, key);
        // return (T) getSessionFactory().getCurrentSession().load(type, key);
    }

    @Override
    public <T> List<T> getAll(Class<T> type) {
        return getSessionFactory().getCurrentSession().createCriteria(type)
                .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Override
    public BigInteger obtenerNumeroCuenta() {
        String seqNextValue = "SELECT nextval('seq_numero_cuenta');";
        Query sql = getSessionFactory().getCurrentSession().createSQLQuery(seqNextValue);
        BigInteger next = (BigInteger) sql.uniqueResult();

        return next;
    }

    @Override
    public Cuenta findCuenta(String cuenta, String dv) {
        Criteria c = getSessionFactory().getCurrentSession().createCriteria(Cuenta.class);
        c.add(Restrictions.eq("cuenta", cuenta));
        if (dv != null && dv.length() > 0) {
            c.add(Restrictions.eq("dv", dv));
        }
        try {
            return (Cuenta) c.list().get(0);
        } catch (IndexOutOfBoundsException iobe) {
            throw new EntityNotFoundException();
        }
    }

    @Override
    public List<Cuenta> findCuentasLike(String cuenta) {
        String hql = "Select c from Cuenta c where c.cuenta like :cuenta";
        Query q = getSessionFactory().getCurrentSession().createQuery(hql);
        q.setParameter("cuenta", cuenta + "%");

        try {
            return (List<Cuenta>) q.list();
        } catch (IndexOutOfBoundsException iobe) {
            throw new EntityNotFoundException();
        }
    }

    @Override
    public Parcela findParcelaByPartida(Integer partida) {
        String hql = "Select p from Parcela p where p.partida = :partida";
        Query q = getSessionFactory().getCurrentSession().createQuery(hql);
        q.setParameter("partida", partida);

        return (Parcela) q.list().get(0);
    }

    @Override
    public List<Titular> findTitulares(Titular titular) {
        Criteria c = getSessionFactory().getCurrentSession().createCriteria(Titular.class);
        if (titular.getApellido() != null && titular.getApellido().length() > 0) {
            c.add(Restrictions.like("apellido", titular.getApellido()));
        }
        if (titular.getNombre() != null && titular.getNombre().length() > 0) {
            c.add(Restrictions.like("nombre", titular.getNombre()));
        }
        if (titular.getCuitcuil() != null && titular.getCuitcuil().length() > 0) {
            c.add(Restrictions.like("cuitcuil", titular.getCuitcuil()));
        }
        return (List<Titular>) c.list();
    }

    @Override
    public List<Parcela> findParcelas(Nomenclatura nomenclatura) {
        Criteria c = createCriteriaNomenclatura(nomenclatura);
        return (List<Parcela>) c.list();
    }

    @Override
    public Parcela findParcela(Nomenclatura nomenclatura) {
        Criteria c = createCriteriaNomenclatura(nomenclatura);
        return (Parcela) c.uniqueResult();
    }

    /*
     * (non-Javadoc)
     * 
     * @see ar.com.jass.catastro.dao.GeneralDAO#exists(ar.com.jass.catastro.model. Nomenclatura)
     */
    @Override
    public boolean exists(Nomenclatura nomenclatura) {
        Criteria c = createCriteriaNomenclatura(nomenclatura);
        return (c.uniqueResult() == null) ? false : true;
    }

    /*
     * (non-Javadoc)
     * 
     * @see ar.com.jass.catastro.dao.GeneralDAO#findTramites(java.lang.Long)
     */
    public List<Tramite> findTramites(Long cuentaId) {
        Criteria c = getSessionFactory().getCurrentSession().createCriteria(Tramite.class);
        c.createAlias("cuentasOrigen", "c");
        c.add(Restrictions.eq("c.id", cuentaId));

        return c.list();
    }

    /*
     * @Override public void initialize(Object o) { Hibernate.initialize(o); }
     */

    /**
     * Arma criterio de busqueda x nomenclatura
     * 
     * @param nomenclatura
     * @return
     */
    private Criteria createCriteriaNomenclatura(Nomenclatura nomenclatura) {
        Criteria c = getSessionFactory().getCurrentSession().createCriteria(Parcela.class);

        if (nomenclatura.getCircunscripcion() != null && nomenclatura.getCircunscripcion() > 0) {
            c.add(Restrictions.eq("nomenclatura.circunscripcion", nomenclatura.getCircunscripcion()));
        }
        if (nomenclatura.getSeccion() != null && nomenclatura.getSeccion().length() > 0) {
            c.add(Restrictions.eq("nomenclatura.seccion", nomenclatura.getSeccion()));
        }
        if (nomenclatura.getFraccion() != null && nomenclatura.getFraccion() > 0) {
            c.add(Restrictions.eq("nomenclatura.fraccion", nomenclatura.getFraccion()));
        }
        if (nomenclatura.getFraccionLetra() != null && nomenclatura.getFraccionLetra().length() > 0) {
            c.add(Restrictions.eq("nomenclatura.fraccionLetra", nomenclatura.getFraccionLetra()));
        }
        if (nomenclatura.getManzana() != null && nomenclatura.getManzana() > 0) {
            c.add(Restrictions.eq("nomenclatura.manzana", nomenclatura.getManzana()));
        }
        if (nomenclatura.getManzanaLetra() != null && nomenclatura.getManzanaLetra().length() > 0) {
            c.add(Restrictions.eq("nomenclatura.manzanaLetra", nomenclatura.getManzanaLetra()));
        }
        if (nomenclatura.getParcela() != null && nomenclatura.getParcela() > 0) {
            c.add(Restrictions.eq("nomenclatura.parcela", nomenclatura.getParcela()));
        }
        if (nomenclatura.getParcelaLetra() != null && nomenclatura.getParcelaLetra().length() > 0) {
            c.add(Restrictions.eq("nomenclatura.parcelaLetra", nomenclatura.getParcelaLetra()));
        }
        if (nomenclatura.getUnidadFuncional() != null && nomenclatura.getUnidadFuncional().length() > 0) {
            c.add(Restrictions.eq("nomenclatura.unidadFuncional", nomenclatura.getUnidadFuncional()));
        }
        return c;
    }

    /*
     * public CuentaCorriente findCuentaCorriente(Integer anio, Integer cuota, String cuenta, String
     * dv, BigDecimal importe, Date fechaVto, TipoMovimiento tipo) {
     * LOG.debug("Buscando CC {} {} {} {} {}", anio, cuota, cuenta, dv, tipo);
     * 
     * Criteria c = getSessionFactory().getCurrentSession().createCriteria(CuentaCorriente.class);
     * c.add(Restrictions.eq("anio", anio)); c.add(Restrictions.eq("cuota", cuota));
     * c.add(Restrictions.eq("tipo", tipo)); c.add(Restrictions.eq("importe", importe));
     * c.add(Restrictions.eq("fechaVto", fechaVto)); c.createAlias("cuenta", "c");
     * c.add(Restrictions.eq("c.cuenta", cuenta)); c.add(Restrictions.eq("c.dv", dv));
     * 
     * try { return (CuentaCorriente) c.list().get(0); } catch (IndexOutOfBoundsException iobe) {
     * throw new EntityNotFoundException(); } }
     */

    public DebitoAutomatico findDA(String cuenta, String dv) {
        LOG.debug("Buscando DA {} {} ", cuenta, dv);

        Criteria c = getSessionFactory().getCurrentSession().createCriteria(DebitoAutomatico.class);
        c.createAlias("cuenta", "c");
        c.add(Restrictions.eq("c.cuenta", cuenta));
        c.add(Restrictions.eq("c.dv", dv));

        try {
            return (DebitoAutomatico) c.list().get(0);
        } catch (IndexOutOfBoundsException iobe) {
            throw new EntityNotFoundException();
        }
    }

    /*
     * public static final String DEUDA_AMOUNT =
     * "select SUM(case when tipo = 'C' then importe*-1 when tipo = 'D' then importe end) as deuda "
     * + " from cc where id_cuenta = :id_cuenta and moratoria = 0 and fecha_vto < :fecha ";
     */

    /**
     * Busqueda de registros de CuentaCorriente de Debitos, donde no se presenten pagos, moratoria y
     * cod_adm y este vencido la fecha de pago
     */
    public static final String DEUDA_AMOUNT = "select COALESCE(SUM(importe),0) as deuda "
            + " from catastro.cc where id_cuenta = :id_cuenta and tipo = 'D' and id_pago is null and moratoria = 0 and cod_adm = 0 "
            + " and fecha_vto < :fecha ";

    /*
     * (non-Javadoc)
     * 
     * @see ar.com.jass.catastro.dao.GeneralDAO#getDeuda(java.lang.Long)
     */
    public BigDecimal getDeuda(Long cuentaId) {
        SQLQuery sqlQuery = getSessionFactory().getCurrentSession().createSQLQuery(DEUDA_AMOUNT);
        sqlQuery.setLong("id_cuenta", cuentaId);
        sqlQuery.setDate("fecha", Calendar.getInstance().getTime());

        sqlQuery.addScalar("deuda", StandardBasicTypes.BIG_DECIMAL);
        return (BigDecimal) sqlQuery.list().get(0);
    }

    public List<Tramite> getTramites(Date from, Date to, EstadoTramite estado, String usuario) {
        Criteria criteria = currentSession().createCriteria(Tramite.class);
        criteria.addOrder(Order.desc("createdDate"));

        criteria.add(Restrictions.ge("createdDate", DateHelper.getFormattedFromDateTime(from)));
        criteria.add(Restrictions.le("createdDate", DateHelper.getFormattedToDateTime(to)));

        /*
         * if (estados != null) { Disjunction estadosCriteria = Restrictions.disjunction(); for
         * (EstadoFilePagos e : estados) { estadosCriteria.add(Restrictions.eq("estado", e)); }
         * criteria.add(estadosCriteria); }
         */
        if (estado != null) {
            criteria.add(Restrictions.eq("estado", estado));
        }
        if (usuario != null && usuario.length() > 1) {
            criteria.add(Restrictions.like("modifiedBy", usuario));
        }
        return criteria.list();
    }

    public LineaTemporal findLineaTemporalByTramite(Long idTramite) {
        Criteria c = getSessionFactory().getCurrentSession().createCriteria(LineaTemporal.class);
        // c.createAlias("tramite", "t");
        c.addOrder(Order.desc("modifiedDate"));
        c.setMaxResults(1);
        c.add(Restrictions.eq("tramite.id", idTramite));
        return (LineaTemporal) c.uniqueResult();
        /*
         * Query hql = getSessionFactory().getCurrentSession().
         * createQuery("select l from LineaTemporal l where l.tramite.id = :id");
         * hql.setParameter("id", idTramite); return (LineaTemporal) hql.uniqueResult();
         */
    }
}
