package ar.com.jass.catastro.business.impl;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.com.jass.catastro.business.CatastroService;
import ar.com.jass.catastro.business.CuentaCorrienteService;
import ar.com.jass.catastro.dao.GeneralDAO;
import ar.com.jass.catastro.dao.ParcelaDAO;
import ar.com.jass.catastro.exceptions.BusinessException;
import ar.com.jass.catastro.model.Cuenta;
import ar.com.jass.catastro.model.EstadoTramite;
import ar.com.jass.catastro.model.Linea;
import ar.com.jass.catastro.model.Parcela;
import ar.com.jass.catastro.model.ResponsableCuenta;
import ar.com.jass.catastro.model.TitularParcela;
import ar.com.jass.catastro.model.Tramite;
import ar.com.jass.catastro.model.cuentacorriente.CuentaCorriente;
import ar.com.jass.catastro.model.temporal.LineaTemporal;
import ar.com.jass.catastro.util.HibernateHelper;

@Service
public class CatastroServiceImpl implements CatastroService {

    private static final Logger LOG = LoggerFactory.getLogger("CatastroServiceImpl");

    @Autowired
    @Qualifier("parcelaDAO")
    private ParcelaDAO parcelaDAO;

    @Autowired
    @Qualifier("generalDAO")
    private GeneralDAO generalDAO;

    @Autowired
    private CuentaCorrienteService ccService;

    public ParcelaDAO getParcelaDAO() {
        return parcelaDAO;
    }

    public void setParcelaDAO(ParcelaDAO parcelaDAO) {
        this.parcelaDAO = parcelaDAO;
    }

    public GeneralDAO getGeneralDAO() {
        return generalDAO;
    }

    public void setGeneralDAO(GeneralDAO generalDAO) {
        this.generalDAO = generalDAO;
    }

    /**
     * Grabar el tramite y datos de cuentas, parcelas, etc. Para gestiones largas, se persisten los
     * tramites para completar la gestion en otro momento
     * 
     * @param tramite
     */
    @Override
    @Transactional
    public Tramite grabarTramite(Tramite tramite, List<Parcela> nuevasParcelas, List<TitularParcela> titulares,
            List<ResponsableCuenta> responsables, Boolean procesar) throws BusinessException {
        LOG.info("Grabar {}, procesar {}", tramite, procesar);
        tramite.getCuentas().clear();

        for (Parcela nuevaParcela : nuevasParcelas) {

            /**
             * Crea la nueva cuenta
             */
            if (nuevaParcela.getId() == null) {
                BigInteger numeroCuenta = generalDAO.obtenerNumeroCuenta();
                Cuenta nuevaCuenta = new Cuenta(numeroCuenta, nuevaParcela);
                cargarResponsables(nuevaCuenta, responsables);
                cargarTitulares(nuevaParcela, titulares);

                nuevaCuenta.setTramite(tramite);
                tramite.getCuentas().add(nuevaCuenta);
                generalDAO.save(nuevaParcela);
            } else {
                tramite.getCuentas().add(nuevaParcela.getCuenta());
            }

            nuevaParcela.getLineaVigente().setFecha(tramite.getVigencia());

            /**
             * PROCESAMIENTO - Activamos nuevas cuentas
             */
            if (procesar) {
                nuevaParcela.getCuenta().setActiva(Boolean.TRUE);
                nuevaParcela.getCuenta().setBloqueada(Boolean.FALSE);
            }

            // generalDAO.saveOrUpdate(nuevaParcela);
            generalDAO.merge(nuevaParcela);
        }

        /**
         * PROCESAMIENTO
         */
        if (procesar) {

            /**
             * Calculo de deuda en las nuevas cuentas a partir de la fecha de creacion del tramite y 
             * calculo retroactivo desde la fecha de vigencia
             */
            List<CuentaCorriente> ajustes = ccService.procesamientoCuentaCorriente(tramite, nuevasParcelas);
            //List<CuentaCorriente> deudaCC = ccService.generarCalculoRetroactivo(tramite, nuevasParcelas);
            if (ajustes.size() > 0) {
                generalDAO.saveList(ajustes);
            }

            /**
             * Desactivamos cuentas origen - Cambiamos estado tramite
             */
            List<Cuenta> cuentasOrigen = tramite.getCuentasOrigen();
            for (Cuenta c : cuentasOrigen) {
                c.setActiva(Boolean.FALSE);
                c.setBloqueada(Boolean.FALSE);
            }
            tramite.setEstado(EstadoTramite.FINALIZADO);
        }

        if (tramite.getId() != null) {
            generalDAO.merge(tramite);
        } else {
            generalDAO.saveOrUpdate(tramite);
        }

        return tramite;
    }

    /*
     * (non-Javadoc)
     * 
     * @see ar.com.jass.catastro.business.CatastroService#grabarTramite(ar.com.jass.catastro.model.
     * Tramite, ar.com.jass.catastro.model.Cuenta, ar.com.jass.catastro.model.Linea,
     * java.lang.Integer)
     */
    @Override
    @Transactional
    public Tramite grabarTramite(Tramite tramite, Linea nuevaLinea, Integer ajusteCategoria) {
        LOG.info("Grabar {}", tramite);

        /*
         * if(ajusteCategoria.equals(4)) { if(nuevaLinea.getSuperficieCubierta()==null)
         * nuevaLinea.setSuperficieCubierta((double) 0);
         * 
         * if(nuevaLinea.getSuperficieCubierta()==null) nuevaLinea.setSuperficieCubierta((double)
         * 0);
         * 
         * if(nuevaLinea.getSuperficieSemi()==null) nuevaLinea.setSuperficieSemi((double) 0);
         * 
         * if(nuevaLinea.getValorEdificio()==null)
         * nuevaLinea.setValorEdificio(BigDecimal.valueOf(0)); }
         */

        if (tramite.getId() != null) {
            generalDAO.saveOrUpdate(tramite);
        } else {
            generalDAO.save(tramite);
        }

        // Persistir linea temporal
        LineaTemporal lineaTemporal = new LineaTemporal(nuevaLinea, ajusteCategoria, tramite);
        generalDAO.save(lineaTemporal);

        return tramite;
    }

    @Override
    @Transactional
    @Deprecated
    public Tramite subdividir(Cuenta cuentaOrigen, List<Parcela> nuevasParcelas, Tramite tramite,
            List<TitularParcela> titulares, List<ResponsableCuenta> responsables) throws BusinessException {
        LOG.info("Tramite subdivision {}, en {} parcelas", tramite, nuevasParcelas.size());

        // Desactivo la cuenta origen y persisto
        cuentaOrigen.setActiva(Boolean.FALSE);

        /**
         * Completo la informacion del tramite Indicando la cuenta origen y agregando las nuevas
         * cuentas generadas
         */
        tramite.getCuentasOrigen().add(cuentaOrigen);
        generalDAO.save(tramite);

        for (Parcela nuevaParcela : nuevasParcelas) {

            // validateNomenclatura(origen.getParcela().getNomenclatura(),
            // p.getNomenclatura());

            cargarTitulares(nuevaParcela, titulares);
            // nuevaParcela.setTitulares(new ArrayList<TitularParcela>(titulares));

            /**
             * Creacion de nueva Cuenta
             */
            BigInteger numeroCuenta = generalDAO.obtenerNumeroCuenta();
            Cuenta nuevaCuenta = new Cuenta(numeroCuenta, nuevaParcela);
            cargarResponsables(nuevaCuenta, responsables);

            nuevaCuenta.setTramite(tramite);
            tramite.getCuentas().add(nuevaCuenta);

            /**
             * Generar deuda a partir de fecha de vigencia Solo en caso de Codigo 6 de tramite
             * (TRATAMAMIENTO ESPECIAL) distribuir deuda origen en las nuevas cuentas
             */
            List<CuentaCorriente> deudaCC = ccService.generarDeuda(tramite.getCreatedDate(), nuevaCuenta,
                    Boolean.FALSE);
            generalDAO.saveList(deudaCC);

            // Persisto nueva parcela
            generalDAO.save(nuevaParcela);
            LOG.info("Cuanta {}, subdividida en cuenta {}", cuentaOrigen, nuevaCuenta);
        }
        return tramite;
    }

    /*
     * (non-Javadoc)
     * 
     * @see ar.com.jass.catastro.business.CatastroService#unificar(java.util.List,
     * ar.com.jass.catastro.model.Parcela)
     */
    @Override
    @Transactional
    @Deprecated
    public Tramite unificar(List<Parcela> origenes, Parcela nuevaParcela, List<TitularParcela> titulares,
            List<ResponsableCuenta> responsables, Tramite tramite) throws BusinessException {
        LOG.info("Unificacion, {} ", tramite);

        /**
         * Desactivamos las cuentas/parcelas origen
         */
        for (Parcela p : origenes) {
            p.getCuenta().desactivar();

            // Agregamos al tramite las cuentas/parcelas origen de la unificacion
            tramite.getCuentasOrigen().add(p.getCuenta());
        }

        cargarTitulares(nuevaParcela, titulares);

        /**
         * Crea la nueva cuenta
         */
        BigInteger numeroCuenta = generalDAO.obtenerNumeroCuenta();
        Cuenta nuevaCuenta = new Cuenta(numeroCuenta, nuevaParcela);
        // nuevaParcela.setCuenta(nuevaCuenta);

        cargarResponsables(nuevaCuenta, responsables);

        nuevaCuenta.setTramite(tramite);
        tramite.getCuentas().add(nuevaCuenta);

        LOG.info("Unificacion - Tramite {} - Creando Cuenta {} ", tramite, nuevaCuenta);

        getGeneralDAO().save(nuevaParcela);
        getGeneralDAO().save(tramite);

        /**
         * Generar deuda a partir de fecha de vigencia Solo en caso de Codigo 6 de tramite
         * (TRATAMAMIENTO ESPECIAL) distribuir deuda origen en las nuevas cuentas
         */
        List<CuentaCorriente> deudaCC = ccService.generarDeuda(tramite.getCreatedDate(), nuevaCuenta, Boolean.FALSE);
        generalDAO.saveList(deudaCC);

        return tramite;
    }

    @Override
    @Transactional
    public Cuenta findCuenta(String cuenta, String dv, String... initialize) {
        Cuenta c = generalDAO.findCuenta(cuenta, dv);
        HibernateHelper.initialize(c, initialize);
        return c;
    }

    /*
     * (non-Javadoc)
     * 
     * @see ar.com.jass.catastro.business.CatastroService#presentaDeuda(ar.com.jass.
     * catastro.model.Cuenta)
     */
    @Override
    @Transactional
    public boolean presentaDeuda(Cuenta cuenta) {
        return false;
/*        if (cuenta.getId() != null) {
            BigDecimal deuda = generalDAO.getDeuda(cuenta.getId());

            return deuda.compareTo(BigDecimal.ZERO) != 0;
        } else {
            return false;
        }*/
    }

    @Override
    @Transactional
    public Tramite inscripcion(Tramite tramite, Parcela parcela, List<TitularParcela> titulares)
            throws BusinessException {
        LOG.info("Procesamiento de inscripcion, {}", parcela);

        /**
         * Crea la nueva cuenta
         */
        BigInteger numeroCuenta = generalDAO.obtenerNumeroCuenta();
        Cuenta nuevaCuenta = new Cuenta(numeroCuenta);
        parcela.setCuenta(nuevaCuenta);

        cargarTitulares(parcela, titulares);

        nuevaCuenta.setTramite(tramite);
        tramite.getCuentas().add(nuevaCuenta);

        LOG.info("inscripcion, creando cuenta {} --> parcela {}", nuevaCuenta, parcela);

        getGeneralDAO().save(parcela);
        getGeneralDAO().save(tramite);
        // Generar Deuda a partir de fecha de vigencia
        List<CuentaCorriente> deudaCC = ccService.generarDeuda(tramite.getCreatedDate(), nuevaCuenta, Boolean.FALSE);
        generalDAO.save(deudaCC);

        // Persistir informacion
        return tramite;
    }

    @Override
    @Transactional
    public Tramite desdoblar(Tramite tramite, Cuenta cuentaOrigen, Parcela parcela, List<TitularParcela> titulares,
            List<ResponsableCuenta> responsables) throws BusinessException {
        LOG.info("Procesamiento de desdoblamiento de cuenta: {}", cuentaOrigen);

        /**
         * Crea la nueva cuenta
         */
        BigInteger numeroCuenta = generalDAO.obtenerNumeroCuenta();
        Cuenta nuevaCuenta = new Cuenta(numeroCuenta);
        parcela.setCuenta(nuevaCuenta);
        nuevaCuenta.setTramite(tramite);

        cargarTitulares(parcela, titulares);
        cargarResponsables(nuevaCuenta, responsables);

        tramite.getCuentas().add(nuevaCuenta);
        tramite.getCuentasOrigen().add(cuentaOrigen);

        LOG.info("Desdoblamiento - Tramite {} - Cuenta {} ", tramite, nuevaCuenta);

        getGeneralDAO().save(parcela);
        getGeneralDAO().save(tramite);

        /**
         * Generar deuda a partir de fecha de vigencia Solo en caso de Codigo 6 de tramite
         * (TRATAMAMIENTO ESPECIAL) distribuir deuda origen en las nuevas cuentas
         */
        List<CuentaCorriente> deudaCC = ccService.generarDeuda(tramite.getCreatedDate(), nuevaCuenta, Boolean.FALSE);
        generalDAO.save(deudaCC);

        return tramite;
    }

    @Override
    @Transactional
    public Tramite updateLinea(Tramite tramite, Cuenta origen, Linea nuevaLinea, Integer ajusteCategoria) {
        Parcela parcela = origen.getParcela();

        // parcela.setCategoria(ajusteCategoria);

        nuevaLinea.setCategoria(ajusteCategoria);
        /*
         * if(ajusteCategoria.equals(4)) { if(nuevaLinea.getSuperficieCubierta()==null)
         * nuevaLinea.setSuperficieCubierta((double) 0);
         * 
         * if(nuevaLinea.getSuperficieCubierta()==null) nuevaLinea.setSuperficieCubierta((double)
         * 0);
         * 
         * if(nuevaLinea.getSuperficieSemi()==null) nuevaLinea.setSuperficieSemi((double) 0);
         * 
         * if(nuevaLinea.getValorEdificio()==null)
         * nuevaLinea.setValorEdificio(BigDecimal.valueOf(0)); }
         */

        // Pasar la linea actual al historico
        parcela.getLineas().add(parcela.getLineaVigente());
        parcela.setLineaVigente(nuevaLinea);

        // Generar movimientos de ajustes
        List<CuentaCorriente> ajustes = ccService.ajusteCuentaCorriente(nuevaLinea, origen, 1, tramite);

        // Persistir los ajustes de cuenta corriente
        generalDAO.saveList(ajustes);

        // grabar parcela
        generalDAO.saveOrUpdate(origen.getParcela());

        // grabar tramite
        // tramite.setCuentasOrigen(Arrays.asList(origen));
        // TODO revisar si es neesariio
        // tramite.setCuentas(null);
        origen.setBloqueada(Boolean.FALSE);
        tramite.setEstado(EstadoTramite.FINALIZADO);
        generalDAO.saveOrUpdate(tramite);

        return tramite;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * ar.com.jass.catastro.business.CatastroService#updateTitulares(ar.com.jass.catastro.model.
     * Tramite, ar.com.jass.catastro.model.Cuenta, java.util.List)
     */
    @Override
    @Transactional
    public Tramite updateTitulares(Tramite tramite, Cuenta origen, List<TitularParcela> titulares) {
        tramite.setCuentasOrigen(Arrays.asList(origen));
        tramite.setEstado(EstadoTramite.FINALIZADO);

        // Sacamos los titulares actuales
        origen.getParcela().getTitulares().clear();
        // Cargamos los nuevos
        cargarTitulares(origen.getParcela(), titulares);

        // Calcular ajuste desde la data del cambio

        // Persistimos el tramite y parcela con nuevos titulares
        getGeneralDAO().updateList(origen.getParcela().getTitulares());
        getGeneralDAO().save(tramite);
        return tramite;
    }

    @Override
    @Transactional
    public Tramite updateResponsables(Tramite tramite, Cuenta origen, List<ResponsableCuenta> responsables) {
        tramite.setCuentasOrigen(Arrays.asList(origen));
        tramite.setEstado(EstadoTramite.FINALIZADO);

        if (origen.getResponsables() != null) {
            origen.getResponsables().clear();
        } else {
            origen.setResponsables(new ArrayList<ResponsableCuenta>());
        }
        for (ResponsableCuenta tp : responsables) {
            tp.setCuenta(origen);
            origen.getResponsables().add(tp);
        }

        // Persistimos el tramite y parcela con nuevos titulares
        getGeneralDAO().updateList(origen.getResponsables());
        getGeneralDAO().save(tramite);
        return tramite;
    }

    /**
     * Carga una lista de titulares en una nueva parcela
     * 
     * @param parcela
     * @param titulares
     */
    private void cargarTitulares(Parcela parcela, List<TitularParcela> titulares) {
        if (parcela.getTitulares() != null) {
            parcela.getTitulares().clear();
        }
        for (TitularParcela tp : titulares) {
            // TitularParcela newTitularParcela = new TitularParcela(parcela, tp.getTitular(),
            // tp.getPorcentaje(),
            // tp.getDesde(), tp.getHasta());
            tp.setParcela(parcela);

            parcela.getTitulares().add(tp);
        }
    }

    /**
     * Carga una lista de responsables en una nueva cuenta. Se agrega solo el/los responsables
     * activos (sin fecha de hasta)
     * 
     * @param parcela
     * @param titulares
     */
    private void cargarResponsables(Cuenta cuenta, List<ResponsableCuenta> responsables) {
        for (ResponsableCuenta rc : responsables) {
            if (rc.getHasta() == null) {
                ResponsableCuenta newResponsable = new ResponsableCuenta(cuenta, rc.getTitular(), rc.getDesde(),
                        rc.getHasta());

                cuenta.getResponsables().add(newResponsable);
            }
        }
    }

    public CuentaCorrienteService getCcService() {
        return ccService;
    }

    public void setCcService(CuentaCorrienteService ccService) {
        this.ccService = ccService;
    }

    @Override
    @Transactional
    public void cancelarTramite(Long idTramite) {
        Tramite tramite = generalDAO.find(Tramite.class, idTramite);
        tramite.setEstado(EstadoTramite.CANCELADO);

        switch (tramite.getTipo()) {
        case SUB:
            // call funcion especial de manejo de tramite Subdivision!
            break;
        case UNI:
            // call funcion especial de manejo de tramite Unificacion!
            break;
        default:
            break;
        }

        // Eliminamos cuentas que se intentaron generar con el trámite
        for (Cuenta c : tramite.getCuentas()) {
            generalDAO.remove(c);
        }
        for (Cuenta c : tramite.getCuentasOrigen()) {
            c.setBloqueada(Boolean.FALSE);
            generalDAO.update(c);
        }
        generalDAO.update(tramite);
    }
}