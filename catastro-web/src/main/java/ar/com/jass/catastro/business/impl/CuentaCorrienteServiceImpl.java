package ar.com.jass.catastro.business.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.com.jass.catastro.batch.deuda.GeneracionDeudaRunBatch;
import ar.com.jass.catastro.business.CuentaCorrienteService;
import ar.com.jass.catastro.dao.CuentaCorrienteDAO;
import ar.com.jass.catastro.dao.GeneralDAO;
import ar.com.jass.catastro.exceptions.BusinessException;
import ar.com.jass.catastro.model.Cuenta;
import ar.com.jass.catastro.model.Linea;
import ar.com.jass.catastro.model.Parcela;
import ar.com.jass.catastro.model.TipoTramite;
import ar.com.jass.catastro.model.Tramite;
import ar.com.jass.catastro.model.cuentacorriente.AjusteDTO;
import ar.com.jass.catastro.model.cuentacorriente.Boleta;
import ar.com.jass.catastro.model.cuentacorriente.CuentaCorriente;
import ar.com.jass.catastro.model.cuentacorriente.DeudaPeriodoStatus;
import ar.com.jass.catastro.model.cuentacorriente.FileDeuda;
import ar.com.jass.catastro.model.cuentacorriente.GeneracionDeudaDTO;
import ar.com.jass.catastro.model.cuentacorriente.Periodo;
import ar.com.jass.catastro.model.cuentacorriente.TipoMovimiento;
import ar.com.jass.catastro.model.pagos.Pago;
import ar.com.jass.catastro.util.DateHelper;
import ar.com.jass.catastro.util.HibernateHelper;
import ar.com.jass.catastro.web.dto.ResumenGeneracionDeuda;

/**
 * @author msquarini
 *
 */
@Service
public class CuentaCorrienteServiceImpl implements CuentaCorrienteService {

    private static final Logger LOG = LoggerFactory.getLogger(CuentaCorrienteServiceImpl.class);

    @Autowired
    @Qualifier("generalDAO")
    private GeneralDAO generalDAO;

    @Autowired
    @Qualifier("ccDAO")
    private CuentaCorrienteDAO cuentaCorrienteDAO;

    @Autowired
    @Qualifier("deudaScheduler")
    private GeneracionDeudaRunBatch batchEmisionDeuda;

    @Autowired
    private DeudaGeneratorStrategy deudaGenerator;

    private Integer cantidadPeriodosAnio;

    public GeneralDAO getGeneralDAO() {
        return generalDAO;
    }

    public void setGeneralDAO(GeneralDAO generalDAO) {
        this.generalDAO = generalDAO;
    }

    @Transactional
    @Override
    public void save(Object o) {
        generalDAO.save(o);
    }

    @Transactional
    @Override
    public List<CuentaCorriente> procesamientoCuentaCorriente(Tramite tramite, List<Parcela> ufs)
            throws BusinessException {
        List<CuentaCorriente> ajustes = new ArrayList<CuentaCorriente>();
        for (Parcela uf : ufs) {
            /**
             * Generacion de deuda a las nuevas cuentas desde el periodo posterior al inicio del
             * tramite
             */
            ajustes.addAll(generarDeuda(tramite.getCreatedDate(), uf.getCuenta(), Boolean.FALSE));
        }
        /**
         * Calculo retroactivo a las nuevas cuentas desde la fecha de vigencia del tramite
         */
        ajustes.addAll(generarCalculoRetroactivo(tramite, ufs));

        return ajustes;
    }

    @Transactional
    public List<CuentaCorriente> generarCalculoRetroactivo(Tramite tramite, List<Parcela> ufs) {
        Date fechaVencimiento = null;
        List<CuentaCorriente> ajustes = null;

        LOG.info("Generando calculo retroactivo Tramite: {}, UFs: {} ", tramite, ufs.size());

        /*
         * if (!tramite.isOficio()) { fechaVencimiento =
         * cuentaCorrienteDAO.findPeriodoVigente().getFechaVto(); }
         */
        Periodo desde = findPeriodo(tramite.getVigencia());
        Periodo hasta = findPeriodo(tramite.getCreatedDate());

        // TODO: obtener el codigoAdministrativo?
        Integer codigoAdministrativo = 9999;

        if (tramite.getTipo().equals(TipoTramite.SUB)) {
            List<CuentaCorriente> movimientosCuentaOrigen = cuentaCorrienteDAO
                    .findMovimientos(tramite.getCuentasOrigen().get(0).getId(), desde, hasta, null, null);

            ajustes = deudaGenerator.generarDeudaRetroactivaSubdivision(movimientosCuentaOrigen, ufs, fechaVencimiento,
                    codigoAdministrativo);

        } else if (tramite.getTipo().equals(TipoTramite.UNI)) {
            ajustes = deudaGenerator.generarDeudaRetroactivaUnificacion(desde.getAnio(), desde.getCuota(),
                    tramite.getCuentas().get(0), tramite.getCuentasOrigen(), fechaVencimiento, codigoAdministrativo);
        }

        return ajustes;
    }

    /*
     * (non-Javadoc)
     * 
     * @see ar.com.jass.catastro.business.CuentaCorrienteService#generarDeuda(java.util. Date,
     * ar.com.jass.catastro.model.Cuenta)
     */
    @Transactional
    @Override
    public List<CuentaCorriente> generarDeuda(Date desde, Cuenta cuenta, boolean aplicaInteres)
            throws BusinessException {
        LOG.info("Generacion periodos deuda. Desde: {} para {}", desde, cuenta);

        List<CuentaCorriente> deudas = new ArrayList<>();
        try {
            Calendar calDesde = Calendar.getInstance();
            calDesde.setTime(desde);
            // Agregamos un mes a la fecha de creacion de tramite, para tomar el periodo posterior
            calDesde.add(Calendar.MONTH, 1);

            // Obtenemos los periodos a partir del siguiente mes de la fecha en que se creo el
            // tramite
            List<Periodo> periodos = findPeriodos(calDesde);

            for (Periodo p : periodos) {
                // Validamos si el periodo fue generado SINO esperamos a la generacion bajo demanda
                if (p.getEstado().equals(DeudaPeriodoStatus.GENERADO)) {
                    // deudas.add(deudaGenerator.generarMovimientoDeuda(p, cuenta, aplicaInteres));
                    GeneracionDeudaDTO dto = new GeneracionDeudaDTO.Builder().cuenta(cuenta).build();
                    deudas.addAll(deudaGenerator.generarDeuda(p, dto).getMovimientos());
                }
            }
        } catch (Exception e) {
            throw new BusinessException("error en generacion de deuda", e);
        }
        return deudas;
    }

    /*
     * @Transactional
     * 
     * @Override public void actualizarInteresDeudaVencida(Periodo periodo, Long idCuenta) { //
     * Buscar DEUDA vencida para la cuenta List<CuentaCorriente> vencidos =
     * cuentaCorrienteDAO.findMovimientosVencidos(idCuenta);
     * 
     * // Actualizar registros con el nuevo interes! for (CuentaCorriente cc : vencidos) { //
     * Diferencia de meses entre la cuota adeudada y el periodo actual // int meses =
     * DateHelper.mesesEntreFechas(cc.getFechaVto(), // periodo.getFechaVto()) + 1; // double tasa =
     * meses * TASA_INTERES_MENSUAL; //
     * cc.setInteres(cc.getImporte().multiply(BigDecimal.valueOf(tasa).setScale(2, //
     * java.math.RoundingMode.HALF_UP))); cc.aplicarInteres(periodo,
     * DeudaGeneratorStrategy.TASA_INTERES_MENSUAL); generalDAO.update(cc); } }
     */

    @Transactional
    @Override
    public Boleta findBoleta(Pago pago) {
        try {
            return cuentaCorrienteDAO.findBoleta(pago.getAnio(), pago.getCuota(), pago.getCuenta(), pago.getDv(),
                    pago.getImporte(), pago.getFechaVto());
        } catch (JpaObjectRetrievalFailureException e) {
            return null;
        }
    }

    @Transactional
    @Override
    public List<Boleta> findBoletas(Long idCuenta, Periodo periodo) {
        return cuentaCorrienteDAO.findBoletas(idCuenta, periodo);
    }

    @Transactional
    @Override
    public List<Boleta> findBoletas(Long idCuenta, Integer anio, Integer cuota, Boolean sinPago) {
        return cuentaCorrienteDAO.findBoletas(idCuenta, anio, cuota, sinPago);
    }

    /*
     * (non-Javadoc)
     * 
     * @see ar.com.jass.catastro.business.CuentaCorrienteService#emisionDeudaAnual(java.util.List)
     */
    @Override
    public void emisionDeudaAnual(List<Periodo> periodos) {
        Thread th = new Thread(batchEmisionDeuda);
        batchEmisionDeuda.run(periodos);
        th.start();
        // batchEmisionDeuda.run(periodos);
    }

    @Transactional
    @Override
    public void cambioEstadoProcesoGeneracionDeuda(Long idPeriodo, DeudaPeriodoStatus estado) {
        List<Periodo> periodos;

        if (idPeriodo == null) {
            Calendar now = Calendar.getInstance();
            periodos = cuentaCorrienteDAO.findPeriodos(now.get(Calendar.YEAR));
        } else {
            Periodo periodo = (Periodo) generalDAO.find(Periodo.class, idPeriodo);
            periodos = new ArrayList<Periodo>(Arrays.asList(periodo));
        }

        for (Periodo periodo : periodos) {

            if (!periodo.getEstado().equals(estado) && !periodo.getEstado().equals(DeudaPeriodoStatus.GENERADO)) {
                periodo.setEstado(estado);
                generalDAO.update(periodo);
                LOG.info("Cambiando estado de periodo {} a {}", periodo, estado);
            }
        }
    }

    @Transactional
    @Override
    public ResumenGeneracionDeuda resumenGeneracionCC(Periodo periodo) {
        /*
         * consulta de cuenta corriente en el periodo Cantidad de cuentas, totales periodo, totales
         * cuotas anuales Cuota min, max, avg
         */
        ResumenGeneracionDeuda r = cuentaCorrienteDAO.getResumenGeneracion(periodo);
        r.setPeriodo(periodo);
        return r;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * ar.com.jass.catastro.business.CuentaCorrienteService#ajusteCambioLinea(ar.com.jass.catastro.
     * model.Linea, ar.com.jass.catastro.model.Cuenta)
     */
    @Transactional
    @Override
    public List<CuentaCorriente> ajusteCuentaCorriente(Linea nuevaLinea, Cuenta cuenta, Integer codigoAdministrativo,
            Tramite tramite) {
        LOG.info("Ajuste Cuenta corriente, {} {} {}", nuevaLinea, cuenta, codigoAdministrativo);

        Periodo periodoVigente = cuentaCorrienteDAO.findPeriodoVigente();

        /*
         * Buscamos el periodo correspondiente a la data de la nueva linea
         */
        Integer mesDesde = DateHelper.getMesFecha(nuevaLinea.getFecha());
        Integer anioDesde = DateHelper.getAnioFecha(nuevaLinea.getFecha());
        // Periodo periodoDesde = cuentaCorrienteDAO.findPeriodo(anioDesde, mesDesde);

        AjusteDTO ajusteDTO = new AjusteDTO.Builder().setAnio(periodoVigente.getAnio()).setCuenta(cuenta)
                .setLinea(nuevaLinea).setCodAdministrativo(codigoAdministrativo)
                .setFechaVtoAjustes(periodoVigente.getFechaVto()).setOficio(tramite.getOficio()).build();

        // List<CuentaCorriente> ajustes = deudaGenerator.ajusteCuenta(periodoDesde, ajusteDTO,
        // periodoVigente);
        List<CuentaCorriente> ajustes = deudaGenerator.ajusteCuenta(anioDesde, mesDesde, ajusteDTO, periodoVigente);

        // APLICA INTERES ???
        return ajustes;
    }

    /*
     * (non-Javadoc)
     * 
     * @see ar.com.jass.catastro.business.CuentaCorrienteService#findPeriodos(java.lang. Integer)
     */
    @Transactional
    @Override
    public List<Periodo> findPeriodos(Integer anio, String... initialize) {
        List<Periodo> periodos = cuentaCorrienteDAO.findPeriodos(anio);
        HibernateHelper.initialize(periodos, initialize);
        return periodos;
    }
    
    @Transactional
    @Override
    public List<Periodo> findPeriodos(Calendar desde, String... initialize) {
        int anio = desde.get(Calendar.YEAR);
        int cuota = desde.get(Calendar.MONTH) + 1; // Meses 0..11
        List<Periodo> periodos = cuentaCorrienteDAO.findPeriodosDesde(anio, cuota);
        HibernateHelper.initialize(periodos, initialize);
        return periodos;
    }

    @Transactional
    @Override
    public Periodo findPeriodo(Date fecha) {
        Integer anio = DateHelper.getAnioFecha(fecha);
        Integer mes = DateHelper.getMesFecha(fecha);
        return findPeriodo(anio, mes);
    }

    @Transactional
    @Override
    public Periodo findPeriodo(Integer anio, Integer cuota) {
        return cuentaCorrienteDAO.findPeriodo(anio, cuota);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * ar.com.jass.catastro.business.CuentaCorrienteService#findPeriodos(ar.com.jass.catastro.model.
     * cuentacorriente.Periodo, ar.com.jass.catastro.model.cuentacorriente.Periodo)
     */
    @Transactional
    @Override
    public List<Periodo> findPeriodos(Periodo desde, Periodo hasta) {
        return cuentaCorrienteDAO.findPeriodos(desde, hasta);
    }

    @Transactional
    @Override
    public List<CuentaCorriente> findMovimientosCC(Cuenta cuenta, Integer anio, TipoMovimiento tipo) {
        return cuentaCorrienteDAO.findMovimientos(cuenta.getId(), anio, tipo);
    }

    @Transactional
    @Override
    public List<CuentaCorriente> findMovimientosCC(Cuenta cuenta, Integer anio, Integer cuota) {
        return cuentaCorrienteDAO.findMovimientos(cuenta.getId(), anio, cuota);
    }

    @Transactional
    @Override
    public CuentaCorriente findCuotaByCuentaId(Long idCuenta, Integer anio, Integer cuota, Integer ajuste,
            TipoMovimiento tipo) {
        return cuentaCorrienteDAO.findCuotaByCuentaId(idCuenta, anio, cuota, ajuste, tipo);
    }

    @Transactional
    @Override
    public CuentaCorriente findMovimientoCC(Long id) {
        CuentaCorriente cc = (CuentaCorriente) generalDAO.find(CuentaCorriente.class, id);
        // HibernateHelper.initialize(cc, "detalle", "fechaPago");
        HibernateHelper.initialize(cc, "detalle");
        return cc;
    }

    /*
     * public List<? extends Deuda> findDeudaCuotaMensual(Long idCuenta, Periodo periodo) { return
     * cuentaCorrienteDAO.findDeudaMensual(idCuenta, periodo.getAnio(), periodo.getCuota(),
     * Boolean.TRUE); }
     */

    /*
     * (non-Javadoc)
     * 
     * @see ar.com.jass.catastro.business.CuentaCorrienteService#getNroAjusteSiguiente(ar.com.jass.
     * catastro.model.cuentacorriente.CuentaCorriente)
     */
    public Integer getNroAjusteSiguiente(CuentaCorriente movimiento) {
        List<CuentaCorriente> movimientos = cuentaCorrienteDAO.findMovimientos(movimiento.getCuenta().getId(),
                movimiento.getAnio(), movimiento.getCuota());
        Integer ajuste = 0;
        for (CuentaCorriente cc : movimientos) {
            if (cc.getAjuste() > ajuste) {
                ajuste = cc.getAjuste();
            }
        }
        ajuste++;
        return ajuste;
    }

    public Integer getNroAjusteSiguiente(Cuenta cuenta, Periodo periodo) {
        List<CuentaCorriente> movimientos = cuentaCorrienteDAO.findMovimientos(cuenta.getId(), periodo.getAnio(),
                periodo.getCuota());
        Integer ajuste = 0;
        for (CuentaCorriente cc : movimientos) {
            if (cc.getAjuste() > ajuste) {
                ajuste = cc.getAjuste();
            }
        }
        ajuste++;
        return ajuste;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * ar.com.jass.catastro.business.CuentaCorrienteService#createFileDeuda(ar.com.jass.catastro.
     * model.cuentacorriente.Periodo, ar.com.jass.catastro.business.impl.FileType)
     */
    @Transactional
    @Override
    public void createFileDeuda(Periodo periodo, FileType tipo) {
        LOG.info("Creando registro de archivo de deuda. {} {}", periodo, tipo);
        FileDeuda fd = new FileDeuda();
        fd.setPeriodo(periodo);
        fd.setTipo(tipo);
        fd.setFilename(tipo.getDeudaFileName());

        generalDAO.save(fd);
    }

    @Transactional
    @Override
    public FileDeuda findFileDeuda(Long idFileDeuda) {
        return (FileDeuda) generalDAO.find(FileDeuda.class, idFileDeuda);
    }

    public CuentaCorrienteDAO getCuentaCorrienteDAO() {
        return cuentaCorrienteDAO;
    }

    public void setCuentaCorrienteDAO(CuentaCorrienteDAO cuentaCorrienteDAO) {
        this.cuentaCorrienteDAO = cuentaCorrienteDAO;
    }

    public Integer getCantidadPeriodosAnio() {
        return cantidadPeriodosAnio;
    }

    public void setCantidadPeriodosAnio(Integer cantidadPeriodosAnio) {
        this.cantidadPeriodosAnio = cantidadPeriodosAnio;
    }

    public DeudaGeneratorStrategy getDeudaGenerator() {
        return deudaGenerator;
    }

    public void setDeudaGenerator(DeudaGeneratorStrategy deudaGenerator) {
        this.deudaGenerator = deudaGenerator;
    }

    public GeneracionDeudaRunBatch getBatchEmisionDeuda() {
        return batchEmisionDeuda;
    }

    public void setBatchEmisionDeuda(GeneracionDeudaRunBatch batchEmisionDeuda) {
        this.batchEmisionDeuda = batchEmisionDeuda;
    }

}
