package ar.com.jass.catastro.batch.deuda.boleta;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import ar.com.jass.catastro.batch.deuda.BatchSemaphore;
import ar.com.jass.catastro.batch.deuda.BatchSemaphore.BATCH_PROCESS;
import ar.com.jass.catastro.model.cuentacorriente.DeudaPeriodoStatus;
import ar.com.jass.catastro.model.cuentacorriente.Periodo;

/**
 * Lanzador de JOB de generacion de boletas
 * 
 * Se crean las boletas para las cuentas en el periodo
 * 
 * @author msquarini
 *
 */
@Service
public class GeneracionBoletaBatch {

    private static final Logger LOG = LoggerFactory.getLogger(GeneracionBoletaBatch.class);

    @Autowired
    @Qualifier("jobLauncherAsync")
    private JobLauncher jobLauncher;
    
    @Autowired
    @Qualifier("boletas-JOB")
    private Job job;
    /*
     * @Autowired private CuentaCorrienteDAO ccDAO;
     */

    public void run(List<Periodo> periodos) {

        if (BatchSemaphore.puedoProcesar(BATCH_PROCESS.BOLETA)) {
            LOG.info("Lanzando: Job {} ", job);

            for (Periodo periodo : periodos) {
                try {
                    // Valiamos que este emitida la deuda para el periodo
                    if (periodo.getEstado().equals(DeudaPeriodoStatus.GENERADO)) {

                        JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
                        jobParametersBuilder.addLong("idPeriodo", periodo.getId());
                        jobParametersBuilder.addLong("anio", Long.valueOf(periodo.getAnio()));
                        jobParametersBuilder.addLong("cuota", Long.valueOf(periodo.getCuota()));
                        jobParametersBuilder.addLong("unique", System.currentTimeMillis());
                        JobParameters params = jobParametersBuilder.toJobParameters();
                        JobExecution execution = jobLauncher.run(getJob(), params);
                        BatchSemaphore.startProceso(BATCH_PROCESS.BOLETA);

                        LOG.info("Proceso: Job {} - Estado {} ", job, execution.getStatus());
                    } else {
                        throw new IllegalStateException("periodo.emision.notexists");
                    }
                } catch (Exception e) {
                    LOG.error("Error en lanzamiento de job {}", job, e);
                    BatchSemaphore.finProcesamiento(BATCH_PROCESS.BOLETA);
                }
            }
        } else {
            LOG.warn("Procesamiento de deuda en ejecucion");
            throw new IllegalStateException("emision.en.ejecucion");
        }
    }

    public JobLauncher getJobLauncher() {
        return jobLauncher;
    }

    public void setJobLauncher(JobLauncher jobLauncher) {
        this.jobLauncher = jobLauncher;
    }

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }
}
