package ar.com.jass.catastro.batch.deuda.file.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import ar.com.jass.catastro.batch.deuda.file.CuentaDTO;

/**
 * Mapeo de cuentas de consulta para generar files de deuda
 * @author msquarini
 *
 */
public class CuentaRowMapper implements RowMapper<CuentaDTO> {

    @Override
    public CuentaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        CuentaDTO bean = new CuentaDTO();
        bean.setId(rs.getLong("id"));        
        bean.setCuenta(rs.getString("cuenta"));
        bean.setDv(rs.getString("dv"));
        return bean;
    }
}
