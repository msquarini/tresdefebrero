package ar.com.jass.catastro.business.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import ar.com.jass.catastro.business.BusinessHelperRatificacion;
import ar.com.jass.catastro.business.ConsultaService;
import ar.com.jass.catastro.exceptions.BusinessException;
import ar.com.jass.catastro.model.Coeficiente;
import ar.com.jass.catastro.model.Parcela;
import ar.com.jass.catastro.model.ParcelaRatificacion;
import ar.com.jass.catastro.model.ResponsableCuentaRatificacion;
import ar.com.jass.catastro.model.TitularParcelaRatificacion;
import ar.com.jass.catastro.util.BusinessMessages;
import ar.com.jass.catastro.util.HibernateHelper;

@Service
public class BusinessHelperRatificacionImpl implements BusinessHelperRatificacion {

    @Autowired
    private ConsultaService consultaService;

    private Map<String, Coeficiente> coeficientes;

    @PostConstruct
    public void init() {
     }

    /**
     * Valida que todos los titulares en la lista tengan cuitcuil cargados!!!
     * 
     * @param titulares
     * @throws BusinessException
     */
    public void checkTitularesConCuit(List<TitularParcelaRatificacion> titulares) throws BusinessException {
        for (TitularParcelaRatificacion t : titulares) {
            if (t.getTitular().getCuitcuil() == null || t.getTitular().getCuitcuil().trim().length() == 0) {
                throw new BusinessException(BusinessMessages.TITULAR_SINCUIT);
            }
        }
    }

    /**
     * Valido que la titularidad este al 100% y todos presenten CUITs.
     * 
     * @param titulares
     * @throws BusinessException
     */
    public void checkTitulares(List<TitularParcelaRatificacion> titulares) throws BusinessException {
        double porcentaje = 0;
        for (TitularParcelaRatificacion t : titulares) {
            if (t.getTitular().getCuitcuil() == null || t.getTitular().getCuitcuil().trim().length() == 0) {
                throw new BusinessException(BusinessMessages.TITULAR_SINCUIT);
            }

            // Titulares activos!
            if (t.getHasta() == null) {
                porcentaje = porcentaje + t.getPorcentaje();
            }
        }
        if (porcentaje != 100d) {
            throw new BusinessException(BusinessMessages.TITULARIDAD_PORCENTAJE);
        }
    }
    
    public void checkResponsablesConCuit(List<ResponsableCuentaRatificacion> responsables) throws BusinessException {
        boolean responsableActivo = Boolean.FALSE;
        for (ResponsableCuentaRatificacion rc : responsables) {
            if (rc.getTitular().getCuitcuil() == null || rc.getTitular().getCuitcuil().trim().length() == 0) {
                throw new BusinessException(BusinessMessages.TITULAR_SINCUIT);
            }

            // Titulares activos!
            if (rc.getHasta() == null) {
                responsableActivo = Boolean.TRUE;
            }
        }
        if (!responsableActivo) {
            throw new BusinessException(BusinessMessages.SIN_RESPONSABLE_VIGENTE);
        }
    }
    
    
    /**
     * Valida que la parcela ratificacion que ha sido seleccionada para borrar no este como origen de otra parcela nueva, al momento de ser borrada de la lista a borrar
     * 
     * @param parcelaRatificacion
     * @throws BusinessException
     */
    public void checkParcelaEnOrigenes(ParcelaRatificacion parcelaRatificacion, List<ParcelaRatificacion> parcelas) throws BusinessException {
    	
        for (ParcelaRatificacion pr : parcelas) {

        	for(Parcela p :pr.getParcelasOrigen())
        	{
        		if(p.getId().equals(parcelaRatificacion.getOrigen().getId()))
        			throw new BusinessException(BusinessMessages.PARCELA_EN_ORIGENES);
        		
        	}
        }
        
    }


    
}
