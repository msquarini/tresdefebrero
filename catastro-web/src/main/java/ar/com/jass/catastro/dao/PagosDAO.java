package ar.com.jass.catastro.dao;

import java.util.Date;
import java.util.List;

import ar.com.jass.catastro.model.pagos.EstadoFilePagos;
import ar.com.jass.catastro.model.pagos.FilePago;

public interface PagosDAO {

    /**
     * Busqueda de registro de archivo de pagos por el hash del archivo
     * 
     * @param hash
     * @return
     */
    public FilePago findFilePago(String hash);
    
    public List<FilePago> getFilePagos(Date from, Date to, EstadoFilePagos[] estados);
    
    public List<FilePago> getFilePagos(EstadoFilePagos[] estados);
}