package ar.com.jass.catastro.business;

import java.util.List;

import ar.com.jass.catastro.exceptions.BusinessException;
import ar.com.jass.catastro.model.Circunscripcion;
import ar.com.jass.catastro.model.ResponsableCuenta;
import ar.com.jass.catastro.model.TitularParcela;

public interface BusinessHelper {

    public void checkResponsablesConCuit(List<ResponsableCuenta> responsables) throws BusinessException;
    
	public void checkTitularesConCuit(List<TitularParcela> titulares) throws BusinessException;

	/**
	 * Check si estan la titularidad a 100% y los titulares presenten CUITs
	 * 
	 * @param titulares
	 * @throws BusinessException
	 */
	public void checkTitulares(List<TitularParcela> titulares) throws BusinessException;

	public List<Circunscripcion> getCircunscripciones();
}
