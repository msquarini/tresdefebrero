package ar.com.jass.catastro.business.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.com.jass.catastro.business.BusinessHelper;
import ar.com.jass.catastro.business.ConsultaService;
import ar.com.jass.catastro.exceptions.BusinessException;
import ar.com.jass.catastro.model.Circunscripcion;
import ar.com.jass.catastro.model.Coeficiente;
import ar.com.jass.catastro.model.ResponsableCuenta;
import ar.com.jass.catastro.model.TitularParcela;
import ar.com.jass.catastro.util.BusinessMessages;

@Service
public class BusinessHelperImpl implements BusinessHelper {

    @Autowired
    private ConsultaService consultaService;

    private List<Circunscripcion> circunscripciones = null;

    private Map<String, Coeficiente> coeficientes;

    @PostConstruct
    public void init() {
        circunscripciones = (List<Circunscripcion>) consultaService.findAll(Circunscripcion.class);
    }

    /**
     * Valida que todos los titulares en la lista tengan cuitcuil cargados!!!
     * 
     * @param titulares
     * @throws BusinessException
     */
    public void checkTitularesConCuit(List<TitularParcela> titulares) throws BusinessException {
        for (TitularParcela t : titulares) {
            if (t.getTitular().getCuitcuil() == null || t.getTitular().getCuitcuil().trim().length() == 0) {
                throw new BusinessException(BusinessMessages.TITULAR_SINCUIT);
            }
        }
    }

    /**
     * Valido que la titularidad este al 100% y todos presenten CUITs.
     * 
     * @param titulares
     * @throws BusinessException
     */
    public void checkTitulares(List<TitularParcela> titulares) throws BusinessException {
        double porcentaje = 0;
        for (TitularParcela t : titulares) {
            if (t.getTitular().getCuitcuil() == null || t.getTitular().getCuitcuil().trim().length() == 0) {
                throw new BusinessException(BusinessMessages.TITULAR_SINCUIT);
            }

            // Titulares activos!
            if (t.getHasta() == null) {
                porcentaje = porcentaje + t.getPorcentaje();
            }
        }
        if (porcentaje != 100d) {
            throw new BusinessException(BusinessMessages.TITULARIDAD_PORCENTAJE);
        }
    }
    
    public void checkResponsablesConCuit(List<ResponsableCuenta> responsables) throws BusinessException {
        boolean responsableActivo = Boolean.FALSE;
        for (ResponsableCuenta rc : responsables) {
            if (rc.getTitular().getCuitcuil() == null || rc.getTitular().getCuitcuil().trim().length() == 0) {
                throw new BusinessException(BusinessMessages.TITULAR_SINCUIT);
            }

            // Titulares activos!
            if (rc.getHasta() == null) {
                responsableActivo = Boolean.TRUE;
            }
        }
        if (!responsableActivo) {
            throw new BusinessException(BusinessMessages.SIN_RESPONSABLE_VIGENTE);
        }
    }

    public List<Circunscripcion> getCircunscripciones() {
        return circunscripciones;
    }
    
}
