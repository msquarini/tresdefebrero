package ar.com.jass.catastro.model;

/**
 * Tipos de tramites sobre una parcela
 * 
 * @author msquarini
 *
 */
public enum TipoTramite {

    SUB("Subdivision", 1) {
    },
    UNI("Unificacion", 2), INS("Inscripcion", 3), DES("Desdoblamiento", 4), TIT("Titulares", 5), RES("Responsable",
            6), LIN("Ajuste Linea", 7), RAT("Ratificacion", 8);

    private String descripcion;
    private Integer codigo;

    TipoTramite(String descripcion, Integer codigo) {
        this.descripcion = descripcion;
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }
}
