package ar.com.jass.catastro.business;

import java.util.Date;
import java.util.List;

import ar.com.jass.catastro.model.Cuenta;
import ar.com.jass.catastro.model.EstadoTramite;
import ar.com.jass.catastro.model.IParcela;
import ar.com.jass.catastro.model.Nomenclatura;
import ar.com.jass.catastro.model.Parcela;
import ar.com.jass.catastro.model.Titular;
import ar.com.jass.catastro.model.Tramite;
import ar.com.jass.catastro.model.temporal.LineaTemporal;

/**
 * @author msquarini
 *
 */
public interface ConsultaService {

    List<?> findAll(Class<?> clazz);

    /**
     * Busqueda de tramite por ID
     * 
     * @param id
     * @param initialize
     * @return
     */
    Tramite findTramite(Long id, String... initialize);

    /**
     * Busqueda de tramites de una determinada cuenta
     * 
     * @param cuentaId
     * @return
     */
    List<Tramite> findTramitesByCuenta(Cuenta cuenta, String... initialize);

    Cuenta findCuenta(String cuenta, String dv, String... initialize);

    List<Cuenta> findCuentasLike(String cuenta);

    Cuenta findCuentaById(Long id, String... initialize);

    /**
     * Obtiene titular por Id
     * 
     * @param id
     * @param initialize
     * @return
     */
    public Titular findTitularById(Long id, String... initialize);

    /**
     * Busqueda de titulares a partir de datos en entidad titular
     * 
     * @param titular
     * @return
     */
    List<Titular> findTitulares(Titular titular);

    List<Parcela> findParcelas(Nomenclatura nomenclatura, String... initialize);

    Parcela findParcelaById(Long id, String... initialize);

    Parcela findParcelaByPartida(Integer partida);

    /**
     * Retorna true si existe una parcela con la misma nomenclatura en la base de datos
     * 
     * @param parcela
     * @return
     */
    public boolean exists(IParcela parcela);

    public List<Tramite> getTramites(Date from, Date to, EstadoTramite estado, String usuario);

    /**
     * Buscar linea temporal de un tramite de ajuste
     * 
     * @param tramite
     * @return
     */
    public LineaTemporal findLineaTemporal(Tramite tramite);
}
