package ar.com.jass.catastro.model.pagos.pmc;

import java.math.BigDecimal;
import java.util.Date;

import ar.com.jass.catastro.model.pagos.FilePago;
import ar.com.jass.catastro.model.pagos.FilePagosRecord;
import ar.com.jass.catastro.model.pagos.PagoTrailerRecord;

/**
 * Trailer de file de pagos de Pago Mis Cuentas
 * @author msquarini
 *
 */
public class PMCTrailer extends PagoTrailerRecord implements FilePagosRecord {

	Date fecha;
	Integer cantidadRegistros;
	BigDecimal total;

	@Override
	public void complete(FilePago filePago) {
		filePago.setFileTrailer(getLine());
		filePago.setCantidadRegistros(this.cantidadRegistros);
		filePago.setTotal(this.total);
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Integer getCantidadRegistros() {
		return cantidadRegistros;
	}

	public void setCantidadRegistros(Integer cantidadRegistros) {
		this.cantidadRegistros = cantidadRegistros;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

}