package ar.com.jass.catastro.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Tipificacion de destinos de parcela de dominio municipal
 * 
 * @author MESquarini
 * 
 */
@Entity
@Table(name = "Destino")
public class Destino implements Serializable {

    private Long id;
    private String destino;

    public Destino() {

    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_destino_gen")
    @SequenceGenerator(name = "seq_destino_gen", sequenceName = "seq_destino", allocationSize = 1)
    @Column(name = "id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "destino")
    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }
}
