package ar.com.jass.catastro.batch.mapper.da;

import java.util.Calendar;

import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;

import ar.com.jass.catastro.model.da.DADetail;
import ar.com.jass.catastro.model.da.DANovedad;

/**
 * Mapeador de entradas de archivo de DA del banco Frances
 * @author msquarini
 *
 */
public class ProvinciaDAFieldMapper implements FieldSetMapper<DADetail> {
	
	public DADetail mapFieldSet(FieldSet fs) {

		if (fs == null) {
			return null;
		}

		DADetail detail = new DADetail("Frances");

		detail.setFecha(fs.readDate("fecha"));
		detail.setCuenta(fs.readString("cuenta"));
		detail.setDv(fs.readString("dv"));	
		detail.setCbu(fs.readString("cbu"));
		
		String novedad = fs.readString("novedad");	
		if ("2".equals(novedad)) {
			detail.setNovedad(DANovedad.ALTA);	
		} else if ("1".equals(novedad) || "7".equals(novedad)) {
			detail.setNovedad(DANovedad.BAJA);
		} else {
			detail.setNovedad(DANovedad.MODIFICACION);
		}
			
		detail.setLine(fs.readString("linea"));

		return detail;
	}

}