package ar.com.jass.catastro.model.cuentacorriente;

/**
 * Tipo de cuenta corriente (Debito/Credito)
 * 
 * @author msquarini
 *
 */
public enum TipoMovimiento {

    D {
        @Override
        public Boolean isDebito() {            
            return true;
        }
    },
    C {
        @Override
        public Boolean isDebito() {
            return false;
        }
    };

    public abstract Boolean isDebito();
}
