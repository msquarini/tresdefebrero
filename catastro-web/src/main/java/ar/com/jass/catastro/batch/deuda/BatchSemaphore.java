package ar.com.jass.catastro.batch.deuda;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Semaforo para procesos batchs generico
 * 
 * @author msquarini
 *
 */
public class BatchSemaphore {

    public enum BATCH_PROCESS {
        DEUDA, BOLETA, DEUDA_FILES
    }
    
    private static final Logger LOG = LoggerFactory.getLogger(BatchSemaphore.class);

    private static Map<BATCH_PROCESS, Integer> semaphores = new HashMap<BATCH_PROCESS, Integer>();
    
    public static boolean puedoProcesar(BATCH_PROCESS process) {
        Integer processes = semaphores.get(process);
        if (processes == null || processes == 0) {
            return true;
        }
        return false;
    }

    public static synchronized void finProcesamiento(BATCH_PROCESS process) {
        endProceso(process);
        Integer procesos = semaphores.get(process);
        LOG.info("{} procesos en ejecucion: {}, estado: {}", process, procesos);
    }

    public static synchronized void startProceso(BATCH_PROCESS process) {
        Integer processes = semaphores.get(process);
        if (processes == null) {
            semaphores.put(process, 1);
        } else {
            semaphores.put(process, processes + 1);
        }
    }

    private static synchronized void endProceso(BATCH_PROCESS process) {
        Integer processes = semaphores.get(process);
        if (processes != null) {
            semaphores.put(process, processes - 1);
        }
    }

}
