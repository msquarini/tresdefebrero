package ar.com.jass.catastro.dao;

import java.io.Serializable;
import java.util.List;

import ar.com.jass.catastro.model.Nomenclatura;
import ar.com.jass.catastro.model.Parcela;
import ar.com.jass.catastro.model.Titular;

public interface ParcelaDAO extends GenericDAO<Parcela, Serializable> {
	
	List<Parcela> findByNomenclatura(Nomenclatura nomenclatura);
	
	List<Parcela> findByTitular(Titular titular);
	
	List<Parcela> findByPartida(Integer partida);
	
	List<Parcela> findByCuenta(String cuenta, String dv);
}