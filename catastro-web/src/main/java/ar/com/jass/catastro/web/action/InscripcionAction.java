package ar.com.jass.catastro.web.action;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import ar.com.jass.catastro.business.BusinessHelper;
import ar.com.jass.catastro.business.CatastroService;
import ar.com.jass.catastro.exceptions.BusinessException;
import ar.com.jass.catastro.model.Nomenclatura;
import ar.com.jass.catastro.model.Parcela;
import ar.com.jass.catastro.model.ResponsableCuenta;
import ar.com.jass.catastro.model.TipoTramite;
import ar.com.jass.catastro.model.Titular;
import ar.com.jass.catastro.model.TitularParcela;
import ar.com.jass.catastro.model.Tramite;
import ar.com.jass.catastro.web.commons.WebConstants;

/**
 * Action de gestion de primera inscripcion de parcela
 * 
 * @author msquarini
 *
 */
public class InscripcionAction extends GridBaseAction<Titular> {

    private static final long serialVersionUID = 7353477345330099548L;

    private static final Logger LOG = LoggerFactory.getLogger("InscripcionAction");

    @Autowired
    private CatastroService catastroService;

    @Autowired
    private BusinessHelper businessHelper;

    /**
     * Datos de nueva parcela (formulario)
     */
    private Parcela nuevaParcela;

    private Nomenclatura nomenclatura;

    /**
     * Tipo de tramite de unificacion
     */
    private Integer tipo;

    /**
     * Tipo de tramite
     */
    private Tramite tramite;

    /*
     * (non-Javadoc)
     * 
     * @see com.opensymphony.xwork2.ActionSupport#input()
     */
    public String input() {
        initCustomData();
        nuevaParcela = new Parcela();
        setInSession(WebConstants.TITULARES_ADD, new HashSet<Titular>());
        setInSession(WebConstants.RESPONSABLES_ADD, new HashSet<ResponsableCuenta>());
        return "input";
    }

    /**
     * Procesamiento
     * 
     * @return
     */
    public String process() {
        LOG.info("procesando Primera Inscripcion");

        try {
            tramite = new Tramite();
            tramite.setTipo(TipoTramite.INS);
            nuevaParcela.getNomenclatura().setCircunscripcion(getNomenclatura().getCircunscripcion());
            nuevaParcela.getNomenclatura().setSeccion(getNomenclatura().getSeccion());

            List<TitularParcela> titulares = new ArrayList<TitularParcela>(
                    (Set<TitularParcela>) getFromSession(WebConstants.TITULARES_ADD));
            businessHelper.checkTitularesConCuit(titulares);

            // Verificar que la nomenclatura no existe en la BD
            if (getConsultaService().exists(nuevaParcela)) {
                LOG.warn("Nomenclatura existente en BD, {}", nuevaParcela);
                addActionError("Nomencla_exists");
                return "process_error";
            }

            tramite = catastroService.inscripcion(tramite, nuevaParcela, titulares);

        } catch (BusinessException be) {
            LOG.error("error de negocio en procesamiento de primera inscripcion", be);
            addActionError(getText(be.getMessage()));
            return "process_error";
        } catch (Exception e) {
            LOG.error("error en procesamiento de primera inscripcion", e);
            addActionError(getText("inscripcion_process_error"));
            return "process_error";
        }
        // Para chequear error en pantalla rapidamente!!
        /*
         * if (getOrigen().getNumero() > 5) { addActionError(getText("unificacion_process_error"));
         * return "process_error"; }
         */
        return "success";
    }

    @SuppressWarnings("unchecked")
    public String getData() {
        LOG.info("getData en inscripcion!!!!");
        try {
            List<Titular> data = new ArrayList<Titular>((Set<Titular>) getFromSession(WebConstants.TITULARES_ADD));
            setData(data);
        } catch (Exception e) {
            LOG.error("error en getData titulares primera inscripcion", e);
        }
        return "dataJSON";
    }

    public CatastroService getCatastroService() {
        return catastroService;
    }

    public void setCatastroService(CatastroService catastroService) {
        this.catastroService = catastroService;
    }

    public Parcela getNuevaParcela() {
        return nuevaParcela;
    }

    public void setNuevaParcela(Parcela nuevaParcela) {
        this.nuevaParcela = nuevaParcela;
    }

    public Integer getTipo() {
        return tipo;
    }

    public void setTipo(Integer tipo) {
        this.tipo = tipo;
    }

    public Tramite getTramite() {
        return tramite;
    }

    public void setTramite(Tramite tramite) {
        this.tramite = tramite;
    }

    public Nomenclatura getNomenclatura() {
        return nomenclatura;
    }

    public void setNomenclatura(Nomenclatura nomenclatura) {
        this.nomenclatura = nomenclatura;
    }

}
