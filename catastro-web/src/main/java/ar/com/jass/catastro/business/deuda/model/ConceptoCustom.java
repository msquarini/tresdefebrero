package ar.com.jass.catastro.business.deuda.model;

import ar.com.jass.catastro.business.deuda.ConceptoCuotaDTO;

public class ConceptoCustom extends AbstractConceptoCuota {

    @Override
    public void crearConceptoCuota(ConceptoCuotaDTO conceptoCuota) {
        // noop
    }

    public static ConceptoCustom AJUSTE_LINEA = new ConceptoCustom("AJUSTE_LINEA", "Ajuste de valuación");
    public static ConceptoCustom PAGO_EXISTENTE = new ConceptoCustom("PAGO_EXISTENTE", "Pago existente");
    
    /**
     * Aplicacion de tope en la generacion de deuda.
     * Solo a cuenta que tengan la marca y periodos mayor al uno.
     */
    public static ConceptoCustom TOPE = new ConceptoCustom("TOPE", "Aplicación de tope al cálculo de deuda");
    public static ConceptoCustom PAGO_ANUAL = new ConceptoCustom("PAGO_ANUAL", "Pago anual");
    public static ConceptoCustom PAGO_ANUAL_BONIFICACION = new ConceptoCustom("PAGO_ANUAL_BONIF", "Bonificacion por pago anual");
    
    public ConceptoCustom(String concepto, String descripcion) {
        super();
        setCodigo(concepto);
        setDescription(descripcion);
    }
}
