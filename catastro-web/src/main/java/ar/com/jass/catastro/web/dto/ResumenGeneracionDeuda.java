package ar.com.jass.catastro.web.dto;

import java.math.BigDecimal;

import ar.com.jass.catastro.model.cuentacorriente.Periodo;

/**
 * DTO informacion de generacion de deuda en cuenta corriente para un periodo
 * @author msquarini
 *
 */
public class ResumenGeneracionDeuda {

    private Periodo periodo;

    private Integer cantidadCC;
    private BigDecimal minImporte;
    private BigDecimal maxImporte;
    private BigDecimal avgImporte;

    private Integer cantidadCA;
    private BigDecimal minImporteCA;
    private BigDecimal maxImporteCA;
    private BigDecimal avgImporteCA;
    
    public Integer getCantidadCC() {
        return cantidadCC;
    }

    public void setCantidadCC(Integer cantidadCC) {
        this.cantidadCC = cantidadCC;
    }

    public BigDecimal getMinImporte() {
        return minImporte;
    }

    public void setMinImporte(BigDecimal minImporte) {
        this.minImporte = minImporte;
    }

    public BigDecimal getMaxImporte() {
        return maxImporte;
    }

    public void setMaxImporte(BigDecimal maxImporte) {
        this.maxImporte = maxImporte;
    }

    public BigDecimal getAvgImporte() {
        return avgImporte;
    }

    public void setAvgImporte(BigDecimal avgImporte) {
        this.avgImporte = avgImporte;
    }

    public Periodo getPeriodo() {
        return periodo;
    }

    public void setPeriodo(Periodo periodo) {
        this.periodo = periodo;
    }

	public Integer getCantidadCA() {
		return cantidadCA;
	}

	public void setCantidadCA(Integer cantidadCA) {
		this.cantidadCA = cantidadCA;
	}

	public BigDecimal getMinImporteCA() {
		return minImporteCA;
	}

	public void setMinImporteCA(BigDecimal minImporteCA) {
		this.minImporteCA = minImporteCA;
	}

	public BigDecimal getMaxImporteCA() {
		return maxImporteCA;
	}

	public void setMaxImporteCA(BigDecimal maxImporteCA) {
		this.maxImporteCA = maxImporteCA;
	}

	public BigDecimal getAvgImporteCA() {
		return avgImporteCA;
	}

	public void setAvgImporteCA(BigDecimal avgImporteCA) {
		this.avgImporteCA = avgImporteCA;
	}

}
