package ar.com.jass.catastro.batch.deuda;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OLDEmisionDeudaSemaphore {

    private static final Logger LOG = LoggerFactory.getLogger(OLDEmisionDeudaSemaphore.class);
    
    private static Boolean procesando = Boolean.FALSE;

    private static int procesos = 0;

    public static boolean puedoProcesar() {
        return !procesando;
    }

    public static synchronized void finProcesamiento() {
        endProceso();
        LOG.info("procesos en ejecucion: {}, estado: {}", procesos, procesando);
        if (procesos == 0) {
            procesando = Boolean.FALSE;
        }
    }

    public static synchronized void startProceso() {
        procesos++;
        procesando = Boolean.TRUE;
    }

    private static synchronized void endProceso() {
        procesos--;
    }

}
