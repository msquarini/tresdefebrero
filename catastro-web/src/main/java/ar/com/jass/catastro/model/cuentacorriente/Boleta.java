package ar.com.jass.catastro.model.cuentacorriente;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import ar.com.jass.catastro.model.Cuenta;
import ar.com.jass.catastro.model.EntityBase;
import ar.com.jass.catastro.model.pagos.Pago;

/**
 * Boleta de pago de deudas La misma agrupa los movimientos de deuda a pagar
 * 
 * @author msquarini
 *
 */
@Entity
@Table(name = "Boleta")
public class Boleta extends EntityBase implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private Long id;

    /**
     * Cuenta a la que pertenece
     */
    private Cuenta cuenta;

    /**
     * Fecha de vencimiento
     */
    private Date fechaVto;

    /**
     * Segunda fecha de vencimiento
     */
    private Date fechaVto2;

    /**
     * Fecha de pago de la boleta. Inicialmente en null (sin pago)
     * 
     */
    private Date fechaPago;

    /**
     * Referencia al pago de la boleta. Inicialmente en null (sin pago)
     */
    private Pago pago;

    /**
     * Total de la boleta (Suma de debitos - creditos) para el periodo
     */
    private BigDecimal importe;

    /**
     * Anio de la boleta
     */
    private Integer anio;

    /**
     * Cuota de la boleta
     */
    private Integer cuota;

    /**
     * Lista de detalle de la boleta
     */
    private List<BoletaDetalle> detalle;

    /**
     * Indica si se trata de una boleta de pago anual
     */
    private Boolean cuotaAnual;
    private Boolean aplicaBonificacion = Boolean.FALSE;
    private BigDecimal porcentajeBonificacion;
    private BigDecimal importeBonificacion;

    /**
     * Constructor default de movimiento de debito
     */
    public Boleta() {
        this.importe = BigDecimal.ZERO;
        this.importeBonificacion = BigDecimal.ZERO;
        this.porcentajeBonificacion = BigDecimal.ZERO;
        this.detalle = new ArrayList<BoletaDetalle>();
        this.cuotaAnual = Boolean.FALSE;
    }

    /**
     * Boleta para el pago anual
     * 
     * @param cuenta
     * @param periodo
     * @param cuotaAnual
     */
    public Boleta(Cuenta cuenta, Periodo periodo, Boolean cuotaAnual) {
        this();
        this.cuenta = cuenta;
        this.anio = periodo.getAnio();
        this.cuota = periodo.getCuota();
        this.fechaVto = periodo.getFechaVto();
        this.fechaVto2 = periodo.getFechaSegundoVto();
        this.cuotaAnual = cuotaAnual;
        if (cuotaAnual) {
            this.porcentajeBonificacion = BigDecimal.valueOf(periodo.getPorcentajeBonificacion());
            this.aplicaBonificacion = Boolean.TRUE;
        }
    }

    /**
     * Boleta para un periodo determinado
     * 
     * @param cuenta
     * @param periodo
     * @param movimientos
     */
    public Boleta(Cuenta cuenta, Periodo periodo, List<CuentaCorriente> movimientos) {
        this();
        this.cuenta = cuenta;
        this.anio = periodo.getAnio();
        this.cuota = periodo.getCuota();
        this.fechaVto = periodo.getFechaVto();
        this.fechaVto2 = periodo.getFechaSegundoVto();

        detalle.add(new BoletaDetalle(periodo, movimientos, this));
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_boleta_gen")
    @SequenceGenerator(name = "seq_boleta_gen", sequenceName = "seq_boleta", allocationSize = 1)
    @Column(name = "id_boleta")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_cuenta")
    public Cuenta getCuenta() {
        return cuenta;
    }

    public void setCuenta(Cuenta cuenta) {
        this.cuenta = cuenta;
    }

    @Column(name = "fecha_vto")
    public Date getFechaVto() {
        return fechaVto;
    }

    public void setFechaVto(Date fechaVto) {
        this.fechaVto = fechaVto;
    }

    @Column(name = "fecha_vto2")
    public Date getFechaVto2() {
        return fechaVto2;
    }

    public void setFechaVto2(Date fechaVto2) {
        this.fechaVto2 = fechaVto2;
    }

    @Column(name = "importe")
    public BigDecimal getImporte() {
        return importe;
    }

    public void setImporte(BigDecimal importe) {
        this.importe = importe;
    }

    @Column(name = "anio")
    public Integer getAnio() {
        return anio;
    }

    public void setAnio(Integer anio) {
        this.anio = anio;
    }

    @Column(name = "cuota")
    public Integer getCuota() {
        return cuota;
    }

    public void setCuota(Integer cuota) {
        this.cuota = cuota;
    }

    @Column(name = "fecha_pago", nullable = true)
    public Date getFechaPago() {
        return fechaPago;
    }

    public void setFechaPago(Date fechaPago) {
        this.fechaPago = fechaPago;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_pago", nullable = true)
    public Pago getPago() {
        return pago;
    }

    public void setPago(Pago pago) {
        this.pago = pago;
    }

    @OneToMany(mappedBy = "boleta")
    @Cascade(CascadeType.ALL)
    public List<BoletaDetalle> getDetalle() {
        return detalle;
    }

    public void setDetalle(List<BoletaDetalle> detalle) {
        this.detalle = detalle;
    }

    /**
     * Creamos el concepto en la boleta, si los movimientos del periodo presentan un saldo negativo
     * para la cuenta
     * 
     * @param movimientos
     */
    public void addDetalle(Periodo periodo, List<CuentaCorriente> movimientos) {
        BoletaDetalle bd = new BoletaDetalle(periodo, movimientos, this);
        // Validamos si el importe del concepto es > 0
        if (bd.getImporte().compareTo(BigDecimal.ZERO) > 0) {
            getDetalle().add(bd);
        }
    }

    @Column(name = "cuota_anual", nullable = false)
    public Boolean getCuotaAnual() {
        return cuotaAnual;
    }

    public void setCuotaAnual(Boolean cuotaAnual) {
        this.cuotaAnual = cuotaAnual;
    }

    @Column(name = "aplica_bonif")
    public Boolean getAplicaBonificacion() {
        return aplicaBonificacion;
    }

    public void setAplicaBonificacion(Boolean aplicaBonificacion) {
        this.aplicaBonificacion = aplicaBonificacion;
    }

    @Column(name = "porc_bonif")
    public BigDecimal getPorcentajeBonificacion() {
        return porcentajeBonificacion;
    }

    public void setPorcentajeBonificacion(BigDecimal porcentajeBonificacion) {
        this.porcentajeBonificacion = porcentajeBonificacion;
    }

    @Column(name = "importe_bonif")
    public BigDecimal getImporteBonificacion() {
        return importeBonificacion;
    }

    public void setImporteBonificacion(BigDecimal importeBonificacion) {
        this.importeBonificacion = importeBonificacion;
    }

    /**
     * Aplicar los totales de la boleta
     */
    @Transient
    public void calcularTotales() {
        BigDecimal parcial = BigDecimal.ZERO;
        for (BoletaDetalle bd : getDetalle()) {
            // Si el detalle es mayor que 0 entonces se suma (si es menor que cero no se aplica)
            if (bd.getImporte().compareTo(BigDecimal.ZERO) > 0) {
                parcial = parcial.add(bd.getImporte());
            }
        }
        if (aplicaBonificacion) {
            this.importeBonificacion = parcial.multiply(getPorcentajeBonificacion());
        }
        this.setImporte(parcial.subtract(importeBonificacion));
    }

    /**
     * Procesar un poco, generando los movimientos de credito para cada detalle que compone la
     * boleta
     * 
     * @param pago
     * @return
     */
    @Transient
    public List<CuentaCorriente> procesarPago(Pago pago) {
        Boolean pagoDoble = false;
        if (getPago() != null) {
            pagoDoble = true;
        } else {
            setFechaPago(pago.getFechaPago());
            setPago(pago);
        }
        List<CuentaCorriente> creditos = new ArrayList<>();
        for (BoletaDetalle bd : getDetalle()) {
            creditos.addAll(bd.procesarPago(pago, pagoDoble, getCuotaAnual(), getPorcentajeBonificacion()));
        }

        return creditos;
    }

    @Override
    public String toString() {
        return "Boleta [id=" + id + ", cuenta=" + cuenta + ", fechaVto=" + fechaVto + ", fechaVto2=" + fechaVto2
                + ", fechaPago=" + fechaPago + ", pago=" + pago + ", importe=" + importe + ", anio=" + anio + ", cuota="
                + cuota + ", cuotaAnual=" + cuotaAnual + ", aplicaBonificacion=" + aplicaBonificacion
                + ", porcentajeBonificacion=" + porcentajeBonificacion + ", importeBonificacion=" + importeBonificacion
                + "]";
    }

}