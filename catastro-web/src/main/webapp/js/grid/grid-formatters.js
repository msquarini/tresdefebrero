/*
 * Funciones de formato de columnas en grillas
 */

/**
 * Agrega los campos de orden y remueve el default
 * 
 * @param request
 * @returns
 */
function sortRequest(request) {
	for ( var key in request.sort) {
		request.sortColumn = key;
		request.sortOrder = request.sort[key];
	}

	// return JSON.stringify(request);
	delete request.sort;
	return request;
}

/**
 * Paddind a izquiera N veces del caracter 0 \xa0 es espacio en blanco
 * 
 * @param str
 * @param max
 * @returns
 */
function pad(str, max) {
	if (str) {
		str = str.toString();
		// return str.length < max ? pad("\xa0" + str, max) : str;
		return str.length < max ? pad("0" + str, max) : str;
	} else {
		return Array(max + 1).join("0");
	}
}

function fmt_nomenclatura(row) {
	return "<small><strong>C:</strong>"
			+ pad(row.nomenclatura.circunscripcion, 3) + "<strong> S:</strong>"
			+ pad(row.nomenclatura.seccion, 3) + "<strong> Fr:</strong>"
			+ pad(row.nomenclatura.fraccion, 3) + "<strong> FrL:</strong>"
			+ pad(row.nomenclatura.fraccionLetra, 3) + "<strong> Mz:</strong>"
			+ pad(row.nomenclatura.manzana, 3) + "<strong> MzL:</strong>"
			+ pad(row.nomenclatura.manzanaLetra, 3) + "<strong> Pc:</strong>"
			+ pad(row.nomenclatura.parcela, 3) + "<strong> PcL:</strong>"
			+ pad(row.nomenclatura.parcelaLetra, 3) + "<strong> UF:</strong>"
			+ pad(row.nomenclatura.unidadFuncional, 4) + "</small>";
}

function fmt_ok_image(bool, iconTrue, iconFalse, msgTrue, msgFalse) {
	var html = "<i class='" + iconTrue
			+ "' aria-hidden='true'  data-toggle='tooltip'  title='" + msgTrue
			+ "'></i>";

	if (!bool && iconFalse) {
		html = "<i class='" + iconFalse
				+ "' aria-hidden='true'  data-toggle='tooltip'  title='"
				+ msgFalse + "'></i>";
	} else if (!bool) {
		return "";
	}
	return html;
}

function fmt_status(msg) {
	// Si es != null entonces realizamos el trim para las comparaciones
	if (msg) {
		msg = msg.trim();
	}

	if (msg == 'ERROR') {
		return "<span class='label label-danger'>" + msg + "</span>";
	} else if ([ 'GENERADO', 'PROCESADO', 'FINALIZADO' ].includes(msg)) {
		return "<span class='label label-success'>" + msg + "</span>";
	} else if ([ 'PROCESANDO' ].includes(msg)) {
		return "<span class='label label-warning'>" + msg + "</span>";
	} else {
		return "<span class='label label-info'>" + msg + "</span>";
	}
}

function fmt_boleta(msg) {
	// Si es != null entonces realizamos el trim para las comparaciones
	if (msg) {
		msg = msg.trim();
	}

	if (msg == 'ERROR') {
		return "<span class='label label-danger'>" + msg + "</span>";
	} else if ([ 'GENERADO', 'PROCESADO', 'FINALIZADO' ].includes(msg)) {
		return "<span class='label label-success'>" + msg + "</span>";
	} else if ([ 'PROCESANDO' ].includes(msg)) {
		return "<span class='label label-warning'>" + msg + "</span>";
	} else {
		return "<span class='label label-info'>" + msg + "</span>";
	}
}

function fmt_cc_tipo_mov(tipo) {
	if (tipo.trim() == 'C') {
		return "<span class='label label-success'>" + tipo + "</span>";
	} else {
		return "<span class='label label-danger'>" + tipo + "</span>";
	}
}

function fmt_boolean(value) {
	if (value) {
		return "<i class=\"fa fa-check-square-o\" aria-hidden=\"true\"></i>";
	} else {
		return "<i class=\"fa fa-square-o\" aria-hidden=\"true\"></i>";
	}
}

// ABM Titular GRILLA
function fmt_accion_titular_grilla(row) {
	return "<button onclick=\"sd_accion_submit('"
			+ row.uuidString
			+ "', "
			+ ((row.cuitcuil) ? true : false)
			+ ", true);\" type=\"button\" class=\"btn btn-xs btn-default command-edit\" data-row-id=\""
			+ row.uuidString
			+ "\"><span class=\"icon glyphicon glyphicon-pencil\"></span></button> ";
}

// Descarga de files de deudas para pmc y link
function cmd_download_file_deuda(row) {
	return "<button onclick=\"download('"
			+ row.id
			+ "');\" type=\"button\" class=\"btn btn-xs btn-default command-edit\" data-row-id=\""
			+ row.id + "\"><span class=\"fa fa-download\"></span></button> ";
}

// Fila y flag si permite edicion
function fmt_acction_subdivision_grilla(row, edit) {
	if (edit) {
		var uuid = row.uuidString;
		return "<button onclick=\"sd_accion_submit('"
				//+ row.partid
				+ uuid 
				+ "', true);\" type=\"button\" class=\"btn btn-xs btn-default command-edit\" data-row-id=\""
				//+ row.partida
				+ uuid
				+ "\"><span class=\"icon glyphicon glyphicon-pencil\"></span></button> "
				+ "<button onclick=\"sd_accion_submit('"
				// + row.partida
				+ uuid
				+ "', false);\"  type=\"button\" class=\"btn btn-xs btn-default command-delete\" data-row-id=\""
				// + row.partida
				+ uuid
				+ "\"><span class=\"icon glyphicon glyphicon-trash\"></span></button>";
	} else {
		return "<button onclick=\"sd_info_parcela('"
				+ row.partida
				+ "');\" type=\"button\" class=\"btn btn-xs btn-default command-edit\" data-row-id=\""
				+ row.partida
				+ "\"><span class=\"icon glyphicon glyphicon-zoom-in\"></span></button> ";
	}
}