<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

	<div class="container " id="error-message">
		<p align="center" class="bg-danger">Se produjo un error en la
			aplicación.</p>
 
<h4>Exception Name: <s:property value="exception" /> </h4>
 
<h4>Exception Details: <s:property value="exceptionStack" /></h4> 

		<p align="center">
			<s:url var="inicio" action="xxx_input" namespace="/" />
			<sj:a theme="bootstrap" cssClass="btn btn-primary"
				targets="container" href="%{inicio}">Inicio</sj:a>
		</p>
	</div>


