<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<style>
.cgb-header-name {
	width: 50%;
}

/* Manito sobre la grilla de datos para indicar que se puede clickear */
#cc-grid-data tbody {
	cursor: pointer;
}

input.currency {
	text-align: right;
	padding-right: 15px;
}
</style>
<script type="text/javascript">
	function closeResponseDialog() {
		// Por compatabilidad con el dialogo	
	}

	$('.selectpicker').selectpicker({
		width : '100%'
	});

	function volver() {
		verGrillaAgrupados(true);
	}
</script>


	<div
		class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div
		class="col-xs-12 col-sm-12 col-md-12 col-lg-12 alert alert-info text-center">
		Movimientos del periodo <a href="#"
			class="btn btn-xs btn-info pull-right" onclick="volver();"><span
			class="glyphicon glyphicon-arrow-left"></span> Volver</a>
			</div>
	</div>
	
<!-- <div class="row"> -->
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

		<table id="cc-movimiento-grid-data"
			class="table table-condensed table-hover table-striped">
			<thead>
				<tr>
					<th data-column-id="id" data-identifier="true" data-type="numeric"
						data-visible="false">IdCC</th>
					<th data-column-id="anio" data-type="numeric" data-visible="true"
						data-width="8%"><s:text name="anio.label" /></th>
					<th data-column-id="cuota" data-type="numeric" data-width="8%"><s:text
							name="cuota.label" /></th>
					<th data-column-id="tipo" data-formatter="cc_tipo_mov"
						data-header-align="center" data-align="center"><s:text
							name="deuda.tipo.label" /></th>
					<th data-column-id="ajuste" data-type="numeric" data-align="right"
						data-header-align="right" data-width="10%"><s:text
							name="deuda.ajuste.label" /></th>
					<th data-column-id="importe" data-type="price" data-align="right"
						data-header-align="right" data-width="10%"><s:text
							name="deuda.importe.label" /></th>
					<th data-column-id="interes" data-type="price" data-align="right"
						data-header-align="right" data-width="15%"><s:text
							name="deuda.interes.label" /></th>
					<th data-column-id="fechaVto" data-type="date" data-align="center"
						data-header-align="center"><s:text
							name="deuda.fechaVto.label" /></th>
					<th data-column-id="moratoria" data-type="numeric"><s:text
							name="deuda.moratoria.label" /></th>
				</tr>
			</thead>

		</table>
	</div>
<!-- </div> -->

<script>
	function detalleCuentaCorriente(id) {
		loader(true, null);

		$.ajax({
			method : "POST",
			url : "catastro/cuentacorrientemovimiento_detail.action",
			data : {
				idCC : id
			},
			traditional : true,
			success : function(res) {
				loader(false, null);
				/* $("#generic-dialog-response-label").text(
						"Detalle de movimiento de cuenta corriente");
				$("#generic-modal-response-body").html(res);
				$('#generic-dialog-response').modal(); */
				showResponseModalDialog(true, res, "Detalle de movimiento de cuenta corriente");
			},
			error : function(res, textStatus, jqXHR) {
				loader(false, null);
				handleErrorMessages(res.responseJSON);
				showErrorModalDialog(false);
			}
		});

	};
	
	$(document)
			.ready(
					function() {
						//search();

						$('input.numberdecimal').number(true, 2, ',', '');

						$("#cc-movimiento-grid-data")
								.bootgrid(
										{
											ajax : true,
											navigation : 2,
											rowSelect : false,
											selection : false,
											multiSelect : false,
											search : true,
											columnSelection : false,
											templates : {
												search : ""
											},
											labels : {
												noResults : "No hay datos de movimientos de cuenta corriente",
												loading : "Cargando información...",
												infos : "Mostrando {{ctx.start}} a {{ctx.end}} de {{ctx.total}} filas"
											},
											responseHandler : function(data) {
												return data;
											},
											requestHandler : function(rq) {
												rq.anio = $("#anio").val();
												return sortRequest(rq);
											},
											url : "catastro/cuentacorrientemovimiento_getData.action",
											statusMapping : {
												0 : "info",
												1 : "warning",
												2 : "danger"
											},
											converters : {
												price : {
													from : function(value) {
														return Number(value
																.replace(",",
																		"."));
													},
													to : function(value) {
														var v = Number(value)
																.toFixed(2);
														return (v + "")
																.replace(".",
																		",");
													}
												},
												date : {
													from : function(value) {
														return moment(value);
													},
													to : function(value) {
														if (!value) {
															return "";
														}
														return moment(value)
																.format(
																		"DD-MM-YYYY");
													}
												}
											},
											formatters : {
												cc_tipo_mov : function(column,
														row) {
													return fmt_cc_tipo_mov(row.tipo);
												}
											}
										}).on("click.rs.jquery.bootgrid",
										function(e, columns, row) {
											// Mostrar informacion de detalle del movimiento
											detalleCuentaCorriente(row.id);
										});

					});

	
</script>