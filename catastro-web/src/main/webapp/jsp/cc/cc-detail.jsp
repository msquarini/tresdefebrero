<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<style>
.small-col {
	font-size: small;
}

.number-align {
	text-align: right;
}
</style>

<script src="js/select/bootstrap-select.min.js"></script>
<link rel="stylesheet" href="css/select/bootstrap-select.css">

<div class="row">
	<div class="col-xs-6 col-sm-6 col-md-4 col-lg-2">
		<label for="anio"><s:text name="anio.label" /></label> <input id="anio"
			class="form-control info" type="text" readonly
			value='<s:property value="cc.anio"></s:property>' />
	</div>
	<div class="col-xs-6 col-sm-6 col-md-4 col-lg-2">
		<label for="cuota"><s:text name="cuota.label" /></label> <input id="cuota"
			class="form-control info" type="text" readonly
			value='<s:property value="cc.cuota"></s:property>' />
	</div>
	<div class="col-xs-6 col-sm-6 col-md-4 col-lg-2">
		<label for="tipocc"><s:text name="deuda.tipo.label" /></label>
		<h3 id="tipocc" align="center" style="margin-top: 0px;">
			<s:property value="cc.tipo.name"></s:property>
		</h3>
	</div>
	<div class="col-xs-6 col-sm-6 col-md-4 col-lg-3">
		<label for="vto"><s:text name="deuda.fechaVto.label" /></label> <input id="vto"
			class="form-control info" type="text" readonly
			value='<s:property value="getText('format.date',{cc.fechaVto})"></s:property>' />
	</div>
	<div class="col-xs-6 col-sm-6 col-md-4 col-lg-3">
		<label for="importe"><s:text name="deuda.importe.label" /></label>
		<div class="input-group">
			<span class="input-group-addon">$</span> <input id="importe"
				class="form-control info number-align" type="text" readonly
				value='<s:property value="getText('money.format',{cc.importe})"></s:property>' />
		</div>
	</div>
</div>
<div class="row">
	<div class="col-xs-6 col-sm-6 col-md-4 col-lg-2">
		<label for="codadm"><s:text name="deuda.codAdm.label" /></label> <input id="codadm"
			class="form-control info" type="text" readonly
			value='<s:property value="cc.codAdministrativo"></s:property>' />
	</div>
	<div class="col-xs-6 col-sm-6 col-md-4 col-lg-2">
		<label for="mora"><s:text name="deuda.moratoria.label" /></label> <input id="mora"
			class="form-control info" type="text" readonly
			value='<s:property value="cc.moratoria"></s:property>' />
	</div>
	<div class="col-xs-6 col-sm-6 col-md-4 col-lg-2">
		<label for="ajuste"><s:text name="deuda.ajuste.label" /></label> <input id="ajuste"
			class="form-control info" type="text" readonly
			value='<s:property value="cc.ajuste"></s:property>' />
	</div>
	<div class="col-xs-6 col-sm-6 col-md-4 col-lg-3">
		<label for="interes"><s:text name="deuda.interes.label" /></label>
		<div class="input-group">
			<span class="input-group-addon">$</span> <input id="interes"
				class="form-control info number-align" type="text" readonly
				value='<s:property value="getText('money.format',{cc.interes})"></s:property>' />
		</div>
	</div>
	<div class="col-xs-6 col-sm-6 col-md-4 col-lg-3">
		<label for="total"><s:text name="deuda.montototal.label" /></label>
		<div class="input-group">
			<span class="input-group-addon">$</span> <input id="total"
				class="form-control info number-align" type="text" readonly
				value='<s:property value="getText('money.format',{cc.montoTotal})"></s:property>' />
		</div>
	</div>
</div>

<div class="row top-buffer">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<table id="ccdetalle-grid-data"
			class="table table-condensed table-hover table-striped">
			<thead>
				<tr>
					<th data-column-id="id" data-identifier="true" data-type="numeric"
						data-visible="false">idCCDetalle</th>
					<th data-column-id="detalle" data-width="60%" data-sortable="false"><s:text
							name="deuda.detalle.label" /></th>
					<th data-column-id="importe" data-width="20%" data-type="number"
						data-sortable="false" data-header-align="center"
						data-align="right"><s:text name="deuda.importe.label" /></th>
				</tr>
			</thead>
			<tbody>
				<s:iterator value="cc.detalle">
					<tr>
						<td><s:property value="id"></s:property></td>
						<%-- <td><s:property value="concepto"></s:property></td> --%>
						<td><s:property value="detalle"></s:property></td>
						<td><s:property value="getText('money.format',{monto})"></s:property></td>
					</tr>
				</s:iterator>
			</tbody>
		</table>
	</div>
</div>

<div class="row top-buffer">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

		<s:if test="cc.pago">
			<div class="panel panel-success">
				<div class="panel-heading clearfix">
					<strong class="panel-title pull-left" style="padding-top: 7.5px;"><s:text
							name="deuda.pago.label" /></strong>
				</div>
				<div class="panel-body">
					<div class="row">

						<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
							<label for="mincc"><s:text name="deuda.fechaPago.label" /></label>
							<input class="form-control info" type="text" readonly
								value='<s:property value="getText('format.date',{cc.pago.fechaPago})"></s:property>' />
						</div>
						<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
							<label for="avgcc"><s:text name="label.total" /></label> <input
								class="form-control info" type="text" readonly
								value='<s:property value="getText('money.format',{cc.pago.importe})"></s:property>' />
						</div>
						<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
							<label for="avgcc"><s:text name="pago.file.origen" /></label> <input
								class="form-control info" type="text" readonly
								value='<s:property value="cc.pago.filePago.origen"></s:property>' />
						</div>
					</div>
				</div>
			</div>
		</s:if>
		<s:else>
			<s:if test="cc.isVencido">
				<div class="panel panel-danger">
					<div class="panel-heading clearfix" align="center">
						<strong class="panel-title" style="padding-top: 7.5px;"><s:text
								name="deuda.vencido.label" /></strong>
					</div>
				</div>
			</s:if>
			<s:else>
				<div class="panel panel-warning">
					<div class="panel-heading clearfix" align="center">
						<strong class="panel-title" style="padding-top: 7.5px;"><s:text
								name="deuda.nopago.label" /></strong>
					</div>
				</div>
			</s:else>
		</s:else>
	</div>
</div>
<script>
	$(document)
			.ready(
					function() {
						aux = fmt_cc_tipo_mov($('#tipocc').html());
						$('#tipocc').html(aux);

						$("#ccdetalle-grid-data")
								.bootgrid(
										{
											ajax : false,
											navigation : 0,
											rowCount : 5,
											search : false,
											columnSelection : false,
											templates : {
												search : ""
											},
											requestHandler : function(request) {
												return sortRequest(request);
											},
											labels : {
												noResults : "No se encontraron detalles de pagos.",
												loading : "Cargando información...",
												infos : "Mostrando {{ctx.start}} a {{ctx.end}} de {{ctx.total}} filas"
											}
										});
					});

	
</script>