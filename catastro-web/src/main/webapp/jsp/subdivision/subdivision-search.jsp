<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<script type="text/javascript">
	$(document).off("cuentaSelectedEvent").on("cuentaSelectedEvent",
			function(event, data) {
				//alert("selN " + data.cuenta.id);
				$("#idCuentaOrigen").val(data.cuenta.id);
				//$("#subdividir_btn").prop('disabled', false);
				$("#subdividir_btn").toggleClass('disabled', false);
			}).off("cuentaDeselectedEvent").on("cuentaDeselectedEvent",
			function(event, data) {
				//alert("Deselect " + data.cuenta.id);
				$("#idCuentaOrigen").val("-1");
				$("#subdividir_btn").toggleClass('disabled', true);
			});

	
	/* Configuracion del formulario, ya que se hace submit del mismo mediante javascript
	 * Se controla el mesanje de error y se muestra dialog generico de errores!!!! 
	 */
	var hash = '';
		
	$('#subdividir-init-form').ajaxForm({
		//target : '#generic-modal-body',
		target : '#container',
		success : function(res, textStatus, jqXHR) {
		},
		error : function(res, textStatus, jqXHR) {
			handleErrorMessages(res.responseJSON);
			if (res.status == '501') {
				hash = res.responseJSON.hash;
				showErrorModalDialog(true);
			} else {
				showErrorModalDialog(false);
			}
		}
	});
	
	function continuar() {
		//$("#hash").val(hash);
		//$('#subdividir-init-form').submit();
		common_submit('catastro/subdivision_init.action', $('#subdividir_btn'), hash, $('#subdividir-init-form'), $('#container'));
	}

	function init_submit() {
		//$('#subdividir-init-form').submit();
		common_submit('catastro/subdivision_init.action', $('#subdividir_btn'), null, $('#subdividir-init-form'), $('#container'));
	};
</script>
<style type="text/css">
</style>

<div class="row">
	<s:include value="../cuenta_search.jsp"></s:include>
</div>

<div class="row">
	<s:form id="subdividir-init-form" action="subdivision_init"
		namespace="/catastro" theme="bootstrap">
		<s:hidden id="idCuentaOrigen" name="idCuentaOrigen"></s:hidden>
		<%-- <s:hidden id="hash" name="hash"></s:hidden> --%>
		
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<%-- <sj:a id="subdividir_btn" cssClass="btn btn-primary disabled"
				value="Subdividir" formIds="subdividir-init-form"
				onErrorTopics="mostrarMensajeError" targets="container"
				onClickTopics="subdividirselect">Subdividir</sj:a> --%>
			<!-- 			<button id="subdividir_btn" type="button"
				class="btn btn-primary disabled" onclick="search_submit();">Subdividir</button>
				 -->

			<%-- <sj:a id="subdividir_btn" cssClass="btn btn-primary disabled"
                        value="Subdividir" formIds="subdividir-init-form" targets="container">Subdividir</sj:a> --%>

			<!-- Para poder manejar la respuesta del llamado ajax!!! -->
			<sj:a id="subdividir_btn" cssClass="btn btn-primary disabled"
				value="Subdividir" onclick="init_submit();">Subdividir</sj:a>

		</div>
	</s:form>
</div>