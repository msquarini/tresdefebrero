<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<!-- SUBDIVISION, datos de la cuenta a subdividir y las nuevas cuentas -->

<script type="text/javascript">
	/* Resultado del procesamiento de subdivision !! */
/* 	$.subscribe('complete', function(event, data) {
		loader(false, $('#btn_subdivision_process'));

		$('#generic-modal-response-body').html(
				event.originalEvent.request.responseText);
		showResponseModalDialog(true, event.originalEvent.request.responseText);
	});
 */
	function validarFormulario() {
		$('#subdivision-form').bootstrapValidator('resetForm');
		$('#subdivision-form').bootstrapValidator('validate');
		return $('#subdivision-form').data('bootstrapValidator').isValid();
	}
	function procesarTramite() {
		if (validarFormulario()) {
			loader(true, $('#btn_subdivision_process'));
			$('#subdivision-form').submit();
		}
	}
	function grabarTramite() {
		if (validarFormulario()) {
			loader(true, $('#btn_subdivision_save'));

			var anio = $('#tf_origen_anio').val();
			var codigo = $('#tipoSubdivision').val();
			var numero = $('#tf_origen_numero').val();
			var oficio = $('#tf_oficio').val();
			var observacion = $('#tf_observacion').val();
			
			$.ajax({
				method : "POST",
				url : "catastro/subdivision_saveTramite.action",
				data : {
					'tramite.anio' : anio,
					'tramite.codigo' : codigo,
					'tramite.numero' : numero,
					'tramite.oficio' : oficio,
					'tramite.observacion' : observacion
				},
				traditional : true,
				success : function(res) {
					loader(false, $('#btn_subdivision_save'));
					//$("#generic-modal-response-body").html(res);
					showResponseModalDialog(true, res); // true para boton imprimir 
				},
				error : function(res, textStatus, jqXHR) {
					loader(false, $('#btn_subdivision_save'));
					// Levantar dialogo con mensaje de error!!!!!!!!!!!!!
					handleErrorMessages(res.responseJSON);
					showErrorModalDialog(false);
				}
			});
		}
	}
</script>

<style type="text/css">
</style>

<s:include value="../common/tramite-header.jsp"></s:include>

<ul class="nav nav-pills"
	style="background-color: #eeeeee; border-bottom-style: ridge;">
	<li class="nav-header disabled"><a><s:text
				name="subdivision.label" /></a></li>
	<li class="active"><a data-toggle="tab" href="#cuenta-origen"><s:text
				name="subdivision.cuentaorigen.label" /></a></li>
	<li><a data-toggle="tab" href="#titulares"><s:text
				name="titulares.label" /></a></li>
	<li><a data-toggle="tab" href="#responsable"><s:text
				name="responsables.label" /></a></li>
	<li><a data-toggle="tab" href="#nuevas"><s:text
				name="subdivision.nuevasparcelas.label" /></a></li>

	<s:if test='tramite.estado.toString() != "FINALIZADO"'>
		<li class="pull-right" style="padding-top: 3px; padding-right: 3px">
			<button id="btn_subdivision_save" class="btn btn-warning"
				type="button" onclick="grabarTramite()">
				<span class="fa fa-floppy-o"></span>&nbsp;&nbsp;
				<s:text name="dialog.label.grabar" />
				&nbsp;&nbsp;
			</button> 
			<button id="btn_subdivision_process" class="btn btn-danger"
				type="button" onclick="procesarTramite()">
				<span class="fa fa-wrench"></span>&nbsp;<s:text name="dialog.label.procesar"/>&nbsp;
			</button>
		</li>
	</s:if>
</ul>

<div class="tab-content">
	<div id="cuenta-origen" class="tab-pane fade in active">
		<s:include value="../cuenta-info.jsp"></s:include>
	</div>
	<div id="titulares" class="tab-pane fade">
		<!-- Grilla de titulares pertenecientes a la parcela a subdividir
			 Se permite edicion de CUIT/CUIL  -->
		<s:include value="../grid/titular-grid.jsp"></s:include>
	</div>
	<div id="responsable" class="tab-pane fade">
		<!-- Grilla de responsables pertenecientes a la cuenta -->
		<s:include value="../info/responsables-info.jsp"></s:include>
	</div>
	<div id="nuevas" class="tab-pane fade">
		<s:include value="subdivision-parcela-grid.jsp"></s:include>
	</div>
</div>

