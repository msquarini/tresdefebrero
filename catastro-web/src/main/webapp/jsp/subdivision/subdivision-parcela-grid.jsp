<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<script type="text/javascript">
	/* Selecciona tipo de subdivision */
	function cambioTipoSubdivision() {
		if ($('#tipoSubdivision').val() < 3) {
			$('#origen_numero').removeClass('hidden');
			$('#origen_anio').removeClass('hidden');
		} else {
			$('#origen_numero').addClass('hidden');
			$('#origen_anio').addClass('hidden');
		}
	}

	/* Ir a dialogo de nueva parcela
	 * Activa el boton de grabar en el dialogo generico */
	function addCuenta() {
		$.ajax({
			method : "POST",
			url : "catastro/subdivision_inputNuevaParcela.action",
			data : {},
			traditional : true,
			success : function(res) {
				loader(false, null);
				var btn_array = [ "submit-dialog-button" ];
				showModalDialog("Informacion de Parcela", res, btn_array);
			},
			error : function(res, textStatus, jqXHR) {
				loader(false, null);
				// Levantar dialogo con mensaje de error!!!!!!!!!!!!!
				handleErrorMessages(res.responseJSON);
				showErrorModalDialog(false);
			}
		});
	}

	/* Id (partida) de la fila elegida y boolean indicando si es edit o delete */
	function sd_accion_submit(id, edit) {
		//$('#partida').val(id);		
		$('#uuid').val(id);
		if (edit) {
			var btn_array = [ "submit-dialog-button" ];
			subdivision_parcela_dialog(
					"catastro\/subdivision_editParcela.action",
					"Informacion de Parcela NUEVO", btn_array);
		} else {
			var btn_array = [];
			subdivision_parcela_dialog(
					"catastro\/subdivision_deleteParcela.action",
					"Eliminar Parcela NUEVO", btn_array);
		}
	};
	function sd_info_parcela(id) {
		//$('#partida').val(id);
		$('#uuid').val(id);
		var btn_array = [];
		subdivision_parcela_dialog("catastro\/subdivision_editParcela.action",
				"Informacion de Parcela", btn_array);
	};

	/* Submit de dialogo de parcela */
	function subdivision_parcela_dialog(action, title, btns) {
		loader(true, null);
		$.ajax({
			method : "POST",
			url : action,
			data : $('#grid-parcela-form').serialize(),
			traditional : true,
			success : function(res) {
				loader(false, null);
				showModalDialog(title, res, btns);
			},
			error : function(res, textStatus, jqXHR) {
				loader(false, null);
				handleErrorMessages(res.responseJSON);
				showErrorModalDialog(false);
			}
		});
	}

	/* ELIMINAR!!!!!!!!!!!! Formulario para editar y borrar parcelas de la grilla de nuevas parcelas */
	$('#grid-parcela-form').ajaxForm({
		target : '#generic-modal-body',
		success : function(res, textStatus, jqXHR) {
			$("#generic-data-label").text("Informacion de Parcela");
			$('#generic-dialog').modal();
			//$('#parcela-grid-data').bootgrid('reload');
		},
		error : function(res, textStatus, jqXHR) {
			alert(res);
		}
	});

	/* Configuracion del formulario, ya que se hace submit del mismo mediante javascript */
	/* PROCESAMIENTO!!!*/
	$('#subdivision-form').ajaxForm({
		target : '#generic-modal-response-body',
		success : function(res, textStatus, jqXHR) {
			loader(false, $('#btn_subdivision_process'));
			showResponseModalDialog(true);
		},
		error : function(res, textStatus, jqXHR) {
			loader(false, $('#btn_subdivision_process'));
			// Levantar dialogo con mensaje de error!!!!!!!!!!!!!
			handleErrorMessages(res.responseJSON);
			showErrorModalDialog(false);
		}
	});

	$(document)
			.ready(
					function() {
						$("#tipoSubdivision").val(
								<s:property value='tramite.codigo'/>);
						cambioTipoSubdivision();
						
						$('#tf_oficio').checkboxpicker({
							offLabel : 'No',
							onLabel : 'Si'
						}).on('change', function() {
							if ($(this).is(':checked')) {
								$(this).prop('value', 'true');
							} else {
								$(this).prop('value', 'false');
							}
						});

						if ($('#tf_oficio').prop('value') == 'true') {
							$('#tf_oficio').prop('checked', 'true');
						}

						$('#subdivision-form')
								.bootstrapValidator(
										{
											framework : 'bootstrap',
											excluded : [ ':disabled', ':hidden' ], // Se incluye hidden para validar en multiples tabs
											//excluded : [ ':disabled' ],
											feedbackIcons : {
												valid : 'glyphicon glyphicon-ok',
												invalid : 'glyphicon glyphicon-remove',
												validating : 'glyphicon glyphicon-refresh'
											},
											message : 'Datos inválidos',
											submitHandler : function(validator,
													form, submitButton) {
												alert(form);
												debugger;
											},
											fields : {
												'tramite.numero' : {
													validators : {
														notEmpty : {
															message : 'Dato Obligatorio'
														},
														integer : {
															message : 'El valor ingresado debe ser numérico'
														}
													}
												},
												'tramite.anio' : {
													validators : {
														notEmpty : {
															message : 'Dato Obligatorio'
														},
														greaterThan : {
															value : 1900,
															message : 'El valor ingresado debe ser mayor que 1900'
														}
													}
												}
											}
										});
					});
</script>

<!--  Formulario para la edicion o delete de parcelas en la grilla de subdivision -->
<s:form id="grid-parcela-form" action="subdivision_editParcela"
	cssClass="form-vertical" namespace="/catastro" theme="bootstrap">
	<%-- <s:hidden id="partida" name="partida"></s:hidden> --%>
	<s:hidden id="uuid" name="uuid"></s:hidden>
</s:form>

<div class="row top-buffer">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

		<!--  Formulario principal de SUBDIVISION, envia datos comunes y realiza el procesamiento !!!! -->
		<s:form id="subdivision-form" action="subdivision_process"
			cssClass="form-vertical" namespace="/catastro" theme="bootstrap">

			<s:if test='tramite.estado.toString != "FINALIZADO"'>
				<fieldset>
			</s:if>
			<s:else>
				<fieldset disabled>
			</s:else>
			<div class="row  input-group-sm" style="display: flex; width: 100%;">
				<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
					<label for="tipoSubdivision"><s:text name="tipo.label" /></label>

					<%-- 			<s:select id="tipoSubdivision" class="form-control"
						list="#{1:'Regimen de propiedad horizontal', 2:'Geodesia', 6: 'Plano sin apertura en ARBA'}"
						headerValue="Seleccione Tipo" name="tramite.codigo" 
						value="<s:property value='tramite.codigo'/>"
						onchange="cambioTipoSubdivision()"></s:select> --%>
					<select id="tipoSubdivision" class="form-control"
						title="Seleccione Tipo" onchange="cambioTipoSubdivision()"
						name="tramite.codigo">
						<option value="1">Regimen de propiedad horizontal</option>
						<option value="2">Geodesia</option>
						<option value="6">Plano sin apertura en ARBA</option>
					</select>
				</div>

				<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3"
					style="flex: 1; padding: 25px;">
					<label for="tf_oficio"><s:text name="tramite.oficio.label" />&nbsp;</label><input
						id="tf_oficio" type="checkbox" name="tramite.oficio"
						value="<s:property value="tramite.oficio" />" />
				</div>
				<div class="col-xs-6 col-sm-6 col-md-3 col-lg-2">
					<div class="form-group">
						<label for="tf_vigencia"><s:text name="vigencia.label" /></label>
						<div class="input-group date" data-provide="datepicker"
							data-date-format="dd/mm/yyyy" data-date-autoclose="true"
							data-date-orientation="bottom" data-date-language="es">
							<input id="tf_vigencia" type="text" class="form-control"
								placeholder="Vigencia"
								value='<s:date name="tramite.vigencia" format="dd/MM/yyyy" />'
								name="tramite.vigencia">
							<div class="input-group-addon">
								<span class="glyphicon glyphicon-calendar"></span>
							</div>
						</div>
					</div>
				</div>

				<div id="origen_numero"
					class="col-xs-6 col-sm-6 col-md-offset-4 col-md-2 col-lg-offset-1 col-lg-2">
					<label for="tf_origen_numero"><s:text
							name="subdivision.plano.numero" /></label>
					<s:textfield id="tf_origen_numero" class="form-control"
						name="tramite.numero" />
				</div>
				<div id="origen_anio" class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
					<div class="form-group">
						<label for="tf_origen_anio"><s:text name="anio.label" /></label>
						<div class="input-group date" data-provide="datepicker"
							data-date-format="yyyy" data-date-autoclose="true"
							data-date-view-mode="years" data-date-min-view-mode="years"
							data-date-end-date="y" data-date-orientation="bottom"
							data-date-language="es" data-date-today-btn="true"
							data-date-today-highlight="true">
							<input id="tf_origen_anio" type="text" class="form-control"
								name="tramite.anio" value="<s:property value='tramite.anio'/>">
							<div class="input-group-addon">
								<span class="glyphicon glyphicon-calendar"></span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row  input-group-sm">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<textarea class="form-control" rows="2" placeholder="Observaciones"
						name="observacion" id="tf_observacion"><s:property
							value="tramite.observacion" /></textarea>
				</div>
			</div>
			</fieldset>
		</s:form>
	</div>

</div>

<table id="parcela-grid-data"
	class="table table-condensed table-hover table-striped bootgrid-table">
	<thead>
		<tr>
			<th data-column-id="cuenta" data-identifier="true"
				data-type="numeric" data-formatter="cuenta" data-width="10%"><s:text
					name="cuenta.label" /></th>
			<th data-column-id="partida" data-identifier="true"
				data-type="numeric" data-formatter="partida" data-width="10%"><s:text
					name="partida.label" /></th>
			<th data-column-id="nomenclatura" data-formatter="nomenclatura"
				data-sortable="false" data-width="40%"><s:text
					name="nomenclatura.label" /></th>
			<%-- <th data-column-id="linea" data-formatter="linea"
				data-sortable="false" data-width="40%"><s:text name="linea.label"/></th> --%>

			<s:if test='tramite.estado.toString != "FINALIZADO"'>
				<th data-column-id="commands" data-formatter="commands"
					data-width="10%" data-sortable="false"><s:text
						name="label.acciones" /></th>
			</s:if>
			<s:else>
				<th data-column-id="commands" data-formatter="commands_view"
					data-width="10%" data-sortable="false"><s:text
						name="label.acciones" /></th>
			</s:else>
		</tr>
	</thead>
</table>
<s:if test='tramite.estado.toString() != "FINALIZADO"'>
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<button id="agregarParcela" class="btn btn-primary " value="Agregar"
				onclick="addCuenta();">
				<s:text name="dialog.label.agregar" />
			</button>
		</div>
	</div>
</s:if>

<script>
	var grid = $("#parcela-grid-data")
			.bootgrid(
					{
						navigation : 0,
						ajax : true,
						rowSelect : true,
						url : "catastro/parcela_getData.action",
						requestHandler : function(request) {
							return sortRequest(request);
						},
						labels : {
							noResults : "No hay parcelas",
							loading : "Cargando información...",
							infos : "Mostrando {{ctx.start}} a {{ctx.end}} de {{ctx.total}} filas"
						},
						formatters : {
							cuenta : function(column, row) {
								if (row.cuenta) {
									return row.cuenta.cuenta;
								}
							},
							partida : function(column, row) {
								return row.partida;
							},
							nomenclatura : function(column, row) {
								return fmt_nomenclatura(row);
							},
							commands : function(column, row) {
								return fmt_acction_subdivision_grilla(row, true);
							},
							commands_view : function(column, row) {
								return fmt_acction_subdivision_grilla(row,
										false);
							}
						}
					}).on("loaded.rs.jquery.bootgrid", function(e) {
			});

	
</script>