<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<!-- Primera Inscripcion Parcela - Formulario de datos de parcela -->

<script type="text/javascript">
	function updateValuacion() {
		var v = parseFloat($('#valor_tierra').val())
				+ parseFloat($('#valor_comun').val())
				+ parseFloat($('#valor_edificio').val());
		//$('#valuacion').text('$ ' + v.toFixed(2).replace('.', ','));
		$('#valuacion').text('$ ' + $.number(v, 2, ',', '.'));
	}

	$('#destinoMunicipal').checkboxpicker({
		offLabel : 'No',
		onLabel : 'Si'
	});

	/* Submit del formulario previa validacion */
	function dialog_submit() {
		$('#inscripcion-parcela-form').bootstrapValidator('validate');

		if ($('#inscripcion-parcela-form').data('bootstrapValidator').isValid()) {
			$('#inscripcion-parcela-form').submit();
		}
	};

	/* Configuracion del formulario, ya que se hace submit del mismo mediante javascript */
	$('#inscripcion-parcela-form').ajaxForm({
		target : '#generic-modal-body',
		success : function(res, textStatus, jqXHR) {
			$('#generic-dialog').modal();
		},
		error : function(res, textStatus, jqXHR) {
		}
	});

	/* Configuracion de validador del formulario */
	$(document)
			.ready(
					function() {

						$('input.number').number(true, 0, '', '');
						$('input.numberdecimal').number(true, 2, ',', '');

						// Set up the number formatting.
						//$('#nuevaParcela.nomenclatura.circunscripcion').number(
						//		true, 0, '', '');

						$('#inscripcion-parcela-form')
								.bootstrapValidator(
										{
											framework : 'bootstrap',
											excluded : [ ':disabled' ],
											feedbackIcons : {
												//valid : 'glyphicon glyphicon-ok',
												invalid : 'glyphicon glyphicon-remove',
												validating : 'glyphicon glyphicon-refresh'
											},
											message : 'Datos invalidos',
											/* submitHandler: function(validator, form, submitButton) {
													
											}, */
											fields : {
												'origen.numero' : {
													validators : {
														notEmpty : {
															message : 'Dato Obligatorio'
														},
														integer : {
															message : 'El valor ingresado debe ser num�rico'
														}
													}
												},
												'origen.anio' : {
													validators : {
														notEmpty : {
															message : 'Dato Obligatorio'
														},
														greaterThan : {
															value : 1900,
															message : 'El valor ingresado debe ser mayor que 1900'
														}
													}
												},
												'nuevaParcela.nomenclatura.circunscripcion' : {
													validators : {
														notEmpty : {
															message : 'Obligatorio'
														}
													}
												},
												'nuevaParcela.nomenclatura.seccion' : {
													validators : {
														notEmpty : {
															message : 'Obligatorio'
														}
													}
												},
												'nuevaParcela.nomenclatura.manzana' : {
													validators : {
														notEmpty : {
															message : 'Obligatorio'
														}
													}
												},
												'nuevaParcela.nomenclatura.parcela' : {
													validators : {
														notEmpty : {
															message : 'Obligatorio'
														}
													}
												},
												'nuevaParcela.partida' : {
													validators : {
														notEmpty : {
															message : 'Partida Obligatorio'
														},
														integer : {
															message : 'Debe ser un valor n�merico de 6 d�gitos'
														}
													}
												},
												'nuevaParcela.dominio.tipo' : {
													validators : {
														notEmpty : {
															message : 'Obligatorio'
														}
													}
												},
												'nuevaParcela.dominio.numero' : {
													validators : {
														notEmpty : {
															message : 'Obligatorio'
														}
													}
												},
												'nuevaParcela.lineaVigente.superficieTerreno' : {
													validators : {
														notEmpty : {
															message : 'Obligatorio'
														},
														greaterThan : {
															message : 'El valor debe ser mayor a 0',
															value : 0,
															inclusive : false
														}
													}
												},
												'nuevaParcela.lineaVigente.superficieCubierta' : {
													validators : {
														notEmpty : {
															message : 'Obligatorio'
														}
													}
												},
												'nuevaParcela.lineaVigente.superficieSemi' : {
													validators : {
														notEmpty : {
															message : 'Obligatorio'
														}
													}
												},
												'nuevaParcela.lineaVigente.valorTierra' : {
													validators : {
														notEmpty : {
															message : 'Obligatorio'
														},
														greaterThan : {
															message : 'El valor debe ser mayor a 0',
															value : 0,
															inclusive : false
														}
													}
												},
												'nuevaParcela.lineaVigente.valorEdificio' : {
													validators : {
														notEmpty : {
															message : 'Obligatorio'
														}
													}
												},
												'nuevaParcela.domicilioPostal.calle' : {
													validators : {
														notEmpty : {
															message : 'Obligatorio'
														}
													}
												},
												'nuevaParcela.domicilioInmueble.calle' : {
													validators : {
														notEmpty : {
															message : 'Obligatorio'
														}
													}
												},
												'nuevaParcela.domicilioPostal.numero' : {
													validators : {
														notEmpty : {
															message : 'Obligatorio'
														}
													}
												},
												'nuevaParcela.domicilioInmueble.numero' : {
													validators : {
														notEmpty : {
															message : 'Obligatorio'
														}
													}
												},
												'nuevaParcela.destino' : {
													validators : {
														notEmpty : {
															message : 'Obligatorio'
														}
													}
												}
											}
										});
					});
</script>
<style type="text/css">
.numberdecimal {
	text-align: right;
}

.has-feedback .form-control {
	top: 0;
	/* Para utilizar todo el area editable */
	padding-right: 10px;
}

.form-control-feedback {
	/* Para correr el icon a la derecha */
	right: -22px;
}
</style>
<s:url var="circ" action="consulta_cargaCircunscripciones"
	namespace="/catastro" includeParams="none" />

<s:form id="inscripcion-parcela-form" action="inscripcion_process"
	cssClass="form-vertical" namespace="/catastro" theme="bootstrap">

	<fieldset>

		<div class="row top-buffer">
			<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
				<label for="circunscripcion"><s:text name="circ.short.label" /></label>
				<%-- 				<s:textfield id="circunscripcion"
					class="form-control input-sm number"
					name="nuevaParcela.nomenclatura.circunscripcion" maxlength="3"
					readonly="true" /> --%>


				<sj:select id="circunscripcion" href="%{circ}" resizable="false"
					class="form-control input-sm" draggable="false" droppable="false"
					selectable="false" sortable="false"
					name="nomenclatura.circunscripcion" list="circunscripciones"
					listKey="circunscripcion" listValue="circunscripcion"
					onChangeTopics="recargaSecciones" headerKey="-1"
					headerValue="Seleccione" />
			</div>
			<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
				<label for="seccion"><s:text name="sec.short.label" /></label>
				<%-- 				<s:textfield id="seccion" class="form-control input-sm"
					maxlength="3" name="nuevaParcela.nomenclatura.seccion"
					readonly="true" /> --%>
				<sj:select id="seccion" href="%{circ}" resizable="false"
					class="form-control input-sm" draggable="false" droppable="false"
					selectable="false" sortable="false" name="nomenclatura.seccion"
					list="secciones" listKey="seccion" listValue="seccion"
					reloadTopics="recargaSecciones" />
			</div>
			<div class="col-xs-6 col-sm-6 col-md-1 col-lg-1">
				<label for="fraccion"><s:text name="fr.short.label" /></label>
				<s:textfield id="fraccion" class="form-control input-sm number"
					maxlength="3" name="nuevaParcela.nomenclatura.fraccion" />
			</div>
			<div class="col-xs-6 col-sm-6 col-md-1 col-lg-1">
				<label for="fraccionLetra"><s:text name="frl.short.label" /></label>
				<s:textfield id="fraccionLetra" class="form-control input-sm"
					maxlength="3" name="nuevaParcela.nomenclatura.fraccionLetra" />
			</div>
			<div class="col-xs-6 col-sm-6 col-md-1 col-lg-1">
				<label for="manzana"><s:text name="mz.short.label" /></label>
				<s:textfield id="manzana" class="form-control input-sm number"
					maxlength="3" name="nuevaParcela.nomenclatura.manzana" />
			</div>
			<div class="col-xs-6 col-sm-6 col-md-1 col-lg-1">
				<label for="manzanaLetra"><s:text name="mzl.short.label" /></label>
				<s:textfield id="manzanaLetra" class="form-control input-sm"
					maxlength="3" name="nuevaParcela.nomenclatura.manzanaLetra" />
			</div>
			<div class="col-xs-6 col-sm-6 col-md-1 col-lg-1">
				<label for="parcela"><s:text name="pc.short.label" /></label>
				<s:textfield id="parcela" class="form-control input-sm number"
					maxlength="3" name="nuevaParcela.nomenclatura.parcela" />
			</div>
			<div class="col-xs-6 col-sm-6 col-md-1 col-lg-1">
				<label for="parcelaLetra"><s:text name="pcl.short.label" /></label>
				<s:textfield id="parcelaLetra" class="form-control input-sm"
					maxlength="3" name="nuevaParcela.nomenclatura.parcelaLetra" />
			</div>
			<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
				<label for="unidadFuncional"><s:text name="uf.short.label" /></label>
				<s:textfield id="unidadFuncional"
					class="form-control input-sm number" maxlength="6"
					name="nuevaParcela.nomenclatura.unidadFuncional" />
			</div>
		</div>

		<!-- Partida | Zona | Categoria -->
		<div class="row">

			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">

				<div class="panel panel-default">
					<div class="panel-heading">
						<strong><s:text name="dominio.panel.label" /></strong>
					</div>
					<div class="panel-body">

						<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
							<label for="inscripcion"><s:text
									name="dominio.tipo.label" /></label> <select id="inscripcion"
								name="nuevaParcela.dominio.tipo" class="form-control"
								onchange="cambioTipoInscripcion()">
								<option value="M"><s:text name="matricula.label" /></option>
								<option value="F"><s:text name="folio.label" /></option>
							</select>
							<%-- <s:textfield id="inscripcion" name="nuevaParcela.dominio.tipo" /> --%>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
							<label for="numero"><s:text name="dominio.numero.label" /></label>
							<s:textfield id="numero" name="nuevaParcela.dominio.numero"
								class="form-control number" maxlength="6" />
						</div>
						<div id="div_anio"
							class="col-xs-6 col-sm-6 col-md-4 col-lg-4 hidden">
							<label for="anio"><s:text name="dominio.anio.label" /></label>
							<s:textfield id="anio" name="nuevaParcela.dominio.anio"
								class="form-control number" maxlength="4" />
						</div>
					</div>
				</div>
			</div>

			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">

				<div class="panel panel-default">
					<div class="panel-heading">
						<strong><s:text name="parcela.panel.label" /></strong>
					</div>
					<div class="panel-body">

						<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
							<label for="partida"><s:text name="partida.label" /></label>
							<s:textfield id="partida" class="form-control number"
								maxlength="6" name="nuevaParcela.partida" />
						</div>

						<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
							<label for="categoria"><s:text name="categoria.label" /></label>
							<select id="categoria" name="nuevaParcela.lineaVigente.categoria"
								class="form-control">
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
								<option value="5">5</option>
								<option value="6">6</option>
								<option value="7">7</option>
								<option value="8">8</option>
							</select>
						</div>

						<div class="col-xs-6 col-sm-6 col-md-5 col-lg-5">
							<label for="tf_vigencia"><s:text name="vigencia.label" /></label>
							<div class="form-group">
								<div class="input-group date" data-provide="datepicker"
									data-date-format="dd/mm/yyyy" data-date-autoclose="true"
									data-date-end-date="0d" data-date-orientation="bottom"
									data-date-language="es" data-date-today-btn="true"
									data-date-today-highlight="true">
									<input id="tf_vigencia" type="text" class="form-control"
										name="nuevaParcela.lineaVigente.fecha"
										value="<s:property value='getText("format.date",{nuevaParcela.lineaVigente.fecha})'/>">
									<div class="input-group-addon">
										<span class="glyphicon glyphicon-calendar"></span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>

		<!--  Superificies -->
		<div class="row">
			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">

				<div class="panel panel-default">
					<div class="panel-heading">
						<strong><s:text name="superficies.panel.label" /></strong>
					</div>
					<div class="panel-body">
						<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
							<div class="form-group">
								<label for="sup_terreno"><s:text name="terreno.label" /></label>
								<div class="input-group">
									<input type="text" id="sup_terreno"
										class="form-control numberdecimal input-numeral" maxlength="8"
										name="nuevaParcela.lineaVigente.superficieTerreno"
										value="<s:property value='%{getText("number.format" ,{nuevaParcela.lineaVigente.superficieTerreno})}'/>">
									<span class="input-group-addon">m<sup>2</sup></span>
								</div>
							</div>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
							<div class="form-group">
								<label for="sup_cubierta"><s:text name="cubierta.label" /></label>
								<div class="input-group">
									<input type="text" id="sup_cubierta"
										class="form-control numberdecimal" maxlength="8"
										name="nuevaParcela.lineaVigente.superficieCubierta"
										value="<s:property value='%{getText("number.format" ,{nuevaParcela.lineaVigente.superficieCubierta})}'/>">
									<span class="input-group-addon">m<sup>2</sup></span>
								</div>
							</div>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
							<div class="form-group">
								<label for="sup_semi"><s:text name="semi.label" /></label>
								<div class="input-group">
									<input type="text" id="sup_semi"
										class="form-control numberdecimal" maxlength="8"
										name="nuevaParcela.lineaVigente.superficieSemi"
										value="<s:property value='%{getText("number.format" ,{nuevaParcela.lineaVigente.superficieSemi})}'/>">
									<span class="input-group-addon">m<sup>2</sup></span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
				<!-- Valores -->
				<div class="panel panel-default">
					<div class="panel-heading clearfix">
						<div class="pull-left">
							<strong><s:text name="valuacion.panel.label" /></strong>
						</div>
						<div class="pull-right" style="padding-right: 15px;">
							<span id="valuacion" class="label label-info"
								style="font-size: 100%">$ 0,00</span>
						</div>
					</div>
					<div class="panel-body">
						<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
							<div class="form-group">
								<label for="valor_tierra"><s:text name="tierra.label" /></label>
								<div class="input-group">
									<span class="input-group-addon">$</span> <input type="text"
										id="valor_tierra" onchange="updateValuacion()"
										class="form-control numberdecimal" maxlength="8"
										name="nuevaParcela.lineaVigente.valorTierra"
										value="<s:property value='%{getText("number.format" ,{nuevaParcela.lineaVigente.valorTierra})}'/>">
								</div>
							</div>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
							<div class="form-group">
								<label for="valor_comun"><s:text name="comun.label" /></label>
								<div class="input-group">
									<span class="input-group-addon">$</span> <input type="text"
										id="valor_comun" onchange="updateValuacion()"
										class="form-control numberdecimal" maxlength="8"
										name="nuevaParcela.lineaVigente.valorComun"
										value="<s:property value='%{getText("number.format" ,{nuevaParcela.lineaVigente.valorComun})}'/>">
								</div>
							</div>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
							<div class="form-group">
								<label for="valor_edificio"><s:text
										name="edificio.label" /></label>
								<div class="input-group">
									<span class="input-group-addon">$</span> <input type="text"
										id="valor_edificio" onchange="updateValuacion()"
										class="form-control numberdecimal" maxlength="8"
										name="nuevaParcela.lineaVigente.valorEdificio"
										value="<s:property value='%{getText("number.format" ,{nuevaParcela.lineaVigente.valorEdificio})}'/>">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Domicilio Inmueble -->
		<div class="panel-group">
			<div class="panel panel-default">
				<div class="panel-heading mano" data-toggle="collapse"
					href="#panel_domicilio_inmueble">
					<strong><s:text name="domicilio.inmueble.panel.label" /></strong>
				</div>
				<div class="panel-body panel-collapse"
					id="panel_domicilio_inmueble">
					<div class="col-xs-6 col-sm-6 col-md-5 col-lg-4">
						<label for="di_calle"><s:text name="domicilio.calle.label" /></label>
						<s:textfield id="di_calle" class="form-control input-sm"
							name="nuevaParcela.domicilioInmueble.calle" />
					</div>
					<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
						<label for="di_numero"><s:text name="domicilio.nro.label" /></label>
						<s:textfield id="di_numero" class="form-control input-sm"
							name="nuevaParcela.domicilioInmueble.numero" />
					</div>
					<div class="col-xs-6 col-sm-6 col-md-1 col-lg-1">
						<label for="di_piso"><s:text name="domicilio.piso.label" /></label>
						<s:textfield id="di_piso" class="form-control input-sm"
							name="nuevaParcela.domicilioInmueble.piso" />
					</div>
					<div class="col-xs-6 col-sm-6 col-md-1 col-lg-1">
						<label for="di_depto"><s:text name="domicilio.depto.label" /></label>
						<s:textfield id="di_depto" class="form-control input-sm"
							name="nuevaParcela.domicilioInmueble.departamento" />
					</div>
					<div class="col-xs-6 col-sm-6 col-md-1 col-lg-2">
						<label for="di_cp"><s:text name="domicilio.cp.label" /></label>
						<s:textfield id="di_cp" class="form-control input-sm"
							name="nuevaParcela.domicilioInmueble.codigoPostal" />
					</div>
					<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
						<label for="di_barrio"><s:text
								name="domicilio.barrio.label" /></label>
						<s:select id="di_barrio" list="#session.barrios" listKey="id"
							listValue="nombre" class="form-control input-sm"
							name="nuevaParcela.domicilioInmueble.barrio.id"></s:select>
					</div>
				</div>
			</div>

		</div>

		<div class="row">
			<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
				<label for="destinoMunicipal"><s:text
						name="dominiomunicipal.label" /></label> <input id="destinoMunicipal"
					data-group-cls="btn-group-sm" type="checkbox"
					name="nuevaParcela.dominioMunicipal" disabled checked value="true" />
			</div>
			<div id="div_destino" class="col-xs-6 col-sm-6 col-md-10 col-lg-10">
				<label for="destino"><s:text name="destino.label" /></label>
				<s:select id="destino" list="#session.destinos" listKey="destino"
					listValue="destino" name="nuevaParcela.destino" headerKey=""
					headerValue="Seleccione"></s:select>
			</div>
		</div>

		<!-- Observacion -->
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<label for="observacion"><s:text name="observacion.label" /></label>
				<s:textfield id="observacion" name="nuevaParcela.observacion" />
			</div>
		</div>
	</fieldset>

</s:form>

