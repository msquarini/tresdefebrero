<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<!-- INSCRIPCION  -->

<script type="text/javascript">
	$.subscribe('complete', function(event, data) {
		showResponseModalDialog(true, event.originalEvent.request.responseText);
	});

	/* Boton de procesamiento de Primera Inscripcion, antes del envio del formulario, se valida el mismo */
	$('#btn').subscribe(
			'validate-inscripcion-form',
			function(event) {

				$('#inscripcion-parcela-form').bootstrapValidator('validate');
				// Valor de validar el formulario
				event.originalEvent.options.submit = $(
						'#inscripcion-parcela-form').data('bootstrapValidator')
						.isValid();

			});
</script>

<style type="text/css">
</style>

<ul class="nav nav-pills"
	style="background-color: #eeeeee; border-bottom-style: ridge;">
	<li class="nav-header disabled"><a>Primera Inscripción</a></li>

	<li class="active"><a data-toggle="tab" href="#nueva">Datos
			Parcela</a></li>
	<li><a data-toggle="tab" href="#titulares">Titulares</a></li>
	<li><a data-toggle="tab" href="#responsable">Responsables</a></li>

	<li class="pull-right" style="padding-top: 3px; padding-right: 3px">
		<sj:submit cssClass="btn btn-danger" id="btn" value="Procesar"
			formIds="inscripcion-parcela-form"
			targets="generic-modal-response-body" onSuccessTopics="complete"
			onBeforeTopics="validate-inscripcion-form"></sj:submit>
	</li>

</ul>

<div class="tab-content">
	<div id="nueva" class="tab-pane fade in active">
		<s:include value="inscripcion-parcela-content.jsp"></s:include>
	</div>
	<div id="titulares" class="tab-pane fade">
		<!-- ABM de titulares (reutilizable) -->
		<s:include value="../grid/titular-abm-grid.jsp"></s:include>
	</div>
	<div id="responsable" class="tab-pane fade">
		<!-- Grilla de responsables pertenecientes a la cuenta -->
		<s:include value="../info/responsables-info.jsp"></s:include>
	</div>
</div>
