<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

<script type="text/javascript">
	function closeResponseDialog() {
		// Por compatabilidad con el dialogo	
	}

	$('.selectpicker').selectpicker({
		width : '100%'
	});

	/* Submit del formulario previa validacion */
	function dialog_submit() {
		$('#codadm-form').bootstrapValidator('resetForm');
		$('#codadm-form').bootstrapValidator('validate');

		if ($('#codadm-form').data('bootstrapValidator').isValid()) {
			loader(true, null);

			$.ajax({
				method : "POST",
				url : "commons/codAdm_update.action",
				data : $('#codadm-form').serialize(),
				traditional : true,
				success : function(res) {
					loader(false, null);
					$("#codadm-grid-data").bootgrid('reload');
					showModalDialog("Resultado", res, []);
				},
				error : function(res, textStatus, jqXHR) {
					loader(false, null);
					handleErrorMessages(res.responseJSON);
					showErrorModalDialog(false);
				}
			});
		}
	};

	$('.form-control').keypress(function(e) {
		if (e.which == 13) {
			e.preventDefault();
			return false; //<---- Add this line
		}
	});

	/* Configuracion de validador del formulario */
	$(document).ready(function() {

		$('#codadm-form').bootstrapValidator({
			framework : 'bootstrap',
			excluded : [ ':disabled' ],
			feedbackIcons : {
				invalid : 'glyphicon glyphicon-remove',
				validating : 'glyphicon glyphicon-refresh'
			},
			message : 'Datos invalidos',
			err : {
				container : 'tooltip'
			},
			fields : {
				'codigoAdministrativo.codigo' : {
					validators : {
						notEmpty : {
							message : 'Obligatorio'
						}
					}
				},
				'codigoAdministrativo.nombre' : {
					validators : {
						notEmpty : {
							message : 'Obligatorio'
						}
					}
				},
				'codigoAdministrativo.vigenteDesde' : {
					validators : {
						notEmpty : {
							message : 'Fecha de vigencia Obligatorio'
						}
					}
				}
			}
		}).on('error.field.bv', function(e, data) {
			// $(e.target)  --> The form instance
			// $(e.target).data('bootstrapValidator')
			//              --> The BootstrapValidator instance
			// data.field   --> The field name
			// data.element --> The new field element
			// data.options --> The new field options
		});
	});
</script>
<style type="text/css">
.has-feedback .form-control {
	top: 0;
	/* Para utilizar todo el area editable */
	padding-right: 10px;
}

.form-control-feedback {
	/* Para correr el icon a la derecha */
	right: -22px;
}
</style>
<form id="codadm-form">

	<fieldset>
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<p id="error" class="bg-danger"></p>
			</div>
		</div>
		<s:hidden id="f_id" name="codigoAdministrativo.id"></s:hidden>
		<div class="row">

			<div class="col-xs-6 col-sm-6 col-md-3 col-lg-2">
				<div class="form-group">
					<label for="f_codigo"><s:text name="codigo" /></label>
					<s:textfield id="f_codigo" class="form-control"
						name="codigoAdministrativo.codigo" />
				</div>
			</div>
			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
				<div class="form-group">
					<label for="f_nombre"><s:text name="nombre" /></label>
					<s:textfield id="f_nombre" class="form-control" maxlength="30"
						name="codigoAdministrativo.nombre" />

				</div>
			</div>

		</div>
		<div class="row">
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-2">
				<div class="form-group">
					<div class="input-group date" data-provide="datepicker" id="desde"
						data-date-format="dd/mm/yyyy" data-date-autoclose="true"
						data-date-orientation="bottom" data-date-language="es">
						<input type="text" class="form-control"
							placeholder="Vigente desde"
							value='<s:date name="codigoAdministrativo.vigenteDesde" format="dd/MM/yyyy" />'
							name="codigoAdministrativo.vigenteDesde">
						<div class="input-group-addon">
							<span class="glyphicon glyphicon-calendar"></span>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-2">
				<div class="form-group">
					<div class="input-group date" data-provide="datepicker" id="hasta"
						data-date-format="dd/mm/yyyy" data-date-autoclose="true"
						data-date-orientation="bottom" data-date-language="es">
						<input type="text" class="form-control"
							placeholder="Vigente Hasta"
							value='<s:date name="codigoAdministrativo.vigenteHasta" format="dd/MM/yyyy" />'
							name="codigoAdministrativo.vigenteHasta">
						<div class="input-group-addon">
							<span class="glyphicon glyphicon-calendar"></span>
						</div>
					</div>
				</div>
			</div>
		</div>

	</fieldset>

</form>


<script>
	

	
</script>