<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

<style>
.small-col {
	font-size: small;
}
</style>

<script src="js/select/bootstrap-select.min.js"></script>
<link rel="stylesheet" href="css/select/bootstrap-select.css">

<form id="codadm-search-form">
	<div
		style="background-color: #eeeeee; padding-top: 7.5px; height: 50px">

		<div class="col-xs-12 col-sm-12 col-md-3 col-lg-2">
			<div class="form-group">
				<div class="input-group date" data-provide="datepicker" id="desde"
					data-date-format="dd/mm/yyyy" data-date-autoclose="true"
					data-date-orientation="bottom" data-date-language="es"
					data-date-today-btn="true" data-date-today-highlight="true">
					<input type="text" class="form-control"
						placeholder="Vigencia desde" value='<s:property value="from"/>'
						name="from">
					<div class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"></span>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-3 col-lg-2">
			<div class="form-group">
				<div class="input-group date" data-provide="datepicker" id="desde"
					data-date-format="dd/mm/yyyy" data-date-autoclose="true"
					data-date-orientation="bottom" data-date-language="es"
					data-date-today-btn="true" data-date-today-highlight="true">
					<input type="text" class="form-control"
						placeholder="Vigencia hasta" value='<s:property value="to"/>'
						name="to">
					<div class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"></span>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-4 col-lg-3">
			<input type="text" class="form-control" placeholder="Codigo"
				value='<s:property value="codigo"/>' name="codigo">
		</div>
		<div class="col-xs-12 col-sm-12 col-md-4 col-lg-1">
			<span class="input-group-btn">
				<button id="search_btn" class="btn btn-default" type="button"
					onclick="search();"
					data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Buscando">
					<span class="glyphicon glyphicon-search"></span>
					<s:text name="search.label"></s:text>
				</button>
			</span>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-4 col-lg-1">
			<span class="input-group-btn">
				<button id="add_btn" class="btn btn-default" type="button"
					onclick="add();">
					<span class="glyphicon glyphicon-plus"></span>
					<s:text name="dialog.label.agregar"></s:text>
				</button>
			</span>
		</div>

	</div>
</form>

<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<table id="codadm-grid-data"
			class="table table-condensed table-hover table-striped">
			<thead>
				<tr>
					<th data-column-id="id" data-identifier="true" data-type="numeric"
						data-visible="false">idCodAdm</th>
					<th data-column-id="codigo" data-width="8%" data-sortable="false"><s:text
							name="codadm.codigo" /></th>
					<th data-column-id="nombre" data-width="18%" data-sortable="false"><s:text
							name="codadm.nombre" /></th>
					<th data-column-id="vigenteDesde" data-type="date" data-width="7%"><s:text
							name="codadm.vigencia.desde" /></th>
					<th data-column-id="vigenteHasta" data-type="date" data-width="7%"><s:text
							name="codadm.vigencia.hasta" /></th>

					<th data-column-id="createdDate" data-converter="datetime"
						data-width="8%"><s:text name="fecha.alta.label" /></th>
					<th data-column-id="createdBy" data-type="string" data-width="10%"
						data-formatter="fmt_small"><s:text name="usuario.alta.label" /></th>
					<th data-column-id="modifiedDate" data-converter="datetime"
						data-width="8%"><s:text name="fecha.modifico.label" /></th>
					<th data-column-id="modifiedBy" data-formatter="fmt_small"
						data-width="10%"><s:text name="usuario.modifico.label" /></th>

					<th data-column-id="commands" data-sortable="false"
						data-formatter="commands" data-width="5%"></th>
				</tr>
			</thead>

		</table>
	</div>
</div>


<script>
	function add() {
		loader(true, $('#add_btn'));

		$.ajax({
			method : "POST",
			url : "commons/codAdm_goToAdd.action",
			traditional : true,
			success : function(res) {
				loader(false, $('#add_btn'));
				var btn_array = [ "submit-dialog-button" ];
				showModalDialog("Nuevo C�digo Administrativo", res, btn_array);
			},
			error : function(res, textStatus, jqXHR) {
				loader(false, $('#add_btn'));
				handleErrorMessages(res.responseJSON);
				showErrorModalDialog(false);
			}
		});
	};

	function edit_codAdm(id) {
		loader(true, null);

		$
				.ajax({
					method : "POST",
					url : "commons/codAdm_goToEdit.action",
					data : {
						idCodAdm : id
					},
					traditional : true,
					success : function(res) {
						loader(false, null);
						var btn_array = [ "submit-dialog-button" ];
						showModalDialog("Edici�n C�digo Administrativo", res,
								btn_array);
					},
					error : function(res, textStatus, jqXHR) {
						loader(false, null);
						handleErrorMessages(res.responseJSON);
						showErrorModalDialog(false);
					}
				});
	};

	function search() {
		loader(true, $('#search_btn'));

		$.ajax({
			method : "POST",
			url : "commons/codAdm_search.action",
			data : $('#codadm-search-form').serialize(),
			traditional : true,
			success : function(res) {
				loader(false, $('#search_btn'));
				$("#codadm-grid-data").bootgrid('reload');
			},
			error : function(res, textStatus, jqXHR) {
				loader(false, $('#search_btn'));
				handleErrorMessages(res.responseJSON);
				showErrorModalDialog(false);
			}
		});
	};

	$('.selectpicker').selectpicker({
		width : '100%'
	});

	$(document)
			.ready(
					function() {
						$("#codadm-grid-data")
								.bootgrid(
										{
											ajax : true,
											navigation : 2,
											rowCount : 15,
											rowSelect : false,
											selection : false,
											multiSelect : false,
											search : false,
											columnSelection : false,
											templates : {
												search : ""
											},
											labels : {
												noResults : "No se encontraron codigos administrivos.",
												loading : "Cargando informaci�n...",
												infos : "Mostrando {{ctx.start}} a {{ctx.end}} de {{ctx.total}} filas"
											},
											formatters : {
												nested : function(column, row) {
													return row.cuenta[column.id];
												},
												commands : function(column, row) {
													var idrow = row.id;
													return "<button onclick=\"edit_codAdm('"
															+ idrow
															+ "');"
															+ "\" type=\"button\" class=\"btn btn-xs btn-default command-edit\" data-row-id=\""
															+ idrow
															+ "\"><span class=\"fa fa-info-circle\" aria-hidden=\"true\"></span></button> ";
												},
												fmt_status : function(column,
														row) {
													return fmt_status(row[column.id]);
												},
												fmt_small : function(column,
														row) {
													return "<small>"
															+ row[column.id]
															+ "</small>";
												}
											},
											converters : {
												date : {
													from : function(value) {
														return moment(value);
													},
													to : function(value) {
														if (!value) {
															return "";
														}
														return moment(value)
																.format(
																		"DD-MM-YYYY");
													}
												},
												datetime : {
													from : function(value) {
														return moment(value);
													},
													to : function(value) {
														return "<small>"
																+ moment(value)
																		.format(
																				"DD-MM-YYYY HH:mm")
																+ "</small>";
													}
												}
											},
											url : "commons/codAdm_getData.action"
										}).on("loaded.rs.jquery.bootgrid",
										function() {

										}).on("click.rs.jquery.bootgrid",
										function(e, columns, row) {
											// Mostrar informacion del file clickeado
											//
										});

					});

	
</script>