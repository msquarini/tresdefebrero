<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>
<html lang="es">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="keywords" content="Tres de febrero, Catastro WEB" />
<meta name="description"
	content="Municipalidad de Tres de Febrero - Catastro WEB" />
<title>Municipalidad de Tres de Febrero</title>

<!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
<!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<sb:head includeScripts="true" includeScriptsValidation="true"
	compressed="false" includeStyles="true" />
<sj:head locale="es" compressed="true" loadAtOnce="true" jqueryui="true"
	loadFromGoogle="false" />

<style type="text/css">
#number_container {
	border: 1px dotted #d0d0d0;
	padding: 15px;
	margin: 10px;
	display: none;
	background: #fafafa;
}

body {
	padding-top: 70px;
	/* 60px to make the container go all the way to the bottom of the topbar */
}

.navbar-brand,
.navbar-nav li a {
    line-height: 80px;
    height: 80px;
    padding-top: 0;
}

.top-buffer {
	margin-top: 50px;
}
</style>
</head>

<body>
	<nav class="navbar navbar-default navbar-fixed-top" >
	<div class="container-fluid">
		  <div class="navbar-header">
			<a class="navbar-brand" href="http://www.tresdefebrero.gov.ar"> <img
				src="../img/logo-new1.png" alt="" height="75" width="180">
			</a>
		</div>
		</div>
	</nav>

	<div class="container top-buffer" id="container">
		
	</div>



	<footer class="navbar-default top-buffer">

		<p align="center">
			� Copyright 2015 - 2016 - <strong>MUNICIPALIDAD DE TRES DE
				FEBRERO</strong> - Conmutador: 4734-2400 - Todos los derechos reservados
		</p>
	</footer>


</body>
</html>
<script type="text/javascript">
$.ajax({

    type: "POST",
    url: '/catastro-web/login_input.action',
    success: function(data) {
          // data is ur summary
         $('#container').html(data);
    }
});    
</script>