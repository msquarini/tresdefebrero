<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%-- <%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%> --%>

<script type="text/javascript">	
</script>

<style type="text/css">
</style>

<ul class="nav nav-pills" style="background-color: #eeeeee; border-bottom-style: ridge;">
	<li class="nav-header disabled"><a><strong>Informacion Deuda</strong></a></li>

	<li class="active"><a data-toggle="tab" href="#periodos"><s:text name="deuda.periodos.label"/></a></li>
	<%-- <li><a data-toggle="tab" href="#pagos"><s:text name="deuda.pagos.label"/></a></li> --%>
	<li><a data-toggle="tab" href="#da"><s:text name="deuda.da.label"/></a></li>
</ul>

<div class="tab-content">
	<div id="periodos" class="tab-pane fade in active">
		<s:include value="periodo-grid.jsp"></s:include>
	</div>

<%-- 	<div id="pagos" class="tab-pane fade">
		<s:push value="%{#session.cuenta.parcela.lineaVigente}">
			<s:include value="linea-info.jsp"></s:include>
		</s:push>
	</div>
 --%>
	<div id="da" class="tab-pane fade">
<%-- 		<s:include value="../grid/titular-grid.jsp"></s:include> --%>
	</div>
	
</div>
