<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<script type="text/javascript">
	function download(id) {
		/* $.ajax({
			method : "POST",
			url : "catastro/periodo_download.action",
			data : {
				idFileDeuda : id
			},
			traditional : true,
			success : function(res) {
				loader(false, null);
			},
			error : function(res, textStatus, jqXHR) {
				loader(false, null);
				handleErrorMessages(res.responseJSON);
				showErrorModalDialog(false);
			}
		}); */
		$('#idFileDeuda').val(id);
		$('#file-deuda-download').submit();
	}
</script>

<style type="text/css">
input[readonly].info {
	background-color: transparent;
	border: 1;
	font-size: 1em;
}

.number-right {
    text-align: right;
}
</style>

<!--  Formulario para descarga de files -->
<s:form id="file-deuda-download" action="periodo_download"
	cssClass="form-vertical" namespace="/catastro">
	<s:hidden id="idFileDeuda" name="idFileDeuda"></s:hidden>
</s:form>


<div class="row">

	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

		<div class="col-xs-6 col-sm-6 col-md-4 col-lg-2">
			<label for="mincc"><s:text name="anio.label" /></label> <input
				class="form-control info" type="text" readonly
				value='<s:property value="periodoInfo.periodo.anio"></s:property>' />
		</div>
		<div class="col-xs-6 col-sm-6 col-md-4 col-lg-1">
			<label for="avgcc"><s:text name="cuota.label" /></label> <input
				class="form-control info" type="text" readonly
				value='<s:property value="periodoInfo.periodo.cuota"></s:property>' />
		</div>
		<div class="col-xs-6 col-sm-6 col-md-4 col-lg-2">
			<label for="maxcc"><s:text name="deuda.fechaVto.label" /></label> <input
				class="form-control info" type="text" readonly
				value='<s:property value="getText('format.date',{periodoInfo.periodo.fechaVto})"></s:property>' />
		</div>
		<%-- 				<div class="col-xs-6 col-sm-6 col-md-4 col-lg-2">
					<label for="cant"><s:text name="deuda.fechaVto2.label" /></label>
					<input class="form-control info" type="text" readonly
						value='<s:property value="getText('format.date',{periodoInfo.periodo.fechaSegundoVto})"></s:property>' />
				</div> --%>
		<div class="col-xs-6 col-sm-6 col-md-4 col-lg-2">
			<label for="cant"><s:text name="periodo.estado.label" /></label>
			<div style="margin-top: -5px;">
				<h4 id="estado_periodo">
					<s:property value="periodoInfo.periodo.estado" />
				</h4>
			</div>
		</div>
		<div class="col-xs-6 col-sm-6 col-md-4 col-lg-2">
			<label for="cant"><s:text name="periodo.ajuste.label" /></label> <input
				class="form-control info" type="text" readonly
				value='<s:property value="getText('number.format', {periodoInfo.periodo.porcentajeAjuste})"></s:property>' />
		</div>
		<div class="col-xs-6 col-sm-6 col-md-4 col-lg-2">
			<s:if test="periodoInfo.periodo.generarAnual">
				<label for="cant"><s:text name="periodo.cuotaanual.label" /></label>
				<div style="margin-top: -5px;">
					<h4>
						<span class='label label-info'> <i
							class="fa fa-check-square-o" aria-hidden="true"></i> # Periodos:
							<s:property value="periodoInfo.periodo.periodosAnual" /> - #
							Cuota: <s:property value="periodoInfo.periodo.numeroCuotaAnual" /></span>
					</h4>
				</div>
			</s:if>
		</div>
	</div>
</div>

<div class="row top-buffer">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<table id="periodo-file-deuda-grid"
			class="table table-condensed table-hover table-striped">
			<thead>
				<tr>
					<th data-column-id="id" data-identifier="true" data-type="numeric"
						data-visible="false">idFileDeuda</th>
					<th data-column-id="tipo" data-width="10%" data-sortable="false">Tipo</th>
					<th data-column-id="filename" data-width="30%"
						data-sortable="false">Nombre archivo</th>
					<th data-column-id="modifiedDate" data-width="20%"
						data-type="datetime" data-sortable="false">Fecha Modif.</th>
					<th data-column-id="commands" data-width="20%"
						data-sortable="false" data-formatter="commands"
						data-header-align="center" data-align="center">Acciones</th>
				</tr>
			</thead>
			<tbody>
				<s:iterator value="periodoInfo.periodo.fileDeuda">
					<tr>
						<td><s:property value="id"></s:property></td>
						<td><s:property value="tipo"></s:property></td>
						<td><s:property value="filename"></s:property></td>
						<td><s:property
								value='getText("format.datetime",{modifiedDate})'></s:property></td>
						<td></td>
					</tr>
				</s:iterator>
			</tbody>
		</table>
	</div>
</div>


<div class="row top-buffer">

	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="panel panel-info">
			<div class="panel-heading">
				<strong><s:text name="deuda.cuotas.label" /></strong>
			</div>
			<div class="panel-body">
				<div class="col-xs-6 col-sm-6 col-md-4 col-lg-3">
					<label for="mincc"><s:text name="deuda.minimo" /></label>
					<div class="input-group">
						<span class="input-group-addon">$</span> <input
							class="form-control info number-right" type="text" readonly
							value='<s:property value="getText('money.format',{periodoInfo.minImporte})"></s:property>' />
					</div>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-4 col-lg-3">
					<label for="avgcc"><s:text name="deuda.promedio" /></label>
					<div class="input-group">
						<span class="input-group-addon">$</span> <input
							class="form-control info number-right" type="text" readonly
							value='<s:property value="getText('money.format',{periodoInfo.avgImporte})"></s:property>' />
					</div>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-4 col-lg-3">
					<label for="maxcc"><s:text name="deuda.maximo" /></label>
					<div class="input-group">
						<span class="input-group-addon">$</span> <input
							class="form-control info number-right" type="text" readonly
							value='<s:property value="getText('money.format',{periodoInfo.maxImporte})"></s:property>' />
					</div>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-4 col-lg-3">
					<label for="cant"><s:text name="deuda.cantidad" /></label> <input
						class="form-control info number-right" type="text" readonly
						value='<s:property value="periodoInfo.cantidadCC"></s:property>' />
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="panel panel-info">
			<div class="panel-heading">
				<strong><s:text name="deuda.cuotasanual.label" /></strong>
			</div>
			<div class="panel-body">
				<div class="col-xs-6 col-sm-6 col-md-4 col-lg-3">
					<label for="minca"><s:text name="deuda.minimo" /></label>
					<div class="input-group">
						<span class="input-group-addon">$</span> <input
							class="form-control info number-right" type="text" readonly
							value="<s:property value="getText('money.format',{periodoInfo.minImporteCA})"></s:property>" />
					</div>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-4 col-lg-3">
					<label for="avgca"><s:text name="deuda.promedio" /></label>
					<div class="input-group">
						<span class="input-group-addon">$</span> <input
							class="form-control info number-right" type="text" readonly
							value="<s:property value="getText('money.format',{periodoInfo.avgImporteCA})"></s:property>" />
					</div>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-4 col-lg-3">
					<label for="maxca"><s:text name="deuda.maximo" /></label>
					<div class="input-group">
						<span class="input-group-addon">$</span> <input
							class="form-control info number-right" type="text" readonly
							value="<s:property value="getText('money.format',{periodoInfo.maxImporteCA})"></s:property>" />
					</div>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-4 col-lg-3">
					<label for="cantca"><s:text name="deuda.cantidad" /></label> <input
						class="form-control info number-right" type="text" readonly
						value='<s:property value="periodoInfo.cantidadCA"></s:property>' />
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$(document)
			.ready(
					function() {
						aux = fmt_status($('#estado_periodo').text());
						$('#estado_periodo').html(aux);

						$("#periodo-file-deuda-grid")
								.bootgrid(
										{
											ajax : false,
											navigation : 0,
											rowCount : 5,
											search : false,
											columnSelection : false,
											templates : {
												search : ""
											},
											labels : {
												noResults : "No se encontraron archivos de deuda para descargar.",
												loading : "Cargando información...",
												infos : "Mostrando {{ctx.start}} a {{ctx.end}} de {{ctx.total}} filas"
											},
											converters : {
												datetime : {
													from : function(value) {
														return moment(value);
													},
													to : function(value) {
														return moment(value)
																.format(
																		"DD-MM-YYYY HH:mm:ss");
													}
												}
											},
											formatters : {
												commands : function(column, row) {
													return cmd_download_file_deuda(row);
													;
												}
											}
										});
					});

	
</script>