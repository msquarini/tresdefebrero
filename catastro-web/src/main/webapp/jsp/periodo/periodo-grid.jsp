<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<!-- Grilla de periodos de deuda -->
<style>
.selected {
	background-color: Gray !important;
}
</style>


<script type="text/javascript">
	function reload() {
		loader(true, $('#reload_btn'));
		$("#periodos-grid-data").bootgrid('reload');
	}

	function emision() {
		loader(true, $('#emision_btn'));
		$.ajax({
			method : "POST",
			url : "catastro/periodo_emision.action",
			data : {},
			traditional : true,
			success : function(res) {
				loader(false, $('#emision_btn'));
			},
			error : function(res, textStatus, jqXHR) {
				loader(false, $('#emision_btn'));
				handleErrorMessages(res.responseJSON);
				showErrorModalDialog(false);
			}
		});
	}

	function generar_boleta(id) {
		loader(true);		
		$.ajax({
			method : "POST",
			url : "catastro/periodo_generarBoletaPeriodo.action",
			data : {
				idPeriodo : id
			},
			traditional : true,
			success : function(res) {
				loader(false);
			},
			error : function(res, textStatus, jqXHR) {
				loader(false);
				handleErrorMessages(res.responseJSON);
				showErrorModalDialog(false);
			}
		});
	}

	/* Botones de accion */
	function cmd_periodo_generar_deuda(row) {
		/* if ([ 'PENDIENTE', 'ERROR' ].includes(row.estado)) {
			return "<button onclick=\"generar_deuda('"
					+ row.id
					+ "');\" type=\"button\" class=\"btn btn-xs btn-default command-edit\" data-toggle=\"tooltip\" title=\"Generar deuda\""
					+ "data-row-id=\"" + row.id
					+ "\"><span class=\"icon glyphicon glyphicon-cog\"></span></button> ";
		} */
		if ([ 'PENDIENTE', 'ERROR' ].includes(row.boletaStatus)) {
			return "<button onclick=\"generar_boleta('"
					+ row.id
					+ "');\" type=\"button\" class=\"btn btn-xs btn-default command-edit stop-click-event\" data-toggle=\"tooltip\" title=\"Generar boletas\""
					+ "data-row-id=\"" + row.id
					+ "\"><span class=\"icon glyphicon glyphicon-cog\"></span></button> ";
		}
	}

	function periodoDetail(id) {
		$.ajax({
			method : "POST",
			url : "catastro/periodo_infoPeriodo.action",
			data : {
				idPeriodo : id
			},
			traditional : true,
			success : function(res) {
				loader(false, null);
				showModalDialog('Detalle de emisi�n del periodo', res, false);
			},
			error : function(res, textStatus, jqXHR) {
				loader(false, null);
				handleErrorMessages(res.responseJSON);
				showErrorModalDialog(false);
			}
		});
	}

	$("#anio").datepicker({
		format : "yyyy", // Notice the Extra space at the beginning
		viewMode : "years",
		minViewMode : "years"
	});

	$('input.number').number(true, 0, '', '');

	$("#anio").on("change", function() {
		if ($(this).val().length == 4) {
			//alert($(this).val());
			$("#periodos-grid-data").bootgrid('reload');
			$("#periodo-detail").html("");
		}
	});

	$(document).ready(function() {
		$('[data-toggle="tooltip"]').tooltip();
	});
</script>

<div class="top-buffer"
	style="background-color: #eeeeee; padding-top: 7.5px; height: 50px">
	<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
		<input id="anio" type="text" class="form-control number"
			placeholder="Filtrar por a�o" maxlength="4" />
	</div>


	<div class="col-xs-12 col-sm-12 col-md-4 col-lg-2">
		<span class="input-group-btn">
			<button id="reload_btn" class="btn btn-default" type="button"
				data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Actualizando..."
				onclick="reload();">
				<span class="fa fa-refresh"></span> Actualizar
			</button>
		</span>
	</div>

	<div class="col-xs-12 col-sm-12 col-md-4 col-lg-2 col-lg-offset-6"
		align="right">
		<span class="input-group-btn">
			<button id="emision_btn" class="btn btn-danger" type="button"
				data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Procesando..."
				onclick="emision();">
				<span class="fa fa-cog"></span> Emisi�n
			</button>
		</span>
	</div>
</div>

<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<table id="periodos-grid-data"
			class="table table-condensed table-hover table-striped">
			<thead>
				<tr>
					<th data-column-id="id" data-identifier="true" data-type="numeric"
						data-visible="false">idPeriodo</th>
					<th data-column-id="anio" data-type="numeric" data-visible="true"><s:text
							name="anio.label" /></th>
					<th data-column-id="cuota" data-type="numeric" data-visible="true"><s:text
							name="cuota.label" /></th>
					<th data-column-id="fechaVto" data-type="date" data-visible="true"><s:text
							name="deuda.fechaVto.label" /></th>
					<th data-column-id="estado" data-formatter="fmt_status"
						data-visible="true"><s:text name="periodo.estado.label" /></th>
					<th data-column-id="boletaStatus" data-formatter="fmt_status"
						data-visible="true"><s:text name="periodo.boleta.label" /></th>
					<th data-column-id="modifiedBy" data-type="string"><s:text
							name="usuario.modifico.label" /></th>
					<th data-column-id="modifiedDate" data-type="datetime"><s:text
							name="fecha.modifico.label" /></th>
					<th data-column-id="commands" data-formatter="commands"
						data-header-align="center" data-align="center"
						data-sortable="false"><s:text name="label.acciones" /></th>
				</tr>
			</thead>
		</table>
	</div>
</div>

<div class="row">
	<div id="periodo-detail" class="container"></div>
</div>
<script>
	$(document)
			.ready(
					function() {
						$("#periodos-grid-data")
								.bootgrid(
										{
											ajax : true,
											navigation : 0,
											rowCount : 12,
											rowSelect : true,
											//selection : true,
											multiSelect : false,
											search : true,
											columnSelection : false,
											templates : {
												search : ""
											},
											labels : {
												noResults : "No hay periodos",
												loading : "Cargando informaci�n...",
												infos : "Mostrando {{ctx.start}} a {{ctx.end}} de {{ctx.total}} filas"
											},
											url : "catastro/periodo_busquedaPeriodos.action",
											// TODO VALIDAR!!!!!!
											responseHandler : function(data) {
												var response = {
													current : 1,
													rowCount : 12,
													rows : data.periodos,
													total : data.length
												};
												return response;
											},
											requestHandler : function(rq) {
												rq.anio = $("#anio").val();
												return sortRequest(rq);
											},
											converters : {
												date : {
													from : function(value) {
														return moment(value);
													},
													to : function(value) {
														return moment(value)
																.format(
																		"DD-MM-YYYY");
													}
												},
												datetime : {
													from : function(value) {
														return moment(value);
													},
													to : function(value) {
														return moment(value)
																.format(
																		"DD-MM-YYYY HH:mm:ss");
													}
												}

											},
											formatters : {
												commands : function(column, row) {
													// var idrow = row.id;
													return cmd_periodo_generar_deuda(row);
												},
												/* fmt_boleta : function(column,
												        row) {
												    return fmt_boleta(row[column.id]);
												}, */
												fmt_status : function(column,
														row) {
													return fmt_status(row[column.id]);
												}
											}
										}).on("click.rs.jquery.bootgrid",
										function(e, columns, row, target) {
											// Mostrar informacion detallada del periodo clickeado
											periodoDetail(row.id);
										}).on("loaded.rs.jquery.bootgrid",
										function(e, columns, row) {
											loader(false, $('#reload_btn'));
											
											$(".stop-click-event").click(function(e) {
										        e.preventDefault();
										        e.stopPropagation();
										    });
										});
					});

	
</script>