<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<style type="text/css">
</style>


<!-- Modal Medium (para ERRORES)-->
<div id="error-dialog" class="modal fade" tabindex="-1" role="dialog"
	aria-labelledby="generic-data-label">
	<div class="modal-dialog modal-md" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="generic-data-label">
					<s:property value="tituloError"></s:property>
				</h4>
			</div>
			<div id="error-dialog-body" class="modal-body">
				<div id="errors-data" class="alert alert-danger"
					style="display: ">
					<s:property value="mensajeError"></s:property>
				</div>
				<div id="warning-data" class="alert alert-warning"
					style="display: none;"></div>
				<div id="info-data" class="alert alert-info" style="display: none;"></div>
				<div id="success-data" class="alert alert-success"
					style="display: none;"></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"
					onclick="closeResponseDialog();">Cerrar</button>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$('#error-dialog').modal('show');

	function closeResponseDialog() {
		$('#error-dialog').modal('hide');
	}
</script>