<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<script type="text/javascript">
	/* Seleciona tipo de unificacion */
	function cambioTipoUnificacion() {
		if ($('#tipoUnificacion').val() < 3) {
			$('#codorg').addClass('hidden');
			$('#folio').addClass('hidden');
			$('#origen_numero').removeClass('hidden');
			$('#origen_anio').removeClass('hidden');
		} else if ($('#tipoUnificacion').val() == 3) {
			$('#folio').removeClass('hidden');
			$('#codorg').addClass('hidden');
		} else if ($('#tipoUnificacion').val() == 4) {
			$('#codorg').removeClass('hidden');
			$('#folio').addClass('hidden');
		} else {
			$('#folio').addClass('hidden');
			$('#codorg').addClass('hidden');
			$('#origen_numero').addClass('hidden');
			$('#origen_anio').addClass('hidden');
		}

		if ($.inArray($('#tipoUnificacion').val(), [ '3', '5' ]) > -1) {
			$('#div_uf').addClass('hidden');
		} else {
			$('#div_uf').removeClass('hidden');
		}
	}

	$('#destinoMunicipal').checkboxpicker({
		offLabel : 'No',
		onLabel : 'Si'
	}).on(
			'change',
			function() {
				$('#destino').prop('disabled',
						!$('#destinoMunicipal').prop('checked'));

				if ($(this).is(':checked')) {
					$(this).prop('value', 'true');
				} else {
					$(this).prop('value', 'false');
				}
			});

	/* Submit del formulario previa validacion */
	/* 	function dialog_submit() {
	 $('#unificacion-parcela-form').bootstrapValidator('resetForm');
	 $('#unificacion-parcela-form').bootstrapValidator('validate');

	 if ($('#unificacion-parcela-form').data('bootstrapValidator').isValid()) {
	 $('#unificacion-parcela-form').submit();
	 }
	 }; */

	/* Configuracion del formulario, ya que se hace submit del mismo mediante javascript */
	$('#unificacion-parcela-form').ajaxForm({
		target : '#generic-modal-response-body',
		success : function(res, textStatus, jqXHR) {
			loader(false, $('#btn_unificacion_process'));
			showResponseModalDialog(true);
		},
		error : function(res, textStatus, jqXHR) {
			loader(false, $('#btn_unificacion_process'));
			// Levantar dialogo con mensaje de error!!!!!!!!!!!!!
			handleErrorMessages(res.responseJSON);
			showErrorModalDialog(false);
		}
	});

	/* Configuracion de validador del formulario */
	$(document)
			.ready(
					function() {
						if ($('#destinoMunicipal').prop('value') == 'true') {
							$('#destinoMunicipal').prop('checked', 'true');
						}

						$('#tf_oficio').checkboxpicker({
							offLabel : 'No',
							onLabel : 'Si'
						}).on('change', function() {
							if ($(this).is(':checked')) {
								$(this).prop('value', 'true');
							} else {
								$(this).prop('value', 'false');
							}
						});

						if ($('#tf_oficio').prop('value') == 'true') {
							$('#tf_oficio').prop('checked', 'true');
						}
						
						$('input.number').number(true, 0, '', '');
						$('input.numberdecimal').number(true, 2, ',', '');

						// Set up the number formatting.
						//$('#nuevaParcela.nomenclatura.circunscripcion').number(
						//		true, 0, '', '');

						// Actualizar los campos 
						cambioTipoUnificacion();
						updateValuacion();
						
						$('#unificacion-parcela-form')
								.bootstrapValidator(
										{
											framework : 'bootstrap',
											excluded : [ ':disabled', ':hidden' ],
											//excluded : [ ':disabled' ],
											feedbackIcons : {
												//valid : 'glyphicon glyphicon-ok',
												invalid : 'glyphicon glyphicon-remove',
												validating : 'glyphicon glyphicon-refresh'
											},
											message : 'Datos invalidos',
											/* submitHandler: function(validator, form, submitButton) {
													
											}, */
											fields : {
												'tramite.numero' : {
													validators : {
														notEmpty : {
															message : 'Dato Obligatorio'
														},
														integer : {
															message : 'El valor ingresado debe ser num�rico'
														}
													}
												},
												'tramite.anio' : {
													validators : {
														notEmpty : {
															message : 'Dato Obligatorio'
														},
														greaterThan : {
															value : 1900,
															message : 'El valor ingresado debe ser mayor que 1900'
														}
													}
												},
												'nuevaParcela.nomenclatura.circunscripcion' : {
													validators : {
														notEmpty : {
															message : 'Obligatorio'
														}
													}
												},
												'nuevaParcela.nomenclatura.seccion' : {
													validators : {
														notEmpty : {
															message : 'Obligatorio'
														}
													}
												},
												'nuevaParcela.nomenclatura.parcela' : {
													validators : {
														notEmpty : {
															message : 'Obligatorio'
														}
													}
												},
												'nuevaParcela.partida' : {
													validators : {
														notEmpty : {
															message : 'Partida Obligatorio'
														},
														integer : {
															message : 'Debe ser un valor n�merico de 6 d�gitos'
														}
													}
												},
												'nuevaParcela.dominio.tipo' : {
													validators : {
														notEmpty : {
															message : 'Obligatorio'
														}
													}
												},
												'nuevaParcela.dominio.numero' : {
													validators : {
														notEmpty : {
															message : 'Obligatorio'
														}
													}
												},
												'nuevaParcela.lineaVigente.superficieTerreno' : {
													validators : {
														notEmpty : {
															message : 'Obligatorio'
														},
														greaterThan : {
															message : 'El valor debe ser mayor a 0',
															value : 0,
															inclusive : false
														}
													}
												},
												'nuevaParcela.lineaVigente.superficieCubierta' : {
													validators : {
														notEmpty : {
															message : 'Obligatorio'
														}
													}
												},
												'nuevaParcela.lineaVigente.superficieSemi' : {
													validators : {
														notEmpty : {
															message : 'Obligatorio'
														}
													}
												},
												'nuevaParcela.lineaVigente.valorTierra' : {
													validators : {
														notEmpty : {
															message : 'Obligatorio'
														},
														greaterThan : {
															message : 'El valor debe ser mayor a 0',
															value : 0,
															inclusive : false
														}
													}
												},
												'nuevaParcela.lineaVigente.valorComun' : {
													validators : {
														notEmpty : {
															message : 'Obligatorio'
														}
													}
												},
												'nuevaParcela.lineaVigente.valorEdificio' : {
													validators : {
														notEmpty : {
															message : 'Obligatorio'
														}
													}
												},
												'nuevaParcela.domicilioPostal.calle' : {
													validators : {
														notEmpty : {
															message : 'Obligatorio'
														}
													}
												},
												'nuevaParcela.domicilioInmueble.calle' : {
													validators : {
														notEmpty : {
															message : 'Obligatorio'
														}
													}
												},
												'nuevaParcela.domicilioPostal.numero' : {
													validators : {
														notEmpty : {
															message : 'Obligatorio'
														}
													}
												},
												'nuevaParcela.domicilioInmueble.numero' : {
													validators : {
														notEmpty : {
															message : 'Obligatorio'
														}
													}
												},
												'nuevaParcela.destino' : {
													validators : {
														notEmpty : {
															message : 'Obligatorio'
														}
													}
												}
											}
										});
					});

	function updateValuacion() {
		var v = parseFloat($('#valor_tierra').val())
				+ parseFloat($('#valor_comun').val())
				+ parseFloat($('#valor_edificio').val());
		$('#valuacion').text('$ ' + $.number(v, 2, ',', '.'));
		//$('#valuacion').text('$ ' + v.toFixed(2).replace('.', ','));
		//  $('#valuacion').text(v.toFixed(2));
		//  $('#valuacion').number(true, 2, ',', '.');
	}
</script>
<style type="text/css">
.has-feedback .form-control {
	top: 0;
	/* Para utilizar todo el area editable */
	padding-right: 10px;
}

.form-control-feedback {
	/* Para correr el icon a la derecha */
	right: -22px;
}

/*  #subdivision-parcela-form .has-feedback .input-group .form-control-feedback {
    top: 0;
    padding-right: 20;

} */
</style>
<s:form id="unificacion-parcela-form" action="unificacion_process"
	cssClass="form-vertical" namespace="/catastro" theme="bootstrap">

	<s:if test='tramite.estado.toString() != "FINALIZADO"'>
		<fieldset>
	</s:if>
	<s:else>
		<fieldset disabled>
	</s:else>
	<div class="row top-buffer input-group-sm">
		<div class="col-xs-6 col-sm-6 col-md-4 col-lg-2">
			<label for="tipoUnificacion"><s:text name="tipo.label" /></label>

			<s:select id="tipoUnificacion" class="form-control"
				list="#{1:'Regimen de propiedad horizontal', 2:'Geodesia', 3:'Estado Parcelario', 4:'Unificacion por Expediente', 5:'Unificacion por hechos existentes'}"
				headerValue="Seleccione Tipo " name="tramite.codigo"
				onchange="cambioTipoUnificacion()"></s:select>
		</div>
		<div id="codorg" class="hidden col-xs-6 col-sm-6 col-md-2 col-lg-2">
			<label for="codorg"><s:text name="organismo.codigo.label" /></label>
			<s:textfield id="origen_codorg" class="form-control number"
				name="tramite.codOrganismo" />
		</div>
		<div id="folio" class="hidden col-xs-6 col-sm-6 col-md-2 col-lg-2">
			<label for="folio"><s:text name="folio.label" /></label>
			<s:textfield id="origen_folio" class="form-control number"
				name="tramite.folio" />
		</div>
		<div id="origen_numero" class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
			<label for="origen_numero"><s:text name="numero.label" /></label>
			<s:textfield id="origen_numero" class="form-control number"
				name="tramite.numero" />
		</div>
		<div id="origen_anio" class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
			<div class="form-group">
				<label for="anio"><s:text name="anio.label" /></label>
				<div class="input-group date" data-provide="datepicker"
					data-date-format="yyyy" data-date-autoclose="true"
					data-date-view-mode="years" data-date-min-view-mode="years"
					data-date-end-date="y" data-date-orientation="bottom"
					data-date-language="es" data-date-today-btn="true"
					data-date-today-highlight="true">
					<input id="anio" type="text" class="form-control"
						name="tramite.anio" value="<s:property value='tramite.anio'/>">
					<div class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"></span>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xs-6 col-sm-6 col-md-3 col-lg-offset-2 col-lg-2"
			style="flex: 1; padding: 25px;">
			<label for="tf_oficio"><s:text name="tramite.oficio.label" />&nbsp;</label><input
				id="tf_oficio" type="checkbox" name="tramite.oficio"
				value="<s:property value="tramite.oficio" />" />
		</div>
	</div>

	<div class="row  input-group-sm">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<textarea class="form-control" rows="2" placeholder="Observaciones"
				name="tramite.observacion" id="tf_observacion"><s:property
					value="tramite.observacion" /></textarea>
		</div>
	</div>

	<div class="row  ">
		<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
			<label for="circunscripcion"><s:text name="circ.short.label" /></label>
			<s:textfield id="circunscripcion"
				class="form-control input-sm number"
				name="nuevaParcela.nomenclatura.circunscripcion" maxlength="3"
				readonly="true" />
		</div>
		<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
			<label for="seccion"><s:text name="sec.short.label" /></label>
			<s:textfield id="seccion" class="form-control input-sm" maxlength="3"
				name="nuevaParcela.nomenclatura.seccion" readonly="true" />
		</div>
		<div class="col-xs-6 col-sm-6 col-md-1 col-lg-1">
			<label for="fraccion"><s:text name="fr.short.label" /></label>
			<s:textfield id="fraccion" class="form-control input-sm number"
				readonly="true" maxlength="3"
				name="nuevaParcela.nomenclatura.fraccion" />
		</div>
		<div class="col-xs-6 col-sm-6 col-md-1 col-lg-1">
			<label for="fraccionLetra"><s:text name="frl.short.label" /></label>
			<s:textfield id="fraccionLetra" class="form-control input-sm"
				readonly="true" maxlength="3"
				name="nuevaParcela.nomenclatura.fraccionLetra" />
		</div>
		<div class="col-xs-6 col-sm-6 col-md-1 col-lg-1">
			<label for="manzana"><s:text name="mz.short.label" /></label>
			<s:textfield id="manzana" class="form-control input-sm number"
				readonly="true" maxlength="3"
				name="nuevaParcela.nomenclatura.manzana" />
		</div>
		<div class="col-xs-6 col-sm-6 col-md-1 col-lg-1">
			<label for="manzanaLetra"><s:text name="mzl.short.label" /></label>
			<s:textfield id="manzanaLetra" class="form-control input-sm"
				readonly="true" maxlength="3"
				name="nuevaParcela.nomenclatura.manzanaLetra" />
		</div>
		<div class="col-xs-6 col-sm-6 col-md-1 col-lg-1">
			<label for="parcela"><s:text name="pc.short.label" /></label>
			<s:textfield id="parcela" class="form-control input-sm number"
				maxlength="3" name="nuevaParcela.nomenclatura.parcela" />
		</div>
		<div class="col-xs-6 col-sm-6 col-md-1 col-lg-1">
			<label for="parcelaLetra"><s:text name="pcl.short.label" /></label>
			<s:textfield id="parcelaLetra" class="form-control input-sm"
				maxlength="3" name="nuevaParcela.nomenclatura.parcelaLetra" />
		</div>
		<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
			<div id="div_uf">
				<label for="unidadFuncional"><s:text name="uf.short.label" /></label>
				<s:textfield id="unidadFuncional" class="form-control input-sm"
					maxlength="6" name="nuevaParcela.nomenclatura.unidadFuncional" />
			</div>
		</div>
	</div>

	<!-- Partida | Zona | Categoria -->
	<div class="row">

		<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">

			<div class="panel panel-default">
				<div class="panel-heading">
					<s:text name="dominio.panel.label" />
				</div>
				<div class="panel-body">

					<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
						<label for="inscripcion"><s:text name="dominio.tipo.label" /></label>
						<select id="inscripcion" name="nuevaParcela.dominio.tipo"
							class="form-control" onchange="cambioTipoInscripcion()">
							<option value="M"><s:text name="matricula.label" /></option>
							<option value="F"><s:text name="folio.label" /></option>
						</select>
						<%-- <s:textfield id="inscripcion" name="nuevaParcela.dominio.tipo" /> --%>
					</div>
					<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
						<label for="numero"><s:text name="dominio.numero.label" /></label>
						<s:textfield id="numero" name="nuevaParcela.dominio.numero"
							class="form-control number" maxlength="6" />
					</div>
					<div id="div_anio"
						class="col-xs-6 col-sm-6 col-md-4 col-lg-4 hidden">
						<label for="anio"><s:text name="dominio.anio.label" /></label>
						<div class="input-group date" data-provide="datepicker"
							data-date-format="yyyy" data-date-autoclose="true"
							data-date-view-mode="years" data-date-min-view-mode="years"
							data-date-end-date="y" data-date-orientation="bottom"
							data-date-language="es" data-date-today-btn="true"
							data-date-today-highlight="true">
							<input id="anio" type="text" class="form-control"
								name="nuevaParcela.dominio.anio"
								value="<s:property value='nuevaParcela.dominio.anio'/>">
							<div class="input-group-addon">
								<span class="glyphicon glyphicon-calendar"></span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">

			<div class="panel panel-default">
				<div class="panel-heading">
					<s:text name="parcela.panel.label" />
				</div>
				<div class="panel-body">

					<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
						<label for="partida"><s:text name="partida.label" /></label>
						<s:textfield id="partida" class="form-control number"
							maxlength="6" name="nuevaParcela.partida" />
					</div>

					<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
						<label for="categoria"><s:text name="categoria.label" /></label>
						<select id="categoria" name="nuevaParcela.lineaVigente.categoria"
							class="form-control">
							<option value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
							<option value="5">5</option>
							<option value="6">6</option>
							<option value="7">7</option>
							<option value="8">8</option>
						</select>
					</div>

					<div class="col-xs-6 col-sm-6 col-md-5 col-lg-5">
						<label for="tf_vigencia"><s:text name="vigencia.label" /></label>
						<div class="input-group date" data-provide="datepicker"
							data-date-format="dd/mm/yyyy" data-date-autoclose="true"
							data-date-orientation="bottom" data-date-language="es"
							data-date-end-date="0d" data-date-today-btn="true"
							data-date-today-highlight="true">
							<input id="tf_vigencia" type="text" class="form-control"
								name="nuevaParcela.lineaVigente.fecha"
								value="<s:property value='getText("format.date",{nuevaParcela.lineaVigente.fecha})'/>">
							<div class="input-group-addon">
								<span class="glyphicon glyphicon-calendar"></span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>

	<!--  Superificies -->
	<div class="row">
		<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">

			<div class="panel panel-default">
				<div class="panel-heading">
					<s:text name="superficies.panel.label" />
				</div>
				<div class="panel-body">
					<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
						<div class="form-group">
							<label for="sup_terreno"><s:text name="terreno.label" /></label>
							<div class="input-group">
								<input type="text" id="sup_terreno"
									class="form-control numberdecimal input-numeral" maxlength="8"
									name="nuevaParcela.lineaVigente.superficieTerreno"
									value="<s:property value='%{getText("number.format" ,{nuevaParcela.lineaVigente.superficieTerreno})}'/>">
								<span class="input-group-addon">m<sup>2</sup></span>
							</div>
						</div>
					</div>
					<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
						<div class="form-group">
							<label for="sup_cubierta"><s:text name="cubierta.label" /></label>
							<div class="input-group">
								<input type="text" id="sup_cubierta"
									class="form-control numberdecimal" maxlength="8"
									name="nuevaParcela.lineaVigente.superficieCubierta"
									value="<s:property value='%{getText("number.format" ,{nuevaParcela.lineaVigente.superficieCubierta})}'/>">
								<span class="input-group-addon">m<sup>2</sup></span>
							</div>
						</div>
					</div>
					<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
						<div class="form-group">
							<label for="sup_semi"><s:text name="semi.label" /></label>
							<div class="input-group">
								<input type="text" id="sup_semi"
									class="form-control numberdecimal" maxlength="8"
									name="nuevaParcela.lineaVigente.superficieSemi"
									value="<s:property value='%{getText("number.format" ,{nuevaParcela.lineaVigente.superficieSemi})}'/>">
								<span class="input-group-addon">m<sup>2</sup></span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
			<!-- Valores -->
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					<div class="pull-left">
						<strong><s:text name="valuacion.panel.label" /></strong>
					</div>
					<div class="pull-right" style="padding-right: 15px;">
						<span id="valuacion" class="label label-info"
							style="font-size: 100%">$ 0,00</span>
					</div>
				</div>
									
				<div class="panel-body">
					<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
						<div class="form-group">
							<label for="valor_tierra"><s:text name="tierra.label" /></label>
							<div class="input-group">
								<span class="input-group-addon">$</span> <input type="text"
									id="valor_tierra" onchange="updateValuacion()"
									class="form-control numberdecimal" maxlength="10"
									name="nuevaParcela.lineaVigente.valorTierra"
									value="<s:property value='%{getText("number.format" ,{nuevaParcela.lineaVigente.valorTierra})}'/>">
							</div>
						</div>
					</div>
					<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
						<div class="form-group">
							<label for="valor_comun"><s:text name="comun.label" /></label>
							<div class="input-group">
								<span class="input-group-addon">$</span> <input type="text"
									id="valor_comun" onchange="updateValuacion()"
									class="form-control numberdecimal" maxlength="10"
									name="nuevaParcela.lineaVigente.valorComun"
									value="<s:property value='%{getText("number.format" ,{nuevaParcela.lineaVigente.valorComun})}'/>">
							</div>
						</div>
					</div>
					<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
						<div class="form-group">
							<label for="valor_edificio"><s:text name="edificio.label" /></label>
							<div class="input-group">
								<span class="input-group-addon">$</span> <input type="text"
									id="valor_edificio" onchange="updateValuacion()"
									class="form-control numberdecimal" maxlength="10"
									name="nuevaParcela.lineaVigente.valorEdificio"
									value="<s:property value='%{getText("number.format" ,{nuevaParcela.lineaVigente.valorEdificio})}'/>">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Domicilio Inmueble -->
	<div class="panel-group">
		<div class="panel panel-default">
			<div class="panel-heading mano" data-toggle="collapse"
				href="#panel_domicilio_inmueble">
				<s:text name="domicilio.inmueble.panel.label" />
			</div>
			<div class="panel-body panel-collapse" id="panel_domicilio_inmueble">
				<div class="col-xs-6 col-sm-6 col-md-5 col-lg-4">
					<label for="di_calle"><s:text name="domicilio.calle.label" /></label>
					<s:textfield id="di_calle" class="form-control input-sm"
						name="nuevaParcela.domicilioInmueble.calle" />
				</div>
				<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
					<label for="di_numero"><s:text name="domicilio.nro.label" /></label>
					<s:textfield id="di_numero" class="form-control input-sm"
						name="nuevaParcela.domicilioInmueble.numero" />
				</div>
				<div class="col-xs-6 col-sm-6 col-md-1 col-lg-1">
					<label for="di_piso"><s:text name="domicilio.piso.label" /></label>
					<s:textfield id="di_piso" class="form-control input-sm"
						name="nuevaParcela.domicilioInmueble.piso" />
				</div>
				<div class="col-xs-6 col-sm-6 col-md-1 col-lg-1">
					<label for="di_depto"><s:text name="domicilio.depto.label" /></label>
					<s:textfield id="di_depto" class="form-control input-sm"
						name="nuevaParcela.domicilioInmueble.departamento" />
				</div>
				<div class="col-xs-6 col-sm-6 col-md-1 col-lg-2">
					<label for="di_cp"><s:text name="domicilio.cp.label" /></label>
					<s:textfield id="di_cp" class="form-control input-sm"
						name="nuevaParcela.domicilioInmueble.codigoPostal" />
				</div>
				<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
					<label for="di_barrio"><s:text
							name="domicilio.barrio.label" /></label>
					<s:select id="di_barrio" list="#session.barrios" listKey="id"
						listValue="nombre" class="form-control input-sm"
						name="nuevaParcela.domicilioInmueble.barrio.id"></s:select>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-xs-6 col-sm-6 col-md-3 col-lg-2">
			<label for="destinoMunicipal"><s:text
					name="dominiomunicipal.label" /></label> <input id="destinoMunicipal"
				data-group-cls="btn-group-sm" type="checkbox"
				name="nuevaParcela.dominioMunicipal"
				value="<s:property value="nuevaParcela.dominioMunicipal" />" />
		</div>
		<div id="div_destino" class="col-xs-6 col-sm-6 col-md-10 col-lg-10">
			<label for="destino"><s:text name="destino.label" /></label>
			<s:select id="destino" list="#session.destinos" listKey="destino"
				disabled="true" listValue="destino" name="nuevaParcela.destino"
				headerKey="" headerValue="Seleccione"></s:select>
		</div>
	</div>

	<!-- Observacion -->
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<label for="observacion"><s:text name="observacion.label" /></label>
			<s:textfield id="observacion" name="nuevaParcela.observacion" />
		</div>
	</div>
	</fieldset>

</s:form>

