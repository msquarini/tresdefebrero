<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<script type="text/javascript">
	var rows;
	/* Activa el boton de agregar parcelas a la lista de unificacion si hay alguna seleccionada */
	$(document).off("cuentaSelectedEvent").on("cuentaSelectedEvent",
			function(event, data) {
				rows = data.id;
				$("#unificacion_add_btn").toggleClass('disabled', false);
			}).off("cuentaDeselectedEvent").on("cuentaDeselectedEvent",
			function(event, data) {
				//alert("Deselect " + data.cuenta.id);
				$("#idCuentaOrigen").val("-1");
				$("#unificacion_add_btn").toggleClass('disabled', true);
			}).off("unificacion-grid-data-loaded")
			.on(
					"unificacion-grid-data-loaded",
					function(event) {
						// Activa boton para iniciar proceso de unificacion si hay mas de 1 cuenta
						if ($("#unificacion-grid-data").bootgrid(
								"getTotalRowCount") > 1) {
							$("#unificar_btn").toggleClass('disabled', false);
						} else {
							$("#unificar_btn").toggleClass('disabled', true);
						}
					});

	/* Incorpora una nueva parcela a la lista */
	function agregarParcela() {
		$.ajax({
			method : "POST",
			url : "catastro/unificacion_addParcelaTOUnificacion.action",
			data : {
				idsParcela : rows
			},
			traditional : true,
			success : function() {
				$("#unificacion-grid-data").bootgrid('reload');

				// window.location = "loadexperiments.action";
			},
			error : function(res, textStatus, jqXHR) {
				// Levantar dialogo con mensaje de error!!!!!!!!!!!!!
				handleErrorMessages(res.responseJSON);
				//$("#errors-data").text(res.responseJSON.actionErrors[0]);
				$('#error-dialog').modal('show');
			}
		});
	}
</script>

<style type="text/css">
</style>

<div class="row">
	<s:include value="../cuenta_search.jsp"></s:include>
</div>
<div class="row">
	<div class="panel panel-default">
		<div class="panel-heading">Parcelas a Unificar</div>
		<div class="panel-body" id="panel_unificacion">
			<s:include value="unificacion-grid.jsp"></s:include>
		</div>
		<div class="panel-footer">
			<div class="col-xs-6 col-sm-6 col-md-1 col-lg-1">
				<s:form id="unificar-init-form" action="unificacion_init"
					namespace="/catastro" theme="bootstrap">

					<sj:a id="unificar_btn" cssClass="btn btn-primary disabled"
						value="Unificar" formIds="unificar-init-form" targets="container">Unificar</sj:a>

				</s:form>
			</div>
			<div class="col-xs-6 col-sm-6 col-md-1 col-lg-1">

				<sj:a id="unificacion_add_btn" cssClass="btn btn-primary disabled"
					value="Agregar" onclick="agregarParcela();">Agregar</sj:a>

			</div>
			<div class="clearfix"></div>
			<!--  Usado para que los botones queden dentro del footer del panel -->
		</div>
	</div>
</div>
