<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<!-- UNIFICACION  -->

<script type="text/javascript">
/* 	$.subscribe('complete', function(event, data) {
		$('#generic-modal-response-body').html(
				event.originalEvent.request.responseText);
		//$('#generic-dialog-response').modal();
		showResponseModalDialog(true);
	}); */

	/* Boton de procesamiento de unificacion, antes del envio del formulario, se valida el mismo */
	/* $('#btnProcesarUnificacion').subscribe(
			'validate-unificacion-form',
			function(event) {
				$('#unificacion-parcela-form').bootstrapValidator('resetForm');
				$('#unificacion-parcela-form').bootstrapValidator('validate');
				// Valor de validar el formulario
				event.originalEvent.options.submit = $(
						'#unificacion-parcela-form').data('bootstrapValidator')
						.isValid();

			});
 */
	function validarFormulario() {
		$('#unificacion-parcela-form').bootstrapValidator('resetForm');
		$('#unificacion-parcela-form').bootstrapValidator('validate');
		return $('#unificacion-parcela-form').data('bootstrapValidator')
				.isValid();
	}
	
	function procesarTramite() {
		if (validarFormulario()) {
			loader(true, $('#btn_unificacion_process'));
			$('#unificacion-parcela-form').submit();
		} else {			
			handleErrorMessages(JSON.parse('{ "actionErrors": ["Error en la validación de datos, verifique los datos del formulario"]}	'));
			showErrorModalDialog(false);
		}
	}

	function grabarTramite() {		
		if (validarFormulario()) {
			loader(true, $('#btn_unificacion_save'));
			var options = {
				target : '#generic-modal-response-body',
				url : 'catastro/unificacion_saveTramite.action',
				success : function() {
					loader(false, $('#btn_unificacion_save'));
					showResponseModalDialog(false);
				},
				error : function(res, textStatus, jqXHR) {
					loader(false, $('#btn_unificacion_save'));
					// Levantar dialogo con mensaje de error!!!!!!!!!!!!!
					handleErrorMessages(res.responseJSON);
					//$("#errors-data").text(res.responseJSON.actionErrors[0]);
					$('#error-dialog').modal('show');
				}
			};
			//$('#unificacion-parcela-form').ajaxForm(options);
			//$('#unificacion-parcela-form').ajaxSubmit({url: 'catastro/unificacion_saveTramite.action'});
			$('#unificacion-parcela-form').ajaxSubmit(options);
		}
	}
</script>

<style type="text/css">
</style>

<s:include value="../common/tramite-header.jsp"></s:include>

<!-- <div class="row"> -->
	<ul class="nav nav-pills"
		style="background-color: #eeeeee; border-bottom-style: ridge;">
		<li class="nav-header disabled"><a>Unificacion</a></li>
		<li class="active"><a data-toggle="tab" href="#cuenta-origen">Parcelas
				Origen</a></li>
		<li><a data-toggle="tab" href="#titulares">Titulares</a></li>
		<li><a data-toggle="tab" href="#responsable">Responsable</a></li>
		<li><a data-toggle="tab" href="#nueva">Nueva Parcela</a></li>
		<%-- <li class="pull-right" style="padding-top: 3px; padding-right: 3px">
			<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
				<sj:submit cssClass="btn btn-danger" id="btnProcesarUnificacion"
					value="Procesar" formIds="unificacion-parcela-form"
					targets="generic-modal-response-body" onSuccessTopics="complete"
					onBeforeTopics="validate-unificacion-form"></sj:submit>
			</div>
		</li> --%>

		<s:if test="tramite.estado.toString() != 'FINALIZADO'">
			<li class="pull-right" style="padding-top: 3px; padding-right: 3px">
				<button id="btn_unificacion_save" class="btn btn-warning"
					type="button" onclick="grabarTramite()">Grabar</button> 
					
					<button id="btn_unificacion_process" class="btn btn-danger"
                    type="button" onclick="procesarTramite()">Procesar</button> <%-- <sj:submit
					cssClass="btn btn-danger" id="btnProcesarUnificacion"
					value="Procesar" formIds="unificacion-parcela-form"
					targets="generic-modal-response-body" onSuccessTopics="complete"
					onBeforeTopics="validate-unificacion-form"></sj:submit> --%>
			</li>
		</s:if>
	</ul>
<!-- </div>
 -->
<div class="tab-content">
	<div id="cuenta-origen" class="tab-pane fade in active">
	<!-- 	<div class="row top-buffer"> -->
			<s:include value="unificacion-grid.jsp"></s:include>
		<!-- </div> -->
	</div>
	<div id="titulares" class="tab-pane fade">
		<!-- Grilla de titulares pertenecientes a la parcela a subdividir
			 Se permite edicion de CUIT/CUIL  -->
		<s:include value="../grid/titular-grid.jsp"></s:include>
	</div>
	<div id="responsable" class="tab-pane fade">
		<!-- Grilla de responsables pertenecientes a la parcela a subdividir -->
		<s:include value="../info/responsables-info.jsp"></s:include>
	</div>
	<div id="nueva" class="tab-pane fade">
		<s:include value="unificacion-parcela-content.jsp"></s:include>
	</div>
</div>
