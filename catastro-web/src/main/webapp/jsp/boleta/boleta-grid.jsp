<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<style>
.cgb-header-name {
	width: 50%;
}

/* Manito sobre la grilla de datos para indicar que se puede clickear */
#boleta-grid-data tbody {
	cursor: pointer;
}

input.currency {
	text-align: right;
	padding-right: 15px;
}
</style>
<script type="text/javascript">
	$("#anio").on("change", function() {
		if ($(this).val().length == 4) {
			$("#boleta-grid-data").bootgrid('reload');
		}
	});
	$('#sinPago').checkboxpicker({
		offLabel : 'No',
		onLabel : 'Si'
	});

	$('.selectpicker').selectpicker({
		width : '100%'
	});

	function searchBoleta() {
		loader(true, $('#search_btn'));
		//$('#cc-search-form').submit();
		$.ajax({
			method : "POST",
			url : "catastro/boleta_search.action",
			data : $('#boleta-search-form').serialize(),
			global : false,
			traditional : true,
			success : function(res) {
				$("#boleta-grid-data").bootgrid('reload');
				loader(false, $('#search_btn'));
			},
			error : function(res, textStatus, jqXHR) {
				loader(false, $('#search_btn'));
				handleErrorMessages(res.responseJSON);
				showErrorModalDialog(false);
			}
		});
	};
</script>


<div style="background-color: #eeeeee; padding-top: 7.5px; height: 50px">
	<s:form id="boleta-search-form" action="boleta_search"
		cssClass="form-vertical" namespace="/catastro" theme="bootstrap">

		<s:if test="generic == true">
			<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
				<input id="cuenta" class="form-control number" name="cuenta.cuenta"
					placeholder="Cuenta" maxlength="6" />
			</div>
		</s:if>

		<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
			<div class="input-group date" data-provide="datepicker"
				data-date-format="yyyy" data-date-autoclose="true"
				data-date-view-mode="years" data-date-min-view-mode="years"
				data-date-end-date="y" data-date-orientation="bottom"
				data-date-language="es" data-date-today-btn="true"
				data-date-today-highlight="true">
				<input id="anio" type="text" class="form-control" name="anio"
					value='<s:property value="anio"/>' placeholder="Filtro por a�o">
				<div class="input-group-addon">
					<span class="glyphicon glyphicon-calendar"></span>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">

			<label for="sinPago">Sin Pago</label> <input id="sinPago"
				data-group-cls="btn-group-sm" type="checkbox" name="sinPago"
				value="true"> <input id="sinPagoHidden" type="hidden"
				name="sinPago" value="false">
		</div>
		<div class="col-xs-12 col-sm-12 col-md-4 col-lg-3">
			<span class="input-group-btn">
				<button id="search_btn" class="btn btn-default" type="button"
					onclick="searchBoleta();"
					data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Buscando">
					<span class="glyphicon glyphicon-search"></span>
					<s:text name="search.label"></s:text>
				</button>
			</span>
		</div>
	</s:form>
</div>

<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

		<table id="boleta-grid-data"
			class="table table-condensed table-hover table-striped">
			<thead>
				<tr>
					<th data-column-id="id" data-identifier="true" data-type="numeric"
						data-visible="false">IdCC</th>
					<th data-column-id="anio" data-type="numeric" data-visible="true"><s:text
							name="anio.label" /></th>
					<th data-column-id="cuota" data-type="numeric"><s:text
							name="cuota.label" /></th>
					<th data-column-id="importe" data-type="price" data-align="right"
						data-header-align="right" data-width="10%"><s:text
							name="deuda.importe.label" /></th>
					<th data-column-id="fechaVto" data-type="date" data-align="center"
						data-header-align="center"><s:text
							name="deuda.fechaVto.label" /></th>
					<th data-column-id="fechaVto2" data-type="date" data-align="center"
						data-header-align="center"><s:text
							name="deuda.fechaVto2.label" /></th>
					<th data-column-id="fechaPago" data-type="date"><s:text
							name="deuda.fechaPago.label" /></th>
					<th data-column-id="cuotaAnual" data-formatter="booleano"><s:text
							name="cuota.anual.label" /></th>
					<th data-column-id="aplicaBonificacion" data-formatter="booleano"><s:text
							name="aplica.bonificacion.label" /></th>
					<th data-column-id="importeBonificacion" data-type="price"
						data-align="right" data-header-align="right" data-width="10%"><s:text
							name="bonificacion.label" /></th>
				</tr>
			</thead>

		</table>
	</div>
</div>

<script>
	$(document)
			.ready(
					function() {
						//search();

						$('input.numberdecimal').number(true, 2, ',', '');

						$("#boleta-grid-data")
								.bootgrid(
										{
											ajax : true,
											navigation : 2,
											rowSelect : false,
											selection : false,
											multiSelect : false,
											search : true,
											columnSelection : false,
											templates : {
												search : ""
											},
											labels : {
												noResults : "No hay datos de boletas",
												loading : "Cargando informaci�n...",
												infos : "Mostrando {{ctx.start}} a {{ctx.end}} de {{ctx.total}} filas"
											},
											responseHandler : function(data) {
												// TODO, validar de tomar el total para setear el input de total
												return data;
											},
											requestHandler : function(rq) {
												rq.anio = $("#anio").val();
												return sortRequest(rq);
											},
											url : "catastro/boleta_getData.action",
											statusMapping : {
												0 : "info",
												1 : "warning",
												2 : "danger"
											},
											converters : {
												price : {
													from : function(value) {
														return Number(value
																.replace(",",
																		"."));
													},
													to : function(value) {
														var v = Number(value)
																.toFixed(2);
														return (v + "")
																.replace(".",
																		",");
													}
												},
												date : {
													from : function(value) {
														return moment(value);
													},
													to : function(value) {
														if (!value) {
															return "";
														}
														return moment(value)
																.format(
																		"DD-MM-YYYY");
													}
												}
											},
											formatters : {
												booleano : function(column, row) {
													return fmt_boolean(row[column.id]);
												}
											}
										}).on("click.rs.jquery.bootgrid",
										function(e, columns, row) {
											// Mostrar informacion de la CC clickeado																			
											//detalleCuentaCorriente(row.id);
										});

					});

	
</script>