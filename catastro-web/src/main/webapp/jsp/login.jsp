<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<script type="text/javascript" src="../js/number/jquery.number.js"></script>

<style type="text/css">
#number_container {
	border: 1px dotted #d0d0d0;
	padding: 15px;
	margin: 10px;
	display: none;
	background: #fafafa;
}

body {
	padding-top: 60px;
	/* 60px to make the container go all the way to the bottom of the topbar */
}

.top-buffer {
	margin-top: 50px;
}

.bottom-buffer {
	margin-bottom: 20px;
}
</style>

<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<h1>Login</h1>

		<s:actionerror theme="bootstrap" />
		<s:actionmessage theme="bootstrap" />

		<p>Ingrese usuario y password para ingresar al sistema</p>
		<s:form action="login_login" enctype="multipart/form-data"
			namespace="/" theme="bootstrap" cssClass="form-horizontal" label=""
			id="loginForm" focusElement="username">
			<fieldset>
				<div class="row">
					<div class="col-xs-9 col-sm-6 col-md-3 col-lg-3">
						<label for="username">Usuario</label>
						<s:textfield id="username" name="username" 
							tooltip="Ingrese su usuario" />
					</div>
				</div>
				<div class="row">
					<div class="col-xs-9 col-sm-6 col-md-3 col-lg-3">
						<label for="password">Password</label>
						<s:textfield id="password" name="password" maxlength="12"
							tooltip="Ingrese su password" />
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<sj:submit cssClass="btn btn-primary" value="Ingresar"
							formIds="loginForm" ></sj:submit>
					</div>
				</div>
			</fieldset>
		</s:form>
	</div>
</div>