<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<script type="text/javascript">
	
</script>
<style>
.align-right {
	text-align: right;
}
</style>
<form id="cuenta-info">

	<fieldset disabled="disabled">

		<div class="row">
			<div class="col-md-2 col-lg-2">
				<label for="partida"><s:text name="partida.label" /></label> <input
					class="form-control input-sm" type="text" readonly
					value='<s:property value="cuentaOrigen.parcela.partida"></s:property>' />
			</div>
			<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
				<s:push value="%{cuentaOrigen.parcela.nomenclatura}">
					<s:include value="common/nomenclatura-info.jsp"></s:include>
				</s:push>
			</div>
			<div class="col-md-2 col-lg-2">
				<div class="row">
					<s:if test="cuentaOrigen.activa">
						<span class='label label-success'> <i
							class="fa fa-check-square-o" aria-hidden="true"></i> Activa
						</span>
					</s:if>
					<s:else>
						<span class='label label-danger'> <i class="fa fa-square-o"
							aria-hidden="true"></i> Activa
						</span>
					</s:else>
				</div>
				<div class="row">
					<s:if test="cuentaOrigen.exenta">
						<span class='label label-warning'> <i
							class="fa fa-check-square-o" aria-hidden="true"></i> Exenta
						</span>
					</s:if>
					<s:else>
						<span class='label label-info'> <i class="fa fa-square-o"
							aria-hidden="true"></i> Exenta
						</span>
					</s:else>
				</div>
				<div class="row">
					<s:if test="cuentaOrigen.parcela.dominioMunicipal">
						<span class='label label-warning'> <i
							class="fa fa-check-square-o" aria-hidden="true"></i> Dom.
							Municipal
						</span>
					</s:if>
					<s:else>
						<span class='label label-info'> <i class="fa fa-square-o"
							aria-hidden="true"></i> Dom. Municipal
						</span>
					</s:else>
				</div>
			</div>

		</div>

		<!-- Datos Parcela -->
		<div class="row">
			<div class="col-md-1 col-lg-1">
				<label for="categoria"><s:text name="categoria.label" /></label> <input
					class="form-control input-sm text-info" type="text" readonly
					value='<s:property value="cuentaOrigen.parcela.lineaVigente.categoria"></s:property>' />
			</div>
			<div class="col-md-1 col-lg-1">
				<label for="partida"><s:text name="dominio.tipo.label" /></label> <input
					class="form-control input-sm" type="text" readonly
					value='<s:text name="%{cuentaOrigen.parcela.dominio.tipo.textKey}"></s:text>' />
			</div>
			<div class="col-md-1 col-lg-1">
				<label for="numero"><s:text name="dominio.numero.label" /></label>
				<input class="form-control input-sm " type="text" readonly
					value='<s:property value="cuentaOrigen.parcela.dominio.numero"></s:property>' />
			</div>
			<div class="col-md-1 col-lg-1">
				<label for="anio"><s:text name="dominio.anio.label" /></label> <input
					class="form-control input-sm" type="text" readonly
					value='<s:property value="cuentaOrigen.parcela.dominio.anio"></s:property>' />
			</div>
			<div class="col-md-2 col-lg-2">
				<label for="creado"><s:text name="fecha.alta.label" /></label> <input
					class="form-control input-sm" type="text" readonly
					value='<s:property value="getText('format.datetime',{cuentaOrigen.createdDate})"></s:property>' />
			</div>
			<div class="col-md-2 col-lg-2">
				<label for="creado"><s:text name="usuario.alta.label" /></label> <input
					class="form-control input-sm" type="text" readonly
					value='<s:property value="cuentaOrigen.createdBy"></s:property>' />
			</div>
			<div class="col-md-2 col-lg-2">
				<label for="creado"><s:text name="fecha.modifico.label" /></label>
				<input class="form-control input-sm" type="text" readonly
					value='<s:property value="getText('format.datetime',{cuentaOrigen.modifiedDate})"></s:property>' />
			</div>
			<div class="col-md-2 col-lg-2">
				<label for="creado"><s:text name="usuario.modifico.label" /></label>
				<input class="form-control input-sm" type="text" readonly
					value='<s:property value="cuentaOrigen.modifiedBy"></s:property>' />
			</div>
		</div>



		<div class="row top-buffer">
			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
				<div class="panel panel-default">
					<div class="panel-heading clearfix">
						<div class="pull-left">
							<strong><s:text name="superficies.panel.label" /></strong>
						</div>
						<div class="pull-right text-primary" style="padding-right: 15px;">
							<span class="label label-info" style="font-size: 100%"><s:text
									name="fecha_data.label" />: <s:property
									value="getText('format.date',{cuentaOrigen.parcela.lineaVigente.fecha})"></s:property></span>
						</div>
					</div>
					<div class="panel-body">
						<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
							<label for="sup_terreno"><s:text name="terreno.label" /></label>
							<div class="input-group">
								<input class="form-control align-right" type="text" readonly
									value='<s:property value="getText('number.format',{cuentaOrigen.parcela.lineaVigente.superficieTerreno})"></s:property>' />
								<span class="input-group-addon">m<sup>2</sup></span>
							</div>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
							<label for="sup_cubierta"><s:text name="cubierta.label" /></label>
							<div class="input-group">
								<input class="form-control align-right" type="text" readonly
									value='<s:property value="getText('number.format',{cuentaOrigen.parcela.lineaVigente.superficieCubierta})"></s:property>' />
								<span class="input-group-addon">m<sup>2</sup></span>
							</div>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
							<label for="sup_semi"><s:text name="semi.label" /></label>
							<div class="input-group">
								<input class="form-control align-right" type="text" readonly
									value='<s:property value="getText('number.format',{cuentaOrigen.parcela.lineaVigente.superficieSemi})"></s:property>' />
								<span class="input-group-addon">m<sup>2</sup></span>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
				<div class="panel panel-default">
					<div class="panel-heading clearfix">
						<div class="pull-left">
							<strong><s:text name="valuacion.panel.label" /></strong>
						</div>
						<div class="pull-right" style="padding-right: 15px;">
							<span class="label label-info" style="font-size: 100%">$ <s:property
									value="getText('money.format',{cuentaOrigen.parcela.lineaVigente.valuacion})"></s:property></span>
						</div>
					</div>
					<div class="panel-body">
						<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
							<label for="valor_tierra"><s:text name="tierra.label" /></label>
							<div class="input-group">
								<span class="input-group-addon">$</span> <input
									class="form-control align-right" type="text" readonly
									value='<s:property value="getText('money.format',{cuentaOrigen.parcela.lineaVigente.valorTierra})"></s:property>' />
							</div>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
							<label for="valor_comun"><s:text name="comun.label" /></label>
							<div class="input-group">
								<span class="input-group-addon">$</span> <input
									class="form-control align-right" type="text" readonly
									value='<s:property value="getText('money.format',{cuentaOrigen.parcela.lineaVigente.valorComun})"></s:property>' />
							</div>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-4 col-lg-42">
							<label for="valor_edificio"><s:text name="edificio.label" /></label>
							<div class="input-group">
								<span class="input-group-addon">$</span> <input
									class="form-control align-right" type="text" readonly
									value='<s:property value="getText('money.format',{cuentaOrigen.parcela.lineaVigente.valorEdificio})"></s:property>' />
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Domicilio Inmueble -->
		<div class="panel-group">
			<div class="panel panel-default">
				<div class="panel-heading" data-toggle="collapse"
					href="#panel_domicilio_inmueble">Domicilio Inmueble</div>
				<div class="panel-body panel-collapse" id="panel_domicilio_inmueble">
					<div class="col-xs-6 col-sm-6 col-md-5 col-lg-4">
						<label for="di_calle">Calle</label>
						<s:textfield id="di_calle" class="form-control input-sm"
							name="cuentaOrigen.parcela.domicilioInmueble.calle" />
					</div>
					<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
						<label for="di_numero">Nro</label>
						<s:textfield id="di_numero" class="form-control input-sm"
							name="cuentaOrigen.parcela.domicilioInmueble.numero" />
					</div>
					<div class="col-xs-6 col-sm-6 col-md-1 col-lg-1">
						<label for="di_piso">Piso</label>
						<s:textfield id="di_piso"  class="form-control input-sm"
							name="cuentaOrigen.parcela.domicilioInmueble.piso" />
					</div>
					<div class="col-xs-6 col-sm-6 col-md-1 col-lg-1">
						<label for="di_depto">Depto</label>
						<s:textfield id="di_depto" class="form-control input-sm"
							name="cuentaOrigen.parcela.domicilioInmueble.departamento" />
					</div>
					<div class="col-xs-6 col-sm-6 col-md-1 col-lg-1">
						<label for="di_cp">CP</label>
						<s:textfield id="di_cp" class="form-control input-sm"
							name="cuentaOrigen.parcela.domicilioInmueble.codigoPostal" />
					</div>
					<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
						<label for="di_barrio">Barrio</label>
						<s:textfield id="di_barrio" class="form-control input-sm"
							name="cuentaOrigen.parcela.domicilioInmueble.barrio.nombre" />
					</div>
				</div>
			</div>
		</div>

		<!-- Observacion -->
		<div class="row">
			<div class="col-xs-12 col-sm12 col-md-12 col-lg-12">
				<label for="observacion">Observacion</label>
				<textarea id="observacion" name="cuentaOrigen.parcela.observacion"
					class="form-control input-sm" style="color: blue" rows="2" readonly></textarea>
			</div>
		</div>
	</fieldset>

</form>