<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<!-- Grilla de lineas valuatorias -->

<script src="js/moment.min.js"></script>

<style>
.cgb-header-name {
	width: 50%;
}
</style>

<script type="text/javascript">
	
</script>


<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	<table id="lineas-grid-data"
		class="table table-condensed table-hover table-striped">
		<thead>
			<tr>
				<th data-column-id="id" data-identifier="true" data-type="numeric"
					data-visible="false">idLinea</th>
				<th data-column-id="fecha" data-type="date" data-visible="true"
					data-width="10%"><s:text name="linea.grid.data" /></th>
				<th data-column-id="categoria" data-type="string" data-visible="true"
					data-width="6%"><s:text name="linea.grid.categoria" /></th>
				<th data-column-id="superficieTerreno" data-type="price"
					data-width="8%" data-visible="true" data-header-align="right"
					data-align="right" data-width="10%"><s:text
						name="linea.grid.terreno" /></th>
				<th data-column-id="superficieCubierta" data-type="price"
					data-header-align="right" data-align="right" data-visible="true"
					data-width="8%"><s:text name="linea.grid.cubierta" /></th>
				<th data-column-id="superficieSemi" data-type="price"
					data-header-align="right" data-align="right" data-visible="true"
					data-width="8%"><s:text name="linea.grid.semi" /></th>
				<th data-column-id="valorTierra" data-type="price" data-width="10%"
					data-header-align="right" data-align="right" data-visible="true"><s:text
						name="linea.grid.tierra" /></th>
				<th data-column-id="valorComun" data-type="price" data-align="right"
					data-header-align="right" data-visible="true" data-width="10%"><s:text
						name="linea.grid.comun" /></th>
				<th data-column-id="valorEdificio" data-type="price"
					data-width="10%" data-header-align="right" data-align="right"
					data-visible="true"><s:text name="linea.grid.edificio" /></th>
				<th data-column-id="createdBy" data-type="string" data-width="8%"
					data-visible="true"><s:text name="usuario.alta.label" /></th>
				<th data-column-id="createdDate" data-type="datetime"
					data-width="8%" data-visible="true"><s:text
						name="fecha.alta.label" /></th>
			</tr>
		</thead>
	</table>
</div>


<script>
	$(document)
			.ready(
					function() {
						$("#lineas-grid-data")
								.bootgrid(
										{
											ajax : true,
											navigation : 2,
											padding : 5,
											rowCount : 5,
											rowSelect : true,
											//selection : true,
											multiSelect : false,
											search : true,
											columnSelection : false,
											templates : {
												search : ""
											},
											labels : {
												noResults : "No hay lineas",
												loading : "Cargando información...",
												infos : "Mostrando {{ctx.start}} a {{ctx.end}} de {{ctx.total}} filas"
											},
											url : "catastro/linea_getData.action",
											requestHandler : function(request) {
												return sortRequest(request);
											},
											converters : {
												price : {
													from : function(value) {
														return Number(value
																.replace(",",
																		"."));
													},
													to : function(value) {
														return $.number(value, 2, ',', '.');
														/* var v = Number(value)
																.toFixed(2);
														return (v + "")
																.replace(".",
																		","); */
													}
												},
												date : {
													from : function(value) {
														return moment(value);
													},
													to : function(value) {
														return moment(value)
																.format(
																		"DD-MM-YYYY");
													}
												},
												datetime : {
													from : function(value) {
														return moment(value);
													},
													to : function(value) {
														return moment(value)
																.format(
																		"DD-MM-YYYY HH:mm:ss");
													}
												}

											}
										});
					});
</script>
