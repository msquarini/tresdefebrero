<!-- Grilla con las parcelas agregadas a la lista para unificacion !!!!!! -->
<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<style>
.cgb-header-name {
	width: 50%;
}
</style>

<script type="text/javascript">
	/* Id (parcela) de la fila elegida y boolean indicando si es edit o delete */
	function unificacion_accion_submit(id, edit) {
		$('#idParcelaTORemove').val(id);

		$('#grid-parcela-form').submit();
	};

	$('#grid-parcela-form').ajaxForm({
		target : '#generic-modal-body',
		success : function(res, textStatus, jqXHR) {

			// Recargo la grilla de parcelas de unificacion
			$('#unificacion-grid-data').bootgrid('reload');
		},
		error : {

		}
	});
</script>

<!--  Formulario para la edicion o delete de parcelas en la grilla de unificacion -->
<s:form id="grid-parcela-form"
	action="unificacion_removeParcelaTOUnificacion"
	cssClass="form-vertical" namespace="/catastro" theme="bootstrap">
	<s:hidden id="idParcelaTORemove" name="idParcelaTORemove"></s:hidden>
</s:form>


<table id="unificacion-grid-data"
	class="table table-condensed table-hover table-striped">
	<thead>
		<tr>
			<th data-column-id="id" data-identifier="true" data-type="numeric"
				data-visible="false">IdParcela</th>
			<th data-column-id="idCuenta" data-type="numeric"
				data-visible="false" data-formatter="idCuenta">IdCuenta</th>
			<th data-column-id="cuenta" data-formatter="cuenta">Cuenta</th>
			<th data-column-id="dv" data-formatter="dv">DV</th>
			<th data-column-id="partida" data-formatter="partida">Partida</th>
			<th data-column-id="nomenclatura" data-formatter="nomenclatura"
				data-header-css-class="cgb-header-name">Nomenclatura</th>
			<th data-column-id="oper" data-formatter="oper"
				data-visible="<s:property value='canDelete' default='false'/>"></th>
		</tr>
	</thead>

</table>
<script>
	$(document)
			.ready(
					function() {
						$("#unificacion-grid-data")
								.bootgrid(
										{
											ajax : true,
											navigation : 2, /* Solo pie */
											rowSelect : false,
											selection : false,
											multiSelect : false,
											search : false,
											columnSelection : false,
											templates : {
												search : ""
											},
											labels : {
												noResults : "No hay parcelas",
												loading : "Cargando información...",
												infos : "Mostrando {{ctx.start}} a {{ctx.end}} de {{ctx.total}} filas"
											},
											url : "catastro/unificacion_getData.action",
											requestHandler : function(request) {
												return sortRequest(request);
											},
											formatters : {
												idCuenta : function(column, row) {
													return row.cuenta.id;
												},
												cuenta : function(column, row) {
													return row.cuenta.cuenta;
												},
												dv : function(column, row) {
													return row.cuenta.dv;
												},
												partida : function(column, row) {
													return row.partida;
												},
												nomenclatura : function(column,
														row) {
													return "C1111:"
															+ row.nomenclatura.circunscripcion
															+ " S:"
															+ row.nomenclatura.seccion
															+ " Fr:"
															+ row.nomenclatura.fraccion
															+ " FrL:"
															+ row.nomenclatura.fraccionLetra
															+ " Mz:"
															+ row.nomenclatura.manzana
															+ " MzL:"
															+ row.nomenclatura.manzanaLetra
															+ " Pc:"
															+ row.nomenclatura.parcela
															+ " PcL:"
															+ row.nomenclatura.parcelaLetra
															+ " UF:"
															+ row.nomenclatura.unidadFuncional;
												},
												oper : function(column, row) {
													return "<button onclick=\"unificacion_accion_submit( "
															+ row.id
															+ ", false);\"  type=\"button\" class=\"btn btn-xs btn-default command-delete\" data-row-id=\""
															+ row.id
															+ "\"><span class=\"icon glyphicon glyphicon-trash\"></span></button>";
												}
											}
										})
								.on(
										"selected.rs.jquery.bootgrid",
										function(e, rows) {
											$(document)
													.trigger(
															'cuentaSelectedEvent',
															$(
																	"#unificacion-grid-data")
																	.bootgrid(
																			"getSelectedRows"));
										})
								.on(
										"loaded.rs.jquery.bootgrid",
										function(e) {
											$(document)
													.trigger(
															'unificacion-grid-data-loaded');
										})

								.on("deselected.rs.jquery.bootgrid",
										function(e, rows) {
											//	$(document).trigger('cuentaDeselectedEvent', rows[0]);
											$('#idSelectedCuenta').val("-1");
										});
					});

	
</script>