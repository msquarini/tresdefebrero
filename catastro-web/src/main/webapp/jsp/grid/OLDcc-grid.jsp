<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<script src="js/jquery.bootgrid.js"></script>
<link rel="stylesheet" href="css/jquery.bootgrid.min.css">

<style>
.cgb-header-name {
	width: 50%;
}

/* Manito sobre la grilla de datos para indicar que se puede clickear */
#cc-grid-data tbody {
	cursor: pointer;
}

input.currency {
	text-align: right;
	padding-right: 15px;
}
</style>
<script type="text/javascript">
	$("#anio").on("change", function() {
		if ($(this).val().length == 4) {
			$("#cc-grid-data").bootgrid('reload');
		}
	});

	$('#sinPago').checkboxpicker({
		offLabel : 'No',
		onLabel : 'Si'
	});

	$('.selectpicker').selectpicker({
		width : '100%'
	});

	function actualizarTotales(totalizador) {
		if (totalizador) {
			$('#totalCreditos').val(totalizador.totalCredito);
			$('#totalDebitos').val(totalizador.totalDebito);
			$('#cantidadCreditos').val(totalizador.cantidadCreditos);
			$('#cantidadDebitos').val(totalizador.cantidadDebitos);
		}
	}

/* 	$('#cc-search-form').ajaxForm({
		global : false,
		success : function(res, textStatus, jqXHR) {
			$("#cc-grid-data").bootgrid('reload');
			actualizarTotales(res.totalizador);
			loader(false, $('#search_btn'));
		},
		error : function(res, textStatus, jqXHR) {
			debugger;
			loader(false, $('#search_btn'));
			handleErrorMessages(res.responseJSON);
			showErrorModalDialog(false);
		}
	}); */

	function search() {
		loader(true, $('#search_btn'));
		//$('#cc-search-form').submit();
		  $.ajax({
	            method : "POST",
	            url : "catastro/cuentacorriente_search.action",
	            data : $('#cc-search-form').serialize(),
	            global: false,
	            traditional : true,
	            success : function(res) {
	            	$("#cc-grid-data").bootgrid('reload');
	                actualizarTotales(res.totalizador);
	                loader(false, $('#search_btn'));
	            },
	            error : function(res, textStatus, jqXHR) {
	                loader(false, $('#search_btn'));
	                handleErrorMessages(res.responseJSON);
	                showErrorModalDialog(false);
	            }
	        });
	};
</script>


<div style="background-color: #eeeeee; padding-top: 7.5px; height: 50px">
	<s:form id="cc-search-form" action="cuentacorriente_search"
		cssClass="form-vertical" namespace="/catastro" theme="bootstrap">

		<s:if test="generic == true">
			<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
				<input id="cuenta" class="form-control number" name="cuenta.cuenta"
					placeholder="Cuenta" maxlength="6" />
			</div>
		</s:if>

		<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
			<div class="input-group date" data-provide="datepicker"
				data-date-format="yyyy" data-date-autoclose="true"
				data-date-view-mode="years" data-date-min-view-mode="years"
				data-date-end-date="y" data-date-orientation="bottom"
				data-date-language="es" data-date-today-btn="true"
				data-date-today-highlight="true">
				<input id="anio" type="text" class="form-control" name="anio"
					value='<s:property value="anio"/>' placeholder="Filtro por a�o">
				<div class="input-group-addon">
					<span class="glyphicon glyphicon-calendar"></span>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
			<select id="tipo" name="tipo" class="selectpicker"
				title="Tipo de Movimiento">
				<option value="D">Deuda</option>
				<option value="C">Cr�dito</option>
				<option value="">Todos</option>
			</select>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">

			<label for="sinPago">Sin Pago</label> <input id="sinPago"
				data-group-cls="btn-group-sm" type="checkbox" name="sinPago"
				value="true"> <input id="sinPagoHidden" type="hidden"
				name="sinPago" value="false">
		</div>
		<div class="col-xs-12 col-sm-12 col-md-4 col-lg-3">
			<span class="input-group-btn">
				<button id="search_btn" class="btn btn-default" type="button"
					onclick="search();"
					data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Buscando">
					<span class="glyphicon glyphicon-search"></span>
					<s:text name="search.label"></s:text>
				</button>
			</span>
		</div>
	</s:form>
</div>

<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

		<table id="cc-grid-data"
			class="table table-condensed table-hover table-striped">
			<thead>
				<tr>
					<th data-column-id="id" data-identifier="true" data-type="numeric"
						data-visible="false">IdCC</th>
					<th data-column-id="anio" data-type="numeric" data-visible="true"><s:text
							name="anio.label" /></th>
					<th data-column-id="cuota" data-type="numeric"><s:text
							name="cuota.label" /></th>
					<th data-column-id="tipo" data-formatter="cc_tipo_mov"
						data-header-align="center" data-align="center"><s:text
							name="deuda.tipo.label" /></th>
					<th data-column-id="importe" data-type="price" data-align="right"
						data-header-align="right" data-width="10%"><s:text
							name="deuda.importe.label" /></th>
					<th data-column-id="interes" data-type="price" data-align="right"
						data-header-align="right" data-width="10%"><s:text
							name="deuda.interes.label" /></th>
					<th data-column-id="montoTotal" data-type="price"
						data-align="right" data-header-align="right" data-width="10%"><s:text
							name="deuda.montototal.label" /></th>
					<th data-column-id="fechaVto" data-type="date" data-align="center"
						data-header-align="center"><s:text
							name="deuda.fechaVto.label" /></th>
					<th data-column-id="fechaPago" data-type="date"><s:text
							name="deuda.fechaPago.label" /></th>
					<th data-column-id="ajuste" data-type="numeric"><s:text
							name="deuda.ajuste.label" /></th>
					<th data-column-id="moratoria" data-type="numeric"><s:text
							name="deuda.moratoria.label" /></th>
				</tr>
			</thead>

		</table>
	</div>
</div>
<!-- Por ahora no lo mostramos hasta definir bien la gestion de CC -->
<%-- <div class="row">
	<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
		<label for="total_credito"><s:text name="cc.cantidad.creditos" /></label>
		<input id="cantidadCreditos" class="form-control" type="text" readonly
			value="0" />
	</div>
	<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
		<label for="total_credito"><s:text name="cc.cantidad.debitos" /></label>
		<input id="cantidadDebitos" class="form-control" type="text" readonly
			value="0" />
	</div>
	<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3" align="right">
		<label for="total_credito"><s:text name="cc.total.creditos" /></label>
		<div class="input-group">
			<span class="input-group-addon">$</span> <input id="totalCreditos"
				class="form-control currency numberdecimal" type="text" readonly
				value="0" />
		</div>
	</div>
	<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3" align="right">
		<label for="total_credito"><s:text name="cc.total.debitos" /></label>
		<div class="input-group">
			<span class="input-group-addon">$</span><input id="totalDebitos"
				class="form-control currency numberdecimal" type="text" readonly
				value="0" />
		</div>
	</div>
</div>
 --%>
<script>
	function detalleCuentaCorriente(id) {
		loader(true, null);

		$.ajax({
			method : "POST",
			url : "catastro/cuentacorriente_detail.action",
			data : {
				idCC : id
			},
			traditional : true,
			success : function(res) {
				loader(false, null);
				$("#generic-dialog-response-label").text(
						"Detalle de movimiento de cuenta corriente");
				$("#generic-modal-response-body").html(res);
				$('#generic-dialog-response').modal();
			},
			error : function(res, textStatus, jqXHR) {
				loader(false, null);
				// Levantar dialogo con mensaje de error!!!!!!!!!!!!!
				handleErrorMessages(res.responseJSON);
				//$("#errors-data").text(res.responseJSON.actionErrors[0]);
				$('#error-dialog').modal('show');
			}
		});

	};

	$(document)
			.ready(
					function() {
						//search();

						$('input.numberdecimal').number(true, 2, ',', '');

						$("#cc-grid-data")
								.bootgrid(
										{
											ajax : true,
											navigation : 2,
											rowSelect : false,
											selection : false,
											multiSelect : false,
											search : true,
											columnSelection : false,
											templates : {
												search : ""
											},
											labels : {
												noResults : "No hay datos de cuenta corriente",
												loading : "Cargando informaci�n...",
												infos : "Mostrando {{ctx.start}} a {{ctx.end}} de {{ctx.total}} filas"
											},
											responseHandler : function(data) {												
												return data;
											},
											requestHandler : function(rq) {
												rq.anio = $("#anio").val();
												return sortRequest(rq);
											},
											url : "catastro/cuentacorriente_getData.action",
											statusMapping : {
												0 : "info",
												1 : "warning",
												2 : "danger"
											},
											converters : {
												price : {
													from : function(value) {
														return Number(value
																.replace(",",
																		"."));
													},
													to : function(value) {
														var v = Number(value)
																.toFixed(2);
														return (v + "")
																.replace(".",
																		",");
													}
												},
												date : {
													from : function(value) {
														return moment(value);
													},
													to : function(value) {
														if (!value) {
															return "";
														}
														return moment(value)
																.format(
																		"DD-MM-YYYY");
													}
												}
											},
											formatters : {
												cc_tipo_mov : function(column,
														row) {
													return fmt_cc_tipo_mov(row.tipo);
												}
											}
										}).on("click.rs.jquery.bootgrid",
										function(e, columns, row) {
											// Mostrar informacion de la CC clickeado																			
											detalleCuentaCorriente(row.id);
										});

					});

	
</script>