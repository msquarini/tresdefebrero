
<style>
.cgb-header-name {
	width: 30%;
}

/* Colorear HEADER */
.column .text { color: #f00 !important; }

/*  Estilo celda */
.cell { color: #f00 !important; font-weight: bold; }
            
</style>

<div class="panel panel-default">
	<div class="panel-heading">Resultado de b�squeda</div>
	<div class="panel-body" id="panel_unificacion">

		<table id="cuenta-grid-data"
			class="table table-condensed table-hover table-striped">
			<thead>
				<tr>
					<th data-column-id="id" data-identifier="true" data-type="numeric"
						data-visible="false">IdParcela</th>
					<th data-column-id="idCuenta" data-type="numeric"
						data-visible="false" data-formatter="idCuenta"
						data-sortable="false">IdCuenta</th>
					<th data-column-id="cuenta" data-formatter="nested" data-width="8%">Cuenta</th>
					<th data-column-id="dv" data-formatter="nested" data-sortable="false"						
						data-width="4%">DV</th>
					<th data-column-id="partida" data-formatter="partida"
						data-width="7%">Partida</th>
					<th data-column-id="nomenclatura" data-formatter="nomenclatura"
						data-sortable="false" data-width="36%">Nomenclatura</th>
					<th data-column-id="activa" data-formatter="activa" data-align="center" data-header-align="center"
						data-sortable="false" data-width="8%">Activa</th>
<!-- 					<th data-column-id="createdDate" data-formatter="nesteddate"
						data-sortable="false">Fecha Creacion</th>
					<th data-column-id="createdBy" data-formatter="nested"
						data-sortable="false">Creado Por</th> -->
					<th data-column-id="modifiedDate" data-formatter="nesteddate"
						data-sortable="false">Ultima Modif.</th>
					<th data-column-id="modifiedBy" data-formatter="nested"
						data-sortable="false">Modif. Por</th>
				</tr>
			</thead>

		</table>
		<input type="hidden" id="idSelectedCuenta" />
	</div>
</div>
<script>
	$(document)
			.ready(
					function() {
						$("#cuenta-grid-data")
								.bootgrid(
										{
											ajax : true,
											navigation : 2,
											rowSelect : true,
											selection : true,
											multiSelect : false,
											search : false,
											columnSelection : false,
											templates : {
												search : ""
											},
/* 	                                         responseHandler : function(data) {
	                                        	 
	                                        	 for (var i in data.rows) {
	                                        		 data.rows[i].status = 2;
	                                        	 }
	                                                var response = {
	                                                    current : 1,
	                                                    rowCount : 12,
	                                                    rows : data.rows,
	                                                    total : data.length
	                                                };
	                                                return response;
	                                            }, */
											requestHandler : function(request) {
												return sortRequest(request);
											},
											labels : {
												noResults : "No hay parcelas",
												loading : "Cargando informaci�n...",
												infos : "Mostrando {{ctx.start}} a {{ctx.end}} de {{ctx.total}} filas"
											},
											statusMapping : {												
											    0 : "text-danger",
												1 : "warning",
												2 : "danger"
											},
											url : "catastro/consulta_getData.action",
											formatters : {
												nested : function(column, row) {
													return row.cuenta[column.id];
												},
												nesteddate : function(column,
														row) {
													value = row.cuenta[column.id];
													if (!value) {
														return "";
													}
													return moment(value)
															.format(
																	"DD/MM/YYYY");
												},
												idCuenta : function(column, row) {
													return row.cuenta.id;
												},
												partida : function(column, row) {
													return row.partida;
												},
												nomenclatura : function(column,
														row) {
													return fmt_nomenclatura(row);
												},
												activa : function(column, row) {
													return fmt_boolean(row.cuenta.activa);
												}
											}
										}).on("selected.rs.jquery.bootgrid",
										function(e, rows) {
											//alert("Selected: " + rows[0].id);
											/* 				}).on("click.rs.jquery.bootgrid", function(e, columns, row) {
											 $(document).trigger('cuentaSelectedEvent', row);
											
											 //alert(row.id + " " + row.cuenta.id);
											 //debugger;
											 $('#idSelectedCuenta').val(row.cuenta.id); */
										}).on(
										"selected.rs.jquery.bootgrid",
										function(e, rows) {
											$(document).trigger(
													'cuentaSelectedEvent',
													rows[0]);
											$('#idSelectedCuenta').val(
													rows[0].cuenta.id);
										}).on(
										"deselected.rs.jquery.bootgrid",
										function(e, rows) {
											$(document).trigger(
													'cuentaDeselectedEvent',
													rows[0]);
											$('#idSelectedCuenta').val("-1");
										});
					});
</script>