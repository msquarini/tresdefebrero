<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<style>
.cgb-header-name {
	width: 50%;
}

/* Manito sobre la grilla de datos para indicar que se puede clickear */
#cc-grid-data tbody {
	cursor: pointer;
}

input.currency {
	text-align: right;
	padding-right: 15px;
}
</style>
<script type="text/javascript">
	function closeResponseDialog() {
		// Por compatabilidad con el dialogo	
	}

	$("#anio").on("change", function() {
		if ($(this).val().length == 4) {
			$("#cc-grid-data").bootgrid('reload');
		}
	});

	$('.selectpicker').selectpicker({
		width : '100%'
	});

	function searchCC() {
		loader(true, $('#search_btn'));
		//$('#cc-search-form').submit();
		$.ajax({
			method : "POST",
			url : "catastro/cuentacorriente_search.action",
			data : $('#cc-search-form').serialize(),
			global : false,
			traditional : true,
			success : function(res) {
				$("#cc-grid-data").bootgrid('reload');
				// Respuesta info de la cuenta (Summary) en el caso generico
				$("#cuenta-summary").html(res);
				verGrillaAgrupados(true);
				loader(false, $('#search_btn'));
			},
			error : function(res, textStatus, jqXHR) {
				loader(false, $('#search_btn'));
				handleErrorMessages(res.responseJSON);
				showErrorModalDialog(false);
			}
		});
	};
</script>

<div style="background-color: #eeeeee; padding-top: 7.5px; height: 50px">
	<s:form id="cc-search-form" action="cuentacorriente_search"
		cssClass="form-vertical" namespace="/catastro" theme="bootstrap">

		<s:if test="generic == true">
			<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
				<input id="cuenta" class="form-control number" name="cuenta.cuenta"
					placeholder="Cuenta" maxlength="6" />
			</div>
		</s:if>

		<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
			<div class="input-group date" data-provide="datepicker"
				data-date-format="yyyy" data-date-autoclose="true"
				data-date-view-mode="years" data-date-min-view-mode="years"
				data-date-end-date="y" data-date-orientation="bottom"
				data-date-language="es" data-date-today-btn="true"
				data-date-today-highlight="true">
				<input id="anio" type="text" class="form-control" name="anio"
					value='<s:property value="anio"/>' placeholder="Filtro por a�o">
				<div class="input-group-addon">
					<span class="glyphicon glyphicon-calendar"></span>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
			<select id="tipo" name="tipo" class="selectpicker"
				title="Tipo de Movimiento">
				<option value="D">Deuda</option>
				<option value="C">Cr�dito</option>
				<option value="">Todos</option>
			</select>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-4 col-lg-3">
			<span class="input-group-btn">
				<button id="search_btn" class="btn btn-default" type="button"
					onclick="searchCC();"
					data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Buscando">
					<span class="glyphicon glyphicon-search"></span>
					<s:text name="search.label"></s:text>
				</button>
			</span>
		</div>
	</s:form>
</div>

<!-- En el caso generico SUMMARY de la cuenta -->
<s:if test="generic == true">
	<div id="cuenta-summary"></div>
</s:if>


<div class="row" id="cc_grid_container">


	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div
			class="col-xs-12 col-sm-12 col-md-12 col-lg-12 alert alert-info text-center">
			Movimientos agrupados por periodo</div>
	</div>

	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

		<table id="cc-grid-data"
			class="table table-condensed table-hover table-striped">
			<thead>
				<tr>
					<th data-column-id="id" data-identifier="true" data-type="numeric"
						data-visible="false">IdCC</th>
					<th data-column-id="anio" data-type="numeric" data-visible="true"
						data-width="8%"><s:text name="anio.label" /></th>
					<th data-column-id="cuota" data-type="numeric" data-width="8%"
						data-order="desc"><s:text name="cuota.label" /></th>
					<th data-column-id="cantidadDebitos" data-type="numeric"
						data-align="right" data-header-align="right" data-width="10%"><s:text
							name="cc.cantidad.debitos" /></th>
					<th data-column-id="cantidadCreditos" data-type="numeric"
						data-align="right" data-header-align="right" data-width="10%"><s:text
							name="cc.cantidad.creditos" /></th>
					<th data-column-id="totalDebito" data-type="price"
						data-align="right" data-header-align="right" data-width="15%"><s:text
							name="cc.total.debitos" /></th>
					<th data-column-id="totalCredito" data-type="price"
						data-align="right" data-header-align="right" data-width="15%"><s:text
							name="cc.total.creditos" /></th>
					<th data-column-id="saldo" data-type="price" data-align="right"
						data-header-align="right" data-width="15%"><s:text
							name="cc.saldo" /></th>
				</tr>
			</thead>

		</table>
	</div>
</div>

<div id="cc-movimientos-tab" class="row hidden">
	<s:include value="../cc/cc-movimientos.jsp"></s:include>
</div>

<script>
	function verGrillaAgrupados(v) {
		if (v) {
			$('#cc_grid_container').removeClass('hidden');
			$('#cc-movimientos-tab').addClass('hidden');
		} else {
			$('#cc_grid_container').addClass('hidden');
			$('#cc-movimientos-tab').removeClass('hidden');
		}
	}
	
	/* Ir a pantalla de detalle de movimientos */
	function movimientosCuentaCorriente(a, c) {
		loader(true, null);

		$.ajax({
			method : "POST",
			url : "catastro/cuentacorrientemovimiento_movimientos.action",
			data : {
				anio : a,
				cuota : c
			},
			traditional : true,
			success : function(res) {
				loader(false, null);
				// Oculta la grilla general y muestra los movimientos
				verGrillaAgrupados(false);
				
				$("#cc-movimiento-grid-data").bootgrid('reload');
			},
			error : function(res, textStatus, jqXHR) {
				loader(false, null);
				handleErrorMessages(res.responseJSON);
				showErrorModalDialog(false);
			}
		});

	};

	$(document)
			.ready(
					function() {
						//search();

						$('input.numberdecimal').number(true, 2, ',', '');

						$("#cc-grid-data")
								.bootgrid(
										{
											ajax : true,
											navigation : 2,
											rowSelect : false,
											selection : false,
											multiSelect : false,
											search : true,
											columnSelection : false,
											templates : {
												search : ""
											},
											labels : {
												noResults : "No hay datos de cuenta corriente",
												loading : "Cargando informaci�n...",
												infos : "Mostrando {{ctx.start}} a {{ctx.end}} de {{ctx.total}} filas"
											},
											responseHandler : function(data) {
												// TODO, validar de tomar el total para setear el input de total
												return data;
											},
											requestHandler : function(rq) {
												rq.anio = $("#anio").val();
												return sortRequest(rq);
											},
											url : "catastro/cuentacorriente_getData.action",
											statusMapping : {
												0 : "info",
												1 : "warning",
												2 : "danger"
											},
											converters : {
												price : {
													from : function(value) {
														return Number(value
																.replace(",",
																		"."));
													},
													to : function(value) {
														var v = Number(value)
																.toFixed(2);
														return (v + "")
																.replace(".",
																		",");
													}
												},
												date : {
													from : function(value) {
														return moment(value);
													},
													to : function(value) {
														if (!value) {
															return "";
														}
														return moment(value)
																.format(
																		"DD-MM-YYYY");
													}
												}
											},
											formatters : {
											/* 												cc_tipo_mov : function(column,
											 row) {
											 return fmt_cc_tipo_mov(row.tipo);
											 } */
											}
										}).on(
										"click.rs.jquery.bootgrid",
										function(e, columns, row) {
											// Mostrar informacion de los movimientos para el periodo CC clickeado
											movimientosCuentaCorriente(
													row.anio, row.cuota);
										});

					});

	
</script>