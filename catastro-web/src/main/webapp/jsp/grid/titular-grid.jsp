<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<!-- Grilla de titulares de parcelas en tramites, se permite edicion de CUIT -->

<style>
.cgb-header-name {
	width: 50%;
}
</style>

<script type="text/javascript">
	function edit_cuitcuil_titular(id, apellido, cuitcuil) {
		$('#uuidTP').val(id);
		$('#f_apellido').val(apellido);
		$('#f_cuitcuil').val(cuitcuil);

		//Resetea el validar del formulario para eliminar errores previos
		$('#titular-cuit-form').bootstrapValidator('resetForm');

		$("#titular-sd-btn").show();
		$('#titular-dialog').modal('show');
	}
</script>
<div class="row top-buffer">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<table id="titular-grid-tramite-data"
			class="table table-condensed table-hover table-striped">
			<thead>
				<tr>
					<th data-column-id="id" data-identifier="true" data-type="numeric"
						data-visible="false">idTitular</th>
					<th data-column-id="apellido" data-formatter="nested"
						data-type="string" data-visible="true"><s:text
							name="titular.apellido" /></th>
					<th data-column-id="nombre" data-formatter="nested"
						data-type="string" data-visible="true"><s:text
							name="titular.nombre" /></th>
					<th data-column-id="cuitcuil" data-formatter="nested"><s:text
							name="titular.cuitcuil" /></th>
					<th data-column-id="documento" data-formatter="nested"><s:text
							name="titular.documento" /></th>
					<th data-column-id="porcentaje" data-type="string"><s:text
							name="titular.parcela.porcentaje" /></th>
					<th data-column-id="desde" data-type="date"><s:text
							name="titular.parcela.desde" /></th>
					<th data-column-id="hasta" data-type="date"><s:text
							name="titular.parcela.hasta" /></th>
					<%-- <s:if test="sinAcciones != null"> --%>
					<th data-column-id="commands" data-formatter="commands"
						data-sortable="false"><s:text name="label.acciones" /></th>
					<%-- </s:if> --%>
				</tr>
			</thead>

		</table>

		<!-- Modal Edicion de CUITCUIL de titular -->
		<div class="modal fade" id="titular-dialog" tabindex="-1"
			role="dialog" aria-labelledby="generic-data-label">
			<div class="modal-dialog modal-md" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title" id="titular-dialog-label">
							<s:text name="dialog.title.titular.actualizarCuitCuil" />
						</h4>
					</div>
					<div class="modal-body" id="titular-dialog-body">

						<s:include value="../dialog/titular-cuit-dialog-content.jsp"></s:include>

					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal"
							onclick="closeResponseDialog();">
							<s:text name="dialog.label.cerrar" />
						</button>
						<button id="titular-sd-btn" type="button" class="btn btn-primary"
							onclick="dialog_titular_cuit_submit();">
							<s:text name="dialog.label.grabar" />
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$(document)
			.ready(
					function() {
						$("#titular-grid-tramite-data")
								.bootgrid(
										{
											ajax : true,
											navigation : 2,
											rowSelect : true,
											//selection : true,
											multiSelect : false,
											search : true,
											columnSelection : false,
											templates : {
												search : ""
											},
											labels : {
												noResults : "No hay titulares",
												loading : "Cargando información...",
												infos : "Mostrando {{ctx.start}} a {{ctx.end}} de {{ctx.total}} filas"
											},
											url : "catastro/titularABM_getData.action",
											requestHandler : function(request) {
												return sortRequest(request);
											},
											converters : {
												date : {
													from : function(value) {
														return moment(value);
													},
													to : function(value) {
														if (!value) {
															return "";
														}
														return moment(value)
																.format(
																		"DD-MM-YYYY");
													}
												}
											},
											formatters : {
												/* Utilizar notacion [] para acceder a las propiedades 
												   y usar solo 1 formatter,
												   nested: f(column,row) { return row.titular[column.id]; }*/
												nested : function(column, row) {
													return row.titular[column.id];
												},
												commands : function(column, row) {
													var idrow = row.uuidString;
													return "<button onclick=\"edit_cuitcuil_titular('"
															+ idrow
															+ "','"
															+ row.titular.apellido
															+ "','"
															+ row.titular.cuitcuil
															+ "');\" type=\"button\" class=\"btn btn-xs btn-default command-edit\" data-row-id=\""
															+ idrow
															+ "\"><span class=\"icon glyphicon glyphicon-pencil\"></span></button> ";
													/* } else {
														return "";
													} */

												}
											}
										})
								.on("selected.rs.jquery.bootgrid",
										function(e, rows) {
											//alert("Selected: " + rows[0].id);
											/* 				}).on("click.rs.jquery.bootgrid", function(e, columns, row) {
											 $(document).trigger('cuentaSelectedEvent', row);
											
											 //alert(row.id + " " + row.cuenta.id);
											 //debugger;
											 $('#idSelectedCuenta').val(row.cuenta.id); */
										})
								.on(
										"selected.rs.jquery.bootgrid",
										function(e, rows) {
											$(document).trigger(
													'ccSelectedEvent', rows[0]);
										}).on("deselected.rs.jquery.bootgrid",
										function(e, rows) {
											/* $(document).trigger('cuentaDeselectedEvent', rows[0]);
											$('#idSelectedCuenta').val("-1"); */
										});
					});
</script>
