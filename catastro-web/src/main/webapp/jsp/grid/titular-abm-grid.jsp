<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<!-- Grilla ABM de Titulares (Reutilizable!) -->

<style>
</style>

<script type="text/javascript">
	var idTitular = -1;

	$(document).off("titularSelectedEvent").on("titularSelectedEvent",
			function(event, data) {
				//$("#idTitular").val(data.id);
				idTitular = data.id
				$("#seleccionar-dialog-button").toggleClass('disabled', false);
			}).off("titularDeselectedEvent").on("titularDeselectedEvent",
			function(event, data) {
				//$("#idTitular").val("-1");
				idTitular = -1;
				$("#seleccionar-dialog-button").toggleClass('disabled', true);
			});

	function searchTitular() {
		$.ajax({
			method : "POST",
			url : "catastro/titular_input.action",
			data : {
			//idCC : id
			},
			traditional : true,
			success : function(res) {
				loader(false, null);
				var btn_array = [ "nuevo-dialog-button",
						"seleccionar-dialog-button" ];
				showModalDialog("Seleccion de titular", res, btn_array);
			},
			error : function(res, textStatus, jqXHR) {
				loader(false, null);
				// Levantar dialogo con mensaje de error!!!!!!!!!!!!!
				handleErrorMessages(res.responseJSON);
				showErrorModalDialog(false);
			}
		});
	}

	/* Recarga de grilla al cerrar dialogo */
	$('#generic-dialog').on('hidden.bs.modal', function() {
		$('#titular-abm-grid-data').bootgrid('reload');
	})

	function cerrarDialogo() {
		$('#generic-dialog').modal('hide');
	}

	/* Seleccionar Titular */
	function seleccionar() {
		$.ajax({
			method : "POST",
			url : "catastro/titularABM_addSelectedTitular.action",
			data : {
				idTitular : idTitular
			},
			traditional : true,
			success : function(res) {
				cerrarDialogo();
			},
			error : function(res, textStatus, jqXHR) {
				// Levantar dialogo con mensaje de error!!!!!!!!!!!!!
				handleErrorMessages(res.responseJSON);
				showErrorModalDialog(false);
			}
		});
	}

	/* Presenta dialogo de edicion de titular para alta de nuevo titular */
	function nuevo() {
		$.ajax({
			method : "POST",
			url : "catastro/titularABM_goToAddTitular.action",
			data : {},
			traditional : true,
			success : function(res) {
				loader(false, null);
				var btn_array = [ "submit-dialog-button" ];
				showModalDialog("Nuevo titular", res, btn_array);
			},
			error : function(res, textStatus, jqXHR) {
				loader(false, null);
				// Levantar dialogo con mensaje de error!!!!!!!!!!!!!
				handleErrorMessages(res.responseJSON);
				showErrorModalDialog(false);
			}
		});
	}

	/* Accion de edicion o eliminacion  */
	function sd_accion_submit(id, isCuitcuil, edit) {
		$.ajax({
			method : "POST",
			url : "catastro/titularABM_goToEditTitular.action",
			data : {
				uuidTP : id
			},
			traditional : true,
			success : function(res) {
				loader(false, null);
				var btn_array = [ "submit-dialog-button" ];
				showModalDialog("Nuevo titular", res, btn_array);
			},
			error : function(res, textStatus, jqXHR) {
				loader(false, null);
				// Levantar dialogo con mensaje de error!!!!!!!!!!!!!
				handleErrorMessages(res.responseJSON);
				showErrorModalDialog(false);
			}
		});
	};
</script>

<div class="row top-buffer">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<table id="titular-abm-grid-data"
			class="table table-condensed table-hover table-striped">
			<thead>
				<tr>
					<th data-column-id="id" data-identifier="true" data-type="numeric"
						data-visible="false">idTitularParcela</th>
					<th data-column-id="apellido" data-type="string" data-width="25%"
						data-formatter="nested" data-visible="true"><s:text
							name="titular.apellido" /></th>
					<th data-column-id="nombre" data-type="string" data-width="15%"
						data-formatter="nested" data-visible="true"><s:text
							name="titular.nombre" /></th>
					<th data-column-id="cuitcuil" data-identifier="true"
						data-width="10%" data-formatter="nested" data-type="numeric"><s:text
							name="titular.cuitcuil" /></th>
					<th data-column-id="documento" data-type="string" data-width="10%"
						data-formatter="nested"><s:text name="titular.documento" /></th>
					<th data-column-id="porcentaje" data-type="price" data-width="10%"><s:text
							name="titular.parcela.porcentaje" /></th>
					<th data-column-id="desde" data-type="date"><s:text
							name="titular.parcela.desde" /></th>
					<th data-column-id="hasta" data-type="date"><s:text
							name="titular.parcela.hasta" /></th>
					<th data-column-id="commands" data-formatter="commands"
						data-sortable="false"><s:text name="label.acciones" /></th>
				</tr>
			</thead>
		</table>
	</div>
</div>
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<s:if test='tramite.estado.toString() != "FINALIZADO"'>
			<button id="addTitular" class="btn btn-primary " value="Agregar"
				onclick="searchTitular();">
				<s:text name="dialog.label.agregar" />
			</button>
		</s:if>
	</div>
</div>

<script>
	$(document)
			.ready(
					function() {
						$("#titular-abm-grid-data")
								.bootgrid(
										{
											ajax : true,
											navigation : 2,
											rowSelect : true,
											selection : false,
											//multiSelect : false,
											search : true,
											columnSelection : false,
											templates : {
												search : ""
											},
											labels : {
												noResults : "No hay titulares",
												loading : "Cargando información...",
												infos : "Mostrando {{ctx.start}} a {{ctx.end}} de {{ctx.total}} filas"
											},
											url : "catastro/titularABM_getData.action",
											requestHandler : function(request) {
												return sortRequest(request);
											},
											converters : {
												price : {
													from : function(value) {
														return Number(value
																.replace(",",
																		"."));
													},
													to : function(value) {
														return (value + "")
																.replace(".",
																		",");
													}
												},
												date : {
													from : function(value) {
														return moment(value);
													},
													to : function(value) {
														if (!value) {
															return "";
														}
														return moment(value)
																.format(
																		"DD-MM-YYYY");
													}
												}
											},
											formatters : {
												nested : function(column, row) {
													return row.titular[column.id];
												},
												commands : function(column, row) {
													return fmt_accion_titular_grilla(row);
												}
											}
										}).on(
										"selected.rs.jquery.bootgrid",
										function(e, rows) {
											$(document).trigger(
													'titularSelectedEvent',
													rows[0]);
											$('#idTitular').val(rows[0].id);
										}).on(
										"deselected.rs.jquery.bootgrid",
										function(e, rows) {
											$(document).trigger(
													'titularDeselectedEvent',
													rows[0]);
											$('#idTitular').val("-1");
										});
					});
</script>
