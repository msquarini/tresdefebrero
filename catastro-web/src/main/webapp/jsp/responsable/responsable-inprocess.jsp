<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<!-- Tramite de gestion de responsables
	NO SE PERMITE GRABACION DEL TRAMITE!!!!
-->

<script type="text/javascript">
	function procesarTramite() {
		  loader(true, $('#btn_process'));
	        $.ajax({
	            method : "POST",
	            url : "catastro/responsable_process.action",
	            data : {
	            },
	            traditional : true,
	            success : function(res) {
	                loader(false,  $('#btn_process'));                
	                showResponseModalDialog(true, res);
	            },
	            error : function(res, textStatus, jqXHR) {
	                loader(false,  $('#btn_process'));
	                handleErrorMessages(res.responseJSON);
	                showErrorModalDialog(false);
	            }
	        });
	}

</script>

<style type="text/css">
</style>

<s:include value="../common/tramite-header.jsp"></s:include>

<ul class="nav nav-pills"
	style="background-color: #eeeeee; border-bottom-style: ridge;">
	<li class="nav-header disabled"><a><s:text
				name="tramite.responsable.label" /></a></li>
	<li class="active"><a data-toggle="tab" href="#cuenta-origen"><s:text
				name="cuenta.label" /></a></li>
	<li><a data-toggle="tab" href="#titulares"><s:text
				name="titulares.label" /></a></li>
	<li><a data-toggle="tab" href="#responsable"><s:text
				name="responsables.label" /></a></li>

	<s:if test='tramite.estado.toString() != "FINALIZADO"'>
		<li class="pull-right" style="padding-top: 3px; padding-right: 3px">
			<button id="btn_process" class="btn btn-danger" type="button"
				onclick="procesarTramite()">
				<span class="fa fa-wrench"></span>&nbsp;
				<s:text name="dialog.label.procesar" />
				&nbsp;
			</button>
		</li>
	</s:if>
</ul>

<div class="tab-content">
	<div id="cuenta-origen" class="tab-pane fade in active">
		<s:include value="../cuenta-info.jsp"></s:include>
	</div>
	<div id="titulares" class="tab-pane fade">
		<s:include value="../grid/titular-grid.jsp"></s:include>
	</div>
	<div id="responsable" class="tab-pane fade">
		<s:include value="responsable-abm-grid.jsp"></s:include>
	</div>
</div>

