<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<!-- Grilla de responsables de la cuenta -->

<style>
.cgb-header-name {
	width: 50%;
}
</style>

<script type="text/javascript">
	var idTitular = -1;

	$(document).off("titularSelectedEvent").on("titularSelectedEvent",
			function(event, data) {
				idTitular = data.id
				$("#seleccionar-dialog-button").toggleClass('disabled', false);
			}).off("titularDeselectedEvent").on("titularDeselectedEvent",
			function(event, data) {
				idTitular = -1;
				$("#seleccionar-dialog-button").toggleClass('disabled', true);
			});

	function searchResponsable() {
		$.ajax({
			method : "POST",
			url : "catastro/titular_input.action",
			data : {
			//idCC : id
			},
			traditional : true,
			success : function(res) {
				loader(false, null);
				var btn_array = [ "nuevo-dialog-button",
						"seleccionar-dialog-button" ];
				showModalDialog("Seleccion de responsable", res, btn_array);
			},
			error : function(res, textStatus, jqXHR) {
				loader(false, null);
				handleErrorMessages(res.responseJSON);
				showErrorModalDialog(false);
			}
		});
	}

	/* Recarga de grilla al cerrar dialogo */
	$('#generic-dialog').on('hidden.bs.modal', function() {
		$('#responsables-grid-data').bootgrid('reload');
	})

	function cerrarDialogo() {
		$('#generic-dialog').modal('hide');
	}

	/* Seleccionar Titular */
	function seleccionar() {
		$.ajax({
			method : "POST",
			url : "catastro/responsable_addSelectedTitular.action",
			data : {
				idTitular : idTitular
			},
			traditional : true,
			success : function(res) {
				cerrarDialogo();
			},
			error : function(res, textStatus, jqXHR) {
				handleErrorMessages(res.responseJSON);
				showErrorModalDialog(false);
			}
		});
	}

	/* Presenta dialogo de edicion de titular para alta de nuevo titular */
	function nuevo() {
		$.ajax({
			method : "POST",
			url : "catastro/responsable_goToAddTitular.action",
			data : {},
			traditional : true,
			success : function(res) {
				loader(false, null);
				var btn_array = [ "submit-dialog-button" ];
				showModalDialog("Nuevo responsable", res, btn_array);
			},
			error : function(res, textStatus, jqXHR) {
				loader(false, null);
				handleErrorMessages(res.responseJSON);
				showErrorModalDialog(false);
			}
		});
	}

	/* Accion de edicion o eliminacion  */
	function sd_accion_submit(id, isCuitcuil, edit) {
		$.ajax({
			method : "POST",
			url : "catastro/responsable_goToEditTitular.action",
			data : {
				uuidTP : id
			},
			traditional : true,
			success : function(res) {
				loader(false, null);
				var btn_array = [ "submit-dialog-button" ];
				showModalDialog("Modificar datos de responsable", res,
						btn_array);
			},
			error : function(res, textStatus, jqXHR) {
				loader(false, null);
				// Levantar dialogo con mensaje de error!!!!!!!!!!!!!
				handleErrorMessages(res.responseJSON);
				showErrorModalDialog(false);
			}
		});
	};
</script>
<%-- 
<!--  Formulario para la edicion o delete de titulares-->
<s:form id="responsable-abm-form" action=responsable_goToEditTitular
	"
	cssClass="form-vertical" namespace="/catastro" theme="bootstrap">
	<s:hidden id="idTitular" name="idTitular"></s:hidden>
	<s:hidden id="uuidTP" name="uuidTP"></s:hidden>
</s:form> --%>

<div class="row top-buffer">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<table id="responsables-grid-data"
			class="table table-condensed table-hover table-striped">
			<thead>
				<tr>
					<th data-column-id="id" data-identifier="true" data-type="numeric"
						data-visible="false">idResponsable</th>
					<th data-column-id="apellido" data-formatter="nested"
						data-type="string" data-visible="true"><s:text
							name="titular.apellido" /></th>
					<th data-column-id="nombre" data-formatter="nested"
						data-type="string" data-visible="true"><s:text
							name="titular.nombre" /></th>
					<th data-column-id="cuitcuil" data-formatter="nested"><s:text
							name="titular.cuitcuil" /></th>
					<th data-column-id="documento" data-formatter="nested"><s:text
							name="titular.documento" /></th>
					<th data-column-id="desde" data-type="date"><s:text
							name="titular.parcela.desde" /></th>
					<th data-column-id="hasta" data-type="date"><s:text
							name="titular.parcela.hasta" /></th>
					<!-- Esta columna se habilita cuando se permite modidicacion de responsable -->
					<th data-column-id="commands" data-formatter="commands"
						data-sortable="false"><s:text name="label.acciones" /></th>
				</tr>
			</thead>
		</table>
	</div>
</div>
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<s:if test='tramite.estado.toString() != "FINALIZADO"'>
			<button id="addResponsable" class="btn btn-primary " value="Agregar"
				onclick="searchResponsable();">
				<s:text name="dialog.label.agregar" />
			</button>
		</s:if>
	</div>
</div>

<script>
	$(document)
			.ready(
					function() {
						$("#responsables-grid-data")
								.bootgrid(
										{
											ajax : true,
											navigation : 2,
											rowCount : 5,
											rowSelect : true,
											//selection : true,
											multiSelect : false,
											search : true,
											columnSelection : false,
											templates : {
												search : ""
											},
											labels : {
												noResults : "No hay responsables",
												loading : "Cargando información...",
												infos : "Mostrando {{ctx.start}} a {{ctx.end}} de {{ctx.total}} filas"
											},
											url : "catastro/responsable_getData.action",
											requestHandler : function(request) {
												return sortRequest(request);
											},
											converters : {
												date : {
													from : function(value) {
														return moment(value);
													},
													to : function(value) {
														if (value) {
															return moment(value)
																	.format(
																			"DD-MM-YYYY");
														} else {
															return "";
														}
													}
												}
											},
											formatters : {
												nested : function(column, row) {
													return row.titular[column.id];
												},
												commands : function(column, row) {
													return fmt_accion_titular_grilla(row);
												}
											}
										});
					});
</script>
