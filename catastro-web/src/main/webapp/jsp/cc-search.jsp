<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<script type="text/javascript">
	$(document).off("cuentaSelectedEvent").on("cuentaSelectedEvent",
			function(event, data) {
				$("#idCuenta").val(data.cuenta.id);
				$("#seleccionar_btn").toggleClass('disabled', false);
			}).off("cuentaDeselectedEvent").on("cuentaDeselectedEvent",
			function(event, data) {
				$("#idCuenta").val("-1");
				$("#seleccionar_btn").toggleClass('disabled', true);
			});
</script>
<style type="text/css">
</style>


<s:include value="cuenta_search.jsp"></s:include>

<div class="row">
	<s:form id="selec-init-form" action="cuentacorriente_search"
		namespace="/catastro" theme="bootstrap">
		<s:hidden id="idCuenta" name="idCuenta"></s:hidden>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<sj:a id="seleccionar_btn" cssClass="btn btn-primary disabled"
				value="Seleccionar" formIds="cc-init-form"
				targets="container">Seleccionar</sj:a>
		</div>
	</s:form>
</div>