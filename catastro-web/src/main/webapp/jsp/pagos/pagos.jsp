<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%-- <%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%> --%>


<style type="text/css">
</style>

<ul class="nav nav-pills"
	style="background-color: #eeeeee; border-bottom-style: ridge;">
	<li class="nav-header disabled"><a><strong>Gestion de
				Pagos</strong></a></li>

	<li><a id="upload-pagos-tab" data-toggle="tab" href="#upload"><s:text
				name="pagos.upload.label" /></a></li>

	<li><a id="file-pagos-tab" data-toggle="tab" href="#file_pagos"><s:text
				name="pagos.files.label" /></a></li>
</ul>

<div class="tab-content top-buffer">

	<div id="upload" class="tab-pane fade in">
		<s:include value="pagos-upload.jsp"></s:include>
	</div>

	<div id="file_pagos" class="tab-pane fade">
		<s:include value="pagos-grid.jsp"></s:include>
	</div>
</div>

<s:if test="tab == 2">
	<script type="text/javascript">
		$('#file-pagos-tab').trigger('click');
	</script>
</s:if>
<s:else>
    <script type="text/javascript">
        $('#upload-pagos-tab').trigger('click');
    </script>
</s:else>