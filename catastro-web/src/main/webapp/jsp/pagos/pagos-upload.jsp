<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<!-- Formulario de UPLOAD de files de pagos -->

<script src="js/upload/fileinput.js"></script>
<script src="js/upload/locales/es.js"></script>
<script src="js/upload/plugins/sortable.js" type="text/javascript"></script>
<link rel="stylesheet" href="css/upload/fileinput.css">

<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<input id="input-id" type="file" name="pagos" multiple>
	</div>
</div>
<script>
	$(document).ready(function() {

		$("#input-id").fileinput({
			uploadUrl : '/catastro-web/pagos/pagos_upload.action',
			language : 'es',
			allowedFileExtensions : [ 'txt' ],
			uploadAsync : true,
			showUpload : true,
			minFileCount : 1,
			maxFileCount : 10,
			fileActionSettings : {
				showUpload : false,
				showPreview : true,
				showRemove : true
			}
		});
	});

	$("#input-id")
			.on(
					'fileuploaded',
					function(event, data, previewId, index) {
						alert(1);

						debugger;
						var form = data.form, files = data.files, extra = data.extra, response = data.response, reader = data.reader;
						console.log('File uploaded triggered');
					});
	
    $('#input-id').on('filebatchuploadcomplete', function(event, files, extra) {
        console.log('File batch upload complete');
    });
    $('#input-id').on('filebatchuploadsuccess', function(event, data) {
        var form = data.form, files = data.files, extra = data.extra,
            response = data.response, reader = data.reader;
        console.log('File batch upload success');
        debugger;
    });
    $('#input-id').on('filebatchuploaderror', function(event, data, msg) {
        var form = data.form, files = data.files, extra = data.extra,
            response = data.response, reader = data.reader;
        console.log('File batch upload error');
      debugger;
    });
</script>