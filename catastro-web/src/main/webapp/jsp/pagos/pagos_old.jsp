<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<script src="js/upload/fileinput.js"></script>
<script src="js/upload/locales/es.js"></script>
<script src="js/upload/plugins/sortable.js" type="text/javascript"></script>
    
    
    
<link rel="stylesheet" href="css/upload/fileinput.css">


<style type="text/css">
</style>

<div class="row">
	<s:actionerror theme="bootstrap" />
</div>
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<%-- <s:form id="filepago-init-form" action="pagos_upload"
			namespace="/pagos" theme="bootstrap">
 --%>

		<input id="input-id" type="file" name="pagos[]" multiple>

		<!-- <div class="file-loading">
				<input id="filePago" name="upload" type="file" multiple />
			</div> -->

		<%-- 	<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
				<sj:a id="uploadPagos" cssClass="btn btn-primary" value="Cargar"
					formIds="filepago-init-form" targets="container">Cargar</sj:a>
			</div> --%>

		<!-- <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">result</div> -->
		<%-- 	</s:form> --%>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {

		$('#input-id').fileinput({
			//language : 'es',
			//uploadUrl : '/catastro-web/pagos/pagos_preprocess.action',
			allowedFileExtensions : [ 'txt' ],
			showUpload : true,
		    minFileCount: 1,
		    maxFileCount: 10,
			fileActionSettings : {
				showUpload : true,
	            showPreview : true,
	            showRemove: true           
			}
		});

	});

	
</script>