<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<script type="text/javascript">
	
</script>

<form id="file-pagos-info">

	<fieldset disabled="disabled">

		<div class="panel panel-default">
			<div class="panel-heading">Archivo de Pagos</div>
			<div class="panel-body" id="panel_file-pagos">
				<div class="row">
					<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
						<label for="cuenta">Nombre</label> <input class="form-control"
							type="text"
							value='<s:property value="filePago.nombre"></s:property>' />
					</div>

					<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
						<label for="dv">Cant. Pagos</label> <input class="form-control"
							type="text" id="dv"
							value='<s:property value="filePago.cantidadPagos"></s:property>' />
					</div>
					<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
						<label for="partida">Cant. Pagos Errores</label> <input
							class="form-control" type="text"
							value='<s:property value="filePago.errores"></s:property>' />
					</div>
				</div>
			</div>
		</div>

	</fieldset>
</form>

<div class="panel panel-default">
	<div class="panel-heading">Detalle de Pagos</div>
	<div class="panel-body" id="panel_file_detalle_pagos">
		<s:include value="filepago-grid.jsp"></s:include>
	</div>
</div>