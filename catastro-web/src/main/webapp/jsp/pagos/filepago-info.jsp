<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<script type="text/javascript">
	
</script>

<style type="text/css">
input[readonly].info {
	background-color: transparent;
	border: 1;
	font-size: 1em;
}
</style>

<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="panel panel-info">
			<div class="panel-heading clearfix">

				<strong class="panel-title pull-left" style="padding-top: 7.5px;"><s:text
						name="pago.file.info" /></strong>
				<div id="filePagoInfo-volver-btn" class="btn-group pull-right">
					<s:url var="pagosURL" action="pagos_search"
						namespace="/pagos" />
					<sj:a href="%{pagosURL}" class="btn btn-primary btn-sm" targets="container">
						<i class="fa fa-arrow-circle-left" aria-hidden="true"></i>  Volver</sj:a>
				</div>
			</div>

			<div class="panel-body">
				<div class="row">
					<div class="col-xs-6 col-sm-6 col-md-4 col-lg-2">
						<label for="mincc"><s:text name="pago.file.nombre" /></label> <input
							class="form-control info" type="text" readonly
							value='<s:property value="filePago.nombre"></s:property>' />
					</div>
					<div class="col-xs-6 col-sm-6 col-md-4 col-lg-2">
						<label for="avgcc"><s:text name="pago.file.origen" /></label> <input
							class="form-control info" type="text" readonly
							value='<s:property value="filePago.origen.descripcion"></s:property>' />
					</div>
					<div class="col-xs-6 col-sm-6 col-md-4 col-lg-2">
						<label for="maxcc"><s:text name="periodo.estado.label" /></label>
						<input class="form-control info" type="text" readonly
							value='<s:property value="filePago.estado.name"></s:property>' />
					</div>
					<div class="col-xs-6 col-sm-6 col-md-4 col-lg-2">
						<label for="cant"><s:text name="pago.file.fecha" /></label> <input
							class="form-control info" type="text" readonly
							value='<s:property value="filePago.fechaFile"></s:property>' />
					</div>
					<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
						<label for="cant"><s:text name="pago.file.hash" /></label> <input
							class="form-control info " type="text" readonly
							value='<s:property value="filePago.hash"></s:property>' />
					</div>

				</div>
				<div class="row top-buffer">
					<div class="col-xs-6 col-sm-6 col-md-4 col-lg-2">
						<label for="cant"><s:text name="pago.file.registros" /></label> <input
							class="form-control info" type="text" readonly
							value='<s:property value="filePago.cantidadRegistros"></s:property>' />
					</div>
					<div class="col-xs-6 col-sm-6 col-md-4 col-lg-2">
						<label for="maxcc"><s:text name="label.total" /></label> <input
							class="form-control info" type="text" readonly
							value='<s:property value="getText('money.format',{filePago.total})"></s:property>' />
					</div>
					<div class="col-xs-6 col-sm-6 col-md-4 col-lg-2">
						<label for="cant"><s:text name="pago.file.procesados" /></label>
						<input class="form-control info" type="text" readonly
							value='<s:property value="filePago.procesados"></s:property>' />
					</div>
					<div class="col-xs-6 col-sm-6 col-md-4 col-lg-2">
						<label for="cant"><s:text name="pago.file.sinDeuda" /></label> <input
							class="form-control info" type="text" readonly
							value='<s:property value="filePago.sinDeuda"></s:property>' />
					</div>
					<div class="col-xs-6 col-sm-6 col-md-4 col-lg-2">
						<label for="cant"><s:text name="pago.file.errores" /></label> <input
							class="form-control info" type="text" readonly
							value='<s:property value="filePago.errores"></s:property>' />
					</div>
					<div class="col-xs-6 col-sm-6 col-md-4 col-lg-2">
						<label for="cant"><s:text name="pago.file.fechaproceso" /></label>
						<input class="form-control info" type="text" readonly
							value='<s:property value="filePago.modifiedDate"></s:property>' />
					</div>
				</div>
			</div>
		</div>
	</div>
</div>