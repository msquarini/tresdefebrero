<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<style>
.small-col {
	font-size: small;
}
</style>

<script src="js/select/bootstrap-select.min.js"></script>
<link rel="stylesheet" href="css/select/bootstrap-select.css">

<s:form id="pagos-search-form" action="pagos_search"
	cssClass="form-vertical" namespace="/pagos" theme="bootstrap">

	<div style="background-color: #eeeeee; padding-top: 7.5px; height: 50px">

		<div class="col-xs-12 col-sm-12 col-md-3 col-lg-2">
			<div class="form-group">
				<div class="input-group date" data-provide="datepicker" id="desde"
					data-date-format="dd/mm/yyyy" data-date-autoclose="true"
					data-date-orientation="bottom" data-date-language="es"
					data-date-today-btn="true" data-date-today-highlight="true">
					<input type="text" class="form-control" placeholder="Fecha desde"
						value='<s:property value="from"/>' name="from">
					<div class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"></span>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-3 col-lg-2">
			<div class="form-group">
				<div class="input-group date" data-provide="datepicker" id="desde"
					data-date-format="dd/mm/yyyy" data-date-autoclose="true"
					data-date-orientation="bottom" data-date-language="es"
					data-date-today-btn="true" data-date-today-highlight="true">
					<input type="text" class="form-control" placeholder="Fecha hasta"
						value='<s:property value="to"/>' name="to">
					<div class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"></span>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-4 col-lg-3">
			<select id="estado" name="estados" class="selectpicker"
				title="Estado" multiple>
				<option value="0">Pendiente</option>
				<option value="3">Finalizado</option>
				<option value="2">Error</option>
				<option value="1">En Proceso</option>
			</select>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-4 col-lg-2">
			<span class="input-group-btn">
				<button id="search_btn" class="btn btn-default" type="button"
					onclick="search();"
					data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Buscando">
					<span class="glyphicon glyphicon-search"></span>
					<s:text name="search.label"></s:text>
				</button>
			</span>
		</div>

		<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 pull-right" align="right">
			<%-- <span class="input-group-btn"> --%>
			<button id="process_btn" class="btn btn-danger" type="button"
				data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Procesando..."
				onclick="procesar();">
				<span class="glyphicon glyphicon-erase"></span>Procesar todos
			</button>
			<%-- </span> --%>
		</div>
	</div>
</s:form>

<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<table id="pagos-grid-data"
			class="table table-condensed table-hover table-striped">
			<thead>
				<tr>
					<th data-column-id="id" data-identifier="true" data-type="numeric"
						data-visible="false">idFilePago</th>
					<th data-column-id="nombre" data-width="18%" data-sortable="false"><s:text
							name="pago.file.nombre" /></th>
					<th data-column-id="origen" data-width="8%" data-sortable="false"><s:text
							name="pago.file.origen" /></th>
					<th data-column-id="fechaFile" data-type="date" data-width="7%"><s:text
							name="pago.file.fecha" /></th>
					<th data-column-id="estado" data-formatter="fmt_status"
						data-width="9%"><s:text name="periodo.estado.label" /></th>
					<th data-column-id="createdDate" data-type="date" data-width="7%"><s:text
							name="pago.file.upload" /></th>
					<th data-column-id="modifiedDate" data-type="date" data-width="7%"><s:text
							name="pago.file.fechaproceso" /></th>
					<th data-column-id="cantidadRegistros" data-sortable="false"
						data-width="6%"><s:text name="pago.file.registros" /></th>
					<!-- 					<th data-column-id="procesados" data-width="8%"># Procesados</th> -->
					<th data-column-id="errores" data-sortable="false" data-width="7%"><s:text
							name="pago.file.errores" /></th>
					<th data-column-id="commands" data-sortable="false"
						data-formatter="commands" data-width="7%">Acciones</th>
				</tr>
			</thead>

		</table>
	</div>
</div>

<div class="row">
	<div id="filePago-info" class="container"></div>
</div>


<s:form id="pagos-procesar-form" action="pagos_procesar"
	namespace="/pagos">
</s:form>

<s:form id="detalle-pagos-form" action="detallePagos_input"
	namespace="/pagos">
	<input type="hidden" id="idFilePago" name="idFilePago" />
</s:form>

<script>
	function search() {
		loader(true, $('#search_btn'));
		$("#filePago-info").html("");
		$('#pagos-search-form').submit();
	};

	function procesar() {
		loader(true, $('#process_btn'));
		$("#filePago-info").html("");
		$('#pagos-procesar-form').submit();
	};

	$('#pagos-search-form').ajaxForm({
		success : function(res, textStatus, jqXHR) {
			$("#pagos-grid-data").bootgrid('reload');
			loader(false, $('#search_btn'));
		},
		error : function(res, textStatus, jqXHR) {
			loader(false, $('#search_btn'));
			alert(res);
		}
	});

	$('#pagos-procesar-form').ajaxForm({
		success : function(res, textStatus, jqXHR) {
			$("#pagos-grid-data").bootgrid('reload');
			loader(false, $('#process_btn'));
		},
		error : function(res, textStatus, jqXHR) {
			loader(false, $('#process_btn'));
			alert(res);
		}
	});

	$('.selectpicker').selectpicker({
		width : '100%'
	});

	$(document)
			.ready(
					function() {
						$("#pagos-grid-data")
								.bootgrid(
										{
											ajax : true,
											navigation : 2,
											rowCount : 5,
											rowSelect : false,
											selection : false,
											multiSelect : false,
											search : false,
											columnSelection : false,
											templates : {
												search : ""
											},
											labels : {
												noResults : "No se encontraron archivos de pagos.",
												loading : "Cargando información...",
												infos : "Mostrando {{ctx.start}} a {{ctx.end}} de {{ctx.total}} filas"
											},
											formatters : {
												nested : function(column, row) {
													return row.cuenta[column.id];
												},
												commands : function(column, row) {
													var idrow = row.id;
													return "<button onclick=\"detalle_pagos('"
															+ idrow
															+ "');"
															+ "\" type=\"button\" class=\"btn btn-xs btn-default command-edit\" data-row-id=\""
															+ idrow
															+ "\"><span class=\"fa fa-info-circle\" aria-hidden=\"true\"></span></button> ";
												},
												fmt_status : function(column,
														row) {
													return fmt_status(row[column.id]);
												}
											},
											converters : {
												date : {
													from : function(value) {
														return moment(value);
													},
													to : function(value) {
														if (!value) {
															return "";
														}
														return moment(value)
																.format(
																		"DD-MM-YYYY");
													}
												}
											},
											url : "pagos/pagos_getData.action"
										}).on("loaded.rs.jquery.bootgrid",
										function() {

										}).on("click.rs.jquery.bootgrid",
										function(e, columns, row) {
											// Mostrar informacion del file clickeado
											filePagoInfo(row.id);
										});

					});

	/* Informacion completa del archivo de pagos con grilla de pagos */
	function detalle_pagos(id) {
		loader(true, null);
		$("#idFilePago").val(id);
		$('#detalle-pagos-form').submit();
	};

	$('#detalle-pagos-form').ajaxForm({
		target : '#container',
		success : function(res, textStatus, jqXHR) {
			loader(false, null);
		},
		error : function(res, textStatus, jqXHR) {
			loader(false, null);
			alert(res);
		}
	});

	/* Informacion simple del archivo de pagos */
	function filePagoInfo(id) {
		$.ajax({
			method : "POST",
			url : "pagos/pagos_infoFilePago.action",
			data : {
				idFilePago : id
			},
			traditional : true,
			success : function(res) {
				$("#filePago-info").html(res);
				$("#filePagoInfo-volver-btn").hide();
			},
			error : function(res, textStatus, jqXHR) {
				// Levantar dialogo con mensaje de error!!!!!!!!!!!!!
				handleErrorMessages(res.responseJSON);
				//$("#errors-data").text(res.responseJSON.actionErrors[0]);
				$('#error-dialog').modal('show');
			}
		});
	}

	
</script>