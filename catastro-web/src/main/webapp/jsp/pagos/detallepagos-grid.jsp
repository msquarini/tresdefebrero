<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<style>
.small-col {
	font-size: small;
}
</style>

<script src="js/select/bootstrap-select.min.js"></script>
<link rel="stylesheet" href="css/select/bootstrap-select.css">

<div class="row">
    <s:include value="filepago-info.jsp"></s:include>
</div>
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<table id="detalle-pagos-grid-data"
			class="table table-condensed table-hover table-striped">
			<thead>
				<tr>
					<th data-column-id="id" data-identifier="true" data-type="numeric"
						data-visible="false">idPago</th>
					<th data-column-id="cuenta" data-formatter="cuenta" data-header-align="center" 
						data-width="10%">Cuenta/DV</th>
					<th data-column-id="fechaVto" data-type="date" data-width="10%" data-header-align="center">Fecha
						Vto</th>
					<th data-column-id="fechaPago" data-type="date" data-width="10%" data-header-align="center">Fecha
						Pago</th>
					<th data-column-id="anio" data-width="4%" data-header-align="center">Anio</th>
					<th data-column-id="cuota" data-width="4%" data-header-align="center">Cuota</th>
					<th data-column-id="importe" data-width="6%" data-type="number" data-header-align="center">importe</th>
					<th data-column-id="procesado" data-align="center" data-header-align="center" data-formatter="image_procesado"
						data-width="5%">Procesado</th>
					<th data-column-id="sinDeudaRegistrada" data-align="center" data-header-align="center" data-formatter="image_sindeuda" data-width="5%">Sin
						Deuda</th>
				</tr>
			</thead>

		</table>
	</div>
</div>
<div class="row"></div>
<script>
	$(document)
			.ready(
					function() {
						$("#filePagoInfo-volver-btn").show();
						
						$("#detalle-pagos-grid-data")
								.bootgrid(
										{
											ajax : true,
											navigation : 0,
											rowCount : 20,
											rowSelect : false,
											sorting : false,											
											selection : false,
											multiSelect : false,
											search : false,
											columnSelection : false,
											templates : {
												search : ""
											},
											labels : {
												noResults : "No se encontraron detalles de pagos.",
												loading : "Cargando información...",
												infos : "Mostrando {{ctx.start}} a {{ctx.end}} de {{ctx.total}} filas"
											},

											formatters : {
												cuenta : function(column, row) {
													return row.cuenta + "/"
															+ row.dv;
												},
												image_procesado : function(column, row) {													
													return fmt_ok_image(row.procesado, 'fa fa-check-square-o', 'fa fa-window-close-o','Procesado', 'Error');
												},
												image_sindeuda : function(column, row) {                                                  
                                                    return fmt_ok_image(row.sinDeudaRegistrada, 'fa fa-exclamation-triangle', '','Sin deuda registrada', '');
                                                }
											},
											converters : {
												date : {
													from : function(value) {
														return moment(value);
													},
													to : function(value) {
														if (!value) {
															return "";
														}
														return moment(value)
																.format(
																		"DD-MM-YYYY");
													}
												}
											},
											url : "pagos/detallePagos_getData.action"
										});

						$('[data-toggle="tooltip"]').tooltip();
					});

	
</script>