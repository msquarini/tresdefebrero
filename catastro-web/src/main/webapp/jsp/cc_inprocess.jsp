<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<script type="text/javascript">	
</script>

<style type="text/css">
</style>

<ul class="nav nav-tabs">
	<li class="active"><a data-toggle="tab" href="#cuenta-origen">Cuenta</a></li>
	<li><a data-toggle="tab" href="#titulares">Titulares</a></li>
	<li><a data-toggle="tab" href="#nuevas">Cuenta Corriente</a></li>
<%-- 	<li class="pull-right">
		<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
			<sj:submit cssClass="btn btn-danger" id="btn"
				value="Procesar" formIds="subdivision-form" targets="container"
				onBeforeTopics="validate-subdivision-form"></sj:submit>
		</div>
	</li> --%>
</ul>

<div class="tab-content">
	<div id="cuenta" class="tab-pane fade in active">
		<s:include value="cuenta-info.jsp"></s:include>
	</div>
	<div id="titulares" class="tab-pane fade">
		<s:include value="table/titulares-table.jsp"></s:include>
	</div>
	<div id="cc" class="tab-pane fade">
		<s:include value="grid/cc-grid.jsp"></s:include>
	</div>
</div>

