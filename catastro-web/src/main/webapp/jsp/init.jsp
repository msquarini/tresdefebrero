<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<html lang="es">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="keywords" content="Tres de febrero, Catastro WEB" />
<meta name="description"
	content="Municipalidad de Tres de Febrero - Catastro WEB" />
<title>Municipalidad de Tres de Febrero</title>

<!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
<!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
<%-- <script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
 --%>

<sj:head locale="es" compressed="true" loadAtOnce="true" jqueryui="true"
	loadFromGoogle="false" />

<sb:head includeScripts="true" includeScriptsValidation="true"
	compressed="false" includeStyles="true" />



<script
	src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.bundle.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.js"></script>

<script src="js/catastro.js"></script>
<link rel="stylesheet" href="css/catastro.css">
<link rel="stylesheet" href="css/callout.css">

<script src="js/datepicker/bootstrap-datepicker.js"></script>
<script src="js/datepicker/bootstrap-datepicker.es.min.js"></script>
<link rel="stylesheet" href="css/datepicker/bootstrap-datepicker.css">

<%-- <script src="js/cleave.js"></script> --%>

<script src="js/number/jquery.number.min.js"></script>
<script src="js/bootstrap-checkbox.min.js"></script>

<script src="js/select/bootstrap-select.min.js"></script>
<link rel="stylesheet" href="css/select/bootstrap-select.css">

<script src="js/moment.min.js"></script>


<!-- Link a Font Awesome -->
<link rel="stylesheet" href="css/fa/css/font-awesome.min.css">

<script src="js/select/bootstrap-select.min.js"></script>
<link rel="stylesheet" href="css/select/bootstrap-select.css">

<link rel="stylesheet" href="css/bootstrapValidator.css">
<script src="js/bootstrapValidator.js"></script>

<!-- Scripts GRILLAS -->
<script src="js/grid/jquery.bootgrid.js"></script>
<link rel="stylesheet" href="css/grid/jquery.bootgrid.min.css">
<!-- Funciones de formato de columnas para grillas -->
<script src="js/grid/grid-formatters.js"></script>

<style type="text/css">
.modal-body {
	position: relative;
	overflow-y: auto;
	max-height: 600px;
	padding: 15px;
}

#number_container {
	border: 1px dotted #d0d0d0;
	padding: 15px;
	margin: 10px;
	display: none;
	background: #fafafa;
}

html {
	position: relative;
	min-height: 100%;
}

body {
	padding-top: 70px;
	/* 60px to make the container go all the way to the bottom of the topbar */
	margin-bottom: 60px;
}

.navbar {
	min-height: 60px;
	height: 70px;
}

.navbar-brand {
	min-height: 70px;
	padding-top: 0;
}

.navbar-collapse.collapse {
	padding-top: 10px;
	padding-right: 10px;
}

.navbar-form .form-control { /* para inputs dentro de navbar */
	/* margin-left: 50px; */
	width: 100%;
}

/* Color sobre la barra de menu al pasar el mouse */
/* .navbar-default .navbar-nav > li > a:hover, .navbar-default .navbar-nav > li > a:focus {
    background-color: #FFFF00;
    color: #FFC0CB;
} */
.top-buffer {
	margin-top: 10px;
}

.footer {
	position: absolute;
	bottom: 0;
	width: 100%;
	/* Set the fixed height of the footer here */
	height: 40px;
	background-color: #f5f5f5;
}

.footer>.container {
	padding-right: 15px;
	padding-left: 15px;
}

.mano {
	cursor: pointer;
}

/* .breadcrumb {
	margin: 0 7px;
	background-color: #FAFAFA;
	border-radius: 0;
	-moz-border-radius: 0;
	-webkit-border-radius: 0;
	-o-border-radius: 0;
	color: #666;
}
 */
/* @media ( min-width : 768px) {
	.breadcrumb {
		float: left;
		margin: 7px 10px;
		background-color: #FAFAFA;
		border-radius: 0;
		-moz-border-radius: 0;
		-webkit-border-radius: 0;
		-o-border-radius: 0;
		color: #666;
	}
} */
</style>
<script type="text/javascript">
	function closeErrorDialog() {
		//nothing
	};

	// HANDLER GLOBAL DE ERROR AJAX!!!
	$(document).ajaxError(function(event, request, settings) {
		debugger;
		handleErrorMessages(request.responseJSON);
		showErrorModalDialog(false);
	});

	function call(action, container, parameters) {
		var url = action + ".action";
		if (parameters) {
			url = url + "?" + parameters;
		}
		$.ajax({
			method : "POST",
			url : url,
			data : {},
			// global: false,
			traditional : true,
			success : function(res) {
				$(container).html(res);
			},
			error : function(res, textStatus, jqXHR) {
				//handleErrorMessages(res.responseJSON);
				//$('#error-dialog').modal('show');
			}
		});
	}

	$(document).bind("eventType", function() {
		alert(' I 3 Fanny');
	});

	$.subscribe('errorjson', function(event, data) {
		debugger;
		alert(event.originalEvent.request.success);
		alert(event.originalEvent.request.responseText);
		//var d = JSON.parse(event.originalEvent.request.responseText);
		//console.log(d);
		//data.id = 'error';
		//data.nextElementSibling = $('#error');
		for (x in data) {
			console.log(x + ":" + data[x]);
		}

		// Opcion 1 con HTML
		//$('#error').html(event.originalEvent.request.responseText);

		// Opcion 2 con JSON
		//var resp = $.parseJSON(event.originalEvent.request.responseText);
		//$('#error').text(resp.errorMessage);
	});

	$(function() {
		$("#quickSearch")
				.autocomplete(
						{
							//source: "/catastro-web/catastro/consulta_autocomplete.action",
							selectFirst : true,
							minLength : 4,
							source : function(request, response) {
								$
										.ajax({
											url : "/catastro-web/catastro/consulta_autocomplete.action",
											dataType : "json",
											data : {
												cuenta : request.term
											},
											success : function(data) {
												response($
														.map(
																data.cuentas,
																function(item) {
																	return {
																		id : item.id,
																		label : item.cuenta,
																		dv : item.dv
																	};
																}));
											}
										});
							},
							select : function(event, ui) {
								//No usar el ID ya que en la carga directa no se seleccionaria
								//alert(ui.item.id);
								this.value = ui.item.value;
								event.preventDefault();
							}
						});
		$("#quickSearch").keypress(function(event) {
			var keycode = (event.keyCode ? event.keyCode : event.which);
			if (keycode == "13") {

				// Busqueda rapida
				loader(true, null);
				$("#cuenta").val($("#quickSearch").val());
				$('#quicksearch-form').submit();
			}
		});
	});

	$('#quicksearch-form').ajaxForm({
		target : '#container',
		success : function(res, textStatus, jqXHR) {
			loader(false, null);
		},
		error : function(res, textStatus, jqXHR) {
			loader(false, null);
			console.log(res);
			alert("error");
		}
	});
</script>
</head>

<body>
	<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
		<!-- <div class="container-fluid"> -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target="#menuNavBar">
				<span class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>

			<a class="navbar-brand" href="http://www.tresdefebrero.gov.ar"> <span><img
					src="img/logo-new.png" alt="" height="75" width="180"></span>Catastro
				WEB
			</a>
		</div>

		<!-- QuickSearch -->
		<s:form id="quicksearch-form" action="consulta_genericSearch"
			namespace="/catastro">
			<input type="hidden" name="cuenta" id="cuenta" />
		</s:form>

		<!-- Desdoblamiento -->
		<%-- <s:url var="desdoblamientoURL" action="desdoblamiento_input"
			namespace="/catastro" /> --%>

		<div class="collapse navbar-collapse" id="menuNavBar">

			<div class="col-sm-3 col-md-3">
				<!-- Search form -->
				<!-- <form class="navbar-form navbar-right" role="search"> -->
				<!-- <form class="navbar-form" role="search"> -->
				<div class="navbar-form search-box">
					<%-- 	<input class="form-control autocomplete" placeholder="Enter A" /><span
							class="input-group-btn">
							<button type="submit" class="btn btn-default">
								<span class="glyphicon glyphicon-search"></span>
							</button>
						</span> --%>
					<input id="searchCuentaId" type="hidden" name="cuentaId" /> <input
						id="quickSearch" type="text" placeholder="Número de cuenta"
						class="form-control autocomplete" /> <i
						class="glyphicon glyphicon-search"></i>
				</div>
				<!-- </form> -->
			</div>
			<ul class="nav navbar-nav navbar-right">
				<li><a id="anchor_emision"
					onclick="call('catastro/periodo_input','#container');">Emision</a></li>

				<li><a id="anchor_cc"
					onclick="call('catastro/cuentacorriente_input','#container');">Cuenta
						Corriente</a></li>

				<li class="dropdown"><a class="dropdown-toggle"
					data-toggle="dropdown" href="#">Trámites<span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a id="anchor_gestion"
							onclick="call('catastro/tramite_input','#container');">Gestion</a></li>
						<li role="separator" class="divider"></li>
						<li><a id="anchor_sub"
							onclick="call('catastro/subdivision_input','#container');">Subdivisión</a></li>
						<li><a id="anchor_rat"
							onclick="call('catastro/ratificacion_input','#container');">Ratificación</a></li>
						<li><a id="anchor_uni"
							onclick="call('catastro/unificacion_input','#container');">Unificación</a></li>
						<li><a id="anchor_ins"
							onclick="call('catastro/inscripcion_input','#container');">Primera
								Inscripción</a></li>
						<%-- <li><sj:a href="%{desdoblamientoURL}" targets="container">Desdoblamiento</sj:a></li> --%>
						<li><a id="anchor_otros"
							onclick="call('catastro/titularABM_input','#container');">Otros</a></li>
					</ul></li>

				<li class="dropdown"><a class="dropdown-toggle"
					data-toggle="dropdown" href="#">Pagos <span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a id="anchor_otros"
							onclick="call('pagos/pagos_input','#container');">Subir
								Archivos</a></li>
						<li><a id="anchor_otros"
							onclick="call('pagos/pagos_input','#container', 'tab=2');">Procesar</a></li>
					</ul></li>


				<li class="dropdown"><a class="dropdown-toggle"
					data-toggle="dropdown" href="#"><span
						class="glyphicon glyphicon-user"></span> <s:property
							value="username" /></a>
					<ul class="dropdown-menu">

						<li><a id="anchor_codadm"
							onclick="call('commons/codAdm_input','#container');">Cod.
								Administrativos</a></li>
						<li><a id="anchor_config"
							onclick="call('commons/config_input','#container');">Configuracion</a></li>

						<li><a href="sso/logout"><span
								class="glyphicon glyphicon-log-in"></span> Logout</a></li>
					</ul></li>
			</ul>

		</div>

	</nav>
	<!-- Indicador de loading... -->
	<div id="loader" style="display: none;"></div>

	<div class="container top-buffer" id="container">

		<div class="chart-container"
			style="position: relative; height: 20vh; width: 40vw">
			<canvas id="chart"></canvas>
		</div>

	</div>

	<footer class="footer">
		<div class="container">
			<p align="center">
				© Copyright 2017 - 2018 - <strong>MUNICIPALIDAD DE TRES DE
					FEBRERO</strong> - Conmutador: 4734-2400 - Todos los derechos reservados
			</p>
		</div>
	</footer>

	<s:include value="dialog/generic-dialog.jsp"></s:include>
</body>

</html>