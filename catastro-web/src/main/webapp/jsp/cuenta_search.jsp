<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<script type="text/javascript">
	$('input.number').number(true, 0, '', '');
	$('input.numberdecimal').number(true, 2, ',', '');

	$('.selectpicker').selectpicker({
		width : '100%'
	});

	$('#div-search-cuenta').show();
	$('#div-search-nomenclatura').hide();

	$('#searchBy').change(function() {
		if (this.selectedIndex < 1) {
			$('#div-search-cuenta').show();
			$('#div-search-nomenclatura').hide();
		} else {
			$('#div-search-cuenta').hide();
			$('#div-search-nomenclatura').show();
		}
	});

	$('#cuenta-search-form').ajaxForm({
		target : '#result_search',
		success : function(res, textStatus, jqXHR) {
			/* 			  document.getElementById("loader").style.display = "none";			
			 $('#load').button('reset');
			 $("html,body").css("cursor", "default");
			 */
			loader(false, $('#search_btn'));
		},
		error : function(res, textStatus, jqXHR) {
			loader(false, $('#search_btn'));
			alert(textStatus);
			alert(res);
			console.log(res);
			alert("error");
		}
	});

	/* Limpiar form  */
	function cleanForm() {
		$('#cuenta-search-form').find("input[type=text], textarea").val('');
		$('#cuenta-search-form').data('bootstrapValidator').resetForm();
	};

	/* Validar form antes del envio!! */
	function search() {
		$('#cuenta-search-form').data('bootstrapValidator').resetForm();
		$('#cuenta-search-form').bootstrapValidator('validate');

		if ($('#cuenta-search-form').data('bootstrapValidator').isValid()) {
			loader(true, $('#search_btn'));
			$('#cuenta-search-form').submit();
		}
	};

	$('#cuenta-search-form').bootstrapValidator({
		framework : 'bootstrap',
		feedbackIcons : {
			//valid : 'glyphicon glyphicon-ok',
			invalid : 'glyphicon glyphicon-remove',
			validating : 'glyphicon glyphicon-refresh'
		},
		message : 'Datos inválidos',
		fields : {
			'cuenta' : {
				validators : {
					notEmpty : {
						message : 'Obligatorio'
					},
					integer : {
						message : 'Debe ser un valor numérico'
					}
				}
			},
/* 			'dv' : {
				validators : {
					notEmpty : {
						message : 'Obligatorio'
					}
				}
			}, */
			'nomenclatura.circunscripcion' : {
				validators : {
					notEmpty : {
						message : 'Obligatorio'
					},
					integer : {
						message : 'Debe ser un valor numérico'
					},
					greaterThan : {
						message : 'Dato inválido',
						value : 1
					}
				}
			},
			'nomenclatura.seccion' : {
				validators : {
					notEmpty : {
						message : 'Obligatorio'
					}
				}
			}
		}
	});
</script>
<style type="text/css">
.has-feedback .form-control {
	top: 0;
	/* Para utilizar todo el area editable */
	padding-right: 10px;
}

.form-control-feedback {
	/* Para correr el icon a la derecha */
	right: -22px;
}
</style>

<s:form id="cuenta-search-form" action="consulta_search"
	cssClass="form-vertical" namespace="/catastro" theme="bootstrap">

	<fieldset>
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<s:actionerror />
				<s:actionmessage />
			</div>
		</div>

		<div class="panel panel-default">
			<div class="panel-heading">
				<s:text name="search.cuenta.label" />
				<span class="glyphicon glyphicon-search"></span>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-xs-6 col-sm-6 col-md-3 col-lg-6">
						<div class="input-group">
							<select class="selectpicker" id="searchBy" name="searchBy">
								<option value="1"><s:text name="cuenta.label" />/
									<s:text name="dv.label" /></option>
								<option value="2"><s:text name="nomenclatura.label" /></option>
							</select> <span class="input-group-btn">
								<button id="search_btn" class="btn btn-default" type="button"
									onclick="search();"
									data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Buscando">
									<span class="glyphicon glyphicon-search"></span>
									<s:text name="search.label"></s:text>
								</button>

							</span> <span class="input-group-btn">
								<button class="btn btn-default" type="button"
									onclick="cleanForm();">
									<span class="glyphicon glyphicon-erase"></span>
									<s:text name="reset.label"></s:text>
								</button>

							</span>
						</div>
					</div>
				</div>

				<div id="div-search-cuenta" class="row top-buffer toggle">
					<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
						<label for="cuenta"><s:text name="cuenta.label" /></label>
						<s:textfield id="cuenta" class="form-control number" name="cuenta"
							maxlength="6" />
					</div>
					<div class="col-xs-6 col-sm-6 col-md-1 col-lg-1">
						<label for="dv"><s:text name="dv.label" /></label>
						<s:textfield id="dv" class="form-control" name="dv" maxlength="1" />
					</div>
				</div>


				<div id="div-search-nomenclatura" class="row top-buffer toggle">
					<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
						<label for="circunscripcion"><s:text
								name="circ.short.label" /></label>
						<%-- <s:textfield id="circunscripcion"
							class="form-control input-sm number"
							name="nomenclatura.circunscripcion" maxlength="3" /> --%>

						<s:url var="circ" action="consulta_cargaCircunscripciones"
							namespace="/catastro" />

						<sj:select id="circunscripcion" href="%{circ}" resizable="false"
							class="form-control" draggable="false" droppable="false"
							selectable="false" sortable="false"
							name="nomenclatura.circunscripcion" list="circunscripciones"
							listKey="circunscripcion" listValue="circunscripcion"
							onChangeTopics="recargaSecciones" headerKey="-1"
							headerValue="Seleccione" />
					</div>
					<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
						<label for="seccion"><s:text name="sec.short.label" /></label>
						<%-- <s:textfield id="seccion" class="form-control input-sm"
							name="nomenclatura.seccion" maxlength="3" /> --%>
						<sj:select id="seccion" href="%{circ}" resizable="false"
							class="form-control" draggable="false" droppable="false"
							selectable="false" sortable="false" name="nomenclatura.seccion"
							list="secciones" listKey="seccion" listValue="seccion"
							reloadTopics="recargaSecciones" />
					</div>
					<div class="col-xs-6 col-sm-6 col-md-1 col-lg-1">
						<label for="fraccion"><s:text name="fr.short.label" /></label>
						<s:textfield id="fraccion" class="form-control number"
							name="nomenclatura.fraccion" maxlength="3" />
					</div>
					<div class="col-xs-6 col-sm-6 col-md-1 col-lg-1">
						<label for="fraccionLetra"><s:text name="frl.short.label" /></label>
						<s:textfield id="fraccionLetra" class="form-control "
							name="nomenclatura.fraccionLetra" maxlength="3" />
					</div>
					<div class="col-xs-6 col-sm-6 col-md-1 col-lg-1">
						<label for="manzana"><s:text name="mz.short.label" /></label>
						<s:textfield id="manzana" class="form-control  number"
							name="nomenclatura.manzana" maxlength="3" />
					</div>
					<div class="col-xs-6 col-sm-6 col-md-1 col-lg-1">
						<label for="manzanaLetra"><s:text name="mzl.short.label" /></label>
						<s:textfield id="manzanaLetra" class="form-control"
							name="nomenclatura.manzanaLetra" maxlength="3" />
					</div>
					<div class="col-xs-6 col-sm-6 col-md-1 col-lg-1">
						<label for="parcela"><s:text name="pc.short.label" /></label>
						<s:textfield id="parcela" class="form-control number"
							name="nomenclatura.parcela" maxlength="3" />
					</div>
					<div class="col-xs-6 col-sm-6 col-md-1 col-lg-1">
						<label for="parcelaLetra"><s:text name="pcl.short.label" /></label>
						<s:textfield id="parcelaLetra" class="form-control "
							name="nomenclatura.parcelaLetra" maxlength="3" />
					</div>
					<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
						<label for="unidadFuncional"><s:text name="uf.short.label" /></label>
						<s:textfield id="unidadFuncional" class="form-control "
							name="nomenclatura.unidadFuncional" maxlength="6" />
					</div>
				</div>


			</div>

		</div>

	</fieldset>
</s:form>
<div id="result_search"></div>
