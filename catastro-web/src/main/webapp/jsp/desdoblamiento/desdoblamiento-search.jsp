<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<script type="text/javascript">	
	$(document).off("cuentaSelectedEvent").on("cuentaSelectedEvent", function(event, data) {		
		$("#idCuentaOrigen").val(data.cuenta.id);
		$("#desdoblamiento_btn").toggleClass('disabled', false);
	}).off("cuentaDeselectedEvent").on("cuentaDeselectedEvent", function(event, data) {
		$("#idCuentaOrigen").val("-1");		
		$("#desdoblamiento_btn").toggleClass('disabled', true);
	});
</script>
<style type="text/css">
</style>


<s:include value="../cuenta_search.jsp"></s:include>

<div class="row">
	<s:form id="desdoblamiento-init-form" action="desdoblamiento_init"
		namespace="/catastro" theme="bootstrap">
		<s:hidden id="idCuentaOrigen" name="idCuentaOrigen"></s:hidden>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<sj:a id="desdoblamiento_btn" cssClass="btn btn-primary disabled"
				value="Desdoblamiento" formIds="desdoblamiento-init-form"
				targets="container" >Desdoblamiento</sj:a>
		</div>
	</s:form>
</div>