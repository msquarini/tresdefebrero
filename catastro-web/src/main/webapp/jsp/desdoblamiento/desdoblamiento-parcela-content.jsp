<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<script type="text/javascript">
	$('#destinoMunicipal').checkboxpicker({
		offLabel : 'No',
		onLabel : 'Si'
	}).on(
			'change',
			function() {
				$('#destino').prop('disabled',
						!$('#destinoMunicipal').prop('checked'));

				if ($(this).is(':checked')) {
					$(this).prop('value', 'true');
				} else {
					$(this).prop('value', 'false');
				}
			});

	/* Submit del formulario previa validacion */
	function dialog_submit() {
		$('#desdoblamiento-parcela-form').bootstrapValidator('validate');

		if ($('#desdoblamiento-parcela-form').data('bootstrapValidator')
				.isValid()) {
			$('#desdoblamiento-parcela-form').submit();
		}
	};

	/* Configuracion del formulario, ya que se hace submit del mismo mediante javascript */
	$('#desdoblamiento-parcela-form').ajaxForm({
		target : '#generic-modal-body',
		success : function(res, textStatus, jqXHR) {
			//$('#info1').fadeIn('slow');
			alert(textStatus);
			alert(res);
		},
		error : function(res, textStatus, jqXHR) {
		}
	});

	/* Configuracion de validador del formulario */
	$(document)
			.ready(
					function() {
						if ($('#destinoMunicipal').prop('value') == 'true') {
							$('#destinoMunicipal').prop('checked', 'true');
						}

						$('input.number').number(true, 0, '', '');
						$('input.numberdecimal').number(true, 2, ',', '');

						// Set up the number formatting.
						//$('#nuevaParcela.nomenclatura.circunscripcion').number(
						//		true, 0, '', '');

						$('#desdoblamiento-parcela-form')
								.bootstrapValidator(
										{
											framework : 'bootstrap',
											excluded : [ ':disabled', ':hidden' ],
											feedbackIcons : {
												//valid : 'glyphicon glyphicon-ok',
												invalid : 'glyphicon glyphicon-remove',
												validating : 'glyphicon glyphicon-refresh'
											},
											message : 'Datos invalidos',
											/* submitHandler: function(validator, form, submitButton) {
													
											}, */
											fields : {
												'origen.numero' : {
													validators : {
														notEmpty : {
															message : 'Dato Obligatorio'
														},
														integer : {
															message : 'El valor ingresado debe ser num�rico'
														}
													}
												},
												'origen.anio' : {
													validators : {
														notEmpty : {
															message : 'Dato Obligatorio'
														},
														greaterThan : {
															value : 1900,
															message : 'El valor ingresado debe ser mayor que 1900'
														}
													}
												},
												'nuevaParcela.nomenclatura.circunscripcion' : {
													validators : {
														notEmpty : {
															message : 'Obligatorio'
														}
													}
												},
												'nuevaParcela.nomenclatura.seccion' : {
													validators : {
														notEmpty : {
															message : 'Obligatorio'
														}
													}
												},
												'nuevaParcela.nomenclatura.parcela' : {
													validators : {
														notEmpty : {
															message : 'Obligatorio'
														}
													}
												},
												'nuevaParcela.partida' : {
													validators : {
														notEmpty : {
															message : 'Partida Obligatorio'
														},
														integer : {
															message : 'Debe ser un valor n�merico de 6 d�gitos'
														}
													}
												},
												'nuevaParcela.dominio.tipo' : {
													validators : {
														notEmpty : {
															message : 'Obligatorio'
														}
													}
												},
												'nuevaParcela.dominio.numero' : {
													validators : {
														notEmpty : {
															message : 'Obligatorio'
														}
													}
												},
												'nuevaParcela.lineaVigente.superficieTerreno' : {
													validators : {
														notEmpty : {
															message : 'Obligatorio'
														}
													}
												},
												'nuevaParcela.lineaVigente.superficieCubierta' : {
													validators : {
														notEmpty : {
															message : 'Obligatorio'
														}
													}
												},
												'nuevaParcela.lineaVigente.superficieSemi' : {
													validators : {
														notEmpty : {
															message : 'Obligatorio'
														}
													}
												},
												'nuevaParcela.lineaVigente.valorTierra' : {
													validators : {
														notEmpty : {
															message : 'Obligatorio'
														}
													}
												},
												'nuevaParcela.lineaVigente.valorComun' : {
													validators : {
														notEmpty : {
															message : 'Obligatorio'
														}
													}
												},
												'nuevaParcela.lineaVigente.valorEdificio' : {
													validators : {
														notEmpty : {
															message : 'Obligatorio'
														}
													}
												},
												'nuevaParcela.domicilioPostal.calle' : {
													validators : {
														notEmpty : {
															message : 'Obligatorio'
														}
													}
												},
												'nuevaParcela.domicilioInmueble.calle' : {
													validators : {
														notEmpty : {
															message : 'Obligatorio'
														}
													}
												},
												'nuevaParcela.domicilioPostal.numero' : {
													validators : {
														notEmpty : {
															message : 'Obligatorio'
														}
													}
												},
												'nuevaParcela.domicilioInmueble.numero' : {
													validators : {
														notEmpty : {
															message : 'Obligatorio'
														}
													}
												},
												'nuevaParcela.destino' : {
													validators : {
														notEmpty : {
															message : 'Obligatorio'
														}
													}
												}
											}
										});
					});
</script>
<style type="text/css">
.has-feedback .form-control {
	top: 0;
	/* Para utilizar todo el area editable */
	padding-right: 10px;
}

.form-control-feedback {
	/* Para correr el icon a la derecha */
	right: -22px;
}
</style>
<s:form id="desdoblamiento-parcela-form" action="desdoblamiento_process"
	cssClass="form-vertical" namespace="/catastro" theme="bootstrap">

	<fieldset>

		<div class="row  ">
			<fieldset disabled>
				<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
				<label for="circunscripcion"><s:text name="circ.short.label"/></label>
					<s:textfield id="circunscripcion"
						class="form-control input-sm number"
						name="nuevaParcela.nomenclatura.circunscripcion" maxlength="3" />
				</div>
				<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
				<label for="seccion"><s:text name="sec.short.label"/></label>
					<s:textfield id="seccion" class="form-control input-sm"
						maxlength="3" name="nuevaParcela.nomenclatura.seccion" />
				</div>
				<div class="col-xs-6 col-sm-6 col-md-1 col-lg-1">
				<label for="fraccion"><s:text name="fr.short.label"/></label>
					<s:textfield id="fraccion" class="form-control input-sm number"
						maxlength="3" name="nuevaParcela.nomenclatura.fraccion" />
				</div>
				<div class="col-xs-6 col-sm-6 col-md-1 col-lg-1">
				<label for="fraccionLetra"><s:text name="frl.short.label"/></label>
					<s:textfield id="fraccionLetra" class="form-control input-sm"
						maxlength="3" name="nuevaParcela.nomenclatura.fraccionLetra" />
				</div>
				<div class="col-xs-6 col-sm-6 col-md-1 col-lg-1">
				<label for="manzana"><s:text name="mz.short.label"/></label>
					<s:textfield id="manzana" class="form-control input-sm number"
						maxlength="3" name="nuevaParcela.nomenclatura.manzana" />
				</div>
				<div class="col-xs-6 col-sm-6 col-md-1 col-lg-1">
				<label for="manzanaLetra"><s:text name="mzl.short.label"/></label>
					<s:textfield id="manzanaLetra" class="form-control input-sm"
						maxlength="3" name="nuevaParcela.nomenclatura.manzanaLetra" />
				</div>
				<div class="col-xs-6 col-sm-6 col-md-1 col-lg-1">
				<label for="parcela"><s:text name="pc.short.label"/></label>
					<s:textfield id="parcela" class="form-control input-sm number"
						maxlength="3" name="nuevaParcela.nomenclatura.parcela" />
				</div>
				<div class="col-xs-6 col-sm-6 col-md-1 col-lg-1">
				<label for="parcelaLetra"><s:text name="pcl.short.label"/></label>
					<s:textfield id="parcelaLetra" class="form-control input-sm"
						maxlength="3" name="nuevaParcela.nomenclatura.parcelaLetra" />
				</div>
				<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
				<label for="unidadFuncional"><s:text name="uf.short.label"/></label>
					<s:textfield id="unidadFuncional"
						class="form-control input-sm number" maxlength="6"
						name="nuevaParcela.nomenclatura.unidadFuncional" />
				</div>
			</fieldset>
		</div>

		<!-- Partida | Zona | Categoria -->
		<div class="row">

			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">

				<div class="panel panel-default">
					<div class="panel-heading"><s:text name="dominio.panel.label"/></div>
					<div class="panel-body">

						<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
							<label for="inscripcion"><s:text name="dominio.tipo.label"/></label> <select id="inscripcion"
								name="nuevaParcela.dominio.tipo" class="form-control" onchange="cambioTipoInscripcion()">
								<option value="F"><s:text name="folio.label"/></option>
								<option value="M"><s:text name="matricula.label"/></option>
							</select>
							<%-- <s:textfield id="inscripcion" name="nuevaParcela.dominio.tipo" /> --%>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
							<label for="numero"><s:text name="dominio.numero.label"/></label>
							<s:textfield id="numero" name="nuevaParcela.dominio.numero"
								class="form-control number" maxlength="6" />
						</div>
						<div id="div_anio" class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
							<label for="anio"><s:text name="dominio.anio.label"/></label>
							<s:textfield id="anio" name="nuevaParcela.dominio.anio"
								class="form-control number" maxlength="4" />
						</div>
					</div>
				</div>
			</div>

			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">

				<div class="panel panel-default">
					<div class="panel-heading"><s:text name="parcela.panel.label"/></div>
					<div class="panel-body">

						<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
							<label for="partida"><s:text name="partida.label"/></label>
							<s:textfield id="partida" class="form-control number"
								maxlength="6" name="nuevaParcela.partida" />
						</div>
						<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
							<label for="categoria"><s:text name="categoria.label"/></label>
							<select id="categoria" name="nuevaParcela.lineaVigente.categoria"
								class="form-control">
								<option value="8">8</option>
								<option value="1">1</option>
								<option value="7">7</option>
								<option value="2">2</option>
								<option value="4">4</option>
								<option value="6">6</option>
								<option value="5">5</option>
								<option value="3">3</option>
							</select>
						</div>

						<div class="col-xs-6 col-sm-6 col-md-5 col-lg-5">
							<label for="tf_vigencia"><s:text name="vigencia.label"/></label>
							<div class="input-group date" data-provide="datepicker"
								data-date-format="dd/mm/yyyy" data-date-autoclose="true"
								data-date-orientation="bottom" data-date-language="es">
								<input id="tf_vigencia" type="text" class="form-control"
									value="12/02/2012">
								<div class="input-group-addon">
									<span class="glyphicon glyphicon-th"></span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>

		<!--  Superificies -->
		<div class="row">
			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">

				<div class="panel panel-default">
					<div class="panel-heading"><s:text name="superficies.panel.label"/></div>
					<div class="panel-body">
						<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
							<label for="sup_terreno"><s:text name="terreno.label"/></label>
							<s:textfield id="sup_terreno"
								name="nuevaParcela.lineaVigente.superficieTerreno"
								class="form-control numberdecimal" maxlength="8" />
						</div>
						<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
							<label for="sup_cubierta"><s:text name="cubierta.label"/></label>
							<s:textfield id="sup_cubierta" class="form-control numberdecimal"
								maxlength="8"
								name="nuevaParcela.lineaVigente.superficieCubierta" />
						</div>
						<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
							<label for="sup_semi"><s:text name="semi.label"/></label>
							<s:textfield id="sup_semi" class="form-control numberdecimal"
								maxlength="8" name="nuevaParcela.lineaVigente.superficieSemi" />
						</div>
					</div>
				</div>
			</div>
			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
				<!-- Valores -->
				<div class="panel panel-default">
					<div class="panel-heading"><s:text name="valores.panel.label"/></div>
					<div class="panel-body">
						<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
							<label for="valor_tierra"><s:text name="tierra.label"/></label>
							<s:textfield id="valor_tierra" class="form-control numberdecimal"
								name="nuevaParcela.lineaVigente.valorTierra" />
						</div>
						<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
							<label for="valor_comun"><s:text name="comun.label"/></label>
							<s:textfield id="valor_comun" class="form-control numberdecimal"
								name="nuevaParcela.lineaVigente.valorComun" />
						</div>
						<div class="col-xs-6 col-sm-6 col-md-4 col-lg-42">
							<label for="valor_edificio"><s:text name="edificio.label"/></label>
							<s:textfield id="valor_edificio"
								class="form-control numberdecimal"
								name="nuevaParcela.lineaVigente.valorEdificio" />
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Domicilio Inmueble -->
		<div class="panel-group">
			<div class="panel panel-default">
				<div class="panel-heading mano" data-toggle="collapse"
					href="#panel_domicilio_inmueble"><s:text name="domicilio.inmueble.panel.label"/></div>
				<div class="panel-body panel-collapse collapse"
					id="panel_domicilio_inmueble">
					<div class="col-xs-6 col-sm-6 col-md-5 col-lg-4">
						<label for="di_calle"><s:text name="domicilio.calle.label"/></label>
						<s:textfield id="di_calle" class="form-control input-sm"
							name="nuevaParcela.domicilioInmueble.calle" />
					</div>
					<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
						<label for="di_numero"><s:text name="domicilio.nro.label"/></label>
						<s:textfield id="di_numero" class="form-control input-sm"
							name="nuevaParcela.domicilioInmueble.numero" />
					</div>
					<div class="col-xs-6 col-sm-6 col-md-1 col-lg-1">
						<label for="di_piso"><s:text name="domicilio.piso.label"/></label>
						<s:textfield id="di_piso" class="form-control input-sm"
							name="nuevaParcela.domicilioInmueble.piso" />
					</div>
					<div class="col-xs-6 col-sm-6 col-md-1 col-lg-1">
						<label for="di_depto"><s:text name="domicilio.depto.label"/></label>
						<s:textfield id="di_depto" class="form-control input-sm"
							name="nuevaParcela.domicilioInmueble.departamento" />
					</div>
					<div class="col-xs-6 col-sm-6 col-md-1 col-lg-2">
						<label for="di_cp"><s:text name="domicilio.cp.label"/></label>
						<s:textfield id="di_cp" class="form-control input-sm"
							name="nuevaParcela.domicilioInmueble.codigoPostal" />
					</div>
					<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
						<label for="di_barrio"><s:text name="domicilio.barrio.label"/></label>
						<s:select id="di_barrio" list="#session.barrios" listKey="id"
							listValue="nombre" class="form-control input-sm"
							name="nuevaParcela.domicilioInmueble.barrio.id"></s:select>
					</div>
				</div>
			</div>


			<!-- Domicilio Inmueble -->

			<div class="panel panel-default">
				<div class="panel-heading mano" data-toggle="collapse"
					href="#panel_domicilio_postal"><s:text name="domicilio.postal.panel.label"/></div>
				<div class="panel-body panel-collapse collapse"
					id="panel_domicilio_postal">
					<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
						<label for="dp_calle"><s:text name="domicilio.calle.label"/></label>
						<s:textfield id="dp_calle"
							name="nuevaParcela.domicilioPostal.calle" />
					</div>
					<div class="col-xs-6 col-sm-6 col-md-1 col-lg-2">
						<label for="dp_numero"><s:text name="domicilio.nro.label"/></label>
						<s:textfield id="dp_numero"
							name="nuevaParcela.domicilioPostal.numero" />
					</div>
					<div class="col-xs-6 col-sm-6 col-md-1 col-lg-1">
						<label for="dp_piso"><s:text name="domicilio.piso.label"/></label>
						<s:textfield id="dp_piso" name="nuevaParcela.domicilioPostal.piso" />
					</div>
					<div class="col-xs-6 col-sm-6 col-md-1 col-lg-1">
						<label for="dp_depto"><s:text name="domicilio.depto.label"/></label>
						<s:textfield id="dp_depto"
							name="nuevaParcela.domicilioPostal.departamento" />
					</div>
					<div class="col-xs-6 col-sm-6 col-md-1 col-lg-1">
						<label for="dp_cp"><s:text name="domicilio.cp.label"/></label>
						<s:textfield id="dp_cp"
							name="nuevaParcela.domicilioPostal.codigoPostal" />
					</div>
					<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
						<label for="dp_barrio"><s:text name="domicilio.barrio.label"/></label>
						<s:select id="dp_barrio" list="#session.barrios" listKey="id"
							listValue="nombre" name="nuevaParcela.domicilioPostal.barrio.id"></s:select>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-xs-6 col-sm-6 col-md-3 col-lg-2">
				<label for="destinoMunicipal"><s:text name="dominiomunicipal.label"/></label> <input
					id="destinoMunicipal" data-group-cls="btn-group-sm" type="checkbox"
					name="nuevaParcela.dominioMunicipal"
					value="<s:property value="nuevaParcela.dominioMunicipal" />" />
			</div>
			<div id="div_destino" class="col-xs-6 col-sm-6 col-md-10 col-lg-10">
				<label for="destino"><s:text name="destino.label"/></label>
				<s:select id="destino" list="#session.destinos" listKey="destino"
					disabled="true" listValue="destino" name="nuevaParcela.destino"
					headerKey="" headerValue="Seleccione"></s:select>
			</div>
		</div>

		<!-- Observacion -->
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<label for="observacion"><s:text name="observacion.label"/></label>
				<s:textfield id="observacion" name="nuevaParcela.observacion" />
			</div>
		</div>
	</fieldset>

</s:form>

