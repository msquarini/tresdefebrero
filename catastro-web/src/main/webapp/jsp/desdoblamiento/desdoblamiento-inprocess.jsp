<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<!--  DESDOBLE -->

<script type="text/javascript">
	$.subscribe('complete', function(event, data) {
		showResponseModalDialog(true, event.originalEvent.request.responseText);
	});

	/* Boton de procesamiento de unificacion, antes del envio del formulario, se valida el mismo */
	$('#btnProcesarDesdoblamiento').subscribe(
			'validate-desdoblamiento-form',
			function(event) {

				$('#desdoblamiento-parcela-form')
						.bootstrapValidator('validate');
				// Valor de validar el formulario
				event.originalEvent.options.submit = $(
						'#desdoblamiento-parcela-form').data(
						'bootstrapValidator').isValid();

			});
</script>

<style type="text/css">
</style>

<ul class="nav nav-pills" style="background-color: #eeeeee; border-bottom-style: ridge;">
	<li class="nav-header disabled"><a>Desdoble</a></li>
	<li class="active"><a data-toggle="tab" href="#infoparcela">Información
			Parcela</a></li>
	<li><a data-toggle="tab" href="#titulares">Titulares</a></li>
	<li><a data-toggle="tab" href="#responsables">Responsables</a></li>
	<li class="pull-right" style="padding-top: 3px; padding-right: 3px">
		<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
			<sj:submit cssClass="btn btn-danger" id="btnProcesarDesdoblamiento"
				value="Procesar" formIds="desdoblamiento-parcela-form"
				targets="generic-modal-response-body" onSuccessTopics="complete"
				onBeforeTopics="validate-desdoblamiento-form"></sj:submit>
		</div>
	</li>
</ul>

<div class="tab-content">
	<div id="infoparcela" class="tab-pane fade in active">
		<s:include value="desdoblamiento-parcela-content.jsp"></s:include>
	</div>
	<div id="titulares" class="tab-pane fade">
		<s:include value="../grid/titular-grid.jsp"></s:include>
	</div>
	<div id="responsables" class="tab-pane fade">
		<!-- Grilla de responsables pertenecientes a la cuenta -->
		<s:include value="../info/responsables-info.jsp"></s:include>
	</div>
</div>

