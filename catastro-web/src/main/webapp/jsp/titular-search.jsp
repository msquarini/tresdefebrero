<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<!-- Panel de BUSQUEDA de titulares -->

<script type="text/javascript">
	$('input.number').number(true, 0, '', '');

	$('#titular-search-form').ajaxForm({
		//target : '#result_search',
		success : function(res, textStatus, jqXHR) {
			$('#titular-grid-data').bootgrid('reload');			
		},
		error : function(res, textStatus, jqXHR) {
            handleErrorMessages(res.responseJSON);
            showErrorModalDialog(false);
		}
	});

	/* Reset de validadores  */
	function resetForm() {
		$('#titular-search-form').data('bootstrapValidator').resetForm();
	};

	/* Validar form antes del envio!! */
	function search() {
		$('#titular-search-form').bootstrapValidator('validate');

		if ($('#titular-search-form').data('bootstrapValidator').isValid()) {
			$('#titular-search-form').submit();
		}
	};

	$('#titular-search-form')
			.bootstrapValidator(
					{
						framework : 'bootstrap',
						feedbackIcons : {
							//valid : 'glyphicon glyphicon-ok',
							invalid : 'glyphicon glyphicon-remove',
							validating : 'glyphicon glyphicon-refresh'
						},
						message : 'Datos inválidos',
						fields : {
							'searchTitular.apellido' : {
								validators : {
									stringLength : {
										min : 3,
										max : 20,
										message : 'El dato ingresado debe ser de al menos 3 caracteres'
									}
								}
							},
							'searchTitular.cuitcuil' : {
								validators : {
									stringLength : {
										min : 11,
										max : 11,
										message : 'El dato ingresado debe ser de 11 posisiones numéricas'
									},
									callback : {
										message : 'Cuit inválido',
										callback : function(value, validator,
												$field) {
											return validaCuit(value);
										}
									}
								}
							}
						}
					});
</script>
<style type="text/css">
.has-feedback .form-control {
	top: 0;
	/* Para utilizar todo el area editable */
	padding-right: 10px;
}

.form-control-feedback {
	/* Para correr el icon a la derecha */
	right: -22px;
}
</style>

<s:form id="titular-search-form" action="titular_search"
	cssClass="form-vertical" namespace="/catastro" theme="bootstrap">

	<fieldset>
		<div class="panel panel-default">
			<div class="panel-heading">
				Busqueda <span class="glyphicon glyphicon-search"></span>
			</div>
			<div class="panel-body">
				<div class="row">

					<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
						<!-- <label for="apellido">Apellido</label> -->
						<s:textfield id="apellido" class="form-control"
							placeholder="Apellido" name="searchTitular.apellido"
							maxlength="20" />
					</div>
					<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
						<!-- <label for="cuitcuil">Cuit/Cuil</label> -->
						<s:textfield id="cuitcuil" class="form-control number"
							placeholder="Cuit/Cuil" name="searchTitular.cuitcuil"
							maxlength="11" />
					</div>
					<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
						<!-- <label for="fraccionLetra">Documento</label> -->
						<s:textfield id="documento" class="form-control number"
							placeholder="Documento" name="searchTitular.documento"
							maxlength="8" />
					</div>
					<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
						<span class="input-group-btn">
							<button class="btn btn-default" type="button" onclick="search();">
								<span class="glyphicon glyphicon-search"></span> Buscar
							</button>
						</span> <span class="input-group-btn">
							<button class="btn btn-default" type="reset"
								onclick="resetForm();">
								<span class="glyphicon glyphicon-erase"></span> Limpiar
							</button>
						</span>
					</div>
				</div>
				<!-- <div id="result_search"></div> -->
				<div>
				 <s:include value="grid/titular-search-grid.jsp"></s:include>
				</div>
			</div>
		</div>
	</fieldset>
</s:form>
