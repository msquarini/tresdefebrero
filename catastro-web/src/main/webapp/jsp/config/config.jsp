<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

<style type="text/css">
</style>

<ul class="nav nav-pills"
	style="background-color: #eeeeee; border-bottom-style: ridge;">
	<li class="nav-header disabled"><a><strong>Configuracion</strong></a></li>

</ul>

<div class="tab-content top-buffer">
	<div id="div_config" class="tab-pane fade in active">

		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<span class="input-group-btn">
				<button id="update_btn" class="btn btn-default" type="button"
					onclick="update();"
					data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Actualizar">
					<span class="glyphicon glyphicon-search"></span>
					<s:text name="update.label"></s:text>
				</button>
			</span>
		</div>
	</div>
</div>

<script>
	function update() {
		loader(true, $('#update_btn'));

		$.ajax({
			method : "POST",
			url : "commons/config_update.action",
			data : {},
			traditional : true,
			success : function(res, textStatus, jqXHR) {
				loader(false, $('#update_btn'));
				handleErrorMessages(res);
				showErrorModalDialog(false);
			},
			error : function(res, textStatus, jqXHR) {
				loader(false, $('#update_btn'));
				handleErrorMessages(res.responseJSON);
				showErrorModalDialog(false);
			}
		});
	};

	
</script>