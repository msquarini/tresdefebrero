<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%-- <%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>
 --%>
<!-- Modal -->
<div class="modal fade" id="generic-dialog" tabindex="-1" role="dialog"
	aria-labelledby="generic-data-label">
	<div class="modal-dialog custom-class" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="generic-data-label"></h4>
			</div>
			<div class="modal-body" id="generic-modal-body">
				<p></p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

				<button id="seleccionar-dialog-button" type="button"
					class="btn btn-primary disabled" onclick="seleccionar();">
					<s:text name="dialog.label.seleccionar" />
				</button>
				<button id="nuevo-dialog-button" type="button"
					class="btn btn-primary" onclick="nuevo();">
					<s:text name="dialog.label.nuevo" />
				</button>

				<button id="submit-dialog-button" type="button"
					class="btn btn-primary" onclick="dialog_submit();">Grabar</button>
			</div>
		</div>
	</div>
</div>


<!-- Modal Medium (para respuestas)-->
<div class="modal fade" id="generic-dialog-response" tabindex="-1"
	role="dialog" aria-labelledby="generic-data-label">
	<div class="modal-dialog modal-md" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="generic-dialog-response-label"></h4>
			</div>
			<div class="modal-body" id="generic-modal-response-body">
				<p></p>
			</div>
			<div class="modal-footer">
				<button id="btn-print" type="button" class="btn btn-default"
					data-dismiss="modal" onclick="print();">
					<i class="fa fa-print" aria-hidden="true"></i> Imprimir
				</button>

				<button type="button" class="btn btn-default" data-dismiss="modal"
					onclick="closeResponseDialog();">Cerrar</button>
			</div>
		</div>
	</div>
</div>



<!-- Modal Medium (para ERRORES)-->
<div id="error-dialog" class="modal fade" tabindex="-1" role="dialog"
	aria-labelledby="generic-data-label">
	<div class="modal-dialog modal-md" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="error-title">Error en la operacion
					realizada.</h4>
			</div>
			<div id="error-dialog-body" class="modal-body">
				<div id="errors-mensaje" class="alert alert-info"
					style="display: none;"></div>
				<div id="errors-data" class="alert alert-danger"
					style="display: none;"></div>
				<div id="warning-data" class="alert alert-warning"
					style="display: none;"></div>
				<div id="info-data" class="alert alert-info" style="display: none;"></div>
				<div id="success-data" class="alert alert-success"
					style="display: none;"></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"
					onclick="closeErrorDialog();">Cerrar</button>
				<button id="btn_error_continuar" type="button"
					class="btn btn-default" data-dismiss="modal" onclick="continuar();">Continuar</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	function handleErrorMessages(jsonResp) {
		$("#errors-data").hide();
		$("#errors-data").empty();
		$("#warning-data").hide();
		$("#warning-data").empty();
		$("#errors-mensaje").hide();
		$("#errors-mensaje").empty();
		$("#info-data").hide();
		$("#info-data").empty();
		$("#success-data").hide();
		$("#success-data").empty();
		if (jsonResp.tituloError) {
			$("#error-title").text(jsonResp.tituloError);
		} else {
			$("#error-title").text("Error en la operación realizada.");
		}
		if (jsonResp.mensajeError) {
			$("#errors-mensaje").html("<p>" + jsonResp.mensajeError + "</p>");
			$("#errors-mensaje").show();
		} 		
		$.each(jsonResp.actionErrors, function(i, error) {
			if (i == 0) {
				$("#errors-data").show();
			}
			$(".alert-danger").append("<p>" + error + "</p>");
		});
		$.each(jsonResp.actionMessages, function(i, error) {
			if (i == 0) {
				$("#warning-data").show();
			}
			$(".alert-warning").append("<p>" + error + "</p>");
		});
		$.each(jsonResp.successMessages, function(i, message) {
			if (i == 0) {
				$("#success-data").show();
			}
			$(".alert-success").append("<p>" + message + "</p>");
		});
		$.each(jsonResp.infoMessages, function(i, message) {
			if (i == 0) {
				$("#info-data").show();
			}
			$(".alert-info").append("<p>" + message + "</p>");
		});
	}

	function showResponseModalDialog(printOption, body, title) {
		$("#generic-dialog-response-label").text("Resultado de la operacion");
		if (title) {
			$("#generic-dialog-response-label").text(title);
		}
		
		if (printOption) {
			$("#btn-print").show();
		} else {
			$("#btn-print").hide();
		}
		$('#generic-modal-response-body').html(body);
		$('#generic-dialog-response').modal();
	}

	function showErrorModalDialog(continuar) {
		if (continuar) {
			$("#btn_error_continuar").show();
		} else {
			$("#btn_error_continuar").hide();
		}
		$('#error-dialog').modal('show');
	}
	
	// Ocultar todos los botones
	function hideAllBtn() {
		$("#seleccionar-dialog-button").hide();
		$("#nuevo-dialog-button").hide();
		$("#submit-dialog-button").hide();
	}

	/* Dialogo de uso comun  (activa los botones por parametros)*/
	function showModalDialog(title, body, show_btn_array) {
		hideAllBtn();
		jQuery.each(show_btn_array, function(i, val) {
			$("#" + val).show();
		});

		/* if (savebtn) {
			$("#submit-dialog-button").show();
		} else {
			$("#submit-dialog-button").hide();
		} */

		$("#generic-data-label").text(title);
		$("#generic-modal-body").html(body);
		$('#generic-dialog').modal('show');
	}

	$('#generic-dialog-response').on('hidden.bs.modal', function () {
		closeResponseDialog();
	})
</script>