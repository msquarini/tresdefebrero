<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<!-- ALTA de RESPONSABLE (Copia de titular) -->

<script src="js/select/bootstrap-select.min.js"></script>
<link rel="stylesheet" href="css/select/bootstrap-select.css">

<script type="text/javascript">
	$('.selectpicker').selectpicker({
		width : 'fit'
	});

	$('#personaJuridica').checkboxpicker({
		offLabel : 'No',
		onLabel : 'Si'
	}).on('change', function() {
		if ($(this).is(':checked')) {
			$(this).prop('value', 'true');
		} else {
			$(this).prop('value', 'false');
		}
	});

	/* Submit del formulario previa validacion */
	function dialog_submit() {
		$('#responsable-edit-form').bootstrapValidator('validate');

		if ($('#responsable-edit-form').data('bootstrapValidator').isValid()) {
			$('#responsable-edit-form').submit();
		}
	};

	$('.form-control').keypress(function(e) {
		if (e.which == 13) {
			e.preventDefault();
			return false; //<---- Add this line
		}
	});

	/* Configuracion del formulario, ya que se hace submit del mismo mediante javascript */
	$('#responsable-edit-form').ajaxForm({
		target : '#generic-modal-response-body',
		success : function(res, textStatus, jqXHR) {
			cerrarDialogo();
		},
		error : function(res, textStatus, jqXHR) {
			//$("#error-titular").text(res.responseJSON.actionErrors[0]);
			handleErrorMessages(res.responseJSON);
			showErrorModalDialog(false);
		}
	});

	/* Configuracion de validador del formulario */
	$(document)
			.ready(
					function() {

						if ($('#personaJuridica').prop('value') == 'true') {
							$('#personaJuridica').prop('checked', 'true');
						}

						$('#responsable-edit-form')
								.bootstrapValidator(
										{
											framework : 'bootstrap',
											excluded : [ ':disabled' ],
											feedbackIcons : {
												invalid : 'glyphicon glyphicon-remove',
												validating : 'glyphicon glyphicon-refresh'
											},
											message : 'Datos invalidos',
											err : {
												// You can set it to popover
												// The message then will be shown in Bootstrap popover
												container : 'tooltip'
											},
											fields : {
												'responsable.titular.apellido' : {
													validators : {
														notEmpty : {
															message : 'Obligatorio'
														}
													}
												},
												'responsable.titular.documento' : {
													validators : {
														notEmpty : {
															message : 'Documento Obligatorio'
														},
														integer : {
															message : 'Debe ser un valor n�merico de 8 d�gitos'
														}
													}
												},
												'responsable.titular.cuitcuil' : {
													validators : {
														notEmpty : {
															message : 'Cuit/Cuil Obligatorio'
														},
														stringLength : {
															min : 11,
															max : 11,
															message : 'El dato ingresado debe ser de 11 posisiones num�ricas'
														},
														callback : {
															message : 'Cuit inv�lido',
															callback : function(
																	value,
																	validator,
																	$field) {
																return validaCuit(value);
															}
														}
													}
												},
												'responsable.desde' : {
													validators : {
														notEmpty : {
															message : 'Fecha desde que es responsable obligatorio'
														}
													}
												}
											}
										}).on('error.field.bv',
										function(e, data) {

											// $(e.target)  --> The form instance
											// $(e.target).data('bootstrapValidator')
											//              --> The BootstrapValidator instance

											// data.field   --> The field name
											// data.element --> The new field element
											// data.options --> The new field options

										});
					});
</script>
<style type="text/css">
.has-feedback .form-control {
	top: 0;
	/* Para utilizar todo el area editable */
	padding-right: 10px;
}

.form-control-feedback {
	/* Para correr el icon a la derecha */
	right: -22px;
}
</style>

<s:form id="responsable-edit-form" action="responsable_addResponsable"
	cssClass="form-vertical" namespace="/catastro" theme="bootstrap">

	<fieldset>
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<p id="error-titular" class="bg-danger"></p>
			</div>
		</div>
		<div class="row  ">
			<%-- <s:hidden name="titular.id"></s:hidden> --%>
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				<s:textfield id="apellido" class="form-control"
					name="responsable.titular.apellido" maxlength="50"
					placeholder="Apellidos" />
			</div>
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				<s:textfield id="nombre" class="form-control" maxlength="50"
					name="responsable.titular.nombre" placeholder="Nombres" />
			</div>

			<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
				<select id="tipodocumento"
					name="responsable.titular.tipoDocumento" class="selectpicker"
					title="Tipo Documento">
					<option value="1">DNI</option>
					<option value="2">Pasaporte</option>
					<option value="3">LC</option>
					<option value="4">LE</option>
				</select>
			</div>
			<div class="col-xs-6 col-sm-6 col-md-1 col-lg-2">
				<s:textfield id="documento" class="form-control number"
					maxlength="8" placeholder="Documento"
					name="responsable.titular.documento" />
			</div>
		</div>

		<!-- Domicilio Postal Titular -->
		<div class="panel-group">
			<div class="panel panel-default">
				<div class="panel-heading" href="#panel_domicilio_inmueble">Domicilio
					Postal</div>
				<div class="panel-body" id="panel_domicilio_postal">
					<div class="col-xs-6 col-sm-6 col-md-5 col-lg-4">
						<!-- <label for="di_calle">Calle</label> -->
						<s:textfield id="di_calle" class="form-control"
							placeholder="Calle"
							name="responsable.titular.domicilioPostal.calle" />
					</div>
					<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
						<!-- <label for="di_numero">Nro</label> -->
						<s:textfield id="di_numero" class="form-control" placeholder="Nro"
							name="responsable.titular.domicilioPostal.numero" />
					</div>
					<div class="col-xs-6 col-sm-6 col-md-1 col-lg-1">
						<!-- 	<label for="di_piso">Piso</label> -->
						<s:textfield id="di_piso" class="form-control" placeholder="Piso"
							name="responsable.titular.domicilioPostal.piso" />
					</div>
					<div class="col-xs-6 col-sm-6 col-md-1 col-lg-1">
						<!-- <label for="di_depto">Depto</label> -->
						<s:textfield id="di_depto" class="form-control" placeholder="Dpto"
							name="responsable.titular.domicilioPostal.departamento" />
					</div>
					<div class="col-xs-6 col-sm-6 col-md-1 col-lg-2">
						<!-- <label for="di_cp">CP</label> -->
						<s:textfield id="di_cp" class="form-control"
							placeholder="C�digo Postal"
							name="responsable.titular.domicilioPostal.codigoPostal" />
					</div>
					<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
						<!-- <label for="di_barrio">Barrio</label> -->
						<s:select id="di_barrio" list="#session.barrios" listKey="id"
							listValue="nombre" class="selectpicker" title="Barrio"
							name="responsable.titular.domicilioPostal.barrio.id"></s:select>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-xs-6 col-sm-6 col-md-3 col-lg-4">
				<label for="personaJuridica">Persona Jur�dica</label> <input
					id="personaJuridica" data-group-cls="btn-group-sm" type="checkbox"
					name="responsable.titular.personaJuridica"
					value='<s:property value="responsable.titular.personaJuridica"/>' />


			</div>
			<div class="col-xs-6 col-sm-6 col-md-1 col-lg-2">
				<s:textfield id="cuitcuil" class="form-control number"
					maxlength="11" placeholder="Cuit/Cuil"
					name="responsable.titular.cuitcuil" />
			</div>
			<div class="col-xs-6 col-sm-6 col-md-1 col-lg-6">
				<s:textfield id="razonSocial" class="form-control" maxlength="50"
					placeholder="Raz�n Social"
					name="responsable.titular.razonSocial" />
			</div>
		</div>

		<div class="row">
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				<div class="form-group">
					<div class="input-group date" data-provide="datepicker" id="desde"
						data-date-format="dd/mm/yyyy" data-date-autoclose="true"
						data-date-orientation="bottom" data-date-language="es">
						<input type="text" class="form-control" placeholder="Fecha desde"
							value='<s:date name="responsable.desde" format="dd/MM/yyyy" />'
							name="responsable.desde">
						<div class="input-group-addon">
							<span class="glyphicon glyphicon-calendar"></span>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				<div class="input-group date" data-provide="datepicker"
					data-date-format="dd/mm/yyyy" data-date-autoclose="true"
					data-date-orientation="bottom" data-date-language="es">
					<input id="hasta" type="text" class="form-control"
						placeholder="Fecha hasta"
						value="<s:date name="responsable.hasta" format="dd/MM/yyyy" />"
						name="responsable.hasta">
					<div class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"></span>
					</div>
				</div>

			</div>
		</div>

		<!-- Observacion -->
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<s:textfield id="observacion"
					name="responsable.titular.observacion" placeholder="Observaci�n" />
			</div>
		</div>
	</fieldset>

</s:form>

<script>
	$('input.number').number(true, 0, '', '');
	$('input.numberdecimal').number(true, 2, ',', '');

	$('#desde').on(
			'changeDate show',
			function(e) {
				// Revalidate the date field
				$('#responsable-edit-form').bootstrapValidator('revalidateField',
						'responsable.desde');
			});

	$('#datePicker').datepicker({
		format : 'mm/dd/yyyy'
	}).on('changeDate', function(e) {
		// Revalidate the date field
		$('#responsable-edit-form').bootstrapValidator('revalidateField', 'desde');
	});

	
</script>