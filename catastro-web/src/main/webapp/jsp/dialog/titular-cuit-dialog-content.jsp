<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<script type="text/javascript">
	function closeResponseDialog() {
		// Por compatabilidad con el dialogo	
	}

	/* Submit del formulario previa validacion */
	function dialog_titular_cuit_submit() {
		$('#titular-cuit-form').bootstrapValidator('validate');

		if ($('#titular-cuit-form').data('bootstrapValidator').isValid()) {
			$('#titular-cuit-form').submit();
		}
	};

	/* Configuracion del formulario, ya que se hace submit del mismo mediante javascript */
	$('#titular-cuit-form').ajaxForm({
		target : '#generic-modal-response-body',
		success : function(res, textStatus, jqXHR) {
			$("#submit-dialog-button").hide();

			$("#titular-grid-data").bootgrid('reload');
			
			$('#titular-dialog').modal('hide');
		},
		error : function(res, textStatus, jqXHR) {
			$("#error").text(res.responseJSON.actionErrors[0]);
			//debugger;
		}
	});

	/* Configuracion de validador del formulario */
	$(document)
			.ready(
					function() {
						$('input.number').number(true, '', '', '');

						$('#titular-cuit-form')
								.bootstrapValidator(
										{
											framework : 'bootstrap',
											excluded : [ ':disabled' ],
											feedbackIcons : {
												//valid : 'glyphicon glyphicon-ok',
												invalid : 'glyphicon glyphicon-remove',
												validating : 'glyphicon glyphicon-refresh'
											},
											message : 'Datos invalidos',
											/* submitHandler: function(validator, form, submitButton) {
													
											}, */
											err : {
												// You can set it to popover
												// The message then will be shown in Bootstrap popover
												container : 'tooltip'
											},
											fields : {
												'cuitcuil' : {
													validators : {
														notEmpty : {
															message : 'Obligatorio'
														},
														stringLength : {
															min : 11,
															max : 11,
															message : 'El dato ingresado debe ser de 11 posisiones numéricas'
														},
														callback : {
															message : 'Cuit inválido',
															callback : function(
																	value,
																	validator,
																	$field) {
																return validaCuit(value);
															}
														}
													}
												}

											}
										}).on('error.field.bv',
										function(e, data) {

											// $(e.target)  --> The form instance
											// $(e.target).data('bootstrapValidator')
											//              --> The BootstrapValidator instance

											// data.field   --> The field name
											// data.element --> The new field element
											// data.options --> The new field options

										});
					});
</script>
<style type="text/css">
.has-feedback .form-control {
	top: 0;
	/* Para utilizar todo el area editable */
	padding-right: 10px;
}

.form-control-feedback {
	/* Para correr el icon a la derecha */
	right: -22px;
}
</style>
<s:form id="titular-cuit-form" action="titularABM_updateCuitCuil"
	cssClass="form-vertical" namespace="/catastro" theme="bootstrap">

	<fieldset>
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<p id="error" class="bg-danger"></p>
			</div>
		</div>
		<div class="row  ">
			<div class="col-xs-6 col-sm-6 col-md-5 col-lg-5">
				<label for="circunscripcion"><s:text name="titular.apellido"/></label>
				<s:textfield id="f_apellido" class="form-control" name="apellido"
					readonly="true" />
			</div>
			<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
				<label for="cuitcuil"><s:text name="titular.cuitcuil"/></label>
				<s:textfield id="f_cuitcuil" class="form-control number"
					maxlength="11" name="cuitcuil" />				
				<s:hidden id="uuidTP" name="uuidTP"></s:hidden>
			</div>
		</div>

	</fieldset>

</s:form>

