<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<script type="text/javascript">	
</script>

<style type="text/css">
th {
text-align: center;
}

.table-xtra-condensed > thead > tr > th,
.table-xtra-condensed > tbody > tr > th,
.table-xtra-condensed > tfoot > tr > th,
.table-xtra-condensed > thead > tr > td,
.table-xtra-condensed > tbody > tr > td,
.table-xtra-condensed > tfoot > tr > td {
  padding: 3px;  
}
</style>

<table class="table table-xtra-condensed" style="width: 98%; margin-top: 10px" >
	<thead style="background-color: #eeeeee;  text-align: center; font-size: 12px; x" >
		<tr>
			<th><s:text name="circ.short.label"/></th>
			<th><s:text name="sec.short.label"/></th>
			<th><s:text name="fr.short.label"/></th>
			<th><s:text name="frl.short.label"/></th>
			<th><s:text name="mz.short.label"/></th>
			<th><s:text name="mzl.short.label"/></th>
			<th><s:text name="pc.short.label"/></th>
			<th><s:text name="pcl.short.label"/></th>
			<th><s:text name="uf.short.label"/></th>
		</tr>
	</thead>
	<tbody style="background-color: #eeeeee; text-align: center;  font-size: 18px; font-weight: bold;" class="text-primary">
		<tr>
			<td><s:property value="circunscripcion"/></td>
			<td><s:property value="seccion"></s:property></td>
			<td><s:property value="fraccion"></s:property></td>
			<td><s:property value="fraccionLetra"></s:property></td>
			<td><s:property value="manzana"></s:property></td>
			<td><s:property value="manzanaLetra"></s:property></td>
			<td><s:property value="parcela"></s:property></td>
			<td><s:property value="parcelaLetra"></s:property></td>
			<td><s:property value="unidadFuncional"></s:property></td>
		</tr>
	</tbody>
</table>