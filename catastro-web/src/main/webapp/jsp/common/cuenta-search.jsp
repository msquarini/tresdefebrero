<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<!-- Busqueda y seleccion de cuenta para tramites comunes
	 Titulares
	 Responsables
	 Ajuste Linea
 -->
<script type="text/javascript">
	var hash = "";

	$(document).off("cuentaSelectedEvent").on("cuentaSelectedEvent",
			function(event, data) {
				$("#idCuentaOrigen").val(data.cuenta.id);
				$("#titular_btn").toggleClass('disabled', false);
				$("#responsable_btn").toggleClass('disabled', false);
				$("#linea_btn").toggleClass('disabled', false);
			}).off("cuentaDeselectedEvent").on("cuentaDeselectedEvent",
			function(event, data) {
				$("#idCuentaOrigen").val("-1");
				$("#titular_btn").toggleClass('disabled', true);
				$("#responsable_btn").toggleClass('disabled', true);
				$("#linea_btn").toggleClass('disabled', true);
			});


	function continuar() {				
		if (hash.startsWith('AJU')) {
			common_submit('catastro/linea_init.action', null, hash);
		} else if (hash.startsWith('TIT')) {
			common_submit('catastro/titularABM_init.action', null, hash);
		} else if (hash.startsWith('RES')) {
			common_submit('catastro/responsable_init.action', null, hash);
		}
	}

	function common_action(action, btn) {
		common_submit(action, btn, null);
	};
	
	function common_submit(url, btn, hashValue) {
		loader(true, btn);		
		$.ajax({
			method : "POST",
			url : url,
			data : $('#common-cuenta-form').serialize() + "&hash=" + hashValue,
			traditional : true,
			global : false,
			success : function(res) {
				loader(false, btn);
				$("#container").html(res);
			},
			error : function(res, textStatus, jqXHR) {				
				loader(false, btn);
				handleErrorMessages(res.responseJSON);
				if (res.status == '501') {					
					hash = res.responseJSON.hash;
					showErrorModalDialog(true);
				} else {
					showErrorModalDialog(false);
				}
			}
		});
	}
</script>
<style type="text/css">
</style>

<div class="row">
	<s:include value="../cuenta_search.jsp"></s:include>
</div>

<div class="row">
	<s:form id="common-cuenta-form" action="titular_init"
		namespace="/catastro" theme="bootstrap">
		<s:hidden id="idCuentaOrigen" name="idCuentaOrigen"></s:hidden>
		<%-- <s:hidden id="hash" name="hash"></s:hidden> --%>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<!-- Para poder manejar la respuesta del llamado ajax!!! -->
			<sj:a id="titular_btn" cssClass="btn btn-primary disabled"
				value="titular"
				onclick="common_action('catastro/titularABM_init.action', $('#titular_btn') );">Gestion Titular</sj:a>
			<sj:a id="responsable_btn" cssClass="btn btn-primary disabled"
				value="responsable"
				onclick="common_action('catastro/responsable_init.action', $('#responsable_btn') );">Gestion Reponsable</sj:a>
			<sj:a id="linea_btn" cssClass="btn btn-primary disabled"
				value="linea"
				onclick="common_action('catastro/linea_init.action',$('#linea_btn'));">Gestion Linea</sj:a>
		</div>
	</s:form>
</div>