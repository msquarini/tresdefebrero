<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<script type="text/javascript">
	
</script>
<style>
.align-right {
	text-align: right;
}

.row.equal {
	display: flex;
	display: -webkit-flex;
	flex-wrap: wrap;
}
</style>

<form id="cuenta-summary">
	<fieldset disabled="disabled">
		<div class="bs-callout bs-callout-info"
			style="background-color: #bacfdd;">
			<div class="row equal">
				<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
					<h3 align="left" class="text-primary">
						<small>Cuenta:</small>&nbsp;<strong><s:property
								value="%{#session.cuenta.cuenta}" /> / <s:property
								value="%{#session.cuenta.dv}" /></strong>
					</h3>
				</div>

				<div class="col-xs-12 col-sm-12 col-md-8 col-lg-6">
					<s:push value="%{#session.cuenta.parcela.nomenclatura}">
						<s:include value="nomenclatura-info.jsp"></s:include>
					</s:push>
				</div>

				<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
					<s:if test="%{#session.cuenta_con_deuda}">
						<h3 align="right" style="margin-right: 10px">
							<span class="label label-danger">Presenta Deuda</span>
						</h3>
					</s:if>
				</div>
			</div>
			<div class="row">

				<div class="col-xs-12 col-sm-12 col-md-3 col-lg-4">
					<h3 align="left" class="text-primary">
						<small><s:text name="fecha_data.label" />:</small>&nbsp;<strong>
							<s:property
								value="getText('format.date',{#session.cuenta.parcela.lineaVigente.fecha})"></s:property>
						</strong>
					</h3>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-3 col-lg-4">
					<h3 align="left" class="text-primary">
						<small><s:text name="valuacion.panel.label" />: $ </small>&nbsp;<strong>
							<s:property
								value="getText('{0,number,#,###.00}',{#session.cuenta.parcela.lineaVigente.valuacion})"></s:property>
						</strong>
					</h3>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-3 col-lg-4">
					<h3 align="left" class="text-primary">
						<small><s:text name="linea.grid.categoria" />: </small>&nbsp;<strong>
							<s:property
								value="%{#session.cuenta.parcela.lineaVigente.categoria}"></s:property>
						</strong>
					</h3>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">Datos
					titulares (que informacion incluir?)</div>
			</div>
		</div>
	</fieldset>
</form>