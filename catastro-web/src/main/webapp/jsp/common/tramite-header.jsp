<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<script type="text/javascript">
	
</script>
<style>
.align-right {
	text-align: right;
}
</style>

<form id="tramite-info">
	<fieldset disabled="disabled">
		<div class="bs-callout bs-callout-info"
			style="background-color: #bacfdd;">
			<div class="row">
				<div class="col-xs-6 col-sm-6 col-md-3 col-lg-2">
					<h3 align="left" class="text-primary">
						<small>Tramite:</small>&nbsp;<strong><s:property
								value="tramite.id" /></strong>
					</h3>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
					<h3 align="left" class="text-primary">
						<small>Tipo:</small>&nbsp;
						<s:property value="tramite.tipo.descripcion" />
					</h3>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-3 col-lg-2">
					<h3 align="left" class="text-primary">
						<small>Cuenta:</small>&nbsp;<strong><s:property
								value="cuentaOrigen.cuenta" />/<s:property
								value="cuentaOrigen.dv" /></strong>
					</h3>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-3 col-lg-2">
					<s:if test="%{#session.cuenta_con_deuda}">
						<h3 align="right" style="margin-right: 10px">
							<span class="label label-danger">Presenta Deuda</span>
						</h3>
					</s:if>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 ">
					<h6>
						<small>Creado:</small>&nbsp;<span><i
							class="glyphicon glyphicon-user"></i>&nbsp;<s:property
								value="tramite.createdBy" /></span>
					</h6>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
					<h6>
						<span><i class="glyphicon glyphicon-calendar"></i>&nbsp;<s:property
								value="tramite.createdDate" /></span>
					</h6>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
					<h6>
						<small>Modificado:</small>&nbsp; <span><i
							class="glyphicon glyphicon-user"></i>&nbsp;<s:property
								value="tramite.modifiedBy" /></span>
					</h6>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
					<h6>
						<span><i class="glyphicon glyphicon-calendar"></i>&nbsp;<s:property
								value="tramite.modifiedDate" /></span>
					</h6>
				</div>
			</div>
	</fieldset>
</form>