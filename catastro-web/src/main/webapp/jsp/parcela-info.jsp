<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<script type="text/javascript">
</script>
<style type="text/css">
</style>

<div class="sg-container">

	<s:form theme="bootstrap" cssClass="form-vertical">
		<fieldset disabled>
			<div class="row">
				<div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
					<label for="cuenta">Cuenta</label>
					<s:textfield id="cuenta" value="%{#session.cuenta.cuenta}" />
				</div>
				<div class="col-xs-6 col-sm-3 col-md-2 col-lg-2">
					<label for="DV">DV</label>
					<s:textfield id="DV" value="%{#session.cuenta.dv}" />
				</div>

				<div
					class="col-xs-6 col-sm-3 col-md-3 col-lg-3 col-md-offset-1 col-lg-offset-1">
					<label for="titular">Nomenclatura</label>
					<s:textfield id="titular" class="form-control"
						value="%{#session.cuenta.catastro.nomenclatura}" />
				</div>
			</div>
			
		</fieldset>
	</s:form>

</div>
