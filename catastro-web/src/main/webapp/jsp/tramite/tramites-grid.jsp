<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<style>
.small-col {
	font-size: small;
}
</style>

<script src="js/select/bootstrap-select.min.js"></script>
<link rel="stylesheet" href="css/select/bootstrap-select.css">

<s:form id="tramites-search-form" action="tramite_search"
	cssClass="form-vertical" namespace="/catastro" theme="bootstrap">

	<div
		style="background-color: #eeeeee; padding-top: 7.5px; height: 50px">

		<div class="col-xs-12 col-sm-12 col-md-3 col-lg-2">
			<div class="form-group">
				<div class="input-group date" data-provide="datepicker" id="desde"
					data-date-format="dd/mm/yyyy" data-date-autoclose="true"
					data-date-orientation="bottom" data-date-language="es"
					data-date-today-btn="true" data-date-today-highlight="true">
					<input type="text" class="form-control" placeholder="Fecha desde"
						value='<s:property value="getText('format.date',{from})"/>'
						name="from">
					<div class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"></span>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-3 col-lg-2">
			<div class="form-group">
				<div class="input-group date" data-provide="datepicker" id="desde"
					data-date-format="dd/mm/yyyy" data-date-autoclose="true"
					data-date-orientation="bottom" data-date-language="es"
					data-date-today-btn="true" data-date-today-highlight="true">
					<input type="text" class="form-control" placeholder="Fecha hasta"
						value='<s:property value="getText('format.date',{to})"/>'
						name="to">
					<div class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"></span>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-4 col-lg-3">
			<select id="estado" name="estado" class="selectpicker" title="Estado">
				<option value="0">Pendiente</option>
				<option value="1">Proceso</option>
				<option value="2">Finalizado</option>
				<option value="3">Error</option>
			</select>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-4 col-lg-2">
			<span class="input-group-btn">
				<button id="search_btn" class="btn btn-default" type="button"
					onclick="search();"
					data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Buscando">
					<span class="glyphicon glyphicon-search"></span>
					<s:text name="search.label"></s:text>
				</button>
			</span>
		</div>

		<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 pull-right"
			align="right">
			<%-- <span class="input-group-btn"> --%>
			<button id="cargarTramite_btn" class="btn btn-warning disabled"
				type="button" disabled="disabled"
				data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Cargando..."
				onclick="cargar();">
				<span class="fa fa-folder-open-o"></span>&nbsp;&nbsp;Cargar&nbsp;&nbsp;
			</button>
			<button id="cancelarTramite_btn" class="btn btn-danger disabled"
				type="button" disabled="disabled"
				data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Cancelando..."
				onclick="cancelar();">
				<span class="fa fa-window-close-o"></span> Cancelar
			</button>
			<%-- </span> --%>
		</div>
	</div>

	<input type="hidden" id="idTramite" name="idTramite" />
</s:form>

<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<table id="tramites-grid-data"
			class="table table-condensed table-hover table-striped">
			<thead>
				<tr>
					<th data-column-id="id" data-identifier="true" data-type="numeric"
						data-width="8%" data-visible="true">Numero</th>
					<%-- <th data-column-id="codigo" data-width="10%" data-sortable="false"><s:text
							name="tramite.codigo.label" /></th> --%>
					<%-- <th data-column-id="tipo" data-width="12%" data-sortable="false"><s:text
							name="tramite.tipo.label" /></th> --%>
					<th data-column-id="tipo" data-formatter="fmt_descripcion"
						data-width="14%" data-sortable="false"><s:text
							name="tramite.tipo.label" /></th>
					<th data-column-id="estado" data-formatter="fmt_status"
						data-width="12%"><s:text name="estado.label" /></th>
					<th data-column-id="cuenta" data-formatter="nested"
						data-width="12%"><s:text name="cuenta.label" /></th>
					<th data-column-id="createdDate" data-type="datetime"
						data-width="12%"><s:text name="fecha.alta.label" /></th>
					<th data-column-id="createdBy" data-type="string" data-width="12%"><s:text
							name="usuario.alta.label" /></th>
					<th data-column-id="modifiedDate" data-type="datetime"
						data-width="12%"><s:text name="fecha.modifico.label" /></th>
					<th data-column-id="modifiedBy" data-type="string" data-width="12%"><s:text
							name="usuario.modifico.label" /></th>
					<!-- <th data-column-id="commands" data-sortable="false"
						data-formatter="commands" data-width="7%">Acciones</th> -->
				</tr>
			</thead>

		</table>
	</div>
</div>


<script>
	function search() {
		loader(true, $('#search_btn'));
		$('#tramites-search-form').submit();
	};

	function cancelar() {
		loader(true, $('#cancelarTramite_btn'));
		//$('#pagos-procesar-form').submit();
		cancelarTramite($('#idTramite').val());
	};

	function cargar() {
		loader(true, $('#cargarTramite_btn'));
		//$('#pagos-procesar-form').submit();
		cargarTramite($('#idTramite').val());
	};

	$('#tramites-search-form').ajaxForm({
		success : function(res, textStatus, jqXHR) {
			$("#tramites-grid-data").bootgrid('reload');
			loader(false, $('#search_btn'));
		},
		error : function(res, textStatus, jqXHR) {
			loader(false, $('#search_btn'));
			alert(res);
		}
	});

	$('.selectpicker').selectpicker({
		width : '100%'
	});

	$(document)
			.ready(
					function() {
						//search();

						$("#tramites-grid-data")
								.bootgrid(
										{
											ajax : true,
											navigation : 2,
											rowCount : 10,
											rowSelect : true,
											selection : true,
											multiSelect : false,
											search : false,
											columnSelection : false,
											templates : {
												search : ""
											},
											labels : {
												noResults : "No se encontraron tr\u00e1mites.",
												loading : "Cargando información...",
												infos : "Mostrando {{ctx.start}} a {{ctx.end}} de {{ctx.total}} filas"
											},
											requestHandler : function(request) {
												return sortRequest(request);
											},
											formatters : {
												nested : function(column, row) {
													if (row.cuentasOrigen[0]) {
														return row.cuentasOrigen[0][column.id];
													}
													return "";
												},
												commands : function(column, row) {
													var idrow = row.id;
													return "<button onclick=\"detalle_pagos('"
															+ idrow
															+ "');"
															+ "\" type=\"button\" class=\"btn btn-xs btn-default command-edit\" data-row-id=\""
															+ idrow
															+ "\"><span class=\"fa fa-info-circle\" aria-hidden=\"true\"></span></button> ";
												},
												fmt_status : function(column,
														row) {
													return fmt_status(row.estado._name);
												},
												fmt_descripcion : function(
														column, row) {
													return row.tipo.descripcion;
												}
											},
											converters : {
												date : {
													from : function(value) {
														return moment(value);
													},
													to : function(value) {
														if (!value) {
															return "";
														}
														return moment(value)
																.format(
																		"DD-MM-YYYY");
													}
												},
												datetime : {
													from : function(value) {
														return moment(value);
													},
													to : function(value) {
														if (!value) {
															return "";
														}
														return moment(value)
																.format(
																		"DD-MM-YYYY HH:mm");
													}
												}
											},
											url : "catastro/tramite_getData.action"
										})
								.on(
										"selected.rs.jquery.bootgrid",
										function(e, rows) {
											switchEnable(
													$('#cargarTramite_btn'),
													true);
											if (rows[0].estado != 'FINALIZADO') {
												switchEnable(
														$('#cancelarTramite_btn'),
														true);
											}
											$('#idTramite').val(rows[0].id);
										}).on(
										"deselected.rs.jquery.bootgrid",
										function(e, rows) {
											switchEnable(
													$('#cargarTramite_btn'),
													false);
											switchEnable(
													$('#cancelarTramite_btn'),
													false);
											$('#idTramite').val("-1");
										});
						;

					});

	function cancelarTramite(id) {
		$.ajax({
			method : "POST",
			url : "catastro/tramite_cancelar.action",
			data : {
				idTramite : id
			},
			global : false,
			traditional : true,
			success : function(res) {
				loader(false, $('#cancelarTramite_btn'));
				$('#container').html(res);
			},
			error : function(res, textStatus, jqXHR) {
				loader(false, $('#cancelarTramite_btn'));
				handleErrorMessages(res.responseJSON);
				$('#error-dialog').modal('show');
			}
		});
	}

	function cargarTramite(id) {
		$.ajax({
			method : "POST",
			url : "catastro/tramite_cargar.action",
			data : {
				idTramite : id
			},
			traditional : true,
			success : function(res) {
				loader(false, $('#cargarTramite_btn'));
				$('#container').html(res);
			},
			error : function(res, textStatus, jqXHR) {
				loader(false, $('#cargarTramite_btn'));
				handleErrorMessages(res.responseJSON);
				showErrorModalDialog(false);
			}
		});
	}

	
</script>