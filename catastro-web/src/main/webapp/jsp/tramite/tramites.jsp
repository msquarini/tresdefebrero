<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

<style type="text/css">
</style>

<ul class="nav nav-pills"
	style="background-color: #eeeeee; border-bottom-style: ridge;">
	<li class="nav-header disabled"><a><strong>Gestion de
				Tramites</strong></a></li>

	<li class="active"><a id="upload-pagos-tab" data-toggle="tab" href="#tramites"><s:text
				name="tramites.label" /></a></li>
	
</ul>

<div class="tab-content top-buffer">

	<div id="tramites" class="tab-pane fade in active">
		<s:include value="tramites-grid.jsp"></s:include>
	</div>

</div>