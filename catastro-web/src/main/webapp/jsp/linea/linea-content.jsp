<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<!-- Ajuste de linea 
	Se edita los campos nuevos.
	Se utiliza la grilla de lineas para ver la historia de movimientos.
	 -->
<script type="text/javascript">
	$('.selectpicker').selectpicker({
		width : 'auto'
	});

	function cambioCategoria() {
		var cat = $('#ajuste_categoria').val();

		if (cat == 4) {
			//$("#sup_terreno").attr('disabled', 'disabled');
			$("#sup_cubierta").attr('disabled', 'disabled');
			$("#sup_semi").attr('disabled', 'disabled');
			$("#valor_comun").attr('disabled', 'disabled');
			//$("#valor_tierra").attr('disabled', 'disabled');
			$("#valor_edificio").attr('disabled', 'disabled');
		} else {
			$('#sup_terreno').removeAttr('disabled');
			$('#sup_cubierta').removeAttr('disabled');
			$('#sup_semi').removeAttr('disabled');
			$('#valor_comun').removeAttr('disabled');
			$('#valor_tierra').removeAttr('disabled');
			$('#valor_edificio').removeAttr('disabled');
		}
	}

	function updateValuacion() {
		var v = parseFloat($('#valor_tierra').val())
				+ parseFloat($('#valor_comun').val())
				+ parseFloat($('#valor_edificio').val());
		$('#valuacion').text('$ ' + $.number(v, 2, ',', '.'));
	}

	/* 	$('#ajuste-linea-form').ajaxForm({
	 target : '#generic-modal-response-body',
	 success : function(res, textStatus, jqXHR) {
	 loader(false, $('#btn_process'));
	 showResponseModalDialog(true);
	 },
	 error : function(res, textStatus, jqXHR) {
	 loader(false, $('#btn_process'));
	 handleErrorMessages(res.responseJSON);
	 showErrorModalDialog(false);
	 }
	 }); */

	$(document).ready(
			function() {
				$('#ajuste_categoria').selectpicker('val',
						$('#hiddenCategoriaActual').val());

				$('input.number').number(true, 0, '', '');
				$('input.numberdecimal').number(true, 2, ',', '');

				$('#tf_oficio').checkboxpicker({
					offLabel : 'No',
					onLabel : 'Si'
				}).on('change', function() {
					if ($(this).is(':checked')) {
						$(this).prop('value', 'true');
					} else {
						$(this).prop('value', 'false');
					}
				});

				if ($('#tf_oficio').prop('value') == 'true') {
					$('#tf_oficio').prop('checked', 'true');
				}

				// Actualizar totales primera vez 
				updateValuacion();
				cambioCategoria();

				$('#ajuste-linea-form').bootstrapValidator({
					framework : 'bootstrap',
					excluded : [ ':disabled' ],
					feedbackIcons : {
						//valid : 'glyphicon glyphicon-ok',
						invalid : 'glyphicon glyphicon-remove',
						validating : 'glyphicon glyphicon-refresh'
					},
					message : 'Datos invalidos',
					/* submitHandler: function(validator, form, submitButton) {                
					}, */
					err : {
						// You can set it to popover
						// The message then will be shown in Bootstrap popover
						container : 'tooltip'
					},
					fields : {
						'ajusteCategoria' : {
							validators : {
								notEmpty : {
									message : 'Obligatorio'
								}
							}
						},
						'nuevaLinea.superficieTerreno' : {
							validators : {
								notEmpty : {
									message : 'Obligatorio'
								},
								greaterThan : {
									message : 'El valor debe ser mayor a 0',
									value : 0,
									inclusive : false
								}
							}
						},
						'nuevaLinea.superficieCubierta' : {
							validators : {
								/* 		callback: {
								            message: 'Obligatorio',
								            callback: function (value, validator, $field) {
								                if(value=="" && $("#ajuste_categoria").val()!=4)
								                	return false;
								                return true;
								            }
								        } */

								notEmpty : {
									message : 'Obligatorio'
								},
								greaterThan : {
									message : 'El valor debe ser mayor a 0',
									value : 0,
									inclusive : false
								}
							}
						},
						'nuevaLinea.superficieSemi' : {
							validators : {
							/* 	callback: {
							        message: 'Obligatorio',
							        callback: function (value, validator, $field) {
							            if(value=="" && $("#ajuste_categoria").val()!=4)
							            	return false;
							            return true;
							        }
							    } */
							/*notEmpty : {
								message : 'Obligatorio'
							}*/
							}
						},
						'nuevaLinea.valorTierra' : {
							validators : {
								notEmpty : {
									message : 'Obligatorio'
								},
								greaterThan : {
									message : 'El valor debe ser mayor a 0',
									value : 0,
									inclusive : false
								}
							}
						},
						'nuevaLinea.valorComun' : {
							validators : {
							/* notEmpty : {
								message : 'Obligatorio'
							} */
							}
						},
						'nuevaLinea.valorEdificio' : {
							validators : {

								/* 					callback: function(value, validator, $field) {
													    // ... Do your logic checking
													    if (value=="" && $("#ajuste_categoria").val()!=4) {
													        return {
													            valid: false,
													            message: 'Obligatorio'
													        }
													    }
														if(parseFloat(value.toString())==0)
														{
														    return {
														        valid: false,
														        message: 'El valor debe ser mayor a 0'
														    }								
														}
													    return {
													        valid: true,
													        message: ''
													    }    
													} */

								notEmpty : {
									message : 'Obligatorio'
								},
								greaterThan : {
									message : 'El valor debe ser mayor a 0',
									value : 0,
									inclusive : false
								}
							}
						},
						'nuevaLinea.fecha' : {
							validators : {
								notEmpty : {
									message : 'Obligatorio'
								}
							}
						}
					}
				}).on('error.field.bv', function(e, data) {

					// $(e.target)  --> The form instance
					// $(e.target).data('bootstrapValidator')
					//              --> The BootstrapValidator instance

					// data.field   --> The field name
					// data.element --> The new field element
					// data.options --> The new field options
				});

			});
</script>

<style type="text/css">
.align-right {
	text-align: right;
}
</style>

<form id="ajuste-linea-form" action="linea_process">

	<s:if test='tramite.estado.toString() == "FINALIZADO"'>
		<fieldset disabled="disabled">
	</s:if>

	<div class="row ">
		<div class="col-xs-6 col-sm-6 col-md-3 col-lg-2">
			<div class="form-group">
				<label for="tf_vigencia"><s:text name="vigencia.label" /></label>
				<div class="input-group date" data-provide="datepicker"
					data-date-format="dd/mm/yyyy" data-date-autoclose="true"
					data-date-end-date="0d" data-date-orientation="bottom"
					data-date-language="es" data-date-today-btn="true"
					data-date-today-highlight="true">
					<input id="tf_vigencia" type="text" class="form-control"
						name="nuevaLinea.fecha"
						value="<s:property value='getText("format.date",{nuevaLinea.fecha})'/>">
					<div class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"></span>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xs-6 col-sm-6 col-md-3 col-lg-2">
			<div class="form-group">
				<label for="ajuste_categoria"><s:text name="categoria.label" /></label>
				<div class="input-group">
					<input type="hidden"
						value='<s:property value="nuevaLinea.categoria"></s:property>'
						id="hiddenCategoriaActual"> <select id="ajuste_categoria"
						name="ajusteCategoria" onchange="cambioCategoria();"
						class="selectpicker form-control">
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
						<option value="4">4</option>
						<option value="5">5</option>
						<option value="6">6</option>
						<option value="7">7</option>
						<option value="8">8</option>
					</select>
				</div>
			</div>
		</div>
		<div class="col-xs-6 col-sm-6 col-md-3 col-lg-offset-2 col-lg-2"
			style="flex: 1; padding: 25px;">
			<label for="tf_oficio"><s:text name="tramite.oficio.label" />&nbsp;</label><input
				id="tf_oficio" type="checkbox" name="tramite.oficio"
				value="<s:property value="tramite.oficio" />" />
		</div>
	</div>

	<div class="row  input-group-sm">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<textarea class="form-control" rows="2" placeholder="Observaciones"
				name="tramite.observacion" id="tf_observacion"><s:property value="tramite.observacion" /></textarea>
		</div>
	</div>

	<div class="row top-buffer">
		<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					<div class="pull-left">
						<strong><s:text name="superficies.panel.label" /></strong>
					</div>
					<%-- <div class="pull-right text-primary" style="padding-right: 15px;">
						 <span class="label label-info" style="font-size: 100%"><s:text
								name="fecha_data.label" />: <s:property
								value="getText('format.date',{fecha})"></s:property></span>
						
						</div> --%>
				</div>

				<div class="panel-body">
					<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
						<div class="form-group">
							<label for="sup_terreno"><s:text name="terreno.label" /></label>
							<div class="input-group">
								<input class="form-control align-right numberdecimal"
									id="sup_terreno" type="text"
									name="nuevaLinea.superficieTerreno"
									value='<s:property value="getText('number.format',{nuevaLinea.superficieTerreno})"></s:property>' />
								<span class="input-group-addon">m<sup>2</sup></span>
							</div>
						</div>
					</div>
					<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
						<div class="form-group">
							<label for="sup_cubierta"><s:text name="cubierta.label" /></label>
							<div class="input-group">
								<input class="form-control align-right numberdecimal"
									id="sup_cubierta" type="text"
									name="nuevaLinea.superficieCubierta"
									value='<s:property value="getText('number.format',{nuevaLinea.superficieCubierta})"></s:property>' />
								<span class="input-group-addon">m<sup>2</sup></span>
							</div>
						</div>
					</div>
					<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
						<div class="form-group">
							<label for="sup_semi"><s:text name="semi.label" /></label>
							<div class="input-group">
								<input class="form-control align-right numberdecimal"
									id="sup_semi" type="text" name="nuevaLinea.superficieSemi"
									value='<s:property value="getText('number.format',{nuevaLinea.superficieSemi})"></s:property>' />
								<span class="input-group-addon">m<sup>2</sup></span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					<div class="pull-left">
						<strong><s:text name="valuacion.panel.label" /></strong>
					</div>
					<div class="pull-right" style="padding-right: 15px;">
						<span id="valuacion" class="label label-info"
							style="font-size: 100%">$ 0,00</span>
					</div>
				</div>
				<div class="panel-body">
					<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
						<div class="form-group">
							<label for="valor_tierra"><s:text name="tierra.label" /></label>
							<div class="input-group">
								<span class="input-group-addon">$</span> <input
									id="valor_tierra" onchange="updateValuacion()"
									class="form-control align-right numberdecimal" type="text"
									name="nuevaLinea.valorTierra"
									value='<s:property value="getText('money.format',{nuevaLinea.valorTierra})"></s:property>' />
							</div>
						</div>
					</div>
					<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
						<div class="form-group">
							<label for="valor_comun"><s:text name="comun.label" /></label>
							<div class="input-group">
								<span class="input-group-addon">$</span> <input id="valor_comun"
									onchange="updateValuacion()"
									class="form-control align-right numberdecimal" type="text"
									name="nuevaLinea.valorComun"
									value='<s:property value="getText('money.format',{nuevaLinea.valorComun})"></s:property>' />
							</div>
						</div>
					</div>
					<div class="col-xs-6 col-sm-6 col-md-4 col-lg-42">
						<div class="form-group">
							<label for="valor_edificio"><s:text name="edificio.label" /></label>
							<div class="input-group">
								<span class="input-group-addon">$</span> <input
									id="valor_edificio" onchange="updateValuacion()"
									name="nuevaLinea.valorEdificio"
									class="form-control align-right numberdecimal" type="text"
									value='<s:property value="getText('money.format',{nuevaLinea.valorEdificio})"></s:property>' />
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<s:if test='tramite.estado.toString() == "FINALIZADO"'>
		</fieldset>
	</s:if>
</form>

<div class="row">
	<s:include value="../grid/lineas-grid.jsp"></s:include>
</div>