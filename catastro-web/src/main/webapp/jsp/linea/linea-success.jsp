<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<script type="text/javascript">
	function closeResponseDialog() {
		goToTramites();
	}
</script>

<style type="text/css">
input[readonly].info {
	background-color: transparent;
	border: 1;
	font-size: 1em;
}
</style>

<div class="row alert alert-success">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<h3>
			<strong><s:actionmessage /></strong>
		</h3>
	</div>
</div>

<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="panel panel-info">
			<div class="panel-heading">
				<s:text name="tramite.label" />
			</div>
			<div class="panel-body">
				<div class="col-xs-6 col-sm-6 col-md-4 col-lg-3">
					<label for="tramite_id"><s:text name="tramite.id.label" /></label>
					<s:textfield id="tramite_id" class="form-control info"
						name="tramite.id" readonly="true"></s:textfield>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-4 col-lg-5">
					<label for="tramite_fecha"><s:text
							name="tramite.fecha.label" /></label> <input type="text"
						class="form-control info" id="tramite_fecha" readonly
						value="<s:date name='tramite.createdDate'/>"></input>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
					<label for="tramite_tipo"><s:text name="tramite.tipo.label" /></label>
					<s:textfield id="tramite_tipo" name="tramite.tipo.descripcion"
						class="form-control info" readonly="true" />
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<table class="table table-hover top-buffer">
			<thead>
				<tr>
					<th>Cuenta</th>
					<th>DV</th>
					<th>Partida</th>
					<th>Nomenclatura</th>
				</tr>
			</thead>
			<tbody>				
					<tr>
						<td><s:property value="cuentaOrigen.cuenta" /></td>
						<td><s:property value="cuentaOrigen.dv" /></td>
						<td><s:property value="cuentaOrigen.parcela.partida" /></td>
						<td><s:property value="cuentaOrigen.parcela.nomenclatura" /></td>
					</tr>
			</tbody>
		</table>
	</div>
</div>