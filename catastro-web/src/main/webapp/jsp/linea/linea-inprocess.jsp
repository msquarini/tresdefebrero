<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<!-- Tramite de gestion de ajuste de linea	
-->

<script type="text/javascript">
	function validarFormulario() {
		$('#ajuste-linea-form').bootstrapValidator('resetForm');
		$('#ajuste-linea-form').bootstrapValidator('validate');
		return $('#ajuste-linea-form').data('bootstrapValidator').isValid();
	}

	function grabarTramite() {
		if (validarFormulario()) {
			loader(true, $('#btn_save'));
			
			$.ajax({
				method : "POST",
				url : "catastro/linea_saveTramite.action",
				data : $('#ajuste-linea-form').serialize(),
				traditional : true,
				success : function(res) {
					loader(false, $('#btn_save'));
					showResponseModalDialog(true, res);
				},
				error : function(res, textStatus, jqXHR) {
					loader(false, $('#btn_save'));
					handleErrorMessages(res.responseJSON);
					showErrorModalDialog(false);
				}
			});
		}
	}

	function procesarTramite() {
		if (validarFormulario()) {
			loader(true, $('#btn_process'));
			$.ajax({
				method : "POST",
				url : "catastro/linea_process.action",
				data : $('#ajuste-linea-form').serialize(),
				global : false,
				traditional : true,
				success : function(res) {
					loader(false, $('#btn_process'));
					showResponseModalDialog(true, res);
				},
				error : function(res, textStatus, jqXHR) {
					loader(false, $('#btn_process'));
					handleErrorMessages(res.responseJSON);
					showErrorModalDialog(false);
				}
			});
		}
	};
</script>

<style type="text/css">
</style>

<s:include value="../common/tramite-header.jsp"></s:include>

<ul class="nav nav-pills"
	style="background-color: #eeeeee; border-bottom-style: ridge;">
	<li class="nav-header disabled"><a><s:text
				name="tramite.ajuste.label" /></a></li>
	<li class="active"><a data-toggle="tab" href="#cuenta-origen"><s:text
				name="cuenta.label" /></a></li>
	<li><a data-toggle="tab" href="#lineas"><s:text
				name="linea.label" /></a></li>

	<s:if test='tramite.estado.toString() != "FINALIZADO"'>
		<li class="pull-right" style="padding-top: 3px; padding-right: 3px">
			<button id="btn_save" class="btn btn-warning" type="button"
				onclick="grabarTramite()">
				<span class="fa fa-floppy-o"></span>&nbsp;&nbsp;
				<s:text name="dialog.label.grabar" />
				&nbsp;&nbsp;
			</button>
			<button id="btn_process" class="btn btn-danger" type="button"
				onclick="procesarTramite()">
				<span class="fa fa-wrench"></span>&nbsp;
				<s:text name="dialog.label.procesar" />
				&nbsp;
			</button>
		</li>
	</s:if>
</ul>

<div class="tab-content">
	<div id="cuenta-origen" class="tab-pane fade in active">
		<s:include value="../info/parcela-info.jsp"></s:include>
	</div>
	<div id="lineas" class="tab-pane fade">
		<s:include value="linea-content.jsp"></s:include>
	</div>

</div>

