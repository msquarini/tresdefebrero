<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<script type="text/javascript">
	$(document).off("cuentaSelectedEvent").on("cuentaSelectedEvent",
			function(event, data) {
				//alert("selN " + data.cuenta.id);
				$("#idCuentaOrigen").val(data.cuenta.id);
				
				$("#ratificar_btn").toggleClass('disabled', false);
			}).off("cuentaDeselectedEvent").on("cuentaDeselectedEvent",
			function(event, data) {
				//alert("Deselect " + data.cuenta.id);
				$("#idCuentaOrigen").val("-1");
				$("#ratificar_btn").toggleClass('disabled', true);
			});

	/* Configuracion del formulario, ya que se hace submit del mismo mediante javascript
	 * Se controla el mesanje de error y se muestra dialog generico de errores!!!! 
	 */
	var hash = '';
	$('#ratificar-init-form').ajaxForm({
		//target : '#generic-modal-body',
		target : '#container',
		success : function(res, textStatus, jqXHR) {
		},
		error : function(res, textStatus, jqXHR) {
			handleErrorMessages(res.responseJSON);
			if (res.status == '501') {
				hash = res.responseJSON.hash;
				showErrorModalDialog(true);
			} else {
				showErrorModalDialog(false);
			}
		}
	});
	
	function continuar() {
		$("#hash").val(hash);
		$('#ratificar-init-form').submit();
	}

	function init_submit() {
		$('#ratificar-init-form').submit();
	};
</script>
<style type="text/css">
</style>

<div class="row">
	<s:include value="../cuenta_search.jsp"></s:include>
</div>

<div class="row">
	<s:form id="ratificar-init-form" action="ratificacion_init"
		namespace="/catastro" theme="bootstrap">
		<s:hidden id="idCuentaOrigen" name="idCuentaOrigen"></s:hidden>
		<s:hidden id="hash" name="hash"></s:hidden>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

			<!-- Para poder manejar la respuesta del llamado ajax!!! -->
			<sj:a id="ratificar_btn" cssClass="btn btn-primary disabled"
				value="Ratificar" onclick="init_submit();"><s:text name="ratificacion.label.iniciar" /></sj:a>

		</div>
	</s:form>
</div>