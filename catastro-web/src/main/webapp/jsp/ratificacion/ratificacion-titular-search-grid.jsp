<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<!-- Grilla de resultado de busqueda de titulares (USO GENERICO) -->

<style>
.cgb-header-name {
	width: 50%;
}
</style>
<script src="js/jquery.bootgrid.js"></script>
<link rel="stylesheet" href="css/jquery.bootgrid.min.css">

<script type="text/javascript">
	
</script>
<div class="row top-buffer">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<table id="titular-grid-data"
			class="table table-condensed table-hover table-striped">
			<thead>
				<tr>
					<th data-column-id="id" data-identifier="true" data-type="numeric"
						data-visible="false">idTitular</th>
					<th data-column-id="apellido" data-type="string"
						data-visible="true">Apellido</th>
					<th data-column-id="nombre" data-type="string" data-visible="true">Nombre</th>
					<th data-column-id="documento" data-type="string">Documento</th>
					<th data-column-id="cuitcuil">Cuit/Cuil</th>
				</tr>
			</thead>

		</table>
	</div>
</div>
<script>
	$(document)
			.ready(
					function() {
						$("#titular-grid-data")
								.bootgrid(
										{
											ajax : true,
											navigation : 2,
											rowSelect : true,
											selection : true,
											multiSelect : false,
											search : true,
											columnSelection : false,
											templates : {
												search : ""
											},
											labels : {
												noResults : "No hay titulares",
												loading : "Cargando información...",
												infos : "Mostrando {{ctx.start}} a {{ctx.end}} de {{ctx.total}} filas"
											},
											url : "catastro/titularratificacion_getData.action",
											formatters : {}

										}).on(
										"selected.rs.jquery.bootgrid",
										function(e, rows) {
											$(document).trigger(
													'titularSelectedEvent',
													rows[0]);
											$('#idTitular').val(
													rows[0].id);
										}).on(
										"deselected.rs.jquery.bootgrid",
										function(e, rows) {
											$(document).trigger(
													'titularDeselectedEvent',
													rows[0]);
											$('#idTitular').val("-1");
										});
					});
</script>
