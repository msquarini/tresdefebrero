<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>


<link rel="stylesheet" href="css/bootstrap-multiselect.css" type="text/css">
<script type="text/javascript" src="js/bootstrap-multiselect.js"></script>

<script type="text/javascript">
	$('.selectpicker').selectpicker({
		width : 'fit'
	});

	$('#destinoMunicipal').checkboxpicker({
		offLabel : 'No',
		onLabel : 'Si'
	}).on(
			'change',
			function() {
				$('#destino').prop('disabled',
						!$('#destinoMunicipal').prop('checked'));

				if ($(this).is(':checked')) {
					$(this).prop('value', 'true');
				} else {
					$(this).prop('value', 'false');
				}
			});

	
	
	function cambioTipoInscripcion() {
		
		
		if ($('#inscripcion').val() == 'F' ) {
			$('#div_anio').removeClass('hidden');
		} else {
			if(!$('#div_anio').hasClass("hidden"))
				$('#div_anio').addClass('hidden');
		}
	}

	/* Submit del formulario previa validacion */
	function dialog_submit_update_uf() {
		$('#ratificacion-parcela-update-form').bootstrapValidator('resetForm');
		$('#ratificacion-parcela-update-form').bootstrapValidator('validate');

		if ($('#ratificacion-parcela-update-form').data('bootstrapValidator').isValid()) {
			//$('#ratificacion-parcela-update-form').submit();
			grabarEdicionUF();
		}
		else
			$("a[href='#cuenta-origen-uf']").click();
	};

	/* Configuracion del formulario, ya que se hace submit del mismo mediante javascript */
	/*$('#ratificacion-parcela-update-form').ajaxForm({
		//target : '#generic-modal-body',
		target : '#generic-modal-response-body',
		success : function(res, textStatus, jqXHR) {
			// Se cierra el dialogo principal y se muestra el dialogo con la respuesta.
			$('#generic-dialog').modal('hide');
			// Refrescar la grilla de parcelas
			$("#parcela-grid-data").bootgrid('reload');
			// Mostrar dialogo de respuesta
			//showResponseModalDialog(false);
		},
		error : function(res, textStatus, jqXHR) {
			$("#error-parcela").text(res.responseJSON.actionErrors[0]);
		}
	});*/

	/* Configuracion de validador del formulario */
	$(document).ready(function() {

		/*Actualizar el combo de seleccion multiple de unidades funcionales origen*/
		var id_selected;
	    <s:iterator value="parcelaEdit.parcelasOrigen">
	    	id_selected = '<s:property value="id"/>';
	    	$("option[value='"+id_selected+"']").attr("selected","selected");
	    </s:iterator>
	    
	    
	    
	    $('#unidades_funcionales_origen').multiselect();
		
		$('#inscripcion').val($('#hidden_dominio').val());
		cambioTipoInscripcion();

		if ($('#destinoMunicipal').prop('value') == 'true') {
			$('#destinoMunicipal').prop('checked', 'true');
		}

		$('input.number').number(true, 0, '', '');
		$('input.numberdecimal').number(true, 2, '.', '');
		/* var cleaveNumeral = new Cleave('.input-numeral', {
			numeral : true,
			numeralThousandsGroupStyle: 'thousand',
			numeralDecimalMark : ',',
			numeralDecimalScale : 2,
			delimiter : '.'
		});
		 */
		$('#ratificacion-parcela-update-form').bootstrapValidator({
			framework : 'bootstrap',
			excluded : [ ':disabled' ],
			feedbackIcons : {
				//valid : 'glyphicon glyphicon-ok',
				invalid : 'glyphicon glyphicon-remove',
				validating : 'glyphicon glyphicon-refresh'
			},
			message : 'Datos invalidos',
			/* submitHandler: function(validator, form, submitButton) {
					
			}, */
			err : {
				// You can set it to popover
				// The message then will be shown in Bootstrap popover
				container : 'tooltip'
			},
			fields : {
				'parcelaEdit.nomenclatura.parcela' : {
					validators : {
						notEmpty : {
							message : 'Obligatorio'
						}
					}
				},
				'parcelaEdit.partida' : {
					validators : {
						notEmpty : {
							message : 'Partida Obligatorio'
						},
						integer : {
							message : 'Debe ser un valor n�merico de 6 d�gitos'
						}
					}
				},
				'parcelaEdit.dominio.tipo' : {
					validators : {
						notEmpty : {
							message : 'Obligatorio'
						}
					}
				},
				'parcelaEdit.dominio.numero' : {
					validators : {
						notEmpty : {
							message : 'Obligatorio'
						}
					}
				},
				'parcelaEdit.lineaVigente.superficieTerreno' : {
					validators : {
						notEmpty : {
							message : 'Obligatorio'
						},
						greaterThan : {
							message : 'El valor debe ser mayor a 0',
							value : 0,
							inclusive : false
						}
					}
				},
				'parcelaEdit.lineaVigente.superficieCubierta' : {
					validators : {
						notEmpty : {
							message : 'Obligatorio'
						}
					}
				},
				'parcelaEdit.lineaVigente.superficieSemi' : {
					validators : {
						notEmpty : {
							message : 'Obligatorio'
						}
					}
				},
				'parcelaEdit.lineaVigente.valorTierra' : {
					validators : {
						notEmpty : {
							message : 'Obligatorio'
						},
						greaterThan : {
							message : 'El valor debe ser mayor a 0',
							value : 0,
							inclusive : false
						}
					}
				},
				'parcelaEdit.lineaVigente.valorComun' : {
					validators : {
						notEmpty : {
							message : 'Obligatorio'
						}
					}
				},
				'parcelaEdit.lineaVigente.valorEdificio' : {
					validators : {
						notEmpty : {
							message : 'Obligatorio'
						},
						greaterThan : {
							message : 'El valor debe ser mayor a 0',
							value : 0,
							inclusive : false
						}
					}
				},
				'parcelaEdit.lineaVigente.fecha' : {
					validators : {
						notEmpty : {
							message : 'Obligatorio'
						}
					}
				},
				'parcelaEdit.domicilioInmueble.calle' : {
					validators : {
						notEmpty : {
							message : 'Obligatorio'
						}
					}
				},
				'parcelaEdit.domicilioInmueble.numero' : {
					validators : {
						notEmpty : {
							message : 'Obligatorio'
						}
					}
				},
				'parcelaEdit.destino' : {
					validators : {
						notEmpty : {
							message : 'Obligatorio'
						}
					}
				}
			}
		}).on('error.field.bv', function(e, data) {

			// $(e.target)  --> The form instance
			// $(e.target).data('bootstrapValidator')
			//              --> The BootstrapValidator instance

			// data.field   --> The field name
			// data.element --> The new field element
			// data.options --> The new field options

			// Get the tab that contains the first invalid field
			if (data.field.indexOf('domicilio') !== -1) {
				var $tabPane = data.element.parents('.panel-body');
				$tabPane.collapse('show');

			}

		});
	});

	function updateValuacion() {
		var v = parseFloat($('#valor_tierra').val())
				+ parseFloat($('#valor_comun').val())
				+ parseFloat($('#valor_edificio').val());
		$('#valuacion').text('$ ' + $.number(v, 2, ',', '.'));
		//$('#valuacion').text('$ ' + v.toFixed(2).replace('.', ','));
		//	$('#valuacion').text(v.toFixed(2));
		//  $('#valuacion').number(true, 2, ',', '.');
	}
</script>
<style type="text/css">
.padding-0 {
	padding-right: 1;
	padding-left: 0;
}

.numberdecimal {
	text-align: right;
}

.has-feedback .form-control {
	top: 0;
	/* Para utilizar todo el area editable */
	padding-right: 10px;
}

.form-control-feedback {
	/* Para correr el icon a la derecha */
	right: -22px;
}

/*  #ratificacion-parcela-form .has-feedback .input-group .form-control-feedback {
    top: 0;
    padding-right: 20;

} */
.mano {
	cursor: pointer;
}

#div_uf_origen > label, #div_uf_origen > span
{
	width:100% !important;
}

.multiselect-native-select > .btn-group
{
	margin-top: -2px;
}
</style>

<s:form id="ratificacion-parcela-update-form" action="ratificacion_saveuf"
	cssClass="form-vertical" namespace="/catastro" theme="bootstrap">
	<fieldset>
		<%-- 		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<s:actionerror />
				<s:actionmessage />
			</div>
		</div> --%>
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<p id="error-parcela" class="bg-danger"></p>
			</div>
		</div>
		<div class="row  ">
			<div class="col-xs-6 col-sm-6 col-md-1 col-lg-1">
				<label for="circunscripcion"><s:text name="circ.short.label" /></label>
				<s:textfield id="circunscripcion"
					class="form-control input-sm number"
					name="parcelaEdit.nomenclatura.circunscripcion" maxlength="3"
					readonly="true" 
					/>
			</div>
			<div class="col-xs-6 col-sm-6 col-md-1 col-lg-1">
				<label for="seccion"><s:text name="sec.short.label" /></label>
				<s:textfield id="seccion" class="form-control input-sm"
					maxlength="3" name="parcelaEdit.nomenclatura.seccion"
					readonly="true" 
					/>
			</div>
			<div class="col-xs-6 col-sm-6 col-md-1 col-lg-1">
				<label for="fraccion"><s:text name="fr.short.label" /></label>
				<s:textfield id="fraccion" class="form-control input-sm number"
					readonly="true" maxlength="3"
					name="parcelaEdit.nomenclatura.fraccion" 
					/>
			</div>
			<div class="col-xs-6 col-sm-6 col-md-1 col-lg-1">
				<label for="fraccionLetra"><s:text name="frl.short.label" /></label>
				<s:textfield id="fraccionLetra" class="form-control input-sm"
					readonly="true" maxlength="3"
					name="parcelaEdit.nomenclatura.fraccionLetra" 
					/>
			</div>
			<div class="col-xs-6 col-sm-6 col-md-1 col-lg-1">
				<label for="manzana"><s:text name="mz.short.label" /></label>
				<s:textfield id="manzana" class="form-control input-sm number"
					readonly="true" maxlength="3"
					name="parcelaEdit.nomenclatura.manzana" 
					/>
			</div>
			<div class="col-xs-6 col-sm-6 col-md-1 col-lg-1">
				<label for="manzanaLetra"><s:text name="mzl.short.label" /></label>
				<s:textfield id="manzanaLetra" class="form-control input-sm"
					readonly="true" maxlength="3"
					name="parcelaEdit.nomenclatura.manzanaLetra" 
					/>
			</div>
			<div class="col-xs-6 col-sm-6 col-md-1 col-lg-1">
				<label for="parcela"><s:text name="pc.short.label" /></label>
				<s:textfield id="parcela" class="form-control input-sm number"
					readonly="true" maxlength="3" name="parcelaEdit.nomenclatura.parcela" 
					/>
			</div>
			<div class="col-xs-6 col-sm-6 col-md-1 col-lg-1">
				<label for="parcelaLetra"><s:text name="pcl.short.label" /></label>
				<s:textfield id="parcelaLetra" class="form-control input-sm"
					readonly="true" maxlength="3" name="parcelaEdit.nomenclatura.parcelaLetra"
					/>
			</div>
			<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
				<div id="div_uf">
					<label for="unidadFuncional"><s:text name="uf.short.label" /></label>
					<s:textfield id="unidadFuncional" class="form-control input-sm"
						readonly="true" maxlength="6" name="parcelaEdit.nomenclatura.unidadFuncional"
					/>
				</div>
			</div>
			
			
			<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
				<div id="div_uf_origen">
					<label for="unidades_funcionales_origen"><s:text name="uf_origen.short.label" /></label>
					<select id="unidades_funcionales_origen" name="unidades_funcionales_origen" multiple="multiple">
					    <s:iterator value="parcelasaRemover">
						    <option value="<s:property value="origen.id" />"><s:property value="nomenclatura.unidadFuncional" /></option>
						</s:iterator>
					</select>

				</div>
			</div>
			
			
		</div>

		<!-- Partida | Zona | Categoria -->
		<div class="row">

			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">

				<div class="panel panel-default">
					<div class="panel-heading">
						<strong><s:text name="dominio.panel.label" /></strong>
					</div>
					<div class="panel-body">

							<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
							    <input type="hidden" id="hidden_dominio" value="<s:property value='parcelaEdit.dominio.tipo'/>"/>
								<label for="inscripcion"><s:text
										name="dominio.tipo.label" /></label> 
								<select id="inscripcion"
									name="parcelaEdit.dominio.tipo" class="form-control" 
									onchange="cambioTipoInscripcion()">								
									<option value="M"><s:text
											name="matricula.label" /></option>
									<option value="F"><s:text name="folio.label" /></option>
								</select>
								<%-- <s:textfield id="inscripcion" name="parcelaEdit.dominio.tipo" /> --%>
							</div>

							<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
								<label for="numero"><s:text name="dominio.numero.label" /></label>
								<s:textfield id="numero" name="parcelaEdit.dominio.numero"
									class="form-control number" maxlength="6" />
							</div>
								<div id="div_anio" class="col-xs-6 col-sm-6 col-md-4 col-lg-4 hidden">
		
									<label for="anio"><s:text name="dominio.anio.label" /></label>
									<div class="input-group date" data-provide="datepicker"
										data-date-format="yyyy" data-date-autoclose="true"
										data-date-view-mode="years" data-date-min-view-mode="years"
										data-date-end-date="y" data-date-orientation="bottom"
										data-date-language="es" data-date-today-btn="true"
										data-date-today-highlight="true">
										<input id="anio" type="text" class="form-control"
											name="parcelaEdit.dominio.anio"
											value="<s:property value='parcelaEdit.dominio.anio'/>">
										<div class="input-group-addon">
											<span class="glyphicon glyphicon-calendar"></span>
										</div>
									</div>
								</div>
						</div>
					</div>
			</div>

			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">

				<div class="panel panel-default">
					<div class="panel-heading">
						<strong><s:text name="parcela.panel.label" /></strong>
					</div>
					<div class="panel-body">

						<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
							<label for="partida"><s:text name="partida.label" /></label>
							<s:textfield id="partida" class="form-control number"
								maxlength="6" name="parcelaEdit.partida" />
						</div>
						<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
							<label for="categoria"><s:text name="categoria.label" /></label>
							<select id="categoria" name="parcelaEdit.lineaVigente.categoria"
								class="selectpicker form-control">
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
								<option value="5">5</option>
								<option value="6">6</option>
								<option value="7">7</option>
								<option value="8">8</option>
							</select>
						</div>

						<div class="col-xs-6 col-sm-6 col-md-5 col-lg-5">
							<label for="tf_vigencia"><s:text name="vigencia.label" /></label>
							<div class="form-group">
								<div class="input-group date" data-provide="datepicker"
									data-date-format="dd/mm/yyyy" data-date-autoclose="true"
									data-date-end-date="0d" data-date-orientation="bottom"
									data-date-language="es" data-date-today-btn="true"
									data-date-today-highlight="true">
									<input id="tf_vigencia" type="text" class="form-control"
										name="parcelaEdit.lineaVigente.fecha"
										value="<s:property value='getText("format.date",{parcelaEdit.lineaVigente.fecha})'/>">
									<div class="input-group-addon">
										<span class="glyphicon glyphicon-calendar"></span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>

		<!--  Superificies -->
		<div class="row">
			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 ">

				<div class="panel panel-default">
					<div class="panel-heading">
						<strong><s:text name="superficies.panel.label" /></strong>
					</div>
					<div class="panel-body">
						<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4"
							style="padding-left: 2px; padding-right: 2px;">
							<div class="form-group">
								<label for="sup_terreno"><s:text name="terreno.label" /></label>
								<div class="input-group">
									<input type="text" id="sup_terreno"
										class="form-control numberdecimal input-numeral" maxlength="8"
										name="parcelaEdit.lineaVigente.superficieTerreno"
										value="<s:property value='{getText("number.format" ,{parcelaEdit.lineaVigente.superficieTerreno})}'/>">
									<span class="input-group-addon">m<sup>2</sup></span>
								</div>
							</div>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4"
							style="padding-left: 2px; padding-right: 2px;">
							<div class="form-group">
								<label for="sup_cubierta"><s:text name="cubierta.label" /></label>
								<div class="input-group">
									<input type="text" id="sup_cubierta"
										class="form-control numberdecimal input-numeral" maxlength="8"
										name="parcelaEdit.lineaVigente.superficieCubierta"
										value="<s:property value='{getText("number.format" ,{parcelaEdit.lineaVigente.superficieCubierta})}'/>">
									<span class="input-group-addon">m<sup>2</sup></span>
								</div>
							</div>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4"
							style="padding-left: 2px; padding-right: 2px;">
							<div class="form-group">
								<label for="sup_semi"><s:text name="semi.label" /></label>
								<div class="input-group">
									<input type="text" id="sup_semi"
										class="form-control numberdecimal input-numeral" maxlength="8"
										name="parcelaEdit.lineaVigente.superficieSemi"
										value="<s:property value='{getText("number.format" ,{parcelaEdit.lineaVigente.superficieSemi})}'/>">
									<span class="input-group-addon">m<sup>2</sup></span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
				<!-- Valores -->
				<div class="panel panel-default">
					<div class="panel-heading clearfix">
						<div class="pull-left">
							<strong><s:text name="valuacion.panel.label" /></strong>
						</div>
						<div class="pull-right" style="padding-right: 15px;">
							<span id="valuacion" class="label label-info"
								style="font-size: 100%">$ 0,00</span>
						</div>
					</div>
					<div class="panel-body">
						<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4"
							style="padding-left: 2px; padding-right: 2px;">
							<div class="form-group">
								<label for="valor_tierra"><s:text name="tierra.label" /></label>
								<div class="input-group">
									<span class="input-group-addon">$</span> <input type="text"
										id="valor_tierra" onchange="updateValuacion()"
										class="form-control numberdecimal input-numeral" maxlength="10"
										name="parcelaEdit.lineaVigente.valorTierra"
										value="<s:property value='{getText("number.format" ,{parcelaEdit.lineaVigente.valorTierra})}'/>">
								</div>
							</div>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4 "
							style="padding-left: 2px; padding-right: 2px;">
							<div class="form-group">
								<label for="valor_comun"><s:text name="comun.label" /></label>
								<div class="input-group">
									<span class="input-group-addon">$</span> <input type="text"
										id="valor_comun" onchange="updateValuacion()"
										class="form-control numberdecimal input-numeral" maxlength="10"
										name="parcelaEdit.lineaVigente.valorComun"
										value="<s:property value='{getText("number.format" ,{parcelaEdit.lineaVigente.valorComun})}'/>">
								</div>
							</div>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4 "
							style="padding-left: 2px; padding-right: 2px;">
							<div class="form-group">
								<label for="valor_edificio"><s:text
										name="edificio.label" /></label>
								<div class="input-group">
									<span class="input-group-addon">$</span> <input type="text"
										id="valor_edificio" onchange="updateValuacion()"
										class="form-control numberdecimal input-numeral" maxlength="10"
										name="parcelaEdit.lineaVigente.valorEdificio"
										value="<s:property value='{getText("number.format" ,{parcelaEdit.lineaVigente.valorEdificio})}'/>">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Domicilio Inmueble -->
		<div class="panel-group">
			<div class="panel panel-default">
				<div class="panel-heading mano" data-toggle="collapse"
					href="#panel_domicilio_inmueble">
					<strong><s:text name="domicilio.inmueble.panel.label" /></strong>
				</div>
				<div class="panel-body panel-collapse" id="panel_domicilio_inmueble">
					<div class="col-xs-6 col-sm-6 col-md-5 col-lg-4">
						<label for="di_calle"><s:text name="domicilio.calle.label" /></label>
						<s:textfield id="di_calle" class="form-control input-sm"
							name="parcelaEdit.domicilioInmueble.calle" />
					</div>
					<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
						<label for="di_numero"><s:text name="domicilio.nro.label" /></label>
						<s:textfield id="di_numero" class="form-control input-sm"
							name="parcelaEdit.domicilioInmueble.numero" />
					</div>
					<div class="col-xs-6 col-sm-6 col-md-1 col-lg-1">
						<label for="di_piso"><s:text name="domicilio.piso.label" /></label>
						<s:textfield id="di_piso" class="form-control input-sm"
							name="parcelaEdit.domicilioInmueble.piso" />
					</div>
					<div class="col-xs-6 col-sm-6 col-md-1 col-lg-1">
						<label for="di_depto"><s:text name="domicilio.depto.label" /></label>
						<s:textfield id="di_depto" class="form-control input-sm"
							name="parcelaEdit.domicilioInmueble.departamento" />
					</div>
					<div class="col-xs-6 col-sm-6 col-md-1 col-lg-2">
						<label for="di_cp"><s:text name="domicilio.cp.label" /></label>
						<s:textfield id="di_cp" class="form-control input-sm"
							name="parcelaEdit.domicilioInmueble.codigoPostal" />
					</div>
					<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
						<label for="di_barrio"><s:text
								name="domicilio.barrio.label" /></label>
						<s:select id="di_barrio" list="#session.barrios" listKey="id"
							listValue="nombre" class="form-control input-sm"
							name="parcelaEdit.domicilioInmueble.barrio.id"></s:select>
					</div>
				</div>
			</div>

		</div>
		

		<div class="row">
			<div class="col-xs-6 col-sm-6 col-md-3 col-lg-2">
				<label for="destinoMunicipal"><s:text
						name="dominiomunicipal.label" /></label> <input id="destinoMunicipal"
					data-group-cls="btn-group-sm" type="checkbox"
					name="parcelaEdit.dominioMunicipal"
					value="<s:property value="parcelaEdit.dominioMunicipal" />" />
			</div>
			<div id="div_destino" class="col-xs-6 col-sm-6 col-md-10 col-lg-10">
				<label for="destino"><s:text name="destino.label" /></label>
				<s:select id="destino" list="#session.destinos" listKey="destino"
					disabled="true" listValue="destino" name="parcelaEdit.destino"
					headerKey="" headerValue="Seleccione"></s:select>
			</div>
		</div>

		<!-- Observacion -->
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<label for="observacion"><s:text name="observacion.label" /></label>
				<s:textfield id="observacion" name="parcelaEdit.observacion" />
			</div>
		</div>
		
	</fieldset>


</s:form>



