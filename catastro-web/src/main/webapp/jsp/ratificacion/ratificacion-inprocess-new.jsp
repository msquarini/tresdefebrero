<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<style type="text/css">
</style>
<script>
function grabarNuevaUF()
{
	loader(true, $('#btn_save_new_uf'));
	$('#ratificacion-parcela-new-form').submit();
	
}

$('#ratificacion-parcela-new-form').ajaxForm({
	//target : '#generic-modal-body',
	target : '#container',
	success : function(res, textStatus, jqXHR) {
		loader(false, $('#btn_save_new_uf'));
	},
	error : function(res, textStatus, jqXHR) {
		loader(false, $('#btn_save_new_uf'));
		handleErrorMessages(res.responseJSON);
		if (res.status == '501') {
			hash = res.responseJSON.hash;
			showErrorModalDialog(true);
		} else {
			showErrorModalDialog(false);
		}
	}
});




</script>


<ul class="nav nav-pills"
	style="background-color: #eeeeee; border-bottom-style: ridge;">
	
	<s:url var="cancelNewURL" action="ratificacion_backToNew" namespace="/catastro" />
	
	<li class="nav-header disabled"><a><s:text
				name="tramite.titular.label" /></a></li>
	<li class="active"><a data-toggle="tab" href="#cuenta-origen-uf"><s:text
				name="cuenta.label" /></a></li>
	<li><a data-toggle="tab" href="#titulares-uf-ratificacion"><s:text
				name="titulares.label" /></a></li>
	<li><a data-toggle="tab" href="#responsables-uf-ratificacion"><s:text
				name="responsables.label" /></a></li>
				
	
	<li class="pull-right" style="padding-top: 3px; padding-right: 3px">
			<div class="btn-group">
				
				<sj:a id="btn_save_new_uf" cssClass="btn btn-danger" value="Grabar" onclick="dialog_submit_new_uf();"><s:text name="dialog.label.grabaruf" /></sj:a>
                    
				<sj:a targets="container" href="%{cancelNewURL}" class="btn btn-primary">
						<span class="fa fa-undo"></span>&nbsp;<s:text name="dialog.label.cancelar" />&nbsp;
				</sj:a>
			</div>
	</li>

</ul>

<div class="tab-content" id="ratificacion-update-uf-tabs">
	<div id="cuenta-origen-uf" class="tab-pane fade in active">
		<s:include value="./ratificacion-uf-new-dialog-content.jsp"></s:include>
	</div>
	<div id="titulares-uf-ratificacion" class="tab-pane fade">
		<s:include value="./ratificacion-titular-abm-grid.jsp"></s:include>
	</div>
	<div id="responsables-uf-ratificacion" class="tab-pane fade">
		<s:include value="./ratificacion-responsable-abm-grid.jsp"></s:include>
	</div>
</div>



