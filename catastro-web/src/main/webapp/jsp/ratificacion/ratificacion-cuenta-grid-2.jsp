<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<style>
.cgb-header-name {
	width: 30%;
}

/* Colorear HEADER */
.column .text { color: #f00 !important; }

/*  Estilo celda */
.cell { color: #f00 !important; font-weight: bold; }

.right_buttons
{
	float:right;
}

.right_buttons form
{
	float:left;
	padding-right: 2px;
}

#add-uf-update-form
{
	float:left;
}

.header_message
{
	font-size:1.5em;
	font-weight: bold;
	font-weight: bolder;
}
.single_message
{
	font-size:1.1em;
}

            
</style>
<script src="js/jquery.bootgrid.js"></script>
<link rel="stylesheet" href="css/jquery.bootgrid.min.css">
<div class="row">
	<div class="panel panel-default">
		<div class="panel-heading">
				<span class="header_message"><s:text name="ratificacion.label.common_message_header"/></span><br>
				<span class="single_message"><s:text name="ratificacion.label.uf_to_update"/></span>
		</div>
		<div class="panel-body">
	
			<table id="ratificacion-cuenta-grid-data-2"
				class="table table-condensed table-hover table-striped">
				<thead>
					<tr>
						<th data-column-id="id" data-identifier="true" data-type="numeric"
							data-visible="false">IdParcela</th>
						<th data-column-id="idCuenta" data-type="numeric"
							data-visible="false" data-formatter="idCuenta"
							data-sortable="false">IdCuenta</th>
						<th data-column-id="cuenta" data-formatter="nested" data-width="8%">Cuenta</th>
						<th data-column-id="dv" data-formatter="nested" data-sortable="false"						
							data-width="4%">DV</th>
						<th data-column-id="partida" data-formatter="partida"
							data-width="7%">Partida</th>
						<th data-column-id="nomenclatura" data-formatter="nomenclatura"
							data-sortable="false" data-width="36%">Nomenclatura</th>
						<th data-column-id="activa" data-formatter="activa" data-align="center" data-header-align="center"
							data-sortable="false" data-width="8%">Activa</th>
	<!-- 					<th data-column-id="createdDate" data-formatter="nesteddate"
							data-sortable="false">Fecha Creacion</th>
						<th data-column-id="createdBy" data-formatter="nested"
							data-sortable="false">Creado Por</th> -->
						<th data-column-id="modifiedDate" data-formatter="nesteddate"
							data-sortable="false">Ultima Modif.</th>
						<th data-column-id="modifiedBy" data-formatter="nested"
							data-sortable="false">Modif. Por</th>
					</tr>
				</thead>
	
			</table>
			<input type="hidden" id="idSelectedCuenta" />
		</div>
		
		<div class="panel-footer">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<s:form id="add-uf-update-form" action="ratificacion_addParcelaTORatificacionUpdate"
						namespace="/catastro" theme="bootstrap">
					<s:hidden id="idCuentaOrigen" name="idCuentaOrigen"></s:hidden>
					<s:hidden id="hash" name="hash"></s:hidden>			
					<sj:a id="ratificacion_add_update_btn" cssClass="btn btn-primary disabled"
						value="Agregar" formIds="add-uf-update-form" targets="container"><s:text name="ratificacion.label.editar" /></sj:a>
				</s:form>
			
				<div class="right_buttons">
				
					<s:form id="ratificar-back-init-form2" action="ratificacion_backToRemove"
							namespace="/catastro" theme="bootstrap">
							<sj:a id="ratificar_back_btn2" cssClass="btn btn-primary"
								value="Atras" formIds="ratificar-back-init-form2" targets="container"><s:text name="ratificacion.label.volver" /></sj:a>
					</s:form>
				
				
					<s:form id="ratificar-update-init-form" action="ratificacion_init_update"
						namespace="/catastro" theme="bootstrap">
						<sj:a id="ratificar_update_btn" cssClass="btn btn-primary"
							value="Actualizar" formIds="ratificar-update-init-form" targets="container"><s:text name="ratificacion.label.continuar" /></sj:a>
					</s:form>
				</div>
			</div>
			<div class="clearfix"></div>
			<!--  Usado para que los botones queden dentro del footer del panel -->
		</div>
	</div>
	
</div>
<script>
	$(document)
			.ready(
					function() {
						$("#ratificacion-cuenta-grid-data-2")
								.bootgrid(
										{
											ajax : true,
											navigation : 2,
											rowSelect : true,
											selection : true,
											multiSelect : false,
											search : false,
											columnSelection : false,
											templates : {
												search : ""
											},
											requestHandler : function(request) {
												for ( var key in request.sort) {
													request.sortColumn = key;
													request.sortOrder = request.sort[key];
												}
												//return JSON.stringify(request);
												return request;
											},
											labels : {
												noResults : "No hay parcelas",
												loading : "Cargando información...",
												infos : "Mostrando {{ctx.start}} a {{ctx.end}} de {{ctx.total}} filas"
											},
											statusMapping : {												
											    0 : "text-danger",
												1 : "warning",
												2 : "danger"
											},
											url : "catastro/ratificacion_getData.action",
											formatters : {
												nested : function(column, row) {
													return row.cuenta[column.id];
												},
												nesteddate : function(column,
														row) {
													value = row.cuenta[column.id];
													if (!value) {
														return "";
													}
													return moment(value)
															.format(
																	"DD/MM/YYYY");
												},
												idCuenta : function(column, row) {
													return row.cuenta.id;
												},
												partida : function(column, row) {
													return row.partida;
												},
												nomenclatura : function(column,
														row) {
													return fmt_nomenclatura(row);
												},
												activa : function(column, row) {
													return fmt_boolean(row.cuenta.activa);
												}
											}
										}).on("selected.rs.jquery.bootgrid",
										function(e, rows) {

										}).on(
										"selected.rs.jquery.bootgrid",
										function(e, rows) {
											$(document).trigger(
													'cuentaSelectedEvent',
													rows[0]);
											$('#idSelectedCuenta').val(
													rows[0].cuenta.id);
										}).on(
										"deselected.rs.jquery.bootgrid",
										function(e, rows) {
											$(document).trigger(
													'cuentaDeselectedEvent',
													rows[0]);
											$('#idSelectedCuenta').val("-1");
										});
					});
	
	var rows;
	
	$(document).off("cuentaSelectedEvent").on("cuentaSelectedEvent",
			function(event, data) {
				//alert("selN " + data.cuenta.id);
				$("#idCuentaOrigen").val(data.cuenta.id);
				
				$("#ratificacion_add_update_btn").toggleClass('disabled', false);
			}).off("cuentaDeselectedEvent").on("cuentaDeselectedEvent",
			function(event, data) {
				//alert("Deselect " + data.cuenta.id);
				$("#idCuentaOrigen").val("-1");
				$("#ratificacion_add_update_btn").toggleClass('disabled', true);
			});


	var hash = '';
	$('#ratificar-init-form').ajaxForm({
		//target : '#generic-modal-body',
		target : '#container',
		success : function(res, textStatus, jqXHR) {
		},
		error : function(res, textStatus, jqXHR) {
			handleErrorMessages(res.responseJSON);
			if (res.status == '501') {
				hash = res.responseJSON.hash;
				showErrorModalDialog(true);
			} else {
				showErrorModalDialog(false);
			}
		}
	});

</script>