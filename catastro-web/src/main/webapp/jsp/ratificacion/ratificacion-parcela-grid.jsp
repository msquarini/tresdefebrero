<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<%-- <link rel="stylesheet" href="css/jquery.bootgrid.min.css">
<script src="js/jquery.bootgrid.js"></script> --%>

<script type="text/javascript">

	function cambioTipoRatificacion() {
		if ($('#tipoRatificacion').val() < 3) {
			$('#origen_numero').removeClass('hidden');
			$('#origen_anio').removeClass('hidden');
		} else {
			$('#origen_numero').addClass('hidden');
			$('#origen_anio').addClass('hidden');
		}
	}

	/* Ir a dialogo de nueva parcela
	 * Activa el boton de grabar en el dialogo generico */
	function addCuenta() {
		$.ajax({
			method : "POST",
			url : "catastro/ratificacion_inputNuevaParcela.action",
			data : {},
			traditional : true,
			success : function(res) {
				loader(false, null);
				var btn_array = [ "submit-dialog-button" ];
				showModalDialog("Informacion de Parcela", res, btn_array);
			},
			error : function(res, textStatus, jqXHR) {
				loader(false, null);
				// Levantar dialogo con mensaje de error!!!!!!!!!!!!!
				handleErrorMessages(res.responseJSON);
				showErrorModalDialog(false);
			}
		});
	}

	/* Id (partida) de la fila elegida y boolean indicando si es edit o delete */
	function sd_accion_submit(id, edit) {
		//alert("accion " + id + " - Edit? " + edit);
		$('#partida').val(id);
		if (edit) {
			$('#grid-parcela-form').attr('action',
					'catastro\/ratificacion_editParcela.action');

			// Activo boton de Grabar en el dialogo de edicion de parcela
			$("#submit-dialog-button").show();
		} else {
			$('#grid-parcela-form').attr('action',
					'catastro\/ratificacion_deleteParcela.action');
		}
		$('#grid-parcela-form').submit();
	};

	/*  Formulario para editar y borrar parcelas de la grilla de nuevas parcelas */
	$('#grid-parcela-form').ajaxForm({
		target : '#generic-modal-body',
		success : function(res, textStatus, jqXHR) {
			$("#generic-data-label").text("Informacion de Parcela");
			$('#generic-dialog').modal();
			//$('#parcela-grid-data').bootgrid('reload');
		},
		error : function(res, textStatus, jqXHR) {
			alert(res);
		}
	});

	/* Configuracion del formulario, ya que se hace submit del mismo mediante javascript */
	/* PROCESAMIENTO!!!*/
	$('#ratificacion-form').ajaxForm({
		target : '#generic-modal-response-body',
		success : function(res, textStatus, jqXHR) {
			loader(false, $('#btn_ratificacion_process'));
			showResponseModalDialog(true);
		},
		error : function(res, textStatus, jqXHR) {
			loader(false, $('#btn_ratificacion_process'));
			// Levantar dialogo con mensaje de error!!!!!!!!!!!!!
			handleErrorMessages(res.responseJSON);
			showErrorModalDialog(false);
		}
	});

	$(document)
			.ready(
					function() {
						$("#tipoRatificacion").val(<s:property value='tramite.codigo'/>);
						
						$('#ratificacion-form')
								.bootstrapValidator(
										{
											framework : 'bootstrap',
											//excluded : [ ':disabled', ':hidden' ], // Se incluye hidden para validar en multiples tabs
											excluded : [ ':disabled' ],
											feedbackIcons : {
												valid : 'glyphicon glyphicon-ok',
												invalid : 'glyphicon glyphicon-remove',
												validating : 'glyphicon glyphicon-refresh'
											},
											message : 'Datos inválidos',
											submitHandler : function(validator,
													form, submitButton) {
												alert(form);
												debugger;
											},
											fields : {
												'tramite.numero' : {
													validators : {
														notEmpty : {
															message : 'Dato Obligatorio'
														},
														integer : {
															message : 'El valor ingresado debe ser numérico'
														}
													}
												},
												'tramite.anio' : {
													validators : {
														notEmpty : {
															message : 'Dato Obligatorio'
														},
														greaterThan : {
															value : 1900,
															message : 'El valor ingresado debe ser mayor que 1900'
														}
													}
												}
											}
										});
					});
</script>

<s:form id="grid-parcela-form" action="ratificacion_editParcela"
	cssClass="form-vertical" namespace="/catastro" theme="bootstrap">
	<s:hidden id="partida" name="partida"></s:hidden>
</s:form>

<div class="row top-buffer">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

		<s:form id="ratificacion-form" action="ratificacion_process"
			cssClass="form-vertical" namespace="/catastro" theme="bootstrap">

			<s:if test='tramite.estado.toString != "FINALIZADO"'>
				<fieldset>
			</s:if>
			<s:else>
				<fieldset disabled>
			</s:else>
			<div class="row  input-group-sm">
				<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
					<label for="tipoRatificacion"><s:text name="tipo.label" /></label>

					<select id="tipoRatificacion" class="form-control" title="Seleccione Tipo"
						onchange="cambioTipoRatificacion()" name="tramite.codigo">
						<option value="1">Regimen de propiedad horizontal</option>
						<option value="2">Geodesia</option>
						<option value="6">Plano sin apertura en ARBA</option>
					</select> 
				</div>
				<div id="origen_numero"
					class="col-xs-6 col-sm-6 col-md-offset-4 col-md-2 col-lg-offset-4 col-lg-2">
					<label for="tf_origen_numero"><s:text
							name="ratificacion.plano.numero" /></label>
					<s:textfield id="tf_origen_numero" class="form-control"
						name="tramite.numero" />
				</div>
				<div id="origen_anio" class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
					<div class="form-group">
						<label for="tf_origen_anio"><s:text name="anio.label" /></label>
						<div class="input-group date" data-provide="datepicker"
							data-date-format="yyyy" data-date-autoclose="true"
							data-date-view-mode="years" data-date-min-view-mode="years"
							data-date-end-date="y" data-date-orientation="bottom"
							data-date-language="es" data-date-today-btn="true"
							data-date-today-highlight="true">
							<input id="tf_origen_anio" type="text" class="form-control"
								name="tramite.anio" value="<s:property value='tramite.anio'/>">
							<div class="input-group-addon">
								<span class="glyphicon glyphicon-calendar"></span>
							</div>
						</div>
					</div>
				</div>
			</div>
			</fieldset>
		</s:form>
	</div>

</div>

<table id="parcela-grid-data"
	class="table table-condensed table-hover table-striped bootgrid-table">
	<thead>
		<tr>
			<th data-column-id="cuenta" data-identifier="true"
				data-type="numeric" data-formatter="cuenta" data-width="10%"><s:text
					name="cuenta.label" /></th>
			<th data-column-id="partida" data-identifier="true"
				data-type="numeric" data-formatter="partida" data-width="10%"><s:text
					name="partida.label" /></th>
			<th data-column-id="nomenclatura" data-formatter="nomenclatura"
				data-sortable="false" data-width="40%"><s:text
					name="nomenclatura.label" /></th>
			<%-- <th data-column-id="linea" data-formatter="linea"
				data-sortable="false" data-width="40%"><s:text name="linea.label"/></th> --%>
			<%-- <s:if test='tramite.estado.toString != "FINALIZADO"'> --%>
			<th data-column-id="commands" data-formatter="commands"
				data-width="10%" data-sortable="false"><s:text
					name="label.acciones" /></th>
			<%-- </s:if> --%>
		</tr>
	</thead>
</table>
<s:if test='tramite.estado.toString() != "FINALIZADO"'>
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<button id="agregarParcela" class="btn btn-primary " value="Agregar"
				onclick="addCuenta();">
				<s:text name="dialog.label.agregar" />
			</button>
		</div>
	</div>
</s:if>

<script>
	var grid = $("#parcela-grid-data")
			.bootgrid(
					{
						navigation : 0,
						ajax : true,
						rowSelect : true,
						url : "catastro/parcela_getData.action",
						requestHandler : function(request) {
							for ( var key in request.sort) {
								request.sortColumn = key;
								request.sortOrder = request.sort[key];
							}
							return request;
						},
						labels : {
							noResults : "No hay parcelas",
							loading : "Cargando información...",
							infos : "Mostrando {{ctx.start}} a {{ctx.end}} de {{ctx.total}} filas"
						},
						formatters : {
							cuenta : function(column, row) {
								if (row.cuenta) {
									return row.cuenta.cuenta;
								}
							},
							partida : function(column, row) {
								return row.partida;
							},
							nomenclatura : function(column, row) {
								return fmt_nomenclatura(row);
							},
							commands : function(column, row) {
								return "<button onclick=\"sd_accion_submit("
										+ row.partida
										+ ", true);\" type=\"button\" class=\"btn btn-xs btn-default command-edit\" data-row-id=\""
										+ row.partida
										+ "\"><span class=\"icon glyphicon glyphicon-pencil\"></span></button> "
										+ "<button onclick=\"sd_accion_submit( "
										+ row.partida
										+ ", false);\"  type=\"button\" class=\"btn btn-xs btn-default command-delete\" data-row-id=\""
										+ row.partida
										+ "\"><span class=\"icon glyphicon glyphicon-trash\"></span></button>";
							}
						}
					}).on("loaded.rs.jquery.bootgrid", function(e) {
			});

	
</script>