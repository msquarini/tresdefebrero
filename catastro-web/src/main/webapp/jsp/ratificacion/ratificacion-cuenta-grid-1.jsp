<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<style>
.cgb-header-name {
	width: 30%;
}

/* Colorear HEADER */
.column .text { color: #f00 !important; }

/*  Estilo celda */
.cell { color: #f00 !important; font-weight: bold; }

.right_buttons
{
	float:right;
}

.right_buttons form
{
	float:left;
	padding-right: 2px;
}

.header_message
{
	font-size:1.5em;
	font-weight: bold;
	font-weight: bolder;
}
.single_message
{
	font-size:1.1em;
}
            
</style>
<script src="js/jquery.bootgrid.js"></script>
<link rel="stylesheet" href="css/jquery.bootgrid.min.css">
<div class="row">
	<div class="panel panel-default">
		<div class="panel-heading">
		<span class="header_message"><s:text name="ratificacion.label.common_message_header"/></span><br>
		<span class="single_message"><s:text name="ratificacion.label.select_uf_remove"/></span>
		</div>
		<div class="panel-body">
	
			<table id="ratificacion-cuenta-grid-data-1"
				class="table table-condensed table-hover table-striped">
				<thead>
					<tr>
						<th data-column-id="id" data-identifier="true" data-type="numeric"
							data-visible="false">IdParcela</th>
						<th data-column-id="idCuenta" data-type="numeric"
							data-visible="false" data-formatter="idCuenta"
							data-sortable="false">IdCuenta</th>
						<th data-column-id="cuenta" data-formatter="nested" data-width="8%">Cuenta</th>
						<th data-column-id="dv" data-formatter="nested" data-sortable="false"						
							data-width="4%">DV</th>
						<th data-column-id="partida" data-formatter="partida"
							data-width="7%">Partida</th>
						<th data-column-id="nomenclatura" data-formatter="nomenclatura"
							data-sortable="false" data-width="36%">Nomenclatura</th>
						<th data-column-id="activa" data-formatter="activa" data-align="center" data-header-align="center"
							data-sortable="false" data-width="8%">Activa</th>
	<!-- 					<th data-column-id="createdDate" data-formatter="nesteddate"
							data-sortable="false">Fecha Creacion</th>
						<th data-column-id="createdBy" data-formatter="nested"
							data-sortable="false">Creado Por</th> -->
						<th data-column-id="modifiedDate" data-formatter="nesteddate"
							data-sortable="false">Ultima Modif.</th>
						<th data-column-id="modifiedBy" data-formatter="nested"
							data-sortable="false">Modif. Por</th>
					</tr>
				</thead>
	
			</table>
			<input type="hidden" id="idSelectedCuenta" />
		</div>
	</div>
</div>
<div class="row">
	<div class="panel panel-default">
		<div class="panel-heading"><s:text name="ratificacion.label.uf_a_eliminar" /></div>
		<div class="panel-body" id="panel_ratificacion">
			<s:include value="ratificacion-remove-grid.jsp"></s:include>
		</div>
		<div class="panel-footer">



				<sj:a id="ratificacion_add_btn" cssClass="btn btn-primary disabled"
					value="Agregar" onclick="agregarParcelaParaRemover();"><s:text name="dialog.label.agregar" /></sj:a>
					
					
				<div class="right_buttons">
					<s:form id="ratificar-remove-init-form" action="ratificacion_init_remove"
						namespace="/catastro" theme="bootstrap">
	
						<sj:a id="ratificar_remover_btn" cssClass="btn btn-primary"
							value="Eliminar" formIds="ratificar-remove-init-form" targets="container"><s:text name="ratificacion.label.continuar" /></sj:a>
	
					</s:form>
				</div>	

		</div>

			
		<div class="clearfix"></div>
		<!--  Usado para que los botones queden dentro del footer del panel -->
	</div>
</div>
<script>
	$(document)
			.ready(
					function() {
						$("#ratificacion-cuenta-grid-data-1")
								.bootgrid(
										{
											ajax : true,
											navigation : 2,
											rowSelect : true,
											selection : true,
											multiSelect : false,
											search : false,
											columnSelection : false,
											templates : {
												search : ""
											},
											requestHandler : function(request) {
												for ( var key in request.sort) {
													request.sortColumn = key;
													request.sortOrder = request.sort[key];
												}
												//return JSON.stringify(request);
												return request;
											},
											labels : {
												noResults : "No hay parcelas",
												loading : "Cargando información...",
												infos : "Mostrando {{ctx.start}} a {{ctx.end}} de {{ctx.total}} filas"
											},
											statusMapping : {												
											    0 : "text-danger",
												1 : "warning",
												2 : "danger"
											},
											url : "catastro/ratificacion_getData.action",
											formatters : {
												nested : function(column, row) {
													return row.cuenta[column.id];
												},
												nesteddate : function(column,
														row) {
													value = row.cuenta[column.id];
													if (!value) {
														return "";
													}
													return moment(value)
															.format(
																	"DD/MM/YYYY");
												},
												idCuenta : function(column, row) {
													return row.cuenta.id;
												},
												partida : function(column, row) {
													return row.partida;
												},
												nomenclatura : function(column,
														row) {
													return fmt_nomenclatura(row);
												},
												activa : function(column, row) {
													return fmt_boolean(row.cuenta.activa);
												}
											}
										}).on("selected.rs.jquery.bootgrid",
										function(e, rows) {

										}).on(
										"selected.rs.jquery.bootgrid",
										function(e, rows) {
											$(document).trigger(
													'cuentaSelectedEvent',
													rows[0]);
											$('#idSelectedCuenta').val(
													rows[0].cuenta.id);
										}).on(
										"deselected.rs.jquery.bootgrid",
										function(e, rows) {
											$(document).trigger(
													'cuentaDeselectedEvent',
													rows[0]);
											$('#idSelectedCuenta').val("-1");
										});
					});
	
	var rows;
	
	$(document).off("cuentaSelectedEvent").on("cuentaSelectedEvent",
			function(event, data) {
				console.log(data);
		
				rows = data.id;
				$("#ratificacion_add_btn").toggleClass('disabled', false);
			}).off("cuentaDeselectedEvent").on("cuentaDeselectedEvent",
			function(event, data) {
				//alert("Deselect " + data.cuenta.id);
				$("#idCuentaOrigen").val("-1");
				$("#ratificacion_add_btn").toggleClass('disabled', true);
			}).off("ratificacion-grid-data-loaded")
			.on(
					"ratificacion-grid-data-loaded",
					function(event) {
						
						if ($("#ratificacion-grid-data").bootgrid(
								"getTotalRowCount") > 1) {
							$("#ratificar_btn").toggleClass('disabled', false);
						} else {
							$("#ratificar_btn").toggleClass('disabled', true);
						}
					});

	/* Incorpora una nueva parcela a la lista */
	function agregarParcelaParaRemover() {
		$.ajax({
			method : "POST",
			url : "catastro/ratificacion_addParcelaTORatificacionRemove.action",
			data : {
				idsParcela : rows
			},
			traditional : true,
			success : function() {
				$("#ratificacion-grid-data").bootgrid('reload');

				// window.location = "loadexperiments.action";
			},
			error : function(res, textStatus, jqXHR) {
				// Levantar dialogo con mensaje de error!!!!!!!!!!!!!
				handleErrorMessages(res.responseJSON);
				//$("#errors-data").text(res.responseJSON.actionErrors[0]);
				$('#error-dialog').modal('show');
			}
		});
	}
</script>