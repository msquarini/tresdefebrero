<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<!-- ALTA de TITULAR -->

<script src="js/select/bootstrap-select.min.js"></script>
<link rel="stylesheet" href="css/select/bootstrap-select.css">

<script type="text/javascript">
	$('.selectpicker').selectpicker({
		width : '100%'
	});

	$('#personaJuridica').checkboxpicker({
		offLabel : 'No',
		onLabel : 'Si'
	}).on('change', function() {
		if ($(this).is(':checked')) {
			$(this).prop('value', 'true');
		} else {
			$(this).prop('value', 'false');
		}
	});

	/* Submit del formulario previa validacion */
	function dialog_submit() {
		$('#titular-edit-form').bootstrapValidator('resetForm');
		$('#titular-edit-form').bootstrapValidator('validate');

		if ($('#titular-edit-form').data('bootstrapValidator').isValid()) {
			$('#titular-edit-form').submit();
		}
	};

	$('.form-control').keypress(function(e) {
		if (e.which == 13) {
			e.preventDefault();
			return false; //<---- Add this line
		}
	});

	/* Configuracion del formulario, ya que se hace submit del mismo mediante javascript */
	$('#titular-edit-form').ajaxForm({
		//target : '#generic-modal-body',
		target : '#generic-modal-response-body',
		success : function(res, textStatus, jqXHR) {
			// Se cierra el dialogo principal
			cerrarDialogo();
		},
		error : function(res, textStatus, jqXHR) {
			//$("#error-titular").text(res.responseJSON.actionErrors[0]);
			//handleErrorMessages(res.responseJSON);
			//$("#errors-data").text(res.responseJSON.actionErrors[0]);
			showErrorModalDialog(false);
		}
	});

	/* Configuracion de validador del formulario */
	$(document)
			.ready(
					function() {

						if ($('#personaJuridica').prop('value') == 'true') {
							$('#personaJuridica').prop('checked', 'true');
						}

						$('#titular-edit-form')
								.bootstrapValidator(
										{
											framework : 'bootstrap',
											excluded : [ ':disabled' ],
											feedbackIcons : {
												invalid : 'glyphicon glyphicon-remove',
												validating : 'glyphicon glyphicon-refresh'
											},
											message : 'Datos invalidos',
											err : {
												// You can set it to popover
												// The message then will be shown in Bootstrap popover
												container : 'tooltip'
											},
											fields : {
												'titularParcela.titular.apellido' : {
													validators : {
														notEmpty : {
															message : 'Obligatorio'
														}
													}
												},
												'titularParcela.titular.domicilioPostal.calle' : {
													validators : {
														notEmpty : {
															message : 'Obligatorio'
														}
													}
												},
												'titularParcela.titular.domicilioPostal.barrio.id' : {
													validators : {
														notEmpty : {
															message : 'Obligatorio'
														}
													}
												},
												'titularParcela.titular.cuitcuil' : {
													validators : {
														notEmpty : {
															message : 'Cuit/Cuil Obligatorio'
														},
														stringLength : {
															min : 11,
															max : 11,
															message : 'El dato ingresado debe ser de 11 posisiones num�ricas'
														},
														callback : {
															message : 'Cuit inv�lido',
															callback : function(
																	value,
																	validator,
																	$field) {
																return validaCuit(value);
															}
														}
													}
												},
												'titularParcela.porcentaje' : {
													validators : {
														notEmpty : {
															message : '% Titularidad Obligatorio'
														},
														between : {
															min : 1,
															max : 100,
															message : 'Mayor a 0 y menor o igual a 100'
														}
													}
												},
												'titularParcela.desde' : {
													validators : {
														notEmpty : {
															message : 'Fecha de Titularidad Obligatorio'
														}
													}
												},
												'titularParcela.titular.mail' : {
													validators : {
														emailAddress : {
															message : 'Direccion de correo no v\u00e1lida'
														}
													}
												}
											}
										}).on('error.field.bv',
										function(e, data) {
											// $(e.target)  --> The form instance
											// $(e.target).data('bootstrapValidator')
											//              --> The BootstrapValidator instance
											// data.field   --> The field name
											// data.element --> The new field element
											// data.options --> The new field options
										});
					});
</script>
<style type="text/css">
.has-feedback .form-control {
	top: 0;
	/* Para utilizar todo el area editable */
	padding-right: 10px;
}

.form-control-feedback {
	/* Para correr el icon a la derecha */
	right: -22px;
}

/* enable absolute positioning */
.inner-addon {
	position: relative;
}

/* style icon */
.inner-addon .fa {
	position: absolute;
	padding: 10px;
	pointer-events: none;
}

/* align icon */
.left-addon .glyphicon {
	left: 0px;
}

.right-addon .glyphicon {
	right: 0px;
}

/* add padding  */
.left-addon input {
	padding-left: 30px;
}

.right-addon input {
	padding-right: 30px;
}
</style>

<s:form id="titular-edit-form" action="titularABMRatificacion_addTitular"
	cssClass="form-vertical" namespace="/catastro" theme="bootstrap">

	<fieldset>
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<p id="error-titular" class="bg-danger"></p>
			</div>
		</div>
		<div class="row  ">
			<%-- <s:hidden name="titular.id"></s:hidden> --%>
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				<s:textfield id="apellido" class="form-control"
					name="titularParcela.titular.apellido" maxlength="50"
					placeholder="Apellidos" />
			</div>
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				<s:textfield id="nombre" class="form-control" maxlength="50"
					name="titularParcela.titular.nombre" placeholder="Nombres" />
			</div>

			<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
				<select id="tipodocumento"
					name="titularParcela.titular.tipoDocumento" class="selectpicker"
					title="Tipo Documento">
					<option value="1">DNI</option>
					<option value="2">Pasaporte</option>
					<option value="3">LC</option>
					<option value="4">LE</option>
				</select>
			</div>
			<div class="col-xs-6 col-sm-6 col-md-1 col-lg-2">
				<s:textfield id="documento" class="form-control number"
					maxlength="8" placeholder="Documento"
					name="titularParcela.titular.documento" />
			</div>
		</div>

		<!-- Domicilio Potal Titular -->
		<div class="panel-group">
			<div class="panel panel-default">
				<div class="panel-heading" href="#panel_domicilio_inmueble">Domicilio Postal</div>
				<div class="panel-body" id="panel_domicilio_postal">
					<div class="col-xs-6 col-sm-6 col-md-5 col-lg-4"
						style="padding-left: 2px; padding-right: 2px;">
						<!-- <label for="di_calle">Calle</label> -->
						<s:textfield id="di_calle" class="form-control"
							placeholder="Calle"
							name="titularParcela.titular.domicilioPostal.calle" />
					</div>
					<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2"
						style="padding-left: 2px; padding-right: 2px;">
						<!-- <label for="di_numero">Nro</label> -->
						<s:textfield id="di_numero" class="form-control" placeholder="Nro"
							name="titularParcela.titular.domicilioPostal.numero" />
					</div>
					<div class="col-xs-6 col-sm-6 col-md-1 col-lg-1"
						style="padding-left: 2px; padding-right: 2px;">
						<!-- 	<label for="di_piso">Piso</label> -->
						<s:textfield id="di_piso" class="form-control" placeholder="Piso"
							name="titularParcela.titular.domicilioPostal.piso" />
					</div>
					<div class="col-xs-6 col-sm-6 col-md-1 col-lg-1"
						style="padding-left: 2px; padding-right: 2px;">
						<!-- <label for="di_depto">Depto</label> -->
						<s:textfield id="di_depto" class="form-control" placeholder="Dpto"
							name="titularParcela.titular.domicilioPostal.departamento" />
					</div>
					<div class="col-xs-6 col-sm-6 col-md-1 col-lg-2"
						style="padding-left: 2px; padding-right: 2px;">
						<!-- <label for="di_cp">CP</label> -->
						<s:textfield id="di_cp" class="form-control"
							placeholder="C�digo Postal"
							name="titularParcela.titular.domicilioPostal.codigoPostal" />
					</div>
					<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2"
						style="padding-left: 2px; padding-right: 2px;">
						<!-- <label for="di_barrio">Barrio</label> -->
						<s:select id="di_barrio" list="#session.barrios" listKey="id"
							listValue="nombre" class="selectpicker" title="Barrio"
							name="titularParcela.titular.domicilioPostal.barrio.id"></s:select>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-xs-6 col-sm-6 col-md-3 col-lg-4">
				<label for="personaJuridica">Persona Jur�dica</label> <input
					id="personaJuridica" data-group-cls="btn-group-sm" type="checkbox"
					name="titularParcela.titular.personaJuridica"
					value='<s:property value="titularParcela.titular.personaJuridica"/>' />


			</div>
			<div class="col-xs-6 col-sm-6 col-md-1 col-lg-2">
				<s:textfield id="cuitcuil" class="form-control number"
					maxlength="11" placeholder="Cuit/Cuil"
					name="titularParcela.titular.cuitcuil" />
			</div>
			<div class="col-xs-6 col-sm-6 col-md-1 col-lg-6">
				<s:textfield id="razonSocial" class="form-control" maxlength="50"
					placeholder="Raz�n Social"
					name="titularParcela.titular.razonSocial" />
			</div>
		</div>

		<div class="row">
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				<div class="form-group">
					<div class="input-group">
						<input type="text" id="porcentaje"
							class="form-control numberdecimal" maxlength="7"
							placeholder="Porcentaje" name="titularParcela.porcentaje"
							value='<s:property value="getText('{0,number,###0.00}',{titularParcela.porcentaje})"/>' />
						<div class="input-group-addon">
							<span class="fa fa-percent"></span>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				<div class="form-group">
					<div class="input-group date" data-provide="datepicker" id="desde"
						data-date-format="dd/mm/yyyy" data-date-autoclose="true"
						data-date-orientation="bottom" data-date-language="es">
						<input type="text" class="form-control" placeholder="Fecha desde"
							value='<s:date name="titularParcela.desde" format="dd/MM/yyyy" />'
							name="titularParcela.desde">
						<div class="input-group-addon">
							<span class="glyphicon glyphicon-calendar"></span>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
				<div class="input-group date" data-provide="datepicker"
					data-date-format="dd/mm/yyyy" data-date-autoclose="true"
					data-date-orientation="bottom" data-date-language="es"
					data-date-end-date="0d">
					<input id="hasta" type="text" class="form-control"
						placeholder="Fecha hasta"
						value="<s:date name="titularParcela.hasta" format="dd/MM/yyyy" />"
						name="titularParcela.hasta">
					<div class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"></span>
					</div>
				</div>

			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="form-group inner-addon left-addon">
					<i class="fa fa-envelope-o" aria-hidden="true"></i> <input
						type="text" class="form-control" id="mail"
						name="titularParcela.titular.mail"
						value='<s:property value="titularParcela.titular.mail"/>'
						placeholder="Correo electronico">
					<%-- <s:textfield id="mail" name="titularParcela.titular.mail"
						placeholder="Correo electronico" /> --%>
				</div>
			</div>
		</div>
		<!-- Observacion -->
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<s:textfield id="observacion"
					name="titularParcela.titular.observacion" placeholder="Observaci�n" />
			</div>
		</div>
	</fieldset>

</s:form>

<script>
	$('input.number').number(true, 0, '', '');
	//$('#porcentaje').val($.number($('#porcentaje').val(), 2, ',', ''));
	$('input.numberdecimal').number(true, 2, '.', '');

	$('#desde').on(
			'changeDate show',
			function(e) {
				// Revalidate the date field
				$('#titular-edit-form').bootstrapValidator('revalidateField',
						'titularParcela.desde');
			});

	$('#datePicker').datepicker({
		format : 'dd/mm/yyyy'
	}).on('changeDate', function(e) {
		// Revalidate the date field
		$('#titular-edit-form').bootstrapValidator('revalidateField', 'desde');
	});

	
</script>