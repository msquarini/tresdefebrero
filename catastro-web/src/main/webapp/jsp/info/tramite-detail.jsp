<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<script type="text/javascript">
	
</script>

<style type="text/css">
input[readonly].info {
	background-color: transparent;
	border: 1;
	font-size: 1em;
}
</style>

<div class="row">

	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="panel panel-info">
			<div class="panel-heading">
				<strong><s:text name="tramite.label" /></strong>
			</div>
			<div class="panel-body">
				<div class="col-xs-6 col-sm-6 col-md-4 col-lg-1">
					<label><s:text name="numero.label" /></label>
					<input class="form-control info" type="text" readonly
						value='<s:property value="tramite.id"></s:property>' />
				</div>
				<div class="col-xs-6 col-sm-6 col-md-4 col-lg-2">
					<label ><s:text name="descripcion.label" /></label> <input
						class="form-control info" type="text" readonly
						value='<s:property value="tramite.tipo.descripcion"></s:property>' />
				</div>
				<div class="col-xs-6 col-sm-6 col-md-4 col-lg-1">
					<label ><s:text name="numero.label" /></label> <input
						class="form-control info" type="text" readonly
						value='<s:property value="tramite.numero"></s:property>' />
				</div>
				<div class="col-xs-6 col-sm-6 col-md-4 col-lg-1">
					<label ><s:text name="anio.label" /></label> <input
						class="form-control info" type="text" readonly
						value='<s:property value="tramite.anio"></s:property>' />
				</div>
				<div class="col-xs-6 col-sm-6 col-md-4 col-lg-2">
					<label ><s:text name="organismo.codigo.label" /></label>
					<input class="form-control info" type="text" readonly
						value='<s:property value="tramite.codOrganismo"></s:property>' />
				</div>
				<div class="col-xs-6 col-sm-6 col-md-4 col-lg-1">
					<label ><s:text name="folio.label" /></label> <input
						class="form-control info" type="text" readonly
						value='<s:property value="tramite.folio"></s:property>' />
				</div>
				<div class="col-xs-6 col-sm-6 col-md-4 col-lg-2">
					<label ><s:text name="fecha.alta.label" /></label> <input
						class="form-control info" type="text" readonly
						value='<s:property value="getText('format.datetime',{tramite.createdDate})"></s:property>' />
				</div>
				<div class="col-xs-6 col-sm-6 col-md-4 col-lg-2">
					<label><s:text name="usuario.alta.label" /></label> <input
						class="form-control info" type="text" readonly
						value='<s:property value="tramite.createdBy"></s:property>' />
				</div>
			</div>
		</div>
	</div>
</div>
