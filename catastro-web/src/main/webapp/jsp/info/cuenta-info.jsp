<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<!-- INFORMACION DE CUENTA
		Detalle de parcela, titular, responsable, tramites, y cuenta corriente
 -->
<script type="text/javascript">
	
</script>

<style type="text/css">
/* .form-control  {
    font-size: 16px;
    font-weight: bold;    
} */
</style>

<div class="bs-callout bs-callout-info"
	style="background-color: #bacfdd;">
	<div class="row">
		<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
			<h3 align="left" class="text-primary">
				<small>Cuenta:</small>&nbsp;<strong><s:property
						value="%{#session.cuenta.cuenta}" /> / <s:property
						value="%{#session.cuenta.dv}" /></strong>
			</h3>
		</div>
		<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4"></div>
		<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
			<s:if test="%{#session.cuenta_con_deuda}">
				<h3 align="right" style="margin-right: 10px">
					<span class="label label-danger">Presenta Deuda</span>
				</h3>
			</s:if>
		</div>
	</div>
</div>

<div>
	<ul class="nav nav-pills"
		style="background-color: #eeeeee; border-bottom-style: ridge;">
		<li class="active"><a data-toggle="tab" href="#parcela"><s:text
					name="parcela.label" /></a></li>
		<li><a data-toggle="tab" href="#linea"><s:text
					name="linea.label" /></a></li>
		<li><a data-toggle="tab" href="#titulares"><s:text
					name="titulares.label" /></a></li>
		<li><a data-toggle="tab" href="#deuda"><s:text
					name="deuda.label" /></a></li>
		<li><a data-toggle="tab" href="#boleta"><s:text
					name="boleta.label" /></a></li>
		<li><a data-toggle="tab" href="#tramites"><s:text
					name="tramites.label" /></a></li>
		<li><a data-toggle="tab" href="#responsables"><s:text
					name="responsables.label" /></a></li>
	</ul>
</div>

<div class="tab-content" style="padding-top: 20px;">
	<div id="parcela" class="tab-pane fade in active">
		<s:include value="parcela-info.jsp"></s:include>
	</div>

	<div id="linea" class="tab-pane fade">
		<s:push value="%{#session.cuenta.parcela.lineaVigente}">
			<s:include value="linea-info.jsp"></s:include>
		</s:push>
	</div>

	<div id="titulares" class="tab-pane fade">
		<s:include value="../grid/titular-grid.jsp?sinAcciones=1"></s:include>
	</div>

	<div id="deuda" class="tab-pane fade">
		<s:include value="../grid/cc-grid.jsp"></s:include>
	</div>

	<div id="boleta" class="tab-pane fade">
		<s:include value="../boleta/boleta-grid.jsp"></s:include>
	</div>

	<div id="tramites" class="tab-pane fade">
		<s:include value="tramites-info.jsp"></s:include>
	</div>

	<div id="responsables" class="tab-pane fade">
		<s:include value="responsables-info.jsp"></s:include>
	</div>

</div>
