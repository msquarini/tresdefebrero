<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<!-- Grilla de responsables de la cuenta -->

<style>
.cgb-header-name {
	width: 50%;
}
</style>

<script type="text/javascript">
	
</script>

<div class="row top-buffer">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<table id="responsables-grid-data"
			class="table table-condensed table-hover table-striped">
			<thead>
				<tr>
					<th data-column-id="id" data-identifier="true" data-type="numeric"
						data-visible="false">idResponsable</th>
					<th data-column-id="apellido" data-formatter="nested"
						data-type="string" data-visible="true"><s:text
							name="titular.apellido" /></th>
					<th data-column-id="nombre" data-formatter="nested"
						data-type="string" data-visible="true"><s:text
							name="titular.nombre" /></th>
					<th data-column-id="cuitcuil" data-formatter="nested"><s:text
							name="titular.cuitcuil" /></th>
					<th data-column-id="documento" data-formatter="nested"><s:text
							name="titular.documento" /></th>
					<th data-column-id="desde" data-type="date"><s:text
							name="titular.parcela.desde" /></th>
					<th data-column-id="hasta" data-type="date"><s:text
							name="titular.parcela.hasta" /></th>
					<!-- Esta columna se habilita cuando se permite modidicacion de responsable -->
					<%-- 					<th data-column-id="commands" data-formatter="commands"
						data-sortable="false"><s:text name="label.acciones" /></th> --%>
				</tr>
			</thead>
		</table>
	</div>
</div>

<script>
	$(document)
			.ready(
					function() {
						$("#responsables-grid-data")
								.bootgrid(
										{
											ajax : true,
											navigation : 2,
											rowCount : 5,
											rowSelect : true,
											//selection : true,
											multiSelect : false,
											search : true,
											columnSelection : false,
											templates : {
												search : ""
											},
											labels : {
												noResults : "No hay responsables",
												loading : "Cargando información...",
												infos : "Mostrando {{ctx.start}} a {{ctx.end}} de {{ctx.total}} filas"
											},
											url : "catastro/responsable_getData.action",
											requestHandler : function(request) {
												return sortRequest(request);
											},
											converters : {
												date : {
													from : function(value) {
														return moment(value);
													},
													to : function(value) {
														if (value) {
															return moment(value)
																	.format(
																			"DD-MM-YYYY");
														} else {
															return "";
														}
													}
												}
											},
											formatters : {
												nested : function(column, row) {
													return row.titular[column.id];
												}
											}
										});
					});
</script>
