<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<!-- Grilla de tramites que afectaron a la cuenta -->

<style>
.cgb-header-name {
	width: 50%;
}
</style>

<script type="text/javascript">
	function tramiteDetail(id) {
		$.ajax({
			method : "POST",
			url : "catastro/tramite_info.action",
			data : {
				idTramite : id
			},
			traditional : true,
			success : function(res) {
				$("#tramite-detail").html(res);
			},
			error : function(res, textStatus, jqXHR) {
				// Levantar dialogo con mensaje de error!!!!!!!!!!!!!
				handleErrorMessages(res.responseJSON);
				//$("#errors-data").text(res.responseJSON.actionErrors[0]);
				$('#error-dialog').modal('show');
			}
		});
	}
</script>

<div class="row ">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<table id="tramites-grid-data"
			class="table table-condensed table-hover table-striped">
			<thead>
				<tr>
					<th data-column-id="id" data-identifier="true" data-type="numeric"
						data-visible="true">Nro</th>
					<!-- 	<th data-column-id="fecha" data-type="date" data-visible="true">Fecha</th> -->
					<th data-column-id="tipo" data-type="string" data-visible="true">Tipo</th>
					<th data-column-id="codigo" data-type="string" data-visible="true">C�digo</th>
					<th data-column-id="createdBy" data-type="string">Usuario</th>
					<th data-column-id="createdDate" data-type="datetime">Creacion</th>
				</tr>
			</thead>
		</table>
	</div>
</div>

<div class="row top-buffer">
	<div id="tramite-detail" class="container"></div>
</div>

<script>
	$(document)
			.ready(
					function() {
						$("#tramites-grid-data")
								.bootgrid(
										{
											ajax : true,
											navigation : 2,
											rowCount : 5,
											rowSelect : true,
											//selection : true,
											multiSelect : false,
											search : true,
											columnSelection : false,
											templates : {
												search : ""
											},
											labels : {
												noResults : "No hay tramites",
												loading : "Cargando informaci�n...",
												infos : "Mostrando {{ctx.start}} a {{ctx.end}} de {{ctx.total}} filas"
											},
											url : "catastro/tramite_getData.action",
											requestHandler : function(request) {
												return sortRequest(request);
											},
											converters : {
												date : {
													from : function(value) {
														return moment(value);
													},
													to : function(value) {
														return moment(value)
																.format(
																		"DD-MM-YYYY");
													}
												},
												datetime : {
													from : function(value) {
														return moment(value);
													},
													to : function(value) {
														return moment(value)
																.format(
																		"DD-MM-YYYY HH:mm:ss");
													}
												}

											}
										}).on("click.rs.jquery.bootgrid",
										function(e, columns, row) {
											tramiteDetail(row.id);
										});
					});
</script>
