<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<script type="text/javascript">
	
</script>

<style type="text/css">
.align-right {
	text-align: right;
}
</style>


<div class="row">

	<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
		<div class="panel panel-default">
			<div class="panel-heading clearfix">
				<div class="pull-left">
					<strong><s:text name="superficies.panel.label" /></strong>
				</div>
				<div class="pull-right text-primary" style="padding-right: 15px;">
					<span class="label label-info" style="font-size: 100%"><s:text
							name="fecha_data.label" />: <s:property
							value="getText('format.date',{fecha})"></s:property></span>
				</div>
			</div>
			<div class="panel-body">
				<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
					<label for="sup_terreno"><s:text name="terreno.label" /></label>
					<div class="input-group">
						<input class="form-control align-right type=" text" readonly
							value='<s:property value="getText('number.format',{superficieTerreno})"></s:property>' />
						<span class="input-group-addon">m<sup>2</sup></span>
					</div>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
					<label for="sup_cubierta"><s:text name="cubierta.label" /></label>
					<div class="input-group">
						<input class="form-control align-right" type="text" readonly
							value='<s:property value="getText('number.format',{superficieCubierta})"></s:property>' />
						<span class="input-group-addon">m<sup>2</sup></span>
					</div>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
					<label for="sup_semi"><s:text name="semi.label" /></label>
					<div class="input-group">
						<input class="form-control align-right" type="text" readonly
							value='<s:property value="getText('number.format',{superficieSemi})"></s:property>' />
						<span class="input-group-addon">m<sup>2</sup></span>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
		<div class="panel panel-default">
			<div class="panel-heading clearfix">
				<div class="pull-left">
					<strong><s:text name="valuacion.panel.label" /></strong>
				</div>
				<div class="pull-right" style="padding-right: 15px;">
					<span class="label label-info" style="font-size: 100%">$ <s:property
							value="getText('{0,number,#,###.00}',{valuacion})"></s:property></span>
				</div>
			</div>
			<div class="panel-body">
				<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
					<label for="valor_tierra"><s:text name="tierra.label" /></label>
					<div class="input-group">
						<span class="input-group-addon">$</span> <input
							class="form-control align-right" type="text" readonly
							value='<s:property value="getText('{0,number,#,###.00}',{valorTierra})"></s:property>' />
					</div>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
					<label for="valor_comun"><s:text name="comun.label" /></label>
					<div class="input-group">
						<span class="input-group-addon">$</span> <input
							class="form-control align-right" type="text" readonly
							value='<s:property value="getText('money.format',{valorComun})"></s:property>' />
					</div>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-4 col-lg-42">
					<label for="valor_edificio"><s:text name="edificio.label" /></label>
					<div class="input-group">
						<span class="input-group-addon">$</span> <input
							class="form-control align-right" type="text" readonly
							value='<s:property value="getText('money.format',{valorEdificio})"></s:property>' />
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<s:include value="../grid/lineas-grid.jsp"></s:include>
</div>