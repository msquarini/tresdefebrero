<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<script type="text/javascript">
	
</script>
<style type="text/css">
</style>
<!-- Nomenclatura -->

<div class="row">
	<div class="col-md-2 col-lg-2">
		<label for="partida"><s:text name="partida.label" /></label> <input
			class="form-control input-sm" type="text" readonly
			value='<s:property value="%{#session.cuenta.parcela.partida}"></s:property>' />
	</div>
	<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
		<s:push value="%{#session.cuenta.parcela.nomenclatura}">
			<s:include value="../common/nomenclatura-info.jsp"></s:include>
		</s:push>
	</div>
	<div class="col-md-2 col-lg-2">
		<div class="row">
			<s:if test="%{#session.cuenta.activa}">
				<span class='label label-success'> <i
					class="fa fa-check-square-o" aria-hidden="true"></i> Activa
				</span>
			</s:if>
			<s:else>
				<span class='label label-danger'> <i class="fa fa-square-o"
					aria-hidden="true"></i> Activa
				</span>
			</s:else>
		</div>
		<div class="row">
			<s:if test="%{#session.cuenta.exenta}">
				<span class='label label-warning'> <i
					class="fa fa-check-square-o" aria-hidden="true"></i> Exenta
				</span>
			</s:if>
			<s:else>
				<span class='label label-info'> <i class="fa fa-square-o"
					aria-hidden="true"></i> Exenta
				</span>
			</s:else>
		</div>
		<div class="row">
			<s:if test="%{#session.cuenta.parcela.dominioMunicipal}">
				<span class='label label-warning'> <i
					class="fa fa-check-square-o" aria-hidden="true"></i> Dom. Municipal
				</span>
			</s:if>
			<s:else>
				<span class='label label-info'> <i class="fa fa-square-o"
					aria-hidden="true"></i> Dom. Municipal
				</span>
			</s:else>
		</div>
	</div>

</div>


<!-- Datos Parcela -->
<div class="row">
	<div class="col-md-1 col-lg-1">
		<label for="categoria"><s:text name="categoria.label" /></label> <input
			class="form-control input-sm text-info" type="text" readonly
			value='<s:property value="%{#session.cuenta.parcela.lineaVigente.categoria}"></s:property>' />
	</div>
	<div class="col-md-1 col-lg-1">
		<label for="partida"><s:text name="dominio.tipo.label" /></label> <input
			class="form-control input-sm" type="text" readonly
			value='<s:text name="%{#session.cuenta.parcela.dominio.tipo.textKey}"></s:text>' />
	</div>
	<div class="col-md-1 col-lg-1">
		<label for="numero"><s:text name="dominio.numero.label" /></label> <input
			class="form-control input-sm " type="text" readonly
			value='<s:property value="%{#session.cuenta.parcela.dominio.numero}"></s:property>' />
	</div>
	<div class="col-md-1 col-lg-1">
		<label for="anio"><s:text name="dominio.anio.label" /></label> <input
			class="form-control input-sm" type="text" readonly
			value='<s:property value="%{#session.cuenta.parcela.dominio.anio}"></s:property>' />
	</div>
	<div class="col-md-2 col-lg-2">
		<label for="creado"><s:text name="fecha.alta.label" /></label> <input
			class="form-control input-sm" type="text" readonly
			value='<s:property value="getText('format.datetime',{#session.cuenta.createdDate})"></s:property>' />
	</div>
	<div class="col-md-2 col-lg-2">
		<label for="creado"><s:text name="usuario.alta.label" /></label> <input
			class="form-control input-sm" type="text" readonly
			value='<s:property value="%{#session.cuenta.createdBy}"></s:property>' />
	</div>
	<div class="col-md-2 col-lg-2">
		<label for="creado"><s:text name="fecha.modifico.label" /></label> <input
			class="form-control input-sm" type="text" readonly
			value='<s:property value="getText('format.datetime',{#session.cuenta.modifiedDate})"></s:property>' />
	</div>
	<div class="col-md-2 col-lg-2">
		<label for="creado"><s:text name="usuario.modifico.label" /></label> <input
			class="form-control input-sm" type="text" readonly
			value='<s:property value="%{#session.cuenta.modifiedBy}"></s:property>' />
	</div>
</div>

<div class="top-buffer">
	<!-- Domicilio Inmueble -->
	<div class="panel-group">
		<div class="panel panel-default">
			<div class="panel-heading">Domicilio Inmueble</div>
			<div class="panel-body" id="panel_domicilio_inmueble">
				<div class="col-xs-6 col-sm-6 col-md-5 col-lg-5">
					<label for="di_calle">Calle</label> <input id="di_calle"
						class="form-control input-sm" readonly
						name="nuevaParcela.domicilioInmueble.calle" />
				</div>
				<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
					<label for="di_numero">Nro</label> <input id="di_numero"
						class="form-control input-sm" readonly
						name="nuevaParcela.domicilioInmueble.numero" />
				</div>
				<div class="col-xs-6 col-sm-6 col-md-1 col-lg-1">
					<label for="di_piso">Piso</label> <input id="di_piso"
						class="form-control input-sm"
						name="nuevaParcela.domicilioInmueble.piso" readonly />
				</div>
				<div class="col-xs-6 col-sm-6 col-md-1 col-lg-1">
					<label for="di_depto">Depto</label> <input id="di_depto"
						class="form-control input-sm"
						name="nuevaParcela.domicilioInmueble.departamento" readonly />
				</div>
				<div class="col-xs-6 col-sm-6 col-md-1 col-lg-1">
					<label for="di_cp">CP</label> <input id="di_cp"
						class="form-control input-sm"
						name="nuevaParcela.domicilioInmueble.codigoPostal" readonly />
				</div>
				<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
					<label for="di_barrio">Barrio</label> <input id="di_barrio"
						class="form-control input-sm"
						name="nuevaParcela.domicilioInmueble.barrio" readonly />
				</div>
			</div>
		</div>

	</div>
</div>

<!-- Observacion -->
<div class="row">
	<div class="col-xs-12 col-sm12 col-md-12 col-lg-12">
		<label for="observacion">Observacion</label>
		<textarea id="observacion" class="form-control input-sm"
			style="color: blue" rows="2" readonly>DD</textarea>
	</div>
</div>