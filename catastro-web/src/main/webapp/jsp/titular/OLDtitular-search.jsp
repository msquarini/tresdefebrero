<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<script type="text/javascript">
	$(document).off("cuentaSelectedEvent").on("cuentaSelectedEvent",
			function(event, data) {
				$("#idCuentaOrigen").val(data.cuenta.id);
				$("#titular_btn").toggleClass('disabled', false);
			}).off("cuentaDeselectedEvent").on("cuentaDeselectedEvent",
			function(event, data) {
				$("#idCuentaOrigen").val("-1");
				$("#titular_btn").toggleClass('disabled', true);
			});

	/* Configuracion del formulario, ya que se hace submit del mismo mediante javascript
	 * Se controla el mesanje de error y se muestra dialog generico de errores!!!! 
	 */
	var hash = '';
	$('#titular-init-form').ajaxForm({
		target : '#container',
		success : function(res, textStatus, jqXHR) {
		},
		error : function(res, textStatus, jqXHR) {
			handleErrorMessages(res.responseJSON);
			if (res.status == '501') {
				hash = res.responseJSON.hash;
				showErrorModalDialog(true);
			} else {
				showErrorModalDialog(false);
			}
		}
	});

	function continuar() {
		$("#hash").val(hash);
		$('#titular-init-form').submit();
	}

	function init_submit() {
		$('#titular-init-form').submit();
	};
</script>
<style type="text/css">
</style>

<div class="row">
	<s:include value="../cuenta_search.jsp"></s:include>
</div>

<div class="row">
	<s:form id="titular-init-form" action="titular_init"
		namespace="/catastro" theme="bootstrap">
		<s:hidden id="idCuentaOrigen" name="idCuentaOrigen"></s:hidden>
		<s:hidden id="hash" name="hash"></s:hidden>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<!-- Para poder manejar la respuesta del llamado ajax!!! -->
			<sj:a id="titular_btn" cssClass="btn btn-primary disabled"
				value="titular" onclick="init_submit();">Gestion Titular</sj:a>
			<sj:a id="responsable_btn" cssClass="btn btn-primary disabled"
				value="responsable" onclick="init_submit();">Gestion Reponsable</sj:a>
			<sj:a id="linea_btn" cssClass="btn btn-primary disabled"
				value="linea" onclick="init_submit();">Gestion Linea</sj:a>
		</div>
	</s:form>
</div>