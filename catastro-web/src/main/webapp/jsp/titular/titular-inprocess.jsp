<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<!-- Tramite de gestion de titulares
	NO SE PERMITE GRABACION DEL TRAMITE!!!!
-->

<script type="text/javascript">
	/* Resultado del procesamiento de gestion de titulares !! */
/* 	$.subscribe('complete', function(event, data) {
		loader(false, $('#btn_process'));

		$('#generic-modal-response-body').html(
				event.originalEvent.request.responseText);
		showResponseModalDialog();
	});

	function validarFormulario() {
		$('#titulares-form').bootstrapValidator('resetForm');
		$('#titulares-form').bootstrapValidator('validate');
		return $('#titulares-form').data('bootstrapValidator').isValid();
	} */
	function procesarTramite() {
		/* if (validarFormulario()) {
			loader(true, $('#btn_process'));
			$('#titulares-form').submit();
		}
		 */
		loader(true, $('#btn_process'));
		$.ajax({
            method : "POST",
            url : "catastro/titularABM_process.action",
            data : {
            },
            traditional : true,
            success : function(res) {
                loader(false,  $('#btn_process'));                
                showResponseModalDialog(true, res);
            },
            error : function(res, textStatus, jqXHR) {
                loader(false,  $('#btn_process'));
                // Levantar dialogo con mensaje de error!!!!!!!!!!!!!
                handleErrorMessages(res.responseJSON);
                showErrorModalDialog(false);
            }
        });
	}

</script>

<style type="text/css">
</style>

<s:include value="../common/tramite-header.jsp"></s:include>

<ul class="nav nav-pills"
	style="background-color: #eeeeee; border-bottom-style: ridge;">
	<li class="nav-header disabled"><a><s:text
				name="tramite.titular.label" /></a></li>
	<li class="active"><a data-toggle="tab" href="#cuenta-origen"><s:text
				name="cuenta.label" /></a></li>
	<li><a data-toggle="tab" href="#titulares"><s:text
				name="titulares.label" /></a></li>
	<li><a data-toggle="tab" href="#responsable"><s:text
				name="responsables.label" /></a></li>

	<s:if test='tramite.estado.toString() != "FINALIZADO"'>
		<li class="pull-right" style="padding-top: 3px; padding-right: 3px">
			<button id="btn_process" class="btn btn-danger" type="button"
				onclick="procesarTramite()">
				<span class="fa fa-wrench"></span>&nbsp;
				<s:text name="dialog.label.procesar" />
				&nbsp;
			</button>
		</li>
	</s:if>
</ul>

<div class="tab-content">
	<div id="cuenta-origen" class="tab-pane fade in active">
		<s:include value="../cuenta-info.jsp"></s:include>
	</div>
	<div id="titulares" class="tab-pane fade">
		<s:include value="../grid/titular-abm-grid.jsp"></s:include>
	</div>
	<div id="responsable" class="tab-pane fade">
		<s:include value="../info/responsables-info.jsp"></s:include>
	</div>
</div>

