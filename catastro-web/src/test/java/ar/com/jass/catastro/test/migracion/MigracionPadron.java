package ar.com.jass.catastro.test.migracion;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import ar.com.jass.catastro.dao.GeneralDAO;
import ar.com.jass.catastro.model.Cuenta;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:application-context-test.xml" })
public class MigracionPadron {

    MigracionConverter mc = new MigracionConverter();

    @Autowired
    @Qualifier("generalDAO")
    GeneralDAO generalDAO;

    @Autowired
    @Qualifier("migracionDAO")
    //MigracionDAO migracionDAO;

    public static void main(String[] args) {

        // Inicializacion

        // Inicio del proceso
    }

    @Test
    @Transactional
    @Rollback(false)
    public void process() {
        /**
         * Entrada de datos Padron Ordenar el proceso por el siguiente criterio: 1 cuentas con
         * origen 000000 y sin subdivision 2 cuentas con origen 000000 y con subdivicion 3 resto de
         * las cuentas
         */
/*        List<PadronTSG> padron = migracionDAO.findTSG();

        for (PadronTSG p : padron) {
            // Construccion de objectos de modelo
            Cuenta c = mc.crearCuenta(p);
            // Persistencia
            getGeneralDAO().save(c);
        }*/
        
        // Marca de migrado
    }

    public GeneralDAO getGeneralDAO() {
        return generalDAO;
    }

    public void setGeneralDAO(GeneralDAO generalDAO) {
        this.generalDAO = generalDAO;
    }

/*    public MigracionDAO getMigracionDAO() {
        return migracionDAO;
    }

    public void setMigracionDAO(MigracionDAO migracionDAO) {
        this.migracionDAO = migracionDAO;
    }*/
}
