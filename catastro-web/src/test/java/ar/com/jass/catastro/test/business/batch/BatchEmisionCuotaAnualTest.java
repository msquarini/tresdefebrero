package ar.com.jass.catastro.test.business.batch;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import ar.com.jass.catastro.batch.deuda.GeneracionDeudaRunBatch;
import ar.com.jass.catastro.dao.GeneralDAO;
import ar.com.jass.catastro.model.cuentacorriente.Periodo;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:application-context-test.xml" })
public class BatchEmisionCuotaAnualTest {

    static Logger LOG = LoggerFactory.getLogger(BatchEmisionCuotaAnualTest.class);

    @Autowired
    @Qualifier("cuotaAnualScheduler")
    private GeneracionDeudaRunBatch batch;

    @Autowired
    @Qualifier("generalDAO")
    GeneralDAO generalDAO;
    
    @Test
    @Deprecated
    public void test_emisionCuotaAnual() {
        try {
            Periodo periodo = new Periodo();
            periodo.setId(4L);
            batch.runCuotaAnual(periodo);
            
            Thread.currentThread().sleep(100000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public GeneracionDeudaRunBatch getBatch() {
        return batch;
    }

    public void setBatch(GeneracionDeudaRunBatch batch) {
        this.batch = batch;
    }

    public GeneralDAO getGeneralDAO() {
        return generalDAO;
    }

    public void setGeneralDAO(GeneralDAO generalDAO) {
        this.generalDAO = generalDAO;
    }

}
