package ar.com.jass.catastro.test.orm;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import ar.com.jass.catastro.business.CatastroService;
import ar.com.jass.catastro.dao.GeneralDAO;
import ar.com.jass.catastro.model.Cuenta;
import ar.com.jass.catastro.model.Domicilio;
import ar.com.jass.catastro.model.Dominio;
import ar.com.jass.catastro.model.Linea;
import ar.com.jass.catastro.model.Nomenclatura;
import ar.com.jass.catastro.model.Parcela;
import ar.com.jass.catastro.model.TipoDominio;
import ar.com.jass.catastro.model.TipoTramite;
import ar.com.jass.catastro.model.Tramite;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:application-context.xml" })
public class CatastroServiceTest {

    static Logger LOG = LoggerFactory.getLogger(CatastroServiceTest.class);

    @Autowired
    CatastroService catastroService;

    @Autowired
    @Qualifier("generalDAO")
    GeneralDAO generalDAO;

    @Test
    public void test_configuration() {
        //catastroService.subdividir(null, null, null, null, null);
    }

    @Test
    public void test_dummies() {
        Cuenta x = catastroService.findCuenta("", "");
        LOG.info("{}", x);
    }

    @Test
    public void test_findCuenta() {
        Cuenta c = catastroService.findCuenta("19", "4", "parcela.linea");

        // LOG.info("Parcela {}", c.getParcela().getId());
    }

    @Test
    @Transactional()
    @Rollback(value = false)
    public void test_subdividir() {
        Domicilio dom = generalDAO.find(Domicilio.class, 1L);
        Domicilio dom2 = new Domicilio(dom);
        dom2.setCalle("LAPRI");

        Cuenta origen = catastroService.findCuenta("214532", "3");
        List<Parcela> nuevasCuentas = new ArrayList<Parcela>();
        Nomenclatura n = new Nomenclatura();
        n.setCircunscripcion(2);
        n.setSeccion("s2");
        n.setParcela(2);
        
        Dominio d = new Dominio();
        d.setAnio(2011);
        d.setNumero(11);
        d.setTipo(TipoDominio.F);        
        
        Parcela p1 = new Parcela();
        p1.setNomenclatura(n);
        p1.setPartida(1113);        
        p1.setDomicilioInmueble(dom);
        p1.setDominio(d);

        nuevasCuentas.add(p1);
        Parcela p2 = new Parcela();
        p2.setNomenclatura(n);
        p2.setPartida(1114);       
        p2.setDomicilioInmueble(dom2);
        p2.setDominio(d);

        Linea lineav = new Linea();
        lineav.setFecha(new Date());
        lineav.setSuperficieCubierta(1d);
        lineav.setSuperficieSemi(1d);
        lineav.setValorTierra(BigDecimal.valueOf(1000d));
        
        p2.setLineaVigente(lineav);
        nuevasCuentas.add(p2);

        Tramite tramite = new Tramite();        
        tramite.setTipo(TipoTramite.SUB);
        
        //catastroService.subdividir(origen, nuevasCuentas, tramite, origen.getParcela().getTitulares());
    }

    @Test
    @Transactional()
    @Rollback(value = false)
    public void test_unificar() {
        Parcela a = generalDAO.find(Parcela.class, 79900L);
        Parcela b = generalDAO.find(Parcela.class, 79901L);

        List<Parcela> origenes = new ArrayList<Parcela>();
        origenes.add(a);
        origenes.add(b);

        Parcela nuevaParcela = new Parcela();
        nuevaParcela.setNomenclatura(new Nomenclatura(a.getNomenclatura()));
        nuevaParcela.setDomicilioInmueble(new Domicilio(a.getDomicilioInmueble()));     
        nuevaParcela.setPartida(998877);
        Linea linea = new Linea();
        linea.setSuperficieCubierta(1d);
        linea.setSuperficieSemi(1d);
        linea.setSuperficieTerreno(1d);
        linea.setValorComun(BigDecimal.valueOf(1));
        linea.setValorEdificio(BigDecimal.valueOf(1));
        linea.setValorTierra(BigDecimal.valueOf(1));
        nuevaParcela.setLineaVigente(linea);
    // linea.setParcela(nuevaParcela);

        Dominio d = new Dominio();
        d.setAnio(2011);
        d.setNumero(11);
        d.setTipo(TipoDominio.F);
        nuevaParcela.setDominio(d);

        Tramite tramite = new Tramite();     
        tramite.setTipo(TipoTramite.UNI);
        // generalDAO.save(nuevaParcela);
        //catastroService.unificar(origenes, nuevaParcela, a.getTitulares(), tramite);
    }

    @Test
    @Transactional
    public void test_loadParcela() {
        Parcela a = generalDAO.find(Parcela.class, 119847L);
        System.out.println(a.getLineaVigente().getId());        
    }
}
