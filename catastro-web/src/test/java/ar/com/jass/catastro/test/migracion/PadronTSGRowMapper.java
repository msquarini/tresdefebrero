package ar.com.jass.catastro.test.migracion;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

/**
 * Mapeo de tabla padron_tsg
 * 
 * @author msquarini
 *
 */
public class PadronTSGRowMapper implements RowMapper<PadronTSG> {

	@Override
	public PadronTSG mapRow(ResultSet rs, int rowNum) throws SQLException {
	    PadronTSG abl = new PadronTSG();
		
		abl.setCuenta(rs.getLong("cuenta"));
		abl.setDigito(rs.getString("digito"));
		abl.setPartida(rs.getString("partida"));
		abl.setAnio(rs.getString("anio"));
		abl.setMarca_baja(rs.getString("marca_baja"));
		
		abl.setNombrecalle(rs.getString("nombrecalle"));
		abl.setNropuerta(rs.getString("nropuerta"));
		//abl.setPiso(rs.getString("piso"));
		//abl.setDepartamento(rs.getString("dpto"));
		abl.setLocalidad(rs.getString("localidad"));
		//abl.setLocalidad(rs.getString("localidad_desc"));
		abl.setAppynom(rs.getString("appynom"));
		
		abl.setCategoria(rs.getInt("categoria"));
		abl.setZona(rs.getString("zona"));
		abl.setVtotal(rs.getBigDecimal("vtotal"));
		abl.setVedificio(rs.getBigDecimal("vedificio"));
		abl.setVtierra(rs.getBigDecimal("vtierra"));
		
		//abl.setNomenclatura(NomenclaturaMapper.map(rs));
		
		return abl;
	}

}
