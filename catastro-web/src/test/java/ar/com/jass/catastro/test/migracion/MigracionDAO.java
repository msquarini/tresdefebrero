package ar.com.jass.catastro.test.migracion;

import java.util.List;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@SuppressWarnings("unchecked")
@Repository("migracionDAO")
public class MigracionDAO {
    private static final Logger logger = LoggerFactory.getLogger("JdbcConsultaDao");
    private JdbcTemplate jdbcTemplate;

    public final static String QUERY = "select * from padron_tsg where cuenta < 324021";

    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public List<PadronTSG> findTSG() {
        List<PadronTSG> beans;
        try {
            beans = this.jdbcTemplate.query(QUERY, new Object[] {}, new PadronTSGRowMapper());

        } catch (EmptyResultDataAccessException e) {
            logger.warn("sin datos a migrar", e);
            return null;
        }
        return beans;
    }
}
