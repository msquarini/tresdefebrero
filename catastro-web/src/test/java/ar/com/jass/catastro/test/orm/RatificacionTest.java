package ar.com.jass.catastro.test.orm;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import ar.com.jass.catastro.business.ConsultaService;
import ar.com.jass.catastro.business.RatificacionService;
import ar.com.jass.catastro.dao.GeneralDAO;
import ar.com.jass.catastro.model.Tramite;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:application-context-test.xml" })
public class RatificacionTest {

	static Logger LOG = LoggerFactory.getLogger(RatificacionTest.class);

	@Autowired
	RatificacionService ratificacionService;

	@Autowired
	ConsultaService consultaService;

	@Autowired
	@Qualifier("generalDAO")
	GeneralDAO generalDAO;

	@Test
	@Transactional
	@Rollback(false)
	public void test_findRatificacionTramite() {
		Tramite tramite = consultaService.findTramite(119l);
		System.out.println(tramite);
	}

	@Test
	@Transactional
	@Rollback(false)
	public void test_procesarRatificacion() {
		Tramite tramite = consultaService.findTramite(127l);
		try {
			tramite = ratificacionService.procesarTramiteRatificacion(tramite);
			System.out.println(tramite);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
