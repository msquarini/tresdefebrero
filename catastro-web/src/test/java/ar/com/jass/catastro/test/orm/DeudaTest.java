package ar.com.jass.catastro.test.orm;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import ar.com.jass.catastro.business.BoletaService;
import ar.com.jass.catastro.business.CatastroService;
import ar.com.jass.catastro.business.CuentaCorrienteService;
import ar.com.jass.catastro.dao.CuentaCorrienteDAO;
import ar.com.jass.catastro.dao.GeneralDAO;
import ar.com.jass.catastro.model.Cuenta;
import ar.com.jass.catastro.model.Linea;
import ar.com.jass.catastro.model.TipoTramite;
import ar.com.jass.catastro.model.Tramite;
import ar.com.jass.catastro.model.cuentacorriente.Boleta;
import ar.com.jass.catastro.model.cuentacorriente.CuentaCorriente;
import ar.com.jass.catastro.model.cuentacorriente.Periodo;
import ar.com.jass.catastro.model.pagos.Pago;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:application-context-test.xml" })
public class DeudaTest {

    static Logger LOG = LoggerFactory.getLogger(DeudaTest.class);

    @Autowired
    CatastroService catastroService;

    @Autowired
    @Qualifier("generalDAO")
    GeneralDAO generalDAO;

    @Autowired
    CuentaCorrienteDAO ccDAO;

    @Autowired
    BoletaService boletaService;

    @Autowired
    CuentaCorrienteService cuentaCorrienteService;

    @Test
    @Transactional
    @Rollback(false)
    public void test_find_boleta() {
        Boleta boleta = generalDAO.find(Boleta.class, 1L);
        System.out.println(boleta);
    }

    @Test
    @Transactional
    @Rollback(false)
    public void test_pago() {
        Pago pago = new Pago();
        pago.setAnio(2018);
        pago.setCuota(6);
        pago.setCuenta("214538");
        pago.setDv("-");

        Boleta boleta = cuentaCorrienteService.findBoleta(pago);
        System.out.println(boleta);
    }

    @Test
    @Rollback(false)
    public void test_emitir() {
        LOG.info("test emision");
        List<Periodo> periodos = cuentaCorrienteService.findPeriodos(2019);
        cuentaCorrienteService.emisionDeudaAnual(periodos);
        
        try {
            Thread.sleep(1000 * 60 * 10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    @Rollback(false)
    @Transactional
    public void test_boleta() {
        LOG.info("test boleta");

        Periodo p = ccDAO.findPeriodo(2018, 2);
        Cuenta cuenta = generalDAO.find(Cuenta.class, 572571L);

        List<Boleta> boletas = boletaService.generarBoleta(cuenta, p, false, true, true);
        for (Boleta b : boletas) {
            System.out.println(b);
           // generalDAO.save(b);
        }
    }

    @Test
    @Rollback(false)
    @Transactional
    public void test_ajuste() {
        LOG.info("test ajuste");
        Calendar c = Calendar.getInstance();
        c.add(Calendar.MONTH, -3);
        Linea linea = new Linea();
        linea.setFecha(c.getTime());
        linea.setValorComun(BigDecimal.valueOf(10000L));
        linea.setValorEdificio(BigDecimal.valueOf(10000L));
        linea.setValorTierra(BigDecimal.valueOf(10000L));

        Cuenta cuenta = generalDAO.find(Cuenta.class, 572571L);
        Tramite t = new Tramite(TipoTramite.LIN);
        try {
            List<CuentaCorriente> ajustes = cuentaCorrienteService.ajusteCuentaCorriente(linea, cuenta, 1, t);
            for (CuentaCorriente cc : ajustes) {
                System.out.println(cc);
                generalDAO.save(cc);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    @Test
    @Rollback(false)
    public void test_boleta_batch() {
        Periodo p = cuentaCorrienteService.findPeriodo(2018, 2);
        List<Periodo> periodos = new ArrayList<Periodo>();
        periodos.add(p);
        boletaService.generacionBoletasBatch(periodos);
        try {
            Thread.sleep(1000 * 60 * 5);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    
    @Test
    @Rollback(false)
    @Transactional
    public void test_hql() {
        List<Cuenta> cuenta = ccDAO.hqlsample();
        System.out.println(cuenta.size());
    }
}
