package ar.com.jass.catastro.test.orm;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import ar.com.jass.catastro.business.CatastroService;
import ar.com.jass.catastro.dao.GeneralDAO;
import ar.com.jass.catastro.model.Cuenta;
import ar.com.jass.catastro.model.da.DANovedad;
import ar.com.jass.catastro.model.da.DebitoAutomatico;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:application-context.xml" })
public class DAServiceTest {

    static Logger LOG = LoggerFactory.getLogger(DAServiceTest.class);

    @Autowired
    CatastroService catastroService;

    @Autowired
    @Qualifier("generalDAO")
    GeneralDAO generalDAO;

    @Test
    @Transactional
    @Rollback(false)
    public void test_altaDA() {
        Cuenta c = generalDAO.findCuenta("cuenta1", "1");
        DebitoAutomatico da = new DebitoAutomatico();
        da.setBanco("Prueba");
        da.setCbu("cbu");
        da.setCuenta(c);
        da.setNovedad(DANovedad.ALTA);
        da.setFecha(new Date());

        generalDAO.save(da);
    }

    @Test
    @Transactional
    public void test_findDA() {
        DebitoAutomatico da = generalDAO.findDA("cuenta1", "1");

        System.out.println(da);
    }
}
