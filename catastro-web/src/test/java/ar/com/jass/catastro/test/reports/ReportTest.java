package ar.com.jass.catastro.test.reports;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import ar.com.jass.catastro.batch.pagos.PagosJobLauncher;
import ar.com.jass.catastro.dao.PagosDAO;
import ar.com.jass.catastro.model.pagos.EstadoFilePagos;
import ar.com.jass.catastro.model.pagos.FilePago;
import ar.com.jass.catastro.report.impl.ReportsHelper;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/reports/reports-context-test.xml" })
public class ReportTest {

    static Logger LOG = LoggerFactory.getLogger(ReportTest.class);

    @Autowired
    ReportsHelper helper;

    @Test
    public void test_config() {
        for (String r: helper.getReportFiles()) {
            System.out.println(r);
        }
    }

}
