package ar.com.jass.catastro.test.orm;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import ar.com.jass.catastro.business.CatastroService;
import ar.com.jass.catastro.business.CuentaCorrienteService;
import ar.com.jass.catastro.dao.CuentaCorrienteDAO;
import ar.com.jass.catastro.dao.GeneralDAO;
import ar.com.jass.catastro.exceptions.BusinessException;
import ar.com.jass.catastro.model.Cuenta;
import ar.com.jass.catastro.model.cuentacorriente.CuentaCorriente;
import ar.com.jass.catastro.model.cuentacorriente.Deuda;
import ar.com.jass.catastro.model.cuentacorriente.DeudaPeriodoStatus;
import ar.com.jass.catastro.model.cuentacorriente.GeneracionDeudaDTO;
import ar.com.jass.catastro.model.cuentacorriente.Periodo;
import ar.com.jass.catastro.model.cuentacorriente.TipoMovimiento;
import ar.com.jass.catastro.model.pagos.FilePago;
import ar.com.jass.catastro.model.pagos.Pago;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:application-context-test.xml" })
public class CCTest {

	static Logger LOG = LoggerFactory.getLogger(CCTest.class);

    @Autowired
    CatastroService catastroService;

    @Autowired
    @Qualifier("generalDAO")
    GeneralDAO generalDAO;

    @Autowired  
    CuentaCorrienteDAO ccDAO;

    
    @Autowired
    CuentaCorrienteService cuentaCorrienteService;
    
    @Test
    @Transactional
    @Rollback(false)
    public void test_generarDeuda_desde() {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.MONTH, 0);
        Cuenta cuenta = generalDAO.find(Cuenta.class, 612365L);
        try {
           List<CuentaCorriente> ccs = cuentaCorrienteService.generarDeuda(c.getTime(), cuenta, false);
           for (CuentaCorriente cc : ccs) {
               System.out.println(cc);
           }
        } catch (BusinessException e) {
            e.printStackTrace();
        }
    }
    
    @Test
    @Transactional
    @Rollback(false)
    public void test_pago() {
        Pago pago = new Pago();
        pago.setAnio(2017);
        pago.setCuota(12);
        pago.setCuenta("214538");
        pago.setDv("-");
        pago.setFileDetail("lineadelpago en el file");
        Calendar c = Calendar.getInstance();
        c.set(2017, 11, 26);
        pago.setFechaVto(c.getTime());
        pago.setImporte(BigDecimal.valueOf(100L));
        pago.setProcesado(Boolean.TRUE);
        
     /*   Deuda deuda = cuentaCorrienteService.findCuentaCorriente(pago);
        List<CuentaCorriente> creditos = deuda.generarMovimientosCredito(pago);
        generalDAO.save(pago);
     
        generalDAO.saveList(creditos);        
        generalDAO.save(deuda);
     */   
    }
    
    @Test
    @Rollback(false)
    public void test_generacionDeuda() {
    	Periodo p = new Periodo();
    	p.setAnio(2018);
    	p.setCuota(1);
    	p.setFechaVto(Calendar.getInstance().getTime());
    	p.setGenerarAnual(true);
    	p.setNumeroCuotaAnual(13);
    	p.setPeriodosAnual(12);
    	//cuentaCorrienteService.generarCuotas(p);
    }
  
    @Test
    @Transactional
    @Rollback(false)
    public void test_consultaMovimientos() {
    	Cuenta c = generalDAO.find(Cuenta.class, 572402L);
    	Integer anio = 2018;
    	TipoMovimiento tipo = null;
    	Boolean sinPago = false;
        List<CuentaCorriente> movs = cuentaCorrienteService.findMovimientosCC(c, anio, tipo);
        for (CuentaCorriente cc : movs) {
        	System.out.println(cc);
        }
    }
    
    
    @Test
    @Transactional
    @Rollback(false)
    public void test_getPeriodoInState() {
        System.out.println(ccDAO.getPeriodoInState(DeudaPeriodoStatus.GENERADO));
    }
    
    
    @Test
    @Transactional
    @Rollback(false)
    public void test_consultaCuentas() {

        Periodo p = new Periodo();
        p.setAnio(2018);
        p.setCuota(2);
    	List<GeneracionDeudaDTO> dtos = ccDAO.getCuentasGeneracionDeudaDTO(p, null, false);
    	System.out.println(dtos.size());
    	Iterator<GeneracionDeudaDTO> it = dtos.iterator();
    	int i = 0;
    	while (it.hasNext()) {
    		System.out.println(it.next());
    		i++;
    	}
    	System.out.println(i);
    }
    
    @Test
    @Rollback(false)
    public void test_filepagos() {
    	// VERIFICAR el funcionamiento
    	// Ligar el pago al registro de CC, id_pago en CC apunta a registro de pago
    	FilePago fp = new FilePago();
    	fp.setCantidadRegistros(10);    	
    	fp.setFechaFile(Calendar.getInstance().getTime());
    	fp.setHash("111222333444");
    	fp.setNombre("Prueba");
    	
    	cuentaCorrienteService.save(fp);
    	        	
    	Pago p = new Pago();
    	p.setAnio(2018);
    	p.setCuenta("1111");
    	p.setCuota(1);
    	p.setDv("2");
    	p.setFechaVto(Calendar.getInstance().getTime());
    	p.setImporte(BigDecimal.valueOf(40L));
    	p.setProcesado(Boolean.TRUE);
    	//p.setValido(Boolean.TRUE);
    	
    	
    	//persistir PAGO!
    	cuentaCorrienteService.save(p);
    	
    	List<Pago> pagos = new ArrayList<>();
    	pagos.add(p);
    	fp.setPagos(pagos);    	
    	fp.setErrores(2);
    	fp.setTotal(BigDecimal.valueOf(100L));
    	
    	cuentaCorrienteService.save(fp);
    }
}
