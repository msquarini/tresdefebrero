package ar.com.jass.catastro.test;

import java.math.BigDecimal;
import java.util.Date;

import com.opensymphony.xwork2.conversion.impl.NumberConverter;

import ar.com.jass.catastro.business.impl.FileType;
import ar.com.jass.catastro.model.pagos.FilePago;
import ar.com.jass.catastro.web.converter.DateConverter;
import ar.com.jass.catastro.web.converter.EnumConverter;

public class ConverterTest {

	public static void main(String[] args) {

		ConverterTest ct = new ConverterTest();
		ct.enumConverter();
	}

	public void enumConverter() {
		EnumConverter ec = new EnumConverter();
		System.out.println(ec.convertToString(null, FileType.LINK));
		FileType ft = (FileType)ec.convertFromString(null, new String[] {"3"}, FileType.class);
		
		System.out.println(ft.getDescripcion());
	}

	public void dateConverter() {
		DateConverter dc = new DateConverter();
		String d = dc.convertToString(null, new Date());
		System.out.println(d);

		Date to = (Date) dc.convertFromString(null, new String[] { d }, Date.class);
		System.out.println(to);
	}

	public void bigdecimalConverter() {
		NumberConverter nc = new NumberConverter();
		BigDecimal bd = new BigDecimal(2.5);
		String bds = (String) nc.convertValue(bd, String.class);
		System.out.println(bds);

		System.out.println((BigDecimal) nc.convertValue(bds, BigDecimal.class));
		System.out.println(bd);
	}
}