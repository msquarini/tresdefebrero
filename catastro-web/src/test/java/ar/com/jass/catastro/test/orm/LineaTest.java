package ar.com.jass.catastro.test.orm;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import ar.com.jass.catastro.business.CatastroService;
import ar.com.jass.catastro.business.ConsultaService;
import ar.com.jass.catastro.business.CuentaCorrienteService;
import ar.com.jass.catastro.dao.CuentaCorrienteDAO;
import ar.com.jass.catastro.dao.GeneralDAO;
import ar.com.jass.catastro.model.Cuenta;
import ar.com.jass.catastro.model.Linea;
import ar.com.jass.catastro.model.Tramite;
import ar.com.jass.catastro.model.cuentacorriente.CuentaCorriente;
import ar.com.jass.catastro.model.cuentacorriente.DeudaPeriodoStatus;
import ar.com.jass.catastro.model.cuentacorriente.GeneracionDeudaDTO;
import ar.com.jass.catastro.model.cuentacorriente.Periodo;
import ar.com.jass.catastro.model.cuentacorriente.TipoMovimiento;
import ar.com.jass.catastro.model.pagos.FilePago;
import ar.com.jass.catastro.model.pagos.Pago;
import ar.com.jass.catastro.model.temporal.LineaTemporal;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:application-context-test.xml" })
public class LineaTest {

	static Logger LOG = LoggerFactory.getLogger(LineaTest.class);

    @Autowired
    CatastroService catastroService;

    @Autowired
    ConsultaService consultaService;
    
    @Autowired
    @Qualifier("generalDAO")
    GeneralDAO generalDAO;

    @Autowired  
    CuentaCorrienteDAO ccDAO;

    
    @Autowired
    CuentaCorrienteService cuentaCorrienteService;
    
    
    @Test
    @Transactional
    @Rollback(false)
    public void test_findLineaTemporal() {
        Tramite tramite = consultaService.findTramite(114L);
        LineaTemporal lt = consultaService.findLineaTemporal(tramite);
        System.out.println(lt);
    }
   
    @Test
    @Transactional
    @Rollback(false)
    public void test_createLinea() {
        Linea l = new Linea();
        l.setSuperficieCubierta(100d);
        l.setSuperficieSemi(200d);
        l.setSuperficieTerreno(300d);
        l.setValorComun(BigDecimal.valueOf(1000D));
        l.setValorEdificio(BigDecimal.valueOf(1000D));
        l.setValorTierra(BigDecimal.valueOf(1000D));
        l.setFecha(new Date());
        generalDAO.save(l);        
    }
   
    @Test
    @Transactional
    @Rollback(false)
    public void test_createLineaTemporal() {
        Tramite t = generalDAO.find(Tramite.class, 115L);
        Linea l = generalDAO.find(Linea.class, 79867L);
        LineaTemporal lt = new LineaTemporal(l, 0, t);        
        generalDAO.save(lt);        
    }
}
