package ar.com.jass.catastro.test.migracion;

import java.sql.ResultSet;
import java.sql.SQLException;

import ar.com.jass.catastro.model.Nomenclatura;


public class NomenclaturaMapper {

	public static Nomenclatura map(ResultSet rs) throws SQLException {
		
		Nomenclatura nomenclatura = new Nomenclatura();
        nomenclatura.setCircunscripcion(rs.getInt("circ"));
        nomenclatura.setSeccion(rs.getString("sec"));
        nomenclatura.setFraccion(rs.getInt("cod_mz"));
        nomenclatura.setManzana(rs.getInt("mz"));
        nomenclatura.setManzanaLetra(rs.getString("mzl"));
        nomenclatura.setParcela(rs.getInt("pc"));
        nomenclatura.setParcelaLetra(rs.getString("pcl"));
        nomenclatura.setUnidadFuncional(rs.getString("uf"));
        
        return nomenclatura;
	}
}
