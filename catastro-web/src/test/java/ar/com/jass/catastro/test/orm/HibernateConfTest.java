package ar.com.jass.catastro.test.orm;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:application-context.xml" })
public class HibernateConfTest {

	static Logger LOG = LoggerFactory.getLogger(HibernateConfTest.class);
	
	@Test
	public void test_configuration() {
		LOG.info("OK");
	}
}
