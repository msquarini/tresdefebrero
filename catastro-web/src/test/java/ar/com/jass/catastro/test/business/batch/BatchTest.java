package ar.com.jass.catastro.test.business.batch;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.item.file.transform.LineAggregator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import ar.com.jass.catastro.batch.deuda.file.DeudaDTO;
import ar.com.jass.catastro.batch.deuda.file.DeudaFileJobLauncher;
import ar.com.jass.catastro.batch.pagos.PagosJobLauncher;
import ar.com.jass.catastro.business.impl.FileType;
import ar.com.jass.catastro.dao.GeneralDAO;
import ar.com.jass.catastro.dao.PagosDAO;
import ar.com.jass.catastro.model.cuentacorriente.CuentaCorriente;
import ar.com.jass.catastro.model.cuentacorriente.Deuda;
import ar.com.jass.catastro.model.cuentacorriente.Periodo;
import ar.com.jass.catastro.model.pagos.EstadoFilePagos;
import ar.com.jass.catastro.model.pagos.FilePago;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:application-context-test.xml" })
public class BatchTest {

    static Logger LOG = LoggerFactory.getLogger(BatchTest.class);

    @Autowired
    private PagosDAO pagosDAO;

    @Autowired
    private GeneralDAO generalDAO;

    @Autowired
    private JobLauncher jobLauncher;

    @Autowired
    @Qualifier("jobLauncherAsync")
    private JobLauncher jobAsyncLauncher;

    // @Autowired
    // @Qualifier("generacionDeudaJOB")
    private Job job;

    /*
     * @Autowired
     * 
     * @Qualifier("pmcJOB")
     */
    private Job jobPMC;

    @Autowired
    @Qualifier("pagosJobLauncher")
    private PagosJobLauncher pagosJobLauncher;

    @Autowired
    private DeudaFileJobLauncher deudaFileJobLauncher;

    @Autowired
    @Qualifier("pmc-aggregator")
    LineAggregator<DeudaDTO> aggregator;

    @Test
    @Transactional
    public void test_aggregatorPMC() {
        try {
            Deuda d = generalDAO.find(CuentaCorriente.class, 371204L);
            DeudaDTO dto = new DeudaDTO(d);
            System.out.println(aggregator.aggregate(dto));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test_launchJobFileDeuda() {
        // Periodo p = generalDAO.find(Periodo.class, 1);
        Periodo p = new Periodo();
        p.setId(4L);
        FileType[] tipos = new FileType[] { FileType.LINK };
        deudaFileJobLauncher.launchJob(tipos, p);
    }

    @Test
    public void test_launchJobPagos() {

        List<FilePago> pagos = pagosDAO.getFilePagos(new EstadoFilePagos[] { EstadoFilePagos.PENDIENTE });

        for (FilePago pago : pagos) {
            pagosJobLauncher.launchJob(pago);
        }

        try {
            Thread.currentThread().sleep(100000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test_jobAsyncPagos() {

        try {
            // JobParameters param = new JobParametersBuilder().toJobParameters();

            JobParameters param = new JobParametersBuilder().addLong("idFilePago", 9L).toJobParameters();
            JobExecution execution = jobAsyncLauncher.run(jobPMC, param);
            LOG.info("Exit Status : " + execution.getStatus());
            for (Throwable th : execution.getAllFailureExceptions()) {
                LOG.error(" - ", th);
            }
        } catch (Exception e) {
            LOG.error("Error en procesamiento de file de pagos", e);
        }
    }

    @Test
    public void test_jobGeneracionDeuda() {

        try {
            JobParameters param = new JobParametersBuilder().toJobParameters();
            JobExecution execution = jobLauncher.run(job, param);

            LOG.info("Exit Status : " + execution.getStatus());
            for (Throwable th : execution.getAllFailureExceptions()) {
                LOG.error(" - ", th);
            }
        } catch (Exception e) {
            LOG.error("Error en generacion de file para mainframe", e);
        }
    }

    @Test
    public void test_Checksum() {
        /*
         * ChecksumTasklet tasklet = new ChecksumTasklet(); File file = new
         * File("C:\\Tools\\eclipse\\eclipse.ini"); try { tasklet.checksum(file); } catch
         * (FileNotFoundException e) { // 
         */
    }

    @Test
    public void test_pmcreader() {

        try {

            String fileLocation = "C:\\pmc_sample.txt";

            JobParameters param = new JobParametersBuilder().addString("fileLocation", fileLocation).toJobParameters();

            JobExecution execution = jobLauncher.run(jobPMC, param);
            List<String> xx = (List<String>) execution.getExecutionContext().get("fallidos");
            System.out.println(xx);

            LOG.info("Exit Status : " + execution.getStatus());
            for (Throwable th : execution.getAllFailureExceptions()) {
                LOG.error(" - ", th);
            }

        } catch (Exception e) {
            LOG.error("Error en generacion de file para mainframe", e);
        }
    }

    public JobLauncher getJobLauncher() {
        return jobLauncher;
    }

    public void setJobLauncher(JobLauncher jobLauncher) {
        this.jobLauncher = jobLauncher;
    }

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }

}
