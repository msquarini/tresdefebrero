package ar.com.jass.catastro.test.migracion;

import ar.com.jass.catastro.model.Barrio;
import ar.com.jass.catastro.model.Cuenta;
import ar.com.jass.catastro.model.Domicilio;
import ar.com.jass.catastro.model.Linea;
import ar.com.jass.catastro.model.Parcela;
import ar.com.jass.catastro.model.Titular;

public class MigracionConverter {

    public Cuenta crearCuenta(PadronTSG padron) {
        Cuenta c = new Cuenta();

        c.setCuenta(String.valueOf(padron.getCuenta()));
        c.setDv(padron.getDigito());
        c.setActiva(padron.getMarca_baja() != null);
        /*
         * c.setFechaAlta(new Date()); c.setUserAlta("migracion");
         */
        Parcela parcela = crearParcela(padron);
        // c.setParcela(parcela);
        parcela.setCuenta(c);

        return c;
    }

    private Parcela crearParcela(PadronTSG padron) {
        Parcela p = new Parcela();

        p.setNomenclatura(padron.getNomenclatura());
        p.setActiva(padron.getMarca_baja() != null);

        //p.setCategoria(padron.getCategoria());

        Linea lv = new Linea();
        // lv.setValorComun(padron.getv);
        lv.setValorEdificio(padron.getVedificio());
        lv.setValorTierra(padron.getVtierra());
        lv.setVigente(true);
        
        lv.setCategoria(padron.getCategoria());

        lv.setFecha(padron.getFecha());

        p.setLineaVigente(lv);
        p.setPartida(Integer.valueOf(padron.getPartida()));

        Domicilio d = crearDomicilio(padron);
        p.setDomicilioInmueble(d);
        //p.setDomicilioPostal(d);

        Titular t = crearTitular(padron);
        //p.getTitulares().add(t);

        return p;
    }

    private Domicilio crearDomicilio(PadronTSG padron) {
        Domicilio d = new Domicilio();
        d.setCalle(padron.getNombrecalle());
        d.setNumero(padron.getNropuerta());
        d.setCodigoPostal(padron.getLocalidad());

        Barrio barrio = new Barrio();
        barrio.setId(1L);
        d.setBarrio(barrio);
        return d;
    }

    private Titular crearTitular(PadronTSG padron) {
        Titular t = new Titular();
        t.setApellido(padron.getAppynom());
        t.setDocumento(0);
        t.setPersonaJuridica(false);

        return t;
    }
}
