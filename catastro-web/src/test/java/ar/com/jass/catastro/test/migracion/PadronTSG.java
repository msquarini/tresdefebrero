package ar.com.jass.catastro.test.migracion;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;

import ar.com.jass.catastro.model.Nomenclatura;

public class PadronTSG {

    Long cuenta;
    String digito;
    Nomenclatura nomenclatura;
    String zona;
    BigDecimal vtierra;
    BigDecimal vedificio;
    BigDecimal vtotal;
    Integer anioval;
    Integer mtsfte;
    Integer supm;
    Integer coddist;
    String codpropietario;
    String codcalle;
    String nombrecalle;
    String nropuerta;
    String appynom;
    String codcallebis;
    String nombrecallebis;
    String nropuertabis;
    String piso;
    String dpto;
    String localidad;
    Integer categoria;
    String categoriabco;
    String ctaorigen;
    String digitoorigen;
    Date fechasubdivision;
    String dominio;
    String serie;
    String anio;
    String partida;
    String dvpartida;
    String marca_especial;
    String marca_baja;
    String marca_certificada;
    String marca_citacion;
    String nueva_localidad;
    String piso_bis;
    String dpto_bis;
    String localidad_bis;
    String pago_anual;
    BigDecimal importe_ult_cuota;

    /**
     * Devolver fecha de valuacion
     * 
     * @return
     */
    public Date getFecha() {
        Calendar c = Calendar.getInstance();
        try {
            c.set(Calendar.DAY_OF_YEAR, Integer.valueOf(getAnio()));
            c.set(Calendar.MONTH, 0);
            c.set(Calendar.DATE, 1);
        } catch (Exception e) {
            
        }
        return c.getTime();
    }

    public Long getCuenta() {
        return cuenta;
    }

    public void setCuenta(Long cuenta) {
        this.cuenta = cuenta;
    }

    public String getDigito() {
        return digito;
    }

    public void setDigito(String digito) {
        this.digito = digito;
    }

    public Nomenclatura getNomenclatura() {
        return nomenclatura;
    }

    public void setNomenclatura(Nomenclatura nomenclatura) {
        this.nomenclatura = nomenclatura;
    }

    public String getZona() {
        return zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }

    public BigDecimal getVtierra() {
        return vtierra;
    }

    public void setVtierra(BigDecimal vtierra) {
        this.vtierra = vtierra;
    }

    public BigDecimal getVedificio() {
        return vedificio;
    }

    public void setVedificio(BigDecimal vedificio) {
        this.vedificio = vedificio;
    }

    public BigDecimal getVtotal() {
        return vtotal;
    }

    public void setVtotal(BigDecimal vtotal) {
        this.vtotal = vtotal;
    }

    public Integer getAnioval() {
        return anioval;
    }

    public void setAnioval(Integer anioval) {
        this.anioval = anioval;
    }

    public Integer getMtsfte() {
        return mtsfte;
    }

    public void setMtsfte(Integer mtsfte) {
        this.mtsfte = mtsfte;
    }

    public Integer getSupm() {
        return supm;
    }

    public void setSupm(Integer supm) {
        this.supm = supm;
    }

    public Integer getCoddist() {
        return coddist;
    }

    public void setCoddist(Integer coddist) {
        this.coddist = coddist;
    }

    public String getCodpropietario() {
        return codpropietario;
    }

    public void setCodpropietario(String codpropietario) {
        this.codpropietario = codpropietario;
    }

    public String getCodcalle() {
        return codcalle;
    }

    public void setCodcalle(String codcalle) {
        this.codcalle = codcalle;
    }

    public String getNombrecalle() {
        return nombrecalle;
    }

    public void setNombrecalle(String nombrecalle) {
        this.nombrecalle = nombrecalle;
    }

    public String getNropuerta() {
        return nropuerta;
    }

    public void setNropuerta(String nropuerta) {
        this.nropuerta = nropuerta;
    }

    public String getAppynom() {
        return appynom;
    }

    public void setAppynom(String appynom) {
        this.appynom = appynom;
    }

    public String getCodcallebis() {
        return codcallebis;
    }

    public void setCodcallebis(String codcallebis) {
        this.codcallebis = codcallebis;
    }

    public String getNombrecallebis() {
        return nombrecallebis;
    }

    public void setNombrecallebis(String nombrecallebis) {
        this.nombrecallebis = nombrecallebis;
    }

    public String getNropuertabis() {
        return nropuertabis;
    }

    public void setNropuertabis(String nropuertabis) {
        this.nropuertabis = nropuertabis;
    }

    public String getPiso() {
        return piso;
    }

    public void setPiso(String piso) {
        this.piso = piso;
    }

    public String getDpto() {
        return dpto;
    }

    public void setDpto(String dpto) {
        this.dpto = dpto;
    }

    public String getLocalidad() {
        return localidad;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    public Integer getCategoria() {
        return categoria;
    }

    public void setCategoria(Integer categoria) {
        this.categoria = categoria;
    }

    public String getCategoriabco() {
        return categoriabco;
    }

    public void setCategoriabco(String categoriabco) {
        this.categoriabco = categoriabco;
    }

    public String getCtaorigen() {
        return ctaorigen;
    }

    public void setCtaorigen(String ctaorigen) {
        this.ctaorigen = ctaorigen;
    }

    public String getDigitoorigen() {
        return digitoorigen;
    }

    public void setDigitoorigen(String digitoorigen) {
        this.digitoorigen = digitoorigen;
    }

    public Date getFechasubdivision() {
        return fechasubdivision;
    }

    public void setFechasubdivision(Date fechasubdivision) {
        this.fechasubdivision = fechasubdivision;
    }

    public String getDominio() {
        return dominio;
    }

    public void setDominio(String dominio) {
        this.dominio = dominio;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public String getAnio() {
        return anio;
    }

    public void setAnio(String anio) {
        this.anio = anio;
    }

    public String getPartida() {
        return partida;
    }

    public void setPartida(String partida) {
        this.partida = partida;
    }

    public String getDvpartida() {
        return dvpartida;
    }

    public void setDvpartida(String dvpartida) {
        this.dvpartida = dvpartida;
    }

    public String getMarca_especial() {
        return marca_especial;
    }

    public void setMarca_especial(String marca_especial) {
        this.marca_especial = marca_especial;
    }

    public String getMarca_baja() {
        return marca_baja;
    }

    public void setMarca_baja(String marca_baja) {
        this.marca_baja = marca_baja;
    }

    public String getMarca_certificada() {
        return marca_certificada;
    }

    public void setMarca_certificada(String marca_certificada) {
        this.marca_certificada = marca_certificada;
    }

    public String getMarca_citacion() {
        return marca_citacion;
    }

    public void setMarca_citacion(String marca_citacion) {
        this.marca_citacion = marca_citacion;
    }

    public String getNueva_localidad() {
        return nueva_localidad;
    }

    public void setNueva_localidad(String nueva_localidad) {
        this.nueva_localidad = nueva_localidad;
    }

    public String getPiso_bis() {
        return piso_bis;
    }

    public void setPiso_bis(String piso_bis) {
        this.piso_bis = piso_bis;
    }

    public String getDpto_bis() {
        return dpto_bis;
    }

    public void setDpto_bis(String dpto_bis) {
        this.dpto_bis = dpto_bis;
    }

    public String getLocalidad_bis() {
        return localidad_bis;
    }

    public void setLocalidad_bis(String localidad_bis) {
        this.localidad_bis = localidad_bis;
    }

    public String getPago_anual() {
        return pago_anual;
    }

    public void setPago_anual(String pago_anual) {
        this.pago_anual = pago_anual;
    }

    public BigDecimal getImporte_ult_cuota() {
        return importe_ult_cuota;
    }

    public void setImporte_ult_cuota(BigDecimal importe_ult_cuota) {
        this.importe_ult_cuota = importe_ult_cuota;
    }

}
