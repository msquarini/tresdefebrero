package ar.com.jass.catastro.test.business;

import java.math.BigDecimal;
import java.util.Calendar;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import ar.com.jass.catastro.business.CatastroService;
import ar.com.jass.catastro.business.CuentaCorrienteService;
import ar.com.jass.catastro.dao.CuentaCorrienteDAO;
import ar.com.jass.catastro.dao.GeneralDAO;
import ar.com.jass.catastro.model.Cuenta;
import ar.com.jass.catastro.model.Linea;
import ar.com.jass.catastro.model.TipoTramite;
import ar.com.jass.catastro.model.Tramite;
import ar.com.jass.catastro.model.temporal.LineaTemporal;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:application-context-test.xml" })
public class CuentaCorrienteTest {

    static Logger LOG = LoggerFactory.getLogger(CuentaCorrienteTest.class);

    @Autowired
    CatastroService catastroService;

    @Autowired
    @Qualifier("generalDAO")
    GeneralDAO generalDAO;

    @Autowired
    CuentaCorrienteDAO ccDAO;

    @Autowired
    CuentaCorrienteService cuentaCorrienteService;

    @Test
    @Transactional
    @Rollback(false)
    public void test_ajusteLinea() {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.MONTH, 2);

        Linea nuevaLinea = new Linea();
        nuevaLinea.setCategoria(3);
        nuevaLinea.setFecha(c.getTime());
        nuevaLinea.setSuperficieCubierta(10000d);
        nuevaLinea.setSuperficieSemi(10000d);
        nuevaLinea.setSuperficieTerreno(10000d);
        nuevaLinea.setValorComun(BigDecimal.valueOf(50000D));
        nuevaLinea.setValorEdificio(BigDecimal.valueOf(50000D));
        nuevaLinea.setValorComun(BigDecimal.valueOf(50000D));

        Cuenta cuenta = generalDAO.find(Cuenta.class, 572452L);
        Tramite t = new Tramite(TipoTramite.LIN);
        try {
            cuentaCorrienteService.ajusteCuentaCorriente(nuevaLinea, cuenta, 1, t);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    @Transactional
    @Rollback(false)
    public void testLineaTemporal() {
        Tramite tramite = generalDAO.find(Tramite.class, 1L);
        Linea linea = generalDAO.find(Linea.class, 612365L);

        LineaTemporal lineaTemporal = new LineaTemporal(linea, 1, tramite);
        generalDAO.save(lineaTemporal);
    }
}
