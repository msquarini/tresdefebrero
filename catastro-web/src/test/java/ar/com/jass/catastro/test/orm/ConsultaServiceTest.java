package ar.com.jass.catastro.test.orm;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import ar.com.jass.catastro.business.ConsultaService;
import ar.com.jass.catastro.dao.GeneralDAO;
import ar.com.jass.catastro.model.Nomenclatura;
import ar.com.jass.catastro.model.Parcela;
import ar.com.jass.catastro.model.Titular;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:application-context-test.xml" })
public class ConsultaServiceTest {

    static Logger LOG = LoggerFactory.getLogger(ConsultaServiceTest.class);

    @Autowired
    ConsultaService consultaService;

    @Autowired
    @Qualifier("generalDAO")
    GeneralDAO generalDAO;

    @Test
    @Transactional
    @Rollback(false)
    public void test_FindTitulares() {
        Titular t = new Titular();
        // t.setApellido("ape");
        t.setCuitcuil("1234-310");
        LOG.info("{}", consultaService.findTitulares(t).get(0));

    }

    @Test   
    @Transactional
    public void test_FindParcelaById() {
        Parcela p = consultaService.findParcelaById(79900L);
       // Cuenta p = consultaService.findCuentaById(572402L);
        System.out.println(p.getCuenta());
        //LOG.info("{}", consultaService.findParcelaById(79900L));

    }

    @Test
    public void test_FindParcelaByNomenclatura() {
        Nomenclatura n = new Nomenclatura();
        // t.setApellido("ape");
        n.setCircunscripcion(4);
        n.setSeccion("B");
        n.setFraccion(3);
        n.setParcela(16);
        n.setParcelaLetra("C");
        List<Parcela> ps = consultaService.findParcelas(n);
        
        LOG.info("{}", ps.get(0).getCuenta());

    }

    public ConsultaService getConsultaService() {
        return consultaService;
    }

    public void setConsultaService(ConsultaService consultaService) {
        this.consultaService = consultaService;
    }

    public GeneralDAO getGeneralDAO() {
        return generalDAO;
    }

    public void setGeneralDAO(GeneralDAO generalDAO) {
        this.generalDAO = generalDAO;
    }

}
