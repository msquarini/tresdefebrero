package ar.com.jass.catastro.test.business;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import ar.com.jass.catastro.business.CatastroService;
import ar.com.jass.catastro.business.CuentaCorrienteService;
import ar.com.jass.catastro.business.deuda.model.ConceptoTasaMantenimiento;
import ar.com.jass.catastro.dao.CuentaCorrienteDAO;
import ar.com.jass.catastro.dao.GeneralDAO;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:application-context-test.xml" })
public class TemplateCuotaTest {

    static Logger LOG = LoggerFactory.getLogger(TemplateCuotaTest.class);

    @Autowired
    CatastroService catastroService;

    @Autowired
    @Qualifier("generalDAO")
    GeneralDAO generalDAO;

    @Autowired
    CuentaCorrienteDAO ccDAO;

    @Autowired
    CuentaCorrienteService cuentaCorrienteService;

    @Test
    @Transactional
    @Rollback(false)
    public void test_find() {
        ConceptoTasaMantenimiento tasa = generalDAO.find(ConceptoTasaMantenimiento.class, 1L);
        System.out.println(tasa);
    }

}
