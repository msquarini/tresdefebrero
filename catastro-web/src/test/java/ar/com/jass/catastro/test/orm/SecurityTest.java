package ar.com.jass.catastro.test.orm;

import java.util.ArrayList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:application-context-test.xml" })
public class SecurityTest {

	static Logger LOG = LoggerFactory.getLogger(SecurityTest.class);

	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	UserDetailsManager userDetailsManager;

	@Autowired
	PasswordEncoder passwordEncoder;

	@Test
	@Transactional
	public void test_login() {
		String username = "test2";
		String password = "mipassword";
		try {
			UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
					username.toLowerCase(), password);
			Authentication authenticated = authenticationManager.authenticate(authentication);

			System.out.println(authenticated);
		} catch (Exception e) {
			LOG.error("error login", e);
		}
	}

	@Test
	@Transactional
	@Rollback(false)
	public void test_createUSer() {
		String username = "test2";
		String password = "mipassword";
		try {
			UserDetails user = new User(username, passwordEncoder.encodePassword(password, null), new ArrayList<GrantedAuthority>());
			userDetailsManager.createUser(user);

		} catch (Exception e) {
			LOG.error("error createuser", e);
		}
	}

	public AuthenticationManager getAuthenticationManager() {
		return authenticationManager;
	}

	public void setAuthenticationManager(AuthenticationManager authenticationManager) {
		this.authenticationManager = authenticationManager;
	}

	public UserDetailsManager getUserDetailsManager() {
		return userDetailsManager;
	}

	public void setUserDetailsManager(UserDetailsManager userDetailsManager) {
		this.userDetailsManager = userDetailsManager;
	}

	public PasswordEncoder getPasswordEncoder() {
		return passwordEncoder;
	}

	public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
		this.passwordEncoder = passwordEncoder;
	}
}
