package ar.com.jass.catastro.test.orm;

import static org.junit.Assert.assertNotNull;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import ar.com.jass.catastro.business.CatastroService;
import ar.com.jass.catastro.dao.CuentaCorrienteDAO;
import ar.com.jass.catastro.dao.GeneralDAO;
import ar.com.jass.catastro.model.Circunscripcion;
import ar.com.jass.catastro.model.Cuenta;
import ar.com.jass.catastro.model.Linea;
import ar.com.jass.catastro.model.Nomenclatura;
import ar.com.jass.catastro.model.Parcela;
import ar.com.jass.catastro.model.Seccion;
import ar.com.jass.catastro.model.TipoTramite;
import ar.com.jass.catastro.model.Titular;
import ar.com.jass.catastro.model.TitularParcela;
import ar.com.jass.catastro.model.Tramite;
import ar.com.jass.catastro.model.cuentacorriente.CCDetalle;
import ar.com.jass.catastro.model.cuentacorriente.CCDetalle.CCDetalleBuilder;
import ar.com.jass.catastro.model.cuentacorriente.CuentaCorriente;
import ar.com.jass.catastro.model.cuentacorriente.DeudaPeriodoStatus;
import ar.com.jass.catastro.model.cuentacorriente.FileDeuda;
import ar.com.jass.catastro.model.cuentacorriente.Periodo;
import ar.com.jass.catastro.model.cuentacorriente.TipoMovimiento;
import ar.com.jass.catastro.model.pagos.EstadoFilePagos;
import ar.com.jass.catastro.model.pagos.FilePago;
import ar.com.jass.catastro.model.pagos.Pago;
import ar.com.jass.catastro.web.dto.ResumenGeneracionDeuda;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:application-context-test.xml" })
public class ModelTest {

    static Logger LOG = LoggerFactory.getLogger(ModelTest.class);

    @Autowired
    CatastroService catastroService;

    @Autowired
    @Qualifier("generalDAO")
    GeneralDAO generalDAO;

    @Autowired
    @Qualifier("ccDAO")
    CuentaCorrienteDAO ccDAO;

    @Test
    @Transactional
    @Rollback(false)
    public void test_filepago() {

        FilePago fp = new FilePago("XXXX");
        fp.setCantidadRegistros(10);
        fp.setErrores(0);
        fp.setEstado(EstadoFilePagos.FINALIZADO);
        fp.setFechaFile(Calendar.getInstance().getTime());
        fp.setHash("hashdemo");
        fp.setNombre("file prueba");
        fp.setFileHeader("header....");
        fp.setFileTrailer("trailer ...");
        fp.setSinDeuda(0);
        fp.setTotal(BigDecimal.valueOf(1000));

        generalDAO.save(fp);
    }

    @Test
    @Transactional
    public void test_presentaDeuda() {
        System.out.println(generalDAO.getDeuda(612358L));
        Cuenta c = new Cuenta();
        c.setId(612358L);
        System.out.println(catastroService.presentaDeuda(c));
    }

    @Test
    @Transactional
    public void test_cuentadv() {
        System.out.println(Cuenta.calculateDV("223456"));
        System.out.println(Cuenta.calculateDV("654321"));

        System.out.println(generalDAO.obtenerNumeroCuenta());
    }

    @Test
    @Transactional
    @Rollback(false)
    public void test_circsecc() {
        List<Circunscripcion> cs = generalDAO.getAll(Circunscripcion.class);
        for (Circunscripcion c : cs) {
            System.out.println(c.getCircunscripcion());
            for (Seccion s : c.getSecciones()) {
                System.out.println(s.getSeccion());
            }
        }
    }

    @Test
    @Transactional
    @Rollback(false)
    public void test_Tramite() {
        Tramite t = new Tramite(TipoTramite.SUB);
        t.setAnio(2011);
        // Cuentas origen
        // t.setCuentasOrigen(cuentasOrigen);

        // t.setCuentas(nuevas);
        generalDAO.save(t);
    }

    @Test
    @Transactional
    @Rollback(false)
    public void test_update_Tramite() {
        Tramite t = generalDAO.find(Tramite.class, 37L);
        t.setAnio(2017);
        // Cuentas origen
        // t.setCuentasOrigen(cuentasOrigen);

        // t.setCuentas(nuevas);
        generalDAO.save(t);
    }

    @Test
    @Transactional
    @Rollback(false)
    public void test_CuentaPersist() {
        Cuenta c = new Cuenta();
        c.setActiva(false);
        c.setCuenta("123456");
        c.setDv("3");
        Tramite t = new Tramite();
        t.setId(11l);
        c.setTramite(t);
        generalDAO.save(c);
    }

    @Test
    @Transactional
    @Rollback(false)
    public void test_TitularPersist() {
        // int randomNum = ThreadLocalRandom.current().nextInt(min, max + 1);
        Titular t = new Titular();
        t.setApellido("Apellido" + Math.random() * 1000);
        t.setNombre("Nombre");
        t.setCuitcuil("1234-" + (int) (Math.random() * 1000));
        t.setPersonaJuridica(true);
        t.setTipoDocumento(1);
        t.setDocumento(1234);
        t.setId(1L);

        generalDAO.save(t);
    }

    @Test
    @Transactional
    @Rollback(false)
    public void test_LineaPersist() {
        // int randomNum = ThreadLocalRandom.current().nextInt(min, max + 1);
        Linea l = new Linea();
        l.setFecha(new Date());

        Parcela pold = generalDAO.find(Parcela.class, 1L);
        // l.setParcela(p);
        Parcela p = new Parcela();
        p.setDomicilioInmueble(pold.getDomicilioInmueble());
        p.setPartida(122);
        p.setCuenta(pold.getCuenta());
        l.setSuperficieCubierta(Math.random() * 1000);
        l.setSuperficieSemi(Math.random() * 1000);
        l.setSuperficieTerreno(Math.random() * 1000);
        l.setValorComun(BigDecimal.valueOf(1000.2));
        l.setValorEdificio(BigDecimal.valueOf(1000.2));
        l.setValorTierra(BigDecimal.valueOf(1000.2));

        p.getLineas().add(l);
        p.setLineaVigente(l);
        // l.setParcela(p);
        // Origen plano = new Origen();
        // plano.setAnio(2222);
        // plano.setNumero(2);
        // plano.setPartido(222);

        // generalDAO.save(plano);
        // p.setOrigen(plano);
        // generalDAO.save(l);
        generalDAO.save(p);
    }

    @Test
    @Transactional
    public void test_findCuenta() {
        Cuenta c = generalDAO.find(Cuenta.class, 572402L);
        assertNotNull("Cuenta no existente!!", c);
        LOG.info("Cuenta {}", c);
        for (TitularParcela tp : c.getParcela().getTitulares()) {
            System.out.println("titular parcela " + tp);
        }
    }

    @Test
    @Transactional
    public void test_findCuentas() {
        List<Cuenta> cuentas = generalDAO.findCuentasLike("2145");
        // Linea l = generalDAO.find(Linea.class, 79867L);

        for (Cuenta c : cuentas) {
            System.out.println(c.getId());
        }
    }

    @Test
    @Transactional
    @Rollback(false)
    public void test_toString() {
        Parcela p = new Parcela();
        p.setId(Long.valueOf(1));
        Nomenclatura n = new Nomenclatura();
        n.setCircunscripcion(1);
        n.setSeccion("SE2");
        n.setFraccion(3);
        n.setFraccionLetra("3f");
        n.setManzana(4);
        n.setManzanaLetra("4m");
        n.setParcela(5);
        n.setParcelaLetra("5p");
        n.setUnidadFuncional("6");

        p.setNomenclatura(n);

        Linea linea = new Linea();
        linea.setSuperficieCubierta(11d);
        linea.setSuperficieSemi(11d);
        linea.setSuperficieTerreno(10d);
        linea.setValorComun(BigDecimal.valueOf(2));
        linea.setValorEdificio(BigDecimal.valueOf(2));
        linea.setValorTierra(BigDecimal.valueOf(2));
        linea.setFecha(new Date());

        // p.setLineaVigente(linea);
        System.out.println(p);
        generalDAO.save(p);
    }

    @Test
    @Transactional
    @Rollback(false)
    public void test_cc() {
        Cuenta cuenta = generalDAO.find(Cuenta.class, 1L);

        CuentaCorriente cc = new CuentaCorriente();
        cc.setAnio(2016);
        cc.setCuota(1);
        cc.setImporte(BigDecimal.valueOf(1322.22));
        cc.setCuenta(cuenta);
        cc.setFechaVto(new Date());
        generalDAO.save(cc);
    }

    @Test
    @Transactional
    @Rollback(false)
    public void test_createCCDetalle() {
        CuentaCorriente cc = generalDAO.find(CuentaCorriente.class, 1L);
       // CCDetalle ccDetalle = new CCDetalleBuilder(cc).monto(BigDecimal.valueOf(100.33f)).detalle("").build();

       // generalDAO.save(ccDetalle);
    }

    @Test
    @Transactional
    @Rollback(false)
    public void test_findCCDetalle() {
        CCDetalle ccDetalle = generalDAO.find(CCDetalle.class, 2L);

        System.out.println(ccDetalle);
    }

    @Test
    @Transactional
    @Rollback(false)
    public void test_periodo2() {
        Periodo p = getGeneralDAO().find(Periodo.class, 4L);

        for (FileDeuda fd : p.getFileDeuda()) {
            System.out.println(fd);
        }
    }

    @Test
    @Transactional
    @Rollback(false)
    public void test_resumenCC() {
        Periodo periodo = new Periodo();
        ResumenGeneracionDeuda res = getCcDAO().getResumenGeneracion(periodo);

        System.out.println(res.getAvgImporte());
        System.out.println(res.getMaxImporte());
        System.out.println(res.getCantidadCC());
    }

    @Test
    @Transactional
    @Rollback(false)
    public void test_cobro_cc() {
        Pago pago = new Pago();
        pago.setAnio(2016);
        pago.setCuota(1);
        pago.setImporte(BigDecimal.valueOf(1000.22));
        pago.setFechaVto(new Date());
        CuentaCorriente cc = generalDAO.find(CuentaCorriente.class, 3L);

        //generalDAO.save(cc.generateCobro(pago));
    }

    @Test
    @Transactional
    public void test_find_cc() {
        //CuentaCorriente cc = generalDAO.findCuentaCorriente(2016, 1, "cuenta1", "1", TipoMovimiento.C);
        //System.out.println(cc);
    }

    @Test
    @Transactional
    @Rollback(false)
    public void test_periodo() {
        Periodo p = new Periodo();
        p.setAnio(2018);
        p.setCuota(2);
        p.setEstado(DeudaPeriodoStatus.A_PROCESAR);
        p.setFechaVto(Calendar.getInstance().getTime());
        p.setFechaSegundoVto(Calendar.getInstance().getTime());
        p.setGenerarAnual(true);
        p.setNumeroCuotaAnual(2);
        p.setPeriodosAnual(11);
        generalDAO.save(p);

    }

    public CatastroService getCatastroService() {
        return catastroService;
    }

    public void setCatastroService(CatastroService catastroService) {
        this.catastroService = catastroService;
    }

    public GeneralDAO getGeneralDAO() {
        return generalDAO;
    }

    public void setGeneralDAO(GeneralDAO generalDAO) {
        this.generalDAO = generalDAO;
    }

    public CuentaCorrienteDAO getCcDAO() {
        return ccDAO;
    }

    public void setCcDAO(CuentaCorrienteDAO ccDAO) {
        this.ccDAO = ccDAO;
    }

}
