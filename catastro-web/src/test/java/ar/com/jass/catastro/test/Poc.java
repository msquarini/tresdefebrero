package ar.com.jass.catastro.test;

import java.util.Date;
import java.util.Locale;

public class Poc {

	public static void main(String[] args) {
		Poc.format();
	}

	
	public static void format() {
		// Sample Credicoop
		String format = "0001911%1$6.6s%2$1.1s%3$td%3$tm%3$ty1%4$-4s%5$-10s%6$011.2f  %7$2d%8$02d                            ";
		//Object[] values = {"884455", "2", new Date(), "suc", "bco", 2000.22, 15, 1};
		// Sample Frances
		format = "130999001242ABL%1$22s%2$1.1s%3$22s%4$tY%4$tm%4$td%5$011.2f00%1$6.6s%2$1.1s%6$2d%7$02d0                           ";
		
		Object[] values = {"884455", "2", "cbu", new Date(), 2000.22, 15, 1};
		System.out.println(String.format(Locale.US,format, values));
	}
}
