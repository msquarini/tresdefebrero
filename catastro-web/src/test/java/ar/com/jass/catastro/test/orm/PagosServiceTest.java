package ar.com.jass.catastro.test.orm;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import ar.com.jass.catastro.business.PagosService;
import ar.com.jass.catastro.dao.GeneralDAO;
import ar.com.jass.catastro.model.pagos.FilePago;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:application-context.xml" })
public class PagosServiceTest {

    static Logger LOG = LoggerFactory.getLogger(PagosServiceTest.class);

    @Autowired
    PagosService pagosService;

    @Autowired
    @Qualifier("generalDAO")
    GeneralDAO generalDAO;



    @Test
    @Transactional()
    @Rollback(value = false)
    public void test_findPagos() {
        Calendar c = Calendar.getInstance();
        Date from = c.getTime();        
        List<FilePago> pagos = pagosService.getFilePagos(from, from, null);
        for (FilePago fp : pagos) {
            LOG.info("{}", fp);
        }
    }

}
