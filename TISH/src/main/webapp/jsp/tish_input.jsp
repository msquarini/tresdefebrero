<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>
<html lang="es">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="keywords" content="Tres de febrero, Seguridad, higiene" />
<meta name="description"
	content="Municipalidad de Tres de Febrero - Tasa Seguridad e Higiene" />
<title>Municipalidad de Tres de Febrero</title>

<!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
<!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
<sb:head includeScripts="false" includeScriptsValidation="false" />
<sj:head locale="es" compressed="true" loadAtOnce="true" jqueryui="true"
	loadFromGoogle="false" />
<script type="text/javascript" src="js/jquery.number.js"></script>
<script type="text/javascript">
	$(function() {
		// Set up the number formatting.
		$('#cuenta').number(true, 0, '', '');
		$('#telefono').number(true, 0, '', '');
		$('#cuit').number(true, 0, '', '');

		$("#cuenta").attr("autocomplete", "off");
		$("#dv").attr("autocomplete", "off");
		$("#cuit").attr("autocomplete", "off");
		$("#telefono").attr("autocomplete", "off");
		$("#mail").attr("autocomplete", "off");

		$("#cuenta").focus();

	});
</script>

<style type="text/css">
#number_container {
	border: 1px dotted #d0d0d0;
	padding: 15px;
	margin: 10px;
	display: none;
	background: #fafafa;
}

.navbar-brand, .navbar-nav li a {
	line-height: 80px;
	height: 80px;
	padding-top: 0;
}

body {
	padding-top: 60px;
	/* 60px to make the container go all the way to the bottom of the topbar */
}

.top-buffer {
	margin-top: 50px;
}
</style>
</head>

<body>
	<!-- 	<nav class="navbar navbar-default navbar-fixed-top">
		<div class="row">
			<a href="http://www.tresdefebrero.gov.ar"> <img
				src="img/logo.svg" alt="" height="75" width="180">
			</a>
		</div>
	</nav> -->

	<nav class="navbar navbar-default navbar-fixed-top">
		<div class="row">
			<a class="navbar-brand" href="http://www.tresdefebrero.gov.ar"> <span><img
					src="img/logo-new.png" alt="" height="75" width="180"></span>
			</a>
		</div>
	</nav>

	<div class="container" id="container">

		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<h1>Tasa de Seguridad e Higiene</h1>

				<s:actionerror theme="bootstrap" />
				<s:actionmessage theme="bootstrap" />


				<p>Ingrese la cuenta y d�gito verificador para generar el
					volante electr�nico de pago</p>
				<s:form action="tish_execute" enctype="multipart/form-data"
					theme="bootstrap" cssClass="form-horizontal" label=""
					id="findAccount" focusElement="cuenta">
					<fieldset>
						<div class="row">
							<div class="col-xs-9 col-sm-6 col-md-3 col-lg-3">
								<label for="cuenta">Cuenta</label>
								<s:textfield id="cuenta" name="cuenta" class="number"
									tooltip="Ingrese el n�mero de cuenta" maxlength="6"
									/>
							</div>
							<div class="col-xs-4 col-sm-3 col-md-3 col-lg-2">
								<label for="DV">D�gito Verificador</label>
								<s:textfield name="dv" id="dv" maxlength="1"  />
							</div>
							<!-- <div class="col-md-4 col-lg-6"></div> -->
						</div>
						<div class="row">
							<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
								<label for="cuit">Cuit/Cuil</label>
								<s:textfield id="cuit" name="cuit" class="number"
									tooltip="Ingrese n�mero de CUI/CUIL" maxlength="11"
									 />
							</div>
							<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
								<label for="telefono">Telefono</label>
								<s:textfield name="telefono" id="telefono" class="number"
									tooltip="Tel�fono de contacto" maxlength="11" />
							</div>
							<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
								<label for="mail">Mail</label>
								<s:textfield name="mail" id="mail" tooltip="Direccion de mail"
									maxlength="50"  />
							</div>
							<!-- <div class="col-md-4 col-lg-6"></div> -->
						</div>
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<s:url var="findAccount" action="seguridadhigiene_execute" />
								<s:submit cssClass="btn btn-primary" value="Enviar"></s:submit>
							</div>
						</div>

					</fieldset>
				</s:form>
			</div>
		</div>
	</div>


	<footer class="navbar-default top-buffer">

		<p align="center">
			� Copyright 2015 - 2016 - <strong>MUNICIPALIDAD DE TRES DE
				FEBRERO</strong> - Conmutador: 4734-2400 - Todos los derechos reservados
		</p>
	</footer>


</body>
</html>