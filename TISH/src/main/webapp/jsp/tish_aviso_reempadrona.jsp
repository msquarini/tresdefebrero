<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<%
  response.setHeader("Cache-Control", "no-cache");
  response.setHeader("Pragma", "no-cache");
  response.setDateHeader("Expires", -1);
%>

<html lang="es">
<head>
<meta http-equiv="Cache-Control"
	content="no-cache, no-store, must-revalidate" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="0" />

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="keywords" content="Tres de febrero, Seguridad, higiene" />
<meta name="description"
	content="Municipalidad de Tres de Febrero - Tasa Seguridad e Higiene" />
<title>Municipalidad de Tres de Febrero</title>

<!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
<!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
<sb:head includeScripts="false" includeScriptsValidation="false" />
<sj:head locale="es" compressed="true" loadAtOnce="true" jqueryui="true"
	loadFromGoogle="false" />

</head>
<style type="text/css">
#number_container {
	border: 1px dotted #d0d0d0;
	padding: 15px;
	margin: 10px;
	display: none;
	background: #fafafa;
}

.navbar-brand, .navbar-nav li a {
	line-height: 80px;
	height: 80px;
	padding-top: 0;
}

body {
	padding-top: 60px;
	/* 60px to make the container go all the way to the bottom of the topbar */
}

.top-buffer {
	margin-top: 50px;
}
</style>
<body>

	<nav class="navbar navbar-default navbar-fixed-top">
		<div class="row">
			<a class="navbar-brand" href="http://www.tresdefebrero.gov.ar"> <span><img
					src="img/logo-new.png" alt="" height="75" width="180"></span>
			</a>
		</div>
	</nav>

	<div class="container" id="container">


		<div class="row">
			<div class="col-md-9">
				<h1>Tasa de Seguridad e Higiene</h1>
				<div class="alert alert-warning" role="alert">
					<p>Sr. Contribuyente, le informamos que hasta el momento no
						registramos que haya cumplimentado con su tr�mite de
						Reempadronamiento establecido en el Decreto N� 529/17. Le
						recordamos que al d�a de la fecha la multa por incumplimiento de
						los deberes formales seg�n Ordenanza Fiscal 2017 es de $900 m�s
						intereses y a partir del mes de julio ser� de 1.000 pesos.</p>
					<p>
						Lo invitamos a que ingrese en el siguiente link y regularice
						cuanto antes su situaci�n. <a
							href="https://tresdefebrero.gov.ar/tish/CtribInicioSistemaController.do">TISH</a>
					</p>
				</div>

			</div>
			<div class="col-md-9">
				<s:url var="continuar" action="tish_continuar" />
				<s:a theme="bootstrap" cssClass="btn btn-primary"
					href="%{continuar}">Continuar</s:a>
			</div>
		</div>
	</div>
	<footer class="navbar-default top-buffer">

		<p align="center">
			� Copyright 2015 - 2016 - <strong>MUNICIPALIDAD DE TRES DE
				FEBRERO</strong> - Conmutador: 4734-2400 - Todos los derechos reservados
		</p>
	</footer>

</body>