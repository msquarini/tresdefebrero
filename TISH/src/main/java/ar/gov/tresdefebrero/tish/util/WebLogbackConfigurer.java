package ar.gov.tresdefebrero.tish.util;

import java.io.FileNotFoundException;

import javax.servlet.ServletContext;

import org.springframework.util.ResourceUtils;
import org.springframework.util.SystemPropertyUtils;
import org.springframework.web.util.WebUtils;

import ch.qos.logback.core.joran.spi.JoranException;

public class WebLogbackConfigurer {
	/**
	 * Parameter specifying the location of the logback config file
	 */
	public static final String CONFIG_LOCATION_PARAM = "logbackConfigLocation";
	/**
	 * Parameter specifying whether to expose the web app root system property
	 */
	public static final String EXPOSE_WEB_APP_ROOT_PARAM = "logbackExposeWebAppRoot";

	private WebLogbackConfigurer() {
	}

	/**
	 * Initialize Logback, including setting the web app root system property.
	 * 
	 * @param servletContext
	 *            the current ServletContext
	 * @see org.springframework.web.util.WebUtils#setWebAppRootSystemProperty
	 */
	public static void initLogging(ServletContext servletContext) {
		// Expose the web app root system property.
		if (exposeWebAppRoot(servletContext)) {
			WebUtils.setWebAppRootSystemProperty(servletContext);
		}

		// Only perform custom Logback initialization in case of a config file.
		String location = servletContext
				.getInitParameter(CONFIG_LOCATION_PARAM);
		if (location != null) {
			// Perform actual Logback initialization; else rely on Logback's
			// default initialization.
			try {
				// Return a URL (e.g. "classpath:" or "file:") as-is;
				// consider a plain file path as relative to the web application
				// root directory.
				if (!ResourceUtils.isUrl(location)) {
					// Resolve system property placeholders before resolving
					// real path.
					location = SystemPropertyUtils
							.resolvePlaceholders(location);
					location = WebUtils.getRealPath(servletContext, location);
				}

				// Write log message to server log.
				servletContext.log("Initializing Logback from [" + location
						+ "]");

				// Initialize
				LogbackConfigurer.initLogging(location);
			} catch (FileNotFoundException ex) {
				throw new IllegalArgumentException(
						"Invalid 'logbackConfigLocation' parameter: "
								+ ex.getMessage());
			} catch (JoranException e) {
				throw new RuntimeException(
						"Unexpected error while configuring logback", e);
			}
		}

	}

	/**
	 * Shut down Logback, properly releasing all file locks and resetting the
	 * web app root system property.
	 * 
	 * @param servletContext
	 *            the current ServletContext
	 * @see WebUtils#removeWebAppRootSystemProperty
	 */
	public static void shutdownLogging(ServletContext servletContext) {
		try {
			servletContext.log("Shutting down Logback");
			LogbackConfigurer.shutdownLogging();
		} finally {
			// Remove the web app root system property.
			if (exposeWebAppRoot(servletContext)) {
				WebUtils.removeWebAppRootSystemProperty(servletContext);
			}
		}
	}

	/**
	 * Return whether to expose the web app root system property, checking the
	 * corresponding ServletContext init parameter.
	 * 
	 * @param servletContext
	 *            the servlet context
	 * @return {@code true} if the webapp's root should be exposed; otherwise,
	 *         {@code false}
	 * @see #EXPOSE_WEB_APP_ROOT_PARAM
	 */
	@SuppressWarnings({ "BooleanMethodNameMustStartWithQuestion" })
	private static boolean exposeWebAppRoot(ServletContext servletContext) {
		String exposeWebAppRootParam = servletContext
				.getInitParameter(EXPOSE_WEB_APP_ROOT_PARAM);
		return (exposeWebAppRootParam == null || Boolean
				.valueOf(exposeWebAppRootParam));
	}
}
