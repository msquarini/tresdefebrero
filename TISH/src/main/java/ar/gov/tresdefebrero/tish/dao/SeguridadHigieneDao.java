package ar.gov.tresdefebrero.tish.dao;

import java.util.Date;
import java.util.List;

import ar.gov.tresdefebrero.tish.beans.PeriodoBean;
import ar.gov.tresdefebrero.tish.beans.SeguridadHigieneBean;

public interface SeguridadHigieneDao {

    public void registrarVEP(SeguridadHigieneBean bean);

    public SeguridadHigieneBean findAccount(Integer cuenta, String dv);
    
    public PeriodoBean findPeriodo(Integer idImpuesto, Date fecha);
    
    public List<PeriodoBean> findPeriodos(final PeriodoBean periodo);
    
    public List<PeriodoBean> findPeriodoPrevios(final PeriodoBean periodo, Integer cuenta);
    
    public boolean cuentaReempadronada(Integer cuenta);
}
