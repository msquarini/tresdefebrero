package ar.gov.tresdefebrero.tish.beans;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import ar.gov.tresdefebrero.tish.dao.SeguridadHigieneDao;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;

public class TestSQL {

    public static void main(String[] args) {

        ApplicationContext context = new ClassPathXmlApplicationContext("application-context.xml");
        SeguridadHigieneDao dao = (SeguridadHigieneDao) context.getBean("jdbcSeguridadHigieneDao");
        
        SeguridadHigieneBean bean = dao.findAccount(156011, "4");
//        
        bean.setMontoMinimoAjustado(BigDecimal.valueOf(100.4));
        bean.setMontoDiferencias(BigDecimal.valueOf(22.41));
        bean.setMonto(BigDecimal.valueOf(110.99));
//        dao.registrarVEP(bean);
        
        PeriodoBean periodo = dao.findPeriodo(1, new Date());
        bean.setPeriodoBean(periodo);
        System.out.println(bean.getBarcode());
    }
}
