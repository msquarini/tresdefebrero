package ar.gov.tresdefebrero.tish.actions;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletContext;

import org.apache.struts2.interceptor.SessionAware;
import org.apache.struts2.util.ServletContextAware;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import com.opensymphony.xwork2.ActionSupport;

import ar.gov.tresdefebrero.tish.beans.PeriodoBean;
import ar.gov.tresdefebrero.tish.beans.SeguridadHigieneBean;
import ar.gov.tresdefebrero.tish.dao.SeguridadHigieneDao;
import ar.gov.tresdefebrero.tish.util.ApplicationContextHelper;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;

// @ParentPackage(value = "poc")
public class SeguridadHigieneAction extends ActionSupport
    implements SessionAware, ServletContextAware {

  private static final long serialVersionUID = 7353477345330099548L;

  private static final Logger logger = LoggerFactory.getLogger("SeguridadHigieneAction");

  private ServletContext context;

  private Integer cuenta;
  private String dv;
  private String cuit;
  private String telefono;
  private String mail;
  /*
   * private Double montoMinimoAjustado; private Double montoDiferencias;
   */
  // private Double alicuota;
  // private Double baseImponible;

  // private List<PeriodoBean> periodos;

  private Double montoPagar;

  private Integer idPeriodoSelected;

  /**
   * @return the montoMinimoAjustado
   */
  /*
   * public Double getMontoMinimoAjustado() { return montoMinimoAjustado; }
   */

  /**
   * @param montoMinimoAjustado the montoMinimoAjustado to set
   */
  /*
   * public void setMontoMinimoAjustado(Double montoMinimoAjustado) { this.montoMinimoAjustado =
   * montoMinimoAjustado; }
   */

  /**
   * @return the montoDiferencias
   */
  /*
   * public Double getMontoDiferencias() { return montoDiferencias; }
   */

  /**
   * @param montoDiferencias the montoDiferencias to set
   */
  /*
   * public void setMontoDiferencias(Double montoDiferencias) { this.montoDiferencias =
   * montoDiferencias; }
   */

  private SeguridadHigieneDao dao;

  private Map<String, Object> session;

  private InputStream inputStreamA;
  private String fileName;

  public static final String EMAIL_ADDRESS_PATTERN =
      "\\b^['_a-z0-9-\\+]+(\\.['_a-z0-9-\\+]+)*@[a-z0-9-]+(\\.[a-z0-9-]+)*\\.([a-z]{2}|aero|arpa|asia|biz|com|coop|edu|gov|info|int|jobs|mil|mobi|museum|name|nato|net|org|pro|tech|tel|travel|xxx)$\\b";

  /**
   * 
   */
  public SeguridadHigieneAction() {
    ApplicationContext springContext = ApplicationContextHelper.getApplicationContext();
    dao = (SeguridadHigieneDao) springContext.getBean("jdbcSeguridadHigieneDao");
    // dao = (SeguridadHigieneDao) springContext.getBean("dummy");
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.apache.struts2.util.ServletContextAware#setServletContext(javax.
   * servlet.ServletContext)
   */
  @Override
  public void setServletContext(ServletContext context) {
    this.context = context;
  }

  public Map<String, Object> getSession() {
    return session;
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.apache.struts2.interceptor.SessionAware#setSession(java.util.Map)
   */
  @Override
  public void setSession(Map<String, Object> session) {
    this.session = session;
  }

  private boolean validateCuit() {
    if ((getCuit() == null) || (getCuit().length() != 11)) {
      return false;
    }
    char[] cuitArray = this.cuit.toCharArray();

    int[] mult = {5, 4, 3, 2, 7, 6, 5, 4, 3, 2};
    int total = 0;
    for (int i = 0; i < mult.length; i++) {
      total += Character.getNumericValue(cuitArray[i]) * mult[i];
    }
    int mod = total % 11;
    int digito = mod == 1 ? 9 : mod == 0 ? 0 : 11 - mod;

    return digito == Character.getNumericValue(cuitArray[10]);
  }

  private boolean validateTelefono() {
    if ((getTelefono() == null) || (getTelefono().length() < 10)) {
      return false;
    }
    return true;
  }

  private boolean validateMail() {
    if (getMail() == null) {
      return false;
    }
    Pattern pattern = Pattern.compile(
        "\\b^['_a-z0-9-\\+]+(\\.['_a-z0-9-\\+]+)*@[a-z0-9-]+(\\.[a-z0-9-]+)*\\.([a-z]{2}|aero|arpa|asia|biz|com|coop|edu|gov|info|int|jobs|mil|mobi|museum|name|nato|net|org|pro|tech|tel|travel|xxx)$\\b",
        2);
    String compare = getMail().trim();
    Matcher matcher = pattern.matcher(compare);
    return matcher.matches();
  }

  public String input() {
    logger.info("tish - input");
    getSession().remove("cuenta");
    return "input";
  }

  public String execute() throws Exception {

    logger.info("tish - execute, cuenta {}, dv {}", this.cuenta, this.dv);
    if (!validateMail()) {
      addFieldError("mail", getText("mail.invalido"));
      logger.info("tish - mail invalido {}", this.mail);
    }
    if (!validateCuit()) {
      addFieldError("cuit", getText("cuit.invalido"));
      logger.info("tish - cuit invalido {}", this.cuit);
    }
    if (!validateTelefono()) {
      addFieldError("telefono", getText("telefono.invalido"));
      logger.info("tish - telefono invalido {}", this.telefono);
    }
    if ((this.cuenta == null)
        || ((this.cuenta.intValue() > 199999) && (this.cuenta.intValue() < 90000))) {
      addFieldError("cuenta", getText("cuenta.invalida"));
      logger.info("tish - cuenta invalidad {}", this.cuenta);
    }
    if (this.dv == null) {
      addFieldError("dv", getText("dv.invalido"));
      logger.info("tish - dv is null");
    }
    if (hasFieldErrors()) {
      return "input";
    }
    if (this.dao.cuentaReempadronada(this.cuenta)) {
      logger.info("tish - cuenta reempadronada {}", this.cuenta);
      return "reempadronada";
    }
    SeguridadHigieneBean bean = this.dao.findAccount(this.cuenta, this.dv);
    if (bean == null) {
      logger.info("tish - cuenta no entontrada{}", this.cuenta);
      addActionError(getText("cuenta.notfound"));
      return "input";
    }
    PeriodoBean periodo = this.dao.findPeriodo(Integer.valueOf(1), new Date());
    if (periodo == null) {
      logger.error("tish - periodo no encontrado impuesto {}, fecha {}", Integer.valueOf(1),
          new Date());
      addActionError(getText("periodo.notexists"));
      return "input";
    }
    bean.setPeriodoBean(periodo);

    List<PeriodoBean> periodos = this.dao.findPeriodos(periodo);
    periodos.add(0, periodo);

    periodos.addAll(this.dao.findPeriodoPrevios(periodo, this.cuenta));

    bean.setTelefono(getTelefono());
    bean.setMail(getMail());
    bean.setCuit(getCuit());
    getSession().put("periodos", periodos);
    getSession().put("cuenta", bean);
    return "aviso";
  }

  public String continuar() {
    return "success";
  }

  /**
   * Descargar el PDF
   * 
   * @return
   * @throws Exception
   */
  public String generate() throws Exception {
    logger.info("tish - periodo {}", idPeriodoSelected);

    SeguridadHigieneBean bean = (SeguridadHigieneBean) getSession().get("cuenta");
    PeriodoBean periodo = obtenerPeriodo(idPeriodoSelected);

    if (bean == null || periodo == null) {
      logger.error("no se encontro en session SHB {} | PB {}", bean, periodo);
      return input();
    }

    logger.info("tish - generate, datos {}, periodo {}", bean, periodo.getLeyenda());

    /*
     * if (getMontoDiferencias() == null) { addFieldError("montoDiferencias", "Monto invalido"); }
     * if (getMontoMinimoAjustado() == null) { addFieldError("montoMinimoAjustado",
     * "Monto invalido"); }
     */
    if (getMontoPagar() == null) {
      addFieldError("montoPagar", getText("monto.invalido"));
    } else {
      // Actualizo el periodo a pagar con el elegido!!
      bean.setPeriodoBean(periodo);
      calcularMonto(bean, periodo);
    }

    if (hasFieldErrors() || hasActionErrors()) {
      return SUCCESS;
    }

    return "predownload";
  }

  private void calcularMonto(SeguridadHigieneBean bean, PeriodoBean periodo) {
    // Double monto = getMontoDiferencias() + getMontoMinimoAjustado();
    Double monto = getMontoPagar();

    // 20170320 - MS - Validacion de minimo x el periodo que se quiera pagar
    if (monto > periodo.getMinimo()) {
      // if (monto > bean.getMontoMinimo().doubleValue()) {
      /*
       * bean.setMontoDiferencias(BigDecimal.valueOf(getMontoDiferencias() ));
       * bean.setMontoMinimoAjustado(BigDecimal.valueOf( getMontoMinimoAjustado()));
       */
      bean.setMonto(BigDecimal.valueOf(monto).setScale(2, java.math.RoundingMode.HALF_UP));

      if (!periodo.esVigente()) {

        if (periodo.getAnio() > 2015) {
          Calendar now = Calendar.getInstance();
          // Fecha vencimiento del periodo seleccionado
          Calendar desde = Calendar.getInstance();
          desde.setTime(periodo.getFechaVto());

          // Calculo de meses de diferencia para aplicar interes
          int diffYear = now.get(Calendar.YEAR) - desde.get(Calendar.YEAR);
          // int diffMonth = diffYear * 12 + now.get(Calendar.MONTH) - desde.get(Calendar.MONTH);
          double diffMonth = diffYear * 12 + now.get(Calendar.MONTH) - desde.get(Calendar.MONTH);

          // Agregamos el interes del 1% mensual
          // 20170530 MS agregado de 5 dias de interes
          diffMonth = diffMonth + 0.16;
          monto = monto + (monto * diffMonth / 100);
          bean.setMonto(BigDecimal.valueOf(monto).setScale(2, java.math.RoundingMode.HALF_UP));
        } else {
          // Periodos previos al 2016, calculo con INDICE de actualizacion
          monto = monto + (monto * periodo.getIndice());
          bean.setMonto(BigDecimal.valueOf(monto).setScale(2, java.math.RoundingMode.HALF_UP));
        }
        // Seteo el periodo que se va a pagar (Nro periodo / anio, ID)
        bean.setPeriodo(periodo.getPeriodo().toString());
        bean.setIdPeriodo(periodo.getIdPeriodo());
        // bean.setFechaVto(periodo.getFechaVto());
        bean.setPeriodoAnio(periodo.getAnio());
      }
    } else {
      logger.info("tish - monto menor que el minimo, datos {}", bean);
      addActionError(getText("monto.menor.minimo"));
    }
  }

  /**
   * Descarga de pdf
   * 
   * @return
   * @throws Exception
   */
  public String download() throws Exception {
    logger.info("descargando volante de pago");
    SeguridadHigieneBean bean = (SeguridadHigieneBean) getSession().get("cuenta");
    if (bean == null) {
      return input();
    }

    try {
      dao.registrarVEP(bean);
    } catch (Exception e) {
      logger.error("", e);
    }

    generarVolantePago(bean);

    // Elimino la cuenta de session
    getSession().remove("cuenta");
    getSession().remove("periodos");

    return "download";
  }

  /**
   * Crear PDF
   */
  private void generarVolantePago(SeguridadHigieneBean bean) {
    try {

      JasperReport jasperReport =
          JasperCompileManager.compileReport("/www/tomcat/conf/tish/tishv2.jrxml");

      // JasperReport jasperReport = JasperCompileManager
      // .compileReport(context.getResourceAsStream("/WEB-INF/classes/tish.jrxml"));

      Map<String, Object> params = new HashMap<String, Object>();
      params.put("bean", bean);

      // Ejecucion de fillReport
      JasperPrint jasperPrint =
          JasperFillManager.fillReport(jasperReport, params, new JREmptyDataSource());

      // Export
      File pdf = File.createTempFile("tish-" + bean.getCuenta(), ".pdf");

      setFileName(pdf.getName());

      JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
      inputStreamA = new FileInputStream(pdf);
    } catch (JRException e) {
      logger.error("", e);
    } catch (IOException e) {
      logger.error("", e);
    }
  }

  public Integer getCuenta() {
    return cuenta;
  }

  public void setCuenta(Integer cuenta) {
    this.cuenta = cuenta;
  }

  public String getDv() {
    return dv;
  }

  public void setDv(String dv) {
    this.dv = dv;
  }

  public InputStream getInputStreamA() {
    return inputStreamA;
  }

  public void setInputStreamA(InputStream inputStream) {
    this.inputStreamA = inputStream;
  }

  public String getFileName() {
    return fileName;
  }

  public void setFileName(String fileName) {
    this.fileName = fileName;
  }

  public Integer getIdPeriodoSelected() {
    return idPeriodoSelected;
  }

  public void setIdPeriodoSelected(Integer idPeriodoSelected) {
    this.idPeriodoSelected = idPeriodoSelected;
  }

  private PeriodoBean obtenerPeriodo(Integer idPeriodo) {
    List<PeriodoBean> periodos = (List<PeriodoBean>) getSession().get("periodos");
    PeriodoBean bean = null;
    for (PeriodoBean p : periodos) {
      if (p.getIdPeriodo() == idPeriodo) {
        bean = p;
        break;
      }
    }
    return bean;
  }

  public Double getMontoPagar() {
    return montoPagar;
  }

  public void setMontoPagar(Double montoPagar) {
    this.montoPagar = montoPagar;
  }

  public String getCuit() {
    return cuit;
  }

  public void setCuit(String cuit) {
    this.cuit = cuit;
  }

  public String getTelefono() {
    return telefono;
  }

  public void setTelefono(String telefono) {
    this.telefono = telefono;
  }

  public String getMail() {
    return mail;
  }

  public void setMail(String mail) {
    this.mail = mail;
  }
}
