package ar.gov.tresdefebrero.tish.util;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public class ApplicationContextHelper implements ApplicationContextAware{

	private static ApplicationContextHelper INSTANCE;
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		context = applicationContext;		
	}

	private ApplicationContext context;
	
	private ApplicationContextHelper() {
		INSTANCE = this;
	}
	
	public static ApplicationContext getApplicationContext() {
		return INSTANCE.context;
	}
	
}
